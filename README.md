# Foodcrowd Store

[![N|Solid](https://www.foodcrowd.com/medias/logo-aldahra-responsiv.svg?context=bWFzdGVyfGltYWdlc3w1MjAyfGltYWdlL3N2Zyt4bWx8aDFjL2g0NS84Nzk2MTg2NTQyMTEwL2xvZ29fYWxkYWhyYV9yZXNwb25zaXYuc3ZnfDkyZjI3NmFhNzMwMzlmZjZiMjczOWNlYTBjYTM4ZTc2ZmFhZmM2Y2UyMDFjODBhMjE2MWE5NDY5ODM0NWE2ZmI)](https://www.foodcrowd.com/)

This Repository represents Aldahra CX Implementatoin
<br />
<br />
### Description:

  - Hybris version: SAP cloud 1905.
  - Hybris B2C Project.
  - JAVA version 11.
  - The DB used on the local machine is MySQL.
<br />
<br />

### Required tools
* Mysql-Server Version 8.X.X 
* Mysql Workbench to access the Database easily
* Git 
* Basic Knowledge with command line
---
<br /> 

## Installation and Run:


#### Hybris Server Preparation:
* Extract SAP-Hybris 1905 inside a folder.
* Extract CXCEP190500P_13-80004551.zip file inside the same folder 
> If the system asks you to override files, click override
* Navigate to "CXCOMM190500P_6-70004140/hybris/bin/platfrom" using commnand line 
* Run the following command, and press enter
```
. ./setantenv.sh && ant clean all 
```

#### Notes

* > The command will Prompt the user to select an envirnoment (default: develop)
* > This command will create "config" folder inside "hybris" folder

<br />


* Navigate to "/hybris/bin/" Directory and create new Folder named "custom"
* Navigate to the newly created direcotry and run the following command
```
git clone https://husamd@bitbucket.org/ecommercearabia/aldahrastore.git.
cd aldahrastore
git fetch && git checkout development
git pull
```
> A new Directory will be created called "aldahrastore", inside it 2 directories called

> If there is a specific branch you want to start, use the command "git fetch && git checkout [your branch name]"

* Open Mysql Workbench and create new schema with the name "Hybris1905AldahraMaster"

* From "hybris/bin/custom/aldahrastore/setup" directory
    1. Copy "mysql-connector-java-8.0.21.jar" to "hybris/bin/platform/lib/dbdriver"
    2. Copy "local.properties" and "localextensions.xml" files to "hybris/bin/config/"

* Open local.properties and change the following properties 
    * db.username
    * db.password

* Navigate to "CXCOMM190500P_6-70004140/hybris/bin/platfrom" directory and run the following command 
```
. ./setantenv.sh && ant clean all && ./hybrisserver.sh
```
> The above step will generate "SOLR" config directory 
* When the server finally runs, stop the server using the "CTRL+C" keys and navigate to  "CXCOMM190500P_6-70004140/hybris/config/solr/configsets/default/conf" 
    1. Copy "schema.xml" and "solrconfig.xml" to the folder
    2. Copy "contractions_ar.txt" and "stopwords_ar.txt" to "lang" direcotry

* Configuring hosts file
    * On Unix Based Systems
        * Open /etc/hosts file
    * On Windows 
        * Open C:\Windows\System32\drivers\etc\hosts file
    * Add the follwing line 
        ```
        127.0.0.1	localhost foodcrowd.local
        ```


* Finally navigate to "CXCOMM190500P_6-70004140/hybris/bin/platfrom" directory and run the following commands only **ONCE**
```
 . ./setantenv.sh && ant addoninstall -Daddonnames="aldahraassistedservicecustomstorefront,aldahraassistedservicepromotioncustomaddon,aldahrasmarteditcustomaddon,personalizationaddon,aldahragoogletagmanager,aldahraconsignmenttrackingcustomaddon,aldahraordermanagementcustomaddon,sapqualtricsaddon,sapymktrecommendationaddon" -DaddonStorefront.yacceleratorstorefront="aldahrastorefront"
 . ./setantenv.sh && ant clean initialize && ./hybrisserver.sh
```

* To start the server another time just use the following command 
```
. ./setantenv.sh && ant clean all && ./hybrisserver.sh
```


If you configured you hosts file you should use the follwing links

* storefront : https://foodcrowd.local:9002/?site=foodcrowd-ae </br>
* Hac: https://foodcrowd.local:9002/hac </br>
* Backoffice: https://foodcrowd.local:9002/backoffice </br>
* ASM: https://foodcrowd.local:9002/?site=foodcrowd-ae&asm=true </br>
* Smartedit: https://foodcrowd.local:9002/smartedit/ </br>
* OCC: https://foodcrowd.local:9002/rest/v2/swagger-ui.html#/ </br>
* Commerce Webservices v2 : https://foodcrowd.local:9002/rest/v2/swagger-ui.html#/ </br>
* Order Management Webservices : https://foodcrowd.local:9002/omswebservice/swagger-ui.html# </br>
* Warehousing Webservices : https://foodcrowd.local:9002/warehousingwebservice/swagger-ui.html# </br>
* CMS Webservices : https://foodcrowd.local:9002/cmswebservices/swagger-ui.html# </br>
* Permission Webservices: https://foodcrowd.local:9002/permissionswebservices/swagger-ui.html#/ </br>
* SmartEdit Webservices: https://foodcrowd.local:9002/smarteditwebservices/swagger-ui.html#/ </br>
* CMS SmartEdit Webservices: https://foodcrowd.local:9002/cmssmarteditwebservices/swagger-ui.html#/ </br>
* Preview Webservices : https://foodcrowd.local:9002/previewwebservices/swagger-ui.html#/ </br>
* Personalization Webservices: https://foodcrowd.local:9002/personalizationwebservices/swagger-ui.html#/ </br>
* Authority Webservices: https://foodcrowd.local:9002/authoritywebservice/swagger-ui.html#/Authorities </br>



> If you did not configure hosts file use the following links

* storefront : https://localhost:9002/?site=foodcrowd-ae </br>
* Hac: https://localhost:9002/hac </br>
* Backoffice: https://localhost:9002/backoffice </br>
* ASM: https://localhost:9002/?site=foodcrowd-ae&asm=true </br>
* Smartedit: https://localhost:9002/smartedit/ </br>
* OCC: https://localhost:9002/rest/v2/swagger-ui.html#/ </br>
* Commerce Webservices v2 : https://localhost:9002/rest/v2/swagger-ui.html#/ </br>
* Order Management Webservices : https://localhost:9002/omswebservice/swagger-ui.html# </br>
* Warehousing Webservices : https://localhost:9002/warehousingwebservice/swagger-ui.html# </br>
* CMS Webservices : https://localhost:9002/cmswebservices/swagger-ui.html# </br>
* Permission Webservices: https://localhost:9002/permissionswebservices/swagger-ui.html#/ </br>
* SmartEdit Webservices: https://localhost:9002/smarteditwebservices/swagger-ui.html#/ </br>
* CMS SmartEdit Webservices: https://localhost:9002/cmssmarteditwebservices/swagger-ui.html#/ </br>
* Preview Webservices : https://localhost:9002/previewwebservices/swagger-ui.html#/ </br>
* Personalization Webservices: https://localhost:9002/personalizationwebservices/swagger-ui.html#/ </br>
* Authority Webservices: https://localhost:9002/authoritywebservice/swagger-ui.html#/Authorities </br>