/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrawebserviceapi.constants;

/**
 * Global class for all Aldahrawebserviceapi constants. You can add global constants for your extension into this class.
 */
public final class AldahrawebserviceapiConstants extends GeneratedAldahrawebserviceapiConstants
{
	public static final String EXTENSIONNAME = "aldahrawebserviceapi";

	private AldahrawebserviceapiConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "aldahrawebserviceapiPlatformLogo";
}
