/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.erp;

import java.util.Optional;

import com.aldahra.aldahraerpclientservices.erp.response.SiteStockResponse;
import com.aldahra.aldahraerpclientservices.exception.ERPWSServiceException;


/**
 * @author mohammad-abumuhasien
 */
public interface ERPWSUpdateSiteStockService
{

	Optional<SiteStockResponse> getSiteStockData(String productCode, String stockSite, String username, String password,
			String poolName,
			String schemaName) throws ERPWSServiceException;


}
