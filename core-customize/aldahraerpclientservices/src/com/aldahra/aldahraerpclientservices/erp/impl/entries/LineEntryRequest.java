/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.erp.impl.entries;

/**
 * @author monzer
 */
public class LineEntryRequest
{
	private String productCode;
	private String productName;
	private String unitOfMeasure;
	private String quantity;
	private String gorssPrice;
	private String entryShippingDate;
	private String entryDiscount;
	private String yeleDimension;



	/**
	 * @return the yeleDimension
	 */
	public String getYeleDimension()
	{
		return yeleDimension;
	}

	/**
	 * @param yeleDimension
	 *           the yeleDimension to set
	 */
	public void setYeleDimension(final String yeleDimension)
	{
		this.yeleDimension = yeleDimension;
	}

	/**
	 * @return the productCode
	 */
	public String getProductCode()
	{
		return productCode;
	}

	/**
	 * @param productCode
	 *           the productCode to set
	 */
	public void setProductCode(final String productCode)
	{
		this.productCode = productCode;
	}

	/**
	 * @return the prductName
	 */
	public String getProductName()
	{
		return productName;
	}

	/**
	 * @param prductName
	 *           the prductName to set
	 */
	public void setProductName(final String productName)
	{
		this.productName = productName;
	}

	/**
	 * @return the unitOfMeasure
	 */
	public String getUnitOfMeasure()
	{
		return unitOfMeasure;
	}

	/**
	 * @param unitOfMeasure
	 *           the unitOfMeasure to set
	 */
	public void setUnitOfMeasure(final String unitOfMeasure)
	{
		this.unitOfMeasure = unitOfMeasure;
	}

	/**
	 * @return the quantity
	 */
	public String getQuantity()
	{
		return quantity;
	}

	/**
	 * @param quantity
	 *           the quantity to set
	 */
	public void setQuantity(final String quantity)
	{
		this.quantity = quantity;
	}

	/**
	 * @return the gorssPrice
	 */
	public String getGorssPrice()
	{
		return gorssPrice;
	}

	/**
	 * @param gorssPrice
	 *           the gorssPrice to set
	 */
	public void setGorssPrice(final String gorssPrice)
	{
		this.gorssPrice = gorssPrice;
	}

	/**
	 * @return the entryShippingDate
	 */
	public String getEntryShippingDate()
	{
		return entryShippingDate;
	}

	/**
	 * @param entryShippingDate
	 *           the entryShippingDate to set
	 */
	public void setEntryShippingDate(final String entryShippingDate)
	{
		this.entryShippingDate = entryShippingDate;
	}

	/**
	 * @return the discount
	 */
	public String getEntryDiscount()
	{
		return entryDiscount;
	}

	/**
	 * @param discount
	 *           the discount to set
	 */
	public void setEntryDiscount(final String discount)
	{
		this.entryDiscount = discount;
	}

	@Override
	public String toString()
	{
		return "{\n\tproductCode:" + productCode + ",\n\t productName:" + productName + ",\n\t unitOfMeasure:" + unitOfMeasure
				+ ",\n\t quantity:" + quantity + ",\n\t gorssPrice:" + gorssPrice + ",\n\t entryShippingDate:" + entryShippingDate
				+ ",\n\t entryDiscount:" + entryDiscount + "\n}";
	}

}
