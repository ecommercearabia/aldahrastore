package com.aldahra.aldahraerpclientservices.erp.impl.entries;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author monzer
 */
@XmlRootElement(name = "PARAM")
public class PurchaseRequestParameters
{

	private List<Field> fields;
	private List<Tab> tabs;

	@XmlElement(name = "FLD", required = true)
	public List<Field> getFields()
	{
		return fields;
	}

	public void setFields(final List<Field> fields)
	{
		this.fields = fields;
	}

	@XmlElement(name = "TAB", required = true)
	public List<Tab> getTabs()
	{
		return tabs;
	}

	public void setTabs(final List<Tab> tabs)
	{
		this.tabs = tabs;
	}

	@Override
	public String toString()
	{
		return "{\n\tfields:" + fields + ",\n\t tabs:" + tabs + "\n}";
	}


}
