package com.aldahra.aldahraerpclientservices.erp.impl.entries;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author monzer
 */
@XmlRootElement
public class Tab implements Serializable{

	private List<Line> lines;
	private String dim;
	private String id;
	private String size;

	public List<Line> getLines() {
		return lines;
	}

	@XmlElement(name = "LIN", required = true)
	public void setLines(final List<Line> lines) {
		this.lines = lines;
	}

	@XmlAttribute(name = "DIM", required = true)
	public String getDim() {
		return dim;
	}

	public void setDim(final String dim) {
		this.dim = dim;
	}

	@XmlAttribute(name = "ID")
	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	@XmlAttribute(name = "SIZE")
	public String getSize() {
		return size;
	}

	public void setSize(final String size) {
		this.size = size;
	}

	@Override
	public String toString()
	{
		return "{\n\tlines:" + lines + ",\n\t dim:" + dim + ",\n\t id:" + id + ",\n\t size:" + size + "\n}";
	}

}
