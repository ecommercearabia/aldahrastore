/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.erp.impl.entries;

import de.hybris.platform.core.model.product.ProductModel;

import com.aldahra.aldahraerpclientwebservices.model.AldahraERPProductPurchaseRequestServiceProviderModel;



/**
 * @author monzer
 */
public class PurchaseRequestProviderContainer
{

	private ProductModel product;
	private AldahraERPProductPurchaseRequestServiceProviderModel provider;
	
	/**
	 * @return the product
	 */
	public ProductModel getProduct()
	{
		return product;
	}
	
	/**
	 * @param product
	 *           the product to set
	 */
	public void setProduct(final ProductModel product)
	{
		this.product = product;
	}
	
	/**
	 * @return the provider
	 */
	public AldahraERPProductPurchaseRequestServiceProviderModel getProvider()
	{
		return provider;
	}

	/**
	 * @param provider
	 *           the provider to set
	 */
	public void setProvider(final AldahraERPProductPurchaseRequestServiceProviderModel provider)
	{
		this.provider = provider;
	}

}
