/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.erp.impl.entries;

import de.hybris.platform.core.model.order.OrderModel;

import com.aldahra.aldahraerpclientwebservices.model.AldahraERPSalesOrderServiceProviderModel;



/**
 * @author monzer
 */
public class SalesOrderProviderContainer
{

	private OrderModel order;
	private AldahraERPSalesOrderServiceProviderModel provider;

	/**
	 * @return the order
	 */
	public OrderModel getOrder()
	{
		return order;
	}

	/**
	 * @param order
	 *           the order to set
	 */
	public void setOrder(final OrderModel order)
	{
		this.order = order;
	}

	/**
	 * @return the provider
	 */
	public AldahraERPSalesOrderServiceProviderModel getProvider()
	{
		return provider;
	}

	/**
	 * @param provider
	 *           the provider to set
	 */
	public void setProvider(final AldahraERPSalesOrderServiceProviderModel provider)
	{
		this.provider = provider;
	}



}
