/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.erp.impl.entries;

/**
 * @author mohammad-abumuhasien
 */
public class StockLineEntryRequest
{
	private String product;
	private String description;
	private String receivingSite;
	private String purchaseUnit;
	private String quantity;
	private String supplier;
	private String receivingDate;
	private String grossPrice;
	private String discount1;
	private String discount2;
	private String returnflag;
	private String returnMessage;


	public StockLineEntryRequest()
	{

	}

	public String getProduct()
	{
		return product;
	}

	public void setProduct(final String product)
	{
		this.product = product;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(final String description)
	{
		this.description = description;
	}

	public String getReceivingSite()
	{
		return receivingSite;
	}

	public void setReceivingSite(final String receivingSite)
	{
		this.receivingSite = receivingSite;
	}

	public String getPurchaseUnit()
	{
		return purchaseUnit;
	}

	public void setPurchaseUnit(final String purchaseUnit)
	{
		this.purchaseUnit = purchaseUnit;
	}

	public String getQuantity()
	{
		return quantity;
	}

	public void setQuantity(final String quantity)
	{
		this.quantity = quantity;
	}

	public String getSupplier()
	{
		return supplier;
	}

	public void setSupplier(final String supplier)
	{
		this.supplier = supplier;
	}

	public String getReceivingDate()
	{
		return receivingDate;
	}

	public void setReceivingDate(final String receivingDate)
	{
		this.receivingDate = receivingDate;
	}

	public String getGrossPrice()
	{
		return grossPrice;
	}

	public void setGrossPrice(final String grossPrice)
	{
		this.grossPrice = grossPrice;
	}

	public String getDiscount1()
	{
		return discount1;
	}

	public void setDiscount1(final String discount1)
	{
		this.discount1 = discount1;
	}

	public String getDiscount2()
	{
		return discount2;
	}

	public void setDiscount2(final String discount2)
	{
		this.discount2 = discount2;
	}

	public String getReturnflag()
	{
		return returnflag;
	}

	public void setReturnflag(final String returnflag)
	{
		this.returnflag = returnflag;
	}

	public String getReturnMessage()
	{
		return returnMessage;
	}

	public void setReturnMessage(final String returnMessage)
	{
		this.returnMessage = returnMessage;
	}

	@Override
	public String toString()
	{
		return "{\n\tproduct:" + product + ",\n\t description:" + description + ",\n\t receivingSite:" + receivingSite
				+ ",\n\t purchaseUnit:" + purchaseUnit + ",\n\t quantity:" + quantity + ",\n\t supplier:" + supplier
				+ ",\n\t receivingDate:" + receivingDate + ",\n\t grossPrice:" + grossPrice + ",\n\t discount1:" + discount1
				+ ",\n\t discount2:" + discount2 + ",\n\t returnflag:" + returnflag + ",\n\t returnMessage:" + returnMessage + "\n}";
	}

}
