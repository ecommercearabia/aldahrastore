package com.aldahra.aldahraerpclientservices.erp.impl.entries;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;


/**
 * @author monzer
 */
@XmlRootElement(name = "FLD")
public class Field implements Serializable
{

	private String name;
	private String type;
	private String text;

	public Field(final String name, final String text)
	{
		super();
		this.name = name;
		this.text = text;
	}

	public Field(final String name, final String type, final String text)
	{
		super();
		this.name = name;
		this.type = type;
		this.text = text;
	}

	public Field(final ERPWebServiceAttributes attrib, final String text)
	{
		super();
		this.name = attrib.getAttributeName();
		this.type = attrib.getAttributeType();
		this.text = text;
	}

	public Field()
	{
	}

	@XmlAttribute(name = "NAME")
	public String getName()
	{
		return name;
	}

	public void setName(final String name)
	{
		this.name = name;
	}

	@XmlAttribute(name = "TYPE")
	public String getType()
	{
		return type;
	}

	public void setType(final String type)
	{
		this.type = type;
	}

	@XmlValue
	public String getText()
	{
		return text;
	}

	public void setText(final String text)
	{
		this.text = text;
	}

	@Override
	public String toString()
	{
		return "{\n\tname:" + name + ",\n\t type:" + type + ",\n\t text:" + text + "\n}";
	}

}
