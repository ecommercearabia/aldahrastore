package com.aldahra.aldahraerpclientservices.erp.impl.entries;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author monzer
 */
@XmlRootElement(name = "LIN")
public class Line implements Serializable{

	private List<Field> fields;
	private String number;

	public List<Field> getFields() {
		return fields;
	}
	@XmlElement(name = "FLD")
	public void setFields(final List<Field> fields) {
		this.fields = fields;
	}

	public String getNumber() {
		return number;
	}
	@XmlAttribute(name = "NUM")
	public void setNumber(final String number) {
		this.number = number;
	}
	@Override
	public String toString()
	{
		return "{\n\tfields:" + fields + ",\n\t number:" + number + "\n}";
	}

}
