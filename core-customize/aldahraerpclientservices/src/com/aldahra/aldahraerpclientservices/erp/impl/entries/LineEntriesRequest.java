/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.erp.impl.entries;

import java.util.List;


/**
 *
 */
public class LineEntriesRequest
{
	private List<LineEntryRequest> entries;

	/**
	 * @return the entries
	 */
	public List<LineEntryRequest> getEntries()
	{
		return entries;
	}

	/**
	 * @param entries
	 *           the entries to set
	 */
	public void setEntries(final List<LineEntryRequest> entries)
	{
		this.entries = entries;
	}

}