/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.erp.impl;

import java.io.StringReader;
import java.io.StringWriter;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.rpc.ServiceException;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.xml.sax.InputSource;

import com.aldahra.aldahraerpclientservices.erp.ERPWSSalesOrderService;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.ERPWebServiceAttributes;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.Field;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.Group;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.Line;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.LineEntryRequest;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.SalesOrderParameters;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.SalesOrderRequest;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.SalesOrderServiceResponse;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.Tab;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxCallContext;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxMessage;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxResultXml;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxWebServiceXmlCCService;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxWebServiceXmlCCServiceLocator;
import com.aldahra.aldahraerpclientservices.erp.response.ResponseMessage;
import com.aldahra.aldahraerpclientservices.erp.response.ResponseMetadata;
import com.aldahra.aldahraerpclientservices.erp.response.SalesOrderResponse;
import com.aldahra.aldahraerpclientservices.erp.response.enums.MessageType;
import com.aldahra.aldahraerpclientservices.erp.response.enums.Status;
import com.aldahra.aldahraerpclientservices.exception.ERPWSExceptionType;
import com.aldahra.aldahraerpclientservices.exception.ERPWSServiceException;
import com.google.common.base.Preconditions;


/**
 * @author monzer
 */
public class DefaultERPWSSalesOrderService implements ERPWSSalesOrderService
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultERPWSSalesOrderService.class);

	@Override
	public SalesOrderResponse createSalesOrder(final SalesOrderRequest order, final String username, final String password,
			final String poolName, final String schemaName, final String orderType) throws ERPWSServiceException
	{
		Preconditions.checkArgument(order != null, "Cannot proceed without an order");
		Preconditions.checkArgument(StringUtils.isNotBlank(username), "Cannot proceed without the service username");
		Preconditions.checkArgument(StringUtils.isNotBlank(password), "Cannot proceed without the service password");
		Preconditions.checkArgument(StringUtils.isNotBlank(poolName), "Cannot proceed without the service Pool name");
		Preconditions.checkArgument(StringUtils.isNotBlank(schemaName), "Cannot proceed without the service schemaName");

		if (!validateSalesOrderRequestData(order))
		{
			LOG.error("Invalid request, Some Request parameters are null!: {}", order.toString());
			throw new ERPWSServiceException(ERPWSExceptionType.INVALID_REQUEST,
					"Invalid request, Some Request parameters are null!");
		}

		final CAdxWebServiceXmlCCService service = new CAdxWebServiceXmlCCServiceLocator();
		final CAdxCallContext call = new CAdxCallContext();
		call.setCodeUser(username);
		call.setPassword(password);
		call.setPoolAlias(poolName);
		call.setPoolId(poolName);
		call.setCodeLang("EN");

		final SalesOrderParameters params = buildSalesOrderRequest(order, orderType);

		CAdxResultXml result = null;
		try
		{
			final String request = marshallSalesOrderParameters(params);
			result = service.getCAdxWebServiceXmlCC().run(call, schemaName, request);
		}
		catch (RemoteException | ServiceException e)
		{
			LOG.error("The service is not reachable or is down");
			LOG.error(e.getMessage());
			throw new ERPWSServiceException(ERPWSExceptionType.SERVER_ERROR, "The service is not reachable or is down: ");
		}
		catch (final JAXBException e)
		{
			LOG.error("Could not marshall the request {} into xml", params);
			LOG.error(e.getMessage());
			throw new ERPWSServiceException(ERPWSExceptionType.MARSHALLING_ERROR,
					"Could not marshall the request " + params.toString());
		}

		if (result == null)
		{
			throw new ERPWSServiceException(ERPWSExceptionType.INVALID_RESPONSE, "The result is null");
		}

		SalesOrderServiceResponse salesOrderResponse = null;

		LOG.info("Web service response code: {}", result.getStatus());

		for (final CAdxMessage message : result.getMessages())
		{
			if (StringUtils.isNotBlank(message.getMessage()))
			{
				LOG.info("Message in response: type={}, message={}", message.getType(), message.getMessage());
			}
		}
		if (result.getStatus() == 1)
		{ // status => 1: success, 0: error
			try
			{
				salesOrderResponse = unmarshallSalesOrderParams(result.getResultXml());
			}
			catch (final JAXBException e)
			{
				throw new ERPWSServiceException(ERPWSExceptionType.UNMARSHALLING_ERROR,
						"Could not unmarshall the response " + result.getResultXml());
			}
		}


		return buildSalesOrderResponse(salesOrderResponse, result);
	}

	/**
	*
	*/
	private SalesOrderResponse buildSalesOrderResponse(final SalesOrderServiceResponse response, final CAdxResultXml result)
	{
		final SalesOrderResponse salesOrderResponse = new SalesOrderResponse();
		final ResponseMessage responseMessage = new ResponseMessage();
		final List<ResponseMetadata> metadata = new ArrayList<>();
		boolean success = true;

		for (final CAdxMessage message : result.getMessages())
		{
			// Some messages retrieved in the response are blank or spaces
			if (StringUtils.isNotBlank(message.getMessage()))
			{
				// Message types => 1: success, 2:warning, 3:error
				final ResponseMetadata data = new ResponseMetadata();
				data.setMessage("Message in response: type=" + message.getType() + ", message=" + message.getMessage());
				data.setType(MessageType.getMessageByCode(message.getType()));
				if (MessageType.ERROR.equals(data.getType()))
				{
					success = false;
				}
				metadata.add(data);
			}
		}
		salesOrderResponse.setCreationSuccess(success && salesOrderCreatedSuccessfully(response));
		salesOrderResponse.setSalesOrderResponse(extractCreationResponse(response));
		responseMessage.setMessage(metadata);
		responseMessage.setResponse(extractCreationResponse(response));
		responseMessage.setStatus(salesOrderResponse.getCreationSuccess() ? Status.SUCCESS : Status.FAILURE);
		salesOrderResponse.setResponse(responseMessage);

		return salesOrderResponse;
	}


	/**
	*
	*/
	private String extractCreationResponse(final SalesOrderServiceResponse response)
	{
		if (response == null)
		{
			return "NO RESPONSE";
		}
		final Optional<Group> resultTab = response.getGroups().stream()
				.filter(group -> ERPWebServiceAttributes.RESULT_GROUP.getAttributeName().equals(group.getId())).findFirst();
		if (resultTab.isEmpty())
		{
			LOG.error("Sales Order creation failed!");
			return "FAILED DUE TO MISSING RESULT GROUP GRP3";
		}

		final Optional<Field> createdSuccessfully = resultTab.get().getFields().stream()
				.filter(field -> ERPWebServiceAttributes.RETURN_FLAG.getAttributeName().equals(field.getName())).findFirst();
		final Optional<Field> creationMessage = resultTab.get().getFields().stream()
				.filter(field -> ERPWebServiceAttributes.MESSAGE.getAttributeName().equals(field.getName())).findFirst();
		final String message = creationMessage.isEmpty() ? "FAILED" : creationMessage.get().getText();
		final String status = createdSuccessfully.isEmpty() ? "0" : createdSuccessfully.get().getText();
		return "Status: " + status + ", message: " + message;
	}

	private boolean salesOrderCreatedSuccessfully(final SalesOrderServiceResponse response)
	{
		if (response == null)
		{
			return false;
		}
		final Optional<Group> resultTab = response.getGroups().stream()
				.filter(group -> ERPWebServiceAttributes.RESULT_GROUP.getAttributeName().equals(group.getId())).findFirst();
		if (resultTab.isEmpty())
		{
			LOG.error("Sales Order creation failed!");
			return false;
		}

		final Optional<Field> createdSuccessfully = resultTab.get().getFields().stream()
				.filter(field -> ERPWebServiceAttributes.RETURN_FLAG.getAttributeName().equals(field.getName())).findFirst();

		// if the createdSuccessfully attribute == 1 then the sales order creation is failed. it has to be 2
		return !createdSuccessfully.isEmpty() || "1".equals(createdSuccessfully.get().getText());
	}

	/**
	*
	*/
	private boolean validateSalesOrderRequestData(final SalesOrderRequest order)
	{
		boolean result = true;
		if (StringUtils.isBlank(order.getSalesOrderSite()))
		{
			LOG.error("SalesOrderSite is blank");
			result = false;
		}
		if (StringUtils.isBlank(order.getOrderCode()))
		{
			LOG.error("OrderCode is blank");
			result = false;
		}
		if (StringUtils.isBlank(order.getOrderType()))
		{
			LOG.error("OrderType is blank");
			result = false;
		}
		if (StringUtils.isBlank(order.getOrderCreationDate()))
		{
			LOG.error("OrderCreationDate is blank");
			result = false;
		}
		if (StringUtils.isBlank(order.getCurrency()))
		{
			LOG.error("Currency is blank");
			result = false;
		}
		if (StringUtils.isBlank(order.getCustomer()))
		{
			LOG.error("Customer is blank");
			result = false;
		}
		if (StringUtils.isBlank(order.getPaymentTerms()))
		{
			LOG.error("PaymentTerms is blank");
			result = false;
		}
		if (StringUtils.isBlank(order.getShippingDate()))
		{
			LOG.error("ShippingDate is blank");
			result = false;
		}
		if (StringUtils.isBlank(order.getShippingSite()))
		{
			LOG.error("ShippingSite is blank");
			result = false;
		}
		return result;
	}

	/**
	*
	*/
	private SalesOrderParameters buildSalesOrderRequest(final SalesOrderRequest order, final String orderType)
	{
		final Field siteField = new Field(ERPWebServiceAttributes.ORDER_SITE.getAttributeName(), order.getSalesOrderSite());
		final Field orderTypeField = new Field(ERPWebServiceAttributes.ORDER_TYPE.getAttributeName(), orderType);
		final Field customerField = new Field(ERPWebServiceAttributes.CUSTOMER.getAttributeName(), order.getCustomer());
		final Field creationDateField = new Field(ERPWebServiceAttributes.ORDER_DATE.getAttributeName(),
				order.getOrderCreationDate());
		final Field orderCodeField = new Field(ERPWebServiceAttributes.ORDER_REFERENCE.getAttributeName(), order.getOrderCode());
		final Field shipSiteField = new Field(ERPWebServiceAttributes.SHIP_SITE.getAttributeName(), order.getShippingSite());
		final Field currencyField = new Field(ERPWebServiceAttributes.CURRENCY.getAttributeName(), order.getCurrency());
		final Field shipDateField = new Field(ERPWebServiceAttributes.SHIP_DATE.getAttributeName(), order.getShippingDate());
		final Field paymentModField = new Field(ERPWebServiceAttributes.PAYMENT_TERMS.getAttributeName(), order.getPaymentTerms());
		final Field orderDiscountField = new Field(ERPWebServiceAttributes.ORDER_DISCOUNT.getAttributeName(),
				order.getOrderDiscount());
		final Field timeslotInfoField = new Field(ERPWebServiceAttributes.TIMESLOT_START_TIME.getAttributeName(),
				order.getTimeslotInfo());
		final Field orderTimestampField = new Field(ERPWebServiceAttributes.ORDER_CREATION_TIMESTAMP.getAttributeName(),
				order.getOrderCreationTimestamp());
		final Field orderShipmentType = new Field(ERPWebServiceAttributes.ORDER_SHIPMENT_TYPE.getAttributeName(),
				order.getOrderShipmentType());
		final SalesOrderParameters request = new SalesOrderParameters();

		request.setFields(Arrays.asList(siteField, orderTypeField, customerField, creationDateField, orderCodeField, shipSiteField,
				currencyField, shipDateField, paymentModField, orderDiscountField, timeslotInfoField, orderTimestampField,
				orderShipmentType));
		request.setTabs(buildSalesOrderRequestEntries(order.getEntryLines()));

		return request;
	}


	private void addFieldToList(final List<Field> list, final Field field)
	{
		list.add(field);
	}

	/**
	 * @return
	 *
	 */
	private List<Tab> buildSalesOrderRequestEntries(final List<LineEntryRequest> entryLines)
	{
		if (CollectionUtils.isEmpty(entryLines))
		{
			return Collections.emptyList();
		}
		final List<Line> lines = new ArrayList<>();
		final Tab entryTab = new Tab();
		int number = 0;
		for (final LineEntryRequest entry : entryLines)
		{
			final List<Field> asList = new ArrayList<>(8);
			addFieldToList(asList, new Field(ERPWebServiceAttributes.PRODUCT, entry.getProductCode()));
			addFieldToList(asList, new Field(ERPWebServiceAttributes.DESCRIPTION, entry.getProductName()));
			addFieldToList(asList, new Field(ERPWebServiceAttributes.SALES_UNIT, entry.getUnitOfMeasure()));
			addFieldToList(asList, new Field(ERPWebServiceAttributes.QUANTITY, entry.getQuantity()));
			addFieldToList(asList, new Field(ERPWebServiceAttributes.GROSS_PRICE, entry.getGorssPrice()));
			addFieldToList(asList, new Field(ERPWebServiceAttributes.SHIP_DATE_LINE, entry.getEntryShippingDate()));
			addFieldToList(asList, new Field(ERPWebServiceAttributes.ENTRY_DISCOUNT, entry.getEntryDiscount()));

			if (Strings.isNotBlank(entry.getYeleDimension()))
			{
				addFieldToList(asList, new Field(ERPWebServiceAttributes.YELE_DIMENSION, entry.getYeleDimension()));
			}

			final Line entryLine = new Line();
			entryLine.setNumber(String.valueOf(++number));

			entryLine.setFields(asList);
			lines.add(entryLine);
		}
		entryTab.setLines(lines);
		entryTab.setDim("100");
		entryTab.setId("GRP2");
		entryTab.setSize(String.valueOf(entryLines.size()));
		return Arrays.asList(entryTab);
	}

	protected String marshallSalesOrderParameters(final SalesOrderParameters param) throws JAXBException
	{
		final JAXBContext context = JAXBContext.newInstance(SalesOrderParameters.class);
		final Marshaller marshaller = context.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		final StringWriter writer = new StringWriter();
		marshaller.marshal(param, writer);
		LOG.info("Sales Order Request: " + writer.toString());
		return writer.toString();
	}

	protected SalesOrderServiceResponse unmarshallSalesOrderParams(final String response) throws JAXBException
	{
		final JAXBContext context = JAXBContext.newInstance(SalesOrderServiceResponse.class);
		final Unmarshaller unmarshaller = context.createUnmarshaller();
		final InputSource input = new InputSource();
		input.setCharacterStream(new StringReader(response));
		return (SalesOrderServiceResponse) unmarshaller.unmarshal(input);
	}

}
