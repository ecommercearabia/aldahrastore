/**
 * CAdxWebServiceXmlCCService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices;

public interface CAdxWebServiceXmlCCService extends javax.xml.rpc.Service {
    public java.lang.String getCAdxWebServiceXmlCCAddress();

	 public com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxWebServiceXmlCC getCAdxWebServiceXmlCC()
			 throws javax.xml.rpc.ServiceException;

	 public com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxWebServiceXmlCC getCAdxWebServiceXmlCC(
			 java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
