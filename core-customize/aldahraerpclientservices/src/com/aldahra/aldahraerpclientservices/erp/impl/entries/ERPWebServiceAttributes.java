/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.erp.impl.entries;

/**
 * @author monzer
 */

class Constants
{
	static final String INTEGER = "Integer";
	static final String DATE_TYPE = "Date";
	static final String CHAR = "Char";
	static final String DECIMAL = "Decimal";
	public static final String CLOB = null;

	private Constants()
	{
	}
}

public enum ERPWebServiceAttributes
{

	// @formatter:off

	STOCK_SITE("YSTOFCY", Constants.CHAR),
	PRODUCT_CODE("YITMREF", Constants.CHAR),
	QUANTITY_AVAILABEL("YAQTY", Constants.DECIMAL),
	ORDER_SITE("YSALFCY", Constants.CHAR),
	ORDER_TYPE("YSOHTYP", Constants.CHAR),
	ORDER_NUMBER("YSOHNUM", Constants.CHAR),
   CUSTOMER("YBPCORD", Constants.CHAR),
   ORDER_DATE("YORDDAT", Constants.DATE_TYPE),
   ORDER_REFERENCE("YCUSORDREF", Constants.CHAR),
   SHIP_SITE("YSTOFCY", Constants.CHAR),
   CURRENCY("YCUR", Constants.CHAR),
   TM_REFERENCE("YTMREF", Constants.CHAR),
   BOKKING_ID("YBOOKING", Constants.CHAR),
   TRANSACTION("YGFLAG",Constants.CHAR),
   DOCUMENT_TYPE("YTIPDOC", Constants.CHAR),
   YEAR("YEXERCICI", Constants.CHAR),
   ADDRESS("YBPAADD",Constants.CHAR),
   DELIVERT_ITEMS("YDLVTRM", Constants.CHAR),
   COUNTRY("YCRY", Constants.CHAR),
   SHIP_DATE("YDEMDLVDAT",Constants.CHAR),
   PAYMENT_TERMS("YPTE", Constants.CHAR),
   PRODUCT("YITMREF", Constants.CHAR),
   DESCRIPTION("YITMDES",Constants.CHAR),
   SALES_UNIT("YSAU", Constants.CHAR),
   QUANTITY("YQTY", Constants.DECIMAL),
   GROSS_PRICE("YGROPRI", Constants.DECIMAL),
   SHIP_DATE_LINE("YDSHIDAT", Constants.DATE_TYPE),
   ORDER_STATUS("YSOQSTA", Constants.INTEGER),
   CLOSE_REASON("YDCCLREN", Constants.CHAR),
   LINE_TEXT("YLINTXT", Constants.CLOB),
   RETURN_FLAG("YRTNFLG", Constants.CHAR),
   MESSAGE("YMSG",Constants.CHAR),
   REQUEST_GROUP("GRP1", ""),
   RESPONSE_GROUP("GRP2",""),
   RESULT_GROUP("GRP3", ""),
   ENTRY_DISCOUNT("YDISC",Constants.DECIMAL),
   ORDER_DISCOUNT("YCDISC",Constants.DECIMAL),
   SITE("YPSHFCY", Constants.CHAR),
   DATE("YPRQDAT",Constants.DATE_TYPE),
   REQUEST_USER("YREQUSR",Constants.CHAR),
   REQUEST_NUMBER("YPSHNUM",Constants.CHAR),
   RECEIVING_SITE("YPRHFCY",Constants.CHAR),
   PURCHASE_UNIT("YPUU", Constants.CHAR),
   STOCK_QUANTITY("YQTYPUU", Constants.DECIMAL),
   SUPPLIER("YBPSNUM", Constants.CHAR),
   RECEIVING_DATE("YEXPRCPDAT", Constants.DATE_TYPE),
   DISCOUNT_1("YDISC1", Constants.DECIMAL),
   DISCOUNT_2("YDISC2",Constants.DECIMAL),
   RETURN_MESSAGE("YMSG", Constants.CHAR),
   BIN_NUMBER("YLOC", Constants.CHAR),
   EXPIRATION_DATE("YEXPDAT", Constants.DATE_TYPE),
   LOT_NUMBER("YLOT", Constants.CHAR),
   SITE_STOCK_QUANTITY("YQTYSTU", Constants.DECIMAL),
   TIMESLOT_START_TIME("YECTS", Constants.CHAR),
	ORDER_CREATION_TIMESTAMP("YHYBTIM", Constants.CHAR),
	ORDER_SHIPMENT_TYPE("YHYBTYP", Constants.CHAR),
	YELE_DIMENSION("YELE", Constants.CHAR);
// @formatter:on

	private String attributeName;
	private String attributeType;

	private ERPWebServiceAttributes(final String attributeName, final String attributeType)
	{
		this.attributeName = attributeName;
		this.attributeType = attributeType;
	}

	/**
	 * @return the attributeName
	 */
	public String getAttributeName()
	{
		return attributeName;
	}

	/**
	 * @return the attributeType
	 */
	public String getAttributeType()
	{
		return attributeType;
	}

	public static ERPWebServiceAttributes getEnumByAttributeName(final String name)
	{
		for (final ERPWebServiceAttributes b : ERPWebServiceAttributes.values())
		{
			if (b.attributeName.equalsIgnoreCase(name))
			{
				return b;
			}
		}
		return null;
	}
}