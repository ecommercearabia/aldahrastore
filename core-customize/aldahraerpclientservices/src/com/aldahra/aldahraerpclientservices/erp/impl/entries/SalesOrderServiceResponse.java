package com.aldahra.aldahraerpclientservices.erp.impl.entries;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author monzer
 */
@XmlRootElement(name="RESULT")
public class SalesOrderServiceResponse {

	private List<Group> groups;
	private List<Tab> tabs;

	@XmlElement(name="GRP")
	public List<Group> getGroups() {
		return groups;
	}
	public void setGroups(final List<Group> groups) {
		this.groups = groups;
	}
	@XmlElement(name="TAB")
	public List<Tab> getTabs() {
		return tabs;
	}
	public void setTabs(final List<Tab> tabs) {
		this.tabs = tabs;
	}

	@Override
	public String toString()
	{
		return "{\n\tgroups:" + groups + ",\n\t tabs:" + tabs + "\n}";
	}

}
