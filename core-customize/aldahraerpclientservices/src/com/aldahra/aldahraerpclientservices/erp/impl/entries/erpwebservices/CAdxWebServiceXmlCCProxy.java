package com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices;

public class CAdxWebServiceXmlCCProxy implements com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxWebServiceXmlCC
{
  private String _endpoint = null;
	private com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxWebServiceXmlCC cAdxWebServiceXmlCC = null;

  public CAdxWebServiceXmlCCProxy() {
    _initCAdxWebServiceXmlCCProxy();
  }

  public CAdxWebServiceXmlCCProxy(final String endpoint) {
    _endpoint = endpoint;
    _initCAdxWebServiceXmlCCProxy();
  }

  private void _initCAdxWebServiceXmlCCProxy() {
    try {
		 cAdxWebServiceXmlCC = (new com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxWebServiceXmlCCServiceLocator())
				 .getCAdxWebServiceXmlCC();
      if (cAdxWebServiceXmlCC != null) {
        if (_endpoint != null)
		{
			((javax.xml.rpc.Stub)cAdxWebServiceXmlCC)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
		}
		else
		{
			_endpoint = (String)((javax.xml.rpc.Stub)cAdxWebServiceXmlCC)._getProperty("javax.xml.rpc.service.endpoint.address");
		}
      }

    }
    catch (final javax.xml.rpc.ServiceException serviceException) {}
  }

  public String getEndpoint() {
    return _endpoint;
  }

  public void setEndpoint(final String endpoint) {
    _endpoint = endpoint;
    if (cAdxWebServiceXmlCC != null)
	{
		((javax.xml.rpc.Stub)cAdxWebServiceXmlCC)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
	}

  }

  public com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxWebServiceXmlCC getCAdxWebServiceXmlCC()
  {
    if (cAdxWebServiceXmlCC == null)
	{
		_initCAdxWebServiceXmlCCProxy();
	}
    return cAdxWebServiceXmlCC;
  }

  public com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxResultXml run(
		  final com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxCallContext callContext, final java.lang.String publicName,
		  final java.lang.String inputXml) throws java.rmi.RemoteException
  {
    if (cAdxWebServiceXmlCC == null)
	{
		_initCAdxWebServiceXmlCCProxy();
	}
    return cAdxWebServiceXmlCC.run(callContext, publicName, inputXml);
  }

  public com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxResultXml save(
		  final com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxCallContext callContext, final java.lang.String publicName,
		  final java.lang.String objectXml) throws java.rmi.RemoteException
  {
    if (cAdxWebServiceXmlCC == null)
	{
		_initCAdxWebServiceXmlCCProxy();
	}
    return cAdxWebServiceXmlCC.save(callContext, publicName, objectXml);
  }

  public com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxResultXml delete(
		  final com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxCallContext callContext, final java.lang.String publicName,
		  final com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxParamKeyValue[] objectKeys) throws java.rmi.RemoteException
  {
    if (cAdxWebServiceXmlCC == null)
	{
		_initCAdxWebServiceXmlCCProxy();
	}
    return cAdxWebServiceXmlCC.delete(callContext, publicName, objectKeys);
  }

  public com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxResultXml read(
		  final com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxCallContext callContext, final java.lang.String publicName,
		  final com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxParamKeyValue[] objectKeys) throws java.rmi.RemoteException
  {
    if (cAdxWebServiceXmlCC == null)
	{
		_initCAdxWebServiceXmlCCProxy();
	}
    return cAdxWebServiceXmlCC.read(callContext, publicName, objectKeys);
  }

  public com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxResultXml query(
		  final com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxCallContext callContext, final java.lang.String publicName,
		  final com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxParamKeyValue[] objectKeys, final int listSize)
		  throws java.rmi.RemoteException
  {
    if (cAdxWebServiceXmlCC == null)
	{
		_initCAdxWebServiceXmlCCProxy();
	}
    return cAdxWebServiceXmlCC.query(callContext, publicName, objectKeys, listSize);
  }

  public com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxResultXml getDescription(
		  final com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxCallContext callContext, final java.lang.String publicName)
		  throws java.rmi.RemoteException
  {
    if (cAdxWebServiceXmlCC == null)
	{
		_initCAdxWebServiceXmlCCProxy();
	}
    return cAdxWebServiceXmlCC.getDescription(callContext, publicName);
  }

  public com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxResultXml actionObject(
		  final com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxCallContext callContext, final java.lang.String publicName,
		  final java.lang.String actionCode, final java.lang.String objectXml) throws java.rmi.RemoteException
  {
    if (cAdxWebServiceXmlCC == null)
	{
		_initCAdxWebServiceXmlCCProxy();
	}
    return cAdxWebServiceXmlCC.actionObject(callContext, publicName, actionCode, objectXml);
  }

  public com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxResultXml getDataXmlSchema(
		  final com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxCallContext callContext, final java.lang.String publicName)
		  throws java.rmi.RemoteException
  {
    if (cAdxWebServiceXmlCC == null)
	{
		_initCAdxWebServiceXmlCCProxy();
	}
    return cAdxWebServiceXmlCC.getDataXmlSchema(callContext, publicName);
  }

  public com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxResultXml actionObjectKeys(
		  final com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxCallContext callContext, final java.lang.String publicName,
		  final java.lang.String actionCode,
		  final com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxParamKeyValue[] objectKeys) throws java.rmi.RemoteException
  {
    if (cAdxWebServiceXmlCC == null)
	{
		_initCAdxWebServiceXmlCCProxy();
	}
    return cAdxWebServiceXmlCC.actionObjectKeys(callContext, publicName, actionCode, objectKeys);
  }

  public com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxResultXml insertLines(
		  final com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxCallContext callContext, final java.lang.String publicName,
		  final com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxParamKeyValue[] objectKeys, final java.lang.String blocKey,
		  final java.lang.String lineKey, final java.lang.String lineXml) throws java.rmi.RemoteException
  {
    if (cAdxWebServiceXmlCC == null)
	{
		_initCAdxWebServiceXmlCCProxy();
	}
    return cAdxWebServiceXmlCC.insertLines(callContext, publicName, objectKeys, blocKey, lineKey, lineXml);
  }

  public com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxResultXml deleteLines(
		  final com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxCallContext callContext, final java.lang.String publicName,
		  final com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxParamKeyValue[] objectKeys, final java.lang.String blocKey,
		  final java.lang.String[] lineKeys) throws java.rmi.RemoteException
  {
    if (cAdxWebServiceXmlCC == null)
	{
		_initCAdxWebServiceXmlCCProxy();
	}
    return cAdxWebServiceXmlCC.deleteLines(callContext, publicName, objectKeys, blocKey, lineKeys);
  }

  public com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxResultXml modify(
		  final com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxCallContext callContext, final java.lang.String publicName,
		  final com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxParamKeyValue[] objectKeys,
		  final java.lang.String objectXml) throws java.rmi.RemoteException
  {
    if (cAdxWebServiceXmlCC == null)
	{
		_initCAdxWebServiceXmlCCProxy();
	}
    return cAdxWebServiceXmlCC.modify(callContext, publicName, objectKeys, objectXml);
  }


}