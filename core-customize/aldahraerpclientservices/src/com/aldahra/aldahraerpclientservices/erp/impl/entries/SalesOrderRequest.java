/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.erp.impl.entries;

import java.util.List;


/**
 * @author monzer
 */
public class SalesOrderRequest
{
	private String salesOrderSite;
	private String orderType;
	private String orderNumber;
	private String customer;
	private String orderCreationDate;
	private String orderCode;
	private String shippingSite;
	private String currency;
	private String tmReference;
	private String bookingId;
	private String transaction;
	private String documentType;
	private String year;
	private String address;
	private String deliveryTerms;
	private String country;
	private String shippingDate;
	private String paymentTerms;
	private List<LineEntryRequest> entryLines;
	private String orderDiscount;
	private String timeslotInfo;
	private String orderCreationTimestamp;
	private String orderShipmentType;

	/**
	 * @return the salesOrderSite
	 */
	public String getSalesOrderSite()
	{
		return salesOrderSite;
	}

	/**
	 * @param salesOrderSite
	 *           the salesOrderSite to set
	 */
	public void setSalesOrderSite(final String salesOrderSite)
	{
		this.salesOrderSite = salesOrderSite;
	}

	/**
	 * @return the orderType
	 */
	public String getOrderType()
	{
		return orderType;
	}

	/**
	 * @param orderType
	 *           the orderType to set
	 */
	public void setOrderType(final String orderType)
	{
		this.orderType = orderType;
	}

	/**
	 * @return the orderNumber
	 */
	public String getOrderNumber()
	{
		return orderNumber;
	}

	/**
	 * @param orderNumber
	 *           the orderNumber to set
	 */
	public void setOrderNumber(final String orderNumber)
	{
		this.orderNumber = orderNumber;
	}

	/**
	 * @return the customer
	 */
	public String getCustomer()
	{
		return customer;
	}

	/**
	 * @param customer
	 *           the customer to set
	 */
	public void setCustomer(final String customer)
	{
		this.customer = customer;
	}

	/**
	 * @return the orderCreationDate
	 */
	public String getOrderCreationDate()
	{
		return orderCreationDate;
	}

	/**
	 * @param orderCreationDate
	 *           the orderCreationDate to set
	 */
	public void setOrderCreationDate(final String orderCreationDate)
	{
		this.orderCreationDate = orderCreationDate;
	}

	/**
	 * @return the orderCode
	 */
	public String getOrderCode()
	{
		return orderCode;
	}

	/**
	 * @param orderCode
	 *           the orderCode to set
	 */
	public void setOrderCode(final String orderCode)
	{
		this.orderCode = orderCode;
	}

	/**
	 * @return the shippingSite
	 */
	public String getShippingSite()
	{
		return shippingSite;
	}

	/**
	 * @param shippingSite
	 *           the shippingSite to set
	 */
	public void setShippingSite(final String shippingSite)
	{
		this.shippingSite = shippingSite;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency()
	{
		return currency;
	}

	/**
	 * @param currency
	 *           the currency to set
	 */
	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	/**
	 * @return the tmReference
	 */
	public String getTmReference()
	{
		return tmReference;
	}

	/**
	 * @param tmReference
	 *           the tmReference to set
	 */
	public void setTmReference(final String tmReference)
	{
		this.tmReference = tmReference;
	}

	/**
	 * @return the bookingId
	 */
	public String getBookingId()
	{
		return bookingId;
	}

	/**
	 * @param bookingId
	 *           the bookingId to set
	 */
	public void setBookingId(final String bookingId)
	{
		this.bookingId = bookingId;
	}

	/**
	 * @return the transaction
	 */
	public String getTransaction()
	{
		return transaction;
	}

	/**
	 * @param transaction
	 *           the transaction to set
	 */
	public void setTransaction(final String transaction)
	{
		this.transaction = transaction;
	}

	/**
	 * @return the documentType
	 */
	public String getDocumentType()
	{
		return documentType;
	}

	/**
	 * @param documentType
	 *           the documentType to set
	 */
	public void setDocumentType(final String documentType)
	{
		this.documentType = documentType;
	}

	/**
	 * @return the year
	 */
	public String getYear()
	{
		return year;
	}

	/**
	 * @param year
	 *           the year to set
	 */
	public void setYear(final String year)
	{
		this.year = year;
	}

	/**
	 * @return the address
	 */
	public String getAddress()
	{
		return address;
	}

	/**
	 * @param address
	 *           the address to set
	 */
	public void setAddress(final String address)
	{
		this.address = address;
	}

	/**
	 * @return the deliveryTerms
	 */
	public String getDeliveryTerms()
	{
		return deliveryTerms;
	}

	/**
	 * @param deliveryTerms
	 *           the deliveryTerms to set
	 */
	public void setDeliveryTerms(final String deliveryTerms)
	{
		this.deliveryTerms = deliveryTerms;
	}

	/**
	 * @return the country
	 */
	public String getCountry()
	{
		return country;
	}

	/**
	 * @param country
	 *           the country to set
	 */
	public void setCountry(final String country)
	{
		this.country = country;
	}

	/**
	 * @return the shippingDate
	 */
	public String getShippingDate()
	{
		return shippingDate;
	}

	/**
	 * @param shippingDate
	 *           the shippingDate to set
	 */
	public void setShippingDate(final String shippingDate)
	{
		this.shippingDate = shippingDate;
	}

	/**
	 * @return the paymentTerms
	 */
	public String getPaymentTerms()
	{
		return paymentTerms;
	}

	/**
	 * @param paymentTerms
	 *           the paymentTerms to set
	 */
	public void setPaymentTerms(final String paymentTerms)
	{
		this.paymentTerms = paymentTerms;
	}

	/**
	 * @return the entryLines
	 */
	public List<LineEntryRequest> getEntryLines()
	{
		return entryLines;
	}

	/**
	 * @param entryLines
	 *           the entryLines to set
	 */
	public void setEntryLines(final List<LineEntryRequest> entryLines)
	{
		this.entryLines = entryLines;
	}

	/**
	 * @return the orderDiscount
	 */
	public String getOrderDiscount()
	{
		return orderDiscount;
	}

	/**
	 * @param orderDiscount
	 *           the orderDiscount to set
	 */
	public void setOrderDiscount(final String orderDiscount)
	{
		this.orderDiscount = orderDiscount;
	}

	/**
	 * @param timeslotInfo
	 *           the timeslotInfo to set
	 */
	public void setTimeslotInfo(final String timeslotInfo)
	{
		this.timeslotInfo = timeslotInfo;
	}

	/**
	 * @return the timeslotInfo
	 */
	public String getTimeslotInfo()
	{
		return timeslotInfo;
	}

	/**
	 * @return the orderCreationTimestamp
	 */
	public String getOrderCreationTimestamp()
	{
		return orderCreationTimestamp;
	}

	/**
	 * @param orderCreationTimestamp
	 *           the orderCreationTimestamp to set
	 */
	public void setOrderCreationTimestamp(final String orderCreationTimestamp)
	{
		this.orderCreationTimestamp = orderCreationTimestamp;
	}

	/**
	 * @return the orderShipmentType
	 */
	public String getOrderShipmentType()
	{
		return orderShipmentType;
	}

	/**
	 * @param orderShipmentType
	 *           the orderShipmentType to set
	 */
	public void setOrderShipmentType(final String orderShipmentType)
	{
		this.orderShipmentType = orderShipmentType;
	}

	@Override
	public String toString()
	{
		return "SalesOrderRequest [salesOrderSite=" + salesOrderSite + ", orderType=" + orderType + ", orderNumber=" + orderNumber
				+ ", customer=" + customer + ", orderCreationDate=" + orderCreationDate + ", orderCode=" + orderCode
				+ ", shippingSite=" + shippingSite + ", currency=" + currency + ", tmReference=" + tmReference + ", bookingId="
				+ bookingId + ", transaction=" + transaction + ", documentType=" + documentType + ", year=" + year + ", address="
				+ address + ", deliveryTerms=" + deliveryTerms + ", country=" + country + ", shippingDate=" + shippingDate
				+ ", paymentTerms=" + paymentTerms + ", entryLines=" + entryLines + ", orderDiscount=" + orderDiscount
				+ ", timeslotInfo=" + timeslotInfo + ", orderCreationTimestamp=" + orderCreationTimestamp + ", orderShipmentType="
				+ orderShipmentType + "]";
	}

}
