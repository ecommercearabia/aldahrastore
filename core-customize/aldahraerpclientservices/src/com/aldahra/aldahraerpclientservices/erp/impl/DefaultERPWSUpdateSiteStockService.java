/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.erp.impl;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import javax.xml.bind.JAXBException;
import javax.xml.rpc.ServiceException;

import org.apache.commons.lang3.StringUtils;
import org.fest.util.Collections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.aldahra.aldahraerpclientservices.erp.ERPWSUpdateSiteStockService;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.ERPWebServiceAttributes;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.Field;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.Group;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.Line;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.SiteStockLines;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.SiteStockParameters;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.SiteStockServiceResponse;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.Tab;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxCallContext;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxMessage;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxResultXml;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxWebServiceXmlCCService;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxWebServiceXmlCCServiceLocator;
import com.aldahra.aldahraerpclientservices.erp.marshaller.XMLMarshallerService;
import com.aldahra.aldahraerpclientservices.erp.marshaller.impl.DefaultXMLMarshallerService;
import com.aldahra.aldahraerpclientservices.erp.response.ResponseMessage;
import com.aldahra.aldahraerpclientservices.erp.response.ResponseMetadata;
import com.aldahra.aldahraerpclientservices.erp.response.SiteStockResponse;
import com.aldahra.aldahraerpclientservices.erp.response.enums.MessageType;
import com.aldahra.aldahraerpclientservices.exception.ERPWSExceptionType;
import com.aldahra.aldahraerpclientservices.exception.ERPWSServiceException;
import com.google.common.base.Preconditions;


/**
 * @author mohammad-abumuhasuen
 */
public class DefaultERPWSUpdateSiteStockService implements ERPWSUpdateSiteStockService
{

	private final XMLMarshallerService<SiteStockParameters, SiteStockServiceResponse> xmlMarshallerService = new DefaultXMLMarshallerService<>();

	private static final Logger LOG = LoggerFactory.getLogger(DefaultERPWSUpdateSiteStockService.class);

	public Optional<SiteStockResponse> getSiteStockData(final String productCode, final String stockSite, final String username,
			final String password, final String poolName, final String schemaName) throws ERPWSServiceException
	{
		Preconditions.checkArgument(productCode != null, "Cannot proceed without an productCode");
		Preconditions.checkArgument(stockSite != null, "Cannot proceed without an stockSite");
		Preconditions.checkArgument(StringUtils.isNotBlank(username), "Cannot proceed without the service username");
		Preconditions.checkArgument(StringUtils.isNotBlank(password), "Cannot proceed without the service password");
		Preconditions.checkArgument(StringUtils.isNotBlank(poolName), "Cannot proceed without the service Pool name");
		Preconditions.checkArgument(StringUtils.isNotBlank(schemaName), "Cannot proceed without the service schemaName");

		final CAdxWebServiceXmlCCService service = new CAdxWebServiceXmlCCServiceLocator();
		final CAdxCallContext call = new CAdxCallContext();
		call.setCodeUser(username);
		call.setPassword(password);
		call.setPoolAlias(poolName);
		call.setPoolId(poolName);
		call.setCodeLang("EN");

		final SiteStockParameters params = buildSiteStockRequest(productCode, stockSite);
		CAdxResultXml result = null;
		try
		{
			final String request = xmlMarshallerService.marshall(params, SiteStockParameters.class);
			result = service.getCAdxWebServiceXmlCC().run(call, schemaName, request);

		}
		catch (RemoteException | ServiceException e)
		{
			LOG.error("The service is not reachable or is down");
			throw new ERPWSServiceException(ERPWSExceptionType.SERVER_ERROR, "The service is not reachable or is down: ");
		}
		catch (final JAXBException e)
		{
			LOG.error("Could not marshall the request {} into xml", params);
			throw new ERPWSServiceException(ERPWSExceptionType.MARSHALLING_ERROR,
					"Could not marshall the request " + params.toString());
		}
		SiteStockServiceResponse siteStockServiceResponse = null;

		if (result == null)
		{
			LOG.error("Result Is null");
			return Optional.empty();
		}

		LOG.info("Web service response code: {}", result.getStatus());

		for (final CAdxMessage message : result.getMessages())
		{
			if (StringUtils.isNotBlank(message.getMessage()))
			{
				LOG.info("Message in response: type={}, message={}", message.getType(), message.getMessage());
			}
		}


		// status => 1: success, 0: error
		if (result.getStatus() == 0)
		{
			LOG.error("Result Status Code is 0 which means there is an error, Please check the log above");
			return Optional.empty();
		}

		try
		{
			LOG.info("{}", result.getResultXml());
			siteStockServiceResponse = xmlMarshallerService.unmarshall(result.getResultXml(), SiteStockServiceResponse.class);
		}
		catch (final JAXBException e)
		{
			throw new ERPWSServiceException(ERPWSExceptionType.UNMARSHALLING_ERROR,
					"Could not unmarshall the response " + result.getResultXml());
		}
		if (siteStockServiceResponse != null)
		{
			return buildSiteStockResponse(siteStockServiceResponse, result);
		}


		return Optional.empty();

	}

	private Optional<SiteStockResponse> buildSiteStockResponse(final SiteStockServiceResponse siteStockServiceResponse,
			final CAdxResultXml result)
	{
		final SiteStockResponse siteStockResponse = new SiteStockResponse();
		final ResponseMessage responseMessage = new ResponseMessage();
		final List<ResponseMetadata> metadata = new ArrayList<>();

		for (final CAdxMessage message : result.getMessages())
		{
			// Some messages retrieved in the response are blank or spaces
			if (StringUtils.isNotBlank(message.getMessage()))
			{
				// Message types => 1: success, 2:warning, 3:error
				final ResponseMetadata data = new ResponseMetadata();
				data.setMessage("Message in response: type=" + message.getType() + ", message=" + message.getMessage());
				data.setType(MessageType.getMessageByCode(message.getType()));
				metadata.add(data);
			}
		}

		fetchResponseAttributes(siteStockServiceResponse, siteStockResponse, responseMessage, metadata);

		return Optional.ofNullable(siteStockResponse);
	}

	private void fetchResponseAttributes(final SiteStockServiceResponse siteStockServiceResponse,
			final SiteStockResponse siteStockResponse, final ResponseMessage responseMessage, final List<ResponseMetadata> metadata)
	{
		if (siteStockServiceResponse == null)
		{
			LOG.info("Response is empty");
			return;
		}

		final Optional<Group> firstgroup = siteStockServiceResponse.getGroups().stream()
				.filter(tab -> tab.getId().equalsIgnoreCase("GRP1")).findFirst();
		if (firstgroup.isPresent())
		{
			final Optional<Field> findFirst = firstgroup.get().getFields().stream()
					.filter(e -> e.getName().equalsIgnoreCase(ERPWebServiceAttributes.PRODUCT.getAttributeName())).findFirst();
			if (findFirst.isPresent())
			{
				siteStockResponse.setProductCode(findFirst.get().getText());

			}
		}


		final List<SiteStockLines> stockLines = fillStockLinesFromSiteStockServiceResponse(siteStockServiceResponse);
		siteStockResponse.setStockLines(stockLines);
		siteStockResponse.setHasStock(!CollectionUtils.isEmpty(stockLines));
		siteStockResponse.setSiteStockResponse(siteStockServiceResponse.toString());
		responseMessage.setMessage(metadata);
		responseMessage.setResponse(extractResponseStatus(siteStockServiceResponse));
		siteStockResponse.setResponse(responseMessage);
	}

	private List<SiteStockLines> fillStockLinesFromSiteStockServiceResponse(
			final SiteStockServiceResponse siteStockServiceResponse)
	{
		final List<SiteStockLines> stockLines = new LinkedList<>();

		if (CollectionUtils.isEmpty(siteStockServiceResponse.getTabs()))
		{
			return stockLines;
		}


		final Optional<Tab> resultab = siteStockServiceResponse.getTabs().stream()
				.filter(tab -> tab.getId().equalsIgnoreCase("GRP2")).findFirst();

		if (resultab.isEmpty() || resultab.get() == null || resultab.get().getLines() == null)
		{
			return stockLines;
		}

		for (final Line line : resultab.get().getLines())
		{
			if (Collections.isEmpty(line.getFields()))
			{
				continue;
			}

			final SiteStockLines siteStockLine = new SiteStockLines();
			line.getFields().stream().forEach(field -> setStockLineField(siteStockLine, field));
			stockLines.add(siteStockLine);
		}



		return stockLines;
	}

	private void setStockLineField(final SiteStockLines siteStockLine, final Field field)
	{
		final String name = field.getName();
		final ERPWebServiceAttributes attribute = ERPWebServiceAttributes.getEnumByAttributeName(name);

		if (attribute == null)
		{
			LOG.error("Cannot Find ERPWebServiceAttributes with attribute name=[{}]", name);
			return;
		}

		switch (attribute)
		{
			case BIN_NUMBER:
				siteStockLine.setBinNumber(field.getText());
				break;
			case EXPIRATION_DATE:
				siteStockLine.setExpirationDate(field.getText());
				break;
			case LOT_NUMBER:
				siteStockLine.setLotNumber(field.getText());
				break;
			case SALES_UNIT:
				siteStockLine.setUnitOfMeasure(field.getText());
				break;
			case SITE_STOCK_QUANTITY:
				siteStockLine.setQuantity(Double.valueOf(field.getText()).intValue());
				break;
			default:
				LOG.warn("Dont Know what to do with Field name [{}]", attribute.getAttributeName());
				break;
		}


	}

	private String extractResponseStatus(final SiteStockServiceResponse siteStockServiceResponse)
	{
		if (siteStockServiceResponse == null)
		{
			return "NO RESPONSE";
		}
		final Optional<Group> resultTab = siteStockServiceResponse.getGroups().stream()
				.filter(group -> ERPWebServiceAttributes.RESULT_GROUP.getAttributeName().equals(group.getId())).findFirst();
		if (resultTab.isEmpty())
		{
			LOG.error("Getting stock level failed!");
			return "FAILED DUE TO MISSING RESULT GROUP GRP3";
		}

		final Optional<Field> createdSuccessfully = resultTab.get().getFields().stream()
				.filter(field -> ERPWebServiceAttributes.RETURN_FLAG.getAttributeName().equals(field.getName())).findFirst();
		final Optional<Field> creationMessage = resultTab.get().getFields().stream()
				.filter(field -> ERPWebServiceAttributes.MESSAGE.getAttributeName().equals(field.getName())).findFirst();
		final String message = creationMessage.isEmpty() ? "FAILED" : creationMessage.get().getText();
		final String status = createdSuccessfully.isEmpty() ? "0" : createdSuccessfully.get().getText();
		return "Status: " + status + ", message: " + message;
	}

	private SiteStockParameters buildSiteStockRequest(final String productCode, final String stockSite)
	{
		final Field siteField = new Field(ERPWebServiceAttributes.STOCK_SITE.getAttributeName(), stockSite);
		final Field requestUserField = new Field(ERPWebServiceAttributes.PRODUCT.getAttributeName(), productCode);

		final SiteStockParameters request = new SiteStockParameters();
		request.setFields(Arrays.asList(siteField, requestUserField));
		return request;
	}

}