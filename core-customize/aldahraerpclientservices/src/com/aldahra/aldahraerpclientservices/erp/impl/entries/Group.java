package com.aldahra.aldahraerpclientservices.erp.impl.entries;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author monzer
 */
@XmlRootElement(name="GRP")
public class Group {

	private String id;
	private List<Field> fields;

	@XmlElement(name="FLD")
	public List<Field> getFields() {
		return fields;
	}

	public void setFields(final List<Field> fields) {
		this.fields = fields;
	}

	@XmlAttribute(name="ID")
	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	@Override
	public String toString()
	{
		return "{\n\tid:" + id + ",\n\t fields:" + fields + "\n}";
	}

}
