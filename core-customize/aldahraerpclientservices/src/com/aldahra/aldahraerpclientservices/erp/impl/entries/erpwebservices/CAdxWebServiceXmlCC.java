/**
 * CAdxWebServiceXmlCC.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices;

public interface CAdxWebServiceXmlCC extends java.rmi.Remote {

	public com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxResultXml run(
			com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxCallContext callContext, java.lang.String publicName,
			java.lang.String inputXml) throws java.rmi.RemoteException;

	public com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxResultXml save(
			com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxCallContext callContext, java.lang.String publicName,
			java.lang.String objectXml) throws java.rmi.RemoteException;

	public com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxResultXml delete(
			com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxCallContext callContext, java.lang.String publicName,
			com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxParamKeyValue[] objectKeys) throws java.rmi.RemoteException;

	public com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxResultXml read(
			com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxCallContext callContext, java.lang.String publicName,
			com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxParamKeyValue[] objectKeys) throws java.rmi.RemoteException;

	public com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxResultXml query(
			com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxCallContext callContext, java.lang.String publicName,
			com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxParamKeyValue[] objectKeys, int listSize)
			throws java.rmi.RemoteException;

	public com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxResultXml getDescription(
			com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxCallContext callContext, java.lang.String publicName)
			throws java.rmi.RemoteException;

	public com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxResultXml actionObject(
			com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxCallContext callContext, java.lang.String publicName,
			java.lang.String actionCode, java.lang.String objectXml) throws java.rmi.RemoteException;

	public com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxResultXml getDataXmlSchema(
			com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxCallContext callContext, java.lang.String publicName)
			throws java.rmi.RemoteException;

	public com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxResultXml actionObjectKeys(
			com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxCallContext callContext, java.lang.String publicName,
			java.lang.String actionCode, com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxParamKeyValue[] objectKeys)
			throws java.rmi.RemoteException;

	public com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxResultXml insertLines(
			com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxCallContext callContext, java.lang.String publicName,
			com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxParamKeyValue[] objectKeys, java.lang.String blocKey,
			java.lang.String lineKey, java.lang.String lineXml) throws java.rmi.RemoteException;

	public com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxResultXml deleteLines(
			com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxCallContext callContext, java.lang.String publicName,
			com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxParamKeyValue[] objectKeys, java.lang.String blocKey,
			java.lang.String[] lineKeys) throws java.rmi.RemoteException;

	public com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxResultXml modify(
			com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxCallContext callContext, java.lang.String publicName,
			com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxParamKeyValue[] objectKeys, java.lang.String objectXml)
			throws java.rmi.RemoteException;

}
