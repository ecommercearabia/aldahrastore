package com.aldahra.aldahraerpclientservices.erp.impl.entries;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author monzer
 */
@XmlRootElement(name = "RESULT")
public class ProductStockServiceResponse implements Serializable{

	private List<Tab> tabs;

	@XmlElement(name = "TAB", required = true)
	public List<Tab> getTabs() {
		return tabs;
	}

	public void setTabs(final List<Tab> tabs) {
		this.tabs = tabs;
	}

	@Override
	public String toString()
	{
		return "{\n\ttabs:" + tabs + "\n}";
	}


}
