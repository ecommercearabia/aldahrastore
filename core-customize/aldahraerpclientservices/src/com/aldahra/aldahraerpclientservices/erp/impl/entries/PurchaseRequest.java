/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.erp.impl.entries;

/**
 * @author monzer
 */
public class PurchaseRequest
{

	private String site;
	private String date;
	private String requestUser;
	private String requestNumber;
	private ProductPurchaseLineEntry productLine;

	/**
	 * @return the site
	 */
	public String getSite()
	{
		return site;
	}

	/**
	 * @param site
	 *           the site to set
	 */
	public void setSite(final String site)
	{
		this.site = site;
	}

	/**
	 * @return the date
	 */
	public String getDate()
	{
		return date;
	}

	/**
	 * @param date
	 *           the date to set
	 */
	public void setDate(final String date)
	{
		this.date = date;
	}

	/**
	 * @return the requestUser
	 */
	public String getRequestUser()
	{
		return requestUser;
	}

	/**
	 * @param requestUser
	 *           the requestUser to set
	 */
	public void setRequestUser(final String requestUser)
	{
		this.requestUser = requestUser;
	}

	/**
	 * @return the requestNumber
	 */
	public String getRequestNumber()
	{
		return requestNumber;
	}

	/**
	 * @param requestNumber
	 *           the requestNumber to set
	 */
	public void setRequestNumber(final String requestNumber)
	{
		this.requestNumber = requestNumber;
	}

	/**
	 * @return the productLine
	 */
	public ProductPurchaseLineEntry getProductLine()
	{
		return productLine;
	}

	/**
	 * @param productLine
	 *           the productLine to set
	 */
	public void setProductLine(final ProductPurchaseLineEntry productLine)
	{
		this.productLine = productLine;
	}

	@Override
	public String toString()
	{
		return "{\n\tsite:" + site + ",\n\t date:" + date + ",\n\t requestUser:" + requestUser + ",\n\t requestNumber:"
				+ requestNumber + ",\n\t productLine:" + productLine + "\n}";
	}

}
