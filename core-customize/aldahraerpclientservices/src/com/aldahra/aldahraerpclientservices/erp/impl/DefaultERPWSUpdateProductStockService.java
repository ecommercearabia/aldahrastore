/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.erp.impl;

import de.hybris.platform.store.services.BaseStoreService;

import java.io.StringReader;
import java.io.StringWriter;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.rpc.ServiceException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.xml.sax.InputSource;

import com.aldahra.aldahraerpclientservices.erp.ERPWSUpdateProductStockService;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.ERPWebServiceAttributes;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.Field;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.Line;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.ProdctStockParameters;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.ProductStockServiceResponse;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.Tab;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxCallContext;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxMessage;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxResultXml;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxWebServiceXmlCCService;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxWebServiceXmlCCServiceLocator;
import com.aldahra.aldahraerpclientservices.erp.response.ProductStockResponse;
import com.aldahra.aldahraerpclientservices.erp.response.ResponseMessage;
import com.aldahra.aldahraerpclientservices.erp.response.ResponseMetadata;
import com.aldahra.aldahraerpclientservices.erp.response.enums.MessageType;
import com.aldahra.aldahraerpclientservices.erp.response.enums.Status;
import com.aldahra.aldahraerpclientservices.exception.ERPWSExceptionType;
import com.aldahra.aldahraerpclientservices.exception.ERPWSServiceException;
import com.google.common.base.Preconditions;


/**
 * @author monzer
 */
public class DefaultERPWSUpdateProductStockService implements ERPWSUpdateProductStockService
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultERPWSUpdateProductStockService.class);

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Override
	public List<ProductStockResponse> getProductStockData(final List<String> productCodes, final String username,
			final String password, final String poolName, final String schemaName, final String stockSiteUid)
			throws ERPWSServiceException
	{
		Preconditions.checkArgument(!CollectionUtils.isEmpty(productCodes), "Cannot proceed without product codes");
		Preconditions.checkArgument(StringUtils.isNotBlank(username), "Cannot proceed without the service username");
		Preconditions.checkArgument(StringUtils.isNotBlank(password), "Cannot proceed without the service password");
		Preconditions.checkArgument(StringUtils.isNotBlank(poolName), "Cannot proceed without the service Pool name");
		Preconditions.checkArgument(StringUtils.isNotBlank(schemaName), "Cannot proceed without the service schemaName");
		Preconditions.checkArgument(StringUtils.isNotBlank(stockSiteUid), "Cannot proceed without the service stock Site Uid");

		final ProdctStockParameters params = buildRequestParameters(productCodes, stockSiteUid);

		final List<ProductStockResponse> responses = new ArrayList();

		productCodes.stream().forEach(code -> {
			Optional<ProductStockResponse> response = Optional.empty();
			try
			{
				response = getProductStockData(code, username, password, poolName, schemaName, stockSiteUid);
			}
			catch (final ERPWSServiceException e)
			{
				final ProductStockResponse res = new ProductStockResponse();
				res.setProductCode(code);
				final ResponseMessage responseMessage = new ResponseMessage();
				final ResponseMetadata metadata = new ResponseMetadata();
				metadata.setMessage(e.getMessage());
				metadata.setType(MessageType.ERROR);
				responseMessage.setMessage(Arrays.asList(metadata));
				res.setResponse(responseMessage);
				res.setHasStock(false);
				responses.add(res);
			}
			if (response.isPresent())
			{
				responses.add(response.get());
			}
		});

		return CollectionUtils.isEmpty(responses) ? Collections.EMPTY_LIST : responses;
	}

	@Override
	public Optional<ProductStockResponse> getProductStockData(final String productCode, final String username,
			final String password, final String poolName, final String schemaName, final String stockSiteUid)
			throws ERPWSServiceException
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(productCode), "Cannot proceed without product codes");
		Preconditions.checkArgument(StringUtils.isNotBlank(username), "Cannot proceed without the service username");
		Preconditions.checkArgument(StringUtils.isNotBlank(password), "Cannot proceed without the service password");
		Preconditions.checkArgument(StringUtils.isNotBlank(poolName), "Cannot proceed without the service Pool name");
		Preconditions.checkArgument(StringUtils.isNotBlank(schemaName), "Cannot proceed without the service schemaName");
		Preconditions.checkArgument(StringUtils.isNotBlank(stockSiteUid), "Cannot proceed without the service stock Site Uid");

		final ProdctStockParameters params = buildRequestParameters(productCode, stockSiteUid);
		return sendProductRequest(params, username, password, poolName, schemaName, stockSiteUid);
	}

	private Optional<ProductStockResponse> buildResponseForProductStock(final ProductStockServiceResponse serviceResponse,
			final CAdxMessage[] messages)
	{
		if (serviceResponse == null)
		{
			return Optional.empty();
		}

		final ProductStockResponse response = new ProductStockResponse();

		final Optional<Tab> productsTab = serviceResponse.getTabs().stream().filter(tab -> "GRP1".equals(tab.getId())).findFirst();
		final Optional<Tab> productsQuantityTab = serviceResponse.getTabs().stream().filter(tab -> "GRP2".equals(tab.getId()))
				.findFirst();
		if (productsTab.isEmpty())
		{
			LOG.error("Product tab object in the response is EMPTY!");
			return Optional.empty();
		}

		productsTab.get().getLines().stream().forEach(product -> {
			response.setProductCode(extrectProductCode(product.getFields()));

			final Optional<Line> quantityLine = productsQuantityTab.isPresent()
					? productsQuantityTab.get().getLines().stream().filter(qLine -> product.getNumber().equals(qLine.getNumber()))
							.findFirst()
					: Optional.empty();

			response.setHasStock(quantityLine.isPresent());
			if (quantityLine.isPresent())
			{
				response.setStock(extractProductStockAvailable(quantityLine.get().getFields()));
			}
			final ResponseMessage responseMessage = new ResponseMessage();
			final List<ResponseMetadata> metadata = new ArrayList();
			final List<String> s = new ArrayList();
			for (final CAdxMessage message : messages)
			{
				// Some messages retrieved in the response are blank or spaces
				if (StringUtils.isNotBlank(message.getMessage()))
				{
					// Message types => 1: success, 2:warning, 3:error
					final ResponseMetadata data = new ResponseMetadata();
					data.setMessage("Message in response: type=" + message.getType() + ", message=" + message.getMessage());
					data.setType(MessageType.getMessageByCode(message.getType()));
					metadata.add(data);
				}
			}
			responseMessage.setMessage(metadata);
			responseMessage.setResponse("Product Code: " + response.getProductCode() + ", has stock: " + quantityLine.isPresent()
					+ ", stock: " + response.getStock());
			responseMessage.setStatus(quantityLine.isEmpty() ? Status.FAILURE : Status.SUCCESS);
			response.setResponse(responseMessage);
		});

		return Optional.ofNullable(response);
	}

	private String extrectProductCode(final List<Field> fields)
	{
		final Optional<Field> productCode = fields.stream()
				.filter(field -> ERPWebServiceAttributes.PRODUCT_CODE.getAttributeName().equals(field.getName())).findFirst();
		return productCode.isPresent() ? productCode.get().getText() : "";
	}

	private int extractProductStockAvailable(final List<Field> fields)
	{
		final Optional<Field> quantity = fields.stream()
				.filter(field -> ERPWebServiceAttributes.QUANTITY_AVAILABEL.getAttributeName().equals(field.getName())).findFirst();
		return quantity.isPresent() ? Double.valueOf(quantity.get().getText()).intValue() : 0;
	}

	private ProdctStockParameters buildRequestParameters(final List<String> productCodes, final String stockSiteUid)
	{
		final List<Line> lines = new ArrayList();
		productCodes.stream().forEach(code -> {
			final Line line = new Line();
			final Field fieldSite = new Field(ERPWebServiceAttributes.STOCK_SITE.getAttributeName(), stockSiteUid);
			final Field fieldProduct = new Field(ERPWebServiceAttributes.PRODUCT_CODE.getAttributeName(), code);
			final List<Field> fields = new ArrayList();
			fields.add(fieldSite);
			fields.add(fieldProduct);
			line.setFields(fields);
			lines.add(line);
		});
		final Tab tab = new Tab();
		tab.setLines(lines);
		tab.setId(ERPWebServiceAttributes.REQUEST_GROUP.getAttributeName());
		final ProdctStockParameters param = new ProdctStockParameters();
		param.setTabs(Arrays.asList(tab));
		return param;
	}

	private ProdctStockParameters buildRequestParameters(final String productCode, final String stockSiteUid)
	{
		final List<Line> lines = new ArrayList();
		final Line line = new Line();
		final Field fieldSite = new Field(ERPWebServiceAttributes.STOCK_SITE.getAttributeName(), stockSiteUid);
		final Field fieldProduct = new Field(ERPWebServiceAttributes.PRODUCT_CODE.getAttributeName(), productCode);
		final List<Field> fields = new ArrayList();
		fields.add(fieldSite);
		fields.add(fieldProduct);
		line.setFields(fields);
		lines.add(line);
		final Tab tab = new Tab();
		tab.setLines(lines);
		tab.setId(ERPWebServiceAttributes.REQUEST_GROUP.getAttributeName());
		final ProdctStockParameters param = new ProdctStockParameters();
		param.setTabs(Arrays.asList(tab));
		return param;
	}

	protected String marshallProductParameters(final ProdctStockParameters param) throws JAXBException
	{
		final JAXBContext context = JAXBContext.newInstance(ProdctStockParameters.class);
		final Marshaller marshaller = context.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		final StringWriter writer = new StringWriter();
		marshaller.marshal(param, writer);
		LOG.info("Request marshalled with the result: {}", writer.toString());
		return writer.toString();
	}

	protected ProductStockServiceResponse unmarshallProductStockParams(final String response) throws JAXBException
	{
		final JAXBContext context = JAXBContext.newInstance(ProductStockServiceResponse.class);
		final Unmarshaller unmarshaller = context.createUnmarshaller();
		final InputSource input = new InputSource();
		input.setCharacterStream(new StringReader(response));
		final ProductStockServiceResponse unmarshal = (ProductStockServiceResponse) unmarshaller.unmarshal(input);
		return unmarshal;
	}

	private Optional<ProductStockResponse> sendProductRequest(final ProdctStockParameters params, final String username,
			final String password, final String poolName, final String schemaName, final String stockSiteUid)
			throws ERPWSServiceException
	{
		final CAdxWebServiceXmlCCService service = new CAdxWebServiceXmlCCServiceLocator();
		final CAdxCallContext call = new CAdxCallContext();
		call.setCodeUser(username);
		call.setPassword(password);
		call.setPoolAlias(poolName);
		call.setPoolId(poolName);
		call.setCodeLang("EN");
		if (params == null)
		{
			LOG.error("Invalid request, Request parameters are null!");
			throw new ERPWSServiceException(ERPWSExceptionType.INVALID_REQUEST, "Request parameters are null!");
		}
		CAdxResultXml result = null;
		try
		{
			final String request = marshallProductParameters(params);
			result = service.getCAdxWebServiceXmlCC().run(call, schemaName, request);
		}
		catch (RemoteException | ServiceException e)
		{
			LOG.error("The service is not reachable or is down");
			throw new ERPWSServiceException(ERPWSExceptionType.SERVER_ERROR, "The service is not reachable or is down: ");
		}
		catch (final JAXBException e)
		{
			LOG.error("Could not marshall the request {} into xml", params);
			throw new ERPWSServiceException(ERPWSExceptionType.MARSHALLING_ERROR,
					"Could not marshall the request " + params.toString());
		}

		ProductStockServiceResponse productStockResponse = null;
		LOG.info("Web service response code: {}", result.getStatus());

		// status => 1: success, 0: error
		if (result == null || result.getStatus() != 1)
		{
			throw new ERPWSServiceException(ERPWSExceptionType.INVALID_RESPONSE, "Web service response code: " + result.getStatus());
		}

		final String resultXml = result.getResultXml();
		try
		{
			LOG.info("Product Stock response data is: {}", resultXml);
			productStockResponse = unmarshallProductStockParams(resultXml);
		}
		catch (final JAXBException e)
		{
			throw new ERPWSServiceException(ERPWSExceptionType.UNMARSHALLING_ERROR,
					"Could not unmarshall the response " + resultXml);
		}

		if (productStockResponse == null)
		{
			throw new ERPWSServiceException(ERPWSExceptionType.INVALID_RESPONSE);
		}

		return buildResponseForProductStock(productStockResponse, result.getMessages());
	}

}
