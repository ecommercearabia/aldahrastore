package com.aldahra.aldahraerpclientservices.erp.impl.entries;

/**
 * @author monzer
 */
import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

@XmlRootElement(name = "PARAM")
@XmlSeeAlso({Tab.class})
public class ProdctStockParameters implements Serializable{

	private List<Tab> tabs;

	@XmlElement(name = "TAB", required = true)
	public List<Tab> getTabs() {
		return tabs;
	}

	public void setTabs(final List<Tab> tabs) {
		this.tabs = tabs;
	}

	@Override
	public String toString()
	{
		return "{\n\ttabs:" + tabs + "\n}";
	}

}
