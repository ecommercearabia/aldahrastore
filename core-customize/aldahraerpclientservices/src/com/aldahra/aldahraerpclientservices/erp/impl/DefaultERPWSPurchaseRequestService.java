package com.aldahra.aldahraerpclientservices.erp.impl;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;
import javax.xml.bind.JAXBException;
import javax.xml.rpc.ServiceException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aldahra.aldahraerpclientservices.erp.ERPWSPurchaseRequestService;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.ERPWebServiceAttributes;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.Field;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.Group;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.Line;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.ProductPurchaseLineEntry;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.PurchaseRequest;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.PurchaseRequestParameters;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.PurchaseRequestServiceResponse;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.Tab;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxCallContext;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxMessage;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxResultXml;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxWebServiceXmlCCService;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.erpwebservices.CAdxWebServiceXmlCCServiceLocator;
import com.aldahra.aldahraerpclientservices.erp.marshaller.XMLMarshallerService;
import com.aldahra.aldahraerpclientservices.erp.response.PurchaseResponse;
import com.aldahra.aldahraerpclientservices.erp.response.ResponseMessage;
import com.aldahra.aldahraerpclientservices.erp.response.ResponseMetadata;
import com.aldahra.aldahraerpclientservices.erp.response.enums.MessageType;
import com.aldahra.aldahraerpclientservices.erp.response.enums.Status;
import com.aldahra.aldahraerpclientservices.exception.ERPWSExceptionType;
import com.aldahra.aldahraerpclientservices.exception.ERPWSServiceException;
import com.google.common.base.Preconditions;


/**
 * @author mohammad-abumuhasien
 * @author monzer
 */
public class DefaultERPWSPurchaseRequestService implements ERPWSPurchaseRequestService
{

	@Resource(name = "xmlMarshallerService")
	private XMLMarshallerService<PurchaseRequestParameters, PurchaseRequestServiceResponse> xmlMarshallerService;

	private static final Logger LOG = LoggerFactory.getLogger(DefaultERPWSPurchaseRequestService.class);

	@Override
	public Optional<PurchaseResponse> createPurchaseRequest(final PurchaseRequest purchaseRequest, final String username,
			final String password, final String poolName, final String schemaName) throws ERPWSServiceException
	{
		Preconditions.checkArgument(purchaseRequest != null, "Cannot proceed with empty request");
		Preconditions.checkArgument(StringUtils.isNotBlank(username), "Cannot proceed without the service username");
		Preconditions.checkArgument(StringUtils.isNotBlank(password), "Cannot proceed without the service password");
		Preconditions.checkArgument(StringUtils.isNotBlank(poolName), "Cannot proceed without the service Pool name");
		Preconditions.checkArgument(StringUtils.isNotBlank(schemaName), "Cannot proceed without the service schemaName");

		if (!validatePurchaseRequestData(purchaseRequest))
		{
			LOG.error("Invalid request, Some Request parameters are null!: {}", purchaseRequest.toString());
			throw new ERPWSServiceException(ERPWSExceptionType.INVALID_REQUEST,
					"Invalid request, Some Request parameters are null!");
		}

		final CAdxWebServiceXmlCCService service = new CAdxWebServiceXmlCCServiceLocator();
		final CAdxCallContext call = new CAdxCallContext();
		call.setCodeUser(username);
		call.setPassword(password);
		call.setPoolAlias(poolName);
		call.setPoolId(poolName);
		call.setCodeLang("EN");

		final PurchaseRequestParameters params = buildPurchaseRequest(purchaseRequest);

		if (params == null)
		{
			LOG.error("Invalid request, Request parameters are null!");
			throw new ERPWSServiceException(ERPWSExceptionType.INVALID_REQUEST, "Request parameters are null!");
		}

		CAdxResultXml result = null;
		String request = "";
		try
		{
			request = xmlMarshallerService.marshall(params, PurchaseRequestParameters.class);

			result = service.getCAdxWebServiceXmlCC().run(call, schemaName, request);

		}
		catch (RemoteException | ServiceException e)
		{
			LOG.error("The service is not reachable or is down");
			throw new ERPWSServiceException(ERPWSExceptionType.SERVER_ERROR, "The service is not reachable or is down: ");
		}
		catch (final JAXBException e)
		{
			LOG.error("Could not marshall the request {} into xml", params);
			throw new ERPWSServiceException(ERPWSExceptionType.MARSHALLING_ERROR,
					"Could not marshall the request " + params.toString());
		}
		PurchaseRequestServiceResponse purchaseRequestServiceResponse = null;
		if (result == null)
		{
			return buildPurchaseResponse(purchaseRequestServiceResponse, result, purchaseRequest.getProductLine().getProduct(),
					request);
		}
		LOG.info("Web service response code: {}", result.getStatus());

		for (final CAdxMessage message : result.getMessages())
		{

			if (StringUtils.isNotBlank(message.getMessage()))
			{

				LOG.info("Message in response: type={}, message={}", message.getType(), message.getMessage());
			}
		}
		if (result.getStatus() == 1)
		{ // status => 1: success, 0: error
			try
			{
				purchaseRequestServiceResponse = xmlMarshallerService.unmarshall(result.getResultXml(),
						PurchaseRequestServiceResponse.class);
			}
			catch (final JAXBException e)
			{
				throw new ERPWSServiceException(ERPWSExceptionType.UNMARSHALLING_ERROR,
						"Could not unmarshall the response " + result.getResultXml());
			}
		}


		return buildPurchaseResponse(purchaseRequestServiceResponse, result, purchaseRequest.getProductLine().getProduct(),
				request);
	}

	/**
	*
	*/
	private Optional<PurchaseResponse> buildPurchaseResponse(final PurchaseRequestServiceResponse response,
			final CAdxResultXml result, final String productCode, final String request)
	{
		final PurchaseResponse purchaseResponse = new PurchaseResponse();
		final ResponseMessage responseMessage = new ResponseMessage();
		final List<ResponseMetadata> metadata = new ArrayList<>();
		boolean success = true;

		for (final CAdxMessage message : result.getMessages())
		{
			// Some messages retrieved in the response are blank or spaces
			if (StringUtils.isNotBlank(message.getMessage()))
			{
				// Message types => 1: success, 2:warning, 3:error
				final ResponseMetadata data = new ResponseMetadata();
				data.setMessage("Message in response: type=" + message.getType() + ", message=" + message.getMessage());
				data.setType(MessageType.getMessageByCode(message.getType()));
				if (MessageType.ERROR.equals(data.getType()))
				{
					success = false;
				}
				metadata.add(data);
			}
		}
		purchaseResponse.setCreationSuccess(success && purchaseCreatedSuccessfully(response));
		final String creationResponse = extractCreationResponse(response);
		purchaseResponse.setPurchaseCreationResponse(creationResponse);
		responseMessage.setMessage(metadata);
		responseMessage.setResponse(creationResponse);
		responseMessage.setStatus(purchaseResponse.getCreationSuccess() ? Status.SUCCESS : Status.FAILURE);
		purchaseResponse.setMessages(responseMessage);
		purchaseResponse.setProductCode(productCode);
		purchaseResponse.setRequest(request);
		purchaseResponse.setResponse(result.getResultXml());
		return Optional.ofNullable(purchaseResponse);
	}

	/**
	*
	*/
	private String extractCreationResponse(final PurchaseRequestServiceResponse response)
	{
		if (response == null)
		{
			return "NO RESPONSE";
		}
		final Optional<Group> resultTab = response.getGroups().stream()
				.filter(group -> ERPWebServiceAttributes.RESULT_GROUP.getAttributeName().equals(group.getId())).findFirst();
		if (resultTab.isEmpty())
		{
			LOG.error("Sales Order creation failed!");
			return "FAILED DUE TO MISSING RESULT GROUP GRP3";
		}

		final Optional<Field> createdSuccessfully = resultTab.get().getFields().stream()
				.filter(field -> ERPWebServiceAttributes.RETURN_FLAG.getAttributeName().equals(field.getName())).findFirst();
		final Optional<Field> creationMessage = resultTab.get().getFields().stream()
				.filter(field -> ERPWebServiceAttributes.MESSAGE.getAttributeName().equals(field.getName())).findFirst();
		final String message = creationMessage.isEmpty() ? "FAILED" : creationMessage.get().getText();
		final String status = createdSuccessfully.isEmpty() ? "0" : createdSuccessfully.get().getText();
		return "Status: " + status + ", message: " + message;
	}

	private boolean purchaseCreatedSuccessfully(final PurchaseRequestServiceResponse response)
	{
		if (response == null)
		{
			return false;
		}
		final Optional<Group> resultTab = response.getGroups().stream()
				.filter(group -> ERPWebServiceAttributes.RESULT_GROUP.getAttributeName().equals(group.getId())).findFirst();
		if (resultTab.isEmpty())
		{
			LOG.error("Sales Order creation failed!");
			return false;
		}

		final Optional<Field> createdSuccessfully = resultTab.get().getFields().stream()
				.filter(field -> ERPWebServiceAttributes.RETURN_FLAG.getAttributeName().equals(field.getName())).findFirst();

		// if the createdSuccessfully attribute == 1 then the Purchase creation is
		// failed. it has to be 2
		return createdSuccessfully.isEmpty() || !"2".equals(createdSuccessfully.get().getText()) ? false : true;
	}

	/**
	*
	*/
	private boolean validatePurchaseRequestData(final PurchaseRequest purchaseRequest)
	{
		if (StringUtils.isBlank(purchaseRequest.getSite()))
		{
			return false;
		}
		if (StringUtils.isBlank(purchaseRequest.getDate()))
		{
			return false;
		}
		if (StringUtils.isBlank(purchaseRequest.getRequestUser()))
		{
			return false;
		}

		if (purchaseRequest.getProductLine() == null)
		{
			return false;
		}
		final ProductPurchaseLineEntry line = purchaseRequest.getProductLine();
		if (StringUtils.isBlank(line.getProduct()))
		{
			return false;
		}
		if (StringUtils.isBlank(line.getPurchaseUnit()))
		{
			return false;
		}
		if (StringUtils.isBlank(line.getQuantity()))
		{
			return false;
		}
		if (StringUtils.isBlank(line.getReceivingSite()))
		{
			return false;
		}
		if (StringUtils.isBlank(line.getReceivingDate()))
		{
			return false;
		}

		return true;
	}

	private PurchaseRequestParameters buildPurchaseRequest(final PurchaseRequest purchaseRequest)
	{
		final Field siteField = new Field(ERPWebServiceAttributes.SITE.getAttributeName(), purchaseRequest.getSite());
		final Field requestUserField = new Field(ERPWebServiceAttributes.REQUEST_USER.getAttributeName(),
				purchaseRequest.getRequestUser());
		final Field dateField = new Field(ERPWebServiceAttributes.DATE.getAttributeName(), purchaseRequest.getDate());
		final Field requestNumberField = new Field(ERPWebServiceAttributes.REQUEST_NUMBER.getAttributeName(),
				purchaseRequest.getRequestNumber());

		final PurchaseRequestParameters request = new PurchaseRequestParameters();
		request.setFields(Arrays.asList(siteField, requestNumberField, dateField, requestUserField));
		request.setTabs(buildPurchaseRequestEntries(purchaseRequest.getProductLine()));
		return request;
	}

	private List<Tab> buildPurchaseRequestEntries(final ProductPurchaseLineEntry entry)
	{
		if (entry == null)
		{
			return Collections.emptyList();
		}
		final List<Line> lines = new ArrayList<>();
		final Tab entryTab = new Tab();
		int number = 0;
		final Field productCode = new Field(ERPWebServiceAttributes.PRODUCT.getAttributeName(),
				ERPWebServiceAttributes.PRODUCT.getAttributeType(), entry.getProduct());
		final Field productDescription = new Field(ERPWebServiceAttributes.DESCRIPTION.getAttributeName(),
				ERPWebServiceAttributes.DESCRIPTION.getAttributeType(), entry.getDescription());
		final Field receivingSite = new Field(ERPWebServiceAttributes.RECEIVING_SITE.getAttributeName(),
				ERPWebServiceAttributes.RECEIVING_SITE.getAttributeType(), entry.getReceivingSite());
		final Field purcahseUnit = new Field(ERPWebServiceAttributes.PURCHASE_UNIT.getAttributeName(),
				ERPWebServiceAttributes.PURCHASE_UNIT.getAttributeType(), entry.getPurchaseUnit());
		final Field quantity = new Field(ERPWebServiceAttributes.STOCK_QUANTITY.getAttributeName(),
				ERPWebServiceAttributes.STOCK_QUANTITY.getAttributeType(), entry.getQuantity());

		final Field supplier = new Field(ERPWebServiceAttributes.SUPPLIER.getAttributeName(),
				ERPWebServiceAttributes.SUPPLIER.getAttributeType(), entry.getSupplier());

		final Field receivingDate = new Field(ERPWebServiceAttributes.RECEIVING_DATE.getAttributeName(),
				ERPWebServiceAttributes.RECEIVING_DATE.getAttributeType(), entry.getReceivingDate());

		final Field grossPrice = new Field(ERPWebServiceAttributes.GROSS_PRICE.getAttributeName(),
				ERPWebServiceAttributes.GROSS_PRICE.getAttributeType(), entry.getGrossPrice());
		final Line entryLine = new Line();
		entryLine.setNumber(String.valueOf(++number));
		entryLine.setFields(Arrays.asList(productCode, productDescription, receivingSite, purcahseUnit, quantity, supplier,
				receivingDate, grossPrice));
		lines.add(entryLine);
		entryTab.setLines(lines);
		entryTab.setDim("1000");
		entryTab.setId("GRP2");
		entryTab.setSize(String.valueOf(1));
		return Arrays.asList(entryTab);
	}
}