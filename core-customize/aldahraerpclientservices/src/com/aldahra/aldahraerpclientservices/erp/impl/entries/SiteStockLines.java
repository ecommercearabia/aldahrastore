package com.aldahra.aldahraerpclientservices.erp.impl.entries;

public class SiteStockLines
{
	private String binNumber;
	private String expirationDate;
	private String lotNumber;
	private int quantity;
	private String unitOfMeasure;

	public SiteStockLines()
	{
	}

	public String getBinNumber()
	{
		return binNumber;
	}

	public void setBinNumber(final String binNumber)
	{
		this.binNumber = binNumber;
	}

	public String getExpirationDate()
	{
		return expirationDate;
	}

	public void setExpirationDate(final String expirationDate)
	{
		this.expirationDate = expirationDate;
	}

	public String getLotNumber()
	{
		return lotNumber;
	}

	public void setLotNumber(final String lotNumber)
	{
		this.lotNumber = lotNumber;
	}


	public int getQuantity()
	{
		return quantity;
	}

	public void setQuantity(final int quantity)
	{
		this.quantity = quantity;
	}

	public String getUnitOfMeasure()
	{
		return unitOfMeasure;
	}

	public void setUnitOfMeasure(final String unitOfMeasure)
	{
		this.unitOfMeasure = unitOfMeasure;
	}

}
