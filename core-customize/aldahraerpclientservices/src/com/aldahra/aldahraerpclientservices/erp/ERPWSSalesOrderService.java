/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.erp;

import com.aldahra.aldahraerpclientservices.erp.impl.entries.SalesOrderRequest;
import com.aldahra.aldahraerpclientservices.erp.response.SalesOrderResponse;
import com.aldahra.aldahraerpclientservices.exception.ERPWSServiceException;


/**
 * @author monzer
 */
public interface ERPWSSalesOrderService
{

	SalesOrderResponse createSalesOrder(SalesOrderRequest abstractOrder, String username, String password, String poolName,
			String schemaName, String orderType) throws ERPWSServiceException;

}
