/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.erp;

import java.util.Optional;

import com.aldahra.aldahraerpclientservices.erp.impl.entries.PurchaseRequest;
import com.aldahra.aldahraerpclientservices.erp.response.PurchaseResponse;
import com.aldahra.aldahraerpclientservices.exception.ERPWSServiceException;


/**
 * @author mohammad-abumuhasien
 * @author monzer
 */
public interface ERPWSPurchaseRequestService
{
	Optional<PurchaseResponse> createPurchaseRequest(PurchaseRequest request, String username, String password, String poolName,
			String schemaName) throws ERPWSServiceException;
}
