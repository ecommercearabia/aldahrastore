package com.aldahra.aldahraerpclientservices.erp.marshaller;

import javax.xml.bind.JAXBException;


public interface XMLMarshallerService<T, R>
{
	public String marshall(T data, Class<?> clazz) throws JAXBException;

	public R unmarshall(String data, Class<?> clazz) throws JAXBException;

}
