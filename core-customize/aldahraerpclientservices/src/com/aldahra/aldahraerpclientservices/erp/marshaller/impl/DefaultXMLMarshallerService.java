package com.aldahra.aldahraerpclientservices.erp.marshaller.impl;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;

import com.aldahra.aldahraerpclientservices.erp.marshaller.XMLMarshallerService;


public class DefaultXMLMarshallerService<T, R> implements XMLMarshallerService<T, R>
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultXMLMarshallerService.class);

	@Override
	public String marshall(final T param, final Class<?> clazz) throws JAXBException
	{
		final JAXBContext context = JAXBContext.newInstance(clazz);
		final Marshaller marshaller = context.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		final StringWriter writer = new StringWriter();
		marshaller.marshal(param, writer);
		LOG.info("XML Request: " + writer.toString());

		return writer.toString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public R unmarshall(final String response, final Class<?> clazz) throws JAXBException
	{
		final JAXBContext context = JAXBContext.newInstance(clazz);
		final Unmarshaller unmarshaller = context.createUnmarshaller();
		final InputSource input = new InputSource();
		input.setCharacterStream(new StringReader(response));
		final R unmarshal = (R) unmarshaller.unmarshal(input);
		return unmarshal;
	}



}
