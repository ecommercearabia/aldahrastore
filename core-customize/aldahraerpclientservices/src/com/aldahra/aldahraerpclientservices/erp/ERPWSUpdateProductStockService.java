/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.erp;

import java.util.List;
import java.util.Optional;

import com.aldahra.aldahraerpclientservices.erp.response.ProductStockResponse;
import com.aldahra.aldahraerpclientservices.exception.ERPWSServiceException;


/**
 * @author monzer
 */
public interface ERPWSUpdateProductStockService
{

	List<ProductStockResponse> getProductStockData(List<String> productCodes, String username, String password,
			String poolName, String schemaName, String stockSiteUid) throws ERPWSServiceException;

	Optional<ProductStockResponse> getProductStockData(String productCode, String username, String password, String poolName,
			String schemaName, String stockSiteUid) throws ERPWSServiceException;

}
