/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.erp.populator;

import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.util.DiscountValue;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.aldahra.aldahraerpclientservices.erp.impl.entries.LineEntriesRequest;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.LineEntryRequest;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.SalesOrderProviderContainer;
import com.aldahra.aldahraerpclientservices.model.UnitOfMesureMappingModel;
import com.aldahra.aldahraerpclientwebservices.model.AldahraERPSalesOrderServiceProviderModel;


/**
 * @author monzer
 */
public class SalesOrderEntryRequestPopulator implements Populator<SalesOrderProviderContainer, LineEntriesRequest>
{

	private static final Logger LOG = LoggerFactory.getLogger(SalesOrderEntryRequestPopulator.class);

	@Resource(name = "productFacade")
	private ProductFacade productFacade;

	@Resource(name = "i18nService")
	private I18NService i18nService;

	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Override
	public void populate(final SalesOrderProviderContainer source, final LineEntriesRequest target) throws ConversionException
	{
		if (source != null && target != null)
		{
			populateOrderEntries(source, target);
		}
	}

	protected void populateOrderEntries(final SalesOrderProviderContainer source, final LineEntriesRequest target)
	{
		if (source.getOrder() != null && !CollectionUtils.isEmpty(source.getOrder().getEntries()))
		{
			final AldahraERPSalesOrderServiceProviderModel provider = source.getProvider();
			final List<LineEntryRequest> entries = new ArrayList<>();
			source.getOrder().getEntries().stream()
					.filter(entry -> entry != null && entry.getProduct() != null && entry.getQuantity() > 0).forEach(sourceEntry -> {
						final LineEntryRequest entry = new LineEntryRequest();
						LOG.info("SalesOrderEntryRequestPopulator: order code: {}, entry PK: {}, product is null: {}",
								source.getOrder().getCode(), sourceEntry.getPk(), sourceEntry.getProduct() == null);
						entry.setProductCode(sourceEntry.getProduct().getCode());
						entry.setProductName(sourceEntry.getProduct().getName());
						entry.setQuantity(String.valueOf(sourceEntry.getQuantity()));
						entry.setGorssPrice(String.valueOf(calculateEntryPriceWithoutTaxes(sourceEntry)));
						entry.setUnitOfMeasure(getUnitOfMeasure(sourceEntry, provider));
						entry.setEntryShippingDate(getFormattedDate(
								source.getOrder().getTimeSlotInfo() != null ? source.getOrder().getTimeSlotInfo().getStartDate()
										: source.getOrder().getCreationtime()));
						entry.setEntryDiscount(String.valueOf(calculateEntryDiscount(sourceEntry)));
						entries.add(entry);
					});
			target.setEntries(entries);
		}
	}

	protected double calculateEntryDiscount(final AbstractOrderEntryModel entry)
	{
		double discountsVal = 0;

		for (final DiscountValue discount : entry.getDiscountValues())
		{
			discountsVal += discount.getValue() * entry.getQuantity();
		}
		final double totalPriceWithoutDiscount = entry.getBasePrice() * entry.getQuantity();
		final double totalPriceWithDiscount = totalPriceWithoutDiscount - discountsVal;
		final double value = getPercentageValue(totalPriceWithDiscount, discountsVal);
		return floorPrice(value, 2);
	}

	private double getPercentageValue(final double priceAfterDiscount, final double totalDiscount)
	{
		final double priceBeforeDiscount = priceAfterDiscount + totalDiscount;
		if (priceBeforeDiscount == 0)
		{
			return 0.0;
		}

		final double percentage = totalDiscount / priceBeforeDiscount * 100;

		return floorPrice(percentage, 2);
	}

	protected double calculateEntryPriceWithoutTaxes(final AbstractOrderEntryModel entry)
	{
		final double taxPercentage = CollectionUtils.isEmpty(entry.getTaxValues()) ? 0.0
				: entry.getTaxValues().stream().mapToDouble(tax -> tax.getValue()).sum();
		final double priceWithTax = entry.getBasePrice() == null ? 0.0 : entry.getBasePrice().doubleValue();
		return calculateEntryPriceWithoutTaxes(priceWithTax, taxPercentage);
	}

	protected double calculateEntryPriceWithoutTaxes(final double priceWithTax, final double taxPercentage)
	{
		final double priceWithoutTax = (priceWithTax * 100) / (100 + taxPercentage);
		return roundPrice(priceWithoutTax, 2);
	}

	private double floorPrice(final double price, final int digits)
	{
		final double digitsWeight = Math.pow(10, digits);
		return Math.floor(price * digitsWeight) / digitsWeight;
	}

	private double roundPrice(final double price, final int digits)
	{
		final double digitsWeight = Math.pow(10, digits);
		return Math.round(price * digitsWeight) / digitsWeight;
	}

	/**
	 *
	 */
	private String getUnitOfMeasure(final AbstractOrderEntryModel source, final AldahraERPSalesOrderServiceProviderModel provider)
	{
		if (source != null && source.getProduct() != null)
		{
			if (StringUtils.isNotBlank(source.getProduct().getUnitOfMeasureCode()))
			{
				return source.getProduct().getUnitOfMeasureCode();
			}
			if (CollectionUtils.isEmpty(provider.getUnitsMapping()))
			{
				return provider.getDefaultUnitOfMeasure();
			}
			final String uom = getUnitOfMeasureByLocaleCode("en", source.getProduct());

			final Optional<UnitOfMesureMappingModel> unitMap = provider.getUnitsMapping().stream()
					.filter(unit -> unit.getUomKey().equals(uom)).findAny();
			return unitMap.isPresent() ? unitMap.get().getUomValue() : provider.getDefaultUnitOfMeasure();
		}
		return provider.getDefaultUnitOfMeasure();
	}

	private String getUnitOfMeasureByLocaleCode(final String localeCode, final ProductModel productModel)
	{
		if (productModel == null)
		{
			return null;
		}
		final Locale currentLocale = i18nService.getCurrentLocale();
		i18nService.setCurrentLocale(Locale.forLanguageTag(localeCode));

		ProductData product = null;

		try
		{
			product = productFacade.getProductForOptions(productModel, null);

		}
		catch (UnknownIdentifierException | AmbiguousIdentifierException e)
		{
			return null;
		}
		i18nService.setCurrentLocale(currentLocale);
		return product != null ? product.getUnitOfMeasure() : null;
	}

	/**
	 *
	 */
	private String getFormattedDate(final Date creationtime)
	{
		final SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		return format.format(creationtime);
	}

}
