/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.erp.populator;

import de.hybris.platform.commerceservices.constants.CommerceServicesConstants;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.couponservices.model.AbstractCouponModel;
import de.hybris.platform.couponservices.services.CouponService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.util.DiscountValue;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.logging.log4j.util.Strings;
import org.fest.util.Collections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aldahra.aldahraerpclientservices.erp.impl.entries.LineEntriesRequest;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.LineEntryRequest;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.SalesOrderProviderContainer;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.SalesOrderRequest;
import com.aldahra.aldahraerpclientwebservices.model.AldahraERPSalesOrderServiceProviderModel;
import com.aldahra.aldahratimeslot.model.TimeSlotInfoModel;
import com.google.common.math.DoubleMath;


/**
 * @author monzer
 */
public class SalesOrderRequestPopulator implements Populator<SalesOrderProviderContainer, SalesOrderRequest>
{

	private static final Logger LOG = LoggerFactory.getLogger(SalesOrderRequestPopulator.class);

	private static final double EPSILON = 0.01d;

	@Resource(name = "salesOrderLinesRequestConverter")
	private Converter<SalesOrderProviderContainer, LineEntriesRequest> salesOrderEntryConverter;

	@Resource(name = "couponService")
	private CouponService couponService;


	@Override
	public void populate(final SalesOrderProviderContainer source, final SalesOrderRequest target) throws ConversionException
	{
		if (source != null && target != null && source.getOrder() != null && source.getProvider() != null)
		{
			populateStandardAttributes(source, target);
			populateSalesOrderEntries(source, target);
		}
	}

	protected void populateStandardAttributes(final SalesOrderProviderContainer source, final SalesOrderRequest target)
	{
		final OrderModel order = source.getOrder();
		final AldahraERPSalesOrderServiceProviderModel provider = source.getProvider();
		target.setSalesOrderSite(provider.getSalesOrderSiteCode());
		target.setOrderType(provider.getSalesOrderType());

		populatePaymentAndDelivery(target, order, provider);

		target.setOrderCreationDate(getFormattedDate(order.getCreationtime()));
		target.setOrderCode(order.getCode());
		target.setShippingSite(provider.getShipSiteCode());
		target.setCurrency(order.getCurrency().getIsocode());
		final TimeSlotInfoModel timeSlotInfo = order.getTimeSlotInfo();
		target.setShippingDate(getFormattedDate(timeSlotInfo != null ? timeSlotInfo.getStartDate() : order.getCreationtime()));
		setOrderDiscounts(source, target);
		target.setTimeslotInfo(getFormattedTimeSlotStartTime(timeSlotInfo)); // [Monzer]: timeslot time should be in HH:MM format
		target.setOrderCreationTimestamp(order.getDate() == null ? "" : getFormattedTime(order.getDate()));
	}

	/**
	 *
	 */
	private void setOrderDiscounts(final SalesOrderProviderContainer source, final SalesOrderRequest target)
	{
		final OrderModel order = source.getOrder();
		// if Marketing Coupon exists no discount will be applied from anywhere else, so the discount will be 0
		if (getMarketingCoupon(order) != null)
		{
			target.setOrderDiscount(String.valueOf(0.0f));
			return;
		}
		target.setOrderDiscount(String.valueOf(getOrderDiscounts(order)));
	}

	private String getFormattedTimeSlotStartTime(final TimeSlotInfoModel timeSlotInfo)
	{
		final String startTime = timeSlotInfo != null ? timeSlotInfo.getStart().replace(":", "") : "";
		return startTime.length() == 3 ? "0" + startTime : startTime;
	}

	private void populatePaymentAndDelivery(final SalesOrderRequest target, final OrderModel order,
			final AldahraERPSalesOrderServiceProviderModel provider)
	{
		if (order.getPaymentMode() == null)
		{
			return;
		}
		switch (order.getPaymentMode().getCode())
		{
			case "card":
			case "apple":
			case "continue":
				target.setCustomer(provider.getCardCustomerCode());
				target.setPaymentTerms(provider.getPickupPaymentType());
				break;
			case "cod":
				target.setCustomer(provider.getCodCustomerCode());
				target.setPaymentTerms(provider.getCodPaymentType());
				break;
			case "ccod":
				target.setCustomer(provider.getPickupCustomerCode());
				target.setPaymentTerms(provider.getPickupPaymentType());
				break;
			case "pis":
				target.setCustomer("U15799");
				target.setPaymentTerms("UCDD");
				break;
			default:
				return;
		}
		target.setOrderShipmentType(
				order.getDeliveryMode() != null && "pickup".equalsIgnoreCase(order.getDeliveryMode().getCode()) ? "1" : "2");
	}


	protected Double getOrderDiscounts(final OrderModel order)
	{
		final List<DiscountValue> discountList = order.getGlobalDiscountValues(); // discounts on the cart itself
		if (discountList == null || discountList.isEmpty())
		{
			return 0.0d;
		}

		return discountList.stream().filter(e -> !CommerceServicesConstants.QUOTE_DISCOUNT_CODE.equals(e.getCode()))
				.map(DiscountValue::getAppliedValue).filter(e -> DoubleMath.fuzzyCompare(e, 0, EPSILON) > 0).reduce(Double::sum)
				.orElse(0.0d);
	}

	/**
	 * @return
	 *
	 */
	private LineEntryRequest getOrderCouponDiscounts(final OrderModel order,
			final AldahraERPSalesOrderServiceProviderModel provider)
	{
		if (Collections.isEmpty(order.getAppliedCouponCodes()))
		{
			return null;
		}

		if (Collections.isEmpty(order.getAllPromotionResults()))
		{
			return null;
		}

		final AbstractCouponModel coupon = getMarketingCoupon(order);

		if (coupon == null)
		{
			return null;
		}

		final String productName = "COUPON DISCOUNT";
		final String yeleDimension = "UA-COP-MRTKNG";
		final String unitOfMeasure = "UN";
		final String quantity = "1";
		final String discount = "0.0";

		final LineEntryRequest line = new LineEntryRequest();
		final String formattedDate = getFormattedDate(
				order.getTimeSlotInfo() != null ? order.getTimeSlotInfo().getStartDate() : order.getCreationtime());

		final String productCode = provider == null || Strings.isBlank(provider.getStoreCreditSKU()) ? "E900083AE"
				: provider.getStoreCreditSKU();

		line.setProductCode(productCode);
		line.setProductName(productName);
		line.setUnitOfMeasure(unitOfMeasure);
		line.setQuantity(quantity);
		// NOTE(Husam): As guaranteed by the customer (Only 1 discount can be applied to an order) thus, the discount came from the coupon
		line.setGorssPrice(String.valueOf(getOrderDiscounts(order) * -1));
		line.setEntryShippingDate(formattedDate);
		line.setEntryDiscount(discount);
		line.setYeleDimension(yeleDimension);

		return line;


	}

	private AbstractCouponModel getMarketingCoupon(final OrderModel order)
	{
		if (order == null || order.getAppliedCouponCodes() == null || Collections.isEmpty(order.getAppliedCouponCodes()))
		{
			return null;
		}
		final Set<AbstractCouponModel> coupons = order.getAppliedCouponCodes().stream()
				.map(e -> getCouponService().getCouponForCode(e)).map(e -> e.orElseGet(null)).filter(Objects::nonNull)
				.filter(e -> e.isMarketingFees()).collect(Collectors.toSet());

		if (coupons.size() == 0)
		{
			LOG.info("No Marketing Coupon found on order {}", order.getCode());
			return null;
		}


		if (coupons.size() > 1)
		{
			LOG.warn("Multilpe \"Marketing Fee\" Coupon applied to the same order, Taking first one");
			LOG.warn("Coupon Codes [{}]", String.join(", ", coupons.stream().map(e -> e.getCouponId()).collect(Collectors.toSet())));
			LOG.warn("Order[{}]", order.getCode());
		}

		return coupons.stream().findFirst().get();
	}

	protected void populateSalesOrderEntries(final SalesOrderProviderContainer source, final SalesOrderRequest target)
	{
		final LineEntriesRequest entries = salesOrderEntryConverter.convert(source);
		final OrderModel order = source.getOrder();
		final LineEntryRequest storeCreditLine = getStoreCreditLine(order, source.getProvider());
		if (storeCreditLine != null)
		{
			entries.getEntries().add(storeCreditLine);
		}

		final LineEntryRequest marketingFeeCoupon = getOrderCouponDiscounts(order, source.getProvider());
		if (marketingFeeCoupon != null)
		{
			entries.getEntries().add(marketingFeeCoupon);
		}


		target.setEntryLines(entries.getEntries());
	}

	private LineEntryRequest getStoreCreditLine(final OrderModel order, final AldahraERPSalesOrderServiceProviderModel provider)
	{
		final double storeCreditValue = order.getStoreCreditAmount().doubleValue();
		if (storeCreditValue == 0.0d)
		{
			return null;
		}

		final LineEntryRequest line = new LineEntryRequest();
		final String formattedDate = getFormattedDate(
				order.getTimeSlotInfo() != null ? order.getTimeSlotInfo().getStartDate() : order.getCreationtime());

		final String productCode = provider == null || Strings.isBlank(provider.getStoreCreditSKU()) ? "E900083AE"
				: provider.getStoreCreditSKU();

		final String productName = "STORE CREDIT";
		final String unitOfMeasure = "UN";
		final String quantity = "1";
		final String discount = "0.0";
		final String yeleDimension = "UA-STR-CREDIT";

		line.setProductCode(productCode);
		line.setProductName(productName);
		line.setUnitOfMeasure(unitOfMeasure);
		line.setQuantity(quantity);
		line.setGorssPrice(String.valueOf(storeCreditValue * -1));
		line.setEntryShippingDate(formattedDate);
		line.setEntryDiscount(discount);
		line.setYeleDimension(yeleDimension);
		return line;
	}

	protected double getProductsDiscountsAmount(final AbstractOrderModel source)
	{
		double discounts = 0.0d;

		final List<AbstractOrderEntryModel> entries = source.getEntries();
		if (entries != null)
		{
			for (final AbstractOrderEntryModel entry : entries)
			{
				final List<DiscountValue> discountValues = entry.getDiscountValues();
				if (discountValues != null)
				{
					for (final DiscountValue dValue : discountValues)
					{
						discounts += dValue.getAppliedValue();
					}
				}
			}
		}
		return discounts;
	}


	private String getFormattedDate(final Date creationtime)
	{
		final SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		return format.format(creationtime);
	}

	private String getFormattedTime(final Date creationDate)
	{
		return creationDate.toInstant().atZone(ZoneId.of("Asia/Dubai")).toLocalDateTime()
				.format(DateTimeFormatter.ofPattern("HHmm"));
	}

	/**
	 * @return the couponService
	 */
	public CouponService getCouponService()
	{
		return couponService;
	}

}
