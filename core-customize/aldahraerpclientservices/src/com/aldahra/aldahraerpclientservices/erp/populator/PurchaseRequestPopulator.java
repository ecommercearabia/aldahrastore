/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.erp.populator;

import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aldahra.aldahraerpclientservices.erp.impl.entries.ProductPurchaseLineEntry;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.PurchaseRequest;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.PurchaseRequestProviderContainer;
import com.aldahra.aldahraerpclientwebservices.model.AldahraERPProductPurchaseRequestServiceProviderModel;


/**
 *
 */
public class PurchaseRequestPopulator implements Populator<PurchaseRequestProviderContainer, PurchaseRequest>
{

	private static final Logger LOG = LoggerFactory.getLogger(PurchaseRequestPopulator.class);

	@Resource(name = "commercePriceService")
	private CommercePriceService commercePriceService;

	@Resource(name = "productFacade")
	private ProductFacade productFacade;

	@Resource(name = "i18nService")
	private I18NService i18nService;

	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Override
	public void populate(final PurchaseRequestProviderContainer source, final PurchaseRequest target) throws ConversionException
	{
		if (source != null && target != null && source.getProduct() != null && source.getProvider() != null)
		{
			populatePurchaseRequestHeaderAttributes(source, target);
			populatePurchaseRequestEntriesAttributes(source, target);
		}
	}

	/**
	 *
	 */
	private void populatePurchaseRequestHeaderAttributes(final PurchaseRequestProviderContainer source,
			final PurchaseRequest target)
	{
		final ProductModel product = source.getProduct();
		final AldahraERPProductPurchaseRequestServiceProviderModel provider = source.getProvider();
		target.setDate(getFormattedDate(new Date()));
		target.setRequestNumber("");
		target.setRequestUser(provider.getRequestUserCode());
		target.setSite(provider.getSiteCode());
	}

	/**
	 *
	 */
	private void populatePurchaseRequestEntriesAttributes(final PurchaseRequestProviderContainer source,
			final PurchaseRequest target)
	{
		final ProductModel product = source.getProduct();
		final AldahraERPProductPurchaseRequestServiceProviderModel provider = source.getProvider();
		final ProductPurchaseLineEntry productLine = new ProductPurchaseLineEntry();
		productLine.setProduct(product.getCode());
		productLine.setDescription(product.getName());
		final PriceInformation price = commercePriceService.getWebPriceForProduct(product);
		productLine.setGrossPrice(
				price == null || price.getPriceValue() == null ? "" : String.valueOf(price.getPriceValue().getValue()));
		productLine.setPurchaseUnit(StringUtils.isNotBlank(product.getPurchaseUnit()) ? product.getPurchaseUnit()
				: provider.getDefaultPurchaseUnit());
		productLine.setQuantity(provider.getDefaultPurchaseQuantity());
		productLine.setReceivingDate(getFormattedDate(new Date()));
		productLine.setReceivingSite(provider.getReceivingSiteCode());
		productLine.setSupplier(provider.getSupplierCode());
		target.setProductLine(productLine);
	}


	/**
	 *
	 */
	private String getFormattedDate(final Date creationtime)
	{
		final SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		final String formattedDate = format.format(creationtime);
		return formattedDate;
	}

}
