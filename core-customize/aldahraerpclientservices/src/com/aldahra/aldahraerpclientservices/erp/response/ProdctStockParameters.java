package com.aldahra.aldahraerpclientservices.erp.response;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import com.aldahra.aldahraerpclientservices.erp.impl.entries.Field;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.Tab;


@XmlRootElement(name = "PARAM")
@XmlSeeAlso(
{ Tab.class })
public class ProdctStockParameters implements Serializable
{

	private List<Tab> tabs;
	private List<Field> fields;

	@XmlElement(name = "TAB", required = true)
	public List<Tab> getTabs()
	{
		return tabs;
	}

	public void setTabs(final List<Tab> tabs)
	{
		this.tabs = tabs;
	}

	@XmlElement(name = "FLD", required = true)
	public List<Field> getFields()
	{
		return fields;
	}

	public void setFields(final List<Field> fields)
	{
		this.fields = fields;
	}

	@Override
	public String toString()
	{
		return "ProdctStockParameters [tabs=" + tabs + "]";
	}

}
