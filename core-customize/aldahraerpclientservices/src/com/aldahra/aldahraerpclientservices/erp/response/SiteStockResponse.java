/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.erp.response;

import java.io.Serializable;
import java.util.List;

import com.aldahra.aldahraerpclientservices.erp.impl.entries.SiteStockLines;


/**
 *
 */
public class SiteStockResponse implements Serializable
{

	private String siteStockResponse;
	private boolean hasStock;
	private ResponseMessage response;
	private String productCode;
	private List<SiteStockLines> stockLines;

	public SiteStockResponse()
	{

	}

	public String getSiteStockResponse()
	{
		return siteStockResponse;
	}

	public void setSiteStockResponse(final String siteStockResponse)
	{
		this.siteStockResponse = siteStockResponse;
	}

	public boolean isHasStock()
	{
		return hasStock;
	}

	public void setHasStock(final boolean hasStock)
	{
		this.hasStock = hasStock;
	}

	public ResponseMessage getResponse()
	{
		return response;
	}

	public void setResponse(final ResponseMessage response)
	{
		this.response = response;
	}

	public List<SiteStockLines> getStockLines()
	{
		return stockLines;
	}

	public void setStockLines(final List<SiteStockLines> stockLines)
	{
		this.stockLines = stockLines;
	}

	public String getProductCode()
	{
		return productCode;
	}

	public void setProductCode(final String productCode)
	{
		this.productCode = productCode;
	}


}
