/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.erp.response.enums;

/**
 *
 */
public enum Status
{
	SUCCESS, FAILURE;
}
