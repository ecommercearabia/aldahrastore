/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.erp.response;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import com.aldahra.aldahraerpclientservices.erp.response.enums.Status;


/**
 *
 */
public class ResponseMessage implements Serializable
{

	private List<ResponseMetadata> message;
	private String response;
	private Status status;

	/**
	 * @return the message
	 */
	public List<ResponseMetadata> getMessage()
	{
		return message;
	}

	/**
	 * @param message
	 *           the message to set
	 */
	public void setMessage(final List<ResponseMetadata> message)
	{
		this.message = message;
	}

	/**
	 * @return the response
	 */
	public String getResponse()
	{
		return response;
	}

	/**
	 * @param response
	 *           the response to set
	 */
	public void setResponse(final String response)
	{
		this.response = response;
	}

	/**
	 * @return the status
	 */
	public Status getStatus()
	{
		return status;
	}

	/**
	 * @param status
	 *           the status to set
	 */
	public void setStatus(final Status status)
	{
		this.status = status;
	}


	private String getMessages(final List<ResponseMetadata> messages)
	{
		return messages.stream().map(message -> message.toString()).collect(Collectors.joining(", "));
	}

	@Override
	public String toString()
	{
		return "{\n\tmessage:" + getMessages(message) + ",\n\t response:" + response + ",\n\t status:" + status + "\n}";
	}

}
