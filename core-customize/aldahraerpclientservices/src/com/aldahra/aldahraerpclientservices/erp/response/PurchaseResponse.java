/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.erp.response;

import java.io.Serializable;


/**
 *
 */
public class PurchaseResponse implements Serializable
{

	private String purchaseCreationResponse;
	private boolean creationSuccess;
	private ResponseMessage messages;
	private String productCode;
	private String request;
	private String response;

	/**
	 * @return the salesOrderResponse
	 */
	public String getPurchaseCreationResponse()
	{
		return purchaseCreationResponse;
	}

	/**
	 * @param salesOrderResponse
	 *           the salesOrderResponse to set
	 */
	public void setPurchaseCreationResponse(final String purchaseCreationResponse)
	{
		this.purchaseCreationResponse = purchaseCreationResponse;
	}

	/**
	 * @return the creationSuccess
	 */
	public boolean getCreationSuccess()
	{
		return creationSuccess;
	}

	/**
	 * @param creationSuccess
	 *           the creationSuccess to set
	 */
	public void setCreationSuccess(final boolean creationSuccess)
	{
		this.creationSuccess = creationSuccess;
	}


	/**
	 * @return the productCode
	 */
	public String getProductCode()
	{
		return productCode;
	}

	/**
	 * @param productCode
	 *           the productCode to set
	 */
	public void setProductCode(final String productCode)
	{
		this.productCode = productCode;
	}

	/**
	 * @return the request
	 */
	public String getRequest()
	{
		return request;
	}

	/**
	 * @param request
	 *           the request to set
	 */
	public void setRequest(final String request)
	{
		this.request = request;
	}

	@Override
	public String toString()
	{
		return "{\n\tpurchaseCreationResponse:" + purchaseCreationResponse + ",\n\t creationSuccess:" + creationSuccess
				+ ",\n\t messages:" + messages + ",\n\t productCode:" + productCode + ",\n\t request:" + request + ",\n\t response:"
				+ response + "\n}";
	}

	/**
	 * @return the messages
	 */
	public ResponseMessage getMessages()
	{
		return messages;
	}

	/**
	 * @param messages
	 *           the messages to set
	 */
	public void setMessages(final ResponseMessage messages)
	{
		this.messages = messages;
	}

	/**
	 * @return the response
	 */
	public String getResponse()
	{
		return response;
	}

	/**
	 * @param response
	 *           the response to set
	 */
	public void setResponse(final String response)
	{
		this.response = response;
	}

}
