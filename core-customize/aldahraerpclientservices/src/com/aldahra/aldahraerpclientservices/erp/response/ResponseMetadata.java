/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.erp.response;

import com.aldahra.aldahraerpclientservices.erp.response.enums.MessageType;


/**
 *
 */
public class ResponseMetadata
{

	private String message;
	private MessageType type;

	/**
	 * @return the message
	 */
	public String getMessage()
	{
		return message;
	}

	/**
	 * @param message
	 *           the message to set
	 */
	public void setMessage(final String message)
	{
		this.message = message;
	}

	/**
	 * @return the type
	 */
	public MessageType getType()
	{
		return type;
	}

	/**
	 * @param type
	 *           the type to set
	 */
	public void setType(final MessageType type)
	{
		this.type = type;
	}

	@Override
	public String toString()
	{
		return "{\n\tmessage:" + message + ",\n\t type:" + type + "\n}";
	}

}
