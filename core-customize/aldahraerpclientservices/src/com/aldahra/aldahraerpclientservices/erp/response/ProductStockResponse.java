package com.aldahra.aldahraerpclientservices.erp.response;

import java.io.Serializable;


/**
 * @author monzer
 */
public class ProductStockResponse implements Serializable{

	private String productCode;
	private int stock;
	private ResponseMessage response;
	private boolean hasStock;

	/**
	 * @return the productCode
	 */
	public String getProductCode()
	{
		return productCode;
	}

	/**
	 * @param productCode
	 *           the productCode to set
	 */
	public void setProductCode(final String productCode)
	{
		this.productCode = productCode;
	}

	/**
	 * @return the stock
	 */
	public int getStock()
	{
		return stock;
	}

	/**
	 * @param stock
	 *           the stock to set
	 */
	public void setStock(final int stock)
	{
		this.stock = stock;
	}

	/**
	 * @return the response
	 */
	public ResponseMessage getResponse()
	{
		return response;
	}

	/**
	 * @param response
	 *           the response to set
	 */
	public void setResponse(final ResponseMessage response)
	{
		this.response = response;
	}

	/**
	 * @return the hasStock
	 */
	public boolean isHasStock()
	{
		return hasStock;
	}

	/**
	 * @param hasStock
	 *           the hasStock to set
	 */
	public void setHasStock(final boolean hasStock)
	{
		this.hasStock = hasStock;
	}

	@Override
	public String toString()
	{
		return "{\n\tproductCode:" + productCode + ",\n\t stock:" + stock + ",\n\t response:" + response + ",\n\t hasStock:"
				+ hasStock + "\n}";
	}

}
