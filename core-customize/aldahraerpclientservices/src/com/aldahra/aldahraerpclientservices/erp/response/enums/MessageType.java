/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.erp.response.enums;

/**
 * @author monzer
 */
public enum MessageType
{
	SUCCESS("1"), WARNING("2"), ERROR("3"), OTHER("OTHER");

	private String code;

	/**
	 *
	 */
	private MessageType(final String code)
	{
		this.code = code;
	}

	/**
	 * @return the code
	 */
	public String getCode()
	{
		return code;
	}

	public static MessageType getMessageByCode(final String code)
	{
		for (final MessageType data : values())
		{
			if (code.equals(data.getCode()))
			{
				return data;
			}
		}
		return OTHER;
	}

}
