/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.erp.response;

import java.io.Serializable;


/**
 *
 */
public class SalesOrderResponse implements Serializable
{

	private String salesOrderResponse;
	private boolean creationSuccess;
	private boolean sentSuccess;
	private ResponseMessage response;

	/**
	 * @return the salesOrderResponse
	 */
	public String getSalesOrderResponse()
	{
		return salesOrderResponse;
	}

	/**
	 * @param salesOrderResponse
	 *           the salesOrderResponse to set
	 */
	public void setSalesOrderResponse(final String salesOrderResponse)
	{
		this.salesOrderResponse = salesOrderResponse;
	}

	/**
	 * @return the creationSuccess
	 */
	public boolean getCreationSuccess()
	{
		return creationSuccess;
	}

	/**
	 * @param creationSuccess
	 *           the creationSuccess to set
	 */
	public void setCreationSuccess(final boolean creationSuccess)
	{
		this.creationSuccess = creationSuccess;
	}

	/**
	 * @return the sentSuccess
	 */
	public boolean getSentSuccess()
	{
		return sentSuccess;
	}

	/**
	 * @param sentSuccess
	 *           the sentSuccess to set
	 */
	public void setSentSuccess(final boolean sentSuccess)
	{
		this.sentSuccess = sentSuccess;
	}

	/**
	 * @return the response
	 */
	public ResponseMessage getResponse()
	{
		return response;
	}

	/**
	 * @param response
	 *           the response to set
	 */
	public void setResponse(final ResponseMessage response)
	{
		this.response = response;
	}

	@Override
	public String toString()
	{
		return "SalesOrderResponse [salesOrderResponse=" + salesOrderResponse + ", creationSuccess=" + creationSuccess
				+ ", sentSuccess=" + sentSuccess + ", response=" + response + "]";
	}



}
