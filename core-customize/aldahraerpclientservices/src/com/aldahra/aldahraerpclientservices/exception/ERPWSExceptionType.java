/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.exception;

/**
 * @author monzer
 */
public enum ERPWSExceptionType
{
	BAD_REQUEST("Bad request due to: "), SERVER_ERROR("Internal server error due to: "), MARSHALLING_ERROR(
			"Error during the marshalling process"), UNMARSHALLING_ERROR("Error during the unmarshalling process"), INVALID_PROVIDER(
					"ERPWS Provider is not provided or not active"), INVALID_REQUEST(
							"Error in the request"), INVALID_RESPONSE("Error response");

	private String errorMessage;

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage()
	{
		return errorMessage;
	}

	/**
	 *
	 */
	private ERPWSExceptionType(final String errorMessage)
	{
		this.errorMessage = errorMessage;
	}



}
