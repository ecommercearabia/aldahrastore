/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.exception;

/**
 * @author monzer
 */
public class ERPWSServiceException extends Exception
{

	private final ERPWSExceptionType type;
	private final String message;

	/**
	 *
	 */
	public ERPWSServiceException(final ERPWSExceptionType type, final String message)
	{
		super(type.getErrorMessage() + " " + message);
		this.type = type;
		this.message = message;
	}

	/**
	 *
	 */
	public ERPWSServiceException(final ERPWSExceptionType type)
	{
		super(type.getErrorMessage());
		this.message = type.getErrorMessage();
		this.type = type;
	}

	/**
	 * @return the type
	 */
	public ERPWSExceptionType getType()
	{
		return type;
	}

	/**
	 * @return the message
	 */
	@Override
	public String getMessage()
	{
		return message;
	}

}
