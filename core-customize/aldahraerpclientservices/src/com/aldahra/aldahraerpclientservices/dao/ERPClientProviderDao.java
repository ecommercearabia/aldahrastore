/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.dao;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.aldahra.aldahraerpclientwebservices.model.AldahraERPClientWebServiceProviderModel;


/**
 * @author monzer
 */
public interface ERPClientProviderDao
{

	Optional<AldahraERPClientWebServiceProviderModel> get(String code);

	Optional<AldahraERPClientWebServiceProviderModel> getActive(String baseStoreUid);

	Optional<AldahraERPClientWebServiceProviderModel> getActive(BaseStoreModel baseStore);

	Optional<AldahraERPClientWebServiceProviderModel> getActiveByCurrentBaseStore();

}
