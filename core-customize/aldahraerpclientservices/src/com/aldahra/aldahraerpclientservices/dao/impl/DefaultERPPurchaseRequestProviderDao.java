/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.dao.impl;

import com.aldahra.aldahraerpclientservices.dao.ERPClientProviderDao;
import com.aldahra.aldahraerpclientwebservices.model.AldahraERPProductPurchaseRequestServiceProviderModel;


/**
 * @author monzer
 */
public class DefaultERPPurchaseRequestProviderDao extends DefaultERPClientProviderDao implements ERPClientProviderDao
{

	/**
	 *
	 */
	public DefaultERPPurchaseRequestProviderDao()
	{
		super(AldahraERPProductPurchaseRequestServiceProviderModel._TYPECODE);
	}

	@Override
	protected String getModelName()
	{
		return AldahraERPProductPurchaseRequestServiceProviderModel._TYPECODE;
	}
}
