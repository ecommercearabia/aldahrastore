/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.dao.impl;

import com.aldahra.aldahraerpclientservices.dao.ERPClientProviderDao;
import com.aldahra.aldahraerpclientwebservices.model.AldahraERPSalesOrderServiceProviderModel;

/**
 *
 */
public class DefaultERPSalesOrderProviderDao extends DefaultERPClientProviderDao implements ERPClientProviderDao
{

	/**
	 *
	 */
	public DefaultERPSalesOrderProviderDao()
	{
		super(AldahraERPSalesOrderServiceProviderModel._TYPECODE);
	}

	@Override
	protected String getModelName()
	{
		return AldahraERPSalesOrderServiceProviderModel._TYPECODE;
	}

}
