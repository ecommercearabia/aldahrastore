/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.aldahra.aldahraerpclientservices.dao.ERPClientProviderDao;
import com.aldahra.aldahraerpclientwebservices.model.AldahraERPClientWebServiceProviderModel;
import com.google.common.base.Preconditions;


/**
 * @author monzer
 */
public abstract class DefaultERPClientProviderDao extends DefaultGenericDao<AldahraERPClientWebServiceProviderModel>
		implements ERPClientProviderDao
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultERPClientProviderDao.class);

	/** The base store service. */
	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	private static final String CODE_MUSTN_T_BE_NULL = "ERP Provider code must not be null";

	private static final String SERVICE_NAME = "";

	private static final String ACTIVE = AldahraERPClientWebServiceProviderModel.ACTIVE;

	private static final String STORES = AldahraERPClientWebServiceProviderModel.STORES;

	private static final String CODE = AldahraERPClientWebServiceProviderModel.CODE;
	/**
	 *
	 */
	public DefaultERPClientProviderDao(final String typeCode)
	{
		super(typeCode);
	}

	protected abstract String getModelName();

	@Override
	public Optional<AldahraERPClientWebServiceProviderModel> get(final String code)
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(code), CODE_MUSTN_T_BE_NULL);
		final Map<String, Object> params = new HashMap();
		params.put(CODE, code);
		final List<AldahraERPClientWebServiceProviderModel> find = find(params);
		final Optional<AldahraERPClientWebServiceProviderModel> findFirst = find.stream().findFirst();

		return findFirst.isPresent() ? Optional.ofNullable(findFirst.get()) : Optional.ofNullable(null);
	}

	@Override
	public Optional<AldahraERPClientWebServiceProviderModel> getActive(final String baseStoreUid)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(baseStoreUid), "Base Store UID is null");
		return getActive(baseStoreService.getBaseStoreForUid(baseStoreUid));
	}

	@Override
	public Optional<AldahraERPClientWebServiceProviderModel> getActive(final BaseStoreModel baseStoreModel)
	{
		Preconditions.checkArgument(baseStoreModel != null, "baseStoreModel must not be null");
		final Optional<Collection<AldahraERPClientWebServiceProviderModel>> find = find(Arrays.asList(baseStoreModel), Boolean.TRUE,
				1);

		return find.isPresent() && !find.get().isEmpty() ? Optional.ofNullable(find.get().iterator().next())
				: Optional.ofNullable(null);
	}

	@Override
	public Optional<AldahraERPClientWebServiceProviderModel> getActiveByCurrentBaseStore()
	{
		return getActive(baseStoreService.getCurrentBaseStore());
	}

	protected Optional<Collection<AldahraERPClientWebServiceProviderModel>> find(final List<BaseStoreModel> stores,
			final Boolean active,
			final Integer countRecords)
	{
		final Map<String, Object> params = new HashMap<>();
		final StringBuilder query = new StringBuilder();
		query.append("SELECT {erpp.PK} ");
		query.append("FROM ");
		query.append("{ ");
		query.append(getModelName()).append(" AS erpp ");

		if (!CollectionUtils.isEmpty(stores))
		{
			query.append("JOIN BaseStore2ERPClientProviders AS bserpp ON {bserpp.target}={erpp.PK} ");
			query.append("JOIN BaseStore AS bs ON {bserpp.source}={bs.PK} AND {bs.PK} IN (?stores) ");
			params.put(STORES, stores);
		}

		query.append("} ");

		int activeValue = 0;
		if (active == null || Boolean.TRUE.equals(active))
		{
			activeValue = 1;
		}
		query.append("WHERE {erpp.active}=?active ");
		params.put(ACTIVE, activeValue);

		query.append("ORDER BY {erpp.creationtime} DESC");

		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query.toString());
		flexibleSearchQuery.getQueryParameters().putAll(params);
		if (countRecords != null && countRecords > 0)
		{
			flexibleSearchQuery.setCount(countRecords);
		}

		final SearchResult<AldahraERPClientWebServiceProviderModel> result = getFlexibleSearchService().search(flexibleSearchQuery);

		return result != null ? Optional.ofNullable(result.getResult()) : Optional.ofNullable(Collections.emptyList());
	}

}
