/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.dao.impl;

import com.aldahra.aldahraerpclientservices.dao.ERPClientProviderDao;
import com.aldahra.aldahraerpclientwebservices.model.AldahraERPProductStockServiceProviderModel;


/**
 *
 */
public class DefaultERPProductStockProviderDao extends DefaultERPClientProviderDao implements ERPClientProviderDao
{

	/**
	 *
	 */
	public DefaultERPProductStockProviderDao()
	{
		super(AldahraERPProductStockServiceProviderModel._TYPECODE);
	}

	@Override
	protected String getModelName()
	{
		return AldahraERPProductStockServiceProviderModel._TYPECODE;
	}
}
