/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.service;

import de.hybris.platform.core.model.order.OrderModel;

import com.aldahra.aldahraerpclientwebservices.model.AldahraERPSalesOrderServiceProviderModel;



/**
 * @author monzer
 */
public interface AldahraERPClientSalesOrderService
{

	void createSalesOrder(OrderModel abstractOrder, AldahraERPSalesOrderServiceProviderModel providerModel);

}
