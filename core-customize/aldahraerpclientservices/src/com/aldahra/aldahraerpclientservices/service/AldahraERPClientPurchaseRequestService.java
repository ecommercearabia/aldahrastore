/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.service;

import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;
import java.util.Optional;

import com.aldahra.aldahraerpclientservices.erp.response.PurchaseResponse;
import com.aldahra.aldahraerpclientservices.exception.ERPWSServiceException;
import com.aldahra.aldahraerpclientwebservices.model.AldahraERPProductPurchaseRequestServiceProviderModel;


/**
 * @author monzer
 */
public interface AldahraERPClientPurchaseRequestService
{

	List<PurchaseResponse> sendPurchaseRequestForProductsUnderMinimumAmount(List<ProductModel> products,
			AldahraERPProductPurchaseRequestServiceProviderModel providerModel)
			throws ERPWSServiceException;

	Optional<PurchaseResponse> sendPurchaseRequestForProductsUnderMinimumAmount(ProductModel product,
			AldahraERPProductPurchaseRequestServiceProviderModel providerModel)
			throws ERPWSServiceException;

}
