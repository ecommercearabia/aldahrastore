/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.aldahra.aldahraerpclientservices.erp.ERPWSUpdateProductStockService;
import com.aldahra.aldahraerpclientservices.erp.ERPWSUpdateSiteStockService;
import com.aldahra.aldahraerpclientservices.erp.response.ProductStockResponse;
import com.aldahra.aldahraerpclientservices.erp.response.ResponseMessage;
import com.aldahra.aldahraerpclientservices.erp.response.ResponseMetadata;
import com.aldahra.aldahraerpclientservices.erp.response.SiteStockResponse;
import com.aldahra.aldahraerpclientservices.erp.response.enums.MessageType;
import com.aldahra.aldahraerpclientservices.exception.ERPWSExceptionType;
import com.aldahra.aldahraerpclientservices.exception.ERPWSServiceException;
import com.aldahra.aldahraerpclientservices.service.AldahraERPClientProductStockService;
import com.aldahra.aldahraerpclientwebservices.model.AldahraERPProductStockServiceProviderModel;
import com.google.common.base.Preconditions;


/**
 * @author monzer
 */
public class DefaultAldahraERPClientStockService implements AldahraERPClientProductStockService
{

	private static final String PROVIDER_IS_NULL = "Provider is null";
	private static final String PRODUCT_CODES_ARE_NULL = "Product codes are null";

	private static final Logger LOG = LoggerFactory.getLogger(DefaultAldahraERPClientStockService.class);


	@Resource(name = "updateProductStockService")
	private ERPWSUpdateProductStockService erpwsUpdateProductStockService;

	@Resource(name = "erpWSUpdateSiteStockService")
	private ERPWSUpdateSiteStockService erpWSUpdateSiteStockService;

	@Override
	public List<ProductStockResponse> getProductsStock(final List<String> productCodes,
			final AldahraERPProductStockServiceProviderModel providerModel) throws ERPWSServiceException
	{
		Preconditions.checkArgument(!CollectionUtils.isEmpty(productCodes), PRODUCT_CODES_ARE_NULL);
		Preconditions.checkArgument(providerModel != null, PROVIDER_IS_NULL);
		final List<ProductStockResponse> responses = new ArrayList<>();

		productCodes.stream().forEach(code -> {
			Optional<ProductStockResponse> response = Optional.empty();
			try
			{
				response = getProductStock(code, providerModel);
			}
			catch (final ERPWSServiceException e)
			{
				final ProductStockResponse res = new ProductStockResponse();
				res.setProductCode(code);
				final ResponseMessage responseMessage = new ResponseMessage();
				final ResponseMetadata metadata = new ResponseMetadata();
				metadata.setMessage(e.getMessage());
				metadata.setType(MessageType.ERROR);
				responseMessage.setMessage(Arrays.asList(metadata));
				res.setResponse(responseMessage);
				res.setHasStock(false);
				responses.add(res);
			}
			if (response.isPresent())
			{
				responses.add(response.get());
			}
		});
		return CollectionUtils.isEmpty(responses) ? Collections.emptyList() : responses;
	}

	@Override
	public Optional<ProductStockResponse> getProductStock(final String productCode,
			final AldahraERPProductStockServiceProviderModel providerModel) throws ERPWSServiceException
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(productCode), PRODUCT_CODES_ARE_NULL);
		Preconditions.checkArgument(providerModel != null, PROVIDER_IS_NULL);

		Optional<ProductStockResponse> stockResponse = Optional.empty();
		try
		{
			stockResponse = erpwsUpdateProductStockService.getProductStockData(productCode, providerModel.getUsername(),
					providerModel.getPassword(), providerModel.getServicePoolName(), providerModel.getStockPublicName(),
					providerModel.getStockSiteCode());
		}
		catch (final ERPWSServiceException e)
		{
			LOG.error("DefaultAldahraERPClientService: Error in stock service: {}", e.getMessage());
			throw new ERPWSServiceException(ERPWSExceptionType.INVALID_RESPONSE);
		}
		if (stockResponse.isEmpty())
		{
			LOG.error("Product Stock response is null!");
			throw new ERPWSServiceException(ERPWSExceptionType.INVALID_RESPONSE);
		}
		return stockResponse;
	}

	@Override
	public Optional<SiteStockResponse> getProductERPStock(final String productCode,
			final AldahraERPProductStockServiceProviderModel providerModel) throws ERPWSServiceException
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(productCode), PRODUCT_CODES_ARE_NULL);
		Preconditions.checkArgument(providerModel != null, PROVIDER_IS_NULL);

		Optional<SiteStockResponse> stockResponse = Optional.empty();
		try
		{
			stockResponse = erpWSUpdateSiteStockService.getSiteStockData(productCode, providerModel.getStockSiteCode(),
					providerModel.getUsername(), providerModel.getPassword(), providerModel.getServicePoolName(),
					providerModel.getStockPublicName());
		}
		catch (final ERPWSServiceException e)
		{
			LOG.error("DefaultAldahraERPClientService: Error in stock service: {}", e.getMessage());
			throw new ERPWSServiceException(ERPWSExceptionType.INVALID_RESPONSE);
		}
		if (stockResponse.isEmpty())
		{
			LOG.error("Product Stock response is null!");
			throw new ERPWSServiceException(ERPWSExceptionType.INVALID_RESPONSE);
		}
		return stockResponse;
	}

}
