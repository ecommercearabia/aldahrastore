/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.service.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.aldahra.aldahraerpclientservices.erp.ERPWSSalesOrderService;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.SalesOrderProviderContainer;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.SalesOrderRequest;
import com.aldahra.aldahraerpclientservices.erp.response.SalesOrderResponse;
import com.aldahra.aldahraerpclientservices.exception.ERPWSServiceException;
import com.aldahra.aldahraerpclientservices.service.AldahraERPClientSalesOrderService;
import com.aldahra.aldahraerpclientwebservices.model.AldahraERPSalesOrderServiceProviderModel;
import com.google.common.base.Preconditions;


/**
 * @author monzer
 */
public class DefaultAldahraERPClientSalesOrderService implements AldahraERPClientSalesOrderService
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultAldahraERPClientSalesOrderService.class);

	@Resource(name = "salesOrderRequestConverter")
	private Converter<SalesOrderProviderContainer, SalesOrderRequest> salesOrderRequestConverter;

	@Resource(name = "salesOrderService")
	private ERPWSSalesOrderService erpwsSalesOrderService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Override
	public void createSalesOrder(final OrderModel order, final AldahraERPSalesOrderServiceProviderModel providerModel)
	{
		Preconditions.checkArgument(providerModel != null, "Provider is null");
		Preconditions.checkArgument(order != null, "Order is null");
		final SalesOrderProviderContainer container = new SalesOrderProviderContainer();
		container.setOrder(order);
		container.setProvider(providerModel);
		final SalesOrderRequest orderRequestData = salesOrderRequestConverter.convert(container);
		SalesOrderResponse salesOrderResponse = null;

		if (!CollectionUtils.isEmpty(orderRequestData.getEntryLines()))
		{
			try
			{
				salesOrderResponse = erpwsSalesOrderService.createSalesOrder(orderRequestData, providerModel.getUsername(),
						providerModel.getPassword(), providerModel.getServicePoolName(), providerModel.getSalesOrderPublicName(),
						providerModel.getSalesOrderType());
			}
			catch (final ERPWSServiceException e)
			{
				LOG.error("DefaultAldahraERPClientService: Error in sales order service: {}", e.getMessage());
				saveSalesOrderResponse(order, "FAILED DUE TO: " + e.getMessage() + "\n " + salesOrderResponse, false);
				return;
			}

			order.setSalesOrderSent(true);
			if (salesOrderResponse == null)
			{
				LOG.error("Sales Order creation failed!");
				saveSalesOrderResponse(order, "FAILED DUE TO NULL RESPONSE", false);
				return;
			}
			final String fullResponse = salesOrderResponse.getSalesOrderResponse() + ", "
					+ salesOrderResponse.getResponse().toString();

			saveSalesOrderResponse(order, fullResponse, salesOrderResponse.getCreationSuccess());
			LOG.info("Sales Order Created successfully: {}", salesOrderResponse.getCreationSuccess());
		}
		else
		{
			LOG.error("Order with code {} and status of {} is not sent to the ERP system", order.getCode(), order.getStatus());
		}
	}

	private void saveSalesOrderResponse(final OrderModel order, final String response, final boolean success)
	{
		final List<String> records = new ArrayList<>();
		if (order.getStore().isEnableSaveStockResponse() && order.getSendOrderResponses() != null)
		{
			records.addAll(order.getSendOrderResponses());
		}
		records.add(response);
		order.setSendOrderResponses(records);
		order.setSalesOrderCreatedSuccessfully(success);
		order.setSalesOrderCreationResponse(response);
		modelService.save(order);
		modelService.refresh(order);
	}

}
