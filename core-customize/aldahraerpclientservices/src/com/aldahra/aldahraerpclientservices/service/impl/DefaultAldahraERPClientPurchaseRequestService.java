/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.service.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.aldahra.aldahraerpclientservices.erp.ERPWSPurchaseRequestService;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.PurchaseRequest;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.PurchaseRequestProviderContainer;
import com.aldahra.aldahraerpclientservices.erp.response.PurchaseResponse;
import com.aldahra.aldahraerpclientservices.erp.response.ResponseMessage;
import com.aldahra.aldahraerpclientservices.erp.response.ResponseMetadata;
import com.aldahra.aldahraerpclientservices.erp.response.enums.MessageType;
import com.aldahra.aldahraerpclientservices.exception.ERPWSExceptionType;
import com.aldahra.aldahraerpclientservices.exception.ERPWSServiceException;
import com.aldahra.aldahraerpclientservices.service.AldahraERPClientPurchaseRequestService;
import com.aldahra.aldahraerpclientwebservices.model.AldahraERPProductPurchaseRequestServiceProviderModel;
import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


/**
 * @author monzer
 */
public class DefaultAldahraERPClientPurchaseRequestService implements AldahraERPClientPurchaseRequestService
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultAldahraERPClientPurchaseRequestService.class);
	private static final Gson GSON = new GsonBuilder().create();

	@Resource(name = "purchaseRequestService")
	private ERPWSPurchaseRequestService erpwsPurchaseRequestService;

	@Resource(name = "purcahseRequestConverter")
	private Converter<PurchaseRequestProviderContainer, PurchaseRequest> purchaseRequestConverter;

	@Override
	public List<PurchaseResponse> sendPurchaseRequestForProductsUnderMinimumAmount(final List<ProductModel> products,
			final AldahraERPProductPurchaseRequestServiceProviderModel providerModel) throws ERPWSServiceException
	{
		Preconditions.checkArgument(!CollectionUtils.isEmpty(products), "Product codes are null");
		Preconditions.checkArgument(providerModel != null, "Provider is null");
		final List<PurchaseResponse> responses = new ArrayList<>();

		products.stream().forEach(product -> {
			Optional<PurchaseResponse> response = Optional.empty();
			try
			{
				response = sendPurchaseRequestForProductsUnderMinimumAmount(product, providerModel);
			}
			catch (final ERPWSServiceException e)
			{
				final PurchaseResponse res = new PurchaseResponse();
				res.setProductCode(product.getCode());
				final ResponseMessage responseMessage = new ResponseMessage();
				final ResponseMetadata metadata = new ResponseMetadata();
				metadata.setMessage(e.getMessage());
				metadata.setType(MessageType.ERROR);
				responseMessage.setMessage(Arrays.asList(metadata));
				res.setMessages(responseMessage);
				res.setResponse(e.getMessage());
				responses.add(res);
			}
			if (response.isPresent())
			{
				responses.add(response.get());
			}
		});
		return CollectionUtils.isEmpty(responses) ? Collections.emptyList() : responses;
	}

	@Override
	public Optional<PurchaseResponse> sendPurchaseRequestForProductsUnderMinimumAmount(final ProductModel product,
			final AldahraERPProductPurchaseRequestServiceProviderModel providerModel) throws ERPWSServiceException
	{
		Preconditions.checkArgument(product != null, "Product codes are null");
		Preconditions.checkArgument(providerModel != null, "Provider is null");

		final PurchaseRequestProviderContainer container = new PurchaseRequestProviderContainer();
		container.setProduct(product);
		container.setProvider(providerModel);
		final PurchaseRequest purchaseRequest = purchaseRequestConverter.convert(container);

		Optional<PurchaseResponse> purchaseResponse = Optional.empty();
		try
		{
			purchaseResponse = erpwsPurchaseRequestService.createPurchaseRequest(purchaseRequest, providerModel.getUsername(),
					providerModel.getPassword(), providerModel.getServicePoolName(), providerModel.getSchemaName());
		}
		catch (final ERPWSServiceException e)
		{
			LOG.error("DefaultAldahraERPClientService: Error in purchase service: {}", e.getMessage());
			throw new ERPWSServiceException(ERPWSExceptionType.INVALID_RESPONSE);
		}
		if (purchaseResponse.isEmpty())
		{
			LOG.error("Product Purchase request response is null!");
			throw new ERPWSServiceException(ERPWSExceptionType.INVALID_RESPONSE);
		}


		LOG.info("Product Purchase request response data is: {}", GSON.toJson(purchaseResponse.get()));
		return purchaseResponse;
	}



}
