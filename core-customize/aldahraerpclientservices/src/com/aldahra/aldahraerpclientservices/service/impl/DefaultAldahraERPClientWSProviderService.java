/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.service.impl;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.aldahra.aldahraerpclientservices.dao.ERPClientProviderDao;
import com.aldahra.aldahraerpclientservices.service.AldahraERPClientWSProviderService;
import com.aldahra.aldahraerpclientwebservices.model.AldahraERPClientWebServiceProviderModel;
import com.google.common.base.Preconditions;


/**
 * @author monzer
 */
public class DefaultAldahraERPClientWSProviderService implements AldahraERPClientWSProviderService
{

	@Resource(name = "erpWebServiceProviderDaoMap")
	private Map<Class<?>, ERPClientProviderDao> erpClientProviderDaoMap;

	@Override
	public Optional<AldahraERPClientWebServiceProviderModel> get(final String code, final Class<?> providerClass)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(code), "ERPProvider code cannot be null");
		Preconditions.checkArgument(providerClass != null, "Provider class cannot be null");
		final Optional<ERPClientProviderDao> dao = getDao(providerClass);
		Preconditions.checkArgument(dao.isPresent(), "Provider dao not found for class " + providerClass.getName());
		return dao.get().get(code);
	}

	@Override
	public Optional<AldahraERPClientWebServiceProviderModel> getActive(final String baseStoreUid, final Class<?> providerClass)
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(baseStoreUid), "base store uid cannot be null");
		return erpClientProviderDaoMap.get(providerClass).getActive(baseStoreUid);
	}

	@Override
	public Optional<AldahraERPClientWebServiceProviderModel> getActive(final BaseStoreModel baseStore,
			final Class<?> providerClass)
	{
		Preconditions.checkArgument(baseStore != null, "base store model cannot be null");
		return erpClientProviderDaoMap.get(providerClass).getActive(baseStore);
	}

	@Override
	public Optional<AldahraERPClientWebServiceProviderModel> getActiveByCurrentStore(final Class<?> providerClass)
	{
		return erpClientProviderDaoMap.get(providerClass).getActiveByCurrentBaseStore();
	}

	protected Optional<ERPClientProviderDao> getDao(final Class<?> providerClass)
	{
		final ERPClientProviderDao dao = erpClientProviderDaoMap.get(providerClass);

		return Optional.ofNullable(dao);
	}

}
