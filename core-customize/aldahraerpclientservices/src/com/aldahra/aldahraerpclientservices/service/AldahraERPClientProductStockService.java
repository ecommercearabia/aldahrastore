/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.service;

import java.util.List;
import java.util.Optional;

import com.aldahra.aldahraerpclientservices.erp.response.ProductStockResponse;
import com.aldahra.aldahraerpclientservices.erp.response.SiteStockResponse;
import com.aldahra.aldahraerpclientservices.exception.ERPWSServiceException;
import com.aldahra.aldahraerpclientwebservices.model.AldahraERPProductStockServiceProviderModel;


/**
 * @author monzer
 */
public interface AldahraERPClientProductStockService
{

	List<ProductStockResponse> getProductsStock(List<String> productCodes,
			AldahraERPProductStockServiceProviderModel providerModel)
			throws ERPWSServiceException;

	Optional<ProductStockResponse> getProductStock(String productCode, AldahraERPProductStockServiceProviderModel providerModel)
			throws ERPWSServiceException;

	Optional<SiteStockResponse> getProductERPStock(String productCode, AldahraERPProductStockServiceProviderModel providerModel)
			throws ERPWSServiceException;


}
