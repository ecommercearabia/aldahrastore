/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraerpclientservices.service;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.aldahra.aldahraerpclientwebservices.model.AldahraERPClientWebServiceProviderModel;


/**
 * @author monzer
 */
public interface AldahraERPClientWSProviderService
{

	public Optional<AldahraERPClientWebServiceProviderModel> get(String code, final Class<?> providerClass);

	public Optional<AldahraERPClientWebServiceProviderModel> getActive(String baseStoreUid, final Class<?> providerClass);

	public Optional<AldahraERPClientWebServiceProviderModel> getActive(BaseStoreModel baseStore, final Class<?> providerClass);

	public Optional<AldahraERPClientWebServiceProviderModel> getActiveByCurrentStore(final Class<?> providerClass);

}
