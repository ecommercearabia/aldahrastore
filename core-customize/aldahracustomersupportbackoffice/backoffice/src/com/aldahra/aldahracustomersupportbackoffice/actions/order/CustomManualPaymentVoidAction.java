/*
 * Decompiled with CFR 0.145.
 *
 * Could not load the following classes:
 *  com.hybris.backoffice.widgets.notificationarea.event.NotificationEvent
 *  com.hybris.backoffice.widgets.notificationarea.event.NotificationEvent$Level
 *  com.hybris.cockpitng.actions.ActionContext
 *  com.hybris.cockpitng.actions.ActionResult
 *  com.hybris.cockpitng.actions.ActionResult$StatusFlag
 *  com.hybris.cockpitng.actions.CockpitAction
 *  com.hybris.cockpitng.engine.impl.AbstractComponentWidgetAdapterAware
 *  com.hybris.cockpitng.util.notifications.NotificationService
 *  de.hybris.platform.core.enums.OrderStatus
 *  de.hybris.platform.core.model.order.OrderModel
 *  de.hybris.platform.orderprocessing.model.OrderProcessModel
 *  de.hybris.platform.processengine.BusinessProcessService
 *  de.hybris.platform.servicelayer.model.ModelService
 *  de.hybris.platform.store.BaseStoreModel
 *  javax.annotation.Resource
 *  org.apache.log4j.Logger
 */
package com.aldahra.aldahracustomersupportbackoffice.actions.order;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.zkoss.zul.Messagebox;

import com.aldahra.aldahrapayment.ccavenue.exception.PaymentException;
import com.aldahra.aldahrapayment.context.PaymentContext;
import com.aldahra.aldahrapayment.entry.PaymentResponseData;
import com.aldahra.aldahrapayment.enums.PaymentResponseStatus;
import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.CockpitAction;
import com.hybris.cockpitng.engine.impl.AbstractComponentWidgetAdapterAware;
import com.hybris.cockpitng.util.notifications.NotificationService;


public class CustomManualPaymentVoidAction extends AbstractComponentWidgetAdapterAware
		implements CockpitAction<OrderModel, OrderModel>
{
	protected static final String MANUAL_PAYMENT_VOID_SUCCESS = "action.manualpaymentvoid.success";
	protected static final String MANUAL_VOID_PAYMENT_EVENT = "ManualVoidPaymentEvent";
	private static final Logger LOG = Logger.getLogger(CustomManualPaymentVoidAction.class);
	@Resource
	private BusinessProcessService businessProcessService;
	@Resource
	private PaymentContext paymentContext;

	@Resource
	private ModelService modelService;
	@Resource
	private NotificationService notificationService;

	public ActionResult<OrderModel> perform(final ActionContext<OrderModel> actionContext)
	{
		ActionResult actionResult = null;
		if (actionContext != null && actionContext.getData() != null)
		{
			final OrderModel order = actionContext.getData();

			try
			{
				final Optional<PaymentResponseData> paymentOrderCanceledResponseData = paymentContext
						.getPaymentOrderCancelResponseData(order.getStore(), order);
				if (paymentOrderCanceledResponseData.isEmpty())
				{
					LOG.error(String.format("Error in executeManualPaymentVoidOperation :paymentOrderCanceledResponseData is empty"));
				}


				final PaymentResponseStatus paymentResponseStatus = paymentOrderCanceledResponseData.get().getStatus();

				if (PaymentResponseStatus.SUCCESS.equals(paymentResponseStatus))
				{

					//					order.getOrderProcess().stream()
					//							.filter(process -> process.getCode().startsWith(order.getStore().getSubmitOrderProcessCode()))
					//							.forEach(filteredProcess -> this.getBusinessProcessService()
					//									.triggerEvent(String.valueOf(filteredProcess.getCode()) + "_" + MANUAL_VOID_PAYMENT_EVENT));
					//					LOG.info(String.format("Payment Void Manual Release completed. %s triggered.", MANUAL_VOID_PAYMENT_EVENT));
					//					order.setStatus(OrderStatus.CANCELLED);
					this.getModelService().save(order);
					actionResult = new ActionResult("success");
				}
				else
				{
					LOG.error("Payment Response Status is not success in executeManualPaymentVoidOperation ");
					actionResult = new ActionResult("error");
					Messagebox.show("Payment Response Status is not success in executeManualPaymentVoidOperation");
				}
			}
			catch (final PaymentException e)
			{
				LOG.error(String.format("Error in executeManualPaymentVoidOperation : %s", e.getMessage()));
				actionResult = new ActionResult("error");
				Messagebox.show(String.format("Error in executeManualPaymentVoidOperation : %s", e.getMessage()));
			}

			actionResult.getStatusFlags().add(ActionResult.StatusFlag.OBJECT_PERSISTED);
		}
		return actionResult;
	}

	public boolean canPerform(final ActionContext<OrderModel> ctx)
	{
		final OrderModel order = ctx.getData();
		//        return order != null && OrderStatus.PAYMENT_NOT_VOIDED.equals((Object)order.getStatus());
		return order != null && order.getPaymentInfo() != null && order.getPaymentInfo() instanceof CreditCardPaymentInfoModel
				&& !OrderStatus.PAYMENT_CAPTURED.equals(order.getStatus()) && !OrderStatus.CANCELLED.equals(order.getStatus())
				&& StringUtils.isBlank(order.getVersionID());
	}

	@Override
	public boolean needsConfirmation(final ActionContext<OrderModel> ctx)
	{
		return true;
	}

	@Override
	public String getConfirmationMessage(final ActionContext<OrderModel> ctx)
	{
		return "Do you want cancel the amount?";
	}

	protected void executeManualPaymentVoidOperation(final OrderModel order) throws PaymentException
	{

	}

	protected BusinessProcessService getBusinessProcessService()
	{
		return this.businessProcessService;
	}

	protected ModelService getModelService()
	{
		return this.modelService;
	}
}

