package com.aldahra.aldahracustomersupportbackoffice.actions.returns;

import de.hybris.platform.basecommerce.enums.ReturnStatus;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.returns.ReturnService;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.aldahra.aldahrapayment.service.CustomRefundService;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEvent;
import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.CockpitAction;
import com.hybris.cockpitng.engine.impl.AbstractComponentWidgetAdapterAware;
import com.hybris.cockpitng.util.notifications.NotificationService;


public class CustomManualRefundAction extends AbstractComponentWidgetAdapterAware
		implements CockpitAction<ReturnRequestModel, ReturnRequestModel>
{
	private static final Logger LOG = Logger.getLogger(CustomManualRefundAction.class);
	protected static final String SUCCESS_MESSAGE_LABEL = "action.manualrefund.success";
	protected static final String FAILURE_MESSAGE_LABEL = "action.manualrefund.failure";
	protected static final String UNSAVED_OBJECT_WARNING_LABEL = "action.manualrefund.unsaved.object.warning";
	protected static final String ACTION_MANUALREFUND_COD_CHECK = "action.manualrefund.cod.check";

	@Resource
	private ModelService modelService;
	@Resource
	private ReturnService returnService;
	@Resource
	private NotificationService notificationService;
	@Resource(name = "businessProcessService")
	private BusinessProcessService businessProcessService;
	@Resource(name = "customRefundService")
	private CustomRefundService customRefundService;

	public boolean canPerform(final ActionContext<ReturnRequestModel> actionContext)
	{
		final Object data = actionContext.getData();
		boolean decision = false;
		if (data instanceof ReturnRequestModel)
		{
			final ReturnStatus returnStatus = ((ReturnRequestModel) data).getStatus();
			decision = returnStatus.equals(ReturnStatus.RECEIVED) || returnStatus.equals(ReturnStatus.COMPLETED);
		}
		return decision;
	}

	public String getConfirmationMessage(final ActionContext<ReturnRequestModel> actionContext)
	{
		final ReturnRequestModel returnRequest = actionContext.getData();
		final String paymentMode = returnRequest.getOrder().getPaymentMode() == null ? null
				: returnRequest.getOrder().getPaymentMode().getCode();


		return actionContext.getLabel(ACTION_MANUALREFUND_COD_CHECK);
	}

	public boolean needsConfirmation(final ActionContext<ReturnRequestModel> actionContext)
	{
		final ReturnRequestModel returnRequest = actionContext.getData();
		final String paymentMode = returnRequest.getOrder().getPaymentMode() == null ? null
				: returnRequest.getOrder().getPaymentMode().getCode();

		return "cod".equalsIgnoreCase(paymentMode);
	}

	public ActionResult<ReturnRequestModel> perform(final ActionContext<ReturnRequestModel> actionContext)
	{
		ActionResult actionResult;
		final ReturnRequestModel returnRequest = actionContext.getData();

		try
		{
			customRefundService.refundAmount(returnRequest.getOrder(), returnRequest.getSubtotal());
			this.getReturnService().requestManualPaymentReversalForReturnRequest(returnRequest);
			this.getNotificationService().notifyUser("", "JustMessage", NotificationEvent.Level.SUCCESS, new Object[]
			{ actionContext.getLabel(SUCCESS_MESSAGE_LABEL) });
			actionResult = new ActionResult("success");
			final OrderProcessModel orderProcessModel = businessProcessService.createProcess(
					"sendOrderRefundEmail-process-" + returnRequest.getOrder().getCode() + "-" + System.currentTimeMillis(),
					"sendOrderRefundEmail-process");
			orderProcessModel.setOrder(returnRequest.getOrder());
			getModelService().save(orderProcessModel);
			businessProcessService.startProcess(orderProcessModel);
		}
		catch (final Exception e)
		{
			LOG.error(e.getMessage(), e);
			this.getNotificationService().notifyUser("", "JustMessage", NotificationEvent.Level.FAILURE, new Object[]
			{ actionContext.getLabel(FAILURE_MESSAGE_LABEL) });
			actionResult = new ActionResult("error");
		}
		actionResult.getStatusFlags().add(ActionResult.StatusFlag.OBJECT_PERSISTED);
		return actionResult;
	}

	//	@Override
	//	public boolean needsConfirmation(final ActionContext<ReturnRequestModel> ctx)
	//	{
	//		return true;
	//	}
	//
	//	@Override
	//	public String getConfirmationMessage(final ActionContext<ReturnRequestModel> ctx)
	//	{
	//		if (StringUtils.isNotBlank(ctx.getData().getTrackingID()))
	//		{
	//			return "Do you want to create new shipment?";
	//		}
	//		else
	//		{
	//			return "Do you want to create shipment?";
	//		}
	//	}

	protected ModelService getModelService()
	{
		return this.modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected ReturnService getReturnService()
	{
		return this.returnService;
	}

	protected NotificationService getNotificationService()
	{
		return this.notificationService;
	}
}

