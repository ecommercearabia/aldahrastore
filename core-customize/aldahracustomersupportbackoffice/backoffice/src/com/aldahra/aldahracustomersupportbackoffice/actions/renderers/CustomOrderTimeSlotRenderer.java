/**
 *
 */
package com.aldahra.aldahracustomersupportbackoffice.actions.renderers;

import java.util.Date;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zul.Listcell;

import com.hybris.cockpitng.core.config.impl.jaxb.listview.ListColumn;
import com.hybris.cockpitng.dataaccess.facades.type.DataType;
import com.hybris.cockpitng.dataaccess.facades.type.TypeFacade;
import com.hybris.cockpitng.dataaccess.services.PropertyValueService;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.labels.LabelService;
import com.hybris.cockpitng.widgets.common.WidgetComponentRenderer;


/**
 * @author origin
 *
 */
public class CustomOrderTimeSlotRenderer implements WidgetComponentRenderer<Listcell, ListColumn, Object>
{
	private static final Logger LOG = LoggerFactory.getLogger(CustomOrderTimeSlotRenderer.class);

	@Resource(name = "typeFacade")
	private TypeFacade typeFacade;

	@Resource(name = "labelService")
	private LabelService labelService;

	@Resource(name = "propertyValueService")
	private PropertyValueService propertyValueService;

	@Override
	public void render(final Listcell parent, final ListColumn configuration, final Object data, final DataType dataType,
			final WidgetInstanceManager widgetInstanceManager)
	{
		boolean isExpress = false;

		final String qualifier = configuration.getQualifier();
		try
		{
			final Object value = this.getPropertyValueService().readValue(data, qualifier);
			final Object express = this.getPropertyValueService().readValue(data, "express");
			isExpress = (express instanceof Boolean) && (boolean) express;

			if (isExpress)
			{
				parent.setLabel("Express");
				return;
			}

			if (!(value instanceof Date))
			{
				LOG.error("Qualifier : {}, is not a Date object", qualifier);
				return;
			}

			final Date dateValue = (Date) value;
			parent.setLabel(dateValue.toString());
		}
		catch (final Exception var12)
		{
			LOG.info("Could not render row.", var12);

			parent.setLabel("Could not render row.:" + var12.getMessage());
		}
	}

	/**
	 * @return the propertyValueService
	 */
	public PropertyValueService getPropertyValueService()
	{
		return propertyValueService;
	}
}
