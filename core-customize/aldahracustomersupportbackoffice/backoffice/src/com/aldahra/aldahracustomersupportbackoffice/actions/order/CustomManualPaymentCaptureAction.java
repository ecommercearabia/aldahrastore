/*
 * Decompiled with CFR 0.145.
 *
 * Could not load the following classes:
 *  com.hybris.backoffice.widgets.notificationarea.event.NotificationEvent
 *  com.hybris.backoffice.widgets.notificationarea.event.NotificationEvent$Level
 *  com.hybris.cockpitng.actions.ActionContext
 *  com.hybris.cockpitng.actions.ActionResult
 *  com.hybris.cockpitng.actions.ActionResult$StatusFlag
 *  com.hybris.cockpitng.actions.CockpitAction
 *  com.hybris.cockpitng.engine.impl.AbstractComponentWidgetAdapterAware
 *  com.hybris.cockpitng.util.notifications.NotificationService
 *  de.hybris.platform.core.enums.OrderStatus
 *  de.hybris.platform.core.model.order.OrderModel
 *  de.hybris.platform.orderprocessing.model.OrderProcessModel
 *  de.hybris.platform.processengine.BusinessProcessService
 *  de.hybris.platform.servicelayer.model.ModelService
 *  de.hybris.platform.store.BaseStoreModel
 *  javax.annotation.Resource
 *  org.apache.log4j.Logger
 */
package com.aldahra.aldahracustomersupportbackoffice.actions.order;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.zkoss.zul.Messagebox;

import com.aldahra.aldahrapayment.ccavenue.exception.PaymentException;
import com.aldahra.aldahrapayment.context.PaymentContext;
import com.aldahra.aldahrapayment.entry.PaymentResponseData;
import com.aldahra.aldahrapayment.enums.PaymentResponseStatus;
import com.aldahra.aldahrapayment.strategy.CustomPaymentTransactionStrategy;
import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.CockpitAction;
import com.hybris.cockpitng.engine.impl.AbstractComponentWidgetAdapterAware;
import com.hybris.cockpitng.util.notifications.NotificationService;


public class CustomManualPaymentCaptureAction extends AbstractComponentWidgetAdapterAware
		implements CockpitAction<OrderModel, OrderModel>
{
	private static final Logger LOG = Logger.getLogger(CustomManualPaymentCaptureAction.class);
	protected static final String MANUAL_PAYMENT_CAPTURE_SUCCESS = "action.manualpaymentcapture.success";
	protected static final String MANUAL_CAPTURE_PAYMENT_EVENT = "ManualCapturePaymentEvent";
	@Resource
	private BusinessProcessService businessProcessService;
	@Resource
	private PaymentContext paymentContext;
	@Resource
	private ModelService modelService;
	@Resource
	private NotificationService notificationService;
	@Resource(name = "paymentTransactionStrategy")
	private CustomPaymentTransactionStrategy paymentTransactionStrategy;



	public ActionResult<OrderModel> perform(final ActionContext<OrderModel> actionContext)
	{
		final ActionResult<OrderModel> actionResult = new ActionResult(ActionResult.SUCCESS);

		if (actionContext != null && actionContext.getData() != null)
		{
			final OrderModel order = actionContext.getData();
			try
			{

				final Optional<PaymentResponseData> paymentOrderConfirmedResponseData = paymentContext
						.getPaymentOrderConfirmedResponseData(order.getStore(), order);
				if (paymentOrderConfirmedResponseData.isEmpty())
				{
					actionResult.setResultCode(ActionResult.ERROR);
					Messagebox.show("paymentOrderConfirmedResponseData is empty");
				}

				final PaymentResponseStatus paymentResponseStatus = paymentOrderConfirmedResponseData.get().getStatus();

				if (PaymentResponseStatus.SUCCESS.equals(paymentResponseStatus))
				{
					order.getOrderProcess().stream()
							.filter(process -> process.getCode().startsWith(order.getStore().getSubmitOrderProcessCode()))
							.forEach(filteredProcess -> this.getBusinessProcessService()
									.triggerEvent(String.valueOf(filteredProcess.getCode()) + "_" + MANUAL_CAPTURE_PAYMENT_EVENT));
					LOG.info(String.format("Payment Capture Manual Release completed. %s triggered.", MANUAL_CAPTURE_PAYMENT_EVENT));
					order.setStatus(OrderStatus.PAYMENT_CAPTURED);
					this.getModelService().save(order);
					actionResult.setResultCode(ActionResult.SUCCESS);
					Messagebox.show("Payment Capture Manual Release completed.");
				}
				else
				{

					actionResult.setResultCode(ActionResult.ERROR);
					Messagebox.show(paymentResponseStatus.toString());
				}

			}
			catch (final PaymentException e)
			{

				LOG.error(String.format("Error in executeManualPaymentCaptureOperation : %s", e.getMessage()));
				Messagebox.show(e.getMessage());
				actionResult.setResultCode(ActionResult.ERROR);
			}


			actionResult.getStatusFlags().add(ActionResult.StatusFlag.OBJECT_PERSISTED);
		}
		return actionResult;
	}



	public boolean canPerform(final ActionContext<OrderModel> ctx)
	{
		final OrderModel order = ctx.getData();
		return order != null && order.getPaymentInfo() != null && order.getPaymentInfo() instanceof CreditCardPaymentInfoModel
				&& !OrderStatus.PAYMENT_CAPTURED.equals(order.getStatus()) && !OrderStatus.CANCELLED.equals(order.getStatus())
				&& StringUtils.isBlank(order.getVersionID());
	}

	@Override
	public boolean needsConfirmation(final ActionContext<OrderModel> ctx)
	{
		return true;
	}

	@Override
	public String getConfirmationMessage(final ActionContext<OrderModel> ctx)
	{
		return "Do you want capture the amount?";
	}


	protected BusinessProcessService getBusinessProcessService()
	{
		return this.businessProcessService;
	}

	protected ModelService getModelService()
	{
		return this.modelService;
	}

	protected NotificationService getNotificationService()
	{
		return this.notificationService;
	}
}

