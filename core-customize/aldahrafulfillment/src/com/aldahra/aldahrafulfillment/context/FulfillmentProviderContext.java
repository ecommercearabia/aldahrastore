package com.aldahra.aldahrafulfillment.context;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.aldahra.aldahrafulfillment.model.FulfillmentProviderModel;


/**
 *
 */
public interface FulfillmentProviderContext
{
	//	public Optional<FulfillmentProviderModel> getProvider(Class<?> providerClass);
	//
	//	public Optional<FulfillmentProviderModel> getProvider(String baseStoreUid, Class<?> providerClass);
	//
	//	public Optional<FulfillmentProviderModel> getProvider(BaseStoreModel baseStoreModel, Class<?> providerClass);

	public Optional<FulfillmentProviderModel> getProvider(BaseStoreModel baseStoreModel, boolean isExpress);
}
