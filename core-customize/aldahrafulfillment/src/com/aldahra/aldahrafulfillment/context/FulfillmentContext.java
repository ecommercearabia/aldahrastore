package com.aldahra.aldahrafulfillment.context;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.Optional;

import com.aldahra.aldahrafulfillment.exception.FulfillentException;


/**
 *
 */
public interface FulfillmentContext
{
	public Optional<String> createShipment(ConsignmentModel consignmentModel) throws FulfillentException;

	public Optional<String> updateShipment(ConsignmentModel consignmentModel) throws FulfillentException;

	public Optional<byte[]> printAWB(ConsignmentModel consignmentModel) throws FulfillentException;

	public Optional<String> getStatus(ConsignmentModel consignmentModel) throws FulfillentException;

	public Optional<ConsignmentStatus> updateStatus(ConsignmentModel consignmentModel) throws FulfillentException;
}
