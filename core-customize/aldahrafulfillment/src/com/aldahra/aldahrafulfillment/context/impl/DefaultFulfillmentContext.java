package com.aldahra.aldahrafulfillment.context.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.aldahra.aldahrafulfillment.context.FulfillmentContext;
import com.aldahra.aldahrafulfillment.context.FulfillmentProviderContext;
import com.aldahra.aldahrafulfillment.exception.FulfillentException;
import com.aldahra.aldahrafulfillment.exception.enums.FulfillentExceptionType;
import com.aldahra.aldahrafulfillment.model.FulfillmentProviderModel;
import com.aldahra.aldahrafulfillment.strategy.FulfillmentStrategy;
import com.google.common.base.Preconditions;


/**
 *
 */
public class DefaultFulfillmentContext implements FulfillmentContext
{
	protected static final Logger LOG = Logger.getLogger(DefaultFulfillmentContext.class);
	/** The fulfillment strategy map. */
	@Resource(name = "fulfillmentStrategyMap")
	private Map<Class<?>, FulfillmentStrategy> fulfillmentStrategyMap;

	@Resource(name = "fulfillmentProviderContext")
	private FulfillmentProviderContext fulfillmentProviderContext;

	/**
	 * @return the fulfillmentProviderContext
	 */
	protected FulfillmentProviderContext getFulfillmentProviderContext()
	{
		return fulfillmentProviderContext;
	}

	private static final String FULFILLMENT_STRATEGY_NOT_FOUND = "strategy not found";
	private static final String CONSIGNMENT_MODEL_MUST_NOT_BE_NULL = "consignment must not be null";
	private static final String ORDER_MODEL_MUST_NOT_BE_NULL = "order must not be null";
	private static final String SHIPMENT_NOT_CREATED = "No shipment has been created for this consignment";

	/**
	 * @return the fulfillmentStrategyMap
	 */
	protected Map<Class<?>, FulfillmentStrategy> getFulfillmentStrategyMap()
	{
		return fulfillmentStrategyMap;
	}

	@Override
	public Optional<String> createShipment(final ConsignmentModel consignmentModel) throws FulfillentException
	{
		Preconditions.checkArgument(consignmentModel != null, CONSIGNMENT_MODEL_MUST_NOT_BE_NULL);
		Preconditions.checkArgument(consignmentModel.getOrder() != null, ORDER_MODEL_MUST_NOT_BE_NULL);

		final Optional<FulfillmentProviderModel> fulfillmentProvider = getFulfillmentProviderContext()
				.getProvider(consignmentModel.getOrder().getStore(), consignmentModel.getOrder().isExpress());

		if (!fulfillmentProvider.isPresent())
		{
			LOG.error("Error Creating Shipment for Consignment: " + consignmentModel.getCode());
			LOG.error("Reason: No Fulfillment Provider Found.");
			return Optional.empty();
		}
		final Optional<FulfillmentStrategy> fulfillmentStrategy = getStrategy(fulfillmentProvider.get().getClass());
		if (fulfillmentStrategy.isPresent())
		{
			return fulfillmentStrategy.get().createShipment(consignmentModel, fulfillmentProvider.get());
		}
		return Optional.empty();
	}

	@Override
	public Optional<byte[]> printAWB(final ConsignmentModel consignmentModel) throws FulfillentException
	{
		Preconditions.checkArgument(consignmentModel != null, CONSIGNMENT_MODEL_MUST_NOT_BE_NULL);
		final Optional<FulfillmentProviderModel> fulfillmentProvider = getFulfillmentProviderContext()
				.getProvider(consignmentModel.getOrder().getStore(), consignmentModel.getOrder().isExpress());

		if (!fulfillmentProvider.isPresent())
		{
			LOG.error("Error Creating Shipment for Consignment: " + consignmentModel.getCode());
			LOG.error("Reason: No Fulfillment Provider Found.");
			return Optional.empty();
		}
		final Optional<FulfillmentStrategy> fulfillmentStrategy = getStrategy(fulfillmentProvider.get().getClass());
		if (fulfillmentStrategy.isPresent())
		{
			return fulfillmentStrategy.get().printAWB(consignmentModel, fulfillmentProvider.get());
		}
		return Optional.empty();
	}

	protected Optional<FulfillmentStrategy> getStrategy(final Class<?> providerClass)
	{
		final FulfillmentStrategy strategy = getFulfillmentStrategyMap().get(providerClass);
		Preconditions.checkArgument(strategy != null, FULFILLMENT_STRATEGY_NOT_FOUND);

		return Optional.ofNullable(strategy);
	}

	@Override
	public Optional<String> getStatus(final ConsignmentModel consignmentModel) throws FulfillentException
	{
		Preconditions.checkArgument(consignmentModel != null, CONSIGNMENT_MODEL_MUST_NOT_BE_NULL);
		Preconditions.checkArgument(consignmentModel.getOrder() != null, ORDER_MODEL_MUST_NOT_BE_NULL);

		final Optional<FulfillmentProviderModel> fulfillmentProvider = getFulfillmentProviderContext()
				.getProvider(consignmentModel.getOrder().getStore(), consignmentModel.getOrder().isExpress());

		if (!fulfillmentProvider.isPresent())
		{
			LOG.error("Error Getting Status for Consignment: " + consignmentModel.getCode());
			LOG.error("Reason: No Fulfillment Provider Found.");
			return Optional.empty();
		}
		final Optional<FulfillmentStrategy> fulfillmentStrategy = getStrategy(fulfillmentProvider.get().getClass());
		if (fulfillmentStrategy.isPresent())
		{
			return fulfillmentStrategy.get().getStatus(consignmentModel, fulfillmentProvider.get());
		}
		return Optional.empty();
	}

	@Override
	public Optional<String> updateShipment(final ConsignmentModel consignmentModel) throws FulfillentException
	{
		Preconditions.checkArgument(consignmentModel != null, CONSIGNMENT_MODEL_MUST_NOT_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNotEmpty(consignmentModel.getTrackingID()), SHIPMENT_NOT_CREATED);
		Preconditions.checkArgument(consignmentModel.getCarrierDetails() != null, SHIPMENT_NOT_CREATED);
		final Optional<FulfillmentProviderModel> fulfillmentProvider = getFulfillmentProviderContext()
				.getProvider(consignmentModel.getOrder().getStore(), consignmentModel.getOrder().isExpress());

		if (!fulfillmentProvider.isPresent())
		{
			LOG.error("Error Creating Shipment for Consignment: " + consignmentModel.getCode());
			LOG.error("Reason: No Fulfillment Provider Found.");
			return Optional.empty();
		}
		final Optional<FulfillmentStrategy> fulfillmentStrategy = getStrategy(fulfillmentProvider.get().getClass());
		if (fulfillmentStrategy.isPresent())
		{
			return fulfillmentStrategy.get().updateShipment(consignmentModel, fulfillmentProvider.get());
		}
		return Optional.empty();
	}

	@Override
	public Optional<ConsignmentStatus> updateStatus(final ConsignmentModel consignmentModel) throws FulfillentException
	{
		Preconditions.checkArgument(consignmentModel != null, CONSIGNMENT_MODEL_MUST_NOT_BE_NULL);
		Preconditions.checkArgument(consignmentModel.getOrder() != null, CONSIGNMENT_MODEL_MUST_NOT_BE_NULL);
		//		Preconditions.checkArgument(type != null, FULFILLMENT_PROVIDOR_MUST_NOT_BE_NULL);

		final Optional<FulfillmentProviderModel> fulfillmentProvider = getFulfillmentProviderContext()
				.getProvider(consignmentModel.getOrder().getStore(), consignmentModel.getOrder().isExpress());

		if (!fulfillmentProvider.isPresent())
		{
			LOG.error("Error Changing  Status for Consignment: " + consignmentModel.getCode());
			LOG.error("Reason: No Fulfillment Provider Found.");
			throw new FulfillentException(FulfillentExceptionType.PROVIDER_NOT_SUPPORTED);
		}

		final Optional<FulfillmentStrategy> fulfillmentStrategy = getStrategy(fulfillmentProvider.get().getClass());
		if (!fulfillmentStrategy.isPresent())
		{
			LOG.error("Error Changing  Status for Consignment: " + consignmentModel.getCode());
			LOG.error("Reason: No Fulfillment Provider Found.");
			throw new FulfillentException(FulfillentExceptionType.PROVIDER_NOT_SUPPORTED);
		}

		final Optional<ConsignmentStatus> currentStatus = fulfillmentStrategy.get().updateStatus(consignmentModel,
				fulfillmentProvider.get());
		if (!currentStatus.isPresent())
		{
			LOG.error("Error Changing  Status for Consignment: " + consignmentModel.getCode());
			LOG.error("Reason: Couldn't Get the status of the consignment.");
			return Optional.empty();
		}
		return currentStatus;
	}

}
