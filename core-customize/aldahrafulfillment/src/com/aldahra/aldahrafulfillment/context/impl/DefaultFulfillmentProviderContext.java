package com.aldahra.aldahrafulfillment.context.impl;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import com.aldahra.aldahrafulfillment.context.FulfillmentProviderContext;
import com.aldahra.aldahrafulfillment.enums.FulfillmentProviderType;
import com.aldahra.aldahrafulfillment.model.FulfillmentProviderModel;
import com.aldahra.aldahrafulfillment.model.LyveFulfillmentProviderModel;
import com.aldahra.aldahrafulfillment.model.ShipaFulfillmentProviderModel;
import com.aldahra.aldahrafulfillment.strategy.FulfillmentProviderStrategy;
import com.google.common.base.Preconditions;


/**
 *
 */
public class DefaultFulfillmentProviderContext implements FulfillmentProviderContext
{
	@Resource(name = "fulfillmentProviderMap")
	private Map<Class<?>, FulfillmentProviderStrategy> fulfillmentProviderMap;

	private static final String PROVIDER_CLASS_MUSTN_T_BE_NULL = "strategy mustn't be null";
	private static final String BASESTORE_UID_MUSTN_T_BE_NULL = "baseStoreUid mustn't be null";
	private static final String BASESTORE_MODEL_MUSTN_T_BE_NULL = "baseStoreModel mustn't be null";

	private static final String PROVIDER_STRATEGY_NOT_FOUND = "strategy not found";

	//	@Override
	protected Optional<FulfillmentProviderModel> getProvider(final Class<?> providerClass)
	{
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<FulfillmentProviderStrategy> strategy = getStrategy(providerClass);
		Preconditions.checkArgument(strategy.isPresent(), PROVIDER_STRATEGY_NOT_FOUND);

		return strategy.get().getActiveProviderByCurrentBaseStore();
	}

	protected Optional<FulfillmentProviderStrategy> getStrategy(final Class<?> providerClass)
	{
		final FulfillmentProviderStrategy strategy = getFulfillmentProviderMap().get(providerClass);

		return Optional.ofNullable(strategy);
	}

	protected Map<Class<?>, FulfillmentProviderStrategy> getFulfillmentProviderMap()
	{
		return fulfillmentProviderMap;
	}

	//	@Override
	protected Optional<FulfillmentProviderModel> getProvider(final String baseStoreUid, final Class<?> providerClass)
	{
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(baseStoreUid != null, BASESTORE_UID_MUSTN_T_BE_NULL);
		final Optional<FulfillmentProviderStrategy> strategy = getStrategy(providerClass);
		Preconditions.checkArgument(strategy.isPresent(), PROVIDER_STRATEGY_NOT_FOUND);

		return strategy.get().getActiveProvider(baseStoreUid);
	}

	//	@Override
	protected Optional<FulfillmentProviderModel> getProvider(final BaseStoreModel baseStoreModel, final Class<?> providerClass)
	{
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(baseStoreModel != null, BASESTORE_MODEL_MUSTN_T_BE_NULL);
		final Optional<FulfillmentProviderStrategy> strategy = getStrategy(providerClass);
		Preconditions.checkArgument(strategy.isPresent(), PROVIDER_STRATEGY_NOT_FOUND);

		return strategy.get().getActiveProvider(baseStoreModel);
	}

	@Override
	public Optional<FulfillmentProviderModel> getProvider(final BaseStoreModel baseStoreModel, final boolean isExpress)
	{
		Preconditions.checkArgument(baseStoreModel != null, BASESTORE_UID_MUSTN_T_BE_NULL);

		final FulfillmentProviderType fulfillmentProvidorType = isExpress ? baseStoreModel.getExpressFulfillmentProvidorType()
				: baseStoreModel.getFulfillmentProvidorType();

		if (fulfillmentProvidorType == null)
		{
			return Optional.empty();
		}

		if (FulfillmentProviderType.LYVE.equals(fulfillmentProvidorType))
		{
			return getProvider(baseStoreModel.getUid(), LyveFulfillmentProviderModel.class);

		}
		else if (FulfillmentProviderType.SHIPA.equals(fulfillmentProvidorType))
		{
			return getProvider(baseStoreModel.getUid(), ShipaFulfillmentProviderModel.class);
		}
		else
		{
			return Optional.empty();
		}

	}
}
