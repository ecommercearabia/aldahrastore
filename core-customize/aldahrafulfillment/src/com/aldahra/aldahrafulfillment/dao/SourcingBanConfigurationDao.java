/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrafulfillment.dao;

import com.aldahra.aldahrafulfillment.model.SourcingBanConfigModel;


/**
 * @author monzer
 */
public interface SourcingBanConfigurationDao
{
	SourcingBanConfigModel getSourcingBanConfiguration();
}
