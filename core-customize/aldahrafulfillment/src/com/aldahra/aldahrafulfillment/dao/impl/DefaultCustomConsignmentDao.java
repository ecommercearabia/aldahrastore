/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrafulfillment.dao.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.consignmenttrackingservices.model.CarrierModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

import java.util.Collections;
import java.util.List;

import org.springframework.util.CollectionUtils;

import com.aldahra.aldahrafulfillment.dao.CustomConsignmentDao;
import com.google.common.base.Preconditions;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class DefaultCustomConsignmentDao extends DefaultGenericDao<ConsignmentModel> implements CustomConsignmentDao
{

	private static final String CARRIER_MUST_NOT_BE_NULL = "CarrierModel must not be null";

	private static final String STATUS_MUST_NOT_BE_NULL = "ConsignmentStatus must not be null";

	private static final String STATUSES = "statuses";

	private static final String CARRIER = "carrier";

	private static final String FQL = "SELECT {c:" + ConsignmentModel.PK + "} FROM {" + ConsignmentModel._TYPECODE
			+ " AS c} WHERE {c:" + ConsignmentModel.STATUS + "} not in (?" + STATUSES + ")" + " AND {c:"
			+ ConsignmentModel.CARRIERDETAILS + "} = ?carrier" + " AND {c:" + ConsignmentModel.TRACKINGID + "} IS NOT NULL AND {c:"
			+ ConsignmentModel.SENTDELIVERYCONFIRMATIONNOTIFICATION + "} = 0";


	public DefaultCustomConsignmentDao()
	{
		super(ConsignmentModel._TYPECODE);
	}

	@Override
	public List<ConsignmentModel> findByCarrierAndNotStatuses(final CarrierModel carrier, final List<ConsignmentStatus> statuses)
	{
		Preconditions.checkArgument(carrier != null, CARRIER_MUST_NOT_BE_NULL);
		Preconditions.checkArgument(statuses != null && org.apache.commons.collections4.CollectionUtils.isNotEmpty(statuses),
				STATUS_MUST_NOT_BE_NULL);

		final FlexibleSearchQuery query = new FlexibleSearchQuery(FQL);
		query.addQueryParameter(STATUSES, statuses);
		query.addQueryParameter(CARRIER, carrier);

		final List<ConsignmentModel> result = getFlexibleSearchService().<ConsignmentModel> search(query).getResult();

		if (CollectionUtils.isEmpty(result))
		{
			Collections.emptyList();
		}
		return result;
	}



}
