/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrafulfillment.dao.impl;

import com.aldahra.aldahrafulfillment.dao.FulfillmentProviderDao;
import com.aldahra.aldahrafulfillment.model.ShipaFulfillmentProviderModel;

/**
 *
 */
public class DefaultShipaFulfillmentProviderDao extends DefaultFulfillmentProviderDao
		implements FulfillmentProviderDao
{

	/**
	 *
	 */
	public DefaultShipaFulfillmentProviderDao()
	{
		super(ShipaFulfillmentProviderModel._TYPECODE);
	}

	@Override
	protected String getModelName()
	{
		return ShipaFulfillmentProviderModel._TYPECODE;
	}

}
