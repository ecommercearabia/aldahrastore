/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrafulfillment.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.aldahra.aldahrafulfillment.dao.FulfillmentProviderDao;
import com.aldahra.aldahrafulfillment.model.FulfillmentProviderModel;
import com.google.common.base.Preconditions;

/**
 *
 */
public abstract class DefaultFulfillmentProviderDao extends DefaultGenericDao<FulfillmentProviderModel>
		implements FulfillmentProviderDao
{

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	private static final String CODE_MUSTN_T_BE_NULL = "code mustn't be null or empty";
	private static final String STORES = "stores";
	private static final String CODE = "code";
	private static final String ACTIVE = "active";

	/**
	 *
	 */
	public DefaultFulfillmentProviderDao(final String typecode)
	{
		super(typecode);
	}

	protected abstract String getModelName();

	@Override
	public Optional<FulfillmentProviderModel> get(final String code)
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(code), CODE_MUSTN_T_BE_NULL);
		final Map<String, Object> params = new HashMap<>();
		params.put(CODE, code);
		final List<FulfillmentProviderModel> find = find(params);
		final Optional<FulfillmentProviderModel> findFirst = find.stream().findFirst();

		return findFirst.isPresent() ? Optional.ofNullable(findFirst.get()) : Optional.ofNullable(null);
	}

	@Override
	public Optional<FulfillmentProviderModel> getActive(final String baseStoreUid)
	{
		return getActive(baseStoreService.getBaseStoreForUid(baseStoreUid));
	}

	@Override
	public Optional<FulfillmentProviderModel> getActive(final BaseStoreModel baseStoreModel)
	{
		Preconditions.checkArgument(baseStoreModel != null, "baseStoreModel must not be null");
		final Optional<Collection<FulfillmentProviderModel>> find = find(Arrays.asList(baseStoreModel), Boolean.TRUE, 1);

		return find.isPresent() && !find.get().isEmpty() ? Optional.ofNullable(find.get().iterator().next())
				: Optional.ofNullable(null);
	}

	@Override
	public Optional<FulfillmentProviderModel> getActiveByCurrentBaseStore()
	{
		return getActive(baseStoreService.getCurrentBaseStore());
	}



	protected Optional<Collection<FulfillmentProviderModel>> find(final List<BaseStoreModel> stores,
			final Boolean active,
			final Integer countRecords)
	{
		final Map<String, Object> params = new HashMap<>();
		final StringBuilder query = new StringBuilder();
		query.append("SELECT {fp.PK} ");
		query.append("FROM ");
		query.append("{ ");
		query.append(getModelName()).append(" AS fp ");


		if (CollectionUtils.isNotEmpty(stores))
		{
			query.append("JOIN BSFPRelation AS bsfpRel ON {bsfpRel.target}={fp.PK} ");
			query.append("JOIN BaseStore AS bs ON {bsfpRel.source}={bs.PK} AND {bs.PK} IN (?stores) ");
			params.put(STORES, stores);
		}

		query.append("} ");

		int activeValue = 0;
		if (active == null || Boolean.TRUE.equals(active))
		{
			activeValue = 1;
		}
		query.append("WHERE {fp.active}=?active ");
		params.put(ACTIVE, activeValue);

		query.append("ORDER BY {fp.creationtime} DESC");

		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query.toString());
		flexibleSearchQuery.getQueryParameters().putAll(params);
		if (countRecords != null && countRecords > 0)
		{
			flexibleSearchQuery.setCount(countRecords);
		}

		final SearchResult<FulfillmentProviderModel> result = getFlexibleSearchService().search(flexibleSearchQuery);

		return result != null ? Optional.ofNullable(result.getResult()) : Optional.ofNullable(Collections.emptyList());
	}

}
