/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrafulfillment.dao.impl;

import de.hybris.platform.consignmenttrackingservices.model.CarrierModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.aldahra.aldahrafulfillment.dao.CarrierDao;


/**
 *
 * @author abu-muhasien
 *
 */
public class DefaultCarrierDao extends DefaultGenericDao<CarrierModel> implements CarrierDao
{
	public DefaultCarrierDao()
	{
		super(CarrierModel._TYPECODE);
	}

	@Override
	public CarrierModel get(final String code)
	{
		if (StringUtils.isNotBlank(code))
		{
			final String query = "SELECT {pk} FROM {" + CarrierModel._TYPECODE + "} WHERE {code}=?code";
			final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query);
			flexibleSearchQuery.addQueryParameter(CarrierModel.CODE, code);

			final SearchResult<CarrierModel> result = getFlexibleSearchService().search(flexibleSearchQuery);
			if (result != null && CollectionUtils.isNotEmpty(result.getResult()))
			{
				return result.getResult().get(0);
			}
		}

		return null;
	}
}