/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrafulfillment.dao.impl;

import com.aldahra.aldahrafulfillment.dao.FulfillmentProviderDao;
import com.aldahra.aldahrafulfillment.model.LyveFulfillmentProviderModel;


/**
 *
 */
public class DefaultLyveFulfillmentProviderDao extends DefaultFulfillmentProviderDao implements FulfillmentProviderDao
{

	/**
	 *
	 */
	public DefaultLyveFulfillmentProviderDao()
	{
		super(LyveFulfillmentProviderModel._TYPECODE);
	}

	@Override
	protected String getModelName()
	{
		return LyveFulfillmentProviderModel._TYPECODE;
	}

}
