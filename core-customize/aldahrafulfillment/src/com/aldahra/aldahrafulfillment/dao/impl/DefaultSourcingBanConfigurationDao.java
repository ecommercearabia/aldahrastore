/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrafulfillment.dao.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import javax.annotation.Resource;

import com.aldahra.aldahrafulfillment.dao.SourcingBanConfigurationDao;
import com.aldahra.aldahrafulfillment.exception.AmbiguousSourcingBanConfigurationException;
import com.aldahra.aldahrafulfillment.model.SourcingBanConfigModel;


/**
 * @author monzer
 */
public class DefaultSourcingBanConfigurationDao implements SourcingBanConfigurationDao
{

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	@Override
	public SourcingBanConfigModel getSourcingBanConfiguration()
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery("SELECT {PK} FROM {SourcingBanConfig}");
		final SearchResult<SourcingBanConfigModel> result = flexibleSearchService.<SourcingBanConfigModel> search(query);
		if (result.getTotalCount() > 1)
		{
			throw new AmbiguousSourcingBanConfigurationException("Only one Sourcing Ban Configuration is Allowed");
		}
		else
		{
			return result.getTotalCount() == 0 ? null : (SourcingBanConfigModel) result.getResult().get(0);
		}
	}

}
