/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrafulfillment.dao;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.aldahra.aldahrafulfillment.model.FulfillmentProviderModel;


/**
 *
 */
public interface FulfillmentProviderDao
{

	public Optional<FulfillmentProviderModel> get(String code);

	public Optional<FulfillmentProviderModel> getActive(String baseStoreUid);

	public Optional<FulfillmentProviderModel> getActive(BaseStoreModel baseStoreModel);

	public Optional<FulfillmentProviderModel> getActiveByCurrentBaseStore();
}
