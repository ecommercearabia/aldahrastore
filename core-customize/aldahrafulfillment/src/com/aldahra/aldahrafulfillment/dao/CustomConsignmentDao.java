/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrafulfillment.dao;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.consignmenttrackingservices.model.CarrierModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.List;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface CustomConsignmentDao
{
	public List<ConsignmentModel> findByCarrierAndNotStatuses(CarrierModel carrier, List<ConsignmentStatus> status);
}
