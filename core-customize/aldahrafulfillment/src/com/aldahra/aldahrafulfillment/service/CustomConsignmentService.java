/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrafulfillment.service;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface CustomConsignmentService
{
	public List<ConsignmentModel> getConsignmentsByStoreAndNotStatus(BaseStoreModel store, List<ConsignmentStatus> statuses,
			boolean express);

	public List<ConsignmentModel> getConsignmentsByCurrentStoreAndNotStatus(List<ConsignmentStatus> statuses, boolean express);
}
