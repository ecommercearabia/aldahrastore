package com.aldahra.aldahrafulfillment.service;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import com.aldahra.aldahrafulfillment.enums.FulfillmentActionHistoryType;
import com.aldahra.aldahrafulfillment.enums.FulfillmentProviderType;
import com.aldahra.aldahrafulfillment.enums.Operation;
import com.aldahra.aldahrafulfillment.model.FulfillmentProviderModel;


/**
 * @author monzer
 */
public interface ConsignmentShipmentResponseService
{


	public void saveTrackingIdAndCarrier(final FulfillmentProviderModel fulfillmentProviderModel,
			final ConsignmentModel consignmentModel, final String trackingId, final String shipmentOrderId,
			final FulfillmentProviderType type);

	public void saveShipmentRequest(final ConsignmentModel consignment, final String request, final Operation operation);

	public void saveShipmentResponse(final ConsignmentModel consignment, final String response, final Operation operation);

	public void saveConsignment(final ConsignmentModel consignment);
	public void updateConsignmentStatus(final ConsignmentModel consignment, final ConsignmentStatus newStatus,
			final String fulfillmentStatusDescription);

	public void saveActionInHistory(final ConsignmentModel consignmentModel, final String request, final String response,
			final FulfillmentActionHistoryType actionType);
}