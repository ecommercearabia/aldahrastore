/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrafulfillment.service.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.warehousing.model.PackagingInfoModel;

import java.time.Instant;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aldahra.aldahrafulfillment.context.FulfillmentProviderContext;
import com.aldahra.aldahrafulfillment.enums.FulfillmentActionHistoryType;
import com.aldahra.aldahrafulfillment.enums.Operation;
import com.aldahra.aldahrafulfillment.exception.FulfillentException;
import com.aldahra.aldahrafulfillment.exception.enums.FulfillentExceptionType;
import com.aldahra.aldahrafulfillment.model.FulfillmentProviderModel;
import com.aldahra.aldahrafulfillment.model.ShipaFulfillmentProviderModel;
import com.aldahra.aldahrafulfillment.service.CarrierService;
import com.aldahra.aldahrafulfillment.service.ConsignmentShipmentResponseService;
import com.aldahra.aldahrafulfillment.service.FulfillentService;
import com.aldahra.aldahrafulfillment.shipa.exception.ShipaException;
import com.aldahra.aldahrafulfillment.shipa.model.Order;
import com.aldahra.aldahrafulfillment.shipa.model.Recipient;
import com.aldahra.aldahrafulfillment.shipa.model.Sender;
import com.aldahra.aldahrafulfillment.shipa.model.cancel.ResponseStatus;
import com.aldahra.aldahrafulfillment.shipa.model.getorder.GetOrderResponse;
import com.aldahra.aldahrafulfillment.shipa.model.orderdata.OrderData;
import com.aldahra.aldahrafulfillment.shipa.model.success.ResponseSucceed;
import com.aldahra.aldahrafulfillment.shipa.service.ShipaService;
import com.aldahra.aldahratimeslot.model.TimeSlotInfoModel;
import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


/**
 * @author mohammad-abu-muhasien
 */
public class DefaultShipaFulfillmentService implements FulfillentService
{
	protected static final Logger LOG = LoggerFactory.getLogger(DefaultShipaFulfillmentService.class);
	private static final String CONSIGNMENT_MODEL_MUSTN_T_BE_NULL = "ConsignmentModel mustn't be null or empty";
	private static final String CONSIGNMENT_ORDER_MUSTN_T_BE_NULL = "ConsignmentModel Order mustn't be null or empty";
	private static final String FULFILLMENT_PROVIDER_MUSTN_T_BE_NULL = "FulfillmentProviderModel Order mustn't be null or empty";
	private static final String DELIVERY_ADDRESS_MUSTN_T_BE_NULL = "deliveryAddress  mustn't be null or empty";
	private static final String REFERENCE_MUSTN_T_BE_NULL = "Reference Id  mustn't be null or empty";
	private static final String SENT_ORDERS_CAN_NOT_BE_ZERO_OR_NOLL = "Sent Orders  list can not be zero or null";
	private static final String UPDATED_ORDER_RESPONSE_CAN_NOT_BE_EMPTY_OR_NOLL = "Update orders Response   can not be empty or null";
	private static final String CASH_ON_DELIVERY = "CashOnDelivery";
	private static final String PREPAID = "Prepaid";
	private static final String CCOD = "CCOD";
	private static final Gson GSON = new GsonBuilder().create();

	@Resource(name = "shipaService")
	private ShipaService shipaService;
	@Resource(name = "modelService")
	private ModelService modelService;
	@Resource(name = "carrierService")
	private CarrierService carrierService;

	@Resource(name = "consignmentShipmentResponseService")
	private ConsignmentShipmentResponseService consignmentShipmentResponseService;

	@Resource(name = "defaultFulfillmentProviderContext")
	private FulfillmentProviderContext fulfillmentProviderContext;

	@Resource(name = "shipaFulfillmentStatusMap")
	private Map<String, ConsignmentStatus> shipaFulfillmentStatusMap;

	@Override
	public Optional<String> createShipment(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillentException
	{
		Preconditions.checkArgument(consignmentModel != null, CONSIGNMENT_MODEL_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(consignmentModel.getOrder() != null, CONSIGNMENT_ORDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(fulfillmentProviderModel != null, FULFILLMENT_PROVIDER_MUSTN_T_BE_NULL);

		final ShipaFulfillmentProviderModel shipaFulfillmentProviderModel = (ShipaFulfillmentProviderModel) fulfillmentProviderModel;
		final Order[] selectedOrder = getSelectedOrder(consignmentModel, shipaFulfillmentProviderModel);
		Preconditions.checkArgument(selectedOrder != null, SENT_ORDERS_CAN_NOT_BE_ZERO_OR_NOLL);
		LOG.info(GSON.toJson(selectedOrder[0]));
		saveShipmentRequest(consignmentModel, selectedOrder[0].toString(), Operation.CREATE);

		final List<ResponseSucceed> sendOrders = shipaService.sendOrders(shipaFulfillmentProviderModel.getBaseUrl(),
				shipaFulfillmentProviderModel.getApiKey(), selectedOrder);

		saveShipmentResponse(consignmentModel, sendOrders.get(0).toString(), Operation.CREATE);

		saveTrackingIdAndCarrier(shipaFulfillmentProviderModel, consignmentModel,
				sendOrders.get(0).getDeliveryInfo().getReference());

		return Optional.ofNullable(consignmentModel.getTrackingID());
	}

	private void saveTrackingIdAndCarrier(final ShipaFulfillmentProviderModel shipaFulfillmentProviderModel,
			final ConsignmentModel consignmentModel, final String trackingId)
	{
		modelService.refresh(consignmentModel);
		consignmentModel.setTrackingID(trackingId);
		consignmentModel.setCarrierDetails(
				carrierService.create(shipaFulfillmentProviderModel.getCode(), shipaFulfillmentProviderModel.getName()));
		saveConsignment(consignmentModel);
	}

	private void saveShipmentRequest(final ConsignmentModel consignment, final String request, final Operation operation)
	{
		switch (operation)
		{
			case CREATE:
				consignment.setCreateShipmentRequestBody(request);
				break;
			case UPDATE:
				consignment.setUpdateShipmentRequestBody(request);
				break;

			default:
				return;
		}
		saveConsignment(consignment);
	}

	private void saveShipmentResponse(final ConsignmentModel consignment, final String response, final Operation operation)
	{
		switch (operation)
		{
			case CREATE:
				consignment.setCreateShipmentResponseBody(response);
				break;
			case UPDATE:
				consignment.setUpdateShipmentResponseBody(response);
				break;
			case STATUS:
				consignment.setStatusShipmentResponseBody(response);
				break;

			default:
				return;
		}
		saveConsignment(consignment);
	}

	private void saveConsignment(final ConsignmentModel consignment)
	{
		modelService.save(consignment);
		modelService.refresh(consignment);
	}

	protected Order[] getSelectedOrder(final ConsignmentModel consignmentModel,
			final ShipaFulfillmentProviderModel fulfillmentProviderModel)
	{
		final AddressModel deliveryAddress = consignmentModel.getOrder().getDeliveryAddress();
		Preconditions.checkArgument(deliveryAddress != null, DELIVERY_ADDRESS_MUSTN_T_BE_NULL);
		// populate sender
		final Sender sender = new Sender();
		sender.setAddress(fulfillmentProviderModel.getSenderAddress());
		sender.setCountry("ARE");
		sender.setEmail(fulfillmentProviderModel.getSenderEmail());
		sender.setCity(fulfillmentProviderModel.getSenderCity());
		sender.setName(fulfillmentProviderModel.getSenderName());
		sender.setPhone(fulfillmentProviderModel.getSenderPhone());

		// populate recipient
		final Recipient recipient = new Recipient();
		recipient.setCountry("ARE"); // Fixed as foodcrowd is not used in another country
		recipient.setEmail(consignmentModel.getOrder().getUser().getUid());
		recipient.setName(deliveryAddress.getFirstname() + " " + deliveryAddress.getLastname());
		recipient.setPhone(deliveryAddress.getMobile());
		recipient.setAddress(getAddress(deliveryAddress));
		recipient.setCity(deliveryAddress.getCity().getShipaCity());

		final Order order = new Order();
		order.setId(consignmentModel.getCode() + "_" + System.currentTimeMillis());
		order.setPaymentMethod(getPaymentMethod(consignmentModel.getOrder().getPaymentMode()));
		order.setGoodsValue(String.valueOf(consignmentModel.getOrder().getTotalPrice()));

		order.setArea(deliveryAddress.getArea() == null ? "" : deliveryAddress.getArea().getName());
		order.setDistrict(deliveryAddress.getDistrict());
		order.setTower(deliveryAddress.getBuildingName());
		order.setApartmentNumber(deliveryAddress.getApartmentNumber());

		if (!CASH_ON_DELIVERY.equalsIgnoreCase(order.getPaymentMethod()) && !CCOD.equalsIgnoreCase(order.getPaymentMethod()))
		{
			order.setAmount(0);
		}
		else
		{
			order.setAmount(consignmentModel.getOrder().getTotalPrice());
		}
		order.setSender(sender);
		order.setDescription(
				fulfillmentProviderModel.getDescription() + " - " + consignmentModel.getOrder().getTimeslotDescription());

		if (fulfillmentProviderModel.getShipaDeliveryType() != null)
		{
			order.setTypeDelivery(fulfillmentProviderModel.getShipaDeliveryType().getCode().toLowerCase());
		}

		order.setRecipient(recipient);

		return new Order[]
		{ order };
	}

	/**
	 *
	 */
	protected String getPaymentMethod(final PaymentModeModel paymentMode)
	{
		if (paymentMode != null)
		{
			switch (paymentMode.getCode())
			{
				case "card":
				case "continue":
				case "apple":
					return PREPAID;
				case "ccod":
					return CCOD;
				case "cod":
					return CASH_ON_DELIVERY;
			}
		}
		return null;
	}

	protected String getAddress(final AddressModel deliveryAddress)
	{
		final StringBuilder address = new StringBuilder();
		if (deliveryAddress.getCountry() != null && !StringUtils.isEmpty(deliveryAddress.getCountry().getName()))
		{
			address.append(deliveryAddress.getCountry().getName()).append("_");
		}
		if (!StringUtils.isEmpty(deliveryAddress.getLine1()))
		{
			address.append("Address Line 1: ").append(deliveryAddress.getLine1()).append(", ");
		}
		if (deliveryAddress.getCity() != null && !StringUtils.isEmpty(deliveryAddress.getCity().getName()))
		{
			address.append("City: ").append(deliveryAddress.getCity().getName()).append(", ");
		}
		if (deliveryAddress.getArea() != null && !StringUtils.isEmpty(deliveryAddress.getArea().getName()))
		{
			address.append("Area: ").append(deliveryAddress.getArea().getName()).append(", ");
		}
		if (!StringUtils.isEmpty(deliveryAddress.getNearestLandmark()))
		{
			address.append("Nearest Landmark: ").append(deliveryAddress.getNearestLandmark());
		}
		return address.toString();
	}

	protected String getEstimatedDeliveryDate(final TimeSlotInfoModel timeSlotInfo)
	{
		if (timeSlotInfo != null)
		{
			final LocalTime localTime = LocalTime.parse(timeSlotInfo.getEnd(), DateTimeFormatter.ofPattern("H:mm"));
			return timeSlotInfo.getDate() + " " + localTime.format(DateTimeFormatter.ofPattern("hh:mm:ss a"));
		}
		else
		{
			LOG.warn("TimeSlotInfo not found on Order, setting default estimated delivery date on update shipment request.");
			return ZonedDateTime.ofInstant(Instant.now(), ZoneId.of("Asia/Dubai")).plusDays(1)
					.format(DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm:ss a"));
		}
	}

	protected OrderData getOrderDataInfo(final ConsignmentModel consignmentModel, final ShipaFulfillmentProviderModel provider)
			throws FulfillentException
	{

		final AddressModel deliveryAddress = consignmentModel.getOrder().getDeliveryAddress();
		final PackagingInfoModel packagingInfo = consignmentModel.getPackagingInfo();
		final TimeSlotInfoModel timeSlotInfo = consignmentModel.getOrder().getTimeSlotInfo();
		Preconditions.checkArgument(deliveryAddress != null, DELIVERY_ADDRESS_MUSTN_T_BE_NULL);

		final OrderData orderData = new OrderData();
		if (packagingInfo != null)
		{
			orderData.setEstimatedDeliveryDate(getEstimatedDeliveryDate(timeSlotInfo));
			orderData.setHeight(getDouble(packagingInfo.getHeight()));
			orderData.setLength(getDouble(packagingInfo.getLength()));
			orderData.setWeight(getDouble(packagingInfo.getGrossWeight()));
			orderData.setWidth(getDouble(packagingInfo.getWidth()));
			orderData.setQuantity(consignmentModel.getConsignmentEntries().stream().filter(Objects::nonNull)
					.map(ConsignmentEntryModel::getQuantity).collect(Collectors.summingLong(Long::longValue)));
			setCoordinates(deliveryAddress, orderData);
			//			orderData.setRecipientAddress(getAddress(deliveryAddress));
			orderData.setReady(true);
		}

		return orderData;

	}

	private void setCoordinates(final AddressModel deliveryAddress, final OrderData orderData) throws FulfillentException
	{
		if (deliveryAddress.getLongitude() != null && deliveryAddress.getLatitude() != null
				&& deliveryAddress.getLongitude().doubleValue() + deliveryAddress.getLatitude().doubleValue() > 0)
		{
			orderData.setRecipientCoordinates(deliveryAddress.getLatitude() + "," + deliveryAddress.getLongitude());
		}
		else
		{
			setDefaultCoordinates(deliveryAddress, orderData);
		}
	}

	private void setDefaultCoordinates(final AddressModel deliveryAddress, final OrderData orderData) throws FulfillentException
	{
		if (deliveryAddress.getArea() != null && deliveryAddress.getArea().getLatitude() != null
				&& deliveryAddress.getArea().getLongitude() != null
				&& deliveryAddress.getArea().getLongitude().doubleValue() + deliveryAddress.getArea().getLatitude().doubleValue() > 0)
		{
			LOG.warn("Coordinates not found on deliveryAddress, setting coordinates from deliveryAddress.area");
			orderData
					.setRecipientCoordinates(deliveryAddress.getArea().getLatitude() + "," + deliveryAddress.getArea().getLongitude());
		}
		else if (deliveryAddress.getCity() != null && deliveryAddress.getCity().getLatitude() != null
				&& deliveryAddress.getCity().getLongitude() != null
				&& deliveryAddress.getCity().getLongitude().doubleValue() + deliveryAddress.getCity().getLatitude().doubleValue() > 0)
		{
			LOG.warn("Coordinates not found on deliveryAddress, setting coordinates from deliveryAddress.city");
			orderData
					.setRecipientCoordinates(deliveryAddress.getCity().getLatitude() + "," + deliveryAddress.getCity().getLongitude());
		}
		else
		{
			LOG.error("No latitude and longitude found on either deliveryAddress, city and area.");
			throw new ShipaException(FulfillentExceptionType.MISSING_COORDINATES);
		}
	}

	private double getDouble(final String value)
	{
		if (StringUtils.isEmpty(value))
		{
			return 0;
		}
		return Double.parseDouble(value);
	}

	@Override
	public Optional<byte[]> printAWB(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws ShipaException
	{
		Preconditions.checkArgument(consignmentModel != null, CONSIGNMENT_MODEL_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(fulfillmentProviderModel != null, FULFILLMENT_PROVIDER_MUSTN_T_BE_NULL);

		final String referenceId = consignmentModel.getTrackingID();
		Preconditions.checkArgument(referenceId != null, REFERENCE_MUSTN_T_BE_NULL);

		final ShipaFulfillmentProviderModel shipaFulfillmentProviderModel = (ShipaFulfillmentProviderModel) fulfillmentProviderModel;

		final String shipaPdfTemplate = shipaFulfillmentProviderModel.getShipaPdfTemplate() != null
				? shipaFulfillmentProviderModel.getShipaPdfTemplate().getCode().toLowerCase()
				: null;

		return Optional.ofNullable(shipaService.downloadOrderByReferenceID(shipaFulfillmentProviderModel.getBaseUrl(),
				shipaFulfillmentProviderModel.getApiKey(), referenceId, shipaFulfillmentProviderModel.getCopies(), shipaPdfTemplate)
				.getBody());

	}


	@Override
	public Optional<String> getStatus(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillentException
	{
		Preconditions.checkArgument(consignmentModel != null, CONSIGNMENT_MODEL_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(fulfillmentProviderModel != null, FULFILLMENT_PROVIDER_MUSTN_T_BE_NULL);
		final String trackingID = consignmentModel.getTrackingID();
		Preconditions.checkArgument(trackingID != null, REFERENCE_MUSTN_T_BE_NULL);
		validateRequestData(consignmentModel, fulfillmentProviderModel, Operation.STATUS);

		final ShipaFulfillmentProviderModel provider = (ShipaFulfillmentProviderModel) fulfillmentProviderModel;
		LOG.info("Getting consignment status for tracking ID: {}", trackingID);

		GetOrderResponse getOrderResponse = null;
		try
		{
			getOrderResponse = shipaService.getOrderByReferenceID(provider.getBaseUrl(), provider.getApiKey(), trackingID);
			LOG.info(GSON.toJson(getOrderResponse));
		}
		catch (final ShipaException e)
		{
			LOG.error("Error in tracking order - Shipox: {}", e.getMessage());
			consignmentShipmentResponseService.saveShipmentResponse(consignmentModel, e.getMessage(), Operation.STATUS);
			consignmentShipmentResponseService.saveActionInHistory(consignmentModel,
					"Tracking shipment " + consignmentModel.getTrackingID(), e.getMessage(), FulfillmentActionHistoryType.GETSTATUS);
			throw new FulfillentException(FulfillentExceptionType.BAD_REQUEST);
		}

		if (getOrderResponse == null)
		{
			LOG.error("Empty response!");
			consignmentShipmentResponseService.saveShipmentResponse(consignmentModel, "Empty response!", Operation.STATUS);
			consignmentShipmentResponseService.saveActionInHistory(consignmentModel,
					"Tracking shipment " + consignmentModel.getTrackingID(), "Empty response!",
					FulfillmentActionHistoryType.GETSTATUS);
			return Optional.empty();
		}

		LOG.info("Create Shipment response is: {}", getOrderResponse);
		consignmentShipmentResponseService.saveShipmentResponse(consignmentModel, GSON.toJson(getOrderResponse), Operation.STATUS);
		consignmentShipmentResponseService.saveActionInHistory(consignmentModel,
				"Tracking shipment " + consignmentModel.getTrackingID(), GSON.toJson(getOrderResponse),
				FulfillmentActionHistoryType.GETSTATUS);

		return getOrderResponse.getDeliveryInfo() != null ? Optional.ofNullable(getOrderResponse.getDeliveryInfo().getCodeStatus())
				: Optional.empty();
	}

	protected void validateRequestData(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel, final Operation operation) throws FulfillentException
	{
		Preconditions.checkArgument(consignmentModel != null, "Consignment model is empty");
		Preconditions.checkArgument(consignmentModel.getOrder() != null, "Order model is empty");
		Preconditions.checkArgument(fulfillmentProviderModel != null, "Shipox provider is not available");
		Preconditions.checkArgument(fulfillmentProviderModel instanceof ShipaFulfillmentProviderModel,
				fulfillmentProviderModel.getClass() + " Provider not supported");

		if (!validateShipaProvider((ShipaFulfillmentProviderModel) fulfillmentProviderModel))
		{
			LOG.error("Not valid provider, check Shipox provider for missing required data");
			consignmentShipmentResponseService.saveShipmentResponse(consignmentModel,
					FulfillentExceptionType.PROVIDER_NOT_SUPPORTED.getValue(),
					operation);
			FulfillmentActionHistoryType type = null;
			switch (operation)
			{
				case CREATE:
					type = FulfillmentActionHistoryType.CREATESHIPMENT;
					break;
				case STATUS:
					type = FulfillmentActionHistoryType.GETSTATUS;
					break;
				case UPDATE:
					type = FulfillmentActionHistoryType.UPDATESTATUS;
					break;
				default:
					type = FulfillmentActionHistoryType.PRINTAWB;
			}

			consignmentShipmentResponseService.saveActionInHistory(consignmentModel,
					fulfillmentProviderModel.getCode() + " for " + operation,
					FulfillentExceptionType.PROVIDER_NOT_SUPPORTED.getValue(), type);
			throw new FulfillentException(FulfillentExceptionType.PROVIDER_NOT_SUPPORTED);
		}
	}

	protected boolean validateShipaProvider(final ShipaFulfillmentProviderModel provider)
	{
		if (provider == null)
		{
			return false;
		}
		if (!provider.getActive())
		{
			LOG.error("Shipox provider is not active");
			return false;
		}
		if (StringUtils.isBlank(provider.getBaseUrl()))
		{
			LOG.error("Empty base url on Shipox provider");
			return false;
		}
		if (StringUtils.isBlank(provider.getApiKey()))
		{
			LOG.error("Empty username on Shipox provider");
			return false;
		}

		return true;
	}
	@Override
	public Optional<String> updateShipment(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillentException
	{
		Preconditions.checkArgument(consignmentModel != null, CONSIGNMENT_MODEL_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(!StringUtils.isEmpty(consignmentModel.getTrackingID()), "Tracking ID must not be null");
		Preconditions.checkArgument(consignmentModel.getCarrierDetails() != null, "CarrierDetails must not be null");
		Preconditions.checkArgument(consignmentModel.getOrder() != null, CONSIGNMENT_ORDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(fulfillmentProviderModel != null, FULFILLMENT_PROVIDER_MUSTN_T_BE_NULL);

		final ShipaFulfillmentProviderModel shipaFulfillmentProviderModel = (ShipaFulfillmentProviderModel) fulfillmentProviderModel;
		final OrderData orderDataInfo = getOrderDataInfo(consignmentModel, shipaFulfillmentProviderModel);
		saveShipmentRequest(consignmentModel, orderDataInfo.toString(), Operation.UPDATE);
		LOG.info(orderDataInfo.toString());
		modelService.refresh(consignmentModel);
		final ResponseStatus updateOrderDataByReferenceId = shipaService.updateOrderDataByReferenceId(
				shipaFulfillmentProviderModel.getBaseUrl(), shipaFulfillmentProviderModel.getApiKey(),
				consignmentModel.getTrackingID(), orderDataInfo);
		LOG.info(updateOrderDataByReferenceId.toString());
		saveShipmentResponse(consignmentModel, updateOrderDataByReferenceId.toString(), Operation.UPDATE);
		Preconditions.checkArgument(updateOrderDataByReferenceId != null, UPDATED_ORDER_RESPONSE_CAN_NOT_BE_EMPTY_OR_NOLL);

		return Optional.ofNullable(consignmentModel.getTrackingID());
	}

	@Override
	public Optional<ConsignmentStatus> updateStatus(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillentException
	{
		final Optional<String> status = getStatus(consignmentModel, fulfillmentProviderModel);
		if (status.isEmpty())
		{
			consignmentShipmentResponseService.saveShipmentResponse(consignmentModel, "No status retrieved!", Operation.STATUS);
			consignmentShipmentResponseService.saveActionInHistory(consignmentModel,
					"Updating the status of " + consignmentModel.getTrackingID(), "No status retrieved!",
					FulfillmentActionHistoryType.UPDATESTATUS);
			return Optional.empty();
		}
		LOG.info("retreived status for {} is {}", consignmentModel.getCode(), status.get());
		final ShipaFulfillmentProviderModel provider = (ShipaFulfillmentProviderModel) fulfillmentProviderModel;
		final Optional<ConsignmentStatus> mappedStatus = shipaFulfillmentStatusMap.keySet().stream()
				.filter(shipmentStatus -> shipmentStatus.equalsIgnoreCase(status.get()))
				.map(key -> shipaFulfillmentStatusMap.get(key)).findAny();
		if (mappedStatus.isEmpty())
		{
			LOG.info("Could not map the {} shipment status", status.get());
			consignmentShipmentResponseService.saveShipmentResponse(consignmentModel,
					status.get() + " not found in the status mapping!",
					Operation.STATUS);
			consignmentShipmentResponseService.saveActionInHistory(consignmentModel,
					"Updating the status of " + consignmentModel.getTrackingID(), status.get() + " not found in the status mapping!",
					FulfillmentActionHistoryType.UPDATESTATUS);
			return Optional.empty();
		}

		LOG.info("Shipment status {} is mapped with {}", status.get(), mappedStatus.get());
		consignmentShipmentResponseService.saveShipmentResponse(consignmentModel, "Mapped status : " + mappedStatus.get(),
				Operation.UPDATE);
		consignmentShipmentResponseService.updateConsignmentStatus(consignmentModel, mappedStatus.get(), status.get());
		consignmentShipmentResponseService.saveActionInHistory(consignmentModel,
				"Updating the status of " + consignmentModel.getTrackingID(),
				mappedStatus.get().toString(), FulfillmentActionHistoryType.UPDATESTATUS);
		return Optional.of(mappedStatus.get());
	}
}
