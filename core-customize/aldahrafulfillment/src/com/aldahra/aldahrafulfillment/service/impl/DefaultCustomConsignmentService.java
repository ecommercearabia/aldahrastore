/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrafulfillment.service.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.consignmenttrackingservices.model.CarrierModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.aldahra.aldahrafulfillment.context.FulfillmentProviderContext;
import com.aldahra.aldahrafulfillment.dao.CustomConsignmentDao;
import com.aldahra.aldahrafulfillment.model.FulfillmentProviderModel;
import com.aldahra.aldahrafulfillment.service.CarrierService;
import com.aldahra.aldahrafulfillment.service.CustomConsignmentService;
import com.google.common.base.Preconditions;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class DefaultCustomConsignmentService implements CustomConsignmentService
{
	protected static final Logger LOG = Logger.getLogger(DefaultCustomConsignmentService.class);

	private static final String STORE_MUST_NOT_BE_NULL = "BaseStoreModel must not be null";

	private static final String STATUS_MUST_NOT_BE_NULL = "ConsignmentStatus must not be null";

	@Resource(name = "fulfillmentProviderContext")
	private FulfillmentProviderContext fulfillmentProviderContext;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "carrierService")
	private CarrierService carrierService;

	@Resource(name = "customConsignmentDao")
	private CustomConsignmentDao customConsignmentDao;

	@Override
	public List<ConsignmentModel> getConsignmentsByStoreAndNotStatus(final BaseStoreModel store,
			final List<ConsignmentStatus> statuses, final boolean express)
	{
		Preconditions.checkArgument(store != null, STORE_MUST_NOT_BE_NULL);
		Preconditions.checkArgument(statuses != null, STATUS_MUST_NOT_BE_NULL);

		final Optional<FulfillmentProviderModel> optional = fulfillmentProviderContext.getProvider(store, express);
		if (!optional.isPresent())
		{
			LOG.error("No FulfillmentProviderModel found for BaseStoreModel: " + store.getUid());
			return Collections.emptyList();
		}
		final CarrierModel carrierModel = carrierService.get(optional.get().getCode());
		if (carrierModel != null)
		{
			return customConsignmentDao.findByCarrierAndNotStatuses(carrierModel, statuses);
		}
		LOG.warn("No CarrierModel found with code: " + optional.get().getCode());
		return Collections.emptyList();
	}

	@Override
	public List<ConsignmentModel> getConsignmentsByCurrentStoreAndNotStatus(final List<ConsignmentStatus> statuses,
			final boolean express)
	{
		return getConsignmentsByStoreAndNotStatus(baseStoreService.getCurrentBaseStore(), statuses, express);
	}

}
