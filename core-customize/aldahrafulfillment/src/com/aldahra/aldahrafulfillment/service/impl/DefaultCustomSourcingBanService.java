/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrafulfillment.service.impl;

import de.hybris.platform.warehousing.sourcing.ban.service.impl.DefaultSourcingBanService;

import javax.annotation.Resource;

import com.aldahra.aldahrafulfillment.dao.SourcingBanConfigurationDao;
import com.aldahra.aldahrafulfillment.model.SourcingBanConfigModel;
import com.aldahra.aldahrafulfillment.service.CustomSourcingBanService;


/**
 * @author monzer
 */
public class DefaultCustomSourcingBanService extends DefaultSourcingBanService implements CustomSourcingBanService
{

	@Resource(name = "sourcingBanConfigurationDao")
	private SourcingBanConfigurationDao sourcingBanConfigDao;

	@Override
	public SourcingBanConfigModel getSourcingBanConfiguration()
	{
		return sourcingBanConfigDao.getSourcingBanConfiguration();
	}

}
