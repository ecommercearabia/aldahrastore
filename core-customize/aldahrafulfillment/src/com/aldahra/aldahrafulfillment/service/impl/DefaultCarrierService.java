/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrafulfillment.service.impl;

import de.hybris.platform.consignmenttrackingservices.model.CarrierModel;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import com.aldahra.aldahrafulfillment.dao.CarrierDao;
import com.aldahra.aldahrafulfillment.enums.FulfillmentProviderType;
import com.aldahra.aldahrafulfillment.service.CarrierService;


/**
 *
 * @author abu-muhasien
 *
 */
public class DefaultCarrierService implements CarrierService
{
	@Resource(name = "carrierDAO")
	private CarrierDao carrierDao;

	@Resource(name = "modelService")
	private ModelService modelService;

	public CarrierDao getCarrierDao()
	{
		return carrierDao;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	@Override
	public CarrierModel get(final String code)
	{
		return getCarrierDao().get(code);
	}

	@Override
	public CarrierModel create(final String code, final String name)
	{
		CarrierModel carrier = get(code);
		if (carrier == null)
		{
			carrier = getModelService().create(CarrierModel.class);
			carrier.setCode(code);
			carrier.setName(name);

			getModelService().save(carrier);
		}

		return carrier;
	}

	@Override
	public CarrierModel create(final String code, final String name, final FulfillmentProviderType type)
	{
		CarrierModel carrier = get(code);
		if (carrier == null)
		{
			carrier = getModelService().create(CarrierModel.class);
			carrier.setCode(code);
			carrier.setName(name);
			carrier.setFulfillmentProviderType(type);
			getModelService().save(carrier);
		}

		return carrier;
	}

}
