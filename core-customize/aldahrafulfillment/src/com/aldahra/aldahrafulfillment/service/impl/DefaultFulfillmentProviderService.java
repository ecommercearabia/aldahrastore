package com.aldahra.aldahrafulfillment.service.impl;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.aldahra.aldahrafulfillment.dao.FulfillmentProviderDao;
import com.aldahra.aldahrafulfillment.model.FulfillmentProviderModel;
import com.aldahra.aldahrafulfillment.service.FulfillmentProviderService;
import com.google.common.base.Preconditions;
/**
 *
 */
public class DefaultFulfillmentProviderService implements FulfillmentProviderService
{
	@Resource(name = "fulfillmentProviderDaoMap")
	private Map<Class<?>, FulfillmentProviderDao> fulfillmentProviderDaoMap;

	protected Map<Class<?>, FulfillmentProviderDao> getFulfillmentProviderDaoMap()
	{
		return fulfillmentProviderDaoMap;
	}

	private static final String CODE_MUSTN_T_BE_NULL = "code mustn't be null or empty";
	private static final String BASESTORE_UID_MUSTN_T_BE_NULL = "baseStoreUid mustn't be null or empty";
	private static final String BASESTORE_MUSTN_T_BE_NULL = "baseStoreModel mustn't be null or empty";

	private static final String PROVIDER_CLASS_MUSTN_T_BE_NULL = "providerClass mustn't be null";
	private static final String PROVIDER_DAO_NOT_FOUND = "dao not found";
	@Override
	public Optional<FulfillmentProviderModel> get(final String code, final Class<?> providerClass)
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(code), CODE_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<FulfillmentProviderDao> dao = getDao(providerClass);
		Preconditions.checkArgument(dao.isPresent(), PROVIDER_DAO_NOT_FOUND);

		return dao.get().get(code);
	}

	@Override
	public Optional<FulfillmentProviderModel> getActive(final String baseStoreUid, final Class<?> providerClass)
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(baseStoreUid), BASESTORE_UID_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<FulfillmentProviderDao> dao = getDao(providerClass);
		Preconditions.checkArgument(dao.isPresent(), PROVIDER_DAO_NOT_FOUND);

		return dao.get().getActive(baseStoreUid);
	}

	@Override
	public Optional<FulfillmentProviderModel> getActive(final BaseStoreModel baseStoreModel, final Class<?> providerClass)
	{
		Preconditions.checkArgument(baseStoreModel != null, BASESTORE_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<FulfillmentProviderDao> dao = getDao(providerClass);
		Preconditions.checkArgument(dao.isPresent(), PROVIDER_DAO_NOT_FOUND);

		return dao.get().getActive(baseStoreModel);
	}

	@Override
	public Optional<FulfillmentProviderModel> getActiveProviderByCurrentBaseStore(final Class<?> providerClass)
	{
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<FulfillmentProviderDao> dao = getDao(providerClass);
		Preconditions.checkArgument(dao.isPresent(), PROVIDER_DAO_NOT_FOUND);

		return dao.get().getActiveByCurrentBaseStore();
	}

	protected Optional<FulfillmentProviderDao> getDao(final Class<?> providerClass)
	{
		final FulfillmentProviderDao dao = getFulfillmentProviderDaoMap().get(providerClass);

		return Optional.ofNullable(dao);
	}

}
