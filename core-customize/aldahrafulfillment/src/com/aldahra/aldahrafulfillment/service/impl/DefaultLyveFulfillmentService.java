/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrafulfillment.service.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.consignmenttrackingservices.model.CarrierModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.aldahra.aldahrafulfillment.context.FulfillmentProviderContext;
import com.aldahra.aldahrafulfillment.enums.FulfillmentActionHistoryType;
import com.aldahra.aldahrafulfillment.enums.Operation;
import com.aldahra.aldahrafulfillment.exception.FulfillentException;
import com.aldahra.aldahrafulfillment.exception.enums.FulfillentExceptionType;
import com.aldahra.aldahrafulfillment.lyve.beans.enums.PaymentMethod;
import com.aldahra.aldahrafulfillment.lyve.beans.innerbeans.Order;
import com.aldahra.aldahrafulfillment.lyve.beans.innerbeans.OrderStatusInfo;
import com.aldahra.aldahrafulfillment.lyve.beans.request.CreateOrderRequest;
import com.aldahra.aldahrafulfillment.lyve.beans.response.CreateOrderResponse;
import com.aldahra.aldahrafulfillment.lyve.beans.response.OrderStatusResponse;
import com.aldahra.aldahrafulfillment.lyve.exception.LyveException;
import com.aldahra.aldahrafulfillment.lyve.service.LyveService;
import com.aldahra.aldahrafulfillment.model.FulfillmentProviderModel;
import com.aldahra.aldahrafulfillment.model.LyveFulfillmentProviderModel;
import com.aldahra.aldahrafulfillment.service.CarrierService;
import com.aldahra.aldahrafulfillment.service.ConsignmentShipmentResponseService;
import com.aldahra.aldahrafulfillment.service.FulfillentService;
import com.aldahra.aldahrafulfillment.shipa.exception.ShipaException;
import com.google.common.base.Preconditions;
import com.google.gson.Gson;


/**
 * @author tuqa
 */
public class DefaultLyveFulfillmentService implements FulfillentService
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultLyveFulfillmentService.class);
	/**
	*
	*/
	private static final String CONSIGNMENT_MODEL_MUSTN_T_BE_NULL = "ConsignmentModel mustn't be null or empty";
	private static final String CONSIGNMENT_ORDER_MUSTN_T_BE_NULL = "ConsignmentModel Order mustn't be null or empty";
	private static final String ORDER_DELIVERY_ADDRESS_MUSTN_T_BE_NULL = "Order DeliveryAddress mustn't be null or empty";

	private static final String FULFILLMENT_PROVIDER_MUSTN_T_BE_LYVE = "FulfillmentProviderModel Order mustn't be LyveFulfillmentProviderModel";

	private static final String FULFILLMENT_PROVIDER_MUSTN_T_BE_NULL = "FulfillmentProviderModel Order mustn't be null or empty";
	private static final Gson gson = new Gson();

	@Resource(name = "lyveService")
	private LyveService lyveService;


	@Resource(name = "lyveFulfillmentStatusMap")
	private Map<String, ConsignmentStatus> lyveFulfillmentStatusMap;

	@Resource(name = "consignmentShipmentResponseService")
	private ConsignmentShipmentResponseService consignmentShipmentResponseService;

	/**
	 * @return the lyveService
	 */
	public LyveService getLyveService()
	{
		return lyveService;
	}

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "carrierService")
	private CarrierService carrierService;


	@Resource(name = "defaultFulfillmentProviderContext")
	private FulfillmentProviderContext fulfillmentProviderContext;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	/**
	 * @return the configurationService
	 */
	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}


	@Override
	public Optional<String> createShipment(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillentException
	{
		Preconditions.checkArgument(consignmentModel != null, CONSIGNMENT_MODEL_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(consignmentModel.getOrder() != null, CONSIGNMENT_ORDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(consignmentModel.getOrder().getDeliveryAddress() != null,
				ORDER_DELIVERY_ADDRESS_MUSTN_T_BE_NULL);

		Preconditions.checkArgument(fulfillmentProviderModel != null, FULFILLMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(fulfillmentProviderModel instanceof LyveFulfillmentProviderModel,
				FULFILLMENT_PROVIDER_MUSTN_T_BE_NULL);



		final LyveFulfillmentProviderModel lyveFulfillmentProviderModel = (LyveFulfillmentProviderModel) fulfillmentProviderModel;

		final CreateOrderRequest createOrderRequest = getCreateOrderRequest(lyveFulfillmentProviderModel, consignmentModel);

		LOG.info(gson.toJson(createOrderRequest));
		saveShipmentRequest(consignmentModel, gson.toJson(createOrderRequest), Operation.CREATE);

		CreateOrderResponse createOrder = null;
		try
		{
			createOrder = getLyveService().createOrder(lyveFulfillmentProviderModel.getCreateOrderBaseUrl(),
					lyveFulfillmentProviderModel.getToken(), createOrderRequest);
		}
		catch (final LyveException e)
		{
			LOG.error(e.getMessage());

			final StringBuilder message = new StringBuilder();
			message.append("type [");
			message.append(e.getType());
			message.append("] :");
			message.append("message[");
			message.append(e.getMessage());
			message.append("] :");
			message.append("respons[");
			message.append(e.getResponsData());
			message.append("]");

			saveShipmentResponse(consignmentModel, message.toString(), Operation.CREATE);

			throw new FulfillentException(FulfillentExceptionType.BAD_REQUEST);
		}

		saveShipmentResponse(consignmentModel, gson.toJson(createOrder), Operation.CREATE);

		final String trackingId = getTrackingId(createOrder);
		saveTrackingIdAndCarrier(lyveFulfillmentProviderModel, consignmentModel, trackingId);

		return Optional.ofNullable(consignmentModel.getTrackingID());
	}


	@Override
	public Optional<String> getStatus(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillentException
	{

		Preconditions.checkArgument(consignmentModel != null, CONSIGNMENT_MODEL_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(consignmentModel.getOrder() != null, CONSIGNMENT_ORDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(fulfillmentProviderModel != null, FULFILLMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(fulfillmentProviderModel instanceof LyveFulfillmentProviderModel,
				FULFILLMENT_PROVIDER_MUSTN_T_BE_LYVE);


		validateRequestData(consignmentModel, fulfillmentProviderModel, Operation.STATUS);

		LOG.info("Getting consignment status for order code {}: ", consignmentModel.getOrder().getCode());


		final LyveFulfillmentProviderModel lyveFulfillmentProviderModel = (LyveFulfillmentProviderModel) fulfillmentProviderModel;

		OrderStatusResponse ordersStatus = null;
		try
		{

			final AbstractOrderModel order = consignmentModel.getOrder();
			final String environment = getConfigurationService().getConfiguration().getString("express.order.environment");
			ordersStatus = getLyveService().getOrderStatus(lyveFulfillmentProviderModel.getOrderStatusBaseUrl(),
					lyveFulfillmentProviderModel.getHash(),
					environment != null ? order.getCode() + "_" + environment.toUpperCase() : order.getCode());

			LOG.info(gson.toJson(ordersStatus));
		}
		catch (final LyveException e)
		{
			LOG.error("Error in tracking order - Lyve: {}", e.getMessage());
			final StringBuilder message = new StringBuilder();
			message.append("type [");
			message.append(e.getType());
			message.append("] :");
			message.append("message[");
			message.append(e.getMessage());
			message.append("] :");
			message.append("respons[");
			message.append(e.getResponsData());
			message.append("]");

			LOG.error(message.toString());
			consignmentShipmentResponseService.saveShipmentResponse(consignmentModel, e.getMessage(), Operation.STATUS);
			consignmentShipmentResponseService.saveActionInHistory(consignmentModel,
					"Tracking shipment" + consignmentModel.getTrackingID(), e.getMessage(), FulfillmentActionHistoryType.GETSTATUS);
			throw new FulfillentException(FulfillentExceptionType.BAD_REQUEST);
		}

		if (ordersStatus == null)
		{
			LOG.error("Empty response!");
			consignmentShipmentResponseService.saveShipmentResponse(consignmentModel, "Empty response!", Operation.STATUS);
			consignmentShipmentResponseService.saveActionInHistory(consignmentModel,
					"Tracking shipment " + consignmentModel.getTrackingID(), "Empty response!",
					FulfillmentActionHistoryType.GETSTATUS);
			return Optional.empty();
		}

		LOG.info("Create Shipment response is: {}", ordersStatus);
		consignmentShipmentResponseService.saveShipmentResponse(consignmentModel, gson.toJson(ordersStatus), Operation.STATUS);
		consignmentShipmentResponseService.saveActionInHistory(consignmentModel,
				"Tracking shipment " + consignmentModel.getTrackingID(), gson.toJson(ordersStatus),
				FulfillmentActionHistoryType.GETSTATUS);

		final String status = getOrdersStatus(ordersStatus);
		return status != null ? Optional.ofNullable(status) : Optional.empty();
	}

	protected void validateRequestData(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel, final Operation operation) throws FulfillentException
	{
		Preconditions.checkArgument(consignmentModel != null, "Consignment model is empty");
		Preconditions.checkArgument(consignmentModel.getOrder() != null, "Order model is empty");
		Preconditions.checkArgument(fulfillmentProviderModel != null, "Lyve provider is not available");
		Preconditions.checkArgument(fulfillmentProviderModel instanceof LyveFulfillmentProviderModel,
				fulfillmentProviderModel.getClass() + " Provider not supported");

		if (!validateLyveProvider((LyveFulfillmentProviderModel) fulfillmentProviderModel))
		{
			LOG.error("Not valid provider, check Lyve provider for missing required data");
			consignmentShipmentResponseService.saveShipmentResponse(consignmentModel,
					FulfillentExceptionType.PROVIDER_NOT_SUPPORTED.getValue(), operation);
			FulfillmentActionHistoryType type = null;
			switch (operation)
			{
				case CREATE:
					type = FulfillmentActionHistoryType.CREATESHIPMENT;
					break;
				case STATUS:
					type = FulfillmentActionHistoryType.GETSTATUS;
					break;
				case UPDATE:
					type = FulfillmentActionHistoryType.UPDATESTATUS;
					break;
				default:
					type = FulfillmentActionHistoryType.PRINTAWB;
			}

			consignmentShipmentResponseService.saveActionInHistory(consignmentModel,
					fulfillmentProviderModel.getCode() + " for " + operation,
					FulfillentExceptionType.PROVIDER_NOT_SUPPORTED.getValue(), type);
			throw new FulfillentException(FulfillentExceptionType.PROVIDER_NOT_SUPPORTED);
		}
	}

	protected boolean validateLyveProvider(final LyveFulfillmentProviderModel provider)
	{
		if (provider == null)
		{
			return false;
		}
		if (!provider.getActive())
		{
			LOG.error("Lyve provider is not active");
			return false;
		}
		if (StringUtils.isBlank(provider.getOrderStatusBaseUrl()))
		{
			LOG.error("Empty base url on Lyve provider");
			return false;
		}
		if (StringUtils.isBlank(provider.getToken()))
		{
			LOG.error("Empty username on Lyve provider");
			return false;
		}

		return true;
	}

	private String getOrdersStatus(final OrderStatusResponse ordersStatus)
	{
		LOG.info("Getting Order status ", gson.toJson(ordersStatus));

		if (CollectionUtils.isEmpty(ordersStatus.getOrders()))
		{
			LOG.error("Order Status Response is Empty");
			return null;
		}
		final OrderStatusInfo orderStatusInfo = ordersStatus.getOrders().entrySet().iterator().next().getValue();

		return orderStatusInfo == null ? null : orderStatusInfo.getOrderStatus() + "";
	}

	/**
	 *
	 */
	private String getTrackingId(final CreateOrderResponse createOrder)
	{
		Preconditions.checkArgument(createOrder != null, "Create Order Response can not be null");

		LOG.info("Getting Tracking Id for orders", gson.toJson(createOrder.getOrders()));

		if (CollectionUtils.isEmpty(createOrder.getOrders()))
		{
			LOG.error("Create Order Response is empty");
			return null;
		}
		final Order order = createOrder.getOrders().entrySet().iterator().next().getValue();

		return order == null ? null : StringUtils.isBlank(order.getTrackId()) ? order.getOrderId() : order.getTrackId();
	}

	/**
	 *
	 */
	private CreateOrderRequest getCreateOrderRequest(final LyveFulfillmentProviderModel lyveFulfillmentProviderModel,
			final ConsignmentModel consignmentModel)
	{
		LOG.info("Create Order Rquest for Consignment {} .......", consignmentModel.getCode());
		final CreateOrderRequest createOrderRequest = new CreateOrderRequest();

		final AbstractOrderModel order = consignmentModel.getOrder();
		final String environment = getConfigurationService().getConfiguration().getString("express.order.environment");
		createOrderRequest
				.setOrderNumber(environment != null ? order.getCode() + "_" + environment.toUpperCase() : order.getCode());
		createOrderRequest.setAmount(order.getTotalPrice());
		createOrderRequest.setCurrency(order.getCurrency().getIsocode());
		createOrderRequest.setPaymentMethod(getPaymentMethod(order.getPaymentMode()));
		createOrderRequest.setPickupNumber(lyveFulfillmentProviderModel.getHash());
		final DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		createOrderRequest.setBusinessDate(df.format(order.getDate()).toString());
		final AddressModel address = order.getDeliveryAddress();

		createOrderRequest.setDestinationName(address.getAddressName());
		createOrderRequest.setDestinationAddress(address.getLine1());
		createOrderRequest.setDestinationArea(address.getArea() == null ? null : address.getArea().getName(Locale.ENGLISH));
		createOrderRequest.setDestinationLatitude(address.getLatitude() == null ? null : address.getLatitude().toString());
		createOrderRequest.setDestinationLongitude(address.getLongitude() == null ? null : address.getLongitude().toString());
		createOrderRequest.setRecipient(address.getFirstname() + " " + address.getLastname());
		createOrderRequest.setRecipientPhone(address.getMobile());
		createOrderRequest.setExtraInfo(address.getNearestLandmark());

		LOG.info("Set Pickup info.......");
		createOrderRequest.setPickupNumber(lyveFulfillmentProviderModel.getPickupNumber());
		createOrderRequest.setPickupName(lyveFulfillmentProviderModel.getPickupName());
		createOrderRequest.setPickupBuilding(lyveFulfillmentProviderModel.getPickupBuilding());
		createOrderRequest.setPickupStreet(lyveFulfillmentProviderModel.getPickupStreet());
		createOrderRequest.setPickupArea(lyveFulfillmentProviderModel.getPickupArea());
		createOrderRequest.setPickupPostalCode(lyveFulfillmentProviderModel.getPickupPostalCode());
		createOrderRequest.setPickupCity(lyveFulfillmentProviderModel.getPickupCity());
		createOrderRequest.setPickupCountry(lyveFulfillmentProviderModel.getPickupCountry());
		createOrderRequest.setPickupAddressNotes(lyveFulfillmentProviderModel.getPickupAddressNotes());
		createOrderRequest.setPickupBusinessVertical(lyveFulfillmentProviderModel.getPickupBusinessVertical());
		createOrderRequest.setPickupContactEmail(lyveFulfillmentProviderModel.getPickupContactEmail());
		createOrderRequest.setPickupContactPhone(lyveFulfillmentProviderModel.getPickupContactPhone());
		createOrderRequest.setPickupOpenTime(lyveFulfillmentProviderModel.getPickupOpenTime());
		createOrderRequest.setPickupCloseTime(lyveFulfillmentProviderModel.getPickupCloseTime());
		createOrderRequest.setPickupRadius(getPickupRadius(lyveFulfillmentProviderModel.getPickupRadius()));
		createOrderRequest.setPickupLattitude(lyveFulfillmentProviderModel.getPickupLattitude());
		createOrderRequest.setPickupLongitude(lyveFulfillmentProviderModel.getPickupLongitude());


		createOrderRequest.setBrand(lyveFulfillmentProviderModel.getBrand());
		createOrderRequest.setIsAutoassign(lyveFulfillmentProviderModel.isIsAutoassign());
		createOrderRequest.setStoreManagerName(lyveFulfillmentProviderModel.getStoreManagerName());
		createOrderRequest.setStoreManagerPhone(lyveFulfillmentProviderModel.getStoreManagerPhone());
		createOrderRequest.setAreaManagerName(lyveFulfillmentProviderModel.getAreaManagerName());
		createOrderRequest.setAreaManagerPhone(lyveFulfillmentProviderModel.getAreaManagerPhone());
		createOrderRequest.setTimezone(lyveFulfillmentProviderModel.getTimezone());




		return createOrderRequest;
	}

	/**
	 *
	 */
	private int getPickupRadius(final String pickupRadius)
	{
		return 0;
	}

	private void saveTrackingIdAndCarrier(final LyveFulfillmentProviderModel lyveFulfillmentProviderModel,
			final ConsignmentModel consignmentModel, final String trackingId)
	{
		LOG.info("Saving Tracking Id {} for consignment {}", trackingId, consignmentModel.getCode());

		modelService.refresh(consignmentModel);
		consignmentModel.setTrackingID(trackingId);
		LOG.info("Saving Carrier {} for consignment {}", lyveFulfillmentProviderModel.getCode(), consignmentModel.getCode());

		final CarrierModel carrierModel = carrierService.create(lyveFulfillmentProviderModel.getCode(),
				lyveFulfillmentProviderModel.getName());
		consignmentModel.setCarrierDetails(carrierModel);
		saveConsignment(consignmentModel);
	}

	private void saveShipmentRequest(final ConsignmentModel consignment, final String request, final Operation operation)
	{
		LOG.info("Saving Shipment Request {} for consignment {}", request, consignment.getCode());

		switch (operation)
		{
			case CREATE:
				LOG.info("Create Shipment Request {}", request);
				consignment.setCreateShipmentRequestBody(request);
				break;
			case UPDATE:
				LOG.info("Update Shipment Request {} ", request);
				consignment.setUpdateShipmentRequestBody(request);
				break;

			default:
				return;
		}
		saveConsignment(consignment);
	}

	private void saveShipmentResponse(final ConsignmentModel consignment, final String response, final Operation operation)
	{
		LOG.info("Saving Shipment Response {} for consignment {}", response, consignment.getCode());

		switch (operation)
		{
			case CREATE:
				consignment.setCreateShipmentResponseBody(response);
				break;
			case UPDATE:
				consignment.setUpdateShipmentResponseBody(response);
				break;
			case STATUS:
				consignment.setStatusShipmentResponseBody(response);
				break;

			default:
				return;
		}
		saveConsignment(consignment);
	}

	private void saveConsignment(final ConsignmentModel consignment)
	{
		LOG.info("Saving Consignment {}", consignment.getCode());
		modelService.save(consignment);
		modelService.refresh(consignment);
	}


	/**
	 *
	 */
	protected PaymentMethod getPaymentMethod(final PaymentModeModel paymentMode)
	{
		LOG.info("Getting Payment Method ...");

		if (paymentMode == null)
		{
			return null;
		}
		LOG.info("Getting Payment Method -> paymentMode {} ....", paymentMode.getCode());
		PaymentMethod paymentMethod = null;
		switch (paymentMode.getCode())
		{
			case "card":
				paymentMethod = PaymentMethod.CARD;
				break;
			case "ccod":
				paymentMethod = PaymentMethod.CARD;
				break;
			case "cod":
				paymentMethod = PaymentMethod.COD;
				break;
			case "continue":
				paymentMethod = PaymentMethod.PAID;
				break;
			case "pic":
				paymentMethod = PaymentMethod.PAID;
				break;
			default:
				break;
		}

		LOG.info("Selected paymentMode {}", paymentMode);

		return paymentMethod;

	}

	@Override
	public Optional<byte[]> printAWB(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws ShipaException
	{
		throw new UnsupportedOperationException();

	}

	@Override
	public Optional<String> updateShipment(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillentException
	{
		throw new UnsupportedOperationException();
	}


	@Override
	public Optional<ConsignmentStatus> updateStatus(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillentException
	{
		final Optional<String> status = getStatus(consignmentModel, fulfillmentProviderModel);
		if (status.isEmpty())
		{
			consignmentShipmentResponseService.saveShipmentResponse(consignmentModel, "No status retrieved!", Operation.STATUS);
			consignmentShipmentResponseService.saveActionInHistory(consignmentModel,
					"Updating the status of " + consignmentModel.getTrackingID(), "No status retrieved!",
					FulfillmentActionHistoryType.UPDATESTATUS);
			return Optional.empty();
		}
		LOG.info("retreived status for {} is {}", consignmentModel.getCode(), status.get());
		final Optional<ConsignmentStatus> mappedStatus = lyveFulfillmentStatusMap.keySet().stream()
				.filter(shipmentStatus -> shipmentStatus.equalsIgnoreCase(status.get())).map(key -> lyveFulfillmentStatusMap.get(key))
				.findAny();
		if (mappedStatus.isEmpty())
		{
			LOG.info("Could not map the {} shipment status", status.get());
			consignmentShipmentResponseService.saveShipmentResponse(consignmentModel,
					status.get() + " not found in the status mapping!", Operation.STATUS);
			consignmentShipmentResponseService.saveActionInHistory(consignmentModel,
					"Updating the status of " + consignmentModel.getTrackingID(), status.get() + " not found in the status mapping!",
					FulfillmentActionHistoryType.UPDATESTATUS);
			return Optional.empty();
		}

		LOG.info("Shipment status {} is mapped with {}", status.get(), mappedStatus.get());
		consignmentShipmentResponseService.saveShipmentResponse(consignmentModel, "Mapped status : " + mappedStatus.get(),
				Operation.UPDATE);
		consignmentShipmentResponseService.updateConsignmentStatus(consignmentModel, mappedStatus.get(), status.get());
		consignmentShipmentResponseService.saveActionInHistory(consignmentModel,
				"Updating the status of " + consignmentModel.getTrackingID(), mappedStatus.get().toString(),
				FulfillmentActionHistoryType.UPDATESTATUS);
		return Optional.of(mappedStatus.get());
	}
}
