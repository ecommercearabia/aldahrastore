package com.aldahra.aldahrafulfillment.service.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;

import com.aldahra.aldahrafulfillment.enums.FulfillmentActionHistoryType;
import com.aldahra.aldahrafulfillment.enums.FulfillmentProviderType;
import com.aldahra.aldahrafulfillment.enums.Operation;
import com.aldahra.aldahrafulfillment.model.FulfillmentProviderModel;
import com.aldahra.aldahrafulfillment.model.FullfillmentHistoryEntryModel;
import com.aldahra.aldahrafulfillment.service.CarrierService;
import com.aldahra.aldahrafulfillment.service.ConsignmentShipmentResponseService;


/**
 * @author monzer
 */
class DefaultConsignmentShipmentResponseService implements ConsignmentShipmentResponseService
{

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "carrierService")
	private CarrierService carrierService;


	public void saveTrackingIdAndCarrier(final FulfillmentProviderModel fulfillmentProviderModel,
			final ConsignmentModel consignmentModel, final String trackingId, final String shipmentOrderId,
			final FulfillmentProviderType type)
	{
		modelService.refresh(consignmentModel);
		consignmentModel.setTrackingID(trackingId);
		consignmentModel.setCarrierDetails(
				carrierService.create(fulfillmentProviderModel.getCode(), fulfillmentProviderModel.getName(), type));
		consignmentModel.setShipmentOrderId(shipmentOrderId);
		saveConsignment(consignmentModel);
	}

	public void saveShipmentRequest(final ConsignmentModel consignment, final String request, final Operation operation)
	{
		switch (operation)
		{
			case CREATE:
				consignment.setCreateShipmentRequestBody(request);
				break;
			default:
				return;
		}
		saveConsignment(consignment);
	}

	public void saveShipmentResponse(final ConsignmentModel consignment, final String response, final Operation operation)
	{
		switch (operation)
		{
			case CREATE:
				consignment.setCreateShipmentResponseBody(response);
				break;
			case STATUS:
				consignment.setStatusShipmentResponseBody(response);
				break;
			case UPDATE:
				consignment.setUpdateShipmentResponseBody(response);
				break;
			default:
				return;
		}
		saveConsignment(consignment);
	}

	public void saveConsignment(final ConsignmentModel consignment)
	{
		modelService.save(consignment);
		modelService.refresh(consignment);
	}

	public void updateConsignmentStatus(final ConsignmentModel consignment, final ConsignmentStatus newStatus,
			final String fulfillmentStatusDescription)
	{
		modelService.refresh(consignment);
		consignment.setFulfillmentStatus(newStatus);
		consignment.setFulfillmentStatusText(fulfillmentStatusDescription);
		if (ConsignmentStatus.DELIVERY_COMPLETED.equals(newStatus))
		{
			consignment.setStatus(newStatus);
		}
		modelService.save(consignment);
	}

	public void saveActionInHistory(final ConsignmentModel consignmentModel, final String request, final String response,
			final FulfillmentActionHistoryType actionType)
	{
		final Set<FullfillmentHistoryEntryModel> history = new HashSet<>();
		if (consignmentModel.getFulfillmentActionHistory() != null)
		{
			history.addAll(consignmentModel.getFulfillmentActionHistory());
		}

		final FullfillmentHistoryEntryModel historyEntry = modelService.create(FullfillmentHistoryEntryModel.class);
		historyEntry.setCarrier(consignmentModel.getCarrierDetails() == null ? "" : consignmentModel.getCarrierDetails().getName());
		historyEntry.setRequest(request);
		historyEntry.setResponse(response);
		historyEntry.setFulfillmentActionType(actionType);
		history.add(historyEntry);

		consignmentModel.setFulfillmentActionHistory(history);
		modelService.save(consignmentModel);
		modelService.refresh(consignmentModel);
	}


}