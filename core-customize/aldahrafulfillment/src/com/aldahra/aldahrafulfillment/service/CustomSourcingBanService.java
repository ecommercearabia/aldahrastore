/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrafulfillment.service;

import de.hybris.platform.warehousing.sourcing.ban.service.SourcingBanService;

import com.aldahra.aldahrafulfillment.model.SourcingBanConfigModel;


/**
 *
 */
public interface CustomSourcingBanService extends SourcingBanService
{
	SourcingBanConfigModel getSourcingBanConfiguration();
}
