package com.aldahra.aldahrafulfillment.lyve.beans.response;

import java.io.Serializable;
import java.util.Map;

import com.aldahra.aldahrafulfillment.lyve.beans.innerbeans.Order;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CreateOrderResponse implements Serializable
{

	@SerializedName("status")
	@Expose
	@JsonProperty(value = "status")
	private int status;

	@SerializedName("vAPI")
	@Expose
	@JsonProperty(value = "vAPI")
	private int vAPI;

	@SerializedName("orders")
	@Expose
	@JsonProperty(value = "orders")
	private Map<String, Order> orders;

	public int getStatus()
	{
		return status;
	}

	public void setStatus(final Integer status)
	{
		this.status = status;
	}

	public int getvAPI()
	{
		return vAPI;
	}

	public void setvAPI(final Integer vAPI)
	{
		this.vAPI = vAPI;
	}

	public Map<String, Order> getOrders()
	{
		return orders;
	}

	public void setOrders(final Map<String, Order> orders)
	{
		this.orders = orders;
	}

	public void setStatus(final int status)
	{
		this.status = status;
	}

	public void setvAPI(final int vAPI)
	{
		this.vAPI = vAPI;
	}

}
