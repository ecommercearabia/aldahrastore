package com.aldahra.aldahrafulfillment.lyve.beans.innerbeans;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Order implements Serializable
{

	@SerializedName("status")
	@Expose
	@JsonProperty(value = "status")
	private int status;

	@SerializedName("vAPI")
	@Expose
	@JsonProperty(value = "vAPI")
	private double vAPI;

	@SerializedName("track_id")
	@Expose
	@JsonProperty(value = "track_id")
	private String trackId;

	@SerializedName("order_id")
	@Expose
	@JsonProperty(value = "order_id")
	private String orderId;

	@SerializedName("pickupNumber")
	@Expose
	@JsonProperty(value = "pickupNumber")
	private String pickupNumber;

	@SerializedName("error_code")
	@Expose
	@JsonProperty(value = "error_code")
	private int errorCode;

	@SerializedName("error_description")
	@Expose
	@JsonProperty(value = "error_description")
	private String errorDescription;

	public int getStatus()
	{
		return status;
	}

	public void setStatus(final int status)
	{
		this.status = status;
	}

	public double getvAPI()
	{
		return vAPI;
	}

	public void setvAPI(final double vAPI)
	{
		this.vAPI = vAPI;
	}

	public String getTrackId()
	{
		return trackId;
	}

	public void setTrackId(final String trackId)
	{
		this.trackId = trackId;
	}

	public String getOrderId()
	{
		return orderId;
	}

	public void setOrderId(final String orderId)
	{
		this.orderId = orderId;
	}

	public String getPickupNumber()
	{
		return pickupNumber;
	}

	public void setPickupNumber(final String pickupNumber)
	{
		this.pickupNumber = pickupNumber;
	}

	public int getErrorCode()
	{
		return errorCode;
	}

	public void setErrorCode(final int errorCode)
	{
		this.errorCode = errorCode;
	}

	public String getErrorDescription()
	{
		return errorDescription;
	}

	public void setErrorDescription(final String errorDescription)
	{
		this.errorDescription = errorDescription;
	}

}
