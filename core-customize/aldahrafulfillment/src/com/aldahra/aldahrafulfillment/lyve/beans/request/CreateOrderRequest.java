package com.aldahra.aldahrafulfillment.lyve.beans.request;


import java.io.Serializable;

import javax.validation.constraints.Size;

import com.aldahra.aldahrafulfillment.lyve.beans.enums.PaymentMethod;
import com.aldahra.aldahrafulfillment.lyve.beans.enums.VehicleType;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CreateOrderRequest implements Serializable
{

	@SerializedName("order_number")
	@Expose
	@Size(max = 250, message = "Order Number can not be more than 250 characters")
	@JsonProperty(value = "order_number")
	private String orderNumber;

	@SerializedName("amount")
	@Expose
	@Size(max = 20, message = "Amount can not be greater than 20 characters")
	@JsonProperty(value = "amount")
	private double amount;

	@SerializedName("payment_method")
	@Expose
	@JsonProperty(value = "payment_method")
	private PaymentMethod paymentMethod;

	@SerializedName("order_source_name")
	@Expose
	@Size(max = 250, message = "Order Number can not be more than 250 characters")
	@JsonProperty(value = "order_source_name")
	private String orderSourceName;

	@SerializedName("vehicle_type")
	@Expose
	@JsonProperty(value = "vehicle_type")
	private VehicleType vehicleType;

	@SerializedName("no_of_packages")
	@Expose
	@Size(max = 20, message = "No of Packages can not be greater than 20 characters")
	@JsonProperty(value = "no_of_packages")
	private int noOfPackages;

	@SerializedName("order_type")
	@Expose
	@Size(max = 250, message = "order Type can not be more than 250 characters")
	@JsonProperty(value = "order_type")
	private String orderType;

	@SerializedName("number_of_bags")
	@Expose
	@Size(max = 20, message = "No of Bags can not be greater than 20 characters")
	@JsonProperty(value = "number_of_bags")
	private int numberOfBags;

	@SerializedName("is_return")
	@Expose
	@JsonProperty(value = "is_return")
	private boolean isReturn;

	@SerializedName("business_date")
	@Expose
	@JsonProperty(value = "business_date")
	private String businessDate;

	@SerializedName("is_aggregator")
	@Expose
	@JsonProperty(value = "is_aggregator")
	private boolean isAggregator;

	@SerializedName("is_schedule")
	@Expose
	@Size(max = 1, message = "IsSchedule can only be 1 or 0")
	@JsonProperty(value = "is_schedule")
	private int isSchedule;

	@SerializedName("preparation_time")
	@Expose
	@Size(max = 3, message = "Preparation Time can not be greater than 3 Character")
	@JsonProperty(value = "preparation_time")
	private int preparationTime;

	@SerializedName("additional_order_number")
	@Expose
	@Size(max = 250, message = "Additional Order Number can not be greater than 250 Character")
	@JsonProperty(value = "additional_order_number")
	private String additionalOrderNumber;

	@SerializedName("pickup_number")
	@Expose
	@Size(max = 100, message = "Pickup Number can not be greater than 100 Character")
	@JsonProperty(value = "pickup_number")
	private String pickupNumber;

	@SerializedName("pickup_name")
	@Expose
	@Size(max = 255, message = "Pickup Name can not be greater than 255 Character")
	@JsonProperty(value = "pickup_name")
	private String pickupName;

	@SerializedName("pickup_building")
	@Expose
	@Size(max = 200, message = "Pickup Building can not be greater than 200 Character")
	@JsonProperty(value = "pickup_building")
	private String pickupBuilding;

	@SerializedName("pickup_street")
	@Expose
	@Size(max = 200, message = "Pickup Street can not be greater than 200 Character")
	@JsonProperty(value = "pickup_street")
	private String pickupStreet;

	@SerializedName("pickup_area")
	@Expose
	@Size(max = 250, message = "Pickup Area can not be greater than 250 Character")
	@JsonProperty(value = "pickup_area")
	private String pickupArea;

	@SerializedName("pickup_postal_code")
	@Expose
	@Size(max = 10, message = "Pickup Postal Code can not be greater than 10 Character")
	@JsonProperty(value = "pickup_postal_code")
	private String pickupPostalCode;

	@SerializedName("pickup_city")
	@Expose
	@Size(max = 25, message = "Pickup City can not be greater than 255 Character")
	private String pickupCity;

	@SerializedName("pickup_country")
	@Expose
	private String pickupCountry;

	@SerializedName("pickup_address_notes")
	@Expose
	private String pickupAddressNotes;

	@SerializedName("pickup_business_vertical")
	@Expose

	private String pickupBusinessVertical;

	@SerializedName("brand")
	@Expose
	private String brand;

	@SerializedName("order_brand_id")
	@Expose
	@Size(max = 20, message = "Order Brand Id can not be greater than 20 characters")
	@JsonProperty(value = "order_brand_id")
	private String orderBrandId;

	@SerializedName("is_autoassign")
	@Expose
	@JsonProperty(value = "is_autoassign")
	private boolean isAutoassign;

	@SerializedName("pickup_contact_email")
	@Expose
	@JsonProperty(value = "pickup_contact_email")
	private String pickupContactEmail;

	@SerializedName("pickup_contact_phone")
	@Expose
	@JsonProperty(value = "pickup_contact_phone")
	private String pickupContactPhone;

	@SerializedName("store_manager_name")
	@Expose
	@JsonProperty(value = "store_manager_name")
	private String storeManagerName;

	@SerializedName("store_manager_phone")
	@Expose
	@JsonProperty(value = "store_manager_phone")
	private String storeManagerPhone;

	@SerializedName("area_manager_name")
	@Expose
	@JsonProperty(value = "area_manager_name")
	private String areaManagerName;

	@SerializedName("area_manager_phone")
	@Expose
	@JsonProperty(value = "area_manager_phone")
	private String areaManagerPhone;

	@SerializedName("target_sos")
	@Expose
	@JsonProperty(value = "target_sos")
	private int targetSos;

	@SerializedName("target_instore")
	@Expose
	@JsonProperty(value = "target_instore")
	private int targetInstore;

	@SerializedName("target_drive")
	@Expose
	@JsonProperty(value = "target_drive")
	private double targetDrive;

	@SerializedName("target_assign")
	@Expose
	@JsonProperty(value = "target_assign")
	private int targetAssign;

	@SerializedName("target_at_customer")
	@Expose
	@JsonProperty(value = "target_at_customer")
	private int targetAtCustomer;

	@SerializedName("target_arrival_at_store")
	@Expose
	@JsonProperty(value = "target_arrival_at_store")
	private int targetArrivalAtStore;

	@SerializedName("target_wait_at_store")
	@Expose
	@JsonProperty(value = "target_wait_at_store")
	private int targetWaitAtStore;

	@SerializedName("timezone")
	@Expose
	@JsonProperty(value = "timezone")
	private String timezone;

	@SerializedName("currency")
	@Expose
	@JsonProperty(value = "currency")
	private String currency;

	@SerializedName("pickup_open_time")
	@Expose
	@JsonProperty(value = "pickup_open_time")
	private String pickupOpenTime;

	@SerializedName("pickup_close_time")
	@Expose
	@JsonProperty(value = "pickup_close_time")
	private String pickupCloseTime;

	@SerializedName("pickup_radius")
	@Expose
	@JsonProperty(value = "pickup_radius")
	private int pickupRadius;

	@SerializedName("pos_number")
	@Expose
	@JsonProperty(value = "pos_number")
	private String posNumber;

	@SerializedName("recipient")
	@Expose
	@JsonProperty(value = "recipient")
	private String recipient;

	@SerializedName("recipient_phone")
	@Expose
	@JsonProperty(value = "recipient_phone")
	private String recipientPhone;

	@SerializedName("destination_name")
	@Expose
	@JsonProperty(value = "destination_name")
	private String destinationName;

	@SerializedName("destination_address")
	@Expose
	@JsonProperty(value = "destination_address")
	private String destinationAddress;

	@SerializedName("destination_latitude")
	@Expose
	@JsonProperty(value = "destination_latitude")
	private String destinationLatitude;

	@SerializedName("destination_longitude")
	@Expose
	@JsonProperty(value = "destination_longitude")
	private String destinationLongitude;

	@SerializedName("destination_area")
	@Expose
	@JsonProperty(value = "destination_area")
	private String destinationArea;

	@SerializedName("pickup_lattitude ")
	@Expose
	@JsonProperty(value = "pickup_lattitude ")
	private String pickupLattitude;

	@SerializedName("pickup_longitude ")
	@Expose
	@JsonProperty(value = "pickup_longitude ")
	private String pickupLongitude;

	@SerializedName("extra_info")
	@Expose
	@JsonProperty(value = "extra_info")
	private String extraInfo;

	public String getOrderNumber()
	{
		return orderNumber;
	}

	public void setOrderNumber(final String orderNumber)
	{
		this.orderNumber = orderNumber;
	}

	public double getAmount()
	{
		return amount;
	}

	public void setAmount(final double amount)
	{
		this.amount = amount;
	}

	public PaymentMethod getPaymentMethod()
	{
		return paymentMethod;
	}

	public void setPaymentMethod(final PaymentMethod paymentMethod)
	{
		this.paymentMethod = paymentMethod;
	}

	public String getOrderSourceName()
	{
		return orderSourceName;
	}

	public void setOrderSourceName(final String orderSourceName)
	{
		this.orderSourceName = orderSourceName;
	}

	public VehicleType getVehicleType()
	{
		return vehicleType;
	}

	public void setVehicleType(final VehicleType vehicleType)
	{
		this.vehicleType = vehicleType;
	}

	public int getNoOfPackages()
	{
		return noOfPackages;
	}

	public void setNoOfPackages(final int noOfPackages)
	{
		this.noOfPackages = noOfPackages;
	}

	public String getOrderType()
	{
		return orderType;
	}

	public void setOrderType(final String orderType)
	{
		this.orderType = orderType;
	}

	public int getNumberOfBags()
	{
		return numberOfBags;
	}

	public void setNumberOfBags(final int numberOfBags)
	{
		this.numberOfBags = numberOfBags;
	}

	public boolean getIsReturn()
	{
		return isReturn;
	}

	public void setIsReturn(final boolean isReturn)
	{
		this.isReturn = isReturn;
	}

	public boolean getIsAggregator()
	{
		return isAggregator;
	}

	public void setIsAggregator(final boolean isAggregator)
	{
		this.isAggregator = isAggregator;
	}

	public int getIsSchedule()
	{
		return isSchedule;
	}

	public void setIsSchedule(final int isSchedule)
	{
		this.isSchedule = isSchedule;
	}

	public int getPreparationTime()
	{
		return preparationTime;
	}

	public void setPreparationTime(final int preparationTime)
	{
		this.preparationTime = preparationTime;
	}

	public String getAdditionalOrderNumber()
	{
		return additionalOrderNumber;
	}

	public void setAdditionalOrderNumber(final String additionalOrderNumber)
	{
		this.additionalOrderNumber = additionalOrderNumber;
	}

	public String getPickupNumber()
	{
		return pickupNumber;
	}

	public void setPickupNumber(final String pickupNumber)
	{
		this.pickupNumber = pickupNumber;
	}

	public String getPickupName()
	{
		return pickupName;
	}

	public void setPickupName(final String pickupName)
	{
		this.pickupName = pickupName;
	}

	public String getPickupBuilding()
	{
		return pickupBuilding;
	}

	public void setPickupBuilding(final String pickupBuilding)
	{
		this.pickupBuilding = pickupBuilding;
	}

	public String getPickupStreet()
	{
		return pickupStreet;
	}

	public void setPickupStreet(final String pickupStreet)
	{
		this.pickupStreet = pickupStreet;
	}

	public String getPickupArea()
	{
		return pickupArea;
	}

	public void setPickupArea(final String pickupArea)
	{
		this.pickupArea = pickupArea;
	}

	public String getPickupPostalCode()
	{
		return pickupPostalCode;
	}

	public void setPickupPostalCode(final String pickupPostalCode)
	{
		this.pickupPostalCode = pickupPostalCode;
	}

	public String getPickupCity()
	{
		return pickupCity;
	}

	public void setPickupCity(final String pickupCity)
	{
		this.pickupCity = pickupCity;
	}

	public String getPickupCountry()
	{
		return pickupCountry;
	}

	public void setPickupCountry(final String pickupCountry)
	{
		this.pickupCountry = pickupCountry;
	}

	public String getPickupAddressNotes()
	{
		return pickupAddressNotes;
	}

	public void setPickupAddressNotes(final String pickupAddressNotes)
	{
		this.pickupAddressNotes = pickupAddressNotes;
	}

	public String getPickupBusinessVertical()
	{
		return pickupBusinessVertical;
	}

	public void setPickupBusinessVertical(final String pickupBusinessVertical)
	{
		this.pickupBusinessVertical = pickupBusinessVertical;
	}

	public String getBrand()
	{
		return brand;
	}

	public void setBrand(final String brand)
	{
		this.brand = brand;
	}

	public String getOrderBrandId()
	{
		return orderBrandId;
	}

	public void setOrderBrandId(final String orderBrandId)
	{
		this.orderBrandId = orderBrandId;
	}

	public boolean getIsAutoassign()
	{
		return isAutoassign;
	}

	public void setIsAutoassign(final boolean isAutoassign)
	{
		this.isAutoassign = isAutoassign;
	}

	public String getPickupContactEmail()
	{
		return pickupContactEmail;
	}

	public void setPickupContactEmail(final String pickupContactEmail)
	{
		this.pickupContactEmail = pickupContactEmail;
	}

	public String getPickupContactPhone()
	{
		return pickupContactPhone;
	}

	public void setPickupContactPhone(final String pickupContactPhone)
	{
		this.pickupContactPhone = pickupContactPhone;
	}

	public String getStoreManagerName()
	{
		return storeManagerName;
	}

	public void setStoreManagerName(final String storeManagerName)
	{
		this.storeManagerName = storeManagerName;
	}

	public String getStoreManagerPhone()
	{
		return storeManagerPhone;
	}

	public void setStoreManagerPhone(final String storeManagerPhone)
	{
		this.storeManagerPhone = storeManagerPhone;
	}

	public String getAreaManagerName()
	{
		return areaManagerName;
	}

	public void setAreaManagerName(final String areaManagerName)
	{
		this.areaManagerName = areaManagerName;
	}

	public String getAreaManagerPhone()
	{
		return areaManagerPhone;
	}

	public void setAreaManagerPhone(final String areaManagerPhone)
	{
		this.areaManagerPhone = areaManagerPhone;
	}

	public int getTargetSos()
	{
		return targetSos;
	}

	public void setTargetSos(final int targetSos)
	{
		this.targetSos = targetSos;
	}

	public int getTargetInstore()
	{
		return targetInstore;
	}

	public void setTargetInstore(final int targetInstore)
	{
		this.targetInstore = targetInstore;
	}

	public double getTargetDrive()
	{
		return targetDrive;
	}

	public void setTargetDrive(final double targetDrive)
	{
		this.targetDrive = targetDrive;
	}

	public int getTargetAssign()
	{
		return targetAssign;
	}

	public void setTargetAssign(final int targetAssign)
	{
		this.targetAssign = targetAssign;
	}

	public int getTargetAtCustomer()
	{
		return targetAtCustomer;
	}

	public void setTargetAtCustomer(final int targetAtCustomer)
	{
		this.targetAtCustomer = targetAtCustomer;
	}

	public int getTargetArrivalAtStore()
	{
		return targetArrivalAtStore;
	}

	public void setTargetArrivalAtStore(final int targetArrivalAtStore)
	{
		this.targetArrivalAtStore = targetArrivalAtStore;
	}

	public int getTargetWaitAtStore()
	{
		return targetWaitAtStore;
	}

	public void setTargetWaitAtStore(final int targetWaitAtStore)
	{
		this.targetWaitAtStore = targetWaitAtStore;
	}

	public String getTimezone()
	{
		return timezone;
	}

	public void setTimezone(final String timezone)
	{
		this.timezone = timezone;
	}

	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	public String getPickupOpenTime()
	{
		return pickupOpenTime;
	}

	public void setPickupOpenTime(final String pickupOpenTime)
	{
		this.pickupOpenTime = pickupOpenTime;
	}

	public String getPickupCloseTime()
	{
		return pickupCloseTime;
	}

	public void setPickupCloseTime(final String pickupCloseTime)
	{
		this.pickupCloseTime = pickupCloseTime;
	}

	public int getPickupRadius()
	{
		return pickupRadius;
	}

	public void setPickupRadius(final int pickupRadius)
	{
		this.pickupRadius = pickupRadius;
	}

	public String getPosNumber()
	{
		return posNumber;
	}

	public void setPosNumber(final String posNumber)
	{
		this.posNumber = posNumber;
	}

	public String getRecipient()
	{
		return recipient;
	}

	public void setRecipient(final String recipient)
	{
		this.recipient = recipient;
	}

	public String getRecipientPhone()
	{
		return recipientPhone;
	}

	public void setRecipientPhone(final String recipientPhone)
	{
		this.recipientPhone = recipientPhone;
	}

	public String getDestinationName()
	{
		return destinationName;
	}

	public void setDestinationName(final String destinationName)
	{
		this.destinationName = destinationName;
	}

	public String getDestinationAddress()
	{
		return destinationAddress;
	}

	public void setDestinationAddress(final String destinationAddress)
	{
		this.destinationAddress = destinationAddress;
	}

	public String getDestinationLatitude()
	{
		return destinationLatitude;
	}

	public void setDestinationLatitude(final String destinationLatitude)
	{
		this.destinationLatitude = destinationLatitude;
	}

	public String getDestinationLongitude()
	{
		return destinationLongitude;
	}

	public void setDestinationLongitude(final String destinationLongitude)
	{
		this.destinationLongitude = destinationLongitude;
	}

	public String getDestinationArea()
	{
		return destinationArea;
	}

	public void setDestinationArea(final String destinationArea)
	{
		this.destinationArea = destinationArea;
	}

	public String getPickupLattitude()
	{
		return pickupLattitude;
	}

	public void setPickupLattitude(final String pickupLattitude)
	{
		this.pickupLattitude = pickupLattitude;
	}

	public String getPickupLongitude()
	{
		return pickupLongitude;
	}

	public void setPickupLongitude(final String pickupLongitude)
	{
		this.pickupLongitude = pickupLongitude;
	}

	public String getExtraInfo()
	{
		return extraInfo;
	}

	public void setExtraInfo(final String extraInfo)
	{
		this.extraInfo = extraInfo;
	}

	public String getBusinessDate()
	{
		return businessDate;
	}

	public void setBusinessDate(final String businessDate)
	{
		this.businessDate = businessDate;
	}

	public void setReturn(final boolean isReturn)
	{
		this.isReturn = isReturn;
	}

	public void setAggregator(final boolean isAggregator)
	{
		this.isAggregator = isAggregator;
	}

	public void setAutoassign(final boolean isAutoassign)
	{
		this.isAutoassign = isAutoassign;
	}

}
