package com.aldahra.aldahrafulfillment.lyve.beans.request;

import java.io.Serializable;
import java.util.List;

import com.aldahra.aldahrafulfillment.lyve.beans.innerbeans.OrderNumberInfo;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderStatusRequest implements Serializable
{

	@SerializedName("hash")
	@Expose
	private String hash;

	@SerializedName("data")
	@Expose
	@JsonProperty("data")
	private List<OrderNumberInfo> orderNumbers;

	public OrderStatusRequest()
	{
		super();
	}

	public String getHash()
	{
		return hash;
	}

	public void setHash(final String hash)
	{
		this.hash = hash;
	}

	public List<OrderNumberInfo> getOrderNumbers()
	{
		return orderNumbers;
	}

	public void setOrderNumbers(final List<OrderNumberInfo> orderNumbers)
	{
		this.orderNumbers = orderNumbers;
	}

}
