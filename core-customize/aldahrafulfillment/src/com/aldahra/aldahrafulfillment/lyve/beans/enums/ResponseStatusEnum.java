package com.aldahra.aldahrafulfillment.lyve.beans.enums;

public enum ResponseStatusEnum
{

	SUCCESS(0), FAILED(1);

	private int code;

	private ResponseStatusEnum(final int code)
	{

		this.code = code;
	}

	public int getCode()
	{
		return code;
	}

}
