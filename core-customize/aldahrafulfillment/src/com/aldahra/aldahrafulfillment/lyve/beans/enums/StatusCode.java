package com.aldahra.aldahrafulfillment.lyve.beans.enums;

import java.util.Arrays;


public enum StatusCode
{
	DONE("Done", 3), CANCELED("Canceled", 4), UNASSIGNED("Unassigned", 5), ASSIGNED("Assigned", 10), IN_PROGRESS("In Progress",
			12), PICKED_BY_CUSTOMER("Picked By Customer", 34), UNKNOWN_CODE("Unknown Statuc Code", -1);

	private String message;
	private int code;

	StatusCode(final String message, final int code)
	{
		this.code = code;
		this.message = message;
	}

	public String getMessage()
	{
		return message;
	}

	public int getCode()
	{
		return code;
	}

	public static StatusCode getStatusCodeByCode(final int code)
	{
		return Arrays.asList(StatusCode.values()).stream().filter(e -> e.getCode() == code).findFirst()
				.orElse(StatusCode.UNKNOWN_CODE);
	}
}
