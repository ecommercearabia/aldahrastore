package com.aldahra.aldahrafulfillment.lyve.beans.response;

import java.io.Serializable;
import java.util.Map;

import com.aldahra.aldahrafulfillment.lyve.beans.enums.StatusCode;
import com.aldahra.aldahrafulfillment.lyve.beans.innerbeans.OrderStatusInfo;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderStatusResponse implements Serializable
{

	@SerializedName("status")
	@Expose
	private int status;

	@SerializedName("vAPI")
	@Expose
	private int vAPI;

	@SerializedName("orders")
	@Expose
	@JsonProperty(value = "orders")
	private Map<String, OrderStatusInfo> orders;

	@Expose
	@SerializedName("error_code")
	@JsonProperty(value = "error_code")
	private int errorCode;

	@Expose
	@SerializedName("error_description")
	@JsonProperty(value = "error_description")
	private String errorDescription;

	public int getStatus()
	{
		return status;
	}

	public StatusCode getDisplayStatus()
	{
		return StatusCode.getStatusCodeByCode(this.status);
	}

	public void setStatus(final int status)
	{
		this.status = status;
	}

	public int getvAPI()
	{
		return vAPI;
	}

	public void setvAPI(final int vAPI)
	{
		this.vAPI = vAPI;
	}

	public Map<String, OrderStatusInfo> getOrders()
	{
		return orders;
	}

	public void setOrders(final Map<String, OrderStatusInfo> orders)
	{
		this.orders = orders;
	}

	/**
	 * @return the errorCode
	 */
	public int getErrorCode()
	{
		return errorCode;
	}

	/**
	 * @param errorCode
	 *           the errorCode to set
	 */
	public void setErrorCode(final int errorCode)
	{
		this.errorCode = errorCode;
	}

	/**
	 * @return the errorDescription
	 */
	public String getErrorDescription()
	{
		return errorDescription;
	}

	/**
	 * @param errorDescription
	 *           the errorDescription to set
	 */
	public void setErrorDescription(final String errorDescription)
	{
		this.errorDescription = errorDescription;
	}



}
