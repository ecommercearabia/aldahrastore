package com.aldahra.aldahrafulfillment.lyve.beans.innerbeans;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderStatusInfo implements Serializable
{

	@SerializedName("status")
	@Expose
	@JsonProperty(value = "status")
	private int status;

	@SerializedName("driver")
	@Expose
	private String driver;

	@SerializedName("driver_id")
	@Expose
	@JsonProperty(value = "driver_id")
	private String driverId;

	@SerializedName("package_number")
	@Expose
	private String packageNumber;

	@SerializedName("driver_phone")
	@Expose
	private String driverPhone;

	@SerializedName("photo_url")
	@Expose
	private String photoUrl;

	@SerializedName("order_status")
	@Expose
	private int orderStatus;

	@SerializedName("vAPI")
	@Expose
	private int vAPI;

	@SerializedName("lat")
	@Expose
	private String lat;

	@SerializedName("lng")
	@Expose
	private String lng;

	@SerializedName("GPSTimeStamp")
	@Expose
	@JsonProperty(value = "GPSTimeStamp")
	private String gPSTimeStamp;

	@SerializedName("error_code")
	@Expose
	@JsonProperty(value = "error_code")
	private int errorCode;

	@SerializedName("error_description")
	@Expose
	@JsonProperty(value = "error_description")
	private String errorDescription;

	public int getStatus()
	{
		return status;
	}

	public void setStatus(final int status)
	{
		this.status = status;
	}

	public String getDriver()
	{
		return driver;
	}

	public void setDriver(final String driver)
	{
		this.driver = driver;
	}

	public String getDriverId()
	{
		return driverId;
	}

	public void setDriverId(final String driverId)
	{
		this.driverId = driverId;
	}

	public String getPackageNumber()
	{
		return packageNumber;
	}

	public void setPackageNumber(final String packageNumber)
	{
		this.packageNumber = packageNumber;
	}

	public String getDriverPhone()
	{
		return driverPhone;
	}

	public void setDriverPhone(final String driverPhone)
	{
		this.driverPhone = driverPhone;
	}

	public String getPhotoUrl()
	{
		return photoUrl;
	}

	public void setPhotoUrl(final String photoUrl)
	{
		this.photoUrl = photoUrl;
	}

	public int getOrderStatus()
	{
		return orderStatus;
	}

	public void setOrderStatus(final int orderStatus)
	{
		this.orderStatus = orderStatus;
	}

	public int getvAPI()
	{
		return vAPI;
	}

	public void setvAPI(final int vAPI)
	{
		this.vAPI = vAPI;
	}

	public String getLat()
	{
		return lat;
	}

	public void setLat(final String lat)
	{
		this.lat = lat;
	}

	public String getLng()
	{
		return lng;
	}

	public void setLng(final String lng)
	{
		this.lng = lng;
	}

	public String getGPSTimeStamp()
	{
		return gPSTimeStamp;
	}

	public void setGPSTimeStamp(final String gPSTimeStamp)
	{
		this.gPSTimeStamp = gPSTimeStamp;
	}



	public int getErrorCode()
	{
		return errorCode;
	}

	public void setErrorCode(final int errorCode)
	{
		this.errorCode = errorCode;
	}

	public String getErrorDescription()
	{
		return errorDescription;
	}

	public void setErrorDescription(final String errorDescription)
	{
		this.errorDescription = errorDescription;
	}

}
