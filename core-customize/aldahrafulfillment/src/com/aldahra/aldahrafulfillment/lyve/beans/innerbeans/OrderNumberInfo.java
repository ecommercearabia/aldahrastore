package com.aldahra.aldahrafulfillment.lyve.beans.innerbeans;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderNumberInfo implements Serializable
{
	@SerializedName("order_number")
	@Expose
	@JsonProperty(value = "order_number")
	private String orderNumber;

	public OrderNumberInfo()
	{
		super();
	}

	public OrderNumberInfo(final String orderNumber)
	{
		this.orderNumber = orderNumber;
	}

	public void setOrderNumber(final String orderNumber)
	{
		this.orderNumber = orderNumber;
	}

	public String getOrderNumber()
	{
		return orderNumber;
	}

}
