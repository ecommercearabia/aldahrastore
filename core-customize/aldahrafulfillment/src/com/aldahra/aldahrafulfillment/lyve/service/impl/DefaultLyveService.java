package com.aldahra.aldahrafulfillment.lyve.service.impl;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.aldahra.aldahrafulfillment.lyve.beans.innerbeans.Order;
import com.aldahra.aldahrafulfillment.lyve.beans.innerbeans.OrderNumberInfo;
import com.aldahra.aldahrafulfillment.lyve.beans.innerbeans.OrderStatusInfo;
import com.aldahra.aldahrafulfillment.lyve.beans.request.CreateOrderRequest;
import com.aldahra.aldahrafulfillment.lyve.beans.request.OrderStatusRequest;
import com.aldahra.aldahrafulfillment.lyve.beans.response.CreateOrderResponse;
import com.aldahra.aldahrafulfillment.lyve.beans.response.OrderStatusResponse;
import com.aldahra.aldahrafulfillment.lyve.exception.LyveException;
import com.aldahra.aldahrafulfillment.lyve.exception.enums.LyveExceptionType;
import com.aldahra.aldahrafulfillment.lyve.service.LyveService;
import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import kong.unirest.UnirestException;


public class DefaultLyveService implements LyveService
{
	private static final String NULL_URL_MSG = "Url Can not be empty";
	private static final String NULL_TOKEN_MSG = "Token Can not be empty";
	private static final String NULL_HASH_MSG = "Hash Can not be empty";
	private static final String NULL_ORDER_NUMBER_MSG = "Order Number not be empty";
	private static final String CONTENT_TYPE = "application/json";
	private static final Set<Integer> INVALID_CODES = new HashSet<>(
			Arrays.asList(101, 102, 103, 201, 202, 301, 302, 303, 304, 305, 306, 307, 308, 309, 310, 311, 312, 313, 314, 315, 400,
					400, 401, 401, 401, 404, 404, 422, 422, 422, 422, 422, 422, 422, 422, 422, 422, 422, 422, 422));

	private static final Logger LOG = LoggerFactory.getLogger(DefaultLyveService.class);
	private static Gson gson;

	static
	{
		gson = new GsonBuilder().create();
	}

	@Override
	public CreateOrderResponse createOrder(final String url, final String token, final CreateOrderRequest request)
			throws LyveException
	{

		Preconditions.checkArgument(Strings.isNotBlank(url), NULL_URL_MSG);
		Preconditions.checkArgument(Strings.isNotBlank(token), NULL_TOKEN_MSG);
		LOG.info("Create Order Request", request.toString());

		final String requestBody = getGson().toJson(request);
		HttpResponse<String> response;
		try
		{
			response = Unirest.post(url).header("Accept", CONTENT_TYPE).header("Content-Type", CONTENT_TYPE)
					.header("x-access-token", token).body(requestBody).asString();

		}
		catch (final UnirestException e)
		{
			LOG.error(e.getMessage());
			throw new LyveException(LyveExceptionType.BAD_REQUEST, LyveExceptionType.BAD_REQUEST.getMessage(), 0, null);
		}

		if (response == null)
		{
			throw new LyveException(LyveExceptionType.NULL_RESPONSE, LyveExceptionType.NULL_RESPONSE.getMessage(), 0, null);
		}
		if (Strings.isBlank(response.getBody()))
		{
			throw new LyveException(LyveExceptionType.NULL_RESPONSE, "Response body is empty" + response.getStatus(), 0, null);
		}

		if (!is2xxStatus(response.getStatus()))
		{
			throw new LyveException(LyveExceptionType.NULL_RESPONSE, "Response body is empty" + response.getBody(), 0, null);
		}

		final CreateOrderResponse responseBean = gson.fromJson(response.getBody(), CreateOrderResponse.class);
		validateCreateOrderResponse(responseBean);

		return responseBean;
	}

	@Override
	public OrderStatusResponse getOrdersStatus(final String url, final String hash, final OrderStatusRequest request)
			throws LyveException
	{

		Preconditions.checkArgument(Strings.isNotBlank(url), NULL_URL_MSG);
		Preconditions.checkArgument(Strings.isNotBlank(hash), NULL_HASH_MSG);

		request.setHash(hash);
		LOG.info("Get Order Status Request", request.toString());
		final String requestBody = getGson().toJson(request);
		HttpResponse<String> response;
		try
		{
			response = Unirest.post(url).header("Accept", CONTENT_TYPE).header("Content-Type", CONTENT_TYPE).body(requestBody)
					.asString();

		}
		catch (final UnirestException e)
		{
			throw new LyveException(LyveExceptionType.BAD_REQUEST, LyveExceptionType.BAD_REQUEST.getMessage(), 0, null);
		}

		if (response == null)
		{
			throw new LyveException(LyveExceptionType.NULL_RESPONSE, LyveExceptionType.NULL_RESPONSE.getMessage(), 0, null);
		}

		if (Strings.isBlank(response.getBody()))
		{
			throw new LyveException(LyveExceptionType.NULL_RESPONSE, "Response body is empty" + response.getStatus(), 0, null);
		}

		if (!is2xxStatus(response.getStatus()))
		{
			throw new LyveException(LyveExceptionType.NULL_RESPONSE, "Response body is empty" + response.getBody(), 0, null);
		}

		OrderStatusResponse responseBean = null;

		try
		{
			responseBean = gson.fromJson(response.getBody(), OrderStatusResponse.class);
		}
		catch (final IllegalStateException e)
		{
			throw new LyveException(LyveExceptionType.NULL_RESPONSE, "Response body is empty" + response.getBody(), 0, null);
		}

		validateOrderStatusResponse(responseBean);

		return responseBean;

	}

	/**
	 *
	 */
	private boolean is2xxStatus(final int status)
	{
		return status >= 200 && status <= 299;
	}

	@Override
	public OrderStatusResponse getOrderStatus(final String url, final String hash, final String orderNumber) throws LyveException
	{
		Preconditions.checkArgument(Strings.isNotBlank(orderNumber), NULL_ORDER_NUMBER_MSG);
		LOG.info("Get Order Status for Order [{}]", orderNumber);
		final OrderStatusRequest orderStatusRequest = new OrderStatusRequest();

		final List<OrderNumberInfo> orderNumbers = new ArrayList<>();
		orderNumbers.add(new OrderNumberInfo(orderNumber));

		orderStatusRequest.setHash(hash);
		orderStatusRequest.setOrderNumbers(orderNumbers);

		final OrderStatusResponse ordersStatus = getOrdersStatus(url, hash, orderStatusRequest);

		validateOrderStatusResponse(ordersStatus);

		return ordersStatus;
	}

	@Override
	public void validateOrderStatusResponse(final OrderStatusResponse ordersStatus) throws LyveException
	{
		LOG.info("Validate Order Status Response");
		if (ordersStatus == null)
		{
			LOG.error("Request is empty");

			throw new LyveException(LyveExceptionType.EMPTY_DATA, LyveExceptionType.EMPTY_DATA.toString(), 0, null);
		}
		if (CollectionUtils.isEmpty(ordersStatus.getOrders()))
		{
			LOG.error("Request is empty");

			throw new LyveException(LyveExceptionType.EMPTY_DATA, LyveExceptionType.EMPTY_DATA.toString(), 0, ordersStatus);
		}
		final String orderCode = ordersStatus.getOrders().keySet().iterator().next();

		final OrderStatusInfo orderStatusInfo = ordersStatus.getOrders().get(orderCode);

		if (orderStatusInfo == null)
		{
			LOG.error("Order Status Info for Order: {} is Empty", orderCode);

			throw new LyveException(LyveExceptionType.EMPTY_DATA, LyveExceptionType.EMPTY_DATA.toString(), 0, ordersStatus);
		}
		if (orderStatusInfo.getErrorCode() == 201)
		{
			LOG.error("Order Status Info Request for Order: {} is invalid, {}", orderCode, orderStatusInfo.getErrorDescription());
			throw new LyveException(LyveExceptionType.SHIPMENT_NOT_CREATED, orderStatusInfo.getErrorDescription(),
					ordersStatus.getStatus(), ordersStatus);
		}
		if (INVALID_CODES.contains(orderStatusInfo.getErrorCode()))
		{
			LOG.error("Order Status Info Request for Order: {} is invalid, {}", orderCode, orderStatusInfo.getErrorDescription());
			throw new LyveException(LyveExceptionType.BAD_REQUEST, orderStatusInfo.getErrorDescription(), ordersStatus.getStatus(),
					ordersStatus);
		}
		else
		{
			LOG.info("Order Status Response is Valid");
		}


	}

	@Override
	public OrderStatusResponse getOrdersStatus(final String url, final String hash, final List<String> orderNumbers)
			throws LyveException
	{

		final OrderStatusRequest orderStatusRequest = new OrderStatusRequest();
		orderStatusRequest.setHash(hash);
		orderNumbers.stream().forEach(e -> orderStatusRequest.getOrderNumbers().add(new OrderNumberInfo(e)));

		return getOrdersStatus(url, hash, orderStatusRequest);
	}

	@Override
	public void validateCreateOrderResponse(final CreateOrderResponse responseBean) throws LyveException
	{
		LOG.info("Start Validate Create Order Response");
		if (responseBean == null)
		{
			LOG.error(" Create Order Respons is Empty");
			throw new LyveException(LyveExceptionType.EMPTY_DATA, LyveExceptionType.EMPTY_DATA.toString(), 0, null);
		}
		if (CollectionUtils.isEmpty(responseBean.getOrders()))
		{
			LOG.error(" Create Order Respons is Empty");
			throw new LyveException(LyveExceptionType.EMPTY_DATA, LyveExceptionType.EMPTY_DATA.toString(), 0, responseBean);
		}
		final String orderCode = responseBean.getOrders().keySet().iterator().next();

		final Order order = responseBean.getOrders().get(orderCode);

		if (order == null)
		{
			LOG.error("Order is null");
			throw new LyveException(LyveExceptionType.EMPTY_DATA, LyveExceptionType.EMPTY_DATA.toString(), 0, responseBean);
		}

		if (INVALID_CODES.contains(order.getErrorCode()))
		{
			LOG.error("Order Status Info Request for Order: {} is invalid, {}", orderCode, order.getErrorDescription());
			throw new LyveException(LyveExceptionType.BAD_REQUEST, order.getErrorDescription(), order.getStatus(), responseBean);
		}
	}

	protected static Gson getGson()
	{
		return gson;
	}
}
