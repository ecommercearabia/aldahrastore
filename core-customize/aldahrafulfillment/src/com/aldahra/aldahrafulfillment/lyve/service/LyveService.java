package com.aldahra.aldahrafulfillment.lyve.service;

import java.util.List;

import com.aldahra.aldahrafulfillment.lyve.beans.request.CreateOrderRequest;
import com.aldahra.aldahrafulfillment.lyve.beans.request.OrderStatusRequest;
import com.aldahra.aldahrafulfillment.lyve.beans.response.CreateOrderResponse;
import com.aldahra.aldahrafulfillment.lyve.beans.response.OrderStatusResponse;
import com.aldahra.aldahrafulfillment.lyve.exception.LyveException;


public interface LyveService
{

	public CreateOrderResponse createOrder(String url, String token, CreateOrderRequest request) throws LyveException;

	public OrderStatusResponse getOrdersStatus(String url, String hash, OrderStatusRequest request) throws LyveException;

	public OrderStatusResponse getOrdersStatus(String url, String hash, List<String> orderNumbers) throws LyveException;

	public OrderStatusResponse getOrderStatus(String url, String hash, String orderNumber) throws LyveException;

	public void validateOrderStatusResponse(OrderStatusResponse ordersStatus) throws LyveException;

	public void validateCreateOrderResponse(CreateOrderResponse responseBean) throws LyveException;

}
