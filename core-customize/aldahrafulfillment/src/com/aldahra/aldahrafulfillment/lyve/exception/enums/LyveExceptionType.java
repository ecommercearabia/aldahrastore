package com.aldahra.aldahrafulfillment.lyve.exception.enums;

public enum LyveExceptionType
{

	NULL_RESPONSE("Empty Response"), BAD_REQUEST("Bad Request"), EMPTY_DATA("Empty Data"), SHIPMENT_NOT_CREATED(
			" Shipment does not exist ");

	private String message;

	private LyveExceptionType(final String message)
	{
		this.message = message;
	}

	public String getMessage()
	{
		return message;
	}
}
