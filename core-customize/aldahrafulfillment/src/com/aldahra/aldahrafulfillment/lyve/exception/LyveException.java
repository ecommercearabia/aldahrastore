package com.aldahra.aldahrafulfillment.lyve.exception;

import com.aldahra.aldahrafulfillment.lyve.exception.enums.LyveExceptionType;
import com.google.gson.Gson;


@SuppressWarnings("serial")
public class LyveException extends Exception
{
	private final int errorCode;
	private final Object respons;


	private final LyveExceptionType type;
	private static Gson gson = new Gson();

	public LyveException(final LyveExceptionType type, final String message, final int errorCode, final Object respons)
	{
		super(message);
		this.type = type;
		this.errorCode = errorCode;
		this.respons = respons;
	}

	public LyveExceptionType getType()
	{
		return type;
	}

	public int getErrorCode()
	{
		return errorCode;
	}

	/**
	 * @return the responsData
	 */
	public String getResponsData()
	{
		return respons == null ? null : gson.toJson(respons);
	}

	/**
	 * @return the responsData
	 */
	public Object getRespons()
	{
		return respons;
	}


}
