package com.aldahra.aldahrafulfillment.shipa.model.history;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 *
 * @author mohammad-abu-muhasien
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class HistoryLogResponse
{
	private String orderId;
	private History history;
	private int status;
	private String info;

	public HistoryLogResponse()
	{

	}

	public HistoryLogResponse(final String orderId, final History history, final int status, final String info)
	{
		super();
		this.orderId = orderId;
		this.history = history;
		this.status = status;
		this.info = info;
	}

	public String getOrderId()
	{
		return orderId;
	}

	public void setOrderId(final String orderId)
	{
		this.orderId = orderId;
	}

	public History getHistory()
	{
		return history;
	}

	public void setHistory(final History history)
	{
		this.history = history;
	}

	public int getStatus()
	{
		return status;
	}

	public void setStatus(final int status)
	{
		this.status = status;
	}

	public String getInfo()
	{
		return info;
	}

	public void setInfo(final String info)
	{
		this.info = info;
	}

	class History
	{
		private String code;
		private String time;
		private String info;

		public History()
		{

		}

		public History(final String code, final String time, final String info)
		{
			super();
			this.code = code;
			this.time = time;
			this.info = info;
		}

		public String getCode()
		{
			return code;
		}

		public void setCode(final String code)
		{
			this.code = code;
		}

		public String getTime()
		{
			return time;
		}

		public void setTime(final String time)
		{
			this.time = time;
		}

		public String getInfo()
		{
			return info;
		}

		public void setInfo(final String info)
		{
			this.info = info;
		}

	}
}
