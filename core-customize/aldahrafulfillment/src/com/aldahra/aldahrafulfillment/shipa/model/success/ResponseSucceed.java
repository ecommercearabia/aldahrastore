package com.aldahra.aldahrafulfillment.shipa.model.success;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;



/**
 *
 * @author mohammad-abu-muhasien
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseSucceed {

	/** The id. */
	private String id;

	/** The code. */
	private int code;

	/** The info. */
	private String info;

	/** The delivery info. */
	private DeliveryInfo deliveryInfo;

	/**
	 * Instantiates a new response succeed.
	 *
	 * @param id the id
	 * @param code the code
	 * @param info the info
	 * @param deliveryInfo the delivery info
	 */
	public ResponseSucceed(final String id, final int code, final String info, final DeliveryInfo deliveryInfo) {
		super();
		this.id = id;
		this.code = code;
		this.info = info;
		this.deliveryInfo = deliveryInfo;
	}

	/**
	 * Instantiates a new response succeed.
	 */
	public ResponseSucceed() {

	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(final String id) {
		this.id = id;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public int getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(final int code) {
		this.code = code;
	}

	/**
	 * Gets the info.
	 *
	 * @return the info
	 */
	public String getInfo() {
		return info;
	}

	/**
	 * Sets the info.
	 *
	 * @param info the new info
	 */
	public void setInfo(final String info) {
		this.info = info;
	}

	/**
	 * Gets the delivery info.
	 *
	 * @return the delivery info
	 */
	public DeliveryInfo getDeliveryInfo() {
		return deliveryInfo;
	}

	/**
	 * Sets the delivery info.
	 *
	 * @param deliveryInfo the new delivery info
	 */
	public void setDeliveryInfo(final DeliveryInfo deliveryInfo) {
		this.deliveryInfo = deliveryInfo;
	}

	@Override
	public String toString()
	{
		return "ResponseSucceed [id=" + id + ", code=" + code + ", info=" + info + ", deliveryInfo=" + deliveryInfo + "]";
	}


}
