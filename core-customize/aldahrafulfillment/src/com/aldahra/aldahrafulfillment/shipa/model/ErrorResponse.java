package com.aldahra.aldahrafulfillment.shipa.model;


import com.aldahra.aldahrafulfillment.shipa.enums.ShipaExceptionType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 * @author Mohammad-abu-muhasien
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorResponse
{

	/** The message. */
	private String message;

	/** The status code. */
	private int statusCode;

	/** The type. */
	private ShipaExceptionType type;

	/**
	 * Instantiates a new error response.
	 */
	public ErrorResponse()
	{

	}

	/**
	 * Instantiates a new error response.
	 *
	 * @param message
	 *           the message
	 * @param statusCode
	 *           the status code
	 * @param type
	 *           the type
	 */
	public ErrorResponse(final String message, final int statusCode, final ShipaExceptionType type)
	{
		super();
		this.message = message;
		this.statusCode = statusCode;
		this.type = type;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage()
	{
		return message;
	}

	/**
	 * Sets the message.
	 *
	 * @param message
	 *           the new message
	 */
	public void setMessage(final String message)
	{
		this.message = message;
	}

	/**
	 * Gets the status code.
	 *
	 * @return the status code
	 */
	public int getStatusCode()
	{
		return statusCode;
	}

	/**
	 * Sets the status code.
	 *
	 * @param statusCode
	 *           the new status code
	 */
	public void setStatusCode(final int statusCode)
	{
		this.statusCode = statusCode;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public ShipaExceptionType getType()
	{
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type
	 *           the new type
	 */
	public void setType(final ShipaExceptionType type)
	{
		this.type = type;
	}

}
