/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrafulfillment.shipa.model.getorder;

import com.aldahra.aldahrafulfillment.shipa.model.success.DeliveryInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 * @author amjad.shati@erabia.com
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetOrderResponse
{
	private int code;
	private DeliveryInfo deliveryInfo;
	private String id;
	private String info;

	/**
	 * @return the code
	 */
	public int getCode()
	{
		return code;
	}

	/**
	 * @param code
	 *           the code to set
	 */
	public void setCode(final int code)
	{
		this.code = code;
	}

	/**
	 * @return the deliveryInfo
	 */
	public DeliveryInfo getDeliveryInfo()
	{
		return deliveryInfo;
	}

	/**
	 * @param deliveryInfo
	 *           the deliveryInfo to set
	 */
	public void setDeliveryInfo(final DeliveryInfo deliveryInfo)
	{
		this.deliveryInfo = deliveryInfo;
	}

	/**
	 * @return the id
	 */
	public String getId()
	{
		return id;
	}

	/**
	 * @param id
	 *           the id to set
	 */
	public void setId(final String id)
	{
		this.id = id;
	}

	/**
	 * @return the info
	 */
	public String getInfo()
	{
		return info;
	}

	/**
	 * @param info
	 *           the info to set
	 */
	public void setInfo(final String info)
	{
		this.info = info;
	}

	@Override
	public String toString()
	{
		return "GetOrderResponse [code=" + code + ", deliveryInfo=" + deliveryInfo + ", id=" + id + ", info=" + info + "]";
	}

	/**
	 *
	 */
	public GetOrderResponse(final int code, final DeliveryInfo deliveryInfo, final String id, final String info)
	{
		super();
		this.code = code;
		this.deliveryInfo = deliveryInfo;
		this.id = id;
		this.info = info;
	}

	/**
	 *
	 */
	public GetOrderResponse()
	{
		super();
	}


}
