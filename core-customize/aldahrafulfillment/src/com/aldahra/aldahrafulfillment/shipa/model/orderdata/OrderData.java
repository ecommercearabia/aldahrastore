package com.aldahra.aldahrafulfillment.shipa.model.orderdata;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 * @author mohammad-abu-muhasien
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderData
{
	@Expose
	@SerializedName("estimatedDeliveryDate")
	private String estimatedDeliveryDate;
	@Expose
	@SerializedName("height")
	private double height;
	@Expose
	@SerializedName("length")
	private double length;
	@Expose
	@SerializedName("quantity")
	private long quantity;
	@Expose
	@SerializedName("ready")
	private boolean ready;
	@Expose
	@SerializedName("recipientAddress")
	private String recipientAddress;
	@Expose
	@SerializedName("recipientCoordinates")
	private String recipientCoordinates;
	@Expose
	@SerializedName("weight")
	private double weight;
	@Expose
	@SerializedName("width")
	private double width;

	public OrderData()
	{

	}

	public OrderData(final String estimatedDeliveryDate, final double height, final double length, final long quantity,
			final boolean ready, final String recipientAddress, final String recipientCoordinates, final double weight,
			final double width)
	{
		super();
		this.estimatedDeliveryDate = estimatedDeliveryDate;
		this.height = height;
		this.length = length;
		this.quantity = quantity;
		this.ready = ready;
		this.recipientAddress = recipientAddress;
		this.recipientCoordinates = recipientCoordinates;
		this.weight = weight;
		this.width = width;
	}

	public long getQuantity()
	{
		return quantity;
	}

	public void setQuantity(final long quantity)
	{
		this.quantity = quantity;
	}

	public String getEstimatedDeliveryDate()
	{
		return estimatedDeliveryDate;
	}

	public void setEstimatedDeliveryDate(final String estimatedDeliveryDate)
	{
		this.estimatedDeliveryDate = estimatedDeliveryDate;
	}

	public double getHeight()
	{
		return height;
	}

	public void setHeight(final double height)
	{
		this.height = height;
	}

	public double getLength()
	{
		return length;
	}

	public void setLength(final double length)
	{
		this.length = length;
	}

	public boolean isReady()
	{
		return ready;
	}

	public void setReady(final boolean ready)
	{
		this.ready = ready;
	}

	public String getRecipientAddress()
	{
		return recipientAddress;
	}

	public void setRecipientAddress(final String recipientAddress)
	{
		this.recipientAddress = recipientAddress;
	}

	public String getRecipientCoordinates()
	{
		return recipientCoordinates;
	}

	public void setRecipientCoordinates(final String recipientCoordinates)
	{
		this.recipientCoordinates = recipientCoordinates;
	}

	public double getWeight()
	{
		return weight;
	}

	public void setWeight(final double weight)
	{
		this.weight = weight;
	}

	public double getWidth()
	{
		return width;
	}

	public void setWidth(final double width)
	{
		this.width = width;
	}

	@Override
	public String toString()
	{
		return "OrderData [estimatedDeliveryDate=" + estimatedDeliveryDate + ", height=" + height + ", length=" + length
				+ ", quantity=" + quantity + ", ready=" + ready + ", recipientAddress=" + recipientAddress + ", recipientCoordinates="
				+ recipientCoordinates + ", weight=" + weight + ", width=" + width + "]";
	}

}
