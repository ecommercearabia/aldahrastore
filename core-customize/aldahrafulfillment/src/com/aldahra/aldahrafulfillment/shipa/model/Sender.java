package com.aldahra.aldahrafulfillment.shipa.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 *
 * @author mohammad-abu-muhasien
 * @author monzer
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Sender extends OrderUser
{

	/**
	 *
	 */
	public Sender()
	{
	}
}
