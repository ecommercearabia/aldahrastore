package com.aldahra.aldahrafulfillment.shipa.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 *
 * @author mohammad-abu-muhasien
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Order
{
	private String id;
	private double amount;
	private String paymentMethod;
	private String description;
	private String typeDelivery;
	private Sender sender;
	private Recipient recipient;
	private String goodsValue;
	private String orderCategory;
	private final String vehicleType;

	private String area;
	private String district;
	private String tower;
	private String apartmentNumber;

	public Order()
	{
		this.orderCategory = "SAMEDAY";
		this.vehicleType = "car";
	}

	public double getAmount()
	{
		return amount;
	}

	public void setAmount(final double amount)
	{
		this.amount = amount;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(final String description)
	{
		this.description = description;
	}

	public String getGoodsValue()
	{
		return goodsValue;
	}

	public void setGoodsValue(final String goodsValue)
	{
		this.goodsValue = goodsValue;
	}

	public String getId()
	{
		return id;
	}

	public void setId(final String id)
	{
		this.id = id;
	}

	public String getPaymentMethod()
	{
		return paymentMethod;
	}

	public void setPaymentMethod(final String paymentMethod)
	{
		this.paymentMethod = paymentMethod;
	}

	public Recipient getRecipient()
	{
		return recipient;
	}

	public void setRecipient(final Recipient recipient)
	{
		this.recipient = recipient;
	}

	public Sender getSender()
	{
		return sender;
	}

	public void setSender(final Sender sender)
	{
		this.sender = sender;
	}

	public String getTypeDelivery()
	{
		return typeDelivery;
	}

	public void setTypeDelivery(final String typeDelivery)
	{
		this.typeDelivery = typeDelivery;
	}

	/**
	 * @return the area
	 */
	public String getArea()
	{
		return area;
	}

	/**
	 * @param area
	 *           the area to set
	 */
	public void setArea(final String area)
	{
		this.area = area;
	}

	/**
	 * @return the district
	 */
	public String getDistrict()
	{
		return district;
	}

	/**
	 * @param district
	 *           the district to set
	 */
	public void setDistrict(final String district)
	{
		this.district = district;
	}

	/**
	 * @return the tower
	 */
	public String getTower()
	{
		return tower;
	}

	/**
	 * @param tower
	 *           the tower to set
	 */
	public void setTower(final String tower)
	{
		this.tower = tower;
	}

	/**
	 * @return the apartmentNumber
	 */
	public String getApartmentNumber()
	{
		return apartmentNumber;
	}

	/**
	 * @param apartmentNumber
	 *           the apartmentNumber to set
	 */
	public void setApartmentNumber(final String apartmentNumber)
	{
		this.apartmentNumber = apartmentNumber;
	}

	@Override
	public String toString()
	{
		return "Order [amount=" + amount + ", description=" + description + ", goodsValue=" + goodsValue + ", id=" + id
				+ ", paymentMethod=" + paymentMethod + ", recipient=" + recipient + ", sender=" + sender + ", typeDelivery="
				+ typeDelivery + ", area=" + area + ", district=" + district + ", tower=" + tower + ", apartmentNumber="
				+ apartmentNumber + ", orderCategory=" + orderCategory + "]";
	}

	/**
	 * @return the orderCategory
	 */
	public String getOrderCategory()
	{
		return orderCategory;
	}

	/**
	 * @param orderCategory
	 *           the orderCategory to set
	 */
	public void setOrderCategory(final String orderCategory)
	{
		this.orderCategory = orderCategory;
	}

}
