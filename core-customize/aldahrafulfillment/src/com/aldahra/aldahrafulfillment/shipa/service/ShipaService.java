package com.aldahra.aldahrafulfillment.shipa.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.aldahra.aldahrafulfillment.shipa.exception.ShipaException;
import com.aldahra.aldahrafulfillment.shipa.model.Order;
import com.aldahra.aldahrafulfillment.shipa.model.cancel.CancelReason;
import com.aldahra.aldahrafulfillment.shipa.model.cancel.ResponseStatus;
import com.aldahra.aldahrafulfillment.shipa.model.getorder.GetOrderResponse;
import com.aldahra.aldahrafulfillment.shipa.model.history.HistoryLogResponse;
import com.aldahra.aldahrafulfillment.shipa.model.orderdata.OrderData;
import com.aldahra.aldahrafulfillment.shipa.model.success.ResponseSucceed;



/**
 *
 * @author mohammad-abu-muhasien
 */
public interface ShipaService
{

	/**
	 * this method Send orders using SHIPA api , you can send multi orders in the same time ,just pass it through array
	 * of order.
	 *
	 * @param apikey
	 *           the apikey, is the key of the project
	 * @param orders
	 *           the orders ,are the orders than you want to send or submit
	 * @return the list of ResponseSucceed which represent the data of the sent orders
	 */
	public List<ResponseSucceed> sendOrders(final String baseUrl, String apikey, Order[] orders) throws ShipaException;

	/**
	 * Gets the order by reference ID.
	 *
	 * @param apikey
	 *           the apikey,is the key of the project.
	 * @param referenceID
	 *           the reference ID of the product that given by SHIPA for example: GAE201806010001.
	 * @return the order that represented by the reference ID.
	 */
	public GetOrderResponse getOrderByReferenceID(final String baseUrl, String apikey, String referenceID) throws ShipaException;

	/**
	 * Cancel order by reference ID.
	 *
	 * @@param apikey the apikey,is the key of the project.
	 * @param referenceID
	 *           the reference ID of the product that given by SHIPA for example: GAE201806010001.
	 * @param reason
	 *           the reason that you want to cancel the order because of it . the the value are limited Other values will
	 *           be rejected : A- (15) represent "Changed in mind" B- 16 represent "Package not available" C- 17
	 *           represent "Others"
	 * @return the cancel response that represent the cancel status.
	 */
	public ResponseStatus cancelOrderByReferenceID(final String baseUrl, String apikey, String referenceID, CancelReason reason)
			throws ShipaException;

	/**
	 * Cancel order by order ID.
	 *
	 * @param apikey
	 *           the apikey
	 * @param orderId
	 *           the order id, that given by the user like :123 , keep in mind that id given by the user and the
	 *           reference id given by SHIPA API
	 * @param reason
	 *           the reason that you want to cancel the order because of it . the the value are limited Other values will
	 *           be rejected : A- (15) represent "Changed in mind" B- 16 represent "Package not available" C- 17
	 *           represent "Others"
	 * @return the cancel response that represent the cancel status.
	 */
	public ResponseStatus cancelOrderByOrderID(final String baseUrl, String apikey, String orderId, CancelReason reason)
			throws ShipaException;

	/**
	 * Download order by reference ID.
	 *
	 * @param apikey
	 *           the apikey
	 * @param referenceID
	 *           the reference ID of the product that given by SHIPA for example: GAE201806010001.
	 * @param copies
	 *           represent number of the copies and The following values are allowed: 1, 2, 3
	 * @param template
	 *           represent the template of the pdf and The The following values are allowed: standard, sticker-6x4,
	 *           international
	 * @return the response entity .
	 */
	public ResponseEntity<byte[]> downloadOrderByReferenceID(final String baseUrl, String apikey, String referenceID, int copies,
			String template) throws ShipaException;

	/**
	 * Gets the history by order ID.Provides timely sorted list of events for specific order for customer reference
	 *
	 * @param apikey
	 *           the apikey is the key of the project
	 * @param orderId
	 *           the order id, that given by the user like :123 , keep in mind that id given by the user and the
	 *           reference id given by SHIPA API
	 * @return the HistoryLogResponse by order ID
	 */
	public HistoryLogResponse getHistoryByOrderID(final String baseUrl, String apikey, String orderId) throws ShipaException;

	/**
	 * Gets the history by ReferenceID ID.Provides timely sorted list of events for specific order for customer reference
	 *
	 * @param apikey
	 *           the apikey is the key of the project
	 * @param orderId
	 *           the order id, that given by the user like :123 , keep in mind that id given by the user and the
	 *           reference id given by SHIPA API
	 * @return the HistoryLogResponse by order ID
	 */

	public HistoryLogResponse getHistoryByReferenceID(final String baseUrl, String apikey, String referanceID)
			throws ShipaException;

	/**
	 * update OrderData By ReferenceId
	 *
	 * @param apikey
	 *           the apikey is the key of the project
	 * @param referenceID
	 *           the reference ID of the product that given by SHIPA for example: GAE201806010001.
	 * @param orderData
	 * @return ResponseStatus represent the operation status
	 */

	public ResponseStatus updateOrderDataByReferenceId(final String baseUrl, String apikey, String referanceID,
			OrderData orderData) throws ShipaException;


}
