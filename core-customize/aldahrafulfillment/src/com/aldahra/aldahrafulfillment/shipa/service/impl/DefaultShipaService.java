package com.aldahra.aldahrafulfillment.shipa.service.impl;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import com.aldahra.aldahrafulfillment.exception.enums.FulfillentExceptionType;
import com.aldahra.aldahrafulfillment.shipa.exception.ShipaException;
import com.aldahra.aldahrafulfillment.shipa.model.Order;
import com.aldahra.aldahrafulfillment.shipa.model.cancel.CancelReason;
import com.aldahra.aldahrafulfillment.shipa.model.cancel.ResponseStatus;
import com.aldahra.aldahrafulfillment.shipa.model.getorder.GetOrderResponse;
import com.aldahra.aldahrafulfillment.shipa.model.history.HistoryLogResponse;
import com.aldahra.aldahrafulfillment.shipa.model.orderdata.OrderData;
import com.aldahra.aldahrafulfillment.shipa.model.success.ResponseSucceed;
import com.aldahra.aldahrafulfillment.shipa.service.ShipaService;
import com.aldahra.aldahrawebserviceapi.util.WebServiceApiUtil;
import com.google.gson.GsonBuilder;




/**
 * @author mohammad-abu-muhasien
 */
@Service
public class DefaultShipaService implements ShipaService
{
	/**
	 *
	 */
	private static final String INVALID_ORDER_ID = "Invalid orderId";
	/**
	 *
	 */
	private static final String INVALID_REFERENCE_ID = "Invalid referenceID";
	/**
	 *
	 */
	private static final String INVALID_APIKEY = "Invalid apikey";
	private static final Logger LOGGER = Logger.getLogger(DefaultShipaService.class);
	private static final String CUSTOMERREF = "customerref";

	/** The Constant HISTORY. */
	private static final String HISTORY = "history";

	/** The Constant CANCEL. */
	private static final String CANCEL = "cancel";

	/** The Constant ORDERS. */
	private static final String ORDERS = "orders";

	/** The Constant API_KEY. */
	private static final String API_KEY = "?apikey=";

	/** The Constant PATH_SEPARATOR. */
	private static final String PATH_SEPARATOR = "/";

	private static final String ORDERS_LIST_CAN_NOT_BE_EMPTY = "orders List can not be empty";

	private static final String APIKEY_CAN_NOT_BE_EMPTY = "apikey can not be empty";

	/**
	 * this method Send orders using SHIPA api , you can send multi orders in the same time ,just pass it through array
	 * of order.
	 *
	 * @param apikey
	 *           the apikey, is the key of the project
	 * @param orders
	 *           the orders ,are the orders than you want to send or submit
	 * @return the list of ResponseSucceed which represent the data of the sent orders
	 */
	@Override
	public List<ResponseSucceed> sendOrders(final String baseUrl, final String apikey, final Order[] orders) throws ShipaException
	{
		if (apikey == null || apikey.trim().isEmpty())
		{
			LOGGER.error(ORDERS_LIST_CAN_NOT_BE_EMPTY);
			throw new ShipaException(FulfillentExceptionType.ORDERS_LIST_CAN_NOT_BE_EMPTY);
		}
		if (orders == null || orders.length == 0)
		{
			LOGGER.error("Invalid order data list");
			throw new ShipaException(FulfillentExceptionType.INVALID_API_KEY);
		}
		final StringBuilder builder = new StringBuilder(baseUrl);
		builder.append(ORDERS).append(API_KEY).append(apikey);
		try
		{
			final ResponseEntity<?> result = WebServiceApiUtil.httPOST(builder.toString(), orders, null, ResponseSucceed[].class);
			if (result.getStatusCodeValue() != 200)
			{
				LOGGER.error("Creating Shipment not successful");
			}
			return Arrays.asList((ResponseSucceed[]) result.getBody());

		}
		catch (final HttpClientErrorException e)
		{
			LOGGER.error("An HttpClientErrorException occurred: " + e.getMessage());
			throw new ShipaException(FulfillentExceptionType.BAD_REQUEST, e);
		}

	}

	/**
	 * Gets the order by reference ID.
	 *
	 * @param apikey
	 *           the apikey,is the key of the project.
	 * @param referenceID
	 *           the reference ID of the product that given by SHIPA for example: GAE201806010001.
	 * @return the order that represented by the reference ID.
	 */
	@Override
	public GetOrderResponse getOrderByReferenceID(final String baseUrl, final String apikey, final String referenceID)
			throws ShipaException
	{
		if (apikey == null || apikey.trim().isEmpty())
		{
			LOGGER.error(INVALID_APIKEY);
			throw new ShipaException(FulfillentExceptionType.INVALID_API_KEY);

		}
		if (referenceID == null || referenceID.trim().isEmpty())
		{
			LOGGER.error(INVALID_REFERENCE_ID);
			throw new ShipaException(FulfillentExceptionType.REFERENCE_ID_CAN_NOT_BE_NULL_OR_EMPTY);

		}
		final StringBuilder builder = new StringBuilder(baseUrl);
		builder.append(ORDERS).append(PATH_SEPARATOR).append(referenceID).append(API_KEY).append(apikey);
		try
		{
			return (GetOrderResponse) WebServiceApiUtil.httpGet(builder.toString(), null, GetOrderResponse.class).getBody();
		}
		catch (final HttpClientErrorException e)
		{
			LOGGER.error("An HttpClientErrorException occurred: " + e.getMessage());
			throw new ShipaException(FulfillentExceptionType.BAD_REQUEST, e);
		}

	}

	/**
	 * Cancel order by reference ID.
	 *
	 * @@param apikey the apikey,is the key of the project.
	 * @param referenceID
	 *           the reference ID of the product that given by SHIPA for example: GAE201806010001.
	 * @param reason
	 *           the reason that you want to cancel the order because of it . the the value are limited Other values will
	 *           be rejected : A- (15) represent "Changed in mind" B- 16 represent "Package not available" C- 17
	 *           represent "Others"
	 * @return the cancel response that represent the cancel status.
	 */
	@Override
	public ResponseStatus cancelOrderByReferenceID(final String baseUrl, final String apikey, final String referenceID,
			final CancelReason reason) throws ShipaException
	{
		if (apikey == null || apikey.trim().isEmpty())
		{
			LOGGER.error(INVALID_APIKEY);
			throw new ShipaException(FulfillentExceptionType.INVALID_API_KEY);

		}
		if (referenceID == null || referenceID.trim().isEmpty())
		{

			LOGGER.error(INVALID_REFERENCE_ID);
			throw new ShipaException(FulfillentExceptionType.REFERENCE_ID_CAN_NOT_BE_NULL_OR_EMPTY);
		}
		if (reason == null)
		{

			LOGGER.error("Reason is required");
			throw new ShipaException(FulfillentExceptionType.CANCEL_REASON_IS_REQUIRED);
		}
		try
		{
			final StringBuilder builder = new StringBuilder(baseUrl);
			builder.append(ORDERS).append(PATH_SEPARATOR).append(referenceID).append(PATH_SEPARATOR).append(CANCEL).append(API_KEY)
					.append(apikey);

			return (ResponseStatus) WebServiceApiUtil.httPOST(builder.toString(), reason, null, ResponseStatus.class).getBody();
		}
		catch (final Exception e)
		{
			throw new ShipaException(FulfillentExceptionType.BAD_REQUEST);
		}

	}

	/**
	 * Download order by reference ID.
	 *
	 * @param apikey
	 *           the apikey
	 * @param referenceID
	 *           the reference ID of the product that given by SHIPA for example: GAE201806010001.
	 * @param copies
	 *           represent number of the copies and The following values are allowed: 1, 2, 3
	 * @param template
	 *           represent the template of the pdf and The The following values are allowed: standard, sticker-6x4,
	 *           international
	 * @return the response entity .
	 */
	@Override
	public ResponseEntity<byte[]> downloadOrderByReferenceID(final String baseUrl, final String apikey, final String referenceID,
			final int copies, final String template) throws ShipaException
	{
		if (apikey == null || apikey.trim().isEmpty())
		{
			LOGGER.error(INVALID_APIKEY);
			throw new ShipaException(FulfillentExceptionType.INVALID_API_KEY);

		}
		if (referenceID == null || referenceID.trim().isEmpty())
		{
			LOGGER.error(INVALID_REFERENCE_ID);
			throw new ShipaException(FulfillentExceptionType.REFERENCE_ID_CAN_NOT_BE_NULL_OR_EMPTY);

		}

		if (copies == 0)
		{
			LOGGER.error("Invalid number of copies");
			throw new ShipaException(FulfillentExceptionType.INVALID_NUMBER_OF_COPIES);

		}
		if (template == null || template.trim().isEmpty())
		{
			LOGGER.error("Invalid template");
			throw new ShipaException(FulfillentExceptionType.INVALID_TEMPLET);

		}

		try
		{

			final StringBuilder builder = new StringBuilder(baseUrl);
			builder.append(ORDERS).append(PATH_SEPARATOR).append(referenceID).append(PATH_SEPARATOR).append("pdf").append(API_KEY)
					.append(apikey);


			return WebServiceApiUtil.httpGetPdf(builder.toString(), null);

		}
		catch (final Exception e)
		{
			throw new ShipaException(FulfillentExceptionType.BAD_REQUEST);
		}

	}

	/**
	 * Cancel order by order ID.
	 *
	 * @param apikey
	 *           the apikey
	 * @param orderId
	 *           the order id, that given by the user like :123 , keep in mind that id given by the user and the
	 *           reference id given by SHIPA API
	 * @param reason
	 *           the reason that you want to cancel the order because of it . the the value are limited Other values will
	 *           be rejected : A- (15) represent "Changed in mind" B- 16 represent "Package not available" C- 17
	 *           represent "Others"
	 * @return the cancel response that represent the cancel status.
	 */
	@Override
	public ResponseStatus cancelOrderByOrderID(final String baseUrl, final String apikey, final String orderId,
			final CancelReason reason) throws ShipaException
	{

		if (apikey == null || apikey.trim().isEmpty())
		{
			LOGGER.error(INVALID_APIKEY);
			throw new ShipaException(FulfillentExceptionType.INVALID_API_KEY);

		}
		if (orderId == null || orderId.trim().isEmpty())
		{
			LOGGER.error(INVALID_ORDER_ID);
			throw new ShipaException(FulfillentExceptionType.ORDER_ID_CAN_NOT_BE_NULL_OR_EMPTY);

		}
		if (reason == null)
		{
			LOGGER.error("Reason is required");
			throw new ShipaException(FulfillentExceptionType.CANCEL_REASON_IS_REQUIRED);

		}
		try
		{
			final StringBuilder builder = new StringBuilder(baseUrl);
			builder.append(CUSTOMERREF).append(PATH_SEPARATOR).append(orderId).append(PATH_SEPARATOR).append(CANCEL).append(API_KEY)
					.append(apikey);

			return (ResponseStatus) WebServiceApiUtil.httPOST(builder.toString(), reason, null, ResponseStatus.class).getBody();
		}
		catch (final Exception e)
		{
			throw new ShipaException(FulfillentExceptionType.BAD_REQUEST);
		}

	}

	/**
	 * Gets the history by order ID. Provides timely sorted list of events for specific order for customer id
	 *
	 * @param apikey
	 *           the apikey is the key of the project
	 * @param orderId
	 *           the order id, that given by the user like :123 , keep in mind that id given by the user and the
	 *           reference id given by SHIPA API
	 * @return the HistoryLogResponse by order ID
	 */
	@Override
	public HistoryLogResponse getHistoryByOrderID(final String baseUrl, final String apikey, final String orderId)
			throws ShipaException
	{
		if (apikey == null || apikey.trim().isEmpty())
		{
			LOGGER.error(INVALID_APIKEY);
			throw new ShipaException(FulfillentExceptionType.INVALID_API_KEY);

		}
		if (orderId == null || orderId.trim().isEmpty())
		{
			LOGGER.error(INVALID_ORDER_ID);
			throw new ShipaException(FulfillentExceptionType.ORDER_ID_CAN_NOT_BE_NULL_OR_EMPTY);

		}
		try
		{
			final StringBuilder builder = new StringBuilder(baseUrl);
			builder.append(CUSTOMERREF).append(PATH_SEPARATOR).append(orderId).append(PATH_SEPARATOR).append(HISTORY).append(API_KEY)
					.append(apikey);

			return (HistoryLogResponse) WebServiceApiUtil.httpGet(builder.toString(), null, HistoryLogResponse.class).getBody();

		}
		catch (final Exception e)
		{

			throw new ShipaException(FulfillentExceptionType.BAD_REQUEST);
		}

	}

	/**
	 * Gets the history by ReferenceID ID.Provides timely sorted list of events for specific order for customer reference
	 *
	 * @param apikey
	 *           the apikey is the key of the project
	 * @param orderId
	 *           the order id, that given by the user like :123 , keep in mind that id given by the user and the
	 *           reference id given by SHIPA API
	 * @return the HistoryLogResponse by order ID
	 */
	@Override
	public HistoryLogResponse getHistoryByReferenceID(final String baseUrl, final String apikey, final String referanceID)
			throws ShipaException
	{
		if (apikey == null || apikey.trim().isEmpty())
		{
			LOGGER.error(INVALID_APIKEY);
			throw new ShipaException(FulfillentExceptionType.INVALID_API_KEY);

		}
		if (referanceID == null || referanceID.trim().isEmpty())
		{
			LOGGER.error(INVALID_REFERENCE_ID);
			throw new ShipaException(FulfillentExceptionType.REFERENCE_ID_CAN_NOT_BE_NULL_OR_EMPTY);

		}
		try
		{
			final StringBuilder builder = new StringBuilder(baseUrl);
			builder.append(ORDERS).append(PATH_SEPARATOR).append(referanceID).append(PATH_SEPARATOR).append(HISTORY).append(API_KEY)
					.append(apikey);

			return (HistoryLogResponse) WebServiceApiUtil.httpGet(builder.toString(), null, HistoryLogResponse.class).getBody();
		}
		catch (final Exception e)
		{

			throw new ShipaException(FulfillentExceptionType.BAD_REQUEST);
		}

	}

	/**
	 * update OrderData By ReferenceId
	 *
	 * @param apikey
	 *           the apikey is the key of the project
	 * @param referenceID
	 *           the reference ID of the product that given by SHIPA for example: GAE201806010001.
	 * @param orderData
	 *           represent the data than you want to add
	 * @return ResponseStatus represent the operation status
	 */
	@Override
	public ResponseStatus updateOrderDataByReferenceId(final String baseUrl, final String apikey, final String referanceID,
			final OrderData orderData) throws ShipaException
	{
		if (apikey == null || apikey.trim().isEmpty())
		{
			LOGGER.error(INVALID_APIKEY);
			throw new ShipaException(FulfillentExceptionType.INVALID_API_KEY);

		}
		if (referanceID == null || referanceID.trim().isEmpty())
		{
			LOGGER.error(INVALID_REFERENCE_ID);
			throw new ShipaException(FulfillentExceptionType.REFERENCE_ID_CAN_NOT_BE_NULL_OR_EMPTY);

		}
		if (orderData == null)
		{
			LOGGER.error("Invalid orderData");
			throw new ShipaException(FulfillentExceptionType.ORDER_DATA_CAN_NOT_BE_NULL_OR_EMPTY);

		}
		try
		{
			final StringBuilder builder = new StringBuilder(baseUrl);
			builder.append(ORDERS).append(PATH_SEPARATOR).append(referanceID).append(API_KEY).append(apikey);
			final String req = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setPrettyPrinting().create()
					.toJson(orderData).toString();
			return (ResponseStatus) WebServiceApiUtil.httpPatch(builder.toString(), req, null, ResponseStatus.class).getBody();

		}
		catch (final HttpClientErrorException e)
		{
			LOGGER.error("An HttpClientErrorException occurred: " + e.getMessage());
			throw new ShipaException(FulfillentExceptionType.BAD_REQUEST, e);
		}

	}




}
