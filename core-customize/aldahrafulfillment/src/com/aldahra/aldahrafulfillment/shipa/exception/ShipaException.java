package com.aldahra.aldahrafulfillment.shipa.exception;

import com.aldahra.aldahrafulfillment.exception.FulfillentException;
import com.aldahra.aldahrafulfillment.exception.enums.FulfillentExceptionType;


public class ShipaException extends FulfillentException
{


	/**
	 *
	 */
	public ShipaException(final FulfillentExceptionType type)
	{
		super(type);
	}

	/**
	 *
	 */
	public ShipaException(final FulfillentExceptionType type, final Throwable throwable)
	{
		super(type, throwable);
	}

	private static final long serialVersionUID = 1L;



}
