package com.aldahra.aldahrafulfillment.exception.enums;

public enum FulfillentExceptionType
{
	ID_IS_ALREADY_EXIST("Id is already exist"), INVALID_API_KEY("invalid api key"), ORDERS_LIST_CAN_NOT_BE_EMPTY(
			"Order List can not be empty"), ORDER_DATA_CAN_NOT_BE_NULL_OR_EMPTY(
					"Order date can not be null or empty"), REFERENCE_ID_CAN_NOT_BE_NULL_OR_EMPTY(
							"Reference can not be null or empty"), CANCEL_REASON_IS_REQUIRED(
									"cancel reason is required"), INVALID_NUMBER_OF_COPIES("Invalid number of copies"), INVALID_TEMPLET(
											"Invalid Template"), ORDER_ID_CAN_NOT_BE_NULL_OR_EMPTY(
													"Order can not be null or empty"), BAD_REQUEST("BAD_REQUEST"), MISSING_COORDINATES(
															"Missing Coordinates on either delivery address or the Area or the City"), PROVIDER_NOT_SUPPORTED(
																	"fulfillment providor is not supported for this store");

	private String value;

	private FulfillentExceptionType(final String value)
	{
		this.value = value;
	}

	public String getValue()
	{
		return value;
	}

}
