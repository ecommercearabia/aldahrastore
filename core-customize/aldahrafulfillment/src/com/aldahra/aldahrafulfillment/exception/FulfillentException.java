package com.aldahra.aldahrafulfillment.exception;

import com.aldahra.aldahrafulfillment.exception.enums.FulfillentExceptionType;


public class FulfillentException extends Exception
{

	private static final long serialVersionUID = 1L;
	private final FulfillentExceptionType type;

	public FulfillentException(final FulfillentExceptionType type)
	{
		super(type != null ? type.getValue() : null);
		this.type = type;

	}

	public FulfillentException(final FulfillentExceptionType type, final Throwable throwable)
	{
		super(type != null ? type.getValue() : null, throwable);
		this.type = type;

	}

	public FulfillentExceptionType getType()
	{
		return type;
	}

}
