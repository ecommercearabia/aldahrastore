/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrafulfillment.interceptor;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PersistenceOperation;
import de.hybris.platform.warehousing.interceptor.ConsignmentPackagingInfoPrepareInterceptor;
import de.hybris.platform.warehousing.model.PackagingInfoModel;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class CustomConsignmentPackagingInfoPrepareInterceptor extends ConsignmentPackagingInfoPrepareInterceptor
{
	@Override
	public void onPrepare(final ConsignmentModel consignment, final InterceptorContext context) throws InterceptorException
	{
		if (context.isNew(consignment))
		{
			final PackagingInfoModel packagingInfo = getModelService().create(PackagingInfoModel.class);
			packagingInfo.setConsignment(consignment);

			final double grossWeight = consignment.getConsignmentEntries().stream().filter(e -> e != null && e.getQuantity() != null)
					.mapToDouble(entry -> entry.getWeight() * entry.getQuantity()).summaryStatistics().getSum();

			packagingInfo.setGrossWeight(String.valueOf(grossWeight));
			packagingInfo.setHeight(DEFAULT_VALUE);
			packagingInfo.setLength(DEFAULT_VALUE);
			packagingInfo.setWidth(DEFAULT_VALUE);
			packagingInfo.setDimensionUnit(DEFAULT_DIMENSION_UNIT);
			packagingInfo.setWeightUnit(DEFAULT_WEIGHT_UNIT);
			packagingInfo.setCreationtime(getTimeService().getCurrentTime());
			packagingInfo.setModifiedtime(getTimeService().getCurrentTime());
			packagingInfo.setInsuredValue(DEFAULT_VALUE);
			context.registerElementFor(packagingInfo, PersistenceOperation.SAVE);
			consignment.setPackagingInfo(packagingInfo);
		}
	}
}
