/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrafulfillment.atp.strategy.impl;

import de.hybris.platform.basecommerce.enums.InStockStatus;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.warehousing.atp.strategy.impl.WarehousingAvailabilityCalculationStrategy;
import de.hybris.platform.warehousing.model.AtpFormulaModel;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class CustomWarehousingAvailabilityCalculationStrategy extends WarehousingAvailabilityCalculationStrategy
{
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomWarehousingAvailabilityCalculationStrategy.class);
	private static final String STOCK_LEVELS = "stockLevels";

	@Override
	protected Map<String, Object> filterStocks(final Collection<StockLevelModel> stockLevels, final AtpFormulaModel atpFormula)
	{
		final Collection<StockLevelModel> stockLevelsFiltered = filterStockLevels(stockLevels);
		if (atpFormula.getExternal() == null || !atpFormula.getExternal().booleanValue())
		{
			stockLevelsFiltered.removeAll(filterStockLevelsExternal(stockLevelsFiltered));
		}
		if (atpFormula.getReturned() == null || !atpFormula.getReturned().booleanValue())
		{
			stockLevelsFiltered.removeAll(filterStockLevelsReturned(stockLevelsFiltered));
		}
		if (Boolean.TRUE.equals(atpFormula.getExpiry()))
		{
			stockLevelsFiltered.removeAll(filterStockLevelsExpired(stockLevelsFiltered));
		}
		final Map<String, Object> params = new HashMap<String, Object>();
		params.put(STOCK_LEVELS, stockLevelsFiltered);
		return params;
	}

	@Override
	public Long calculateAvailability(final Collection<StockLevelModel> stockLevels)
	{
		Long availability = 0L;
		if (!stockLevels.isEmpty())
		{
			if (stockLevels.stream().anyMatch(stockLevel -> InStockStatus.FORCEINSTOCK.equals(stockLevel.getInStockStatus())))
			{
				return null;
			}
			final AtpFormulaModel atpFormula = getDefaultAtpFormula(stockLevels);
			if (atpFormula != null)
			{
				final Map<String, Object> params = filterStocks(stockLevels, atpFormula);
				availability = getAtpFormulaService().getAtpValueFromFormula(atpFormula, params);
			}
			else
			{
				LOGGER.debug("No AtpFormula found, The availability is set to 0 by default");
			}
		}
		return availability;
	}

	private Collection<StockLevelModel> filterStockLevelsExpired(final Collection<StockLevelModel> stockLevelsFiltered)
	{
		final Date today = new Date();
		return stockLevelsFiltered.isEmpty() ? Collections.emptyList()
				: stockLevelsFiltered.stream().filter(s -> s.getExpiryDate() != null && today.after(s.getExpiryDate()))
						.collect(Collectors.toList());
	}
}
