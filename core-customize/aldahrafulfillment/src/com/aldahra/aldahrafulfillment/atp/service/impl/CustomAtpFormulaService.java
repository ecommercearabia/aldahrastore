/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrafulfillment.atp.service.impl;

import de.hybris.platform.warehousing.atp.formula.services.AtpFormulaService;
import de.hybris.platform.warehousing.atp.formula.services.impl.DefaultAtpFormulaService;
import de.hybris.platform.warehousing.model.AtpFormulaModel;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class CustomAtpFormulaService extends DefaultAtpFormulaService implements AtpFormulaService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomAtpFormulaService.class);
	protected static final String EXPIRY = "expiry";

	@Override
	public Long getAtpValueFromFormula(final AtpFormulaModel atpFormula, final Map<String, Object> params)
	{
		final List<Long> results = new ArrayList<>();
		results.add(0L);
		final Set<PropertyDescriptor> propertyDescriptors = new HashSet<>();
		if (atpFormula != null)
		{
			try
			{
				Arrays.stream(Introspector.getBeanInfo(atpFormula.getClass()).getPropertyDescriptors())
						.filter(descriptor -> descriptor.getPropertyType().equals(Boolean.class)).forEach(propertyDescriptor -> {
							final boolean bl = propertyDescriptors.add(propertyDescriptor);
						});
			}
			catch (final IntrospectionException introspectionException)
			{
				LOGGER.error("Sourcing failed to interpret the ATP formula.");
			}
			final List<String> atpAvailabilityVariables = Arrays.asList(RETURNED, EXTERNAL, EXPIRY);
			propertyDescriptors.stream()
					.filter(propertyDescriptor -> !atpAvailabilityVariables.contains(propertyDescriptor.getName().toLowerCase()))
					.forEach(formulaVarPropDescriptor -> {
						final List<Long> list2 = getAtpFormulaVariableValue(params, results, formulaVarPropDescriptor, atpFormula);
					});
		}
		return results.stream().mapToLong(Long::longValue).sum();
	}
}
