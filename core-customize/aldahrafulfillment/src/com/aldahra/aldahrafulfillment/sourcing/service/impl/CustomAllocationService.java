/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrafulfillment.sourcing.service.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.warehousing.allocation.AllocationException;
import de.hybris.platform.warehousing.allocation.AllocationService;
import de.hybris.platform.warehousing.allocation.impl.DefaultAllocationService;
import de.hybris.platform.warehousing.data.allocation.DeclineEntries;
import de.hybris.platform.warehousing.data.allocation.DeclineEntry;
import de.hybris.platform.warehousing.data.comment.WarehousingCommentContext;
import de.hybris.platform.warehousing.data.comment.WarehousingCommentEventType;
import de.hybris.platform.warehousing.data.sourcing.SourcingResult;
import de.hybris.platform.warehousing.data.sourcing.SourcingResults;
import de.hybris.platform.warehousing.model.DeclineConsignmentEntryEventModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.google.common.base.Strings;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class CustomAllocationService extends DefaultAllocationService implements AllocationService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomAllocationService.class);

	@Override
	public Collection<ConsignmentModel> createConsignments(final AbstractOrderModel order, final String code,
			final SourcingResults results)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("results", results);
		Assert.isTrue(!Strings.isNullOrEmpty(code), "Parameter code cannot be null or empty");
		final AtomicLong index = new AtomicLong();
		order.getConsignments().forEach(value -> {
			final long l = index.getAndIncrement();
		});
		final Collection consignments = results.getResults().stream()
				.map(result -> createConsignment(order, String.valueOf(code) + "_" + index.getAndIncrement(), result))
				.collect(Collectors.toList());
		getModelService().save(order);
		return consignments;
	}

	@Override
	public ConsignmentModel createConsignment(final AbstractOrderModel order, final String code, final SourcingResult result)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("result", result);
		ServicesUtil.validateParameterNotNullStandardMessage("order", order);
		Assert.isTrue(!Strings.isNullOrEmpty(code), "Parameter code cannot be null or empty");
		final ConsignmentModel consignment = getModelService().create(ConsignmentModel.class);
		consignment.setCode(code);
		consignment.setOrder(order);
		try
		{
			consignment.setFulfillmentSystemConfig(getWarehousingFulfillmentConfigDao().getConfiguration(result.getWarehouse()));
			final Set<Entry<AbstractOrderEntryModel, Long>> resultEntries = result.getAllocation().entrySet();
			final Optional<PointOfServiceModel> pickupPos = resultEntries.stream()
					.map(entry -> entry.getKey().getDeliveryPointOfService()).filter(Objects::nonNull).findFirst();
			if (pickupPos.isPresent())
			{
				consignment.setStatus(ConsignmentStatus.READY);
				consignment.setDeliveryMode(getDeliveryModeService().getDeliveryModeForCode(PICKUP_CODE));
				consignment.setShippingAddress(pickupPos.get().getAddress());
				consignment.setDeliveryPointOfService(pickupPos.get());
			}
			else
			{
				consignment.setStatus(ConsignmentStatus.READY);
				consignment.setDeliveryMode(order.getDeliveryMode());
				consignment.setShippingAddress(order.getDeliveryAddress());
				consignment.setShippingDate(getShippingDateStrategy().getExpectedShippingDate(consignment));
			}
			final Set entries = resultEntries.stream()
					.map(mapEntry -> createConsignmentEntry(mapEntry.getKey(), mapEntry.getValue(), consignment))
					.collect(Collectors.toSet());
			consignment.setConsignmentEntries(entries);
			consignment.setWarehouse(result.getWarehouse());
			if (consignment.getFulfillmentSystemConfig() == null)
			{
				getWarehousingConsignmentWorkflowService().startConsignmentWorkflow(consignment);
			}
			if (!consignment.getWarehouse().isExternal())
			{
				getInventoryEventService().createAllocationEvents(consignment);
			}
		}
		catch (final AmbiguousIdentifierException ambiguousIdentifierException)
		{
			consignment.setStatus(ConsignmentStatus.CANCELLED);
			LOGGER.error("Cancelling consignment with code " + consignment.getCode()
					+ " since only one fulfillment system configuration is allowed per consignment.");
		}
		getModelService().save(consignment);
		return consignment;
	}

	@Override
	public Collection<ConsignmentModel> manualReallocate(final DeclineEntries declinedEntries) throws AllocationException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("declinedEntries", declinedEntries);
		Assert.isTrue(CollectionUtils.isNotEmpty(declinedEntries.getEntries()), "Entries cannot be null or empty.");
		final boolean isEntryWithNoLocation = declinedEntries.getEntries().stream()
				.anyMatch(entry -> entry.getReallocationWarehouse() == null);
		if (isEntryWithNoLocation)
		{
			throw new AllocationException("Invalid or no warehouse selected for manual reallocation");
		}
		final boolean isWarehouseWithNoStockEntry = declinedEntries.getEntries().stream().anyMatch(entry -> {
			final Collection stockLevels = getStockLevelDao().findStockLevels(
					entry.getConsignmentEntry().getOrderEntry().getProduct().getCode(),
					Collections.singletonList(entry.getReallocationWarehouse()));
			return CollectionUtils.isEmpty(stockLevels);
		});
		if (isWarehouseWithNoStockEntry)
		{
			throw new AllocationException("No stock level entry found for manual reallocation");
		}
		final ArrayList<ConsignmentModel> newConsignments = new ArrayList<ConsignmentModel>();
		final ConsignmentModel consignment = (declinedEntries.getEntries().iterator().next()).getConsignmentEntry()
				.getConsignment();
		final AbstractOrderModel order = consignment.getOrder();
		declinedEntries.getEntries().stream().map(this::declineConsignmentEntry).filter(Optional::isPresent).map(Optional::get)
				.forEach(event -> consolidateConsignmentEntries(newConsignments, consignment, order, event));
		return newConsignments;
	}

	@Override
	protected void consolidateConsignmentEntries(final List<ConsignmentModel> newConsignments, final ConsignmentModel consignment,
			final AbstractOrderModel order, final DeclineConsignmentEntryEventModel event)
	{
		final Optional<ConsignmentModel> optional = newConsignments.stream()
				.filter(newConsignment -> newConsignment.getWarehouse().equals(event.getReallocatedWarehouse())).findAny();
		if (optional.isPresent())
		{
			final ConsignmentModel newConsolidatedConsignment = optional.get();
			final ConsignmentEntryModel consignmentEntryModel = createConsignmentEntry(event.getConsignmentEntry().getOrderEntry(),
					event.getQuantity(), newConsolidatedConsignment);
			if (!consignmentEntryModel.getConsignment().getWarehouse().isExternal())
			{
				getInventoryEventService().createAllocationEventsForConsignmentEntry(consignmentEntryModel);
			}
			final HashSet<ConsignmentEntryModel> consEntries = new HashSet<ConsignmentEntryModel>();
			newConsolidatedConsignment.getConsignmentEntries().forEach(consEntries::add);
			consEntries.add(consignmentEntryModel);
			newConsolidatedConsignment.setConsignmentEntries(consEntries);
		}
		else
		{
			final HashMap<AbstractOrderEntryModel, Long> allocation = new HashMap<AbstractOrderEntryModel, Long>();
			allocation.put(event.getConsignmentEntry().getOrderEntry(), event.getQuantity());
			if (allocation.isEmpty())
			{
				throw new AllocationException("Unable to process reallocation since there is nothing to reallocate.");
			}
			final SourcingResult sourcingResult = new SourcingResult();
			sourcingResult.setAllocation(allocation);
			sourcingResult.setWarehouse(event.getReallocatedWarehouse());
			final ConsignmentModel newConsignment2 = createConsignment(order,
					String.valueOf(consignment.getCode()) + "_" + getNumDeclines(consignment), sourcingResult);
			newConsignments.add(newConsignment2);
		}
	}

	@Override
	public void autoReallocate(final DeclineEntries declinedEntries)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("declinedEntries", declinedEntries);
		Assert.isTrue(CollectionUtils.isNotEmpty(declinedEntries.getEntries()), "Entries cannot be null or empty.");
		final ConsignmentModel consignment = declinedEntries.getEntries().iterator().next().getConsignmentEntry().getConsignment();
		LOGGER.debug("Declining consignment with code : {}", consignment.getCode());
		declinedEntries.getEntries().forEach(this::declineConsignmentEntry);
	}

	@Override
	protected int getNumDeclines(final ConsignmentModel consignment)
	{
		return consignment.getConsignmentEntries().stream()
				.filter(entry -> CollectionUtils.isNotEmpty(entry.getDeclineEntryEvents()))
				.mapToInt(entry -> entry.getDeclineEntryEvents().size()).sum();
	}

	@Override
	protected Optional<DeclineConsignmentEntryEventModel> declineConsignmentEntry(final DeclineEntry declineEntry)
	{
		final Long quantityToDecline = getQuantityToDecline(declineEntry);
		if (quantityToDecline > 0L)
		{
			final ConsignmentEntryModel consignmentEntry = declineEntry.getConsignmentEntry();
			final DeclineConsignmentEntryEventModel event = getModelService().create(DeclineConsignmentEntryEventModel.class);
			event.setConsignmentEntry(consignmentEntry);
			event.setReason(declineEntry.getReason());
			event.setQuantity(quantityToDecline);
			event.setReallocatedWarehouse(declineEntry.getReallocationWarehouse());
			getModelService().save(event);
			if (!declineEntry.getConsignmentEntry().getConsignment().getWarehouse().isExternal())
			{
				reallocateAllocationEvent(declineEntry, quantityToDecline);
			}
			if (!Objects.isNull(declineEntry.getNotes()))
			{
				final WarehousingCommentContext commentContext = new WarehousingCommentContext();
				commentContext.setCommentType(WarehousingCommentEventType.REALLOCATE_CONSIGNMENT_COMMENT);
				commentContext.setItem(declineEntry.getConsignmentEntry());
				commentContext.setSubject(REALLOCATE_COMMENT_SUBJECT);
				commentContext.setText(declineEntry.getNotes());
				final String code = "reallocation_" + getGuidKeyGenerator().generate().toString();
				getConsignmentEntryCommentService().createAndSaveComment(commentContext, code);
			}
			consignmentEntry.setQuantity(Long.valueOf(consignmentEntry.getQuantity() - quantityToDecline));
			getModelService().save(consignmentEntry);
			return Optional.of(event);
		}
		return Optional.empty();
	}

	@Override
	protected Long getQuantityToDecline(final DeclineEntry declineEntry)
	{
		getModelService().refresh(declineEntry.getConsignmentEntry());
		final ConsignmentEntryModel consignmentEntry = declineEntry.getConsignmentEntry();
		return declineEntry.getQuantity() <= consignmentEntry.getQuantity() ? declineEntry.getQuantity()
				: useQuantityPending(consignmentEntry, declineEntry);
	}

	@Override
	protected Long useQuantityPending(final ConsignmentEntryModel consignmentEntry, final DeclineEntry declineEntry)
	{
		return consignmentEntry.getQuantity();
	}

	@Override
	protected ConsignmentEntryModel createConsignmentEntry(final AbstractOrderEntryModel orderEntry, final Long quantity,
			final ConsignmentModel consignment)
	{
		LOGGER.debug("ConsignmentEntry :: Product [{}]: \tQuantity: '{}'", orderEntry.getProduct().getCode(), quantity);
		final ConsignmentEntryModel entry = getModelService().create(ConsignmentEntryModel.class);
		entry.setOrderEntry(orderEntry);
		entry.setQuantity(quantity);
		entry.setConsignment(consignment);
		final HashSet<ConsignmentEntryModel> consignmentEntries = new HashSet<ConsignmentEntryModel>();
		if (orderEntry.getConsignmentEntries() != null)
		{
			orderEntry.getConsignmentEntries().forEach(consignmentEntries::add);
		}
		LOGGER.info("Weight: " + orderEntry.getProduct().getWeight() + " ,TotalWeight: "
				+ orderEntry.getProduct().getWeight() * quantity);
		entry.setWeight(orderEntry.getProduct().getWeight() * quantity);
		consignmentEntries.add(entry);

		orderEntry.setConsignmentEntries(consignmentEntries);
		return entry;
	}

	@Override
	protected void reallocateAllocationEvent(final DeclineEntry declineEntry, final Long quantityToDecline)
	{
		getInventoryEventService().reallocateAllocationEvent(declineEntry, quantityToDecline);
	}
}
