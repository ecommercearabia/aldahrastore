/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrafulfillment.strategy.impl;

import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.warehousing.stock.strategies.impl.DefaultStockLevelSelectionStrategy;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class CustomStockLevelSelectionStrategy extends DefaultStockLevelSelectionStrategy
{
	@Override
	public Map<StockLevelModel, Long> getStockLevelsForAllocation(final Collection<StockLevelModel> stockLevels,
			final Long quantityToAllocate)
	{
		final LinkedHashMap<StockLevelModel, Long> stockMap = new LinkedHashMap<StockLevelModel, Long>();
		final List<StockLevelModel> filteredStockLevels = filterAsnCancelledStockLevels(stockLevels);
		long quantityLeftAllocate = quantityToAllocate;
		final Date now = new Date();
		final List<StockLevelModel> sortedStocks = filteredStockLevels.stream().filter(s -> checkExpiry(s, now))
				.sorted(Comparator.comparing(StockLevelModel::getExpiryDate, Comparator.nullsLast(Comparator.naturalOrder())))
				.collect(Collectors.toList());
		final Iterator<StockLevelModel> it = sortedStocks.iterator();
		while (quantityLeftAllocate > 0L && it.hasNext())
		{
			final StockLevelModel stockLevel = it.next();
			final Long stockAvailability = getCommerceStockLevelCalculationStrategy()
					.calculateAvailability(Collections.singletonList(stockLevel));
			quantityLeftAllocate = addToStockMap(stockMap, stockLevel, quantityLeftAllocate, stockAvailability);
		}
		finalizeStockMap(stockMap, quantityLeftAllocate);
		return stockMap;
	}

	private boolean checkExpiry(final StockLevelModel stockLevel, final Date now)
	{
		if (stockLevel.getExpiryDate() != null)
		{
			return now.before(stockLevel.getExpiryDate());
		}
		return true;
	}
}
