package com.aldahra.aldahrafulfillment.strategy.impl;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import javax.annotation.Resource;

import com.aldahra.aldahrafulfillment.model.FulfillmentProviderModel;
import com.aldahra.aldahrafulfillment.model.LyveFulfillmentProviderModel;
import com.aldahra.aldahrafulfillment.service.FulfillmentProviderService;
import com.aldahra.aldahrafulfillment.strategy.FulfillmentProviderStrategy;


/**
 *
 */
public class DefaultLyveFulfillmentProviderStrategy implements FulfillmentProviderStrategy
{
	@Resource(name = "fulfillmentProviderService")
	private FulfillmentProviderService fulfillmentProviderService;

	protected FulfillmentProviderService getFulfillmentProviderService()
	{
		return fulfillmentProviderService;
	}

	@Override
	public Optional<FulfillmentProviderModel> getActiveProvider(final String baseStoreUid)
	{
		return getFulfillmentProviderService().getActive(baseStoreUid, LyveFulfillmentProviderModel.class);
	}

	@Override
	public Optional<FulfillmentProviderModel> getActiveProvider(final BaseStoreModel baseStoreModel)
	{
		return getFulfillmentProviderService().getActive(baseStoreModel, LyveFulfillmentProviderModel.class);
	}

	@Override
	public Optional<FulfillmentProviderModel> getActiveProviderByCurrentBaseStore()
	{
		return getFulfillmentProviderService().getActiveProviderByCurrentBaseStore(LyveFulfillmentProviderModel.class);
	}
}

