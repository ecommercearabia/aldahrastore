/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrafulfillment.strategy.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.Optional;

import javax.annotation.Resource;

import com.aldahra.aldahrafulfillment.exception.FulfillentException;
import com.aldahra.aldahrafulfillment.model.FulfillmentProviderModel;
import com.aldahra.aldahrafulfillment.service.FulfillentService;
import com.aldahra.aldahrafulfillment.strategy.FulfillmentStrategy;


/**
 *
 */
public class DefaultLyveFulfillmentStrategy implements FulfillmentStrategy
{
	@Resource(name = "lyveFulfillmentService")
	private FulfillentService lyveFulfillmentService;

	/**
	 * @return the shipaFulfillmentService
	 */
	public FulfillentService getLyveFulfillmentService()
	{
		return lyveFulfillmentService;
	}

	@Override
	public Optional<String> createShipment(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillentException
	{
		return getLyveFulfillmentService().createShipment(consignmentModel, fulfillmentProviderModel);
	}

	@Override
	public Optional<byte[]> printAWB(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillentException
	{
		return getLyveFulfillmentService().printAWB(consignmentModel, fulfillmentProviderModel);
	}

	@Override
	public Optional<String> getStatus(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillentException
	{
		return getLyveFulfillmentService().getStatus(consignmentModel, fulfillmentProviderModel);
	}

	@Override
	public Optional<String> updateShipment(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillentException
	{
		return getLyveFulfillmentService().updateShipment(consignmentModel, fulfillmentProviderModel);
	}

	@Override
	public Optional<ConsignmentStatus> updateStatus(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillentException
	{
		return getLyveFulfillmentService().updateStatus(consignmentModel, fulfillmentProviderModel);

	}

}
