/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrafulfillment.strategy.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.Optional;

import javax.annotation.Resource;

import com.aldahra.aldahrafulfillment.exception.FulfillentException;
import com.aldahra.aldahrafulfillment.model.FulfillmentProviderModel;
import com.aldahra.aldahrafulfillment.service.FulfillentService;
import com.aldahra.aldahrafulfillment.strategy.FulfillmentStrategy;

/**
 *
 */
public class DefaultShipaFulfillmentStrategy implements FulfillmentStrategy
{
	@Resource(name = "shipaFulfillmentService")
	private FulfillentService shipaFulfillmentService;

	/**
	 * @return the shipaFulfillmentService
	 */
	public FulfillentService getShipaFulfillmentService()
	{
		return shipaFulfillmentService;
	}

	@Override
	public Optional<String> createShipment(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel)
			throws FulfillentException
	{
		return getShipaFulfillmentService().createShipment(consignmentModel, fulfillmentProviderModel);
	}

	@Override
	public Optional<byte[]> printAWB(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel)
			throws FulfillentException
	{
		return getShipaFulfillmentService().printAWB(consignmentModel, fulfillmentProviderModel);
	}

	@Override
	public Optional<String> getStatus(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillentException
	{
		return getShipaFulfillmentService().getStatus(consignmentModel, fulfillmentProviderModel);
	}

	@Override
	public Optional<String> updateShipment(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillentException
	{
		return getShipaFulfillmentService().updateShipment(consignmentModel, fulfillmentProviderModel);
	}

	@Override
	public Optional<ConsignmentStatus> updateStatus(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillentException
	{
		return getShipaFulfillmentService().updateStatus(consignmentModel, fulfillmentProviderModel);
	}

}
