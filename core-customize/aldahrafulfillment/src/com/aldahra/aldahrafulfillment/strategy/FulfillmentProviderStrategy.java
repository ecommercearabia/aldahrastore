/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrafulfillment.strategy;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.aldahra.aldahrafulfillment.model.FulfillmentProviderModel;


/**
 *
 */
public interface FulfillmentProviderStrategy
{
	/**
	 *
	 */
	public Optional<FulfillmentProviderModel> getActiveProvider(String baseStoreUid);

	/**
	 *
	 */
	public Optional<FulfillmentProviderModel> getActiveProvider(BaseStoreModel baseStoreModel);

	/**
	 *
	 */
	public Optional<FulfillmentProviderModel> getActiveProviderByCurrentBaseStore();

}
