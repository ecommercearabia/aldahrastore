/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrafulfillment.strategy;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.Optional;

import com.aldahra.aldahrafulfillment.exception.FulfillentException;
import com.aldahra.aldahrafulfillment.model.FulfillmentProviderModel;


/**
 *
 */
public interface FulfillmentStrategy
{
	public Optional<String> createShipment(ConsignmentModel consignmentModel, FulfillmentProviderModel fulfillmentProviderModel)
			throws FulfillentException;

	public Optional<String> updateShipment(ConsignmentModel consignmentModel, FulfillmentProviderModel fulfillmentProviderModel)
			throws FulfillentException;

	public Optional<byte[]> printAWB(ConsignmentModel consignmentModel, FulfillmentProviderModel fulfillmentProviderModel)
			throws FulfillentException;

	public Optional<String> getStatus(ConsignmentModel consignmentModel, FulfillmentProviderModel fulfillmentProviderModel)
			throws FulfillentException;

	public Optional<ConsignmentStatus> updateStatus(ConsignmentModel consignmentModel,
			FulfillmentProviderModel fulfillmentProviderModel) throws FulfillentException;
}
