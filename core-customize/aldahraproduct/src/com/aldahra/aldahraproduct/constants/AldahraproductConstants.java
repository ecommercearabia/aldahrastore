/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraproduct.constants;

/**
 * Global class for all Aldahraproduct constants. You can add global constants for your extension into this class.
 */
public final class AldahraproductConstants extends GeneratedAldahraproductConstants
{
	public static final String EXTENSIONNAME = "aldahraproduct";

	private AldahraproductConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "aldahraproductPlatformLogo";
}
