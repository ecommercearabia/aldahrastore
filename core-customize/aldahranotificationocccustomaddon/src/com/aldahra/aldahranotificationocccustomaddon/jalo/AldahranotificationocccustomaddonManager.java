/*
 *  
 * [y] hybris Platform
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.aldahra.aldahranotificationocccustomaddon.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.aldahra.aldahranotificationocccustomaddon.constants.AldahranotificationocccustomaddonConstants;
import org.apache.log4j.Logger;

public class AldahranotificationocccustomaddonManager extends GeneratedAldahranotificationocccustomaddonManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( AldahranotificationocccustomaddonManager.class.getName() );
	
	public static final AldahranotificationocccustomaddonManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (AldahranotificationocccustomaddonManager) em.getExtension(AldahranotificationocccustomaddonConstants.EXTENSIONNAME);
	}
	
}
