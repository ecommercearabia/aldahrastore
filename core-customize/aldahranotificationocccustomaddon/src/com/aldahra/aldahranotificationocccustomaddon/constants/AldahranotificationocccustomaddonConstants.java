/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahranotificationocccustomaddon.constants;

/**
 * Global class for all aldahranotificationocccustomaddon constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings(
{ "deprecation", "squid:CallToDeprecatedMethod" })
public final class AldahranotificationocccustomaddonConstants extends GeneratedAldahranotificationocccustomaddonConstants
{
	public static final String EXTENSIONNAME = "aldahranotificationocccustomaddon"; //NOSONAR

	private AldahranotificationocccustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
