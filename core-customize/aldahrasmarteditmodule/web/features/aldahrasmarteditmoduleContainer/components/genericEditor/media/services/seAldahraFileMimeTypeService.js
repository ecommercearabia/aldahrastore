angular.module('seAldahraFileMimeTypeServiceModule', ['seFileMimeTypeServiceModule'])
    .constant('seFileMimeTypeServiceConstants', {
        VALID_IMAGE_MIME_TYPE_CODES: ['FFD8FFDB', 'FFD8FFE0', 'FFD8FFE1', '474946383761', '424D', '49492A00', '4D4D002A', '89504E470D0A1A0A', '52494646', '57454250']
    });
