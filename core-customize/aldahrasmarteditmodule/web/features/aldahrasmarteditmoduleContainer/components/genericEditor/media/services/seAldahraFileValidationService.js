angular.module('seAldahraFileValidationServiceModule', ['seFileValidationServiceModule', 'seObjectValidatorFactoryModule', 'seAldahraFileMimeTypeServiceModule'])
    .constant('seFileValidationServiceConstants', {
        ACCEPTED_FILE_TYPES: ['jpeg', 'jpg', 'gif', 'bmp', 'tiff', 'tif', 'png', 'webp'],
        MAX_FILE_SIZE_IN_BYTES: 20 * 1024 * 1024,
        I18N_KEYS: {
            FILE_TYPE_INVALID: 'se.upload.file.type.invalid',
            FILE_SIZE_INVALID: 'se.upload.file.size.invalid'
        }
    });
