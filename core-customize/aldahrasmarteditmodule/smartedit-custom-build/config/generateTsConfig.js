/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
/* jshint esversion: 6 */
module.exports = function() {

    return {
        config: function(data, conf) {
            const lodash = require('lodash');

            const aldahrasmarteditmodulePaths = {
                "aldahrasmarteditmodule/*": ["web/features/aldahrasmarteditmodule/*"],
                "aldahrasmarteditmodulecommons": ["web/features/aldahrasmarteditmodulecommons"],
                "aldahrasmarteditmodulecommons*": ["web/features/aldahrasmarteditmodulecommons*"]
            };

            const yssmarteditmoduleContainerPaths = {
                "aldahrasmarteditmodulecontainer/*": ["web/features/aldahrasmarteditmoduleContainer/*"],
                "aldahrasmarteditmodulecommons": ["web/features/aldahrasmarteditmodulecommons"],
                "aldahrasmarteditmodulecommons*": ["web/features/aldahrasmarteditmodulecommons*"]
            };

            const commonsLayerInclude = ["../../jsTarget/web/features/aldahrasmarteditmodulecommons/**/*"];
            const innerLayerInclude = commonsLayerInclude.concat(["../../jsTarget/web/features/aldahrasmarteditmodule/**/*"]);
            const outerLayerInclude = commonsLayerInclude.concat(["../../jsTarget/web/features/aldahrasmarteditmoduleContainer/**/*"]);
            const commonsLayerTestInclude = ["../../jsTests/tests/aldahrasmarteditmodulecommons/unit/**/*"];
            const innerLayerTestInclude = commonsLayerTestInclude.concat(["../../jsTests/tests/aldahrasmarteditmodule/unit/features/**/*"]);
            const outerLayerTestInclude = commonsLayerTestInclude.concat(["../../jsTests/tests/aldahrasmarteditmoduleContainer/unit/features/**/*"]);

            function addYsmarteditmodulePaths(conf) {
                lodash.merge(conf.compilerOptions.paths, lodash.cloneDeep(aldahrasmarteditmodulePaths));
            }

            function addYsmarteditmoduleContainerPaths(conf) {
                lodash.merge(conf.compilerOptions.paths, lodash.cloneDeep(yssmarteditmoduleContainerPaths));
            }

            // PROD
            addYsmarteditmodulePaths(conf.generateProdSmarteditTsConfig.data);
            addYsmarteditmoduleContainerPaths(conf.generateProdSmarteditContainerTsConfig.data);
            conf.generateProdSmarteditTsConfig.data.include = innerLayerInclude;
            conf.generateProdSmarteditContainerTsConfig.data.include = outerLayerInclude;

            // DEV
            addYsmarteditmodulePaths(conf.generateDevSmarteditTsConfig.data);
            addYsmarteditmoduleContainerPaths(conf.generateDevSmarteditContainerTsConfig.data);
            conf.generateDevSmarteditTsConfig.data.include = innerLayerInclude;
            conf.generateDevSmarteditContainerTsConfig.data.include = outerLayerInclude;

            // KARMA
            addYsmarteditmodulePaths(conf.generateKarmaSmarteditTsConfig.data);
            addYsmarteditmoduleContainerPaths(conf.generateKarmaSmarteditContainerTsConfig.data);
            conf.generateKarmaSmarteditTsConfig.data.include = innerLayerInclude.concat(innerLayerTestInclude);
            conf.generateKarmaSmarteditContainerTsConfig.data.include = outerLayerInclude.concat(outerLayerTestInclude);

            // IDE
            addYsmarteditmodulePaths(conf.generateIDETsConfig.data);
            addYsmarteditmoduleContainerPaths(conf.generateIDETsConfig.data);
            conf.generateIDETsConfig.data.include = conf.generateIDETsConfig.data.include.concat(["../../jsTests/tests/**/unit/**/*"]);

            return conf;
        }
    };

};
