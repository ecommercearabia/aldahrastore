/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
/* jshint unused:false, undef:false */
module.exports = function() {

    /***
     *  Naming:
     *  File or Files masks should end in File or Files,
     *  ex: someRoot.path.myBlaFiles = /root/../*.*
     *
     *  General rules:
     *  No copy paste
     *  No duplicates
     *  Avoid specific files when possible, try to specify folders
     *  What happens to aldahrasmarteditmodule, happens to aldahrasmarteditmoduleContainer
     *  Try to avoid special cases and exceptions
     */
    var lodash = require('lodash');

    var paths = {};

    paths.tests = {};

    paths.tests.root = 'jsTests';
    paths.tests.testsRoot = paths.tests.root + '/tests';
    paths.tests.aldahrasmarteditmoduleContainerTestsRoot = paths.tests.testsRoot + '/aldahrasmarteditmoduleContainer';
    paths.tests.aldahrasmarteditmoduleContainere2eTestsRoot = paths.tests.aldahrasmarteditmoduleContainerTestsRoot + '/e2e';

    paths.tests.aldahrasmarteditmodulee2eTestFiles = paths.tests.root + '/e2e/**/*Test.js';
    paths.tests.aldahrasmarteditmoduleContainere2eTestFiles = paths.tests.aldahrasmarteditmoduleContainere2eTestsRoot + '/**/*Test.js';

    paths.e2eFiles = [
        paths.tests.aldahrasmarteditmodulee2eTestFiles,
        paths.tests.aldahrasmarteditmoduleContainere2eTestFiles
    ];

    /**
     * Code coverage
     */
    paths.coverage = {
        dir: './jsTarget/test/coverage',
        smarteditDirName: 'smartedit',
        smarteditcontainerDirName: 'smarteditcontainer',
        smarteditcommonsDirName: 'smarteditcommons'
    };

    return paths;

}();
