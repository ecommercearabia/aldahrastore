/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
const {
    resolve
} = require('path');

const {
    group,
    webpack: {
        entry,
        alias
    }
} = require('../../smartedit-build/builders');

const commonsAlias = alias('aldahrasmarteditmodulecommons', resolve('./jsTarget/web/features/aldahrasmarteditmodulecommons'));

const smartedit = group(
    commonsAlias,
    alias('aldahrasmarteditmodule', resolve('./jsTarget/web/features/aldahrasmarteditmodule'))
);
const smarteditContainer = group(
    commonsAlias,
    alias('aldahrasmarteditmodulecontainer', resolve('./jsTarget/web/features/aldahrasmarteditmoduleContainer')),
);

module.exports = {
    ySmarteditKarma: () => group(
        smartedit
    ),
    ySmarteditContainerKarma: () => group(
        smarteditContainer
    ),
    ySmartedit: () => group(
        smartedit,
        entry({
            aldahrasmarteditmodule: resolve('./jsTarget/web/features/aldahrasmarteditmodule/aldahrasmarteditmoduleModule.ts')
        })
    ),
    ySmarteditContainer: () => group(
        smarteditContainer,
        entry({
            aldahrasmarteditmoduleContainer: resolve('./jsTarget/web/features/aldahrasmarteditmoduleContainer/aldahrasmarteditmodulecontainerModule.ts')
        })
    )
};
