/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrasmarteditmodule.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.aldahra.aldahrasmarteditmodule.constants.AldahrasmarteditmoduleConstants;
import org.apache.log4j.Logger;

@SuppressWarnings("PMD")
public class AldahrasmarteditmoduleManager extends GeneratedAldahrasmarteditmoduleManager
{
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger( AldahrasmarteditmoduleManager.class.getName() );
	
	public static final AldahrasmarteditmoduleManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (AldahrasmarteditmoduleManager) em.getExtension(AldahrasmarteditmoduleConstants.EXTENSIONNAME);
	}
	
}
