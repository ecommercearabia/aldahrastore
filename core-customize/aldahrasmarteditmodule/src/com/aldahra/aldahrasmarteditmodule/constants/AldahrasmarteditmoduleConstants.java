/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrasmarteditmodule.constants;

@SuppressWarnings("PMD")
public class AldahrasmarteditmoduleConstants extends GeneratedAldahrasmarteditmoduleConstants
{
	public static final String EXTENSIONNAME = "aldahrasmarteditmodule";

	private AldahrasmarteditmoduleConstants()
	{
		// Intentionally left empty.
	}
}
