/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
import {doImport} from './forcedImports';
doImport();
import {SeModule} from 'smarteditcommons';
/**
 * @ngdoc overview
 * @name aldahrasmarteditmoduleContainer
 * @description
 * Placeholder for documentation
 */
@SeModule({
	imports: [
		'smarteditServicesModule',
		'seAldahraFileValidationServiceModule',
		'seAldahraFileMimeTypeServiceModule'


	]
})
export class /* @ngInject */ AldahrasmarteditmoduleContainer {}
