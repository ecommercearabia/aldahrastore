angular.module('aldahrasmarteditmoduleContainerTemplates', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('web/features/aldahrasmarteditmoduleContainer/abAnalyticsToolbarItem/abAnalyticsToolbarItemTemplate.html',
    "<h2>AB Analytics</h2>\n" +
    "<p>This is a dummy toolbar item used to demonstrate functionality.</p>\n"
  );


  $templateCache.put('web/features/aldahrasmarteditmoduleContainer/abAnalyticsToolbarItem/abAnalyticsToolbarItemWrapperTemplate.html',
    "<ab-analytics-toolbar-item></ab-analytics-toolbar-item>"
  );

}]);
