/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
angular.module('abAnalyticsToolbarItemModule', ['aldahrasmarteditmoduleContainerTemplates'])
    .component('abAnalyticsToolbarItem', {
        templateUrl: 'abAnalyticsToolbarItemTemplate.html'
    });
