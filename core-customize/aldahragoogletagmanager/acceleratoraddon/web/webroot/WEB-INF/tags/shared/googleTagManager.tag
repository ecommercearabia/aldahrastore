<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="jspPage" value="${fn:split(request.requestURI, '/')[fn:length(fn:split(request.requestURI, '/'))-1]}" />
<c:set var="singlequote" value="'"/>
<c:if test="${not empty gtmContainerId}" >
    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=${gtmContainerId}"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager -->
</c:if>

<script>

dataLayer.push({'var': 'value'});

<c:choose>
<c:when test="${not empty user and user.uid ne 'anonymous'}">

window.dataLayer = window.dataLayer || [];
dataLayer.push({
    	'customerID':'${user.customerId}',
        'userID':'${user.uid}',
        'email':'${user.uid}',
        'firstName':'${user.firstName}',
        'lastName':'${user.lastName}', 
        'genderId':'${genderId}',
        'phoneNumber':'${user.mobileNumber}',
       	'imageProductURL' : '${imageProductURL}',
        'productURL':'${productURL}',
        'categoryIds':[
                <c:forEach items="${categoryIds}" var="category">
                '${category.code}',
                </c:forEach>
            ] ,
		'productAllSupercategories':[
		    <c:forEach items="${productAllSupercategories}" var="category">
		    '${category.code}',
		    </c:forEach>
		] 
        
});
</c:when>
<c:when test="${pageType == 'PRODUCT'}">
<c:set var = "email" value = "${fn:substringAfter(user.uid, '|')}" />
	   window.dataLayer = window.dataLayer || [];
	   dataLayer.push({
	           'userID':'${email}',
	           'email':'${email}',   
	           'imageProductURL' : '${imageProductURL}',
	           'productURL':'${productURL}',
	           'categoryIds':[
                   <c:forEach items="${categoryIds}" var="category">
                   '${category.code}',
                   </c:forEach>
               ]   ,
		       'productAllSupercategories':[
		           <c:forEach items="${productAllSupercategories}" var="category">
		           '${category.code}',
		           </c:forEach>
		       ] 
	   });

</c:when>
<c:otherwise>
   <c:set var = "email" value = "${fn:substringAfter(user.uid, '|')}" />
   window.dataLayer = window.dataLayer || [];
   dataLayer.push({
           'userID':'${email}',
           'email':'${email}',
           'imageProductURL' : '${imageProductURL}',
           'productURL':'${productURL}',
           'categoryIds':[
               <c:forEach items="${categoryIds}" var="category">
               '${category.code}',
               </c:forEach>
           ]  ,
           'productAllSupercategories':[
               <c:forEach items="${productAllSupercategories}" var="category">
               '${category.code}',
               </c:forEach>
           ] 
   });
</c:otherwise>
</c:choose>



<c:choose>
    <c:when test="${pageType == 'PRODUCT' && gtmecommerce!='legacy'}">
        window.dataLayer = window.dataLayer || [];
        <c:set var="categories" value="" />
        
        <c:forEach items="${product.categories}" var="category">
            //category ${category.name}
            <c:set var="categoryName" value="${fn:replace(fn:replace(category.name, singlequote, ''),'&#039',' ')}" />
            <c:set var="categories">${categories}; ${categoryName}</c:set>
        </c:forEach>
        //This is shown on product pages
        <c:set var="productName" value="${fn:replace(fn:replace(product.name, singlequote, ''),'&#039',' ')}" />
        dataLayer.push({
            'event':'product-detail',
            'ecommerce':{
                'detail':{
                    'actionField':{},
                    'products':[{
                        'name':'${productName}',
                        'price': Number(${product.price.value}),
                        'category':'${fn:substringAfter(categories, '; ')}',
                        'id':'${product.code}',
                        'variant':'${product.variantType}',
                        'brand':'${product.manufacturer}'
                    }]
                }
            }
        });
    </c:when>
    <c:when test="${pageType == 'CATEGORY' && gtmecommerce!='legacy'}">
        window.dataLayer = window.dataLayer || [];
        //Enhanced ecommerce
        <c:set var="lastBreadcrumb" value="${breadcrumbs[fn:length(breadcrumbs)-1]}" />
        dataLayer.push({
            'event':'product-impressions',
            'list':'${lastBreadcrumb.name}',
            'ecommerce':{
                'impressions':[
                    <c:forEach items="${searchPageData.results}" var="product" varStatus="status">
                    {
                        <c:set var="productName" value="${fn:replace(fn:replace(product.name, singlequote, ''),'&#039',' ')}" />
                        'name':'${productName}',
                        'price':Number(${product.price.value}),
                        'list':'${lastBreadcrumb.name}',
                        'position':${status.count},
                        'id':'${product.code}',
                        'brand':'${product.manufacturer}',
                    },
                    </c:forEach>
                ]
            }
        });
    </c:when>
    
    <c:when test="${pageType == 'PRODUCTSEARCH'}">
        //number of search results: ${searchPageData.pagination.totalNumberOfResults} 
        window.dataLayer = window.dataLayer || [];
        dataLayer.push({
                'event':'search-results',
                'results':'${searchPageData.pagination.totalNumberOfResults}',
                'search-term':'${searchPageData.freeTextSearch}'
        });
        <c:if test="${gtmecommerce!='legacy'}">
            dataLayer.push({
                'event':'product-impressions',
                'list':'Search Results',
                'ecommerce':{
                    'impressions':[
                        <c:forEach items="${searchPageData.results}" var="product" varStatus="status">
                        {
                            <c:set var="productName" value="${fn:replace(fn:replace(product.name, singlequote, ''),'&#039',' ')}" />
                            'name':'${productName}',
                            'price': Number(${product.price.value}),
                            'list':'Search Results',
                            'position':${status.count},
                            'id':'${product.code}',
                            'brand':'${product.manufacturer}',
                        },
                        </c:forEach>
                    ]
                }
            });
        </c:if>
    </c:when>
    <c:when test="${pageType == 'CART' && gtmecommerce!='legacy'}">
        window.dataLayer = window.dataLayer || [];
        dataLayer.push({
            'event':'checkout-step',
            'ecommerce':{
                'checkout':{
                    'actionField':{'step':1,'option':'Cart Page'},
                    'products':[
                        <c:forEach items="${cartData.entries}" var="entry">
                        {
                            <c:set var="entryProductName" value="${fn:replace(fn:replace(entry.product.name, singlequote, ''),'&#039',' ')}" />
                            'name':'${entryProductName}',
                            'price':Number(${entry.basePrice.value}),
                            'id':'${entry.product.code}',
                            'variant':'${entry.product.variantType}',
                            'brand':'${entry.product.manufacturer}',
                            'quantity':${entry.quantity}
                        },
                        </c:forEach>
                    ]
                }
            }
        });
    </c:when>
    <c:when test="${pageType == 'ORDERCONFIRMATION'}">
        <c:choose>
            <c:when test="${gtmecommerce == legacy}">
                //Legacy e-commerce transaction push
                dataLayer.push({
                    'event':'transaction',
                    'transactionId': '${orderData.code}',
                    'transactionAffiliation': '${siteName}',
                    'transactionTotal': Number(${orderData.totalPrice.value}),
                    'transactionTax': Number(${orderData.totalTax.value}),
                    'transactionShipping': Number(${orderData.deliveryCost.value}),
                    'transactionProducts': [
                        <c:forEach items="${orderData.entries}" var="entry">
                        {
                            <c:set var="entryProductName" value="${fn:replace(fn:replace(entry.product.name, singlequote, ''),'&#039',' ')}" />
                            'sku': '${entry.product.code}',
                            'name': '${entryProductName}',
                            <c:choose>
                                <c:when test="${not empty entry.product.categories}">
                                <c:set var="entryProductCategoryName" value="${fn:replace(fn:replace(entry.product.categories[fn:length(entry.product.categories) - 1].name, singlequote, ''),'&#039',' ')}" />
                                    'category': '${entryProductCategoryName}',
                                </c:when>
                                <c:otherwise>
                                    'category':'',
                                </c:otherwise>
                            </c:choose>
                            'price': Number(${entry.product.price.value}),
                            'quantity': ${entry.quantity}
                        },
                        </c:forEach>
                    ]
                });
            </c:when>
            <c:otherwise>
            <c:set var="orderCategoryName" value="${fn:replace(fn:replace(orderData.deliveryAddress.country.name, singlequote, ''),'&#039',' ')}" />
                dataLayer.push({
                    'event':'transaction',
                    'transactionPostalCode':'${orderData.deliveryAddress.postalCode}',
                    'transactionCountryName':'${orderCategoryName}',
                    'ecommerce':{
                        'purchase':{
                            'actionField':{
                                'id':'${orderData.code}',
                                'affiliation': '${siteName}',
                                'revenue':Number(${orderData.totalPrice.value}),
                                'tax':Number(${orderData.totalTax.value}),
                                'shipping':Number(${orderData.deliveryCost.value}),
                            },
                            'products': [
                                <c:forEach items="${orderData.entries}" var="entry">
                                {
                                    'id': '${entry.product.code}',
                                    <c:set var="entryProductName" value="${fn:replace(fn:replace(entry.product.name, singlequote, ''),'&#039',' ')}" />
                                    'name': '${entryProductName}',
                                     <c:choose>
                                        <c:when test="${not empty entry.product.categories}">
                                        <c:set var="entryProductCategoryName" value="${fn:replace(fn:replace(entry.product.categories[fn:length(entry.product.categories) - 1].name, singlequote, ''),'&#039',' ')}" />
                                            'category': '${entryProductCategoryName}',
                                        </c:when>
                                        <c:otherwise>
                                            'category':'',
                                        </c:otherwise>
                                    </c:choose>
                                    'price': Number(${entry.product.price.value}),
                                    'brand': '${entry.product.manufacturer}',
                                    'variant':'${entry.product.variantType}',
                                    'quantity':${entry.quantity}
                                },
                                </c:forEach>
                            ]
                        }
                    }
                });
            </c:otherwise>
        </c:choose>
        
    </c:when>
    <c:otherwise>
        //DataLayer: No specific Page type page
    </c:otherwise>
</c:choose>
<c:forEach items="${gtmcheckoutStepsList}" var="checkoutStep"  varStatus="status">
    <c:if test="${jspPage == checkoutStep && gtmecommerce!='legacy'}">
        window.dataLayer = window.dataLayer || [];
        dataLayer.push({
            'event':'checkout-option',
            'ecommerce':{
                'checkout':{
                    'actionField':{'step':${status.count}+1,'option':'Single Checkout Page'},
                    'products':[
                        <c:forEach items="${cartData.entries}" var="entry">
                        {
                            <c:set var="entryProductName" value="${fn:replace(fn:replace(entry.product.name, singlequote, ''),'&#039',' ')}" />
                            'name':'${entryProductName}',
                            'price': Number(${entry.basePrice.value}),
                            'id':'${entry.product.code}',
                            'variant':'${entry.product.variantType}',
                            'brand':'${entry.product.manufacturer}',
                            'quantity':${entry.quantity}
                        },
                        </c:forEach>
                    ]
                }
            }
        });
    </c:if>
</c:forEach>
<c:if test="${gtmecommerce!='legacy'}">
    function trackAddToCart_GTM(productCode, quantityAdded, name, price) {
        <c:set var="lastBreadcrumb" value="${breadcrumbs[fn:length(breadcrumbs)-1]}" />
        window.dataLayer = window.dataLayer || [];
        dataLayer.push({
          'event': 'add-to-cart',
          'productId': productCode,
          'productName': name,
          'quantity':quantityAdded,
          'ecommerce': {
            'currencyCode': '${currentCurrency.isocode}',
            'add': { 
              <c:if test="${pageType == 'CATEGORY'}">
                'actionField': {'list': '${lastBreadcrumb.name}'},
              </c:if>
              <c:if test="${pageType == 'PRODUCTSEARCH'}">
                'actionField': {'list': 'Search Results'},
              </c:if>
              'products': [{                       
                'name':  name,
                'id': productCode,
                'price': Number(price),
                'quantity': quantityAdded
               }]
            }
          }
        });
    }
    function trackRemoveFromCart_GTM(productCode, initialQuantity, name, price) {
        window.dataLayer = window.dataLayer || [];
        dataLayer.push({
          'event': 'remove-from-cart',
          'productId': productCode,
          'productName': name,
          'price':price,
          'quantity':initialQuantity,
          'ecommerce': {
            'currencyCode': '${currentCurrency.isocode}',
            'remove': {
               
              'products': [{                        
                'name':  name,
                'id': productCode,
                'price': Number(price),
                'quantity': initialQuantity
               }]
            }
          }
        });
    }
    function trackUpdateCart_GTM(productCode, initialQuantity, newQuantity, name, price) {
            
            if (initialQuantity < newQuantity) {
                trackAddToCart_GTM(productCode, newQuantity-initialQuantity, name, price);
            } 
            else if (initialQuantity > newQuantity){
                trackRemoveFromCart_GTM(productCode, initialQuantity-newQuantity, name, price);
            }
    }
    window.mediator.subscribe('trackAddToCart', function(data) {
        if (data.productCode && data.quantity)
        {
            trackAddToCart_GTM(data.productCode, data.quantity, data.cartData.productName, data.cartData.productPrice);
        }
    });
    window.mediator.subscribe('trackUpdateCart', function(data) {
        if (data.productCode && data.initialCartQuantity && data.newCartQuantity)
        {
            trackUpdateCart_GTM(data.productCode, data.initialCartQuantity, data.newCartQuantity, data.cartData.productName, data.cartData.productPrice);
        }
    });
    window.mediator.subscribe('trackRemoveFromCart', function(data) {
        if (data.productCode && data.initialCartQuantity)
        {
            trackRemoveFromCart_GTM(data.productCode, data.initialCartQuantity,  data.productName, data.productPrice);
        }
    });
</c:if>
</script>