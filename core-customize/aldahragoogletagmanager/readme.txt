This extension is for google tag manager for b2b and b2c.

To install this extension you have to put it inside the custom/common folder and then add it to the localextensions.xml file in the config extension,
first you have to install the addon for both the b2b and b2c:
ant addoninstall -Daddonnames="aldahragoogletagmanager" -DaddonStorefront.yacceleratorstorefront="yerabiab2cstorefront"
ant addoninstall -Daddonnames="aldahragoogletagmanager" -DaddonStorefront.yacceleratorstorefront="yerabiab2bstorefront"
and then you have to do an ant clean all.
