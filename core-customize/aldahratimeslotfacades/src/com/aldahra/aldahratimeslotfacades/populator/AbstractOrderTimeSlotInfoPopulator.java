/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahratimeslotfacades.populator;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import com.aldahra.aldahratimeslot.model.TimeSlotInfoModel;
import com.aldahra.aldahratimeslotfacades.TimeSlotInfoData;


/**
 * @author amjad.shati@erabia.com
 *
 * @param <S>
 *           SOURCE, the generic type extends AbstractOrderModel
 * @param <T>
 *           TARGET, the generic type extends AbstractOrderData
 */
public class AbstractOrderTimeSlotInfoPopulator <S extends AbstractOrderModel, T extends AbstractOrderData> implements Populator<S, T>
{
	@Resource(name = "timeSlotInfoConverter")
	private Converter<TimeSlotInfoModel, TimeSlotInfoData> timeSlotInfoConverter;

	@Override
	public void populate(final AbstractOrderModel source, final AbstractOrderData target)
	{
		if (source != null && source.getTimeSlotInfo() != null)
		{
			final TimeSlotInfoData infoData = timeSlotInfoConverter.convert(source.getTimeSlotInfo());
			target.setTimeSlotInfoData(infoData);
		}
	}

}
