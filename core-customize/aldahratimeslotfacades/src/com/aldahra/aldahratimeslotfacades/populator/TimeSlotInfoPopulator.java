/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahratimeslotfacades.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.store.services.BaseStoreService;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import javax.annotation.Resource;

import com.aldahra.aldahratimeslot.model.TimeSlotInfoModel;
import com.aldahra.aldahratimeslotfacades.TimeSlotInfoData;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class TimeSlotInfoPopulator implements Populator<TimeSlotInfoModel, TimeSlotInfoData>
{
	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;
	@Override
	public void populate(final TimeSlotInfoModel source, final TimeSlotInfoData target)
	{
		if (source != null)
		{
			target.setStart(LocalTime.parse(source.getStart(), DateTimeFormatter.ofPattern("H:mm")));
			target.setEnd(LocalTime.parse(source.getEnd(), DateTimeFormatter.ofPattern("H:mm")));
			target.setDate(source.getDate());
			target.setDay(source.getDay());
			target.setPeriodCode(source.getPeriodCode());
		}
	}

}
