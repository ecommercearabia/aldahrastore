/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahratimeslotfacades.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.store.services.BaseStoreService;

import java.time.format.DateTimeFormatter;

import javax.annotation.Resource;

import com.aldahra.aldahratimeslot.model.TimeSlotInfoModel;
import com.aldahra.aldahratimeslotfacades.TimeSlotInfoData;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class TimeSlotInfoReversePopulator implements Populator<TimeSlotInfoData, TimeSlotInfoModel>
{
	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;
	@Override
	public void populate(final TimeSlotInfoData source, final TimeSlotInfoModel target)
	{
		if (source != null)
		{
			target.setStart(source.getStart().format(DateTimeFormatter.ofPattern("H:mm")));
			target.setEnd(source.getEnd().format(DateTimeFormatter.ofPattern("H:mm")));
			target.setDate(source.getDate());
			target.setDay(source.getDay());
			target.setPeriodCode(source.getPeriodCode());
		}
	}

}
