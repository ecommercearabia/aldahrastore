/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahratimeslotfacades.validation;

import com.aldahra.aldahratimeslotfacades.TimeSlotInfoData;
import com.aldahra.aldahratimeslotfacades.exception.TimeSlotException;


/**
 * @author amjad.shati@erabia.com
 */
public interface TimeSlotValidationService
{
	public boolean validate(final TimeSlotInfoData timeSlotInfo) throws TimeSlotException;
}
