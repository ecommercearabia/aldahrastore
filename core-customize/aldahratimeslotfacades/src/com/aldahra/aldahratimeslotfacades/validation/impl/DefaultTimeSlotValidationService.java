/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahratimeslotfacades.validation.impl;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import javax.annotation.Resource;

import com.aldahra.aldahratimeslot.model.PeriodModel;
import com.aldahra.aldahratimeslot.service.TimeSlotService;
import com.aldahra.aldahratimeslotfacades.TimeSlotInfoData;
import com.aldahra.aldahratimeslotfacades.exception.TimeSlotException;
import com.aldahra.aldahratimeslotfacades.exception.type.TimeSlotExceptionType;
import com.aldahra.aldahratimeslotfacades.validation.TimeSlotValidationService;


/**
 * @author amjad.shati@erabia.com
 */
public class DefaultTimeSlotValidationService implements TimeSlotValidationService
{
	@Resource(name = "timeSlotService")
	private TimeSlotService timeSlotService;

	@Override
	public boolean validate(final TimeSlotInfoData timeSlotInfo) throws TimeSlotException
	{
		if (timeSlotInfo == null || timeSlotInfo.getPeriodCode() == null || timeSlotInfo.getStart() == null
				|| timeSlotInfo.getEnd() == null || timeSlotInfo.getDate() == null)
		{
			throw new TimeSlotException(TimeSlotExceptionType.INVALID_CHOSEN_TIMESLOT, "Missing required parameters.");
		}
		final Optional<PeriodModel> period = timeSlotService.getPeriod(timeSlotInfo.getPeriodCode());
		if (period.isEmpty() || period.get().getTimeSlotWeekDay() == null
				|| period.get().getTimeSlotWeekDay().getTimeSlot() == null)
		{
			throw new TimeSlotException(TimeSlotExceptionType.NO_TIMESLOT_CONFIGURATIONS_AVAILABLE,
					"Invalid time slot configurations.");
		}
		final String start = timeSlotInfo.getStart().format(DateTimeFormatter.ofPattern("H:mm"));
		final String timezone = period.get().getTimeSlotWeekDay().getTimeSlot().getTimezone();
		if (!isPeriodEnabledNow(timeSlotInfo.getStart(), timezone, timeSlotInfo.getDate(), period.get().getExpiry()))
		{
			throw new TimeSlotException(TimeSlotExceptionType.INVALID_CHOSEN_TIMESLOT, "Period not enabled.");
		}

		return true;
	}

	private boolean isPeriodEnabledNow(final LocalTime localTime, final String timezone, final String date, final long expiry)
	{
		final ZonedDateTime now = getTimeNow(timezone);

		final ZonedDateTime intervalStart = ZonedDateTime.of(LocalDate.parse(date, DateTimeFormatter.ofPattern("dd/MM/yyyy")),
				localTime, now.getZone());

		return !(now.isAfter(intervalStart.minusSeconds(expiry)));
	}

	private ZonedDateTime getTimeNow(final String timezone)
	{
		final ZoneId zoneId = ZoneId.of(timezone);
		return ZonedDateTime.ofInstant(Instant.now(), zoneId);
	}
}
