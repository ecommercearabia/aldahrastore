/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahratimeslotfacades.facade.impl;

import de.hybris.platform.basecommerce.enums.WeekDay;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.store.services.BaseStoreService;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.aldahra.aldahratimeslot.enums.TimeFormat;
import com.aldahra.aldahratimeslot.enums.TimeSlotConfigType;
import com.aldahra.aldahratimeslot.model.PeriodModel;
import com.aldahra.aldahratimeslot.model.TimeSlotModel;
import com.aldahra.aldahratimeslot.model.TimeSlotWeekDayModel;
import com.aldahra.aldahratimeslot.service.TimeSlotService;
import com.aldahra.aldahratimeslotfacades.PeriodData;
import com.aldahra.aldahratimeslotfacades.TimeSlotData;
import com.aldahra.aldahratimeslotfacades.TimeSlotDayData;
import com.aldahra.aldahratimeslotfacades.exception.TimeSlotException;
import com.aldahra.aldahratimeslotfacades.exception.type.TimeSlotExceptionType;
import com.aldahra.aldahratimeslotfacades.facade.TimeSlotFacade;
import com.aldahra.core.enums.ShipmentType;



/**
 * @author amjad.shati@erabia.com
 *
 */
public class DefaultTimeSlotFacade implements TimeSlotFacade
{
	private static final String NO_TIMESLOT_CONFIGURATIONS_MSG = "No timeslot found for the zoneDeliveryModeCode or the area you have chosen";
	private static final String NO_TIMESLOT_WEEKDAYS_MSG = "No active timeslot weedays found";
	private static final String NO_TIMEZONE_FOUND_MSG = "No timezone found on TimeSlotModel";
	private static final String INVALID_NUMBER_OF_DAYS_MSG = "Attribute numberOfDays mustn't be zero or negative";

	@Resource(name = "timeSlotService")
	private TimeSlotService timeSlotService;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Override
	public Optional<TimeSlotData> getTimeSlotDataByArea(final String areaCode) throws TimeSlotException
	{
		final Optional<TimeSlotModel> timeSlot = timeSlotService.getActiveByArea(areaCode);
		validateTimeSlotConfigurations(timeSlot);
		return Optional.ofNullable(convert(timeSlot.get()));
	}

	private TimeSlotData convert(final TimeSlotModel timeSlot) throws TimeSlotException
	{
		final TimeSlotData timeSlotData = new TimeSlotData();
		final List<TimeSlotDayData> timeSlotDays = new ArrayList<>();
		final String timezone = timeSlot.getTimezone();
		final int numberOfDays = timeSlot.getNumberOfDays();
		final Map<WeekDay, TimeSlotWeekDayModel> timeSlotWeekDays = getDays(timeSlot.getTimeSlotWeekDays());
		ZonedDateTime today = getTimeNow(timezone);
		for (int i = 0; i < numberOfDays; i++)
		{
			final TimeSlotWeekDayModel timeSlotWeekDay = timeSlotWeekDays.get(WeekDay.valueOf(today.getDayOfWeek().toString()));
			if (timeSlotWeekDay != null && timeSlotWeekDay.isActive())
			{
				final TimeSlotDayData dayData = createTimeSlotDayData(timeSlotWeekDay, today);
				timeSlotDays.add(dayData);
			}
			today = today.plusDays(1);
		}
		if (timeSlotDays.isEmpty())
		{
			throw new TimeSlotException(TimeSlotExceptionType.NO_TIMESLOT_WEEKDAYS, NO_TIMESLOT_WEEKDAYS_MSG);
		}
		timeSlotData.setTimeSlotDays(timeSlotDays);
		return timeSlotData;
	}

	@Override
	public Optional<TimeSlotData> getTimeSlotData(final String zoneDeliveryModeCode) throws TimeSlotException
	{
		final Optional<TimeSlotModel> timeSlot = timeSlotService.getActive(zoneDeliveryModeCode);
		validateTimeSlotConfigurations(timeSlot);
		return Optional.ofNullable(convert(timeSlot.get()));
	}

	private void validateTimeSlotConfigurations(final Optional<TimeSlotModel> timeSlot) throws TimeSlotException
	{
		if (timeSlot.isEmpty())
		{
			throw new TimeSlotException(TimeSlotExceptionType.NO_TIMESLOT_CONFIGURATIONS_AVAILABLE, NO_TIMESLOT_CONFIGURATIONS_MSG);
		}
		if (CollectionUtils.isEmpty(timeSlot.get().getTimeSlotWeekDays()))
		{
			throw new TimeSlotException(TimeSlotExceptionType.NO_TIMESLOT_WEEKDAYS, NO_TIMESLOT_WEEKDAYS_MSG);
		}
		if (StringUtils.isEmpty(timeSlot.get().getTimezone()))
		{
			throw new TimeSlotException(TimeSlotExceptionType.NO_TIMEZONE_FOUND, NO_TIMEZONE_FOUND_MSG);
		}
		if (timeSlot.get().getNumberOfDays() <= 0)
		{
			throw new TimeSlotException(TimeSlotExceptionType.INVALID_NUMBER_OF_DAYS, INVALID_NUMBER_OF_DAYS_MSG);
		}
	}

	private ZonedDateTime getTimeNow(final String timezone)
	{
		final ZoneId zoneId = ZoneId.of(timezone);
		return ZonedDateTime.ofInstant(Instant.now(), zoneId);
	}

	private DateTimeFormatter getDateTimeFormatter(final TimeFormat format)
	{
		if (TimeFormat.HOUR_12.equals(format))
		{
			return DateTimeFormatter.ofPattern("hh:mm a");
		}
		else
		{
			return DateTimeFormatter.ofPattern("H:mm");
		}
	}

	@Override
	public DateTimeFormatter getDateTimeFormatter(final String zoneDeliveryModeCode)
	{
		final Optional<TimeSlotModel> optional = timeSlotService.getActive(zoneDeliveryModeCode);
		if (optional.isPresent())
		{
			return getDateTimeFormatter(optional.get().getTimeFormat());
		}
		return getDateTimeFormatter(TimeFormat.HOUR_24);
	}

	private Map<WeekDay, TimeSlotWeekDayModel> getDays(final List<TimeSlotWeekDayModel> timeSlotWeekDays)
	{
		return timeSlotWeekDays.stream()
				.collect(Collectors.toMap(TimeSlotWeekDayModel::getDay, timeSlotWeekDay -> timeSlotWeekDay));
	}

	private TimeSlotDayData createTimeSlotDayData(final TimeSlotWeekDayModel timeSlotWeekDay, final ZonedDateTime currentDay)
	{
		final TimeSlotDayData dayData = new TimeSlotDayData();
		final List<PeriodData> periods = new ArrayList<>();

		dayData.setDate(currentDay.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		dayData.setDay(timeSlotWeekDay.getName());

		for (final PeriodModel period : timeSlotWeekDay.getPeriods())
		{
			if (period.isActive())
			{
				final PeriodData periodData = createPeriodData(period, dayData.getDate());
				periods.add(periodData);
			}
		}

		dayData.setPeriods(periods.stream().sorted(Comparator.comparing(PeriodData::getStart)).collect(Collectors.toList()));

		return dayData;
	}

	private PeriodData createPeriodData(final PeriodModel period, final String date)
	{
		final DateTimeFormatter formatter = getDateTimeFormatter(period.getTimeSlotWeekDay().getTimeSlot().getTimeFormat());
		final PeriodData periodData = new PeriodData();

		periodData.setCode(period.getCode());

		final LocalTime intervalStart = LocalTime.parse(period.getStart(), getDateTimeFormatter(TimeFormat.HOUR_24));
		final LocalTime intervalEnd = LocalTime.parse(period.getEnd(), getDateTimeFormatter(TimeFormat.HOUR_24));

		periodData.setStart(intervalStart);
		periodData.setEnd(intervalEnd);
		periodData.setIntervalFormattedValue(intervalStart.format(formatter) + " - " + intervalEnd.format(formatter));

		setEnabled(period, periodData, date, period.getCapacity());

		return periodData;
	}

	private void setEnabled(final PeriodModel period, final PeriodData periodData, final String date, final int capacity)
	{
		if (!maxOrderLimitReached(periodData, date, period.getTimeSlotWeekDay().getTimeSlot().getTimezone(), capacity)
				&& isPeriodEnabledNow(periodData, period.getTimeSlotWeekDay().getTimeSlot().getTimezone(), date, period.getExpiry()))
		{
			periodData.setEnabled(true);
		}
	}

	/**
	 * Returns true if the capacity limit is reached per this timeslot.
	 *
	 * @param timezone
	 * @param date
	 */
	private boolean maxOrderLimitReached(final PeriodData periodData, final String date, final String timezone, final int capacity)
	{
		final int numberOfOrdersByTimeSlot = timeSlotService.getNumberOfOrdersByTimeSlot(baseStoreService.getCurrentBaseStore(),
				periodData.getStart().format(getDateTimeFormatter(TimeFormat.HOUR_24)),
				periodData.getEnd().format(getDateTimeFormatter(TimeFormat.HOUR_24)), date, timezone);
		return numberOfOrdersByTimeSlot >= capacity;
	}

	private boolean isPeriodEnabledNow(final PeriodData periodData, final String timezone, final String date, final long expiry)
	{
		final ZonedDateTime now = getTimeNow(timezone);

		final ZonedDateTime intervalStart = ZonedDateTime.of(LocalDate.parse(date, DateTimeFormatter.ofPattern("dd/MM/yyyy")),
				periodData.getStart(), now.getZone());

		return !(now.isAfter(intervalStart.minusSeconds(expiry)));
	}

	@Override
	public boolean isTimeSlotEnabled(final String zoneDeliveryModeCode)
	{
		return timeSlotService.isTimeSlotEnabled(zoneDeliveryModeCode);
	}

	@Override
	public boolean isTimeSlotEnabledByArea(final String areaCode)
	{
		return timeSlotService.isTimeSlotEnabledByArea(areaCode);
	}

	@Override
	public boolean isTimeSlotEnabled(final CMSSiteModel cmsSiteService) throws TimeSlotException
	{
		if (cmsSiteService == null || cmsSiteService.getTimeSlotConfigType() == null || cmsSiteService.isTimeSlotDisabled()
				|| !cartService.hasSessionCart())
		{
			return false;
		}

		final CartModel sessionCart = cartService.getSessionCart();
		AddressModel address = null;
		if (TimeSlotConfigType.BY_AREA.equals(cmsSiteService.getTimeSlotConfigType()))
		{


			if (ShipmentType.PICKUP_IN_STORE.equals(sessionCart.getShipmentType()))
			{
				address = sessionCart.getSite().getDefaultDeliveryPointOfService() == null
						|| sessionCart.getSite().getDefaultDeliveryPointOfService().getAddress() == null ? null
								: sessionCart.getSite().getDefaultDeliveryPointOfService().getAddress();
			}
			else
			{
				address = sessionCart.getDeliveryAddress();
			}


			if (address == null || address.getArea() == null)
			{
				throw new TimeSlotException(TimeSlotExceptionType.NO_DELIVERY_AREA_SELECTED, "NO_DELIVERY_AREA_SELECTED");
			}

			try
			{
				final Optional<TimeSlotData> timeSlot = getTimeSlotDataByArea(address.getArea().getCode());

				return timeSlot.isPresent();
			}
			catch (final TimeSlotException e)
			{
				return false;
			}

		}
		else if (TimeSlotConfigType.BY_DELIVERYMODE.equals(cmsSiteService.getTimeSlotConfigType()))
		{
			if (sessionCart.getDeliveryMode() == null)
			{
				throw new TimeSlotException(TimeSlotExceptionType.NO_DELIVERY_METHOD_SELECTED, "NO_DELIVERY_METHOD_SELECTED");
			}
			try
			{
				final Optional<TimeSlotData> timeSlot = getTimeSlotData(sessionCart.getDeliveryMode().getCode());

				return timeSlot.isPresent();
			}
			catch (final TimeSlotException e)
			{
				return false;
			}
		}


		return false;
	}

	@Override
	public boolean isTimeSlotEnabledByCurrentSite() throws TimeSlotException
	{
		return cmsSiteService.getCurrentSite() == null ? false : isTimeSlotEnabled(cmsSiteService.getCurrentSite());
	}
}
