/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahratimeslotfacades.exception.type;

/**
 *
 */
public enum TimeSlotExceptionType
{
	NO_TIMESLOT_CONFIGURATIONS_AVAILABLE, NO_TIMESLOT_WEEKDAYS, NO_TIMEZONE_FOUND, INVALID_NUMBER_OF_DAYS, NO_TIMESLOTS_FOUND, INVALID_CHOSEN_TIMESLOT, NO_DELIVERY_AREA_SELECTED, NO_DELIVERY_METHOD_SELECTED;
}
