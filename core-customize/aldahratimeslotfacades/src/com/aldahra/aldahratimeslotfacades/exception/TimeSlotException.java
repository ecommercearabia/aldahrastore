/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahratimeslotfacades.exception;

import java.util.Arrays;

import com.aldahra.aldahratimeslotfacades.exception.type.TimeSlotExceptionType;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class TimeSlotException extends Exception
{
	private final TimeSlotExceptionType timeSlotExceptionType;


	public TimeSlotException(final TimeSlotExceptionType timeSlotExceptionType, final String message)
	{
		super(message);
		this.timeSlotExceptionType = timeSlotExceptionType;
	}

	public TimeSlotException(final Throwable sourceException, final TimeSlotExceptionType timeSlotExceptionType,
			final String message)
	{
		super(message, sourceException);
		this.timeSlotExceptionType = timeSlotExceptionType;
	}

	/**
	 * @return the timeSlotExceptionType
	 */
	public TimeSlotExceptionType getTimeSlotExceptionType()
	{
		return timeSlotExceptionType;
	}

	@Override
	public String toString()
	{
		return "TimeSlotException [timeSlotExceptionType=" + timeSlotExceptionType + ", getMessage()=" + getMessage()
				+ ", getStackTrace()=" + Arrays.toString(getStackTrace()) + "]";
	}


}
