package com.aldahra.aldahracomponentsfacades.constants;

@SuppressWarnings({"deprecation","squid:CallToDeprecatedMethod"})
public class AldahracomponentsfacadesConstants extends GeneratedAldahracomponentsfacadesConstants
{
	public static final String EXTENSIONNAME = "aldahracomponentsfacades";
	
	private AldahracomponentsfacadesConstants()
	{
		//empty
	}
	
	
}
