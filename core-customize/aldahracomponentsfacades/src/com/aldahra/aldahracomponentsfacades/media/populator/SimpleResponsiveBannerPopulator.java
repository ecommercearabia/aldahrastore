/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahracomponentsfacades.media.populator;

import de.hybris.platform.acceleratorcms.model.components.SimpleResponsiveBannerComponentModel;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.aldahra.aldahracomponents.data.SimpleResponsiveBannerComponentData;


/**
 * The Class SimpleResponsiveBannerPopulator.
 *
 * @author amjad.shati@erabia.com
 *
 *         The Class SimpleResponsiveBannerPopulator.
 */
public class SimpleResponsiveBannerPopulator
		implements Populator<SimpleResponsiveBannerComponentModel, SimpleResponsiveBannerComponentData>
{


	/** The media container converter. */
	@Resource(name = "responsiveMediaContainerConverter")
	private Converter<MediaContainerModel, List<ImageData>> mediaContainerConverter;

	/** The commerce common I18N service. */
	@Resource(name = "commerceCommonI18NService")
	private CommerceCommonI18NService commerceCommonI18NService;

	/**
	 * Fill the source to target.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 * @throws ConversionException
	 *            the conversion exception
	 */
	@Override
	public void populate(final SimpleResponsiveBannerComponentModel source, final SimpleResponsiveBannerComponentData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setUrlLink(source.getUrlLink());
		target.setYoutubeId(source.getYoutubeId());
		target.setMedias(mediaContainerConverter.convert(source.getMedia(commerceCommonI18NService.getCurrentLocale())));
	}

}
