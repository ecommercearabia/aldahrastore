/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.aldahra.aldahraotpbackoffice.services;

/**
 * Hello World AldahraotpbackofficeService
 */
public class AldahraotpbackofficeService
{
	public String getHello()
	{
		return "Hello";
	}
}
