/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.test.constants;

/**
 * 
 */
public class AldahraTestConstants extends GeneratedAldahraTestConstants
{

	public static final String EXTENSIONNAME = "aldahratest";

}
