package com.aldahra.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.PriceService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;

import com.aldahra.core.service.ProductPromotionPercentageDiscount;



public class ProductPromotionPercentageValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider
{
	private FieldNameProvider fieldNameProvider;

	private PriceService priceService;

	@Resource(name = "productPromotionPercentageDiscount")
	private ProductPromotionPercentageDiscount productPromotionPercentageDiscount;

	/**
	 * @return the productPromotionPercentageDiscount
	 */
	protected ProductPromotionPercentageDiscount getProductPromotionPercentageDiscount()
	{
		return productPromotionPercentageDiscount;
	}

	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{
		if (!(model instanceof ProductModel))
		{
			throw new FieldValueProviderException("Cannot evaluate price of non-product item");
		}


		final Collection<FieldValue> fieldValues = new ArrayList<>();
		final ProductModel product = (ProductModel) model;

		if (indexedProperty.isCurrency() && CollectionUtils.isNotEmpty(indexConfig.getCurrencies()))
		{

			final CurrencyModel sessionCurrency = this.i18nService.getCurrentCurrency();

			try
			{
				for (final CurrencyModel currency : indexConfig.getCurrencies())
				{

					this.i18nService.setCurrentCurrency(currency);
					addFieldValues(fieldValues, product, indexedProperty, currency.getIsocode());

				}
			}
			finally
			{
				this.i18nService.setCurrentCurrency(sessionCurrency);
			}
		}
		else
		{
			addFieldValues(fieldValues, product, indexedProperty, (String) null);
		}
		return fieldValues;
	}



	protected void addFieldValues(final Collection<FieldValue> fieldValues, final ProductModel product,
			final IndexedProperty indexedProperty, final String currency) throws FieldValueProviderException
	{
		final double promotionPercentageDiscount = getProductPromotionPercentageDiscount().getPromotionPercentageDiscount(product);

		final Collection<String> fieldNames = this.fieldNameProvider.getFieldNames(indexedProperty,
				(currency == null) ? null : currency.toLowerCase(Locale.ROOT));

		for (final String fieldName : fieldNames)
		{
			fieldValues.add(new FieldValue(fieldName, promotionPercentageDiscount));
		}
	}




	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}


	public void setPriceService(final PriceService priceService)
	{
		this.priceService = priceService;
	}
}
