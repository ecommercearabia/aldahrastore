package com.aldahra.core.exception;

import com.aldahra.core.exception.enums.LoyaltyPointExceptionType;


/**
 * @author yhammad
 *
 */
public class LoyaltyPointException extends Exception
{

	private final LoyaltyPointExceptionType loyaltyPointExceptionType;


	public LoyaltyPointException(final String message, final LoyaltyPointExceptionType type)
	{
		super(message);
		this.loyaltyPointExceptionType = type;
	}

	/**
	 * @return the type
	 */
	public LoyaltyPointExceptionType getLoyaltyPointExceptionType()
	{
		return loyaltyPointExceptionType;
	}
}
