package com.aldahra.core.exception.enums;


public enum LoyaltyPointExceptionType
{
	NOT_AVAILABLE, MIN_LIMIT_CONVERT, BALANCE_NOT_AVAILABLE, CONFIG_ERROR;
}
