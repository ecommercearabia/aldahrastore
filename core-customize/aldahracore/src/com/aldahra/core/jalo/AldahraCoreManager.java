/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.core.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.aldahra.core.constants.AldahraCoreConstants;
import com.aldahra.core.setup.CoreSystemSetup;


/**
 * Do not use, please use {@link CoreSystemSetup} instead.
 * 
 */
public class AldahraCoreManager extends GeneratedAldahraCoreManager
{
	public static final AldahraCoreManager getInstance()
	{
		final ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (AldahraCoreManager) em.getExtension(AldahraCoreConstants.EXTENSIONNAME);
	}
}
