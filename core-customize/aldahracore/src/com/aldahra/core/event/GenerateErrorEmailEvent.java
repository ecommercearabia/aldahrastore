package com.aldahra.core.event;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;

import com.aldahra.core.enums.IntegrationProvider;



/**
 * @author abu-muhasien
 */
public class GenerateErrorEmailEvent extends AbstractEvent
{
	private IntegrationProvider integrationProvider;
	private AbstractOrderModel abstractOrder;
	private String errorMsg;

	/**
	 * Instantiates a new payment failed notification email event.
	 *
	 * @param process the process
	 */
	public GenerateErrorEmailEvent(final IntegrationProvider integrationProvider, final AbstractOrderModel abstractOrder, final String errorMsg)
	{
		super();
		this.integrationProvider=integrationProvider;
		this.abstractOrder=abstractOrder;
		this.errorMsg=errorMsg;
	}

	/**
	 * @return the errorMsg
	 */
	public String getErrorMsg()
	{
		return errorMsg;
	}

	/**
	 * @param errorMsg
	 *           the errorMsg to set
	 */
	public void setErrorMsg(final String errorMsg)
	{
		this.errorMsg = errorMsg;
	}

	/**
	 * @return the abstractOrder
	 */
	public AbstractOrderModel getAbstractOrder()
	{
		return abstractOrder;
	}

	/**
	 * @param abstractOrder
	 *           the abstractOrder to set
	 */
	public void setAbstractOrder(final AbstractOrderModel abstractOrder)
	{
		this.abstractOrder = abstractOrder;
	}

	/**
	 * @return the integrationProvider
	 */
	public IntegrationProvider getIntegrationProvider()
	{
		return integrationProvider;
	}

	/**
	 * @param integrationProvider
	 *           the integrationProvider to set
	 */
	public void setIntegrationProvider(final IntegrationProvider integrationProvider)
	{
		this.integrationProvider = integrationProvider;
	}
}
