package com.aldahra.core.event;

import de.hybris.platform.acceleratorservices.site.AbstractAcceleratorSiteEventListener;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.StringUtils;

import com.aldahra.core.model.GenerateNotificationEmailProcessModel;
import com.google.common.base.Preconditions;



/**
 *
 * @author Tuqa
 *
 */
public class GenerateNotificationEmailListener extends AbstractAcceleratorSiteEventListener<GenerateNotificationEmailEvent>
{

	/** The model service. */
	private ModelService modelService;

	/** The business process service. */
	private BusinessProcessService businessProcessService;

	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}


	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}


	@Override
	protected void onSiteEvent(final GenerateNotificationEmailEvent event)
	{
		Preconditions.checkArgument(event.getAbstractOrder() != null, "AbstractOrder must not be null.");
		Preconditions.checkArgument(!StringUtils.isEmpty(event.getMsg()), "Error message must not be null or empty.");
		Preconditions.checkArgument(!StringUtils.isEmpty(event.getIntegrationProvider()), "Integration Provider must not be null.");
		final GenerateNotificationEmailProcessModel sendingErrorEmailProcessModel = getBusinessProcessService().createProcess(
				"generateNotificationEmail-process-" + event.getAbstractOrder().getCode() + "-" + System.currentTimeMillis(),
				"generateNotificationEmail-process");
		sendingErrorEmailProcessModel.setOrder(event.getAbstractOrder());
		sendingErrorEmailProcessModel.setIntegrationProvider(event.getIntegrationProvider());
		sendingErrorEmailProcessModel.setNotificationMsg(event.getMsg());
		sendingErrorEmailProcessModel.setOperation(event.getOperation());
		getModelService().save(sendingErrorEmailProcessModel);
		getBusinessProcessService().startProcess(sendingErrorEmailProcessModel);
	}

	@Override
	protected boolean shouldHandleEvent(final GenerateNotificationEmailEvent event)
	{
		final AbstractOrderModel order = event.getAbstractOrder();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order", order);
		final BaseSiteModel site = order.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order.site", site);
		return site.getChannel() != null;
	}


	@Override
	protected SiteChannel getSiteChannelForEvent(final GenerateNotificationEmailEvent event)
	{
		final AbstractOrderModel order = event.getAbstractOrder();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order", order);
		final BaseSiteModel site = order.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order.site", site);
		return site.getChannel();
	}
}
