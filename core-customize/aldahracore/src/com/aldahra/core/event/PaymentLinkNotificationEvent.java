/**
 *
 */
package com.aldahra.core.event;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;
import de.hybris.platform.core.model.order.AbstractOrderModel;


/**
 * @author mohammad-abumuahsien
 *
 */
public class PaymentLinkNotificationEvent extends AbstractCommerceUserEvent<BaseSiteModel>
{
	private AbstractOrderModel order;

	private String paymentLink;

	private String invoiceId;

	public PaymentLinkNotificationEvent()
	{

	}

	public PaymentLinkNotificationEvent(final AbstractOrderModel order)
	{
		this.order = order;
	}

	/**
	 * @return the order
	 */
	public AbstractOrderModel getOrder()
	{
		return order;
	}

	/**
	 * @param order
	 *           the order to set
	 */
	public void setOrder(final AbstractOrderModel order)
	{
		this.order = order;
	}

	/**
	 * @return the paymentLink
	 */
	public String getPaymentLink()
	{
		return paymentLink;
	}

	/**
	 * @param paymentLink
	 *           the paymentLink to set
	 */
	public void setPaymentLink(final String paymentLink)
	{
		this.paymentLink = paymentLink;
	}

	/**
	 * @return the invoiceId
	 */
	public String getInvoiceId()
	{
		return invoiceId;
	}

	/**
	 * @param invoiceId
	 *           the invoiceId to set
	 */
	public void setInvoiceId(final String invoiceId)
	{
		this.invoiceId = invoiceId;
	}

}
