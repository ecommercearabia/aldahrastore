package com.aldahra.core.event;

import de.hybris.platform.acceleratorservices.site.AbstractAcceleratorSiteEventListener;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.StringUtils;

import com.aldahra.core.model.GenerateErrorEmailProcessModel;
import com.google.common.base.Preconditions;



/**
 *
 * @author amjad.shati@erabia,com
 *
 */
public class GenerateErrorEmailListener extends AbstractAcceleratorSiteEventListener<GenerateErrorEmailEvent>
{

	/** The model service. */
	private ModelService modelService;

	/** The business process service. */
	private BusinessProcessService businessProcessService;

	/**
	 * Gets the business process service.
	 *
	 * @return the business process service
	 */
	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	/**
	 * Sets the business process service.
	 *
	 * @param businessProcessService
	 *           the new business process service
	 */
	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	/**
	 * Gets the model service.
	 *
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * Sets the model service.
	 *
	 * @param modelService
	 *           the modelService to set
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * On site event.
	 *
	 * @param event
	 *           the event
	 */
	@Override
	protected void onSiteEvent(final GenerateErrorEmailEvent event)
	{
		Preconditions.checkArgument(event.getAbstractOrder() != null, "AbstractOrder must not be null.");
		Preconditions.checkArgument(!StringUtils.isEmpty(event.getErrorMsg()), "Error message must not be null or empty.");
		Preconditions.checkArgument(!StringUtils.isEmpty(event.getIntegrationProvider()), "Integration Provider must not be null.");
		final GenerateErrorEmailProcessModel sendingErrorEmailProcessModel = getBusinessProcessService().createProcess(
				"generateErrorEmail-process-" + event.getAbstractOrder().getCode() + "-" + System.currentTimeMillis(),
				"generateErrorEmail-process");
		sendingErrorEmailProcessModel.setOrder(event.getAbstractOrder());
		sendingErrorEmailProcessModel.setIntegrationProvider(event.getIntegrationProvider());
		sendingErrorEmailProcessModel.setErrorMsg(event.getErrorMsg());
		getModelService().save(sendingErrorEmailProcessModel);
		getBusinessProcessService().startProcess(sendingErrorEmailProcessModel);
	}

	/**
	 * Should handle event.
	 *
	 * @param event
	 *           the event
	 * @return true, if successful
	 */
	@Override
	protected boolean shouldHandleEvent(final GenerateErrorEmailEvent event)
	{
		final AbstractOrderModel order = event.getAbstractOrder();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order", order);
		final BaseSiteModel site = order.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order.site", site);
		return site.getChannel() != null;
	}

	/**
	 * Gets the site channel for event.
	 *
	 * @param event
	 *           the event
	 * @return the site channel for event
	 */
	@Override
	protected SiteChannel getSiteChannelForEvent(final GenerateErrorEmailEvent event)
	{
		final AbstractOrderModel order = event.getAbstractOrder();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order", order);
		final BaseSiteModel site = order.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order.site", site);
		return site.getChannel();
	}
}
