package com.aldahra.core.event;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;

import com.aldahra.core.enums.IntegrationProvider;



/**
 * @author Tuqa
 */
public class GenerateNotificationEmailEvent extends AbstractEvent
{
	private IntegrationProvider integrationProvider;
	private AbstractOrderModel abstractOrder;
	private String msg;
	private String operation;


	public GenerateNotificationEmailEvent(final IntegrationProvider integrationProvider, final AbstractOrderModel abstractOrder,
			final String msg, final String operation)
	{
		super();
		this.integrationProvider = integrationProvider;
		this.abstractOrder = abstractOrder;
		this.msg = msg;
		this.operation = operation;
	}


	public String getMsg()
	{
		return msg;
	}


	public void setMsg(final String msg)
	{
		this.msg = msg;
	}


	public AbstractOrderModel getAbstractOrder()
	{
		return abstractOrder;
	}


	public void setAbstractOrder(final AbstractOrderModel abstractOrder)
	{
		this.abstractOrder = abstractOrder;
	}


	public IntegrationProvider getIntegrationProvider()
	{
		return integrationProvider;
	}


	public void setIntegrationProvider(final IntegrationProvider integrationProvider)
	{
		this.integrationProvider = integrationProvider;
	}

	/**
	 * @return the operation
	 */
	public String getOperation()
	{
		return operation;
	}


	/**
	 * @param operation
	 *           the operation to set
	 */
	public void setOperation(final String operation)
	{
		this.operation = operation;
	}
}
