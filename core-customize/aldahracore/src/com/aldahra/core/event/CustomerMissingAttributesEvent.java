/**
 *
 */
package com.aldahra.core.event;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;

import java.util.List;


/**
 * @author monzer
 *
 */
public class CustomerMissingAttributesEvent extends AbstractCommerceUserEvent<BaseSiteModel>
{

	private final List<String> missingAttributes;

	public CustomerMissingAttributesEvent(final List<String> missingAttributes)
	{
		this.missingAttributes = missingAttributes;
	}

	/**
	 * @return the missingAttributes
	 */
	public List<String> getMissingAttributes()
	{
		return missingAttributes;
	}

}
