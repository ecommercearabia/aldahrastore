/**
 *
 */
package com.aldahra.core.dao;

import de.hybris.platform.core.model.user.CustomerModel;

import java.util.List;


/**
 *
 * @author Tuqa
 *
 * 
 */
public interface CustomCustomerDao
{



	List<CustomerModel> getAllCustomersWithoutNationalityOrTitle();


	List<CustomerModel> getAllCustomers();

	List<CustomerModel> getAllCustomersAboveLoyaltyPointLimit(double limit);

}
