/**
 *
 */
package com.aldahra.core.dao;

import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import java.util.List;


/**
 * @author monzer
 *
 */
public interface CustomStockLevelDao
{

	List<StockLevelModel> getByLotNumberAndWarehouse(String productCode, String lotNumber, WarehouseModel warehouse);

	List<StockLevelModel> getStockLevelsByProductCode(String productCode);

}
