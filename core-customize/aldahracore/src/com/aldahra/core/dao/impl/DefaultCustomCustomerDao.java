/**
 *
 */
package com.aldahra.core.dao.impl;

import de.hybris.platform.commerceservices.customer.dao.impl.DefaultCustomerDao;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.map.HashedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aldahra.core.dao.CustomCustomerDao;



/**
 * The Class DefaultCustomCustomerDao.
 *
 * @author Tuqa

 */
public class DefaultCustomCustomerDao extends DefaultCustomerDao implements CustomCustomerDao
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultCustomCustomerDao.class);

	/** The flexible search service. */

	@Resource(name = "flexibleSearchService")

	private FlexibleSearchService flexibleSearchService;


	private static final String BASE_QUERY = "SELECT {" + CustomerModel.PK + "} from {" + CustomerModel._TYPECODE + " }";

	@Override
	public List<CustomerModel> getAllCustomersWithoutNationalityOrTitle()
	{
		final String query = BASE_QUERY + " where {" + CustomerModel.NATIONALITY + "} IS NULL or {" + CustomerModel.TITLE
				+ "} IS NULL";
		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query);
		return getFlexibleSearchService().<CustomerModel> search(searchQuery).getResult();
	}


	/**
	 * Gets the all customers.
	 *
	 * @return the all customers
	 */
	@Override
	public List<CustomerModel> getAllCustomers()
	{
		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(BASE_QUERY);
		final Map<String, Object> queryParams = new HashedMap();

		searchQuery.addQueryParameters(queryParams);

		return getFlexibleSearchService().<CustomerModel> search(searchQuery).getResult();
	}

	/**
	 * @return the flexibleSearchService
	 */
	@Override
	protected FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	@Override
	public List<CustomerModel> getAllCustomersAboveLoyaltyPointLimit(final double limit)
	{

		final String query = BASE_QUERY + " where {"
				+ CustomerModel.LOYALTYPOINTS + "} >= ?limit ";
		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query);
		final Map<String, Object> queryParams = new HashedMap();
		queryParams.put("limit", limit);
		searchQuery.addQueryParameters(queryParams);

		return getFlexibleSearchService().<CustomerModel> search(searchQuery).getResult();
	}



}
