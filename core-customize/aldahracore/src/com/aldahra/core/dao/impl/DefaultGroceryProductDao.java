/**
 *
 */
package com.aldahra.core.dao.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.aldahra.aldahraproduct.model.GroceryVariantProductModel;
import com.aldahra.core.dao.GroceryProductDao;

/**
 * @author monzer
 *
 */
public class DefaultGroceryProductDao implements GroceryProductDao
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultGroceryProductDao.class);

	private static final String CATALOG_VERSION_ATTRIBUTE = "catalogVersion";

	private static final String ALL_PRODUCT_QUERY = "select {" + GroceryVariantProductModel.PK + "} from {"
			+ GroceryVariantProductModel._TYPECODE + " as p join " + CatalogVersionModel._TYPECODE + " as c on {p."
			+ GroceryVariantProductModel.CATALOGVERSION + "} = {c." + CatalogVersionModel.PK + "}} where {c."
			+ CatalogVersionModel.PK
			+ "}=?" + CATALOG_VERSION_ATTRIBUTE;

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	@Override
	public List<GroceryVariantProductModel> getAllGroceryProductByCatalogVersion(final CatalogVersionModel catalogVersion)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(ALL_PRODUCT_QUERY);
		query.addQueryParameter(CATALOG_VERSION_ATTRIBUTE, catalogVersion.getPk());
		final SearchResult<GroceryVariantProductModel> searchResult = flexibleSearchService
				.<GroceryVariantProductModel> search(query);

		if (searchResult == null || CollectionUtils.isEmpty(searchResult.getResult()))
		{
			LOG.error("No products found or retrieved");
			return Collections.emptyList();
		}

		return searchResult.getResult();
	}

}
