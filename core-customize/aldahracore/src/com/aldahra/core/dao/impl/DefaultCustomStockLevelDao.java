/**
 *
 */
package com.aldahra.core.dao.impl;

import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.StringUtils;

import com.aldahra.core.dao.CustomStockLevelDao;
import com.google.common.base.Preconditions;


/**
 * @author monzer
 *
 */
public class DefaultCustomStockLevelDao implements CustomStockLevelDao
{

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	@Override
	public List<StockLevelModel> getByLotNumberAndWarehouse(final String productCode, final String lotNumber,
			final WarehouseModel warehouse)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(productCode), "cannot find stock level with empty product");
		Preconditions.checkArgument(warehouse != null, "cannot find stock level with empty warehouse");
		Preconditions.checkArgument(StringUtils.isNotBlank(lotNumber), "cannot find stock level with empty lot number");

		final String query = "SELECT {" + StockLevelModel.PK + "} from {" + StockLevelModel._TYPECODE + "} where {"
				+ StockLevelModel.PRODUCTCODE + "}=?productCode and {" + StockLevelModel.LOTNUMBER + "}=?lotNumber and {"
				+ StockLevelModel.WAREHOUSE + "}=?warehouse";
		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query);
		final Map<String, Object> queryParams = new HashedMap();
		queryParams.put("productCode", productCode);
		queryParams.put("lotNumber", lotNumber);
		queryParams.put("warehouse", warehouse);
		searchQuery.addQueryParameters(queryParams);

		return flexibleSearchService.<StockLevelModel> search(searchQuery).getResult();
	}

	@Override
	public List<StockLevelModel> getStockLevelsByProductCode(final String productCode)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(productCode), "cannot find stock level with empty product");

		final String query = "SELECT {" + StockLevelModel.PK + "} from {" + StockLevelModel._TYPECODE + "} where {"
				+ StockLevelModel.PRODUCTCODE + "}=?productCode ";

		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query);
		final Map<String, Object> queryParams = new HashedMap();
		queryParams.put("productCode", productCode);
		searchQuery.addQueryParameters(queryParams);

		return flexibleSearchService.<StockLevelModel> search(searchQuery).getResult();
	}

}
