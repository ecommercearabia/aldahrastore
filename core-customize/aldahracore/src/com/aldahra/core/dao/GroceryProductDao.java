/**
 *
 */
package com.aldahra.core.dao;

import de.hybris.platform.catalog.model.CatalogVersionModel;

import java.util.List;

import com.aldahra.aldahraproduct.model.GroceryVariantProductModel;


/**
 * @author monzer
 *
 */
public interface GroceryProductDao
{

	List<GroceryVariantProductModel> getAllGroceryProductByCatalogVersion(CatalogVersionModel catalogVersion);

}
