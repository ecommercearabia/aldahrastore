/**
 *
 */
package com.aldahra.core.service;

import de.hybris.platform.core.model.user.UserModel;

import java.util.List;

import com.hybris.backoffice.model.user.BackofficeRoleModel;

/**
 * @author monzer
 *
 */
public interface WarehousingAndOMSAuthorityService
{
	List<BackofficeRoleModel> getAuthorityGroupForCurrentUser();

	List<BackofficeRoleModel> getAuthorityGroupForUser(UserModel user);

}
