/**
 *
 */
package com.aldahra.core.service;

import java.util.Optional;


/**
 * The Interface CustomerMatchingService.
 *
 * @author monzer
 */
public interface CustomerMatchingService
{

	/**
	 * Gets Match the identifier if it is email or mobile number.
	 *
	 * @param identifier
	 *           the identifier
	 * @return the customer UID
	 */
	Optional<String> getCustomerUidAfterMatching(String identifier);

}
