package com.aldahra.core.service;

import de.hybris.platform.core.model.product.ProductModel;


/**
 * @author mnasro
 *
 */
public interface ProductPromotionPercentageDiscount
{
	public double getPromotionPercentageDiscount(final ProductModel productModel);

}
