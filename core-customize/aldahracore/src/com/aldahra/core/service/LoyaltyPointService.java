package com.aldahra.core.service;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;

import com.aldahra.core.exception.LoyaltyPointException;
import com.aldahra.core.model.LoyaltyPointHistoryModel;


/**
 * @author mnasro
 *
 */
public interface LoyaltyPointService
{

	public List<LoyaltyPointHistoryModel> getLoyaltyPointHistory(CustomerModel customerModel);

	public List<LoyaltyPointHistoryModel> getLoyaltyPointHistoryByCurrentCustomer();

	public double getLoyaltyPoints(CustomerModel customerModel);

	public double getLoyaltyPointsByCurrentCustomer();

	public boolean isLoyaltyPointsEnabled(BaseStoreModel baseStoreModel);

	public boolean isLoyaltyPointsEnabledByCurrentStore();

	public void addLoyaltyPoints(ConsignmentModel consignmentModel) throws LoyaltyPointException;

	public void redeemLoyaltyPoints(BaseStoreModel baseStoreModel, CustomerModel customerModel, double redeemLoyaltyPoint)
			throws LoyaltyPointException;

	public void redeemLoyaltyPoints(BaseStoreModel baseStoreModel, CustomerModel customerModel) throws LoyaltyPointException;

	public void returnLoyaltyPoints(ReturnRequestModel returnModel) throws LoyaltyPointException;

}
