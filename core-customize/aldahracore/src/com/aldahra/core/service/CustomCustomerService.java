/**
 *
 */
package com.aldahra.core.service;

import de.hybris.platform.core.model.user.CustomerModel;

import java.util.List;



/**
 * The Interface CustomCustomerService.
 *
 * @author Tuqa
 */
public interface CustomCustomerService
{

	List<CustomerModel> getAllCustomersWithoutNationalityOrTitle();

	/**
	 * Gets the all customers.
	 *
	 * @return the all customers
	 */
	List<CustomerModel> getAllCustomers();

	List<CustomerModel> getAllCustomersAboveLoyaltyPointLimit(double limit);

}
