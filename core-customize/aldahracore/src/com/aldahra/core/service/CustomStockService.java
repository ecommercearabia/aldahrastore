/**
 *
 */
package com.aldahra.core.service;

import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.stock.StockService;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;

import java.util.List;


/**
 * @author monzer
 *
 */
public interface CustomStockService extends StockService
{

	List<StockLevelModel> getByLotNumberAndWarehouse(String productCode, String lotNumber, WarehouseModel warehouse);

	List<StockLevelModel> getStockLevelsByProductCode(final String productCode);

	void reserve(StockLevelModel stockLevel, int reserveAmount) throws InsufficientStockLevelException;

	void release(StockLevelModel stockLevel, int releaseAmount);

}
