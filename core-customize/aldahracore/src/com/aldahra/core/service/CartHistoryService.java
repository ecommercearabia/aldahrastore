/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.core.service;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.AbstractOrderModel;


/**
 *
 */
public interface CartHistoryService
{
	void updateCartHistory(AbstractOrderModel abstractOrder, SalesApplication sourceApplication, String operationName);

}