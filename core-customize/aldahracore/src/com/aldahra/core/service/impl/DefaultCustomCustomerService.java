/**
 *
 */
package com.aldahra.core.service.impl;

import de.hybris.platform.core.model.user.CustomerModel;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aldahra.core.dao.CustomCustomerDao;
import com.aldahra.core.service.CustomCustomerService;



/**
 * The Class DefaultCustomCustomerService.
 *
 * @author Tuqa
 */
public class DefaultCustomCustomerService implements CustomCustomerService
{


	private static final Logger LOG = LoggerFactory.getLogger(DefaultCustomCustomerService.class);
	/** The default custom customer dao. */

	@Resource(name = "customCustomerDao")

	private CustomCustomerDao customCustomerDao;


	@Override
	public List<CustomerModel> getAllCustomersWithoutNationalityOrTitle()
	{

		return getcustomCustomerDao().getAllCustomersWithoutNationalityOrTitle();
	}



	/**
	 * Gets the all customers.
	 *
	 * @return the all customers
	 */
	@Override
	public List<CustomerModel> getAllCustomers()
	{
		return getcustomCustomerDao().getAllCustomers();
	}

	/**
	 * Gets the default custom customer dao.
	 *
	 * @return the defaultCustomCustomerDao
	 */
	protected CustomCustomerDao getcustomCustomerDao()

	{
		return customCustomerDao;
	}

	@Override
	public List<CustomerModel> getAllCustomersAboveLoyaltyPointLimit(final double limit)
	{
		if (limit == 0)
		{
			return Collections.emptyList();
		}
		return getcustomCustomerDao().getAllCustomersAboveLoyaltyPointLimit(limit);
	}




}
