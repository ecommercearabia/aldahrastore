/**
 *
 */
package com.aldahra.core.service.impl;

import de.hybris.platform.basecommerce.enums.StockLevelUpdateType;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;
import de.hybris.platform.stock.impl.DefaultStockService;
import de.hybris.platform.stock.model.StockLevelHistoryEntryModel;
import de.hybris.platform.util.Utilities;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.aldahra.core.dao.CustomStockLevelDao;
import com.aldahra.core.service.CustomStockService;
import com.google.common.base.Preconditions;


/**
 * @author monzer
 *
 */
public class DefaultCustomStockService extends DefaultStockService implements CustomStockService
{

	@Resource(name = "customStockLevelDao")
	private CustomStockLevelDao customStockLevelDao;

	@Override
	public List<StockLevelModel> getByLotNumberAndWarehouse(final String productCode, final String lotNumber,
			final WarehouseModel warehouse)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(productCode), "cannot find stock level with empty product");
		Preconditions.checkArgument(warehouse != null, "cannot find stock level with empty warehouse");
		Preconditions.checkArgument(StringUtils.isNotBlank(lotNumber), "cannot find stock level with empty lot number");

		return customStockLevelDao.getByLotNumberAndWarehouse(productCode, lotNumber, warehouse);
	}


	@Override
	public List<StockLevelModel> getStockLevelsByProductCode(final String productCode)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(productCode), "cannot find stock level with empty product");
		return customStockLevelDao.getStockLevelsByProductCode(productCode);
	}

	@Override
	public void reserve(final StockLevelModel stockLevel, final int reserveAmount) throws InsufficientStockLevelException
	{
		if (reserveAmount <= 0)
		{
			throw new IllegalArgumentException("amount must be greater than zero.");
		}
		if (stockLevel == null)
		{
			throw new IllegalArgumentException("Cannot reserve on empty stock level");
		}

		final Integer reserved = this.getStockLevelDao().reserve(stockLevel, reserveAmount);
		if (reserved == null)
		{
			throw new InsufficientStockLevelException("insufficient available amount for stock level [" + stockLevel.getPk() + "]");
		}
		this.clearCacheForItem(stockLevel);
		this.createStockLevelHistoryEntry(stockLevel, StockLevelUpdateType.CUSTOMER_RESERVE, reserved, null);
	}

	private void clearCacheForItem(final StockLevelModel stockLevel)
	{
		Utilities.invalidateCache(stockLevel.getPk());
		this.getModelService().refresh(stockLevel);
	}

	private StockLevelHistoryEntryModel createStockLevelHistoryEntry(final StockLevelModel stockLevel,
			final StockLevelUpdateType updateType, final int reserved, final String comment)
	{
		if (stockLevel.getMaxStockLevelHistoryCount() != 0)
		{
			final StockLevelHistoryEntryModel historyEntry = this.getModelService().create(StockLevelHistoryEntryModel.class);
			historyEntry.setStockLevel(stockLevel);
			historyEntry.setActual(stockLevel.getAvailable());
			historyEntry.setReserved(reserved);
			historyEntry.setUpdateType(updateType);
			if (comment != null)
			{
				historyEntry.setComment(comment);
			}
			historyEntry.setUpdateDate(new Date());
			this.getModelService().save(historyEntry);
			return historyEntry;
		}
		else
		{
			return null;
		}
	}

	@Override
	public void release(final StockLevelModel stockLevel, final int releaseAmount)
	{
		if (releaseAmount <= 0)
		{
			throw new IllegalArgumentException("amount must be greater than zero.");
		}
		if (stockLevel == null)
		{
			throw new IllegalArgumentException("Cannot release on empty stock level");
		}

		final Integer released = this.getStockLevelDao().release(stockLevel, releaseAmount);
		if (released == null)
		{
			throw new SystemException("insufficient available amount for stock level [" + stockLevel.getPk() + "]");
		}
		this.clearCacheForItem(stockLevel);
		this.createStockLevelHistoryEntry(stockLevel, StockLevelUpdateType.CUSTOMER_RELEASE, released, null);
	}


}
