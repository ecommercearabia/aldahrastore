package com.aldahra.core.service.impl;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;

import javax.annotation.Resource;

import com.aldahra.core.service.ProductPromotionPercentageDiscount;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class DefaultProductPromotionPercentageDiscount implements ProductPromotionPercentageDiscount
{

	@Resource(name = "productDiscountPopulator")
	private Populator<ProductModel, ProductData> productDiscountPopulator;


	/**
	 * @return the productDiscountPopulator
	 */
	protected Populator<ProductModel, ProductData> getProductDiscountPopulator()
	{
		return productDiscountPopulator;
	}


	@Override
	public double getPromotionPercentageDiscount(final ProductModel productModel)
	{
		if (productModel == null)
		{
			return 0d;
		}

		final ProductData productData = new ProductData();
		getProductDiscountPopulator().populate(productModel, productData);

		return (productData.getDiscount() != null && productData.getDiscount().getPercentage() != null)
				? productData.getDiscount().getPercentage()
				: 0d;

	}
}
