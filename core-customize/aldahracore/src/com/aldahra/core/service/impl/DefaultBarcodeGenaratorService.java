package com.aldahra.core.service.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.model.media.MediaFolderModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.site.BaseSiteService;

import java.awt.Font;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import com.aldahra.core.service.BarcodeGenaratorService;
import com.onbarcode.barcode.Code128;


@Service
public class DefaultBarcodeGenaratorService implements BarcodeGenaratorService
{
	private static final Logger LOG = Logger.getLogger(DefaultBarcodeGenaratorService.class);

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "mediaService")
	private MediaService mediaService;

	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	@Resource(name = "catalogVersionService")
	private CatalogVersionService catalogVersionService;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "orderConverter")
	private Converter<OrderModel, OrderData> orderConverter;

	private final String AIR_LINE = "SAUDI AIRLINES CATERING CO\n" + "شركة الخطوط السعودية للتموين";
	private final String VATNO = "(VAT NO. %s)\n" + "(رقم السجل الضريبي %s)";
	private final String INVOICE_ISSUE_DATE = "Invoice Issue Data\n" + "تاريخ اصدار الفاتورة";
	private final String TOTAL_AMOUNT = "Total Amount(SAR)\n" + "المبلغ الاجمالي (ريال سعودي)";
	private final String VAT_AMMOUNT = "VAT Amount(SAR)\n" + "قيمة الضرية المضافة (ريال سعودي)";

	@Override
	public ByteArrayOutputStream genarateCode128Barcode(final String value) throws IOException
	{
		if (value == null || value.isEmpty())
		{
			throw new IllegalArgumentException("The value can not be null");
		}

		final Code128 barcode = new Code128();

		// Set barcode data text to encode
		barcode.setData(value);
		barcode.setLeftMargin(7f);
		barcode.setRightMargin(7f);
		barcode.setBottomMargin(7f);
		barcode.setTopMargin(7);
		barcode.setBarcodeWidth(900f);
		barcode.setBarcodeHeight(270f);
		barcode.setTextFont(new Font("Arial", 0, 60));
		barcode.setAutoResize(true);

		final ByteArrayOutputStream out = new ByteArrayOutputStream();

		try
		{
			barcode.drawBarcode(out);
		}
		catch (final Exception e)
		{
			throw new IOException(e.getMessage());
		}

		return out;
	}

	@Override
	public MediaModel generateBarcodeAsMedia(final ConsignmentModel consignment) throws IOException
	{
		if (consignment == null)
		{
			throw new IllegalArgumentException("The consignment can not be null");
		}
		ByteArrayOutputStream byteArray = null;
		try
		{
			byteArray = genarateCode128Barcode(consignment.getTrackingID());
		}
		catch (final IllegalArgumentException e)
		{
			byteArray = genarateCode128Barcode(" ");
		}


		final MediaModel media = modelService.create(MediaModel.class);
		final String mediaName = consignment.getTrackingID() + "-" + LocalDateTime.now().toString();
		media.setCode(mediaName);
		media.setMime("image/x-png");
		media.setRealFileName(mediaName);
		final AbstractOrderModel order = consignment.getOrder();
		modelService.refresh(order);

		try
		{
			media.setCatalogVersion(catalogVersionService.getCatalogVersion("Default", "Online"));
			modelService.save(media);
			modelService.refresh(media);
			modelService.refresh(consignment);
			consignment.setBarcode(media);
			modelService.save(consignment);
		}
		catch (final Exception e)
		{
			LOG.error("Could not set CatalogVersionModel on MediaModel", e);
			return null;
		}

		final MediaFolderModel mediaFolderModel = getDocumentMediaFolder();

		try (InputStream dataStream = new ByteArrayInputStream(byteArray.toByteArray());)
		{
			mediaService.setStreamForMedia(media, dataStream, mediaName, "image/x-png", mediaFolderModel);
		}
		catch (final Exception e)
		{
			LOG.error("could not generate barcode from byteArray", e);
		}

		return media;
	}


	/**
	 * Gets the {@link MediaFolderModel} to save the generated Media
	 *
	 * @return the {@link MediaFolderModel}
	 */
	protected MediaFolderModel getDocumentMediaFolder()
	{
		return mediaService.getFolder("documents");
	}



}
