/**
 *
 */
package com.aldahra.core.service.impl;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import com.aldahra.core.service.WarehousingAndOMSAuthorityService;
import com.google.common.collect.Lists;
import com.hybris.backoffice.model.user.BackofficeRoleModel;

/**
 * @author monzer
 *
 */
public class DefaultWarehousingAndOMSAuthorityService implements WarehousingAndOMSAuthorityService
{

	@Resource(name = "userService")
	private UserService userService;

	@Override
	public List<BackofficeRoleModel> getAuthorityGroupForCurrentUser()
	{
		return getAuthorityGroupForUser(userService.getCurrentUser());
	}

	@Override
	public List<BackofficeRoleModel> getAuthorityGroupForUser(final UserModel user)
	{
		if (user == null)
		{
			return Collections.EMPTY_LIST;
		}
		return getAllAuthorityGroupsForUser(user.getUid());
	}

	public List<BackofficeRoleModel> getAllAuthorityGroupsForUser(final String userId)
	{
		final UserModel userModel = userService.getUserForUID(userId);
		final Set<BackofficeRoleModel> allUserGroupsForUser = userService.getAllUserGroupsForUser(userModel,
				BackofficeRoleModel.class);

		return Lists.newArrayList(allUserGroupsForUser);
	}

}
