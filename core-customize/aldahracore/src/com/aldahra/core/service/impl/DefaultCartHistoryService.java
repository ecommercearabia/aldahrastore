/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.core.service.impl;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.aldahra.core.model.CartHistoryModel;
import com.aldahra.core.service.CartHistoryService;
import com.google.common.base.Preconditions;


/**
 *
 */
public class DefaultCartHistoryService implements CartHistoryService
{

	@Resource(name = "modelService")
	private ModelService modelService;

	@Override
	public void updateCartHistory(final AbstractOrderModel abstractOrder, final SalesApplication sourceApplication,
			final String operation)
	{
		Preconditions.checkArgument(abstractOrder != null, "Abstract Order can not be null");
		Preconditions.checkArgument(sourceApplication != null, "Source Application can not be null");
		Preconditions.checkArgument(StringUtils.isNotBlank(operation), "Operation Name can not be null");

		abstractOrder.setLastOperationSource(sourceApplication);
		final CartHistoryModel cartHistory = modelService.create(CartHistoryModel.class);
		cartHistory.setOperationName(operation);
		cartHistory.setOperationSource(sourceApplication);
		final List<CartHistoryModel> history = new ArrayList<CartHistoryModel>();
		if (abstractOrder.getOperationHistory() != null)
		{
			history.addAll(abstractOrder.getOperationHistory());
		}
		history.add(cartHistory);
		abstractOrder.setOperationHistory(history);
		modelService.save(abstractOrder);
	}

}