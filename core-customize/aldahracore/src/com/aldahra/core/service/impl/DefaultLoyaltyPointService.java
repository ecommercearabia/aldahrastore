package com.aldahra.core.service.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.aldahra.aldahrastorecredit.service.StoreCreditService;
import com.aldahra.core.enums.LoyaltyPointActionType;
import com.aldahra.core.exception.LoyaltyPointException;
import com.aldahra.core.exception.enums.LoyaltyPointExceptionType;
import com.aldahra.core.model.LoyaltyPointHistoryModel;
import com.aldahra.core.service.LoyaltyPointService;
import com.google.common.base.Preconditions;


/**
 * @author mnasro
 *
 */
public class DefaultLoyaltyPointService implements LoyaltyPointService
{
	private static final String BASESTORE_MUSTN_T_BE_NULL = "baseStoreModel mustn't be null or empty";
	private static final String CUSTOMER_MUSTN_T_BE_NULL = "customer Model mustn't be null or empty";
	private static final String CONSIGNMENT_MUSTN_T_BE_NULL = "consignmentModel mustn't be null or empty";
	private static final String RETURNREQUEST_MUSTN_T_BE_NULL = "Return Request mustn't be null or empty";
	private static final String ORDER_MUSTN_T_BE_NULL = "Order mustn't be null or empty";
	private static final String LOYALTY_POINTS_DISABLED = "Loyalty Points is Disabled";

	private static final String CONSIGNMENT_ORDER_MUSTN_T_BE_NULL = "order in the consignmentModel mustn't be null or empty";

	private static final String CONSIGNMENT_ORDER_STORE_MUSTN_T_BE_NULL = "store in the order in the consignmentModel mustn't be null or empty";

	private static final String CONSIGNMENT_ORDER_USER_MUSTN_T_BE_NULL = "User in the order in the consignmentModel mustn't be null or empty";


	private static final Logger LOG = LoggerFactory.getLogger(DefaultLoyaltyPointService.class);


	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "modelService")
	private ModelService modelService;


	@Resource(name = "storeCreditService")
	private StoreCreditService storeCreditService;

	@Override
	public List<LoyaltyPointHistoryModel> getLoyaltyPointHistory(final CustomerModel customerModel)
	{
		Preconditions.checkArgument(customerModel != null, CUSTOMER_MUSTN_T_BE_NULL);

		final List<LoyaltyPointHistoryModel> loyaltyPointHistory = customerModel.getLoyaltyPointHistory();

		if (CollectionUtils.isEmpty(loyaltyPointHistory))
		{
			return Collections.emptyList();
		}

		return loyaltyPointHistory;
	}

	@Override
	public List<LoyaltyPointHistoryModel> getLoyaltyPointHistoryByCurrentCustomer()
	{
		final UserModel currentUser = getUserService().getCurrentUser();
		if (currentUser instanceof CustomerModel)
		{
			return getLoyaltyPointHistory((CustomerModel) currentUser);
		}
		return Collections.emptyList();
	}

	@Override
	public double getLoyaltyPoints(final CustomerModel customerModel)
	{
		Preconditions.checkArgument(customerModel != null, CUSTOMER_MUSTN_T_BE_NULL);
		return customerModel.getLoyaltyPoints();
	}

	@Override
	public double getLoyaltyPointsByCurrentCustomer()
	{
		final UserModel currentUser = getUserService().getCurrentUser();
		if (currentUser instanceof CustomerModel)
		{
			return getLoyaltyPoints((CustomerModel) currentUser);
		}
		return 0;
	}

	@Override
	public boolean isLoyaltyPointsEnabled(final BaseStoreModel baseStoreModel)
	{
		Preconditions.checkArgument(baseStoreModel != null, BASESTORE_MUSTN_T_BE_NULL);
		if (baseStoreModel.isEnableLoyaltyPoint())
		{
			LOG.info("Loyalty Points is Enabled");
			return true;
		}
		LOG.info(LOYALTY_POINTS_DISABLED);
		return false;
	}

	@Override
	public boolean isLoyaltyPointsEnabledByCurrentStore()
	{
		return isLoyaltyPointsEnabled(getBaseStoreService().getCurrentBaseStore());
	}

	@Override
	public void addLoyaltyPoints(final ConsignmentModel consignmentModel) throws LoyaltyPointException
	{
		Preconditions.checkArgument(consignmentModel != null, CONSIGNMENT_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(consignmentModel.getOrder() != null, CONSIGNMENT_ORDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(consignmentModel.getOrder().getStore() != null, CONSIGNMENT_ORDER_STORE_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(consignmentModel.getOrder().getUser() != null, CONSIGNMENT_ORDER_USER_MUSTN_T_BE_NULL);
		getModelService().refresh(consignmentModel);

		if (!consignmentModel.getOrder().getStore().isEnableLoyaltyPoint())
		{
			LOG.warn(LOYALTY_POINTS_DISABLED);
			throw new LoyaltyPointException(LoyaltyPointExceptionType.NOT_AVAILABLE.toString(),
					LoyaltyPointExceptionType.NOT_AVAILABLE);
		}
		final double productToLoyaltyPointsConversion = consignmentModel.getOrder().getStore()
				.getProductTOLoyaltyPointsConversion();

		double totalPrice = 0.0d;
		for (final ConsignmentEntryModel entry : consignmentModel.getConsignmentEntries())
		{
			if (entry.getOrderEntry() == null || entry.getOrderEntry().getProduct() == null
					|| !entry.getOrderEntry().getProduct().isLoyaltyPoint())
			{
				LOG.debug("Consigment {} is empty", entry.getPk());
				continue;
			}
			totalPrice += entry.getOrderEntry().getTotalPrice() == null ? 0.0d : entry.getOrderEntry().getTotalPrice().doubleValue();
		}

		final double totalLoyaltyPoints = totalPrice * productToLoyaltyPointsConversion;

		final CustomerModel customer = (CustomerModel) consignmentModel.getOrder().getUser();
		final double loyaltyPoints = customer.getLoyaltyPoints();

		customer.setLoyaltyPoints(totalLoyaltyPoints);
		getModelService().save(consignmentModel);
		getModelService().refresh(consignmentModel);


		customer.setLoyaltyPoints(customRoundValue(loyaltyPoints + totalLoyaltyPoints));
		getModelService().save(customer);
		getModelService().refresh(customer);

		LOG.info("{} Loyalty Points added successfully to customer {}", totalLoyaltyPoints, customer.getPk());

		addLoyaltyPointHistoryModel(customer, consignmentModel.getOrder().getStore(), consignmentModel.getOrder().getCode(),
				LoyaltyPointActionType.ADD, totalPrice, totalLoyaltyPoints, consignmentModel.getCode());

	}



	@Override
	public void redeemLoyaltyPoints(final BaseStoreModel baseStoreModel, final CustomerModel customerModel,
			final double redeemLoyaltyPoint) throws LoyaltyPointException
	{
		Preconditions.checkArgument(baseStoreModel != null, BASESTORE_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(customerModel != null, CUSTOMER_MUSTN_T_BE_NULL);

		if (!baseStoreModel.isEnableLoyaltyPoint())
		{
			LOG.warn(LOYALTY_POINTS_DISABLED);
			throw new LoyaltyPointException(LoyaltyPointExceptionType.NOT_AVAILABLE.toString(),
					LoyaltyPointExceptionType.NOT_AVAILABLE);
		}

		getModelService().refresh(customerModel);
		final double loyaltyPoints = customerModel.getLoyaltyPoints();

		final double loyaltyPointToStoreCreditConversion = baseStoreModel.getLoyaltyPointToStoreCreditConversion();

		final double convertLimitLoyaltyPoints = baseStoreModel.getConvertLimitLoyaltyPoints();

		if (loyaltyPointToStoreCreditConversion < 0)
		{
			LOG.warn("Loyalty Point To Store Credit Conversion can not be {}", loyaltyPointToStoreCreditConversion);
			throw new LoyaltyPointException(LoyaltyPointExceptionType.CONFIG_ERROR.toString(),
					LoyaltyPointExceptionType.CONFIG_ERROR);
		}
		if (loyaltyPoints < redeemLoyaltyPoint)
		{
			LOG.warn("Loyalty Point can not be is less than redeem");
			throw new LoyaltyPointException(LoyaltyPointExceptionType.BALANCE_NOT_AVAILABLE.toString(),
					LoyaltyPointExceptionType.BALANCE_NOT_AVAILABLE);
		}

		if (redeemLoyaltyPoint < convertLimitLoyaltyPoints)
		{
			LOG.warn("Redeem Loyalty Point can not be less than Convert Limit Loyalty Points");
			throw new LoyaltyPointException(LoyaltyPointExceptionType.MIN_LIMIT_CONVERT.toString(),
					LoyaltyPointExceptionType.MIN_LIMIT_CONVERT);
		}

		final BigDecimal amount = BigDecimal.valueOf(redeemLoyaltyPoint / loyaltyPointToStoreCreditConversion);
		getStoreCreditService().addStoreCredit(customerModel, baseStoreModel, amount);

		getModelService().refresh(customerModel);
		customerModel.setLoyaltyPoints(loyaltyPoints - redeemLoyaltyPoint);

		getModelService().save(customerModel);
		getModelService().refresh(customerModel);

		LOG.info("{} Loyalty Points redeem successfully for customer {}", loyaltyPoints - redeemLoyaltyPoint,
				customerModel.getPk());
		addLoyaltyPointHistoryModel(customerModel, baseStoreModel, null, LoyaltyPointActionType.REDEEM,
				customRoundValue(amount.doubleValue()), redeemLoyaltyPoint, customerModel.getUid());

	}

	/**
	 *
	 * @param amount
	 *
	 * @return
	 *
	 */

	private double customRoundValue(final double amount)

	{
		if (Math.round(amount) > amount)

		{
			return Math.round(amount);
		}

		if (Math.round(amount) < amount)
		{
			return Math.round(amount) + 0.5;
		}
		if (Math.round(amount) == amount)
		{
			return amount;
		}
		return amount;
	}

	@Override
	public void redeemLoyaltyPoints(final BaseStoreModel baseStoreModel, final CustomerModel customerModel)
			throws LoyaltyPointException
	{
		Preconditions.checkArgument(baseStoreModel != null, BASESTORE_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(customerModel != null, CUSTOMER_MUSTN_T_BE_NULL);

		getModelService().refresh(customerModel);
		final double loyaltyPoints = customerModel.getLoyaltyPoints();

		if (loyaltyPoints <= 0)
		{
			LOG.warn("Redeem Loyalty Point can not be less than Convert Limit Loyalty Points");
			return;
		}

		redeemLoyaltyPoints(baseStoreModel, customerModel, loyaltyPoints);
	}

	@Override
	public void returnLoyaltyPoints(final ReturnRequestModel returnModel) throws LoyaltyPointException
	{
		Preconditions.checkArgument(returnModel != null, RETURNREQUEST_MUSTN_T_BE_NULL);

		final OrderModel order = returnModel.getOrder();
		Preconditions.checkArgument(order != null, ORDER_MUSTN_T_BE_NULL);

		final BaseStoreModel baseStoreModel = order.getStore();
		Preconditions.checkArgument(baseStoreModel != null, BASESTORE_MUSTN_T_BE_NULL);

		final CustomerModel customer = (CustomerModel) order.getUser();
		Preconditions.checkArgument(customer != null, CUSTOMER_MUSTN_T_BE_NULL);

		if (!baseStoreModel.isEnableLoyaltyPoint())
		{
			LOG.warn("Loyalty Points is Disabeld in base store");
			throw new LoyaltyPointException(LoyaltyPointExceptionType.NOT_AVAILABLE.toString(),
					LoyaltyPointExceptionType.NOT_AVAILABLE);
		}


		final double subtotal = returnModel.getSubtotal() == null ? 0.0d : returnModel.getSubtotal().doubleValue();


		final double productToLoyaltyPointsConversion = baseStoreModel.getProductTOLoyaltyPointsConversion();
		final double loyaltyPointsRemoved = subtotal * productToLoyaltyPointsConversion;

		getModelService().refresh(customer);

		double totalLoyaltyPoints = 0.0d;
		if (customer.getLoyaltyPoints() >= loyaltyPointsRemoved)
		{
			LOG.info("The return loyalty points removed successfully");
			totalLoyaltyPoints = customer.getLoyaltyPoints() - loyaltyPointsRemoved;
		}
		else
		{
			LOG.warn("Customer Loyalty Points is less than or equal the Removed Loyality Points");
		}
		customer.setLoyaltyPoints(customRoundValue(totalLoyaltyPoints));
		getModelService().save(customer);
		getModelService().refresh(customer);

		LOG.info("{} Loyalty Points return successfully from customer {}", loyaltyPointsRemoved, customer.getPk());

		returnModel.setLoyaltyPointsRemoved(loyaltyPointsRemoved);
		getModelService().save(returnModel);
		getModelService().refresh(returnModel);

		addLoyaltyPointHistoryModel(customer, baseStoreModel, order.getCode(), LoyaltyPointActionType.RETURN, subtotal,
				totalLoyaltyPoints, returnModel.getCode());
	}



	/**
	 * @param order
	 * @param refrenceCode
	 * @param amount
	 * @param remove
	 */
	private void addLoyaltyPointHistoryModel(final CustomerModel customer, final BaseStoreModel baseStoreModel,
			final String orderCode, final LoyaltyPointActionType type, final double amount, final double points,
			final String refrenceCode)
	{


		final LoyaltyPointHistoryModel loyaltyPointHistoryModel = (LoyaltyPointHistoryModel) getModelService()
				.create(LoyaltyPointHistoryModel.class);

		loyaltyPointHistoryModel.setActionType(type);
		loyaltyPointHistoryModel.setDateOfPurchase(new Date());
		loyaltyPointHistoryModel.setProductTOLoyaltyPointsConversion(baseStoreModel.getProductTOLoyaltyPointsConversion());
		loyaltyPointHistoryModel.setBalancePoints(customer.getLoyaltyPoints());
		loyaltyPointHistoryModel.setPoints(customRoundValue(points));
		loyaltyPointHistoryModel.setOrderCode(orderCode);
		loyaltyPointHistoryModel.setCustomer(customer);
		loyaltyPointHistoryModel.setConvertLimitLoyaltyPoints(baseStoreModel.getConvertLimitLoyaltyPoints());
		loyaltyPointHistoryModel.setLoyaltyPointToStoreCreditConversion(baseStoreModel.getLoyaltyPointToStoreCreditConversion());
		loyaltyPointHistoryModel.setAmount(BigDecimal.valueOf(amount));
		loyaltyPointHistoryModel.setReferenceCode(refrenceCode);
		getModelService().save(loyaltyPointHistoryModel);

		LOG.info("Added new Loyalty Point History successfully for customer {}", customer.getPk());
	}

	/**
	 * @return the userService
	 */
	protected UserService getUserService()
	{
		return userService;
	}

	/**
	 * @return the baseStoreService
	 */
	protected BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	/**
	 * @return the storeCreditService
	 */
	protected StoreCreditService getStoreCreditService()
	{
		return storeCreditService;
	}

	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}


}
