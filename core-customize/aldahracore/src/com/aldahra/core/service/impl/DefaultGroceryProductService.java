/**
 *
 */
package com.aldahra.core.service.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aldahra.aldahraproduct.model.GroceryVariantProductModel;
import com.aldahra.core.dao.GroceryProductDao;
import com.aldahra.core.service.GroceryProductService;

/**
 * @author monzer
 *
 */
public class DefaultGroceryProductService implements GroceryProductService
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultGroceryProductService.class);

	@Resource(name = "groceryProductDao")
	private GroceryProductDao groceryProductDao;

	@Override
	public List<GroceryVariantProductModel> getAllByCatalogVersion(final CatalogVersionModel catalogVersion)
	{
		if (catalogVersion == null)
		{
			LOG.error("Cannot get products with empty catalog version");
			return Collections.emptyList();
		}
		return groceryProductDao.getAllGroceryProductByCatalogVersion(catalogVersion);
	}

}
