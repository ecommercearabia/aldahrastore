/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.core.service.impl;

import de.hybris.platform.basecommerce.enums.InStockStatus;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.aldahra.core.beans.AllStockLevelAttributes;
import com.aldahra.core.beans.StockLevelQuantityAttributes;
import com.aldahra.core.service.CustomStockService;
import com.aldahra.core.service.UpdateProductStockLevelService;


/**
 * @author monzer
 */
public class DefaultUpdateProductStockLevelService implements UpdateProductStockLevelService
{
	private static final String WAREHOUSE_MODEL_IS_NULL = "warehouse Model is null";
	private static final String STOCK_LEVEL_WITH_0_QUANTITY_WILL_NOT_BE_CREATED = "Stock level with 0 quantity will not be created";
	private static final String QAUNTITY_IS_NULL = "qauntity is null";
	private static final String UPDATE_SROCK_FIELDS_CODE_IS_NULL = "updateSrockFields code is null";
	private static final String PRODUCT_CODE_IS_NULL = "Product code is null";

	private static final Logger LOG = LoggerFactory.getLogger(DefaultUpdateProductStockLevelService.class);

	@Resource(name = "catalogVersionService")
	private CatalogVersionService catalogVersionService;

	@Resource(name = "stockService")
	private CustomStockService stockService;

	@Resource(name = "productService")
	private ProductService productService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Override
	public void updateStockLevelForProduct(final String productCode, final CatalogVersionModel productCatalogVersion,
			final WarehouseModel warehouse, final Integer quantity, final StockLevelQuantityAttributes updateStockFields)
	{
		if (StringUtils.isBlank(productCode))
		{
			LOG.error(PRODUCT_CODE_IS_NULL);
			return;
		}
		if (updateStockFields == null)
		{
			LOG.error(UPDATE_SROCK_FIELDS_CODE_IS_NULL);
			return;
		}
		if (quantity == null)
		{
			LOG.error(QAUNTITY_IS_NULL);
			return;
		}
		if (quantity.intValue() <= 0)
		{
			LOG.error(STOCK_LEVEL_WITH_0_QUANTITY_WILL_NOT_BE_CREATED);
			return;
		}
		if (warehouse == null)
		{
			LOG.error(WAREHOUSE_MODEL_IS_NULL);
			return;
		}
		if (productCatalogVersion == null)
		{
			LOG.error("catalogVersion Model is null");
			return;
		}

		final Optional<ProductModel> product = getProductForCodeAndCataogVersion(productCode, productCatalogVersion);
		if (product.isEmpty())
		{
			LOG.error("Product with code {} is not found", productCode);
			return;
		}

		updateStockLevelForProduct(product.get(), quantity, warehouse, updateStockFields);
	}

	@Override
	public void updateStockLevelForProduct(final ProductModel product, final Integer quantity, final WarehouseModel warehouse,
			final StockLevelQuantityAttributes updateStockFields)
	{
		if (product == null)
		{
			LOG.error(PRODUCT_CODE_IS_NULL);
			return;
		}
		if (updateStockFields == null)
		{
			LOG.error(UPDATE_SROCK_FIELDS_CODE_IS_NULL);
			return;
		}
		if (quantity == null)
		{
			LOG.error(QAUNTITY_IS_NULL);
			return;
		}
		if (quantity.intValue() <= 0)
		{
			LOG.error(STOCK_LEVEL_WITH_0_QUANTITY_WILL_NOT_BE_CREATED);
			return;
		}
		if (warehouse == null)
		{
			LOG.error(WAREHOUSE_MODEL_IS_NULL);
			return;
		}

		updateProductStock(product, warehouse, quantity, updateStockFields);
	}

	private void updateProductStock(final ProductModel product, final WarehouseModel warehouse, final Integer quantity,
			final StockLevelQuantityAttributes updateStockFields)
	{
		if (quantity == 0)
		{
			LOG.info("Quantity for the product {} is 0, this might be because the product does not exist in the Third party system",
					product.getCode());
			return;
		}

		if (updateStockFields instanceof AllStockLevelAttributes)
		{
			updateStockWithAllFields(product, warehouse, quantity, (AllStockLevelAttributes) updateStockFields);
		}
		else
		{
			updateStockQuantity(product, warehouse, quantity, updateStockFields);
		}

	}

	/**
	 * Create/Update the stock level with all attributes (binNumber, lotNumber, expirationDate)
	 *
	 * @param product
	 * @param quantity
	 * @param updateStockFields
	 * @return
	 */
	private void updateStockWithAllFields(final ProductModel product, final WarehouseModel warehouse, final Integer quantity,
			final AllStockLevelAttributes updateStockFields)
	{
		final List<StockLevelModel> stockLevels = stockService.getByLotNumberAndWarehouse(product.getCode(),
				updateStockFields.getLotNumber(), warehouse);

		if (CollectionUtils.isEmpty(stockLevels) && updateStockFields.isAutoCreate())
		{
			createStockLevel(product, quantity, updateStockFields, warehouse);
		}

		for (final StockLevelModel stockLevel : stockLevels)
		{
			updateStockLevel(product, stockLevel, quantity, updateStockFields);
			updateInventoryEvents(stockLevel, updateStockFields.isRemoveInventoryEvents());
		}

	}

	/**
	 * @param product
	 * @param stockLevel
	 * @param quantity
	 * @param updateStockFields
	 */
	private void updateStockLevel(final ProductModel product, final StockLevelModel stockLevel, final Integer quantity,
			final AllStockLevelAttributes updateStockFields)
	{
		if (updateStockFields.isResetStockReserved())
		{
			stockLevel.setReserved(0);
		}
		saveUpdateStockResponse(stockLevel, updateStockFields.getResponse(), updateStockFields.isEnableSaveStockResponse());
		stockLevel.setAvailable(quantity);
		stockLevel.setInStockStatus(InStockStatus.NOTSPECIFIED);
		stockLevel.setBin(updateStockFields.getBinNumber());
		stockLevel.setBinNumber(updateStockFields.getBinNumber());
		stockLevel.setLotNumber(updateStockFields.getLotNumber());
		stockLevel.setExpiryDate(updateStockFields.getExpirationDate());
		modelService.save(stockLevel);
		LOG.info("STOCK UPDATED FOR PRODUCT CODE: {} with quantity of {}", product.getCode(), quantity);
	}

	/**
	 * Create/Update the stock level with the quantity
	 *
	 * @param product
	 * @param quantity
	 * @param updateStockFields
	 * @return
	 */
	private Optional<StockLevelModel> updateStockQuantity(final ProductModel product, final WarehouseModel warehouse,
			final Integer quantity, final StockLevelQuantityAttributes updateStockFields)
	{
		final StockLevelModel stockLevel = stockService.getStockLevel(product, warehouse);

		if (stockLevel == null && updateStockFields.isAutoCreate())
		{
			return Optional.ofNullable(createStockLevel(product, quantity, warehouse, updateStockFields.getResponse(),
					updateStockFields.isEnableSaveStockResponse()));
		}
		else if (stockLevel != null)
		{
			updateStockLevel(product, stockLevel, quantity, updateStockFields.getResponse(),
					updateStockFields.isEnableSaveStockResponse(), updateStockFields.isResetStockReserved());
			return Optional.ofNullable(updateInventoryEvents(stockLevel, updateStockFields.isRemoveInventoryEvents()));
		}
		else if (!updateStockFields.isAutoCreate())
		{
			return Optional.ofNullable(saveUpdateStockResponse(stockLevel, updateStockFields.getResponse(),
					updateStockFields.isEnableSaveStockResponse()));
		}
		else
		{
			LOG.error("No actions have been taken because Empty Stock : {} and autoCreate is : {}", stockLevel == null,
					updateStockFields.isAutoCreate());
			return Optional.empty();
		}
	}

	private void updateStockLevel(final ProductModel product, final StockLevelModel stockLevel, final Integer quantity)
	{
		stockLevel.setAvailable(quantity);
		stockLevel.setInStockStatus(InStockStatus.NOTSPECIFIED);
		modelService.save(stockLevel);
		LOG.info("STOCK UPDATED FOR PRODUCT CODE: [{}] with quantity of {}", product.getCode(), quantity);
	}

	private StockLevelModel updateStockLevel(final ProductModel product, final StockLevelModel stockLevel, final Integer quantity,
			final String response, final boolean saveStockResponse, final boolean resetStockReversed)
	{
		if (resetStockReversed)
		{
			stockLevel.setReserved(0);
		}
		saveUpdateStockResponse(stockLevel, response, saveStockResponse);
		stockLevel.setAvailable(quantity);
		stockLevel.setInStockStatus(InStockStatus.NOTSPECIFIED);
		modelService.save(stockLevel);
		LOG.info("STOCK UPDATED FOR PRODUCT CODE: [{}] with quantity of {}", product.getCode(), quantity);
		return stockLevel;
	}

	protected void updateInventoryEvents(final StockLevelModel stockLevel)
	{
		stockLevel.setInventoryEvents(null);
		modelService.save(stockLevel);
	}

	protected StockLevelModel updateInventoryEvents(final StockLevelModel stockLevel, final boolean removeInventoryEvents)
	{
		if (removeInventoryEvents)
		{
			LOG.info("Delete inventory events where product code = {}", stockLevel.getProductCode());
			stockLevel.setInventoryEvents(null);
			modelService.save(stockLevel);
		}
		return stockLevel;
	}

	private StockLevelModel createStockLevel(final ProductModel product, final Integer quantity, final WarehouseModel warehouse,
			final String response, final boolean saveResponse)
	{
		final StockLevelModel newStock = modelService.create(StockLevelModel.class);
		newStock.setWarehouse(warehouse);
		newStock.setAvailable(quantity);
		newStock.setProduct(product);
		newStock.setReserved(0);
		newStock.setCreateResponse("Stock created with quantity of " + quantity + " for warehouse " + warehouse.getCode());
		newStock.setProductCode(product.getCode());
		newStock.setInStockStatus(InStockStatus.NOTSPECIFIED);
		saveUpdateStockResponse(newStock, response, saveResponse);
		modelService.save(newStock);
		LOG.info("STOCK CREATED FOR PRODUCT CODE: {} with quantity of {}", product.getCode(), quantity);
		return newStock;
	}

	private void createStockLevel(final ProductModel product, final Integer quantity,
			final AllStockLevelAttributes updateStockFields, final WarehouseModel warehouse)
	{
		final StockLevelModel newStock = modelService.create(StockLevelModel.class);
		newStock.setWarehouse(warehouse);
		newStock.setAvailable(quantity);
		newStock.setProduct(product);
		newStock.setReserved(0);
		newStock.setExpiryDate(updateStockFields.getExpirationDate());
		newStock.setBinNumber(updateStockFields.getBinNumber());
		newStock.setBin(updateStockFields.getBinNumber());
		newStock.setLotNumber(updateStockFields.getLotNumber());
		newStock.setCreateResponse("Stock created with quantity of " + quantity + " for warehouse " + warehouse.getCode());
		newStock.setProductCode(product.getCode());
		newStock.setInStockStatus(InStockStatus.NOTSPECIFIED);
		modelService.save(newStock);
		LOG.info("STOCK CREATED FOR PRODUCT CODE: {} with quantity of {}", product.getCode(), quantity);
	}

	private StockLevelModel saveUpdateStockResponse(final StockLevelModel stockLevel, final String message, final boolean enabled)
	{
		final List<String> records = new ArrayList<>();
		if (!enabled)
		{
			return stockLevel;

		}

		if (stockLevel.getUpdateRecords() != null)
		{
			records.addAll(stockLevel.getUpdateRecords());
		}
		records.add(message);
		stockLevel.setUpdateRecords(records);
		modelService.save(stockLevel);

		return stockLevel;
	}

	//-----------------------------------------------------------------------------------------------------
	@Override
	public void updateStockLevelForProductWithExtraData(final String productCode, final CatalogVersionModel productCatalogVersion,
			final WarehouseModel warehouse, final Integer quantity, final AllStockLevelAttributes updateStockFields)
	{
		if (StringUtils.isBlank(productCode))
		{
			LOG.error(PRODUCT_CODE_IS_NULL);
			return;
		}

		final Optional<ProductModel> product = getProductForCodeAndCataogVersion(productCode, productCatalogVersion);
		if (product.isEmpty())
		{
			LOG.error("Product with code {} is not found", productCode);
			return;
		}
		updateStockLevelForProductWithExtraData(product.get(), quantity, warehouse, updateStockFields);
	}

	@Override
	public void updateStockLevelForProductWithExtraData(final ProductModel product, final Integer quantity,
			final WarehouseModel warehouse, final AllStockLevelAttributes updateStockFields)
	{
		if (!validateUpdateStockParams(product, quantity, updateStockFields, warehouse))
		{
			return;
		}

		updateProductStock(product, warehouse, quantity, updateStockFields);
	}

	/**
	 * @param product
	 * @param quantity
	 * @param updateStockFields
	 * @param warehouse
	 */
	private boolean validateUpdateStockParams(final ProductModel product, final Integer quantity,
			final AllStockLevelAttributes updateStockFields, final WarehouseModel warehouse)
	{
		if (product == null)
		{
			LOG.error(PRODUCT_CODE_IS_NULL);
			return false;
		}
		if (updateStockFields == null)
		{
			LOG.error(UPDATE_SROCK_FIELDS_CODE_IS_NULL);
			return false;
		}
		if (quantity == null)
		{
			LOG.error(QAUNTITY_IS_NULL);
			return false;
		}
		if (quantity.intValue() <= 0)
		{
			LOG.error(STOCK_LEVEL_WITH_0_QUANTITY_WILL_NOT_BE_CREATED);
			return false;
		}
		if (warehouse == null)
		{
			LOG.error(WAREHOUSE_MODEL_IS_NULL);
			return false;
		}
		if (StringUtils.isBlank(updateStockFields.getBinNumber()))
		{
			LOG.error("Bin Number Model is null");
			return false;
		}
		if (StringUtils.isBlank(updateStockFields.getLotNumber()))
		{
			LOG.error("Lot Number Model is null");
			return false;
		}
		//		if (updateStockFields.getExpirationDate() == null)
		//		{
		//			LOG.error("Expiration Date Model is null");
		//			return false;
		//		}F
		return true;

	}

	private Optional<ProductModel> getProductForCodeAndCataogVersion(final String productCode,
			final CatalogVersionModel productCatalogVersion)
	{
		ProductModel product = null;
		try
		{
			product = productService.getProductForCode(productCatalogVersion, productCode);
		}
		catch (final UnknownIdentifierException | AmbiguousIdentifierException e)
		{
			LOG.error("Product with code {} maybe not found", productCode);
			return Optional.empty();
		}

		return Optional.ofNullable(product);
	}

}
