package com.aldahra.core.service.impl;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.warehousing.taskassignment.services.WarehousingConsignmentWorkflowService;

import com.aldahra.core.service.WarehousingDeliveryService;


/**
 * The Class DefaultWarehousingDeliveryService.
 *
 * @author amjad.shati@erabia.com
 */
public class DefaultWarehousingDeliveryService implements WarehousingDeliveryService
{

	/** The Constant CONFIRM_CONSIGNMENT_DELIVERY_CHOICE. */
	protected static final String CONFIRM_CONSIGNMENT_DELIVERY_CHOICE = "confirmConsignmentDelivery";

	/** The Constant DELIVERING_TEMPLATE_CODE. */
	protected static final String DELIVERING_TEMPLATE_CODE = "NPR_Delivering";

	/** The warehousing consignment workflow service. */
	private WarehousingConsignmentWorkflowService warehousingConsignmentWorkflowService;

	/**
	 * Confirm consignment delivery.
	 *
	 * @param consignment
	 *           the consignment
	 */
	@Override
	public void confirmConsignmentDelivery(final ConsignmentModel consignment)
	{
		getWarehousingConsignmentWorkflowService().decideWorkflowAction(consignment, DELIVERING_TEMPLATE_CODE,
				CONFIRM_CONSIGNMENT_DELIVERY_CHOICE);
	}

	/**
	 * Gets the warehousing consignment workflow service.
	 *
	 * @return the warehousing consignment workflow service
	 */
	public WarehousingConsignmentWorkflowService getWarehousingConsignmentWorkflowService()
	{
		return warehousingConsignmentWorkflowService;
	}

	/**
	 * Sets the warehousing consignment workflow service.
	 *
	 * @param warehousingConsignmentWorkflowService
	 *           the new warehousing consignment workflow service
	 */
	public void setWarehousingConsignmentWorkflowService(
			final WarehousingConsignmentWorkflowService warehousingConsignmentWorkflowService)
	{
		this.warehousingConsignmentWorkflowService = warehousingConsignmentWorkflowService;
	}

}
