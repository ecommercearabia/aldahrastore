/**
 *
 */
package com.aldahra.core.service.impl;

import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.aldahra.aldahrauser.service.MobilePhoneService;
import com.aldahra.core.service.CustomerMatchingService;


/**
 * @author monzer
 *
 */
public class DefaultCustomerMatchingService implements CustomerMatchingService
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultCustomerMatchingService.class);
	private static final Pattern EMAIL_REGEX = Pattern.compile("^(.+)@(.+)$");

	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	/**
	 * Gets Match the identifier if it is email or mobile number.
	 *
	 * @param identifier
	 *           the identifier
	 * @return the customer UID
	 */
	@Override
	public Optional<String> getCustomerUidAfterMatching(final String identifier)
	{
		if (StringUtils.isBlank(identifier))
		{
			LOG.error("Identifier is empty");
			return Optional.empty();
		}
		return validateUsername(identifier);
	}

	protected Optional<String> validateUsername(final String username)
	{
		final Matcher emailMatcher = EMAIL_REGEX.matcher(username);
		if (!emailMatcher.matches())
		{
			LOG.info("Identifier is a mobile number {}", username);
			return fetchUserEmailByMobileNumber(username);
		}
		return Optional.ofNullable(username);
	}

	/**
	 * @param username
	 * @param request
	 */
	protected Optional<String> fetchUserEmailByMobileNumber(final String username)
	{
		String defaultMobileCountry = null;
		if (cmsSiteService.getCurrentSite() == null || cmsSiteService.getCurrentSite().getDefaultMobileCountry() == null
				|| StringUtils.isBlank(cmsSiteService.getCurrentSite().getDefaultMobileCountry().getIsdcode()))
		{
			LOG.error("Could not get the default mobile country code");
			return Optional.ofNullable(username);
		}
		defaultMobileCountry = cmsSiteService.getCurrentSite().getDefaultMobileCountry().getIsocode();
		final Optional<String> normalizedPhoneNumber = mobilePhoneService
				.validateAndNormalizePhoneNumberByIsoCode(defaultMobileCountry, username);
		if (normalizedPhoneNumber.isEmpty())
		{
			LOG.error("Could not normalized the mobile number {} with country code {}", username, defaultMobileCountry);
			return Optional.ofNullable(username);
		}
		final List<CustomerModel> customersByMobileNumber = mobilePhoneService
				.getCustomersByMobileNumber(normalizedPhoneNumber.get());
		// Since the mobile number is unique, there will be only one customer found by the mobile number
		if (!CollectionUtils.isEmpty(customersByMobileNumber) && customersByMobileNumber.size() == 1)
		{
			final CustomerModel customerModel = customersByMobileNumber.get(0);
			return Optional.ofNullable(customerModel.getUid());
		}
		LOG.error("No users found for mobile number {}", username);
		return Optional.empty();
	}


}
