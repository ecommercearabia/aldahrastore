/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.core.service;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import com.aldahra.core.beans.AllStockLevelAttributes;
import com.aldahra.core.beans.StockLevelQuantityAttributes;


/**
 * @author monzer
 */
public interface UpdateProductStockLevelService
{
	/**
	 * Only updates the available quantity
	 *
	 * @param productCode
	 * @param quantity
	 * @param updateSrockFields
	 */
	void updateStockLevelForProduct(String productCode, CatalogVersionModel productCatalogVersion, final WarehouseModel warehouse,
			Integer quantity, StockLevelQuantityAttributes updateStockFields);

	void updateStockLevelForProduct(final ProductModel product, final Integer quantity, final WarehouseModel warehouse,
			StockLevelQuantityAttributes updateSrockFields);

	/**
	 * Updates the stock quantity, bin number, LOT number and expiration date.
	 *
	 * @param productCode
	 * @param quantity
	 * @param updateSrockFields
	 */
	void updateStockLevelForProductWithExtraData(String productCode, CatalogVersionModel productCatalogVersion,
			final WarehouseModel warehouse, Integer quantity,
			AllStockLevelAttributes updateSrockFields);

	void updateStockLevelForProductWithExtraData(final ProductModel product, final Integer quantity,
			final WarehouseModel warehouse,
			AllStockLevelAttributes updateSrockFields);
}
