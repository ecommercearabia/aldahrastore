/**
 *
 */
package com.aldahra.core.service;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;


/**
 * The Interface WarehousingDeliveryService.
 *
 * @author amjad.shati@erabia.com
 */
public interface WarehousingDeliveryService
{

	/**
	 * Confirm consignment delivery.
	 *
	 * @param consignment
	 *           the consignment
	 */
	void confirmConsignmentDelivery(final ConsignmentModel consignment);
}
