/**
 *
 */
package com.aldahra.core.service;

import de.hybris.platform.catalog.model.CatalogVersionModel;

import java.util.List;

import com.aldahra.aldahraproduct.model.GroceryVariantProductModel;


/**
 * @author monzer
 *
 */
public interface GroceryProductService
{

	List<GroceryVariantProductModel> getAllByCatalogVersion(CatalogVersionModel catalogVersion);

}
