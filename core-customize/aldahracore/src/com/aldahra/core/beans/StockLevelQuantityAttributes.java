/**
 *
 */
package com.aldahra.core.beans;

/**
 * @author monzer
 *
 */
public class StockLevelQuantityAttributes
{

	private String response;
	private boolean removeInventoryEvents;
	private boolean resetStockReserved;
	private boolean autoCreate;
	private boolean enableSaveStockResponse;

	/**
	 * @return the response
	 */
	public String getResponse()
	{
		return response;
	}

	/**
	 * @param response
	 *           the response to set
	 */
	public void setResponse(final String response)
	{
		this.response = response;
	}

	/**
	 * @return the removeInventoryEvents
	 */
	public boolean isRemoveInventoryEvents()
	{
		return removeInventoryEvents;
	}

	/**
	 * @param removeInventoryEvents
	 *           the removeInventoryEvents to set
	 */
	public void setRemoveInventoryEvents(final boolean removeInventoryEvents)
	{
		this.removeInventoryEvents = removeInventoryEvents;
	}

	/**
	 * @return the resetStockReserved
	 */
	public boolean isResetStockReserved()
	{
		return resetStockReserved;
	}

	/**
	 * @param resetStockReserved
	 *           the resetStockReserved to set
	 */
	public void setResetStockReserved(final boolean resetStockReserved)
	{
		this.resetStockReserved = resetStockReserved;
	}

	/**
	 * @return the autoCreate
	 */
	public boolean isAutoCreate()
	{
		return autoCreate;
	}

	/**
	 * @param autoCreate
	 *           the autoCreate to set
	 */
	public void setAutoCreate(final boolean autoCreate)
	{
		this.autoCreate = autoCreate;
	}

	/**
	 * @return the enableSaveStockResponse
	 */
	public boolean isEnableSaveStockResponse()
	{
		return enableSaveStockResponse;
	}

	/**
	 * @param enableSaveStockResponse
	 *           the enableSaveStockResponse to set
	 */
	public void setEnableSaveStockResponse(final boolean enableSaveStockResponse)
	{
		this.enableSaveStockResponse = enableSaveStockResponse;
	}


}
