/**
 *
 */
package com.aldahra.core.beans;

import java.util.Date;


/**
 * @author monzer
 *
 */
public class AllStockLevelAttributes extends StockLevelQuantityAttributes
{

	private String binNumber;
	private String lotNumber;
	private Date expirationDate;
	private String unitOfMeasure;

	/**
	 * @return the binNumber
	 */
	public String getBinNumber()
	{
		return binNumber;
	}

	/**
	 * @param binNumber
	 *           the binNumber to set
	 */
	public void setBinNumber(final String binNumber)
	{
		this.binNumber = binNumber;
	}

	/**
	 * @return the lotNumber
	 */
	public String getLotNumber()
	{
		return lotNumber;
	}

	/**
	 * @param lotNumber
	 *           the lotNumber to set
	 */
	public void setLotNumber(final String lotNumber)
	{
		this.lotNumber = lotNumber;
	}

	/**
	 * @return the expirationDate
	 */
	public Date getExpirationDate()
	{
		return expirationDate;
	}

	/**
	 * @param expirationDate
	 *           the expirationDate to set
	 */
	public void setExpirationDate(final Date expirationDate)
	{
		this.expirationDate = expirationDate;
	}

	@Override
	public String toString()
	{
		return "{\n\tbinNumber:" + binNumber + ",\n\t lotNumber:" + lotNumber + ",\n\t expirationDate:" + expirationDate
				+ ",\n\t unitOfMeasure:" + unitOfMeasure + "\n}";
	}

	/**
	 * @return the unitOfMeasure
	 */
	public String getUnitOfMeasure()
	{
		return unitOfMeasure;
	}

	/**
	 * @param unitOfMeasure
	 *           the unitOfMeasure to set
	 */
	public void setUnitOfMeasure(final String unitOfMeasure)
	{
		this.unitOfMeasure = unitOfMeasure;
	}

}
