/**
 *
 */
package com.aldahra.core.handler;

import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import java.util.Arrays;

import javax.annotation.Resource;

import com.aldahra.aldahrafulfillment.atp.strategy.impl.CustomWarehousingAvailabilityCalculationStrategy;


/**
 * @author monzer
 *
 */
public class StockLevelInStockAttributeHandler implements DynamicAttributeHandler<Boolean, StockLevelModel>
{

	@Resource(name = "commerceStockLevelCalculationStrategy")
	private CustomWarehousingAvailabilityCalculationStrategy warehousingAvailabilityCalculationStrategy;

	@Override
	public Boolean get(final StockLevelModel stock)
	{
		final Long availability = warehousingAvailabilityCalculationStrategy.calculateAvailability(Arrays.asList(stock));

		return availability != null && availability.intValue() > 0 && !stock.isIsExpired();
	}

	@Override
	public void set(final StockLevelModel stock, final Boolean isExpired)
	{
		throw new UnsupportedOperationException();
	}

}
