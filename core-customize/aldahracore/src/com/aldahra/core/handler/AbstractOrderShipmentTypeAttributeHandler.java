package com.aldahra.core.handler;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import org.springframework.util.CollectionUtils;

import com.aldahra.core.enums.ShipmentType;


/**
 * @author monzer
 *
 */
public class AbstractOrderShipmentTypeAttributeHandler implements DynamicAttributeHandler<ShipmentType, AbstractOrderModel>
{

	@Override
	public ShipmentType get(final AbstractOrderModel abstractOrderModel)
	{
		if (abstractOrderModel == null || CollectionUtils.isEmpty(abstractOrderModel.getEntries()))
		{
			return null;
		}

		for (final AbstractOrderEntryModel entry : abstractOrderModel.getEntries())
		{
			if (entry.getDeliveryPointOfService() != null)
			{
				return ShipmentType.PICKUP_IN_STORE;
			}
		}
		return ShipmentType.DELIVERY;
	}

	@Override
	public void set(final AbstractOrderModel abstractOrderModel, final ShipmentType shipmentType)
	{
		throw new UnsupportedOperationException();
	}


}
