/**
 *
 */
package com.aldahra.core.handler;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import org.apache.logging.log4j.util.Strings;

import com.aldahra.aldahratimeslot.model.TimeSlotInfoModel;


/**
 * @author monzer
 *
 */
public class AbstractOrderTimeslotDescriptionAttributeHandler implements DynamicAttributeHandler<String, AbstractOrderModel>
{

	private static final String EMPTY_STRING = Strings.EMPTY;

	@Override
	public String get(final AbstractOrderModel order)
	{
		if (order == null || order.getTimeSlotInfo() == null)
		{
			return EMPTY_STRING;
		}
		final TimeSlotInfoModel timeSlotInfo = order.getTimeSlotInfo();
		return timeSlotInfo.getDay() + ", " + timeSlotInfo.getDate() + " between " + timeSlotInfo.getStart() + " - "
				+ timeSlotInfo.getEnd();
	}

	@Override
	public void set(final AbstractOrderModel order, final String timeslot)
	{
		throw new UnsupportedOperationException();
	}

}
