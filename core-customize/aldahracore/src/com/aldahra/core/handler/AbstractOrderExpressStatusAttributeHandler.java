/**
 *
 */
package com.aldahra.core.handler;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import java.util.Objects;

import org.apache.logging.log4j.util.Strings;

import com.aldahra.core.enums.DeliveryModeType;


/**
 * @author Tuqa
 *
 */
public class AbstractOrderExpressStatusAttributeHandler implements DynamicAttributeHandler<String, AbstractOrderModel>
{


	@Override
	public String get(final AbstractOrderModel order)
	{
		if (order == null || Objects.isNull(order.isExpress()) || order.getDeliveryType() == null)
		{
			return Strings.EMPTY;
		}

		if (DeliveryModeType.NORMAL.getCode().equals(order.getDeliveryType()))
		{
			return "NORMAL";
		}

		return order.isExpress() ? "EXPRESS" : "NORMAL";

	}

	@Override
	public void set(final AbstractOrderModel order, final String Status)
	{
		throw new UnsupportedOperationException();
	}

}
