package com.aldahra.core.handler;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import org.springframework.util.CollectionUtils;


public class AbstractOrderLoyaltyPointsAttributeHandler implements DynamicAttributeHandler<Double, AbstractOrderModel>
{

	@Override
	public Double get(final AbstractOrderModel abstractOrderModel)
	{
		if (abstractOrderModel == null || CollectionUtils.isEmpty(abstractOrderModel.getConsignments()))
		{
			return 0.0d;
		}

		double loyaltyPoints = 0.0d;
		for (final ConsignmentModel cons : abstractOrderModel.getConsignments())
		{
			loyaltyPoints = +cons.getLoyaltyPoints();
		}

		return loyaltyPoints;
	}

	@Override
	public void set(final AbstractOrderModel abstractOrderModel, final Double loyaltyPoints)
	{
		throw new UnsupportedOperationException();
	}


}
