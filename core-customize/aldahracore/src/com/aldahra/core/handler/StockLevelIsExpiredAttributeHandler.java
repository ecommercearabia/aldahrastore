/**
 *
 */
package com.aldahra.core.handler;

import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import java.util.Date;


/**
 * @author monzer
 *
 */
public class StockLevelIsExpiredAttributeHandler implements DynamicAttributeHandler<Boolean, StockLevelModel>
{

	@Override
	public Boolean get(final StockLevelModel stock)
	{
		return stock != null && stock.getExpiryDate() != null && stock.getExpiryDate().before(new Date());
	}

	@Override
	public void set(final StockLevelModel stock, final Boolean isExpired)
	{
		throw new UnsupportedOperationException();
	}

}
