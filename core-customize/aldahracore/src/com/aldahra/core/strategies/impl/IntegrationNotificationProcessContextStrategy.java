package com.aldahra.core.strategies.impl;

import de.hybris.platform.acceleratorservices.process.strategies.impl.AbstractOrderProcessContextStrategy;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;

import java.util.Optional;

import com.aldahra.core.model.GenerateErrorEmailProcessModel;
import com.aldahra.core.model.GenerateNotificationEmailProcessModel;


/**
 * @author mnasro
 */
public class IntegrationNotificationProcessContextStrategy extends AbstractOrderProcessContextStrategy
{
	@Override
	protected Optional<AbstractOrderModel> getOrderModel(final BusinessProcessModel businessProcessModel)
	{
		return Optional.of(businessProcessModel)
				.filter(businessProcess -> businessProcess instanceof GenerateNotificationEmailProcessModel)
				.map(businessProcess -> ((GenerateNotificationEmailProcessModel) businessProcess).getOrder());
	}
}
