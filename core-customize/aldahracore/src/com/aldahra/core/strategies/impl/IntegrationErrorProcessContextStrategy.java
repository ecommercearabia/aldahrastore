package com.aldahra.core.strategies.impl;

import de.hybris.platform.acceleratorservices.process.strategies.impl.AbstractOrderProcessContextStrategy;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;

import java.util.Optional;

import com.aldahra.core.model.GenerateErrorEmailProcessModel;


/**
 * @author mnasro
 */
public class IntegrationErrorProcessContextStrategy extends AbstractOrderProcessContextStrategy
{
	@Override
	protected Optional<AbstractOrderModel> getOrderModel(final BusinessProcessModel businessProcessModel)
	{
		return Optional.of(businessProcessModel)
				.filter(businessProcess -> businessProcess instanceof GenerateErrorEmailProcessModel)
				.map(businessProcess -> ((GenerateErrorEmailProcessModel) businessProcess).getOrder());
	}
}
