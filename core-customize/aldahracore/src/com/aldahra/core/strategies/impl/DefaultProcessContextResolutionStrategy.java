package com.aldahra.core.strategies.impl;

import de.hybris.platform.acceleratorservices.process.strategies.ProcessContextResolutionStrategy;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class DefaultProcessContextResolutionStrategy implements ProcessContextResolutionStrategy<BaseSiteModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultProcessContextResolutionStrategy.class);

	@Resource(name = "processContextResolutionStrategyMap")
	private Map<Class<?>, ProcessContextResolutionStrategy<BaseSiteModel>> processStrategyMap;

	@Override
	public void initializeContext(final BusinessProcessModel businessProcessModel)
	{
		final Optional<ProcessContextResolutionStrategy<BaseSiteModel>> strategy = getStrategy(businessProcessModel);
		if (strategy.isPresent())
		{
			strategy.get().initializeContext(businessProcessModel);
		}
		else
		{
			LOG.warn("Attempt to initialize process context for business process [{}] failed, no strategy found",
					businessProcessModel.getCode());
		}
	}

	@Override
	public CatalogVersionModel getContentCatalogVersion(final BusinessProcessModel businessProcessModel)
	{
		return getStrategy(businessProcessModel)
				.map(contextStrategy -> contextStrategy.getContentCatalogVersion(businessProcessModel)).orElse(null);
	}

	@Override
	public BaseSiteModel getCmsSite(final BusinessProcessModel businessProcessModel)
	{
		return getStrategy(businessProcessModel).map(contextStrategy -> contextStrategy.getCmsSite(businessProcessModel))
				.orElse(null);
	}

	protected Optional<ProcessContextResolutionStrategy<BaseSiteModel>> getStrategy(
			final BusinessProcessModel businessProcessModel)
	{
		final Class<?> processClass = businessProcessModel.getClass();
		ProcessContextResolutionStrategy<BaseSiteModel> strategy = getProcessStrategyMap().get(processClass);
		if (strategy == null)
		{
			final Class<?> bestClass = findMostSpecificClass(processClass);
			strategy = getProcessStrategyMap().get(bestClass);
		}
		return Optional.of(strategy);
	}

	private Class<?> findMostSpecificClass(final Class<?> processClass)
	{
		Class<?> mostSpecific = null;
		final Set<Class<?>> supportedClasses = getProcessStrategyMap().keySet();
		for (final Class<?> supportedClass : supportedClasses)
		{
			if (!supportedClass.isAssignableFrom(processClass))
			{
				continue;
			}

			if (mostSpecific == null || mostSpecific.isAssignableFrom(supportedClass))
			{
				mostSpecific = supportedClass;
			}
		}
		return mostSpecific;
	}

	protected Map<Class<?>, ProcessContextResolutionStrategy<BaseSiteModel>> getProcessStrategyMap()
	{
		return processStrategyMap;
	}
}
