package com.aldahra.core.comparators;

import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;

import java.util.Comparator;


public class ConsignmentEntryPriorityComparator implements Comparator<ConsignmentEntryModel>
{

	@Override
	public int compare(final ConsignmentEntryModel conEntyModel1, final ConsignmentEntryModel conEntyModel2)
	{

		final int long1 = getConsignmentEntryPriority(conEntyModel1);
		final int long2 = getConsignmentEntryPriority(conEntyModel2);

		return long1 > long2 ? -1 : 1;

	}

	/**
	 * @param conEntyModel
	 * @return
	 */
	private int getConsignmentEntryPriority(final ConsignmentEntryModel conEntyModel)
	{

		return (conEntyModel == null || conEntyModel.getOrderEntry() == null || conEntyModel.getOrderEntry().getProduct() == null
				|| conEntyModel.getOrderEntry().getProduct().getPickSlipPriority() == null) ? 0
						: conEntyModel.getOrderEntry().getProduct().getPickSlipPriority().getPriority();
	}

}