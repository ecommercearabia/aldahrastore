package com.aldahra.core.comparators;

import de.hybris.platform.catalog.CatalogService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.solrfacetsearch.search.FacetValue;

import java.util.Comparator;

import javax.annotation.Resource;

import org.springframework.util.Assert;
import org.springframework.util.StringUtils;


public class FacetCategoryPriorityComparator implements Comparator<FacetValue>
{

	private static final String PROP_CATALOGID_KEY = "facet.category.priority.catalogid";

	@Resource(name = "catalogService")
	private CatalogService catalogService;

	@Resource(name = "categoryService")
	private CategoryService categoryService;


	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Resource(name = "facetDisplayNameComparator")
	private Comparator facetDisplayNameComparator;

	/**
	 * @return the facetDisplayNameComparator
	 */
	protected Comparator getFacetDisplayNameComparator()
	{
		return facetDisplayNameComparator;
	}

	public int compare(final FacetValue value1, final FacetValue value2)
	{

		final String catalogId = getCatalogId();
		if (value1 == null || value2 == null || StringUtils.isEmpty(catalogId))
		{
			return 0;
		}

		final CategoryModel cat1 = getCategory(catalogId, value1.getName());
		final CategoryModel cat2 = getCategory(catalogId, value2.getName());

		Assert.notNull(cat1, "Parameter Category 1 cannot be null.");

		Assert.notNull(cat2, "Parameter Category 2 cannot be null.");

		final int long1 = cat1.getPriority();
		final int long2 = cat2.getPriority();
		if (long1 == long2)
		{
			return getFacetDisplayNameComparator().compare(value1, value2);
		}

		return long1 > long2 ? -1 : 1;

	}

	/**
	 * @param name
	 * @return
	 */
	private CategoryModel getCategory(final String catalogId, final String code)
	{
		final CatalogVersionModel catalogVersion = catalogService.getCatalogVersion(catalogId, "Online");
		try
		{
			return categoryService.getCategoryForCode(catalogVersion, code);

		}
		catch (final Exception e)
		{
			return null;
		}
	}

	private String getCatalogId()
	{
		return getConfigurationService().getConfiguration().getString(PROP_CATALOGID_KEY);
	}

	//	facet.category.priority.catalogid=foodcrowdProductCatalog
}
