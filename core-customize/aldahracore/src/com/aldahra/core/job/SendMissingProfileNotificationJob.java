/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.core.job;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aldahra.core.event.CustomerMissingAttributesEvent;
import com.aldahra.core.model.SendMissingProfileNotificationCronJobModel;
import com.aldahra.core.service.CustomCustomerService;


/**
 * @author Tuqa
 */
public class SendMissingProfileNotificationJob extends AbstractJobPerformable<SendMissingProfileNotificationCronJobModel>
{

	private static final Logger LOG = LoggerFactory.getLogger(SendMissingProfileNotificationJob.class);

	@Resource(name = "eventService")
	private EventService eventService;

	@Resource(name = "customCustomerService")
	private CustomCustomerService customCustomerService;

	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	static final List<String> MISSINGATTRIBUTES = Arrays.asList("Nationality", "Title");


	@Override
	public PerformResult perform(final SendMissingProfileNotificationCronJobModel cronjob)
	{
		LOG.debug("Start Send notification to updated profile for customers .....");
		final List<CustomerModel> customers = getCustomCustomerService().getAllCustomersWithoutNationalityOrTitle();
		boolean flag = false;
		for (final CustomerModel customer : customers)
		{
			try
			{
				sendNotification(customer, cronjob);
				flag = true;
			}

			catch (final Exception e)
			{
				LOG.info("Can not Send notification to updated profile Information for customer : " + customer.getUid());
				flag = false;
			}
		}
		LOG.debug("Send notification to updated profile for customers Finished.....");
		return new PerformResult((flag ? CronJobResult.SUCCESS : CronJobResult.ERROR), CronJobStatus.FINISHED);

	}

	/**
	 * @param customer
	 * @param cronjob
	 */
	private void sendNotification(final CustomerModel customer, final SendMissingProfileNotificationCronJobModel cronjob)
	{
		if ((cronjob.isSentMissingProfileNotificationToAll() && customer.isUpdateMissingAttributesEmailSent())
				|| !customer.isUpdateMissingAttributesEmailSent())
		{
			publishEvent(cronjob.getSite(), customer, cronjob.getStore());
			customer.setUpdateMissingAttributesEmailSent(true);
			LOG.info("Send notification to updated profile Information for customer : " + customer.getUid());
		}
		else if ((!cronjob.isSentMissingProfileNotificationToAll()) && customer.isUpdateMissingAttributesEmailSent())
		{
			LOG.info("Customer already notified: " + customer.getUid());
		}

	}



	private void publishEvent(final BaseSiteModel site, final CustomerModel customer, final BaseStoreModel store)
	{
		final CustomerMissingAttributesEvent event = new CustomerMissingAttributesEvent(MISSINGATTRIBUTES);
		event.setSite(site);
		event.setCustomer(customer);
		event.setBaseStore(store);
		event.setCurrency(customer.getSessionCurrency());
		event.setLanguage(customer.getSessionLanguage() != null ? customer.getSessionLanguage() : site.getDefaultLanguage());
		getEventService().publishEvent(event);
	}



	protected CustomCustomerService getCustomCustomerService()

	{
		return customCustomerService;
	}

	/**
	 * @return the eventService
	 */
	protected EventService getEventService()
	{
		return eventService;
	}


}
