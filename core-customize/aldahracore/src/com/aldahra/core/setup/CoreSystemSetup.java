/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.core.setup;

import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;

import java.util.ArrayList;
import java.util.List;

import com.aldahra.core.constants.AldahraCoreConstants;


/**
 * This class provides hooks into the system's initialization and update processes.
 */
@SystemSetup(extension = AldahraCoreConstants.EXTENSIONNAME)
public class CoreSystemSetup extends AbstractSystemSetup
{
	public static final String IMPORT_ACCESS_RIGHTS = "accessRights";

	/**
	 * This method will be called by system creator during initialization and system update. Be sure that this method can
	 * be called repeatedly.
	 *
	 * @param context
	 *           the context provides the selected parameters and values
	 */
	@SystemSetup(type = Type.ESSENTIAL, process = Process.ALL)
	public void createEssentialData(final SystemSetupContext context)
	{
		importImpexFile(context, "/aldahracore/import/common/essential-data.impex");
		importImpexFile(context, "/aldahracore/import/common/countries.impex");
		importImpexFile(context, "/aldahracore/import/common/delivery-modes.impex");

		importImpexFile(context, "/aldahracore/import/common/themes.impex");
		importImpexFile(context, "/aldahracore/import/common/user-groups.impex");
		importImpexFile(context, "/aldahracore/import/common/cronjobs.impex");

		importImpexFile(context, "/aldahracore/import/marketing/remove-and-update-defult-data.impex");
		importImpexFile(context, "/aldahracore/import/marketing/consents.impex");
		importImpexFile(context, "/aldahracore/import/marketing/consents_en.impex");
		importImpexFile(context, "/aldahracore/import/marketing/consents_ar.impex");


		importFiles(context, "/aldahracore/import/marketing/essentialdata-MKTOutboundAbandonedCart.impex",
				"/aldahracore/import/marketing/essentialdata-MKTOutboundConsent.impex",
				"/aldahracore/import/marketing/essentialdata-MKTOutboundCouponRedemption.impex",
				"/aldahracore/import/marketing/essentialdata-MKTOutboundCustomer.impex",
				"/aldahracore/import/marketing/essentialdata-MKTOutboundOrder.impex",
				"/aldahracore/import/marketing/essentialdata-MKTOutboundProduct.impex",
				"/aldahracore/import/marketing/essentialdata-MKTOutboundProductCategory.impex",
				"/aldahracore/import/marketing/essentialdata-MKTOutboundProductReview.impex",
				"/aldahracore/import/marketing/essentialdata-MKTOutboundPromotion.impex",
				"/aldahracore/import/marketing/essentialdata-MKTOutboundReturnRequest.impex",
				"/aldahracore/import/marketing/essentialdata-MKTOutboundSavedCart.impex",
				"/aldahracore/import/marketing/essentialdata-MKTOutboundSingleCodeCoupon.impex");
	}

	/**
	 * Generates the Dropdown and Multi-select boxes for the project data import
	 */
	@Override
	@SystemSetupParameterMethod
	public List<SystemSetupParameter> getInitializationOptions()
	{
		final List<SystemSetupParameter> params = new ArrayList<>();

		params.add(createBooleanSystemSetupParameter(IMPORT_ACCESS_RIGHTS, "Import Users & Groups", true));

		return params;
	}

	/**
	 * This method will be called during the system initialization.
	 *
	 * @param context
	 *           the context provides the selected parameters and values
	 */
	@SystemSetup(type = Type.PROJECT, process = Process.ALL)
	public void createProjectData(final SystemSetupContext context)
	{
		final boolean importAccessRights = getBooleanSystemSetupParameter(context, IMPORT_ACCESS_RIGHTS);

		final List<String> extensionNames = getExtensionNames();

		processCockpit(context, importAccessRights, extensionNames, "cmsbackoffice",
				"/aldahracore/import/cockpits/cmscockpit/cmscockpit-users.impex",
				"/aldahracore/import/cockpits/cmscockpit/cmscockpit-access-rights.impex");

		processCockpit(context, importAccessRights, extensionNames, "productcockpit",
				"/aldahracore/import/cockpits/productcockpit/productcockpit-users.impex",
				"/aldahracore/import/cockpits/productcockpit/productcockpit-access-rights.impex",
				"/aldahracore/import/cockpits/productcockpit/productcockpit-constraints.impex");

		processCockpit(context, importAccessRights, extensionNames, "customersupportbackoffice",
				"/aldahracore/import/cockpits/cscockpit/cscockpit-users.impex",
				"/aldahracore/import/cockpits/cscockpit/cscockpit-access-rights.impex");

		importFiles(context, "/aldahracore/import/marketing/projectdata-MKTOutboundAbandonedCart.impex",
				"/aldahracore/import/marketing/projectdata-MKTOutboundConsent.impex",
				"/aldahracore/import/marketing/projectdata-MKTOutboundCouponRedemption.impex",
				"/aldahracore/import/marketing/projectdata-MKTOutboundCustomer.impex",
				"/aldahracore/import/marketing/projectdata-MKTOutboundOrder.impex",
				"/aldahracore/import/marketing/projectdata-MKTOutboundProduct.impex",
				"/aldahracore/import/marketing/projectdata-MKTOutboundProductCategory.impex",
				"/aldahracore/import/marketing/projectdata-MKTOutboundProductReview.impex",
				"/aldahracore/import/marketing/projectdata-MKTOutboundPromotion.impex",
				"/aldahracore/import/marketing/projectdata-MKTOutboundReturnRequest.impex",
				"/aldahracore/import/marketing/projectdata-MKTOutboundSavedCart.impex",
				"/aldahracore/import/marketing/projectdata-MKTOutboundSingleCodeCoupon.impex");
	}

	private void importFiles(final SystemSetupContext context, final String... files)
	{
		for (final String file : files)
		{
			importImpexFile(context, file);
		}
	}

	protected void processCockpit(final SystemSetupContext context, final boolean importAccessRights,
			final List<String> extensionNames, final String cockpit, final String... files)
	{
		if (importAccessRights && extensionNames.contains(cockpit))
		{
			for (final String file : files)
			{
				importImpexFile(context, file);
			}
		}
	}

	protected List<String> getExtensionNames()
	{
		return Registry.getCurrentTenant().getTenantSpecificExtensionNames();
	}
}
