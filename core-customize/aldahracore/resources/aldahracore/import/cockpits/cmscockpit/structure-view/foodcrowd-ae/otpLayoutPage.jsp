<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<template:page pageTitle="${pageTitle}">

	<div class="row sec1">
	<br/><br/>
		<div class="col-md-3 col-sm-2 col-xs-1"></div>
		<div class="col-md-6 col-sm-8 col-xs-10">
			<cms:pageSlot position="OTPSection1" var="feature" >
				<cms:component component="${feature}" />
			</cms:pageSlot>
		</div>
		<div class="col-md-3 col-sm-2 col-xs-1"></div>
	</div>

	<cms:pageSlot position="OTPSection2" var="feature">
		<cms:component component="${feature}" />
	</cms:pageSlot>

</template:page>