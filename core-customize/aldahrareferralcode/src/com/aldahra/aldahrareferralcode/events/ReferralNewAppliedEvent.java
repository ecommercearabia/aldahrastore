/**
 *
 */
package com.aldahra.aldahrareferralcode.events;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;


/**
 * @author mohammad-abumuahsien
 *
 */
public class ReferralNewAppliedEvent extends AbstractCommerceUserEvent<BaseSiteModel>
{

}
