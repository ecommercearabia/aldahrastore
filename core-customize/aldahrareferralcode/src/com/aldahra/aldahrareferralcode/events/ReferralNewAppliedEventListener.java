/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrareferralcode.events;

import de.hybris.platform.acceleratorservices.site.AbstractAcceleratorSiteEventListener;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import com.aldahra.Ordermanagement.model.ReferralNewAppliedEmailProcessModel;


/**
 * Listener for customer registration events.
 *
 * @author mohammad-abumuhasien
 */
public class ReferralNewAppliedEventListener extends AbstractAcceleratorSiteEventListener<ReferralNewAppliedEvent>
{
	private BusinessProcessService businessProcessService;
	private ModelService modelService;



	protected SiteChannel getSiteChannelForEvent(final ReferralNewAppliedEvent event)
	{
		final BaseSiteModel site = event.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order.site", site);
		return site.getChannel();
	}

	protected void onSiteEvent(final ReferralNewAppliedEvent event)
	{
		final ReferralNewAppliedEmailProcessModel process = getBusinessProcessService().createProcess(
				"referralNewAppliedEmailProcess-" + event.getCustomer().getUid() + "-" + System.currentTimeMillis(),
				"referralNewAppliedEmailProcess");

		process.setSite(event.getSite());
		process.setStore(event.getBaseStore());
		process.setCustomer(event.getCustomer());
		process.setLanguage(event.getLanguage());
		modelService.save(process);
		getBusinessProcessService().startProcess(process);

	}

	/**
	 * @return the businessProcessService
	 */
	public BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	/**
	 * @param businessProcessService
	 *           the businessProcessService to set
	 */
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}



}
