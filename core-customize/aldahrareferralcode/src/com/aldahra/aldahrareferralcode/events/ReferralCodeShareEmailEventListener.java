/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrareferralcode.events;

import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import de.hybris.platform.servicelayer.model.ModelService;

import org.springframework.beans.factory.annotation.Required;

import com.aldahra.Ordermanagement.model.ReferralCodeShareEmailProcessModel;


/**
 * Listener for customer registration events.
 *
 * @author mohammad-abumuhasien
 */
public class ReferralCodeShareEmailEventListener extends AbstractEventListener<ReferralCodeShareEmailEvent>
{

	private ModelService modelService;
	private BusinessProcessService businessProcessService;

	protected BusinessProcessService getBusinessProcessService()
	{

		return businessProcessService;
	}

	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;

	}

	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}



	@Override
	protected void onEvent(final ReferralCodeShareEmailEvent registerEvent)
	{
		final ReferralCodeShareEmailProcessModel referralCodeShareEmailProcessModel = (ReferralCodeShareEmailProcessModel) getBusinessProcessService()
				.createProcess(
						"referralCodeShareEmailProcess-" + registerEvent.getCustomer().getUid() + "-" + System.currentTimeMillis(),
						"referralCodeShareEmailProcess");
		referralCodeShareEmailProcessModel.setSite(registerEvent.getSite());
		referralCodeShareEmailProcessModel.setCustomer(registerEvent.getCustomer());
		referralCodeShareEmailProcessModel.setLanguage(registerEvent.getLanguage());
		referralCodeShareEmailProcessModel.setCurrency(registerEvent.getCurrency());
		referralCodeShareEmailProcessModel.setStore(registerEvent.getBaseStore());
		referralCodeShareEmailProcessModel.setToAddresses(registerEvent.getToEmails());
		getModelService().save(referralCodeShareEmailProcessModel);
		getBusinessProcessService().startProcess(referralCodeShareEmailProcessModel);

	}



}
