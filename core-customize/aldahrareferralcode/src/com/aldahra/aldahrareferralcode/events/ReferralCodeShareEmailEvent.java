/**
 *
 */
package com.aldahra.aldahrareferralcode.events;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.List;


/**
 * @author mohammad-abumuahsien
 *
 */
public class ReferralCodeShareEmailEvent extends AbstractCommerceUserEvent<BaseSiteModel>
{

	private List<String> toEmails;



	public ReferralCodeShareEmailEvent()
	{

	}

	public ReferralCodeShareEmailEvent(final CustomerModel customer)
	{
		setCustomer(customer);
	}


	/**
	 *
	 */
	public ReferralCodeShareEmailEvent(final CustomerModel customer, final List<String> toEmails)
	{
		super();
		this.toEmails = toEmails;
		setCustomer(customer);
	}

	/**
	 *
	 */
	public ReferralCodeShareEmailEvent(final List<String> toEmails)
	{
		super();
		this.toEmails = toEmails;
	}

	/**
	 * @return the toEmails
	 */
	public List<String> getToEmails()
	{
		return toEmails;
	}


	/**
	 * @param toEmails
	 *           the toEmails to set
	 */
	public void setToEmails(final List<String> toEmails)
	{
		this.toEmails = toEmails;
	}



}
