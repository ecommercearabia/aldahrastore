/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrareferralcode.service;

import de.hybris.platform.core.model.user.CustomerModel;

import java.util.List;

import com.aldahra.aldahrareferralcode.model.AppliedReferralHistoryModel;


/**
 * @author mohammedbaker The Interface AppliedReferralHistoryService.
 */
public interface AppliedReferralHistoryService
{

	/**
	 * Gets the all.
	 *
	 * @return the all
	 */
	public List<AppliedReferralHistoryModel> getAll();

	/**
	 * Gets the by referral code.
	 *
	 * @param code the code
	 * @return the by referral code
	 */
	public List<AppliedReferralHistoryModel> getByReferralCode(String code);

	/**
	 * Gets the by owner customer.
	 *
	 * @param ownerCustomer the owner customer
	 * @return the by owner customer
	 */
	public List<AppliedReferralHistoryModel> getByOwnerCustomer(CustomerModel ownerCustomer);

	/**
	 * Gets the by referral code and applied customer.
	 *
	 * @param referralCode the referral code
	 * @param appliedCustomer the applied customer
	 * @return the by referral code and applied customer
	 */
	public List<AppliedReferralHistoryModel> getByReferralCodeAndAppliedCustomer(String referralCode,
			CustomerModel appliedCustomer);

	/**
	 * Gets the by owner customer and applied customer.
	 *
	 * @param ownerCustomer the owner customer
	 * @param appliedCustomer the applied customer
	 * @return the by owner customer and applied customer
	 */
	public List<AppliedReferralHistoryModel> getByOwnerCustomerAndAppliedCustomer(CustomerModel ownerCustomer,
			CustomerModel appliedCustomer);

}
