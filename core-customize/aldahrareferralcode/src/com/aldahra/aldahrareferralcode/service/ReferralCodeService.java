/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrareferralcode.service;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;
import java.util.Optional;

import com.aldahra.aldahrareferralcode.exception.ReferralCodeException;
import com.aldahra.aldahrareferralcode.model.ReferralCodeModel;


/**
 * @author mohammedbaker The Interface ReferralCodeService.
 */
public interface ReferralCodeService
{

	/**
	 * Gets the all.
	 *
	 * @return the all
	 */
	public List<ReferralCodeModel> getAll();

	/**
	 * Gets the by code.
	 *
	 * @param code
	 *           the code
	 * @return the by code
	 */
	public Optional<ReferralCodeModel> getByCode(String code);

	/**
	 * Generate.
	 *
	 * @param ownerCustomer
	 *           the owner customer
	 * @param baseStoreModel
	 *           the base store model
	 * @return the optional
	 * @throws ReferralCodeException
	 *            the referral code exception
	 */
	public Optional<ReferralCodeModel> generate(CustomerModel ownerCustomer, BaseStoreModel baseStoreModel)
			throws ReferralCodeException;

	/**
	 * Generate by current base store.
	 *
	 * @param ownerCustomer
	 *           the owner customer
	 * @return the optional
	 * @throws ReferralCodeException
	 *            the referral code exception
	 */
	public Optional<ReferralCodeModel> generateByCurrentBaseStore(CustomerModel ownerCustomer) throws ReferralCodeException;

	/**
	 * Generate by current base store and current customer.
	 *
	 * @return the optional
	 * @throws ReferralCodeException
	 *            the referral code exception
	 */
	public Optional<ReferralCodeModel> generateByCurrentBaseStoreAndCurrentCustomer() throws ReferralCodeException;

	/**
	 * Can apply customer.
	 *
	 * @param referralCode
	 *           the referral code
	 * @return true, if successful
	 */
	public boolean canApplyCustomer(final BaseStoreModel baseStore, String referralCode);

	public boolean canApplyCustomerByCurrentBaseStore(String referralCode);

	/**
	 * Apply customer.
	 *
	 * @param referralCode
	 *           the referral code
	 * @param appliedCustomer
	 *           the applied customer
	 * @throws ReferralCodeException
	 *            the referral code exception
	 */
	public boolean applyCustomer(BaseStoreModel baseStore, String referralCode, CustomerModel appliedCustomer)
			throws ReferralCodeException;

	public boolean applyCustomerByCurrentBaseStore(String referralCode, CustomerModel appliedCustomer)
			throws ReferralCodeException;

	public boolean applyCustomerByCurrentBaseStoreAndCurrentCustomer(String referralCode) throws ReferralCodeException;

	/**
	 * Apply order.
	 *
	 * @param orderModel
	 *           the order model
	 * @throws ReferralCodeException
	 *            the referral code exception
	 */
	public boolean applyOrder(AbstractOrderModel orderModel) throws ReferralCodeException;

	/**
	 * Can apply order.
	 *
	 * @param orderModel
	 *           the order model
	 * @return true, if successful
	 */
	public boolean canApplyOrder(AbstractOrderModel orderModel);

	/**
	 * Checks if is enabled.
	 *
	 * @param baseStore
	 *           the base store
	 * @return true, if is enabled
	 */
	public boolean isEnabled(BaseStoreModel baseStore);

	/**
	 * Gets the referral code by current customer.
	 *
	 * @return the referral code by current customer
	 * @throws ReferralCodeException
	 *            the referral code exception
	 */
	public Optional<ReferralCodeModel> getReferralCodeByCurrentCustomer() throws ReferralCodeException;


	public void shareByEmailsByCurrentSiteAndStore(final List<String> toEmail, final boolean seperateEmail)
			throws ReferralCodeException;

	public void shareByEmails(CMSSiteModel cmsSiteModel, BaseStoreModel baseStoreModel, final List<String> toEmail,
			final boolean seperateEmail) throws ReferralCodeException;

	public Optional<ReferralCodeModel> getAppliedReferralCodeByCurrentCustomer() throws ReferralCodeException;
}
