/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrareferralcode.service.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.aldahra.aldahrareferralcode.dao.ReferralCodeCustomerDAO;
import com.aldahra.aldahrareferralcode.model.ReferralCodeModel;
import com.aldahra.aldahrareferralcode.service.ReferralCodeCustomerService;


/**
 * @author mohammedbaker The Class DefaultReferralCodeCustomerService.
 */
public class DefaultReferralCodeCustomerService implements ReferralCodeCustomerService
{

	/** The customer DAO. */
	@Resource(name = "referralCodeCustomerDAO")
	private ReferralCodeCustomerDAO customerDAO;

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(DefaultReferralCodeCustomerService.class);
	@Resource
	private UserService userService;
	/**
	 * Gets the customer by referral code.
	 *
	 * @param referralCode
	 *           the referral code
	 * @return the customer by referral code
	 */
	@Override
	public Optional<CustomerModel> getCustomerByReferralCode(final ReferralCodeModel referralCode)
	{
		LOG.info("CustomerService: start getCustomerByReferralCode method");
		if (Objects.isNull(referralCode))
		{
			LOG.error("CustomerService: referralCode is null");
			throw new IllegalArgumentException("referralCode is null");
		}
		return customerDAO.getByReferralCode(referralCode);
	}

	@Override
	public boolean isFirstOrder(final CustomerModel appliedCustomer)
	{
		LOG.info("Check if its first order for the Customer");

		final Collection<OrderModel> orders = appliedCustomer.getOrders();

		return Objects.isNull(orders) || orders.isEmpty() || orders.size() <= 1;

	}


}
