/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrareferralcode.service.impl;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.aldahra.aldahrareferralcode.dao.AppliedReferralHistoryDAO;
import com.aldahra.aldahrareferralcode.model.AppliedReferralHistoryModel;
import com.aldahra.aldahrareferralcode.model.ReferralCodeModel;
import com.aldahra.aldahrareferralcode.service.AppliedReferralHistoryService;
import com.aldahra.aldahrareferralcode.service.ReferralCodeService;
import com.aldahra.aldahrareferralcode.validator.ReferralCodeValidator;




/**
 * @author mohammedbaker The Class DefaultAppliedReferralHistoryService.
 */
public class DefaultAppliedReferralHistoryService implements AppliedReferralHistoryService
{

	/** The applied referral history DAO. */
	@Resource(name = "appliedReferralHistoryDAO")
	private AppliedReferralHistoryDAO appliedReferralHistoryDAO;

	/** The model service. */
	@Resource(name = "modelService")
	private ModelService modelService;

	/** The validator. */
	@Resource(name = "referralCodeValidator")
	private ReferralCodeValidator validator;

	/** The referral code service. */
	@Resource(name = "referralCodeService")
	private ReferralCodeService referralCodeService;

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(DefaultAppliedReferralHistoryService.class);


	/**
	 * Gets the all.
	 *
	 * @return the all
	 */
	@Override
	public List<AppliedReferralHistoryModel> getAll()
	{
		LOG.info("AppliedReferralHistoryService: start getAll method");
		return appliedReferralHistoryDAO.getAll();
	}

	/**
	 * Gets the by referral code.
	 *
	 * @param code
	 *           the code
	 * @return the by referral code
	 */
	@Override
	public List<AppliedReferralHistoryModel> getByReferralCode(final String code)
	{
		LOG.info("AppliedReferralHistoryService: start getByReferralCode method");
		if (Objects.isNull(code))
		{
			LOG.error("AppliedReferralHistoryService: code is null");
			throw new IllegalArgumentException("code is null");
		}
		LOG.info("AppliedReferralHistoryService: end getByReferralCode method");
		return appliedReferralHistoryDAO.getByReferralCode(code);
	}

	/**
	 * Gets the by owner customer.
	 *
	 * @param ownerCustomer
	 *           the owner customer
	 * @return the by owner customer
	 */
	@Override
	public List<AppliedReferralHistoryModel> getByOwnerCustomer(final CustomerModel ownerCustomer)
	{
		LOG.info("AppliedReferralHistoryService: start getByOwnerCustomer method");
		validator.validateOwnerCustomer(ownerCustomer);
		LOG.info("AppliedReferralHistoryService: end getByOwnerCustomer method");

		final ReferralCodeModel referralCodeModel = ownerCustomer.getReferralCode();
		if (Objects.isNull(referralCodeModel))
		{
			return Collections.emptyList();
		}
		return getByReferralCode(referralCodeModel.getCode());
	}

	/**
	 * Gets the by referral code and applied customer.
	 *
	 * @param referralCode
	 *           the referral code
	 * @param appliedCustomer
	 *           the applied customer
	 * @return the by referral code and applied customer
	 */
	@Override
	public List<AppliedReferralHistoryModel> getByReferralCodeAndAppliedCustomer(final String referralCode,
			final CustomerModel appliedCustomer)
	{
		LOG.info("AppliedReferralHistoryService: start getByReferralCodeAndAppliedCustomer method");
		if (Objects.isNull(referralCode))
		{
			LOG.error("AppliedReferralHistoryService: referralCode is null");
			throw new IllegalArgumentException("referralCode is null");
		}
		validator.validateAppliedCustomer(appliedCustomer);
		LOG.info("AppliedReferralHistoryService: end getByReferralCodeAndAppliedCustomer method");
		return appliedReferralHistoryDAO.getByReferralCodeAndAppliedCustomer(referralCode, appliedCustomer.getCustomerID());
	}

	/**
	 * Gets the by owner customer and applied customer.
	 *
	 * @param ownerCustomer
	 *           the owner customer
	 * @param appliedCustomer
	 *           the applied customer
	 * @return the by owner customer and applied customer
	 */
	@Override
	public List<AppliedReferralHistoryModel> getByOwnerCustomerAndAppliedCustomer(final CustomerModel ownerCustomer,
			final CustomerModel appliedCustomer)
	{
		LOG.info("AppliedReferralHistoryService: start getByOwnerCustomerAndAppliedCustomer method");
		validator.validateOwnerCustomer(ownerCustomer);
		validator.validateAppliedCustomer(appliedCustomer);
		final ReferralCodeModel referralCodeModel = ownerCustomer.getReferralCode();
		if (Objects.isNull(referralCodeModel))
		{
			return Collections.emptyList();
		}
		return getByReferralCodeAndAppliedCustomer(referralCodeModel.getCode(), appliedCustomer);
	}




}
