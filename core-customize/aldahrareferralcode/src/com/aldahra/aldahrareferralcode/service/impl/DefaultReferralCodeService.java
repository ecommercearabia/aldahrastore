/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrareferralcode.service.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.aldahra.aldahrareferralcode.bean.ReferralCodeConfig;
import com.aldahra.aldahrareferralcode.dao.ReferralCodeDAO;
import com.aldahra.aldahrareferralcode.enums.ReferralCodeMaxAmountType;
import com.aldahra.aldahrareferralcode.enums.ReferralConfigType;
import com.aldahra.aldahrareferralcode.enums.ReferralOrderRewardType;
import com.aldahra.aldahrareferralcode.enums.SaveType;
import com.aldahra.aldahrareferralcode.event.CustomCustomerRegistrationEvent;
import com.aldahra.aldahrareferralcode.events.ReferralCodeShareEmailEvent;
import com.aldahra.aldahrareferralcode.events.ReferralNewAppliedEvent;
import com.aldahra.aldahrareferralcode.exception.ReferralCodeException;
import com.aldahra.aldahrareferralcode.exception.type.ReferralCodeExceptionType;
import com.aldahra.aldahrareferralcode.model.AppliedReferralHistoryModel;
import com.aldahra.aldahrareferralcode.model.ReferralCodeModel;
import com.aldahra.aldahrareferralcode.service.AppliedReferralHistoryService;
import com.aldahra.aldahrareferralcode.service.ReferralCodeCustomerService;
import com.aldahra.aldahrareferralcode.service.ReferralCodeService;
import com.aldahra.aldahrareferralcode.validator.ReferralCodeValidator;
import com.aldahra.aldahrastorecredit.service.StoreCreditService;
import com.google.common.base.Preconditions;




/**
 * @author mohammedbaker The Class DefaultReferralCodeService.
 */
public class DefaultReferralCodeService implements ReferralCodeService
{

	/** The referral code DAO. */
	@Resource(name = "referralCodeDAO")
	private ReferralCodeDAO referralCodeDAO;

	/** The model service. */
	@Resource(name = "modelService")
	private ModelService modelService;

	/** The key generator. */
	@Resource(name = "referralCodeGenerator")
	private KeyGenerator keyGenerator;

	/** The validator. */
	@Resource(name = "referralCodeValidator")
	private ReferralCodeValidator validator;

	/** The base store service. */
	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	/** The user service. */
	@Resource(name = "userService")
	private UserService userService;

	/** The applied referral history service. */
	@Resource(name = "appliedReferralHistoryService")
	private AppliedReferralHistoryService appliedReferralHistoryService;

	/** The store credit service. */
	@Resource(name = "storeCreditService")
	private StoreCreditService storeCreditService;

	/** The customer service. */
	@Resource(name = "referralCodeCustomerService")
	private ReferralCodeCustomerService customerService;

	@Resource(name = "eventService")
	private EventService eventService;

	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;
	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;
	private static final String ABSTRACT_ORDER_NULL_MSG = "order is null";
	private static final String STORE_NULL_MSG = "store on the order is null";
	private static final String USER_ORDER_NULL_MSG = "User on the Order is null";
	private static final String USER_IS_NOT_CUSTOMER = "User on the Order is not a customer";


	/**
	 * @return the cmsSiteService
	 */
	public CMSSiteService getCmsSiteService()
	{
		return cmsSiteService;
	}

	private static final String BASE_STORE_NULL_MSG = "base store is null";

	/**
	 * @return the referralCodeDAO
	 */
	protected ReferralCodeDAO getReferralCodeDAO()
	{
		return referralCodeDAO;
	}

	/**
	 * @return the eventService
	 */
	public EventService getEventService()
	{
		return eventService;
	}

	/**
	 * @param eventService
	 *           the eventService to set
	 */
	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}

	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @return the keyGenerator
	 */
	protected KeyGenerator getKeyGenerator()
	{
		return keyGenerator;
	}

	/**
	 * @return the validator
	 */
	protected ReferralCodeValidator getValidator()
	{
		return validator;
	}

	/**
	 * @return the baseStoreService
	 */
	protected BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	/**
	 * @return the userService
	 */
	protected UserService getUserService()
	{
		return userService;
	}

	/**
	 * @return the appliedReferralHistoryService
	 */
	protected AppliedReferralHistoryService getAppliedReferralHistoryService()
	{
		return appliedReferralHistoryService;
	}

	/**
	 * @return the storeCreditService
	 */
	protected StoreCreditService getStoreCreditService()
	{
		return storeCreditService;
	}

	/**
	 * @return the customerService
	 */
	protected ReferralCodeCustomerService getCustomerService()
	{
		return customerService;
	}



	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(DefaultReferralCodeService.class);

	/**
	 * Gets the all.
	 *
	 * @return the all
	 */
	@Override
	public List<ReferralCodeModel> getAll()
	{
		LOG.info("ReferralCodeService: start getAll method");
		return referralCodeDAO.getAll();
	}

	/**
	 * Gets the by code.
	 *
	 * @param code
	 *           the code
	 * @return the by code
	 */
	@Override
	public Optional<ReferralCodeModel> getByCode(final String code)
	{
		LOG.info("ReferralCodeService: start getByCode method");
		if (Objects.isNull(code))
		{
			LOG.error("ReferralCodeService: code is null");
			return Optional.empty();
		}
		return referralCodeDAO.getByCode(code);
	}

	/**
	 * Generate.
	 *
	 * @param ownerCustomer
	 *           the owner customer
	 * @param baseStoreModel
	 *           the base store model
	 * @return the optional
	 * @throws ReferralCodeException
	 *            the referral code exception
	 */
	@Override
	public Optional<ReferralCodeModel> generate(final CustomerModel ownerCustomer, final BaseStoreModel baseStoreModel)
			throws ReferralCodeException
	{
		LOG.info("ReferralCodeService: start generate method");
		if (Objects.isNull(ownerCustomer))
		{
			LOG.error("ReferralCodeService: ownerCustomer is null");
			throw new IllegalArgumentException("ownerCustomer is null");
		}
		if (!Objects.isNull(ownerCustomer.getReferralCode()))
		{
			final ReferralCodeException ex = new ReferralCodeException(ReferralCodeExceptionType.CUSTOMER_HAS_CODE,
					ReferralCodeExceptionType.CUSTOMER_HAS_CODE.getMsg());
			LOG.error(ex.getMessage());
			throw ex;
		}

		validator.validateBaseStore(baseStoreModel);

		final ReferralCodeModel referralCodeModel = getReferralCodeModel(baseStoreModel, ownerCustomer);
		ownerCustomer.setReferralCode(referralCodeModel);
		modelService.save(ownerCustomer);
		LOG.info("ReferralCodeService: end generate method");
		return Optional.ofNullable(referralCodeModel);
	}

	/**
	 * Gets the referral code model.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @return the referral code model
	 */
	private ReferralCodeModel getReferralCodeModel(final BaseStoreModel baseStoreModel, final CustomerModel ownerCustomer)
	{
		final ReferralCodeModel referralCodeModel = modelService.create(ReferralCodeModel.class);
		referralCodeModel.setCode(generateCode());
		referralCodeModel.setMaxAmount(baseStoreModel.getReferralCodeMaxAmount());
		referralCodeModel.setValid(true);
		referralCodeModel.setCurrentAmount(0);
		referralCodeModel.setUnlimitedApplied(baseStoreModel.isReferralCodeUnlimitedAppliedEnable());
		referralCodeModel.setMaxNumberOfCustomer(baseStoreModel.getReferralCodeMaxCustomerApplied());
		referralCodeModel.setPercentage(baseStoreModel.getReferralCodePercentage());
		referralCodeModel.setAppliedCustomers(Collections.emptyList());
		referralCodeModel.setReferralCodeMaxAmountType(baseStoreModel.getReferralCodeMaxAmountType());
		referralCodeModel.setReferralCodeOwner(ownerCustomer);
		referralCodeModel.setFixedRewardAmount(baseStoreModel.getReferralCodeSenderRewardAmount());
		referralCodeModel.setSaveType(baseStoreModel.getSaveType());
		referralCodeModel.setReferralOrderRewardType(baseStoreModel.getReferralOrderRewardType());
		return referralCodeModel;
	}

	/**
	 * Generate by current base store.
	 *
	 * @param ownerCustomer
	 *           the owner customer
	 * @return the optional
	 * @throws ReferralCodeException
	 *            the referral code exception
	 */
	@Override
	public Optional<ReferralCodeModel> generateByCurrentBaseStore(final CustomerModel ownerCustomer) throws ReferralCodeException
	{
		LOG.info("ReferralCodeService: start generateByCurrentBaseStore method");
		return generate(ownerCustomer, baseStoreService.getCurrentBaseStore());
	}

	/**
	 * Generate by current base store and current customer.
	 *
	 * @return the optional
	 * @throws ReferralCodeException
	 *            the referral code exception
	 */
	@Override
	public Optional<ReferralCodeModel> generateByCurrentBaseStoreAndCurrentCustomer() throws ReferralCodeException
	{
		LOG.info("ReferralCodeService: start generateByCurrentBaseStoreAndCurrentCustomer method");

		return Objects.isNull(baseStoreService.getCurrentBaseStore()) ? Optional.empty()
				: generate(getCurrentCustomer(), baseStoreService.getCurrentBaseStore());
	}

	/**
	 * Apply customer.
	 *
	 * @param referralCode
	 *           the referral code
	 * @param appliedCustomer
	 *           the applied customer
	 * @throws ReferralCodeException
	 *            the referral code exception
	 */
	@Override
	public boolean applyCustomer(final BaseStoreModel baseStore, final String referralCode, final CustomerModel appliedCustomer)
			throws ReferralCodeException
	{
		validator.validateAppliedCustomer(appliedCustomer);

		if (!canApplyCustomer(baseStore, referralCode))
		{
			LOG.info(String.format("can't apply referral code with code = %s to the customer  with customer Id = %s", referralCode,
					appliedCustomer.getCustomerID()));
			return false;
		}
		final Optional<ReferralCodeModel> code = getByCode(referralCode);

		appliedCustomer.setAppliedReferralCode(code.get());
		LOG.info(String.format("apply referral code with code = %s to the customer  with customer Id = %s", referralCode,
				appliedCustomer.getCustomerID()));
		modelService.save(appliedCustomer);
		code.get().setIsApplied(true);
		modelService.save(code.get());
		sendRegistrationEmailToOwnerCustomer(code, appliedCustomer);
		addNewAppliedReward(baseStore, appliedCustomer);
		return true;

	}

	public boolean applyCustomerByCurrentBaseStore(final String referralCode, final CustomerModel appliedCustomer)
			throws ReferralCodeException
	{
		final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
		return applyCustomer(currentBaseStore, referralCode, appliedCustomer);


	}

	public boolean applyCustomerByCurrentBaseStoreAndCurrentCustomer(final String referralCode) throws ReferralCodeException
	{

		final CustomerModel currentCustomer = getCurrentCustomer();
		return applyCustomerByCurrentBaseStore(referralCode, currentCustomer);
	}


	/**
	 *
	 */
	private void sendRegistrationEmailToOwnerCustomer(final Optional<ReferralCodeModel> referralCode,
			final CustomerModel appliedCustomer)
	{
		if (referralCode.isEmpty())
		{
			return;
		}
		if (Objects.isNull(appliedCustomer))
		{
			return;
		}
		final Optional<CustomerModel> customerByReferralCode = customerService.getCustomerByReferralCode(referralCode.get());

		if (customerByReferralCode.isPresent())
		{
			final CustomCustomerRegistrationEvent event = new CustomCustomerRegistrationEvent();
			eventService
					.publishEvent(initializeCustomCustomerRegistrationEvent(event, customerByReferralCode.get(), appliedCustomer));
		}

	}

	protected AbstractCommerceUserEvent initializeCustomCustomerRegistrationEvent(final AbstractCommerceUserEvent event,
			final CustomerModel customerModel, final CustomerModel appliedCustomer)
	{
		event.setBaseStore(getBaseStoreService().getCurrentBaseStore());
		event.setSite(baseSiteService.getCurrentBaseSite());
		event.setCustomer(customerModel);
		if (event instanceof CustomCustomerRegistrationEvent)
		{
			((CustomCustomerRegistrationEvent) event).setAppliedCustomer(appliedCustomer);
		}
		event.setLanguage(commonI18NService.getCurrentLanguage());
		event.setCurrency(commonI18NService.getCurrentCurrency());
		return event;
	}

	/**
	 * Can apply customer.
	 *
	 * @param referralCode
	 *           the referral code
	 * @return true, if successful
	 */
	@Override
	public boolean canApplyCustomer(final BaseStoreModel baseStore, final String referralCode)
	{
		LOG.info("ReferralCodeService: start canApplyCustomer method");

		if (!isEnabled(baseStore))
		{
			LOG.info("referral code feature is not enabled in the current base store");
			return false;
		}

		final Optional<ReferralCodeModel> code = getByCode(referralCode);
		if (code.isEmpty() || !(code.get().isValid()))
		{
			LOG.info(String.format("can't apply customer using referral code = %s", referralCode));
			LOG.info(String.format("code is empty or not valid, code = %s", referralCode));
			return false;
		}
		final ReferralCodeModel referralCodeModel = code.get();
		final ReferralCodeConfig referralCodeConfig = getReferralCodeConfig(baseStore, referralCodeModel);

		if (!referralCodeConfig.isUnlimitedAppliedEnable() && !CollectionUtils.isEmpty(referralCodeModel.getAppliedCustomers())
				&& referralCodeModel.getAppliedCustomers().size() == referralCodeModel.getMaxNumberOfCustomer())
		{
			LOG.info(String.format("can't apply customer using referral code = %s", referralCode));
			LOG.info(String.format("max number of applied customer has reached, current number = %d and max number = %d",
					referralCodeModel.getAppliedCustomers().size(), referralCodeModel.getMaxNumberOfCustomer()));
			return false;

		}
		return true;

	}

	@Override
	public boolean canApplyCustomerByCurrentBaseStore(final String referralCode)
	{
		final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
		return canApplyCustomer(currentBaseStore, referralCode);
	}

	/**
	 * Apply order.
	 *
	 * @param orderModel
	 *           the order model
	 * @throws ReferralCodeException
	 *            the referral code exception
	 */
	@Override
	public boolean applyOrder(final AbstractOrderModel orderModel) throws ReferralCodeException
	{

		LOG.info("ReferralCodeService: start applyOrder method");
		Preconditions.checkArgument(!Objects.isNull(orderModel), ABSTRACT_ORDER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(orderModel.getStore()), STORE_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(orderModel.getUser()), USER_ORDER_NULL_MSG);
		Preconditions.checkArgument((orderModel.getUser() instanceof CustomerModel), USER_IS_NOT_CUSTOMER);

		final BaseStoreModel baseStore = orderModel.getStore();
		final CustomerModel appliedCustomer = (CustomerModel) orderModel.getUser();
		final ReferralCodeModel appliedReferralCode = appliedCustomer.getAppliedReferralCode();


		if (!canApplyOrder(orderModel))
		{
			final ReferralCodeException ex = new ReferralCodeException(ReferralCodeExceptionType.ORDER_CANT_APPLY,
					ReferralCodeExceptionType.ORDER_CANT_APPLY.getMsg());
			LOG.error(ex.getMessage());
			throw ex;
		}



		//TODO create method to get boolean mnasro

		final double profit = calculateProfit(orderModel);

		if (profit == 0)
		{
			LOG.info("profit = 0 ");
			return false;
		}



		final Optional<CustomerModel> customerByReferralCode = customerService.getCustomerByReferralCode(appliedReferralCode);
		if (customerByReferralCode.isEmpty())
		{
			final ReferralCodeException ex = new ReferralCodeException(ReferralCodeExceptionType.INVALID_REFERRAL_CODE,
					ReferralCodeExceptionType.INVALID_REFERRAL_CODE.getMsg());
			LOG.error(ex.getMessage());
			throw ex;
		}
		LOG.info(String.format("add store credit to customer with customer Id = %s", customerByReferralCode.get()));

		storeCreditService.addStoreCredit(customerByReferralCode.get(), baseStore, BigDecimal.valueOf(profit));
		createReferralHistory(appliedReferralCode, profit, appliedCustomer);
		final ReferralCodeConfig referralCodeConfig = getReferralCodeConfig(orderModel.getStore(), appliedReferralCode);

		if (referralCodeConfig.getMaxAmountType() == ReferralCodeMaxAmountType.ALL)
		{
			LOG.info(String.format("update referral code current amount to become %f",
					appliedReferralCode.getCurrentAmount() + profit));
			appliedReferralCode.setCurrentAmount(appliedReferralCode.getCurrentAmount() + profit);
		}
		modelService.saveAll(appliedReferralCode);
		LOG.info("code applied to customer");
		LOG.info("ReferralCodeService: end applyOrder method");
		return true;
	}

	/**
	 * Can apply order.
	 *
	 * @param orderModel
	 *           the order model
	 * @return true, if successful
	 */
	@Override
	public boolean canApplyOrder(final AbstractOrderModel orderModel)
	{
		LOG.info("ReferralCodeService: start canApplyOrder method");
		validator.validateOrder(orderModel);

		final BaseStoreModel baseStore = orderModel.getStore();

		if (!isEnabled(baseStore))
		{
			LOG.info("referral code feature is not enabled in the current base store");
			return false;
		}

		final CustomerModel appliedCustomer = (CustomerModel) orderModel.getUser();

		if (Objects.isNull(appliedCustomer.getAppliedReferralCode()))
		{
			LOG.info(String.format("customer with customer Id = %s doesn't have an appliead referral code",
					appliedCustomer.getCustomerID()));
			return false;
		}

		final ReferralCodeModel appliedReferralCode = appliedCustomer.getAppliedReferralCode();

		if (!appliedReferralCode.isValid())
		{
			LOG.info(String.format("referral code with code = %s is not valid", appliedReferralCode.getCode()));
			return false;
		}

		final double profit = calculateProfit(orderModel);
		LOG.info(String.format("profit = %f", profit));
		if (profit <= 0)
		{
			return false;
		}


		if (isSingleOrder(orderModel))
		{
			return getCustomerService().isFirstOrder(appliedCustomer);
		}
		else
		{
			return true;
		}
	}

	/**
	 *
	 */
	private boolean isSingleOrder(final AbstractOrderModel orderModel)
	{
		return !(orderModel == null || orderModel.getStore() == null || orderModel.getStore().getReferralOrderRewardType() == null
				|| ReferralOrderRewardType.MULTIPLE.equals(orderModel.getStore().getReferralOrderRewardType()));
	}

	/**
	 * Checks if is enabled.
	 *
	 * @param baseStore
	 *           the base store
	 * @return true, if is enabled
	 */
	@Override
	public boolean isEnabled(final BaseStoreModel baseStore)
	{
		LOG.info("ReferralCodeService: start isEnabled method");
		if (Objects.isNull(baseStore))
		{
			final IllegalArgumentException ex = new IllegalArgumentException(BASE_STORE_NULL_MSG);
			LOG.error(ex.getMessage());
			throw ex;
		}

		LOG.info("ReferralCodeService: end isEnabled method");
		return baseStore.isReferralCodeEnable();
	}

	/**
	 * Gets the referral code by current customer.
	 *
	 * @return the referral code by current customer
	 * @throws ReferralCodeException
	 *            the referral code exception
	 */
	@Override
	public Optional<ReferralCodeModel> getReferralCodeByCurrentCustomer() throws ReferralCodeException
	{
		LOG.info("ReferralCodeService: start getReferralCodeCurrentCustomer method");
		final CustomerModel currentCustomer = getCurrentCustomer();
		getModelService().refresh(currentCustomer);
		return Optional.ofNullable(currentCustomer.getReferralCode());
	}

	@Override
	public Optional<ReferralCodeModel> getAppliedReferralCodeByCurrentCustomer() throws ReferralCodeException
	{
		LOG.info("ReferralCodeService: start getReferralCodeCurrentCustomer method");
		final CustomerModel currentCustomer = getCurrentCustomer();
		getModelService().refresh(currentCustomer);
		return Optional.ofNullable(currentCustomer.getAppliedReferralCode());
	}

	/**
	 * Creates the referral history.
	 *
	 * @param appliedReferralCode
	 *           the applied referral code
	 * @param profit
	 *           the profit
	 * @param appliedCustomer
	 *           the applied customer
	 */
	protected void createReferralHistory(final ReferralCodeModel appliedReferralCode, final double profit,
			final CustomerModel appliedCustomer)
	{
		LOG.info("ReferralCodeService: start createReferralHistory method");
		final AppliedReferralHistoryModel appliedReferralHistoryModel = modelService.create(AppliedReferralHistoryModel.class);
		appliedReferralHistoryModel.setAmount(profit);
		appliedReferralHistoryModel.setAppliedCustomer(appliedCustomer);
		appliedReferralHistoryModel.setAppliedCustomerId(appliedCustomer.getCustomerID());
		appliedReferralHistoryModel.setReferralCodeModel(appliedReferralCode);
		appliedReferralHistoryModel.setReferralCode(appliedReferralCode.getCode());
		appliedReferralHistoryModel.setCreationDate(new Date());
		modelService.save(appliedReferralHistoryModel);
		LOG.info(String.format("applied referral history is saved. amount = %f, appliedCustomer = %s, referralCode = %s.", profit,
				appliedCustomer.toString(), appliedReferralCode.toString()));
		LOG.info("ReferralCodeService: end createReferralHistory method");
	}

	/**
	 * Calculate profit.
	 *
	 * @param orderModel
	 *           the order model
	 * @return the double
	 */
	protected double calculateProfit(final AbstractOrderModel orderModel)
	{
		LOG.info("ReferralCodeService: start calculateProfit method");
		final BaseStoreModel baseStore = orderModel.getStore();
		final CustomerModel appliedCustomer = (CustomerModel) orderModel.getUser();
		final ReferralCodeModel appliedReferralCode = appliedCustomer.getAppliedReferralCode();
		final ReferralCodeConfig referralCodeConfig = getReferralCodeConfig(orderModel.getStore(), appliedReferralCode);

		double profit = calculateProfitPerOrder(appliedReferralCode, baseStore, orderModel.getTotalPrice());
		LOG.info(String.format("profit per order is %f", profit));

		if (Objects.isNull(appliedReferralCode.getReferralCodeMaxAmountType()))
		{
			LOG.error(String.format("referral code with code = %s doesn't have a maxAmountType", appliedReferralCode.getCode()));
			profit = 0;
			return profit;
		}

		switch (referralCodeConfig.getMaxAmountType())
		{
			case ALL:
				LOG.info("ReferralCodeMaxAmountType is ALL");
				profit = (profit + appliedReferralCode.getCurrentAmount()) <= referralCodeConfig.getMaxAmount() ? profit
						: (referralCodeConfig.getMaxAmount() - appliedReferralCode.getCurrentAmount());
				break;

			case PERUSER:
				LOG.info("ReferralCodeMaxAmountType is PERUSER");
				final double customerTotalAmountPerUser = calculateCustomerTotalAmountPerUser(appliedReferralCode.getCode(),
						appliedCustomer);

				profit = (profit + customerTotalAmountPerUser) <= referralCodeConfig.getMaxAmount() ? profit
						: (referralCodeConfig.getMaxAmount() - customerTotalAmountPerUser);
				break;

			default:

		}

		LOG.info(String.format("final profit is %f", profit));
		return profit;
	}

	/**
	 * Calculate customer total amount per user.
	 *
	 * @param code
	 *           the code
	 * @param appliedCustomer
	 *           the applied customer
	 * @return the double
	 */
	protected double calculateCustomerTotalAmountPerUser(final String code, final CustomerModel appliedCustomer)
	{
		LOG.info("ReferralCodeService: start calculateCustomerTotalAmountPerUser method");
		LOG.info(String.format("get histories for referral code %s", code));
		final List<AppliedReferralHistoryModel> histories = appliedReferralHistoryService.getByReferralCodeAndAppliedCustomer(code,
				appliedCustomer);

		double total = 0;

		for (final AppliedReferralHistoryModel history : histories)
		{
			total += history.getAmount();
		}
		LOG.info(String.format("total amount by customer with customer Id = %s is %f", appliedCustomer.getCustomerID(), total));
		return total;
	}

	/**
	 * Calculate profit per order.
	 *
	 * @param appliedReferralCode
	 *           the applied referral code
	 * @param baseStore
	 *           the base store
	 * @param subTotal
	 *           the sub total
	 * @return the double
	 */
	protected double calculateProfitPerOrder(final ReferralCodeModel appliedReferralCode, final BaseStoreModel baseStore,
			final Double subTotal)
	{
		LOG.info("ReferralCodeService: start calculateProfitPerOrder method");
		final ReferralCodeConfig referralCodeConfig = getReferralCodeConfig(baseStore, appliedReferralCode);
		double profit = SaveType.FIXEDAMOUNT.equals(referralCodeConfig.getSaveType()) ? referralCodeConfig.getSenderRewardAmount()
				: (subTotal * referralCodeConfig.getPercentage()) / 100;

		LOG.info(String.format("profit before per order validation is %f", profit));
		if (referralCodeConfig.isMaxAmountPerOrderEnable())
		{
			LOG.info("referralCodeMaxAmountPerOrder is enabled");
			profit = profit > referralCodeConfig.getMaxAmountPerOrder() ? referralCodeConfig.getMaxAmountPerOrder() : profit;
		}
		LOG.info(String.format("profit after per order validation is %f", profit));
		return profit;
	}

	/**
	 * Generate code.
	 *
	 * @return the string
	 */
	protected String generateCode()
	{
		LOG.info("ReferralCodeService: start generateCode method");
		final Object generate = keyGenerator.generate();
		LOG.info(String.format("generated code is %s", (String) generate));
		return (String) generate;
	}

	/**
	 * Gets the current customer.
	 *
	 * @return the current customer
	 * @throws ReferralCodeException
	 *            the referral code exception
	 */
	protected CustomerModel getCurrentCustomer() throws ReferralCodeException
	{
		final UserModel currentUser = userService.getCurrentUser();
		if (Objects.isNull(currentUser))
		{
			final ReferralCodeException ex = new ReferralCodeException(ReferralCodeExceptionType.CURRENT_CUSTOMER_NOT_FOUND,
					ReferralCodeExceptionType.CURRENT_CUSTOMER_NOT_FOUND.getMsg());
			LOG.error(ex.getMessage());
			throw ex;
		}
		if (!(currentUser instanceof CustomerModel))
		{
			final ReferralCodeException ex = new ReferralCodeException(ReferralCodeExceptionType.NOT_A_CUSTOMER,
					ReferralCodeExceptionType.NOT_A_CUSTOMER.getMsg());
			LOG.error(ex.getMessage());
			throw ex;
		}

		return (CustomerModel) currentUser;
	}


	private boolean addNewAppliedReward(final BaseStoreModel baseStore, final CustomerModel customer)
	{
		if (Objects.isNull(baseStore))
		{
			final IllegalArgumentException ex = new IllegalArgumentException(BASE_STORE_NULL_MSG);
			LOG.error(ex.getMessage());
			throw ex;
		}
		if (Objects.isNull(customer))
		{
			final IllegalArgumentException ex = new IllegalArgumentException("customer is null");
			LOG.error(ex.getMessage());
			throw ex;
		}

		if (!baseStore.isReferralCodeNewAppliedRewardEnabled())
		{
			LOG.info("new applied reward disabled.");
			return false;
		}

		if (baseStore.getReferralCodeNewAppliedRewardAmount() <= 0)
		{
			LOG.info("invalid new applied reward amount.");
		}
		final double amount = baseStore.getReferralCodeNewAppliedRewardAmount();
		storeCreditService.addStoreCredit(customer, baseStore, BigDecimal.valueOf(amount));
		try
		{
			if (baseStore.isReferralCodeNewAppliedRewardEmailEnabled())
			{
				sendNewAppliedCustomerByCurrentSiteAndStore(customer);
			}
		}
		catch (final ReferralCodeException e)
		{
			LOG.error(e.getMessage());
		}
		LOG.info(String.format("%f is added as reward to customer %s", amount, customer.getCustomerID()));
		return true;
	}



	@Override
	public void shareByEmailsByCurrentSiteAndStore(final List<String> toEmails, final boolean seperateEmail)
			throws ReferralCodeException
	{
		shareByEmails(getCmsSiteService().getCurrentSite(), getBaseStoreService().getCurrentBaseStore(), toEmails, seperateEmail);
	}

	@Override
	public void shareByEmails(final CMSSiteModel cmsSiteModel, final BaseStoreModel baseStoreModel, final List<String> toEmails,
			final boolean seperateEmail) throws ReferralCodeException
	{
		if (Objects.isNull(baseStoreModel))
		{
			throw new IllegalArgumentException("baseStoreModel is null");
		}

		if (!baseStoreModel.isReferralCodeShareByEmailEnable())
		{
			LOG.warn("referral code share by email is disabled");
			return;
		}

		if (toEmails == null || toEmails.isEmpty())
		{

			LOG.error(ReferralCodeExceptionType.EMAILS_TO_LIST_IS_EMPTY.getMsg());
			throw new ReferralCodeException(ReferralCodeExceptionType.EMAILS_TO_LIST_IS_EMPTY,
					ReferralCodeExceptionType.EMAILS_TO_LIST_IS_EMPTY.getMsg());
		}
		final CustomerModel currentCustomer = getCurrentCustomer();
		if (Objects.isNull(currentCustomer.getReferralCode()))
		{
			LOG.error(ReferralCodeExceptionType.CUSTOMER_DOESNT_HAVE_A_CODE.getMsg());
			throw new ReferralCodeException(ReferralCodeExceptionType.EMAILS_TO_LIST_IS_EMPTY,
					ReferralCodeExceptionType.EMAILS_TO_LIST_IS_EMPTY.getMsg());
		}
		final ReferralCodeShareEmailEvent referralCodeShareEmailEvent = new ReferralCodeShareEmailEvent(currentCustomer, toEmails);
		referralCodeShareEmailEvent.setSite(cmsSiteModel);
		referralCodeShareEmailEvent.setLanguage(currentCustomer.getSessionLanguage());
		referralCodeShareEmailEvent.setBaseStore(baseStoreModel);

		if (seperateEmail)
		{
			for (final String email : toEmails)
			{
				referralCodeShareEmailEvent.setToEmails(Arrays.asList(email));
				eventService.publishEvent(referralCodeShareEmailEvent);
			}
		}
		else
		{
			referralCodeShareEmailEvent.setToEmails(toEmails);
			eventService.publishEvent(referralCodeShareEmailEvent);
		}

	}


	protected void sendNewAppliedCustomerByCurrentSiteAndStore(final CustomerModel customer) throws ReferralCodeException
	{
		final ReferralNewAppliedEvent event = new ReferralNewAppliedEvent();
		event.setSite(getCmsSiteService().getCurrentSite());
		event.setLanguage(commonI18NService.getCurrentLanguage());
		event.setBaseStore(getBaseStoreService().getCurrentBaseStore());
		event.setCustomer(customer);

		eventService.publishEvent(event);
	}

	private ReferralCodeConfig getReferralCodeConfig(final BaseStoreModel baseStoreModel,
			final ReferralCodeModel referralCodeModel)
	{
		final ReferralCodeConfig referralCodeConfig = new ReferralCodeConfig();
		final ReferralConfigType referralConfigType = baseStoreModel.getReferralConfigType() == null ? ReferralConfigType.STORE
				: baseStoreModel.getReferralConfigType();
		if (ReferralConfigType.STORE.equals(referralConfigType))
		{
			referralCodeConfig.setUnlimitedAppliedEnable(baseStoreModel.isReferralCodeUnlimitedAppliedEnable());
			referralCodeConfig.setPercentage(baseStoreModel.getReferralCodePercentage());
			referralCodeConfig.setMaxAmount(baseStoreModel.getReferralCodeMaxAmount());
			referralCodeConfig.setSenderRewardAmount(baseStoreModel.getReferralCodeSenderRewardAmount());
			referralCodeConfig.setMaxCustomerApplied(baseStoreModel.getReferralCodeMaxCustomerApplied());
			referralCodeConfig.setMaxAmountType(baseStoreModel.getReferralCodeMaxAmountType());
			referralCodeConfig.setSaveType(baseStoreModel.getSaveType());
		}
		else
		{

			referralCodeConfig.setSenderRewardAmount(referralCodeModel.getFixedRewardAmount());
			referralCodeConfig.setMaxCustomerApplied(referralCodeModel.getMaxNumberOfCustomer());
			referralCodeConfig.setPercentage(referralCodeModel.getPercentage());
			referralCodeConfig.setMaxAmount(referralCodeModel.getMaxAmount());
			referralCodeConfig.setMaxAmountType(referralCodeModel.getReferralCodeMaxAmountType());
			referralCodeConfig.setUnlimitedAppliedEnable(referralCodeModel.isUnlimitedApplied());
			referralCodeConfig.setSaveType(referralCodeModel.getSaveType());
		}
		referralCodeConfig.setEnable(baseStoreModel.isReferralCodeEnable());
		referralCodeConfig.setMaxAmountPerOrderEnable(baseStoreModel.isReferralCodeMaxAmountPerOrderEnable());
		referralCodeConfig.setShareByEmailEnable(baseStoreModel.isReferralCodeShareByEmailEnable());
		referralCodeConfig.setNewAppliedRewardEnabled(baseStoreModel.isReferralCodeNewAppliedRewardEnabled());
		referralCodeConfig.setNewAppliedRewardEmailEnabled(baseStoreModel.isReferralCodeNewAppliedRewardEmailEnabled());
		referralCodeConfig.setSenderRewardEnabled(baseStoreModel.isReferralCodeSenderRewardEnabled());
		referralCodeConfig.setSenderRewardEmailEnabled(baseStoreModel.isReferralCodeSenderRewardEmailEnabled());
		//			referralCodeConfig.setShareEmailYoutubeId(baseStoreModel.getReferralCodeShareEmailYoutubeId());
		referralCodeConfig.setNewAppliedRewardAmount(baseStoreModel.getReferralCodeNewAppliedRewardAmount());
		referralCodeConfig.setMaxAmountPerOrder(baseStoreModel.getReferralCodeMaxAmountPerOrder());

		return referralCodeConfig;

	}
}
