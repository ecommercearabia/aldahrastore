/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrareferralcode.service;

import de.hybris.platform.core.model.user.CustomerModel;

import java.util.Optional;

import com.aldahra.aldahrareferralcode.model.ReferralCodeModel;


/**
 * @author mohammedbaker The Interface ReferralCodeCustomerService.
 */
public interface ReferralCodeCustomerService
{

	/**
	 * Gets the customer by referral code.
	 *
	 * @param referralCode
	 *           the referral code
	 * @return the customer by referral code
	 */
	public Optional<CustomerModel> getCustomerByReferralCode(ReferralCodeModel referralCode);

	/**
	 * Checks if it is the first order.
	 *
	 * @param appliedCustomer
	 *           the applied customer
	 * @return true, if is first order
	 */
	public boolean isFirstOrder(final CustomerModel appliedCustomer);


}
