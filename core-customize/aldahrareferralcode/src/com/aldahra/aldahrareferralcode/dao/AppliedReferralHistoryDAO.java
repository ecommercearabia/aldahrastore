/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrareferralcode.dao;

import java.util.List;

import com.aldahra.aldahrareferralcode.model.AppliedReferralHistoryModel;

/**
 * @author mohammedbaker The Interface AppliedReferralHistoryDAO.
 */
public interface AppliedReferralHistoryDAO
{

	/**
	 * Gets the all.
	 *
	 * @return the all
	 */
	public List<AppliedReferralHistoryModel> getAll();

	/**
	 * Gets the by referral code.
	 *
	 * @param code
	 *           the code
	 * @return the by referral code
	 */
	public List<AppliedReferralHistoryModel> getByReferralCode(String code);

	/**
	 * Gets the by referral code and applied customer.
	 *
	 * @param referralCode
	 *           the referral code
	 * @param appliedCustomerId
	 *           the applied customer id
	 * @return the by referral code and applied customer
	 */
	public List<AppliedReferralHistoryModel> getByReferralCodeAndAppliedCustomer(String referralCode,
			String appliedCustomerId);
}
