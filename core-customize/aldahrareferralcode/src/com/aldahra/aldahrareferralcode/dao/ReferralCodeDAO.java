/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrareferralcode.dao;

import java.util.List;
import java.util.Optional;

import com.aldahra.aldahrareferralcode.model.ReferralCodeModel;


/**
 * @author mohammedbaker The Interface ReferralCodeDAO.
 */
public interface ReferralCodeDAO
{

	/**
	 * Gets the all.
	 *
	 * @return the all
	 */
	public List<ReferralCodeModel> getAll();

	/**
	 * Gets the by code.
	 *
	 * @param code
	 *           the code
	 * @return the by code
	 */
	public Optional<ReferralCodeModel> getByCode(String code);

}
