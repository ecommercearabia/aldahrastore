/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrareferralcode.dao;

import de.hybris.platform.core.model.user.CustomerModel;

import java.util.Optional;

import com.aldahra.aldahrareferralcode.model.ReferralCodeModel;

/**
 * @author mohammedbaker The Interface ReferralCodeCustomerDAO.
 */
public interface ReferralCodeCustomerDAO
{

	/**
	 * Gets the by referral code.
	 *
	 * @param referralCode
	 *           the referral code
	 * @return the by referral code
	 */
	public Optional<CustomerModel> getByReferralCode(ReferralCodeModel referralCode);
}
