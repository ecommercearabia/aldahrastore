/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrareferralcode.dao.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.List;

import javax.annotation.Resource;

import com.aldahra.aldahrareferralcode.dao.AppliedReferralHistoryDAO;
import com.aldahra.aldahrareferralcode.model.AppliedReferralHistoryModel;


/**
 * @author mohammedbaker The Class DefaultAppliedReferralHistoryDAO.
 */
public class DefaultAppliedReferralHistoryDAO implements AppliedReferralHistoryDAO
{

	/** The flexible search service. */
	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	/** The base select query. */
	private final String BASE_SELECT_QUERY = "SELECT {p:" + AppliedReferralHistoryModel.PK + "} " + "FROM {"
			+ AppliedReferralHistoryModel._TYPECODE + " AS p} ";

	/** The code key. */
	private final String CODE_KEY = "code";

	/** The customer key. */
	private final String CUSTOMER_KEY = "customerId";

	/**
	 * Gets the all.
	 *
	 * @return the all
	 */
	@Override
	public List<AppliedReferralHistoryModel> getAll()
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(BASE_SELECT_QUERY);
		return flexibleSearchService.<AppliedReferralHistoryModel> search(query).getResult();
	}

	/**
	 * Gets the by referral code.
	 *
	 * @param code
	 *           the code
	 * @return the by referral code
	 */
	@Override
	public List<AppliedReferralHistoryModel> getByReferralCode(final String code)
	{
		final String stringQuery = BASE_SELECT_QUERY + "WHERE " + "{p:" + AppliedReferralHistoryModel.REFERRALCODE + "}=?code";
		final FlexibleSearchQuery query = new FlexibleSearchQuery(stringQuery);
		query.addQueryParameter(CODE_KEY, code);
		return flexibleSearchService.<AppliedReferralHistoryModel> search(query).getResult();
	}

	/**
	 * Gets the by referral code and applied customer.
	 *
	 * @param referralCode
	 *           the referral code
	 * @param appliedCustomerId
	 *           the applied customer id
	 * @return the by referral code and applied customer
	 */
	@Override
	public List<AppliedReferralHistoryModel> getByReferralCodeAndAppliedCustomer(final String referralCode,
			final String appliedCustomerId)
	{
		final String stringQuery = BASE_SELECT_QUERY + " WHERE " + "{p:" + AppliedReferralHistoryModel.REFERRALCODE + "}=?code"
				+ " AND {p:" + AppliedReferralHistoryModel.APPLIEDCUSTOMERID + "}=?customerId";
		final FlexibleSearchQuery query = new FlexibleSearchQuery(stringQuery);
		query.addQueryParameter(CODE_KEY, referralCode);
		query.addQueryParameter(CUSTOMER_KEY, appliedCustomerId);
		return flexibleSearchService.<AppliedReferralHistoryModel> search(query).getResult();
	}



}
