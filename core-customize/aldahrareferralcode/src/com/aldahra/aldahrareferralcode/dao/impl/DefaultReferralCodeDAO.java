/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrareferralcode.dao.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.aldahra.aldahrareferralcode.dao.ReferralCodeDAO;
import com.aldahra.aldahrareferralcode.model.ReferralCodeModel;


/**
 * @author mohammedbaker The Class DefaultReferralCodeDAO.
 */
public class DefaultReferralCodeDAO implements ReferralCodeDAO
{

	/** The flexible search service. */
	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	/** The base select query. */
	private final String BASE_SELECT_QUERY = "SELECT {p:" + ReferralCodeModel.PK + "} " + "FROM {" + ReferralCodeModel._TYPECODE
			+ " AS p} ";

	/** The code key. */
	private final String CODE_KEY = "code";
	/* private final String OWNER_CODE_KEY = "codeOwner"; */

	/**
	 * Gets the all.
	 *
	 * @return the all
	 */
	@Override
	public List<ReferralCodeModel> getAll()
	{

		final FlexibleSearchQuery query = new FlexibleSearchQuery(BASE_SELECT_QUERY);
		return flexibleSearchService.<ReferralCodeModel> search(query).getResult();
	}

	/**
	 * Gets the by code.
	 *
	 * @param code
	 *           the code
	 * @return the by code
	 */
	@Override
	public Optional<ReferralCodeModel> getByCode(final String code)
	{
		final String queryString = BASE_SELECT_QUERY + "WHERE " + "{p:" + ReferralCodeModel.CODE + "}=?" + CODE_KEY;
		final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);
		query.addQueryParameter(CODE_KEY, code);
		final List<ReferralCodeModel> result = flexibleSearchService.<ReferralCodeModel> search(query).getResult();
		return Optional.ofNullable(CollectionUtils.isEmpty(result) ? null : result.get(0));
	}

}
