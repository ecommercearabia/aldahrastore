/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrareferralcode.dao.impl;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.aldahra.aldahrareferralcode.dao.ReferralCodeCustomerDAO;
import com.aldahra.aldahrareferralcode.model.ReferralCodeModel;


/**
 * @author mohammedbaker The Class DefaultReferralCodeCustomerDAO.
 */
public class DefaultReferralCodeCustomerDAO implements ReferralCodeCustomerDAO
{

	/** The flexible search service. */
	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	/** The base select query. */
	private final String BASE_SELECT_QUERY = "SELECT {p:" + CustomerModel.PK + "} " + "FROM {" + CustomerModel._TYPECODE
			+ " AS p} ";

	/** The referral code key. */
	private final String REFERRAL_CODE_KEY = "referralCode";

	/**
	 * Gets the by referral code.
	 *
	 * @param referralCode
	 *           the referral code
	 * @return the by referral code
	 */
	@Override
	public Optional<CustomerModel> getByReferralCode(final ReferralCodeModel referralCode)
	{
		final String stringQuery = BASE_SELECT_QUERY + "WHERE " + "{p:" + CustomerModel.REFERRALCODE + "}=?" + REFERRAL_CODE_KEY;
		final FlexibleSearchQuery query = new FlexibleSearchQuery(stringQuery);
		query.addQueryParameter(REFERRAL_CODE_KEY, referralCode);
		final List<CustomerModel> result = flexibleSearchService.<CustomerModel> search(query).getResult();
		return Optional.ofNullable(CollectionUtils.isEmpty(result) ? null : result.get(0));
	}

}
