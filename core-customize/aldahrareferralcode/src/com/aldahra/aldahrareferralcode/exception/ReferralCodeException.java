/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrareferralcode.exception;

import com.aldahra.aldahrareferralcode.exception.type.ReferralCodeExceptionType;


/**
 * @author mohammedbaker The Class ReferralCodeException.
 */
public class ReferralCodeException extends Exception
{

	/** The type. */
	private final ReferralCodeExceptionType type;

	/**
	 * Instantiates a new referral code exception.
	 *
	 * @param type
	 *           the type
	 * @param message
	 *           the message
	 */
	public ReferralCodeException(final ReferralCodeExceptionType type, final String message)
	{
		super(message);
		this.type = type;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public ReferralCodeExceptionType getType()
	{
		return type;
	}

}
