/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrareferralcode.exception.type;


/**
 * @author mohammedbaker The Enum ReferralCodeExceptionType.
 */
public enum ReferralCodeExceptionType
{

	/** The invalid max amount. */
	INVALID_MAX_AMOUNT("max amount is invalid"),
	/** The invalid current amount. */
	INVALID_CURRENT_AMOUNT("current amount is invalid"),
	/** The max amount reached. */
	MAX_AMOUNT_REACHED("current amount is greater than max amount"),
	/** The invalid percentage. */
	INVALID_PERCENTAGE("percentage is invlaid"),
	/** The customer has code. */
	CUSTOMER_HAS_CODE("customer already has a code"),
	/** The not a customer. */
	NOT_A_CUSTOMER("user is not a customer"),
	/** The invalid referral code. */
	INVALID_REFERRAL_CODE("referral code is invalid"),
	/** The max number of customer applied reached. */
	MAX_NUMBER_OF_CUSTOMER_APPLIED_REACHED("max number of customer applied reached"),
	/** The order cant apply. */
	ORDER_CANT_APPLY("order can't apply"),
	/** The current customer not found. */
	CURRENT_CUSTOMER_NOT_FOUND("current customer is null"), EMAILS_TO_LIST_IS_EMPTY(
			"to emails list is empty"), EMAILS_CC_LIST_IS_EMPTY(
					"cc emails list is empty"), EMAILS_BCC_LIST_IS_EMPTY(
							"bcc emails list is empty"), CUSTOMER_DOESNT_HAVE_A_CODE("customer doesn't have a code");

	/** The msg. */
	private final String msg;

	/**
	 * Instantiates a new referral code exception type.
	 *
	 * @param msg
	 *           the msg
	 */
	private ReferralCodeExceptionType(final String msg)
	{
		this.msg = msg;
	}

	/**
	 * Gets the msg.
	 *
	 * @return the msg
	 */
	public String getMsg()
	{
		return msg;
	}

}
