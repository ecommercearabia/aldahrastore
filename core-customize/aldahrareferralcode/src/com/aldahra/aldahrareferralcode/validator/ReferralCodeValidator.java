/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrareferralcode.validator;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Objects;

import org.apache.log4j.Logger;

import com.aldahra.aldahrareferralcode.exception.ReferralCodeException;
import com.aldahra.aldahrareferralcode.exception.type.ReferralCodeExceptionType;


/**
 * @author mohammedbaker The Class ReferralCodeValidator.
 */
public class ReferralCodeValidator
{

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(ReferralCodeValidator.class);

	/**
	 * Validate owner customer.
	 *
	 * @param ownerCustomer
	 *           the owner customer
	 */
	public void validateOwnerCustomer(final CustomerModel ownerCustomer)
	{
		if (Objects.isNull(ownerCustomer))
		{
			LOG.error("ReferralCodeValidator: ownerCustomer is null");
			throw new IllegalArgumentException("ownerCustomer is null");
		}
	}

	/**
	 * Validate applied customer.
	 *
	 * @param appliedCustomer
	 *           the applied customer
	 */
	public void validateAppliedCustomer(final CustomerModel appliedCustomer)
	{
		if (Objects.isNull(appliedCustomer))
		{
			LOG.error("ReferralCodeValidator: appliedCustomer is null");
			throw new IllegalArgumentException("appliedCustomer is null");
		}
	}

	/**
	 * Validate base store.
	 *
	 * @param baseStore
	 *           the base store
	 * @throws ReferralCodeException
	 *            the referral code exception
	 */
	public void validateBaseStore(final BaseStoreModel baseStore) throws ReferralCodeException
	{
		if (Objects.isNull(baseStore))
		{
			final IllegalArgumentException ex = new IllegalArgumentException("base store is null");
			LOG.error(ex.getMessage(), ex);
			throw ex;
		}

		if (baseStore.getReferralCodeMaxAmount() < 0)
		{
			final ReferralCodeException ex = new ReferralCodeException(ReferralCodeExceptionType.INVALID_MAX_AMOUNT,
					ReferralCodeExceptionType.INVALID_MAX_AMOUNT.getMsg());
			LOG.error(ex.getMessage(), ex);
			throw ex;
		}

		if (baseStore.getReferralCodePercentage() < 0 || baseStore.getReferralCodePercentage() > 100)
		{
			final ReferralCodeException ex = new ReferralCodeException(ReferralCodeExceptionType.INVALID_PERCENTAGE,
					ReferralCodeExceptionType.INVALID_CURRENT_AMOUNT.getMsg());
			LOG.error(ex.getMessage(), ex);
			throw ex;
		}
	}

	/**
	 * Validate order.
	 *
	 * @param order
	 *           the order
	 */
	public void validateOrder(final AbstractOrderModel order)
	{
		if (Objects.isNull(order))
		{
			final IllegalArgumentException ex = new IllegalArgumentException("order is null");
			LOG.error(ex.getMessage(), ex);
			throw ex;
		}
		if (Objects.isNull(order.getStore()))
		{
			final IllegalArgumentException ex = new IllegalArgumentException("order store is null");
			LOG.error(ex.getMessage(), ex);
			throw ex;
		}
		if (Objects.isNull(order.getUser()))
		{
			final IllegalArgumentException ex = new IllegalArgumentException("order user is null");
			LOG.error(ex.getMessage(), ex);
			throw ex;
		}

		if (Objects.isNull(order.getTotalPrice()))
		{
			final IllegalArgumentException ex = new IllegalArgumentException("order total price is null");
			LOG.error(ex.getMessage(), ex);
			throw ex;
		}

	}
}
