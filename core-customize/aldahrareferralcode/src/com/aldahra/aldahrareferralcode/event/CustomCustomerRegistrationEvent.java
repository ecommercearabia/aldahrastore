/**
 *
 */
package com.aldahra.aldahrareferralcode.event;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;
import de.hybris.platform.core.model.user.CustomerModel;


/**
 * @author mohammad-abumuahsien
 *
 */
public class CustomCustomerRegistrationEvent extends AbstractCommerceUserEvent<BaseSiteModel>
{

	private CustomerModel appliedCustomer;


	public CustomerModel getAppliedCustomer()
	{
		return appliedCustomer;
	}


	public void setAppliedCustomer(final CustomerModel appliedCustomer)
	{
		this.appliedCustomer = appliedCustomer;
	}


}
