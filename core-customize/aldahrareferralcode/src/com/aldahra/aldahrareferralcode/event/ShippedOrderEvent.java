/**
 *
 */
package com.aldahra.aldahrareferralcode.event;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;


/**
 * @author mohammad-abumuahsien
 *
 */
public class ShippedOrderEvent extends AbstractEvent
{
	private AbstractOrderModel order;

	/**
	 * @param process
	 */

	public ShippedOrderEvent(final AbstractOrderModel order)
	{

		this.order = order;

	}

	/**
	 * @return the order
	 */
	protected AbstractOrderModel getOrder()
	{
		return order;
	}

	/**
	 * @param order
	 *           the order to set
	 */
	protected void setOrder(final OrderModel order)
	{
		this.order = order;
	}



}
