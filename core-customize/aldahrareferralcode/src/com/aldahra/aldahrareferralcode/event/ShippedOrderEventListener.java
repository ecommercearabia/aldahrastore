/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrareferralcode.event;

import de.hybris.platform.acceleratorservices.email.EmailGenerationService;
import de.hybris.platform.acceleratorservices.site.AbstractAcceleratorSiteEventListener;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.Optional;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aldahra.Ordermanagement.model.OrderShippingProcessModel;
import com.aldahra.aldahrareferralcode.service.ReferralCodeCustomerService;


/**
 * Listener for customer registration events.
 *
 * @author mohammad-abumuhasien
 */
public class ShippedOrderEventListener extends AbstractAcceleratorSiteEventListener<ShippedOrderEvent>
{

	private static final Logger LOG = LoggerFactory.getLogger(ShippedOrderEventListener.class);

	private BusinessProcessService businessProcessService;
	@Resource(name = "emailGenerationService")
	private EmailGenerationService emailGenerationService;

	@Resource(name = "referralCodeCustomerService")
	private ReferralCodeCustomerService referralCodeCustomerService;

	private ModelService modelService;

	@Override
	protected SiteChannel getSiteChannelForEvent(final ShippedOrderEvent event)
	{
		final AbstractOrderModel order = event.getOrder();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order", order);
		final BaseSiteModel site = order.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order.site", site);
		return site.getChannel();
	}

	@Override
	protected void onSiteEvent(final ShippedOrderEvent event)
	{
		LOG.info("Start ShippedOrderEventListener .... ");
		final AbstractOrderModel orderModel = event.getOrder();

		final OrderShippingProcessModel orderProcessModel =(OrderShippingProcessModel) getBusinessProcessService().createProcess(
				"sendOrderShippedEmailProcess-" + orderModel.getCode() + "-" + System.currentTimeMillis(),
				"sendOrderShippedEmailProcess");

		orderProcessModel.setOrder((OrderModel) orderModel);
		final UserModel referralCodeOwner = getReferralCodeOwner(orderModel);
		if (referralCodeOwner != null)
		{
			LOG.info("referral Code Owner is not null .... ");
			orderProcessModel.setUser(referralCodeOwner);
		}

		//getModelService().save(orderProcessModel);

		getBusinessProcessService().startProcess(orderProcessModel);
		LOG.info("End ShippedOrderEventListener .... ");

	}

	/**
	 * @param orderModel
	 * @return
	 */
	private UserModel getReferralCodeOwner(final AbstractOrderModel orderModel)
	{
		LOG.info("Get ReferralCodeOwner .... ");

		final CustomerModel user = (CustomerModel) orderModel.getUser();
		final Optional<CustomerModel> customerByReferralCode = getReferralCodeCustomerService()
				.getCustomerByReferralCode(user.getAppliedReferralCode());


		return customerByReferralCode.isPresent() ? customerByReferralCode.get() : null;
	}

	/**
	 * @return the businessProcessService
	 */
	public BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	/**
	 * @param businessProcessService
	 *           the businessProcessService to set
	 */
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	/**
	 * @return the emailGenerationService
	 */
	public EmailGenerationService getEmailGenerationService()
	{
		return emailGenerationService;
	}

	/**
	 * @param emailGenerationService
	 *           the emailGenerationService to set
	 */
	public void setEmailGenerationService(final EmailGenerationService emailGenerationService)
	{
		this.emailGenerationService = emailGenerationService;
	}

	/**
	 * @return the referralCodeCustomerService
	 */
	protected ReferralCodeCustomerService getReferralCodeCustomerService()
	{
		return referralCodeCustomerService;
	}

	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}



}
