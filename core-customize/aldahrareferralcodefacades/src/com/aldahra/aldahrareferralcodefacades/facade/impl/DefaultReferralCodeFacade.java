/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrareferralcodefacades.facade.impl;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.aldahra.aldahrareferralcode.exception.ReferralCodeException;
import com.aldahra.aldahrareferralcode.model.ReferralCodeModel;
import com.aldahra.aldahrareferralcode.service.ReferralCodeService;
import com.aldahra.aldahrareferralcodefacades.ReferralCodeData;
import com.aldahra.aldahrareferralcodefacades.facade.ReferralCodeFacade;


/**
 * @author mohammedbaker The Class DefaultReferralCodeFacade.
 */
public class DefaultReferralCodeFacade implements ReferralCodeFacade
{

	/** The referral code service. */
	@Resource(name = "referralCodeService")
	private ReferralCodeService referralCodeService;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	/** The referral code converter. */
	@Resource(name = "referralCodeConverter")
	private Converter<ReferralCodeModel, ReferralCodeData> referralCodeConverter;

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(DefaultReferralCodeFacade.class);

	/**
	 * Gets the by owner customer.
	 *
	 * @param customerModel
	 *           the customer model
	 * @return the by owner customer
	 */
	@Override
	public Optional<ReferralCodeData> getByOwnerCustomer(final CustomerModel customerModel)
	{
		LOG.info("ReferralCodeFacade: start getByOwnerCustomer method");
		if (Objects.isNull(customerModel))
		{
			LOG.error("ReferralCodeFacade: customerModel is null");
			throw new IllegalArgumentException("customerModel is null");
		}
		final ReferralCodeModel referralCodeModel = customerModel.getReferralCode();
		if (Objects.isNull(referralCodeModel))
		{
			LOG.info(String.format("no referral code for customer %s", customerModel.getCustomerID()));
			return Optional.empty();
		}
		return Optional.ofNullable(referralCodeConverter.convert(referralCodeModel));
	}

	/**
	 * Gets the by current customer.
	 *
	 * @return the by current customer
	 */
	@Override
	public Optional<ReferralCodeData> getByCurrentCustomer()
	{
		LOG.info("ReferralCodeFacade: start getByCurrentCustomer method");
		Optional<ReferralCodeModel> referralCodeModel;
		try
		{
			referralCodeModel = referralCodeService.getReferralCodeByCurrentCustomer();
		}
		catch (final ReferralCodeException e)
		{
			LOG.error(e.getMessage(), e);
			return Optional.empty();
		}

		if (referralCodeModel.isEmpty())
		{
			LOG.info("no referral code for current customer");
			return Optional.empty();
		}
		return Optional.ofNullable(referralCodeConverter.convert(referralCodeModel.get()));
	}

	@Override
	public boolean isEnabledByCurrentBaseStore()
	{

		return isEnabled(baseStoreService.getCurrentBaseStore());
	}

	@Override
	public boolean isEnabled(final BaseStoreModel baseStore)
	{

		return referralCodeService.isEnabled(baseStore);
	}

	@Override
	public Optional<ReferralCodeModel> generate(final CustomerModel ownerCustomer, final BaseStoreModel baseStoreModel)
			throws ReferralCodeException
	{
		return referralCodeService.generate(ownerCustomer, baseStoreModel);
	}

	@Override
	public Optional<ReferralCodeModel> generateByCurrentBaseStore(final CustomerModel ownerCustomer) throws ReferralCodeException
	{
		return referralCodeService.generateByCurrentBaseStore(ownerCustomer);
	}

	@Override
	public Optional<ReferralCodeModel> generateByCurrentBaseStoreAndCurrentCustomer() throws ReferralCodeException
	{

		return referralCodeService.generateByCurrentBaseStoreAndCurrentCustomer();
	}

	@Override
	public void applyReferralCodeByCurrentCustomerAndBaseStore(final String referralCode)
	{
		try
		{
			referralCodeService.applyCustomerByCurrentBaseStoreAndCurrentCustomer(referralCode);
		}
		catch (final ReferralCodeException e)
		{
			LOG.error(e.getMessage(), e);
		}
	}

	@Override
	public void applyReferralCode(final CustomerModel customer, final BaseStoreModel baseStore, final String referralCode)
	{
		try
		{
			referralCodeService.applyCustomer(baseStore, referralCode, customer);
		}
		catch (final ReferralCodeException e)
		{
			LOG.error(e.getMessage(), e);
		}

	}

	@Override
	public void applyReferralCodeByCurrentBaseStore(final CustomerModel customer, final String referralCode)
	{
		try
		{
			referralCodeService.applyCustomerByCurrentBaseStore(referralCode, customer);
		}
		catch (final ReferralCodeException e)
		{
			LOG.error(e.getMessage(), e);
		}

	}

	@Override
	public Optional<ReferralCodeData> getAppliedReferralCodeByCurrentCustomer()
	{
		LOG.info("ReferralCodeFacade: start getAppliedReferralCodeByCurrentCustomer method");
		Optional<ReferralCodeModel> referralCodeModel;
		try
		{
			referralCodeModel = referralCodeService.getAppliedReferralCodeByCurrentCustomer();
		}
		catch (final ReferralCodeException e)
		{
			LOG.error(e.getMessage(), e);
			return Optional.empty();
		}

		if (referralCodeModel.isEmpty())
		{
			LOG.info("no applied referral code for current customer");
			return Optional.empty();
		}
		return Optional.ofNullable(referralCodeConverter.convert(referralCodeModel.get()));
	}

}
