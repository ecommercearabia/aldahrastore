/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrareferralcodefacades.facade;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.aldahra.aldahrareferralcode.exception.ReferralCodeException;
import com.aldahra.aldahrareferralcode.model.ReferralCodeModel;
import com.aldahra.aldahrareferralcodefacades.ReferralCodeData;


/**
 * @author mohammedbaker The Interface ReferralCodeFacade.
 */
public interface ReferralCodeFacade
{

	/**
	 * Gets the by owner customer.
	 *
	 * @param customerModel
	 *           the customer model
	 * @return the by owner customer
	 */
	public Optional<ReferralCodeData> getByOwnerCustomer(CustomerModel customerModel);

	/**
	 * Gets the by current customer.
	 *
	 * @return the by current customer
	 */
	public Optional<ReferralCodeData> getByCurrentCustomer();

	public Optional<ReferralCodeData> getAppliedReferralCodeByCurrentCustomer();
	/**
	 * Checks if is enabled.
	 *
	 * @param baseStore
	 *           the base store
	 * @return true, if is enabled
	 */
	public boolean isEnabledByCurrentBaseStore();

	/**
	 * Checks if is enabled.
	 *
	 * @param baseStore
	 *           the base store
	 * @return true, if is enabled
	 */
	public boolean isEnabled(BaseStoreModel baseStore);

	public Optional<ReferralCodeModel> generate(CustomerModel ownerCustomer, BaseStoreModel baseStoreModel)
			throws ReferralCodeException;

	/**
	 * Generate by current base store.
	 *
	 * @param ownerCustomer
	 *           the owner customer
	 * @return the optional
	 * @throws ReferralCodeException
	 *            the referral code exception
	 */
	public Optional<ReferralCodeModel> generateByCurrentBaseStore(CustomerModel ownerCustomer) throws ReferralCodeException;

	/**
	 * Generate by current base store and current customer.
	 *
	 * @return the optional
	 * @throws ReferralCodeException
	 *            the referral code exception
	 */
	public Optional<ReferralCodeModel> generateByCurrentBaseStoreAndCurrentCustomer() throws ReferralCodeException;

	public void applyReferralCodeByCurrentCustomerAndBaseStore(String referralCode);

	public void applyReferralCode(CustomerModel customer, BaseStoreModel baseStore, String referralCode);

	public void applyReferralCodeByCurrentBaseStore(CustomerModel customer, String referralCode);

}
