/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrareferralcodefacades.populator;

import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Objects;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.aldahra.aldahrareferralcode.model.AppliedReferralHistoryModel;
import com.aldahra.aldahrareferralcodefacades.AppliedReferralHistoryData;


/**
 * @author mohammedbaker The Class AppliedReferralHistoryPopulator.
 */
public class AppliedReferralHistoryPopulator implements Populator<AppliedReferralHistoryModel, AppliedReferralHistoryData>
{

	/** The customer converter. */
	@Resource(name = "customerConverter")
	private Converter<CustomerModel, CustomerData> customerConverter;


	/**
	 * Populate.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 * @throws ConversionException
	 *            the conversion exception
	 */
	@Override
	public void populate(final AppliedReferralHistoryModel source, final AppliedReferralHistoryData target)
			throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setAmount(source.getAmount());
		target.setCreationDate(source.getCreationDate());
		if (!Objects.isNull(source.getAppliedCustomer()))
		{
			target.setCustomer(customerConverter.convert(source.getAppliedCustomer()));
		}
	}

}
