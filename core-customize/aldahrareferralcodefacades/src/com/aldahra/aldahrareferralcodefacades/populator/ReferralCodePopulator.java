/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrareferralcodefacades.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.List;
import java.util.Objects;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.util.Assert;

import com.aldahra.aldahrareferralcode.model.AppliedReferralHistoryModel;
import com.aldahra.aldahrareferralcode.model.ReferralCodeModel;
import com.aldahra.aldahrareferralcodefacades.AppliedReferralHistoryData;
import com.aldahra.aldahrareferralcodefacades.ReferralCodeData;


/**
 * @author mohammedbaker The Class ReferralCodePopulator.
 */
public class ReferralCodePopulator implements Populator<ReferralCodeModel, ReferralCodeData>
{

	/** The applied referral history convertor. */
	@Resource(name = "appliedReferralHistoryConverter")
	private Converter<AppliedReferralHistoryModel, AppliedReferralHistoryData> appliedReferralHistoryConvertor;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	/**
	 * Populate.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 * @throws ConversionException
	 *            the conversion exception
	 */
	@Override
	public void populate(final ReferralCodeModel source, final ReferralCodeData target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		if (!Objects.isNull(source.getCode()))
		{
			target.setCode(source.getCode());
		}
		if (!CollectionUtils.isEmpty(source.getAppliedReferralHistory()))
		{
			target.setHistories(appliedReferralHistoryConvertor.convertAll(source.getAppliedReferralHistory()));
			target.setTotalAmount(getTotalAmount(source.getAppliedReferralHistory()));
		}
		target.setPercentage(source.getPercentage());
		target.setValid(source.isValid());
		target.setFixedRewardAmount(source.getFixedRewardAmount());
		target.setSaveType(source.getSaveType());
		target.setReferralOrderRewardType(source.getReferralOrderRewardType());
		getNewAppliedAmount(target);

	}

	/**
	 *
	 */
	protected void getNewAppliedAmount(final ReferralCodeData target)
	{
		final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
		if (!Objects.isNull(currentBaseStore))
		{
			target.setNewAppliedAmount(currentBaseStore.getReferralCodeNewAppliedRewardAmount());
		}

	}

	public double getTotalAmount(final List<AppliedReferralHistoryModel> histories)
	{
		double total = 0;
		for (final AppliedReferralHistoryModel history : histories)
		{
			total += history.getAmount();
		}
		return total;
	}

}
