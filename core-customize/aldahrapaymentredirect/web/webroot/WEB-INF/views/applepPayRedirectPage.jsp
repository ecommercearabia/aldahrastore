<%@page import="org.apache.poi.util.SystemOutLogger"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>CCAvenue Apple Pay Redirect Page</title>
    <link rel="stylesheet" href="<c:url value="/static/aldahrapaymentredirect-webapp.css"/>" type="text/css"
          media="screen, projection"/>
</head>
<div class="container">

    <h2>CCAvenue Apple Pay Redirect Page</h2>

    <h2>Please wait, you will be redirected soon ..</h2>

	<%
		response.sendRedirect("gatewaysdk://applepay?response="+request.getAttribute("body"));	
	%>
</div>
</html>