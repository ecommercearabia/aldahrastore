/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.alldahra.paymentrerdirefct.controller;

import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aldahra.aldahrapayment.ccavenue.enums.PaymentExceptionType;
import com.aldahra.aldahrapayment.ccavenue.exception.PaymentException;
import com.aldahra.aldahrapayment.context.PaymentProviderContext;
import com.aldahra.aldahrapayment.model.CCAvenuePaymentProviderModel;
import com.aldahra.aldahrapayment.model.PaymentProviderModel;
import com.ccavenue.security.AesCryptUtil;
import com.google.gson.JsonObject;


/**
 * @author monzer
 */
@Controller
@RequestMapping("/payment")
public class CCAvenueApplePayRedirectController
{

	private static final Logger LOG = LoggerFactory.getLogger(CCAvenueApplePayRedirectController.class);

	private static final String FC_STORE_ID = "foodcrowd-ae";
	private static final String CCAVENUE_ENC_RESPONSE_PARAM = "encResp";
	private static final String AMPERSAND = "&";
	private static final String EQUALS = "=";
	private static final String MPGS_REDIRECT = "mpgsRedirect";
	private static final String UTF_8 = "UTF-8";
	private static final String BODY = "body";

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "paymentProviderContext")
	private PaymentProviderContext paymentProviderContext;

	@RequestMapping(method =
	{ RequestMethod.GET, RequestMethod.POST }, value = "/apple/redirect")
	public String getApplePayRedirectPage(final Model model, final HttpServletRequest request)
			throws PaymentException, UnsupportedEncodingException
	{
		final CCAvenuePaymentProviderModel provider = getCCAvenuePaymentProviderByStore().get();

		if (StringUtils.isBlank(provider.getAppleWorkingKey()))
		{
			LOG.error("CCAvenue Apple Pay working key is empty, please check the provider from the backoffice");
			throw new PaymentException("CCAvenue Apple Pay working key is empty", PaymentExceptionType.FAILURE_RESULT);
		}

		final String encResp = request.getParameter(CCAVENUE_ENC_RESPONSE_PARAM);

		if (StringUtils.isBlank(encResp))
		{
			LOG.error("Apple Pay response should include the encResp parameter which is missing");
			throw new PaymentException("Apple Pay response encResp parameter is missing", PaymentExceptionType.FAILURE_RESULT);
		}

		LOG.info("Decrypting apple pay response: {}", encResp);
		final AesCryptUtil aesUtil = new AesCryptUtil(provider.getAppleWorkingKey());
		final String response = aesUtil.decrypt(encResp);

		final String[] parts = response.split(AMPERSAND);

		final JsonObject json = new JsonObject();

		for (final String part : parts)
		{
			final String[] keyVal = part.split(EQUALS); // The equal separates key and values
			json.addProperty(keyVal[0], keyVal.length == 2 ? keyVal[1] : "");
		}
		LOG.info("Apple Pay response has been successfully collected as json");
		model.addAttribute(BODY, URLEncoder.encode(json.toString(), UTF_8));

		return "applepPayRedirectPage";
	}

	private Optional<CCAvenuePaymentProviderModel> getCCAvenuePaymentProviderByStore() throws PaymentException
	{
		final BaseStoreModel baseStoreForUid = baseStoreService.getBaseStoreForUid(FC_STORE_ID);
		if (baseStoreForUid == null)
		{
			LOG.error("Could not find base store with {} id", FC_STORE_ID);
			throw new PaymentException("Could not find base store by : " + FC_STORE_ID, PaymentExceptionType.FAILURE_RESULT);
		}
		final Optional<PaymentProviderModel> provider = paymentProviderContext.getProvider(baseStoreForUid);
		if (provider.isEmpty() || !(provider.get() instanceof CCAvenuePaymentProviderModel))
		{
			LOG.error("CCAvenue payment provider is not provided");
			throw new PaymentException("CCAvenue payment provider is not provided", PaymentExceptionType.FAILURE_RESULT);
		}
		return Optional.of((CCAvenuePaymentProviderModel) provider.get());
	}

}
