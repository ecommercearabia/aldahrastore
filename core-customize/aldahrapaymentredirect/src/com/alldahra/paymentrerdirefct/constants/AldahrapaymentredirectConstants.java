/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.alldahra.paymentrerdirefct.constants;

/**
 * Global class for all Aldahrapaymentredirect constants. You can add global constants for your extension into this class.
 */
public final class AldahrapaymentredirectConstants extends GeneratedAldahrapaymentredirectConstants
{
	public static final String EXTENSIONNAME = "aldahrapaymentredirect";

	private AldahrapaymentredirectConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "aldahrapaymentredirectPlatformLogo";
}
