/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.fulfilmentprocess.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.aldahra.fulfilmentprocess.constants.AldahraFulfilmentProcessConstants;

public class AldahraFulfilmentProcessManager extends GeneratedAldahraFulfilmentProcessManager
{
	public static final AldahraFulfilmentProcessManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (AldahraFulfilmentProcessManager) em.getExtension(AldahraFulfilmentProcessConstants.EXTENSIONNAME);
	}
	
}
