/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.fulfilmentprocess.actions.returns;

import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.returns.model.ReturnProcessModel;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aldahra.core.service.LoyaltyPointService;


/**
 * Mock implementation to execute when payment capture was successful.
 */
public class SuccessCaptureAction extends AbstractProceduralAction<ReturnProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(SuccessCaptureAction.class);

	@Resource(name = "loyaltyPointService")
	private LoyaltyPointService loyaltyPointService;

	@Override
	public void executeAction(final ReturnProcessModel process)
	{
		LOG.info("Process: " + process.getCode() + " in step " + getClass().getSimpleName());
		try
		{
			getLoyaltyPointService().returnLoyaltyPoints(process.getReturnRequest());
		}
		catch (final Exception e)
		{
			LOG.error("Can not Return loyalty points for Customrt: {}", process.getUser().getUid());
		}
	}


	/**
	 * @return the loyaltyPointService
	 */
	protected LoyaltyPointService getLoyaltyPointService()
	{
		return loyaltyPointService;
	}
}
