/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraconsignmenttrackingcustomaddon.constants;

/**
 * Global class for all Aldahraconsignmenttrackingcustomaddonaddon web constants. You can add global constants for your extension into this class.
 */
public final class AldahraconsignmenttrackingcustomaddonaddonWebConstants // NOSONAR
{
	private AldahraconsignmenttrackingcustomaddonaddonWebConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
