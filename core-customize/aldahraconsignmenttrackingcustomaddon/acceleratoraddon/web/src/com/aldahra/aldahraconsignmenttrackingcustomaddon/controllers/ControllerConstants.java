/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraconsignmenttrackingcustomaddon.controllers;

/**
 */
public interface ControllerConstants
{
	interface Views
	{
		String _AddonPrefix = "addon:/aldahraconsignmenttrackingcustomaddon/";

		interface Pages
		{

			interface Consignment
			{
				String TrackPackagePage = _AddonPrefix + "pages/consignment/trackPackage";
			}
		}
	}
}
