/*
 *  
 * [y] hybris Platform
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.aldahra.aldahraconsignmenttrackingcustomaddon.jalo;

import com.aldahra.aldahraconsignmenttrackingcustomaddon.constants.AldahraconsignmenttrackingcustomaddonaddonConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

public class AldahraconsignmenttrackingcustomaddonaddonManager extends GeneratedAldahraconsignmenttrackingcustomaddonaddonManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( AldahraconsignmenttrackingcustomaddonaddonManager.class.getName() );
	
	public static final AldahraconsignmenttrackingcustomaddonaddonManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (AldahraconsignmenttrackingcustomaddonaddonManager) em.getExtension(AldahraconsignmenttrackingcustomaddonaddonConstants.EXTENSIONNAME);
	}
	
}
