/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraconsignmenttrackingcustomaddon.constants;

/**
 * Global class for all Aldahraconsignmenttrackingcustomaddonaddon constants. You can add global constants for your extension into this class.
 */
public final class AldahraconsignmenttrackingcustomaddonaddonConstants extends GeneratedAldahraconsignmenttrackingcustomaddonaddonConstants
{
	public static final String EXTENSIONNAME = "aldahraconsignmenttrackingcustomaddon";

	private AldahraconsignmenttrackingcustomaddonaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
