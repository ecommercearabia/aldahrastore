/**
 *
 */
package com.aldahra.storefront.form;

import java.io.Serializable;


/**
 * @author Tuqa
 *
 */
public class DeliveryModeForm implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String deliveryModeCode;
	
	/**
	 * @return the deliveryModeCode
	 */
	public String getDeliveryModeCode()
	{
		return deliveryModeCode;
	}
	
	/**
	 * @param deliveryModeCode
	 *           the deliveryModeCode to set
	 */
	public void setDeliveryModeCode(final String deliveryModeCode)
	{
		this.deliveryModeCode = deliveryModeCode;
	}


}
