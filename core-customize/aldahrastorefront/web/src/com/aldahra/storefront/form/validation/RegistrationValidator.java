package com.aldahra.storefront.form.validation;

import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.aldahra.aldahrareferralcode.service.ReferralCodeService;
import com.aldahra.aldahrauser.model.NationalityModel;
import com.aldahra.aldahrauser.service.MobilePhoneService;
import com.aldahra.aldahrauser.service.NationalityService;
import com.aldahra.storefront.form.RegisterForm;



/**
 * Validates registration forms.
 *
 * @author mnasro
 *
 */
@Component("customRegistrationValidator")
public class RegistrationValidator implements Validator
{
	private static final Logger LOG = Logger.getLogger(RegistrationValidator.class);

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;

	@Resource(name = "userFacade")
	private UserFacade userFacade;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "referralCodeService")
	private ReferralCodeService referralCodeService;

	@Resource(name = "nationalityService")
	private NationalityService nationalityService;

	/**
	 * @return the nationalityService
	 */
	public NationalityService getNationalityService()
	{
		return nationalityService;
	}

	public static final Pattern MOBILE_REGEX = Pattern.compile("[^a-zA-Z.]+$");


	@Override
	public boolean supports(final Class<?> aClass)
	{
		return RegisterForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final RegisterForm registerForm = (RegisterForm) object;
		final String titleCode = registerForm.getTitleCode();
		final String firstName = registerForm.getFirstName();
		final String lastName = registerForm.getLastName();
		final String email = registerForm.getEmail();
		final String pwd = registerForm.getPwd();
		final String checkPwd = registerForm.getCheckPwd();
		final boolean termsCheck = registerForm.isTermsCheck();
		final String mobileNumber = registerForm.getMobileNumber();
		final String mobileCountry = registerForm.getMobileCountry();
		final String referralCode = registerForm.getReferralCode();
		final String nationality = registerForm.getNationality();
		final String nationalityId = registerForm.getNationalityId();

		if (StringUtils.isEmpty(mobileCountry))
		{
			errors.rejectValue("mobileCountry", "register.mobileCountry.invalid");
		}
		if (StringUtils.isEmpty(mobileNumber) || !validateMobileNumber(mobileNumber))
		{
			errors.rejectValue("mobileNumber", "register.mobileNumber.invalid");
		}
		else if (!StringUtils.isEmpty(mobileCountry))
		{
			final Optional<String> normalizedPhoneNumber = mobilePhoneService.validateAndNormalizePhoneNumberByIsoCode(mobileCountry,
					mobileNumber);

			if (normalizedPhoneNumber.isPresent())
			{
				registerForm.setMobileNumber(normalizedPhoneNumber.get());
				if (!mobilePhoneService.isMobileNumberValidByCurrentCustomer(normalizedPhoneNumber.get()))
				{
					errors.rejectValue("mobileNumber", "register.mobileNumber.customer.exists");
				}
			}
			else
			{
				errors.rejectValue("mobileNumber", "register.mobileNumber.format.invalid");
				errors.rejectValue("mobileCountry", "register.mobileCountry.invalid");
			}
		}

		validateTitleCode(errors, titleCode);
		validateName(errors, firstName, "firstName", "register.firstName.invalid");
		validateName(errors, lastName, "lastName", "register.lastName.invalid");

		if (StringUtils.length(firstName) + StringUtils.length(lastName) > 255)
		{
			errors.rejectValue("lastName", "register.name.invalid");
			errors.rejectValue("firstName", "register.name.invalid");
		}
		validateReferralCode(errors, referralCode);
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		if (registerForm.isHasNationalId())
		{
			validateNationalityID(errors, currentSite, nationalityId);
		}
		validateEmail(errors, email);
		validatePassword(errors, pwd);
		validateNationality(errors, currentSite, nationality);
		//		comparePasswords(errors, pwd, checkPwd);
		validateTermsAndConditions(errors, termsCheck);
	}



	/**
	 * @param errors
	 * @param currentSite
	 * @param nationality
	 */
	private void validateNationality(final Errors errors, final CMSSiteModel cmsSiteModel, final String nationality)
	{
		if (cmsSiteModel != null && cmsSiteModel.isNationalityCustomerEnabled() && cmsSiteModel.isNationalityCustomerRequired()
				&& StringUtils.isEmpty(nationality))
		{
			errors.rejectValue("nationality", "profile.nationality.invalid");
		}
		if (StringUtils.isNotEmpty(nationality))
		{
			final Optional<NationalityModel> national = nationalityService.get(nationality);
			if (!national.isPresent())
			{
				errors.rejectValue("nationality", "profile.nationality.invalid");
			}
		}

	}

	/**
	 * @param errors
	 * @param currentSite
	 * @param nationalityId
	 */
	private void validateNationalityID(final Errors errors, final CMSSiteModel currentSite, final String nationalityId)
	{
		if (currentSite != null && currentSite.isNationalityIdCustomerEnabled() && currentSite.isNationalityIdCustomerRequired()
				&& StringUtils.isEmpty(nationalityId))
		{
			errors.rejectValue("nationalityId", "register.nationalityid.invalid");
		}
	}

	/**
	 *
	 * @param errors
	 * @param pwd
	 * @param checkPwd
	 */
	protected void comparePasswords(final Errors errors, final String pwd, final String checkPwd)
	{
		if (StringUtils.isNotEmpty(pwd) && StringUtils.isNotEmpty(checkPwd) && !StringUtils.equals(pwd, checkPwd))
		{
			errors.rejectValue("checkPwd", "validation.checkPwd.equals");
		}
		else
		{
			if (StringUtils.isEmpty(checkPwd))
			{
				errors.rejectValue("checkPwd", "register.checkPwd.invalid");
			}
		}
	}

	/**
	 *
	 * @param errors
	 * @param pwd
	 */
	protected void validatePassword(final Errors errors, final String pwd)
	{
		if (StringUtils.isEmpty(pwd))
		{
			errors.rejectValue("pwd", "register.pwd.invalid");
		}
		else if (StringUtils.length(pwd) < 6 || StringUtils.length(pwd) > 255)
		{
			errors.rejectValue("pwd", "register.pwd.invalid");
		}
	}

	/**
	 *
	 * @param errors
	 * @param email
	 */
	protected void validateEmail(final Errors errors, final String email)
	{
		if (StringUtils.isEmpty(email))
		{
			errors.rejectValue("email", "register.email.invalid");
		}
		else if (StringUtils.length(email) > 255 || !validateEmailAddress(email))
		{
			errors.rejectValue("email", "register.email.invalid");
		}
		else
		{
			try
			{
				if (userFacade.getUserUID(email) != null)
				{
					errors.rejectValue("email", "registration.error.account.exists.title");
				}
			}
			catch (final UnknownIdentifierException e)
			{
				LOG.info("Customer is not existed this is valid where UID=[" + email + "]");

			}
		}

	}

	/**
	 *
	 * @param errors
	 * @param email
	 */
	protected void validateReferralCode(final Errors errors, final String referralCode)
	{

		if (StringUtils.isEmpty(referralCode) || referralCodeService.canApplyCustomerByCurrentBaseStore(referralCode))
		{
			return;
		}
		errors.rejectValue("referralCode", "register.referralcode.invalid");
		LOG.info("Referral Code  is not exist [" + referralCode + "]");
	}

	/**
	 *
	 * @param errors
	 * @param name
	 * @param propertyName
	 * @param property
	 */
	protected void validateName(final Errors errors, final String name, final String propertyName, final String property)
	{
		if (StringUtils.isBlank(name))
		{
			errors.rejectValue(propertyName, property);
		}
		else if (StringUtils.length(name) > 255)
		{
			errors.rejectValue(propertyName, property);
		}
	}

	/**
	 *
	 * @param errors
	 * @param titleCode
	 */
	protected void validateTitleCode(final Errors errors, final String titleCode)
	{
		if (StringUtils.isEmpty(titleCode) | StringUtils.length(titleCode) > 255)
		{
			errors.rejectValue("titleCode", "register.title.invalid");
		}
	}

	/**
	 *
	 * @param number
	 * @return true or false
	 */
	public boolean validateMobileNumber(final String number)
	{
		final Matcher matcher = MOBILE_REGEX.matcher(number);
		return matcher.matches();
	}

	/**
	 *
	 * @param email
	 * @return true or false
	 */
	protected boolean validateEmailAddress(final String email)
	{
		final String emailRestriction = getEmailRestriction();
		Matcher matcher = null;

		//		if (emailRestriction == null || emailRestriction.isEmpty())
		//		{
		matcher = Pattern.compile(configurationService.getConfiguration().getString(WebConstants.EMAIL_REGEX)).matcher(email);
		//		}
		//		else
		//		{
		//
		//			matcher = Pattern.compile("^[\\w-\\+]+(\\.[\\w]+)*@(" + emailRestriction + ")$", Pattern.CASE_INSENSITIVE)
		//					.matcher(email);
		//		}

		return matcher.matches();
	}

	/**
	 * @return
	 */
	private String getEmailRestriction()
	{
		final String siteID = cmsSiteService.getCurrentSite() == null ? "" : cmsSiteService.getCurrentSite().getUid();
		final String emailRestriction = configurationService.getConfiguration()
				.getString("website.registration.email.restriction." + siteID);
		if (StringUtils.isBlank(emailRestriction))
		{
			return null;
		}

		return emailRestriction.replaceAll(";", "|");
	}

	/**
	 *
	 * @param errors
	 * @param termsCheck
	 */
	protected void validateTermsAndConditions(final Errors errors, final boolean termsCheck)
	{
		if (!termsCheck)
		{
			errors.rejectValue("termsCheck", "register.terms.not.accepted");
		}
	}
}
