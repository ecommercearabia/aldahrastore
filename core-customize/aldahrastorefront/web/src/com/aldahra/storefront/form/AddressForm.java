package com.aldahra.storefront.form;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * The Class AddressForm.
 *
 * @author mnasro
 */
public class AddressForm
{
	/** The address id. */
	private String chosenAddressId;
	/** The address id. */
	private String addressId;

	private boolean validateForm;
	/** The title code. */
	private String titleCode;

	/** The first name. */
	private String firstName;

	/** The last name. */
	private String lastName;

	/** The line 1. */
	private String line1;

	/** The line 2. */
	private String line2;

	/** The town city. */
	private String townCity;

	/** The region iso. */
	private String regionIso;

	/** The postcode. */
	private String postcode;

	/** The country iso. */
	private String countryIso;

	/** The save in address book. */
	private Boolean saveInAddressBook;

	/** The default address. */
	private Boolean defaultAddress;

	/** The shipping address. */
	private Boolean shippingAddress;

	/** The billing address. */
	private Boolean billingAddress;

	/** The edit address. */
	private Boolean editAddress;

	/** The phone. */
	private String phone;

	/** The mobile. */
	private String mobile;

	/** The mobile country. */
	private String mobileCountry;

	/** The city code. */
	private String cityCode;

	/** The area code. */
	private String areaCode;

	/** The nearest landmark. */
	private String nearestLandmark;

	/** The latitude. */
	private Double latitude;

	/** The longitude. */
	private Double longitude;

	/** The addressName. */
	private String addressName;

	private boolean edit;
	private String buildingName;
	private String streetName;
	private String apartmentNumber;
	
	/**
	 * @return the buildingName
	 */
	public String getBuildingName()
	{
		return buildingName;
	}

	/**
	 * @param buildingName
	 *           the buildingName to set
	 */
	public void setBuildingName(final String buildingName)
	{
		this.buildingName = buildingName;
	}

	/**
	 * @return the streetName
	 */
	public String getStreetName()
	{
		return streetName;
	}

	/**
	 * @param streetName
	 *           the streetName to set
	 */
	public void setStreetName(final String streetName)
	{
		this.streetName = streetName;
	}

	/**
	 * @return the apartmentNumber
	 */
	public String getApartmentNumber()
	{
		return apartmentNumber;
	}

	/**
	 * @param apartmentNumber
	 *           the apartmentNumber to set
	 */
	public void setApartmentNumber(final String apartmentNumber)
	{
		this.apartmentNumber = apartmentNumber;
	}

	/**
	 * @return the nearestLandmark
	 */
	@NotNull(message = "{address.nearestLandmark.invalid}")
	@Size(min = 1, max = 255, message = "{address.nearestLandmark.invalid}")
	public String getNearestLandmark()
	{
		return nearestLandmark;
	}

	/**
	 * @return the edit
	 */
	public boolean isEdit()
	{
		return edit;
	}

	/**
	 * @param edit
	 *           the edit to set
	 */
	public void setEdit(final boolean edit)
	{
		this.edit = edit;
	}

	/**
	 * @param nearestLandmark
	 *           the nearestLandmark to set
	 */
	public void setNearestLandmark(final String nearestLandmark)
	{
		this.nearestLandmark = nearestLandmark;
	}

	/**
	 * @return the latitude
	 */
	public Double getLatitude()
	{
		return latitude;
	}

	/**
	 * @param latitude
	 *           the latitude to set
	 */
	public void setLatitude(final Double latitude)
	{
		this.latitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	public Double getLongitude()
	{
		return longitude;
	}

	/**
	 * @param longitude
	 *           the longitude to set
	 */
	public void setLongitude(final Double longitude)
	{
		this.longitude = longitude;
	}

	/**
	 * @return the addressName
	 */
	@NotNull(message = "{address.addressName.invalid}")
	@Size(min = 1, max = 255, message = "{address.addressName.invalid}")
	public String getAddressName()
	{
		return addressName;
	}

	/**
	 * @param addressName
	 *           the addressName to set
	 */
	public void setAddressName(final String addressName)
	{
		this.addressName = addressName;
	}

	/**
	 * Gets the area code.
	 *
	 * @return the areaCode
	 */
	public String getAreaCode()
	{
		return areaCode;
	}

	/**
	 * Sets the area code.
	 *
	 * @param areaCode
	 *           the areaCode to set
	 */
	public void setAreaCode(final String areaCode)
	{
		this.areaCode = areaCode;
	}


	/**
	 * Gets the city code.
	 *
	 * @return the cityCode
	 */
	public String getCityCode()
	{
		return cityCode;
	}

	/**
	 * Sets the city code.
	 *
	 * @param cityCode
	 *           the cityCode to set
	 */
	public void setCityCode(final String cityCode)
	{
		this.cityCode = cityCode;
	}

	/**
	 * Gets the address id.
	 *
	 * @return the address id
	 */
	public String getAddressId()
	{
		return addressId;
	}

	/**
	 * Sets the address id.
	 *
	 * @param addressId
	 *           the new address id
	 */
	public void setAddressId(final String addressId)
	{
		this.addressId = addressId;
	}

	/**
	 * Gets the title code.
	 *
	 * @return the title code
	 */
	@Size(max = 255, message = "{address.title.invalid}")
	public String getTitleCode()
	{
		return titleCode;
	}

	/**
	 * Sets the title code.
	 *
	 * @param titleCode
	 *           the new title code
	 */
	public void setTitleCode(final String titleCode)
	{
		this.titleCode = titleCode;
	}

	/**
	 * Gets the first name.
	 *
	 * @return the first name
	 */
	@NotNull(message = "{address.firstName.invalid}")
	@Size(min = 1, max = 255, message = "{address.firstName.invalid}")
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * Sets the first name.
	 *
	 * @param firstName
	 *           the new first name
	 */
	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * Gets the last name.
	 *
	 * @return the last name
	 */
	@NotNull(message = "{address.lastName.invalid}")
	@Size(min = 1, max = 255, message = "{address.lastName.invalid}")
	public String getLastName()
	{
		return lastName;
	}

	/**
	 * Sets the last name.
	 *
	 * @param lastName
	 *           the new last name
	 */
	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	/**
	 * Gets the line 1.
	 *
	 * @return the line 1
	 */
	@NotNull(message = "{address.line1.invalid}")
	@Size(min = 1, max = 255, message = "{address.line1.invalid}")
	public String getLine1()
	{
		return line1;
	}

	/**
	 * Sets the line 1.
	 *
	 * @param line1
	 *           the new line 1
	 */
	public void setLine1(final String line1)
	{
		this.line1 = line1;
	}

	/**
	 * Gets the line 2.
	 *
	 * @return the line 2
	 */
	public String getLine2()
	{
		return line2;
	}

	/**
	 * Sets the line 2.
	 *
	 * @param line2
	 *           the new line 2
	 */
	public void setLine2(final String line2)
	{
		this.line2 = line2;
	}

	/**
	 * Gets the town city.
	 *
	 * @return the town city
	 */
	@NotNull(message = "{address.townCity.invalid}")
	@Size(min = 1, max = 255, message = "{address.townCity.invalid}")
	public String getTownCity()
	{
		return townCity;
	}

	/**
	 * Sets the town city.
	 *
	 * @param townCity
	 *           the new town city
	 */
	public void setTownCity(final String townCity)
	{
		this.townCity = townCity;
	}

	/**
	 * Gets the region iso.
	 *
	 * @return the region iso
	 */
	public String getRegionIso()
	{
		return regionIso;
	}

	/**
	 * Sets the region iso.
	 *
	 * @param regionIso
	 *           the new region iso
	 */
	public void setRegionIso(final String regionIso)
	{
		this.regionIso = regionIso;
	}

	/**
	 * Gets the postcode.
	 *
	 * @return the postcode
	 */
	@NotNull(message = "{address.postcode.invalid}")
	@Size(min = 1, max = 10, message = "{address.postcode.invalid}")
	public String getPostcode()
	{
		return postcode;
	}

	/**
	 * Sets the postcode.
	 *
	 * @param postcode
	 *           the new postcode
	 */
	public void setPostcode(final String postcode)
	{
		this.postcode = postcode;
	}

	/**
	 * Gets the country iso.
	 *
	 * @return the country iso
	 */
	@NotNull(message = "{address.country.invalid}")
	@Size(min = 1, max = 255, message = "{address.country.invalid}")
	public String getCountryIso()
	{
		return countryIso;
	}

	/**
	 * Sets the country iso.
	 *
	 * @param countryIso
	 *           the new country iso
	 */
	public void setCountryIso(final String countryIso)
	{
		this.countryIso = countryIso;
	}

	/**
	 * Gets the save in address book.
	 *
	 * @return the save in address book
	 */
	public Boolean getSaveInAddressBook()
	{
		return saveInAddressBook;
	}

	/**
	 * Sets the save in address book.
	 *
	 * @param saveInAddressBook
	 *           the new save in address book
	 */
	public void setSaveInAddressBook(final Boolean saveInAddressBook)
	{
		this.saveInAddressBook = saveInAddressBook;
	}

	/**
	 * Gets the default address.
	 *
	 * @return the default address
	 */
	public Boolean getDefaultAddress()
	{
		return defaultAddress;
	}

	/**
	 * Sets the default address.
	 *
	 * @param defaultAddress
	 *           the new default address
	 */
	public void setDefaultAddress(final Boolean defaultAddress)
	{
		this.defaultAddress = defaultAddress;
	}

	/**
	 * Gets the shipping address.
	 *
	 * @return the shipping address
	 */
	public Boolean getShippingAddress()
	{
		return shippingAddress;
	}

	/**
	 * Sets the shipping address.
	 *
	 * @param shippingAddress
	 *           the new shipping address
	 */
	public void setShippingAddress(final Boolean shippingAddress)
	{
		this.shippingAddress = shippingAddress;
	}

	/**
	 * Gets the billing address.
	 *
	 * @return the billing address
	 */
	public Boolean getBillingAddress()
	{
		return billingAddress;
	}

	/**
	 * Sets the billing address.
	 *
	 * @param billingAddress
	 *           the new billing address
	 */
	public void setBillingAddress(final Boolean billingAddress)
	{
		this.billingAddress = billingAddress;
	}

	/**
	 * Gets the edits the address.
	 *
	 * @return the edits the address
	 */
	public Boolean getEditAddress()
	{
		return editAddress;
	}

	/**
	 * Sets the edits the address.
	 *
	 * @param editAddress
	 *           the new edits the address
	 */
	public void setEditAddress(final Boolean editAddress)
	{
		this.editAddress = editAddress;
	}

	/**
	 * Gets the phone.
	 *
	 * @return the phone
	 */
	public String getPhone()
	{
		return phone;
	}

	/**
	 * Sets the phone.
	 *
	 * @param value
	 *           the new phone
	 */
	public void setPhone(final String value)
	{
		phone = value;
	}

	/**
	 * Gets the mobile.
	 *
	 * @return the mobile
	 */
	public String getMobile()
	{
		return mobile;
	}

	/**
	 * Sets the mobile.
	 *
	 * @param mobile
	 *           the mobile to set
	 */
	public void setMobile(final String mobile)
	{
		this.mobile = mobile;
	}

	/**
	 * Gets the mobile country.
	 *
	 * @return the mobileCountry
	 */
	public String getMobileCountry()
	{
		return mobileCountry;
	}

	/**
	 * Sets the mobile country.
	 *
	 * @param mobileCountry
	 *           the mobileCountry to set
	 */
	public void setMobileCountry(final String mobileCountry)
	{
		this.mobileCountry = mobileCountry;
	}

	/**
	 * @return the chosenAddressId
	 */
	public String getChosenAddressId()
	{
		return chosenAddressId;
	}

	/**
	 * @param chosenAddressId
	 *           the chosenAddressId to set
	 */
	public void setChosenAddressId(final String chosenAddressId)
	{
		this.chosenAddressId = chosenAddressId;
	}

	/**
	 * @return the validateForm
	 */
	public boolean isValidateForm()
	{
		return validateForm;
	}

	/**
	 * @param validateForm
	 *           the validateForm to set
	 */
	public void setValidateForm(final boolean validateForm)
	{
		this.validateForm = validateForm;
	}



}
