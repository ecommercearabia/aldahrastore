/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.storefront.form;

/**
 * The Class PaymentDetailsForm.
 *
 * @author mnasro
 *
 *         The Class PaymentDetailsForm.
 */
public class PaymentDetailsForm
{

	/**
	 */
	public PaymentDetailsForm()
	{
		super();
		this.saveInAccount = true;
	}

	/** The payment mode code. */
	private String paymentModeCode;

	/** The save in account. */
	private boolean saveInAccount;



	/**
	 * Gets the save in account.
	 *
	 * @return the save in account
	 */
	public boolean getSaveInAccount()
	{

		return saveInAccount;
	}

	/**
	 * Sets the save in account.
	 *
	 * @param saveInAccount
	 *           the new save in account
	 */
	public void setSaveInAccount(final boolean saveInAccount)
	{
		this.saveInAccount = saveInAccount;
	}

	/**
	 * Gets the payment mode code.
	 *
	 * @return the paymentModeCode
	 */
	public String getPaymentModeCode()
	{
		return paymentModeCode;
	}

	/**
	 * Sets the payment mode code.
	 *
	 * @param paymentModeCode
	 *           the paymentModeCode to set
	 */
	public void setPaymentModeCode(final String paymentModeCode)
{
		this.paymentModeCode = paymentModeCode;
	}



}
