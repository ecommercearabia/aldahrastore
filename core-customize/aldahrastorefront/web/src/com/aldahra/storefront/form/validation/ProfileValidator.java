/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.storefront.form.validation;


import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.aldahra.aldahrauser.model.NationalityModel;
import com.aldahra.aldahrauser.service.MobilePhoneService;
import com.aldahra.aldahrauser.service.NationalityService;
import com.aldahra.aldahrauserfacades.customer.facade.CustomCustomerFacade;
import com.aldahra.storefront.form.UpdateProfileForm;


/**
 * Validator for profile forms.
 *
 * @author monzer
 */
@Component("customProfileValidator")
public class ProfileValidator implements Validator
{
	@Resource(name = "customCustomerFacade")
	private CustomCustomerFacade customerFacade;

	@Resource(name = "nationalityService")
	private NationalityService nationalityService;


	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	/**
	 * @return the cmsSiteService
	 */
	public CMSSiteService getCmsSiteService()
	{
		return cmsSiteService;
	}

	/**
	 * @return the nationalityService
	 */
	public NationalityService getNationalityService()
	{
		return nationalityService;
	}

	/** The mobile phone service. */
	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;

	public static final Pattern MOBILE_REGEX = Pattern.compile("[^a-zA-Z.]+$");

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return UpdateProfileForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final UpdateProfileForm profileForm = (UpdateProfileForm) object;
		//		final String title = profileForm.getTitleCode();
		final String firstName = profileForm.getFirstName();
		final String lastName = profileForm.getLastName();
		final String nationality = profileForm.getNationality();
		final String nationalityID = profileForm.getNationalityId();
		//		if (StringUtils.isNotEmpty(title) && StringUtils.length(title) > 255)
		//		{
		//			errors.rejectValue("titleCode", "profile.title.invalid");
		//		}

		if (StringUtils.isBlank(firstName))
		{
			errors.rejectValue("firstName", "profile.firstName.invalid");
		}
		else if (StringUtils.length(firstName) > 255)
		{
			errors.rejectValue("firstName", "profile.firstName.invalid");
		}

		if (StringUtils.isBlank(lastName))
		{
			errors.rejectValue("lastName", "profile.lastName.invalid");
		}
		else if (StringUtils.length(lastName) > 255)
		{
			errors.rejectValue("lastName", "profile.lastName.invalid");
		}
		validateMobileNumber(profileForm, errors);

		final CMSSiteModel currentSite = getCmsSiteService().getCurrentSite();

		validateNationality(errors, currentSite, nationality);
		if (profileForm.isHasNationalId())
		{
			validateNationalityID(errors, currentSite, nationalityID);
		}
	}

	/**
	 * @param errors
	 * @param currentSite
	 * @param nationalityID
	 */
	private void validateNationalityID(final Errors errors, final CMSSiteModel currentSite, final String nationalityID)
	{
		if (currentSite != null && currentSite.isNationalityIdCustomerEnabled() && currentSite.isNationalityIdCustomerRequired()
				&& StringUtils.isEmpty(nationalityID))
		{
			errors.rejectValue("nationalityID", "profile.nationalityid.invalid");
		}

	}

	/**
	 * @param errors
	 * @param currentSite
	 * @param nationality
	 */
	private void validateNationality(final Errors errors, final CMSSiteModel cmsSiteModel, final String nationality)
	{
		if (cmsSiteModel != null && cmsSiteModel.isNationalityCustomerEnabled() && cmsSiteModel.isNationalityCustomerRequired()
				&& StringUtils.isEmpty(nationality))
		{
			errors.rejectValue("nationality", "profile.nationality.invalid");
		}
		if (StringUtils.isNotEmpty(nationality))
		{
			final Optional<NationalityModel> national = nationalityService.get(nationality);
			if (!national.isPresent())
			{
				errors.rejectValue("nationality", "profile.nationality.invalid");
			}
		}

	}

	/**
	 * @param mobileNumber
	 * @param mobileCountry
	 * @param errors
	 */
	private void validateMobileNumber(final UpdateProfileForm profileForm, final Errors errors)
	{
		if (profileForm == null)
		{
			errors.rejectValue("profileForm", "profile.invalid");
		}
		final String mobileNumber = profileForm.getMobileNumber();
		final String mobileCountry = profileForm.getMobileCountry();
		if (StringUtils.isEmpty(mobileCountry))
		{
			errors.rejectValue("mobileCountry", "profile.mobileCountry.invalid");
		}
		if (StringUtils.isEmpty(mobileNumber) || !validateMobileNumber(mobileNumber))
		{
			errors.rejectValue("mobileNumber", "profile.mobileNumber.invalid");
		}
		else if (!StringUtils.isEmpty(mobileCountry))
		{
			final Optional<String> normalizedPhoneNumber = getMobilePhoneService()
					.validateAndNormalizePhoneNumberByIsoCode(mobileCountry, mobileNumber);

			if (normalizedPhoneNumber.isPresent())
			{
				profileForm.setMobileNumber(normalizedPhoneNumber.get());
				if (!getMobilePhoneService().isMobileNumberValidByCurrentCustomer(normalizedPhoneNumber.get()))
				{
					errors.rejectValue("mobileNumber", "profile.mobileNumber.customer.exists");
				}
			}
			else
			{
				errors.rejectValue("mobileNumber", "profile.mobileNumber.format.invalid");
				errors.rejectValue("mobileCountry", "profile.mobileCountry.invalid");
			}
		}
	}

	public boolean validateMobileNumber(final String number)
	{
		final Matcher matcher = MOBILE_REGEX.matcher(number);
		return matcher.matches();
	}

	/**
	 * @return the mobilePhoneService
	 */
	public MobilePhoneService getMobilePhoneService()
	{
		return mobilePhoneService;
	}
}
