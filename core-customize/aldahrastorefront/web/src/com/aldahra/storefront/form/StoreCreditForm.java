/**
 *
 */
package com.aldahra.storefront.form;

import java.io.Serializable;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class StoreCreditForm implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String sctCode;
	private String scAmount;

	/**
	 * @return the sctCode
	 */
	public String getSctCode()
	{
		return sctCode;
	}

	/**
	 * @param sctCode
	 *           the sctCode to set
	 */
	public void setSctCode(final String sctCode)
	{
		this.sctCode = sctCode;
	}

	/**
	 * @return the scAmount
	 */
	public String getScAmount()
	{
		return scAmount;
	}

	/**
	 * @param scAmount
	 *           the scAmount to set
	 */
	public void setScAmount(final String scAmount)
	{
		this.scAmount = scAmount;
	}
}
