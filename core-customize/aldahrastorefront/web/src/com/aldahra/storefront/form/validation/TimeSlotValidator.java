/**
 *
 */
package com.aldahra.storefront.form.validation;

import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.aldahra.aldahraorderfacades.data.OrderNoteData;
import com.aldahra.aldahraorderfacades.facade.OrderNoteFacade;
import com.aldahra.facades.exception.ExpressOrderException;
import com.aldahra.facades.facade.CustomAcceleratorCheckoutFacade;
import com.aldahra.storefront.form.TimeSlotForm;


/**
 * @author amjad.shati@erabia.com
 *
 */
@Component("timeSlotValidator")
public class TimeSlotValidator implements Validator
{
	@Resource(name = "orderNoteFacade")
	private OrderNoteFacade orderNoteFacade;

	@Resource(name = "defaultAcceleratorCheckoutFacade")
	private CustomAcceleratorCheckoutFacade defaultAcceleratorCheckoutFacade;

	@Override
	public boolean supports(final Class<?> clazz)
	{
		return TimeSlotForm.class.equals(clazz);
	}


	@Override
	public void validate(final Object object, final Errors errors)
	{
		final TimeSlotForm form = (TimeSlotForm) object;
		final String periodCode = form.getPeriodCode();
		final String start = form.getStart();
		final String end = form.getEnd();
		final String day = form.getDay();
		final String date = form.getDate();

		try
		{
			if (getDefaultAcceleratorCheckoutFacade().isExpressOrder())
			{
				return;
			}
		}
		catch (final ExpressOrderException e)
		{

		}

		if (StringUtils.isEmpty(periodCode))
		{
			errors.rejectValue("periodCode", "timeslot.periodCode.invalid");
		}
		if (StringUtils.isEmpty(start))
		{
			errors.rejectValue("start", "timeslot.start.invalid");
		}
		if (StringUtils.isEmpty(end))
		{
			errors.rejectValue("end", "timeslot.end.invalid");
		}
		if (StringUtils.isEmpty(day))
		{
			errors.rejectValue("day", "timeslot.day.invalid");
		}
		if (StringUtils.isEmpty(date))
		{
			errors.rejectValue("date", "timeslot.date.invalid");
		}
		if (StringUtils.length(form.getNote()) > 500)
		{
			errors.rejectValue("note", "timeslot.note.invalid");
		}
	}

	protected void validateOrderNote(final TimeSlotForm form, final Errors errors)
	{
		if (StringUtils.isEmpty(form.getNoteCode()) || "other".equalsIgnoreCase(form.getNoteCode()))
		{
			// DO NOTHING
			//				if (StringUtils.isEmpty(form.getNote()))
			//				{
			//					errors.rejectValue("note", "timeslot.note.invalid");
			//				}
		}
		else
		{
			final Optional<OrderNoteData> optional = orderNoteFacade.getOrderNotesByCurrentSite().stream().filter(Objects::nonNull)
					.filter(n -> n.getCode().equals(form.getNoteCode())).findFirst();
			if (optional.isPresent())
			{
				form.setNote(optional.get().getNote());
			}
		}
	}

	protected OrderNoteFacade getOrderNoteFacade()
	{
		return orderNoteFacade;
	}

	protected CustomAcceleratorCheckoutFacade getDefaultAcceleratorCheckoutFacade()
	{
		return defaultAcceleratorCheckoutFacade;
	}

}
