package com.aldahra.storefront.form.validation;


import de.hybris.platform.order.PaymentModeService;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.aldahra.storefront.form.PaymentDetailsForm;


@Component("customPaymentDetailsValidator")
public class PaymentDetailsValidator implements Validator
{
	@Resource(name = "paymentModeService")
	private PaymentModeService paymentModeService;


	protected PaymentModeService getPaymentModeService()
	{
		return paymentModeService;
	}

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return PaymentDetailsForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final PaymentDetailsForm form = (PaymentDetailsForm) object;

		final String paymentModeCode = form.getPaymentModeCode();

		if (StringUtils.isEmpty(paymentModeCode) || getPaymentModeService().getPaymentModeForCode(paymentModeCode) == null)
		{
			errors.rejectValue("paymentModeCode", "paymentdetails.paymentmodecode.invalid");
		}
	}
}
