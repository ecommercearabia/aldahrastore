/**
 *
 */
package com.aldahra.storefront.form;

import java.io.Serializable;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class TimeSlotForm implements Serializable
{
	private static final long serialVersionUID = 1L;

	private String periodCode;
	private String start;
	private String end;
	private String date;
	private String day;

	private String noteCode;
	private String note;

	/**
	 * @return the periodCode
	 */
	public String getPeriodCode()
	{
		return periodCode;
	}

	/**
	 * @param periodCode
	 *           the periodCode to set
	 */
	public void setPeriodCode(final String periodCode)
	{
		this.periodCode = periodCode;
	}

	/**
	 * @return the start
	 */
	public String getStart()
	{
		return start;
	}

	/**
	 * @param start
	 *           the start to set
	 */
	public void setStart(final String start)
	{
		this.start = start;
	}

	/**
	 * @return the end
	 */
	public String getEnd()
	{
		return end;
	}

	/**
	 * @param end
	 *           the end to set
	 */
	public void setEnd(final String end)
	{
		this.end = end;
	}

	/**
	 * @return the date
	 */
	public String getDate()
	{
		return date;
	}

	/**
	 * @param date
	 *           the date to set
	 */
	public void setDate(final String date)
	{
		this.date = date;
	}

	/**
	 * @return the day
	 */
	public String getDay()
	{
		return day;
	}

	/**
	 * @param day
	 *           the day to set
	 */
	public void setDay(final String day)
	{
		this.day = day;
	}

	/**
	 * @return the noteCode
	 */
	public String getNoteCode()
	{
		return noteCode;
	}

	/**
	 * @param noteCode
	 *           the noteCode to set
	 */
	public void setNoteCode(final String noteCode)
	{
		this.noteCode = noteCode;
	}

	/**
	 * @return the note
	 */
	public String getNote()
	{
		return note;
	}

	/**
	 * @param note
	 *           the note to set
	 */
	public void setNote(final String note)
	{
		this.note = note;
	}


}
