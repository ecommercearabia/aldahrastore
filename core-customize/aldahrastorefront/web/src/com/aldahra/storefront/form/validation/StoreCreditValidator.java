/**
 *
 */
package com.aldahra.storefront.form.validation;

import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.aldahra.aldahrastorecredit.enums.StoreCreditModeType;
import com.aldahra.storefront.form.StoreCreditForm;


/**
 * @author amjad.shati@erabia.com
 *
 */
@Component("storeCreditValidator")
public class StoreCreditValidator implements Validator
{
	@Resource(name = "cartFacade")
	private CartFacade cartFacade;

	@Resource(name = "cartService")
	private CartService cartService;

	@Override
	public boolean supports(final Class<?> clazz)
	{
		return StoreCreditForm.class.equals(clazz);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final StoreCreditForm form = (StoreCreditForm) object;
		final String sctCode = form.getSctCode();
		final String scAmount = form.getScAmount();
		if (StringUtils.isEmpty(sctCode))
		{
			errors.rejectValue("sctCode", "storeCredit.sctCode.invalid");
		}
		StoreCreditModeType storeCreditModeType = null;
		try
		{
			storeCreditModeType = StoreCreditModeType.valueOf(sctCode);
		}
		catch (final Exception e)
		{
			errors.rejectValue("sctCode", "storeCredit.sctCode.invalid");
		}

		if (StoreCreditModeType.REDEEM_SPECIFIC_AMOUNT.getCode().equals(sctCode))
		{
			if (StringUtils.isEmpty(scAmount))
			{
				errors.rejectValue("scAmount", "storeCredit.scAmount.invalid");
			}

			double scAmountValue = 0.0;
			try
			{
				scAmountValue = Double.parseDouble(scAmount);
			}
			catch (final Exception ex)
			{
				errors.rejectValue("scAmount", "storeCredit.scAmount.invalid");
			}
			if (scAmountValue < 0)
			{
				errors.rejectValue("scAmount", "storeCredit.scAmount.invalid");
			}
			else
			{


				final CartData cart = getCart();

				final CartModel cartModel = getCartModel();

				if (cart ==null || cartModel == null || cartModel.getStore() == null || !cartModel.getStore().isEnableCheckTotalPriceWithStoreCreditLimit())
				{
					return;
				}

				double totalPriceWithTaxVal = cart.getTotalPriceWithTax() == null || cart.getTotalPriceWithTax().getValue() == null
						? 0.0d
						: cart.getTotalPriceWithTax().getValue().doubleValue();


				final double storeCreditAmount = cart.getStoreCreditAmount() == null || cart.getStoreCreditAmount().getValue() == null
						? 0.0d
						: cart.getStoreCreditAmount().getValue().doubleValue();

				totalPriceWithTaxVal += storeCreditAmount;

				final double totalPricewithStoreCreditLimit = cartModel.getStore() == null ? 0.0d
						: cartModel.getStore().getTotalPriceWithStoreCreditLimit();

				totalPriceWithTaxVal -= totalPricewithStoreCreditLimit;

				if (totalPriceWithTaxVal < scAmountValue)
				{
					errors.rejectValue("scAmount", "storeCredit.scAmount.check.limit.invalid");
				}

			}

		}


	}


	/**
	 * @return
	 */
	protected CartData getCart()
	{
		return cartFacade.hasSessionCart() ? cartFacade.getSessionCart() : null;
	}

	protected CartModel getCartModel()
	{
		return cartService.hasSessionCart() ? cartService.getSessionCart() : null;
	}

}
