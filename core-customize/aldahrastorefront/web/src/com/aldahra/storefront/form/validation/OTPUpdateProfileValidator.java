/**
 *
 */
package com.aldahra.storefront.form.validation;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.aldahra.storefront.form.OTPForm;


/**
 * @author monzer
 *
 */
@Component("otpUpdateProfileValidator")
public class OTPUpdateProfileValidator extends OTPValidator
{
	@Override
	public boolean supports(final Class<?> form)
	{
		return form.getClass().equals(OTPForm.class);
	}

	@Override
	public void validate(final Object form, final Errors errors)
	{
		final OTPForm otpForm = (OTPForm) form;
		validateSend(otpForm, errors);
		validateOTPCode(otpForm, errors);
	}

	@Override
	public void validateSend(final Object object, final Errors errors)
	{
		final OTPForm otpForm = (OTPForm) object;
		validateStandardFields(otpForm, errors);
		validatePhoneNumber(otpForm, errors);
	}

	/**
	 * @param otpForm
	 * @param errors
	 */
	@Override
	public void validatePhoneNumber(final OTPForm form, final Errors errors)
	{
		if (StringUtils.isNotBlank(form.getMobileNumber()))
		{
			final boolean valid = getMobilePhoneService().isMobileNumberValidByCurrentCustomer(form.getMobileNumber());
			if (!valid)
			{
				errors.rejectValue("mobileNumber", "test.found");
			}
		}
	}

}
