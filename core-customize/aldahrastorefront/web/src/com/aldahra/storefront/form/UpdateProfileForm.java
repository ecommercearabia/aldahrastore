/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.storefront.form;


/**
 * Form object for updating profile.
 */
public class UpdateProfileForm
{

	/** The title code. */
	private String titleCode;

	/** The first name. */
	private String firstName;

	/** The last name. */
	private String lastName;

	/** The nationality. */
	private String nationality;

	/** The nationality id. */
	private String nationalityId;

	/** The has national id. */
	private boolean hasNationalId;

	/** The has national id. */
	private boolean backToCart;

	/**
	 * @return the backToCart
	 */
	public boolean isBackToCart()
	{
		return backToCart;
	}

	/**
	 * @param backToCart
	 *           the backToCart to set
	 */
	public void setBackToCart(final boolean backToCart)
	{
		this.backToCart = backToCart;
	}

	/**
	 * Gets the nationality.
	 *
	 * @return the nationality
	 */
	public String getNationality()
	{
		return nationality;
	}

	/**
	 * Sets the nationality.
	 *
	 * @param nationality
	 *           the nationality to set
	 */
	public void setNationality(final String nationality)
	{
		this.nationality = nationality;
	}

	/**
	 * Gets the nationality id.
	 *
	 * @return the nationalityId
	 */
	public String getNationalityId()
	{
		return nationalityId;
	}

	/**
	 * Sets the nationality id.
	 *
	 * @param nationalityId
	 *           the nationalityId to set
	 */
	public void setNationalityId(final String nationalityId)
	{
		this.nationalityId = nationalityId;
	}

	/**
	 * Checks if is checks for national id.
	 *
	 * @return the hasNationalId
	 */
	public boolean isHasNationalId()
	{
		return hasNationalId;
	}

	/**
	 * Sets the checks for national id.
	 *
	 * @param hasNationalId
	 *           the hasNationalId to set
	 */
	public void setHasNationalId(final boolean hasNationalId)
	{
		this.hasNationalId = hasNationalId;
	}


	/** The mobile number. */
	private String mobileNumber;

	/** The mobile country. */
	private String mobileCountry;

	/**
	 * Gets the mobile number.
	 *
	 * @return the mobileNumber
	 */
	public String getMobileNumber()
	{
		return mobileNumber;
	}

	/**
	 * Sets the mobile number.
	 *
	 * @param mobileNumber
	 *           the mobileNumber to set
	 */
	public void setMobileNumber(final String mobileNumber)
	{
		this.mobileNumber = mobileNumber;
	}

	/**
	 * Gets the mobile country.
	 *
	 * @return the mobileCountry
	 */
	public String getMobileCountry()
	{
		return mobileCountry;
	}

	/**
	 * Sets the mobile country.
	 *
	 * @param mobileCountry
	 *           the mobileCountry to set
	 */
	public void setMobileCountry(final String mobileCountry)
	{
		this.mobileCountry = mobileCountry;
	}

	/**
	 * Gets the title code.
	 *
	 * @return the titleCode
	 */
	public String getTitleCode()
	{
		return titleCode;
	}

	/**
	 * Sets the title code.
	 *
	 * @param titleCode
	 *           the titleCode to set
	 */
	public void setTitleCode(final String titleCode)
	{
		this.titleCode = titleCode;
	}

	/**
	 * Gets the first name.
	 *
	 * @return the firstName
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * Sets the first name.
	 *
	 * @param firstName
	 *           the firstName to set
	 */
	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * Gets the last name.
	 *
	 * @return the lastName
	 */
	public String getLastName()
	{
		return lastName;
	}


	/**
	 * Sets the last name.
	 *
	 * @param lastName
	 *           the lastName to set
	 */
	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}



}
