/**
 *
 */
package com.aldahra.storefront.form;

/**
 * @author monzer
 *
 */
public class OTPLoginForm
{

	private String email;
	private String mobileCountry;
	private String mobileNumber;
	private String otpCode;

	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * @param email
	 *           the email to set
	 */
	public void setEmail(final String email)
	{
		this.email = email;
	}

	/**
	 * @return the mobileCountry
	 */
	public String getMobileCountry()
	{
		return mobileCountry;
	}

	/**
	 * @param mobileCountry
	 *           the mobileCountry to set
	 */
	public void setMobileCountry(final String mobileCountry)
	{
		this.mobileCountry = mobileCountry;
	}

	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber()
	{
		return mobileNumber;
	}

	/**
	 * @param mobileNumber
	 *           the mobileNumber to set
	 */
	public void setMobileNumber(final String mobileNumber)
	{
		this.mobileNumber = mobileNumber;
	}

	/**
	 * @return the otpCode
	 */
	public String getOtpCode()
	{
		return otpCode;
	}

	/**
	 * @param otpCode
	 *           the otpCode to set
	 */
	public void setOtpCode(final String otpCode)
	{
		this.otpCode = otpCode;
	}




}
