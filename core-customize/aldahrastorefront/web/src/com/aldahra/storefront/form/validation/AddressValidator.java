/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.storefront.form.validation;



import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.AddressService;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.aldahra.aldahrauser.service.MobilePhoneService;
import com.aldahra.storefront.form.AddressForm;


/**
 * Validator for address forms. Enforces the order of validation
 */
@Component("customAddressValidator")
public class AddressValidator implements Validator
{
	private static final int MAX_FIELD_LENGTH = 255;
	private static final int MAX_POSTCODE_LENGTH = 10;
	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;

	@Resource(name = "addressService")
	private AddressService addressService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;
	@Resource(name = "userFacade")
	private UserFacade userFacade;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return AddressForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final CartModel sessionCart = cartService.getSessionCart();
		final AddressForm addressForm = (AddressForm) object;
		if (StringUtils.isEmpty(addressForm.getChosenAddressId()))
		{
			validateStandardFields(addressForm, errors);
			validateCountrySpecificFields(addressForm, errors);
		}
		else
		{
			final AddressModel chosenAddress = modelService.get(PK.parse(addressForm.getChosenAddressId()));
			fillAddressForm(chosenAddress, addressForm);
			validateStandardFields(addressForm, errors);
			validateCountrySpecificFields(addressForm, errors);
		}
	}

	/**
	 * @param chosenAddress
	 * @param addressForm
	 * @return
	 */
	private AddressForm fillAddressForm(final AddressModel addressModel, final AddressForm addressForm)
	{

		addressForm.setAddressName(
				StringUtils.isBlank(addressForm.getAddressName()) ? addressModel.getAddressName() : addressForm.getAddressName());
		addressForm.setApartmentNumber(StringUtils.isBlank(addressForm.getApartmentNumber())?addressModel.getApartmentNumber():addressForm.getApartmentNumber());
		addressForm.setAreaCode(StringUtils.isBlank(addressForm.getAreaCode())?addressModel.getArea() == null ? null : addressModel.getArea().getCode():addressForm.getAreaCode());
		addressForm.setCityCode(StringUtils.isBlank(addressForm.getCityCode())?addressModel.getCity() == null ? null : addressModel.getCity().getCode():addressForm.getCityCode());
		addressForm.setLatitude(addressForm.getLatitude() ==null ?addressModel.getLatitude():addressForm.getLatitude());
		addressForm.setLongitude(addressForm.getLongitude() ==null ?addressModel.getLongitude():addressForm.getLongitude());
		addressForm.setBuildingName(StringUtils.isBlank(addressForm.getBuildingName())?addressModel.getBuildingName():addressForm.getBuildingName());
		addressForm.setTitleCode(StringUtils.isBlank(addressForm.getTitleCode())?addressModel.getTitle() == null ? null : addressModel.getTitle().getCode():addressForm.getTitleCode());
		addressForm.setAddressName(StringUtils.isBlank(addressForm.getAddressName())?addressModel.getAddressName():addressForm.getAddressName());
		addressForm.setFirstName(StringUtils.isBlank(addressForm.getFirstName())?addressModel.getFirstname():addressForm.getFirstName());
		addressForm.setLastName(StringUtils.isBlank(addressForm.getLastName())?addressModel.getLastname():addressForm.getLastName());
		addressForm.setLine1(StringUtils.isBlank(addressForm.getLine1())?addressModel.getLine1():addressForm.getLine1());
		addressForm.setLine2(StringUtils.isBlank(addressForm.getLine2())?addressModel.getLine2():addressForm.getLine2());
		addressForm.setMobile(StringUtils.isBlank(addressForm.getMobile())?addressModel.getMobile():addressForm.getMobile());
		addressForm.setMobileCountry(StringUtils.isBlank(addressForm.getMobileCountry())?(addressModel.getMobileCountry() == null ? null : addressModel.getMobileCountry().getIsocode()):addressForm.getMobileCountry());
		addressForm.setNearestLandmark(StringUtils.isBlank(addressForm.getNearestLandmark())?addressModel.getNearestLandmark():addressForm.getNearestLandmark());
		addressForm.setStreetName(StringUtils.isBlank(addressForm.getStreetName())?addressModel.getCustomStreetName():addressForm.getStreetName());
		addressForm.setCountryIso(StringUtils.isBlank(addressForm.getCountryIso())?(addressModel.getCountry() == null ? null : addressModel.getCountry().getIsocode()):addressForm.getCountryIso());
		addressForm.setAddressId(addressModel.getPk().toString());
		addressForm.setChosenAddressId(addressModel.getPk().toString());
		addressForm.setDefaultAddress(userFacade.isDefaultAddress(addressModel.getPk().toString()));
		addressForm.setSaveInAddressBook(addressModel.getVisibleInAddressBook());
		return addressForm;
	}

	private void validateCity(final AddressForm addressForm, final Errors errors)
	{

	}

	private void validateArea(final AddressForm addressForm, final Errors errors)
	{

	}

	protected void validateStandardFields(final AddressForm addressForm, final Errors errors)
	{
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();

		validateStringField(addressForm.getTitleCode(), AddressField.TITLE, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getCountryIso(), AddressField.COUNTRY, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getFirstName(), AddressField.FIRSTNAME, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getLastName(), AddressField.LASTNAME, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getLine1(), AddressField.LINE1, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getMobile(), AddressField.MOBILE, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getMobileCountry(), AddressField.MOBILECOUNTRY, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getAddressName(), AddressField.ADDRESS_NAME, MAX_FIELD_LENGTH, errors);
		if (currentSite != null && currentSite.isCustomStreetNameAddressEnabled()
				&& currentSite.isCustomStreetNameAddressRequired())
		{
			validateStringField(addressForm.getStreetName(), AddressField.STREET_NAME, MAX_FIELD_LENGTH, errors);
		}
		if (currentSite != null && currentSite.isBuildingNameAddressEnabled() && currentSite.isBuildingNameAddressRequired())
		{
			validateStringField(addressForm.getBuildingName(), AddressField.BUILDING_NAME, MAX_FIELD_LENGTH, errors);
		}
		if (currentSite != null && currentSite.isApartmentNumberAddressEnabled() && currentSite.isApartmentNumberAddressRequired())
		{
			validateStringField(addressForm.getApartmentNumber(), AddressField.APARTMENT_NUMBER, MAX_FIELD_LENGTH, errors);
		}
		vaildateMobile(addressForm, errors);
	}

	protected void vaildateMobile(final AddressForm addressForm, final Errors errors)
	{
		if (addressForm.getMobileCountry() != null && !StringUtils.isEmpty(addressForm.getMobile()))
		{
			addressForm.setMobile(addressForm.getMobile());
			final Optional<String> normalizedPhoneNumber = mobilePhoneService
					.validateAndNormalizePhoneNumberByIsoCode(addressForm.getMobileCountry(), addressForm.getMobile());

			if (normalizedPhoneNumber.isEmpty())
			{
				errors.rejectValue("mobile", "register.mobileNumber.format.invalid");
				errors.rejectValue("mobileCountry", "register.countryMobileNumber.invalid");
			}
			else
			{
				addressForm.setMobile(normalizedPhoneNumber.get());
			}

		}
	}

	protected void validateCountrySpecificFields(final AddressForm addressForm, final Errors errors)
	{
		final String isoCode = addressForm.getCountryIso();
		if (isoCode != null)
		{
			switch (CountryCode.lookup(isoCode))
			{
				case CHINA:

					validateFieldNotNull(addressForm.getRegionIso(), AddressField.REGION, errors);
					break;
				case CANADA:

					validateFieldNotNull(addressForm.getRegionIso(), AddressField.REGION, errors);
					break;
				case USA:

					validateFieldNotNull(addressForm.getRegionIso(), AddressField.REGION, errors);
					break;
				case JAPAN:
					validateFieldNotNull(addressForm.getRegionIso(), AddressField.REGION, errors);
					validateStringField(addressForm.getLine2(), AddressField.LINE2, MAX_FIELD_LENGTH, errors);
					break;
				default:



					validateStringField(addressForm.getCityCode(), AddressField.CITY, MAX_FIELD_LENGTH, errors);
					final boolean isAreaRequired = isAreaRequired();
					if (isAreaRequired)
					{
						validateStringField(addressForm.getAreaCode(), AddressField.AREA, MAX_FIELD_LENGTH, errors);

					}
					break;
			}
		}
	}

	/**
	 * @return
	 */
	private boolean isAreaRequired()
	{
		final String siteID = cmsSiteService.getCurrentSite() == null ? "" : cmsSiteService.getCurrentSite().getUid();

		return configurationService.getConfiguration().getBoolean("website.address.area.required." + siteID);
	}

	protected static void validateStringField(final String addressField, final AddressField fieldType, final int maxFieldLength,
			final Errors errors)
	{
		if (addressField == null || StringUtils.isEmpty(addressField) || (StringUtils.length(addressField) > maxFieldLength))
		{
			errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
		}
	}

	protected static void validateStringFieldLength(final String field, final AddressField fieldType, final int maxFieldLength,
			final Errors errors)
	{
		if (StringUtils.isNotEmpty(field) && StringUtils.length(field) > maxFieldLength)
		{
			errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
		}
	}

	protected static void validateFieldNotNull(final String addressField, final AddressField fieldType, final Errors errors)
	{
		if (addressField == null)
		{
			errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
		}
	}

	protected enum CountryCode
	{
		USA("US"), CANADA("CA"), JAPAN("JP"), CHINA("CN"), BRITAIN("GB"), GERMANY("DE"), DEFAULT("");

		private final String isoCode;

		private static Map<String, CountryCode> lookupMap = new HashMap<String, CountryCode>();
		static
		{
			for (final CountryCode code : CountryCode.values())
			{
				lookupMap.put(code.getIsoCode(), code);
			}
		}

		private CountryCode(final String isoCodeStr)
		{
			this.isoCode = isoCodeStr;
		}

		public static CountryCode lookup(final String isoCodeStr)
		{
			CountryCode code = lookupMap.get(isoCodeStr);
			if (code == null)
			{
				code = DEFAULT;
			}
			return code;
		}

		public String getIsoCode()
		{
			return isoCode;
		}
	}

	protected enum AddressField
	{
		TITLE("titleCode", "address.title.invalid"), FIRSTNAME("firstName", "address.firstName.invalid"), LASTNAME("lastName",
				"address.lastName.invalid"), LINE1("line1", "address.line1.invalid"), LINE2("line2", "address.line2.invalid"), TOWN(
						"townCity", "address.townCity.invalid"), POSTCODE("postcode", "address.postcode.invalid"), REGION("regionIso",
								"address.regionIso.invalid"), COUNTRY("countryIso", "address.country.invalid"), CITY("cityCode",
										"address.city.invalid"), AREA("areaCode", "address.area.invalid"), MOBILECOUNTRY("mobileCountry",
												"address.mobile.country.filed.error"), MOBILE("mobile",
														"address.mobile.filed.error"), ADDRESS_NAME("addressName",
																"address.addressName.invalid"), BUILDING_NAME("buildingName",
																		"address.buildingName.invalid"), STREET_NAME("streetName",
																				"address.streetName.invalid"), APARTMENT_NUMBER(
																								"apartmentNumber",
																						"address.apartmentNumber.invalid");


		private final String fieldKey;
		private final String errorKey;

		private AddressField(final String fieldKey, final String errorKey)
		{
			this.fieldKey = fieldKey;
			this.errorKey = errorKey;
		}

		public String getFieldKey()
		{
			return fieldKey;
		}

		public String getErrorKey()
		{
			return errorKey;
		}
	}
}
