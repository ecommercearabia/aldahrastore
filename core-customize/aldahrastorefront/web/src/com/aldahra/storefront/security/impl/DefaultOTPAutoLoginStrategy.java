/**
 *
 */
package com.aldahra.storefront.security.impl;

import de.hybris.platform.acceleratorstorefrontcommons.security.GUIDCookieStrategy;
import de.hybris.platform.commercefacades.customer.CustomerFacade;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.RememberMeServices;

import com.aldahra.storefront.security.OTPAutoLoginStrategy;
import com.aldahra.storefront.security.token.OTPAuthenticationToken;


/**
 * @author monzer
 *
 */
public class DefaultOTPAutoLoginStrategy implements OTPAutoLoginStrategy
{

	private static final Logger LOG = Logger.getLogger(DefaultOTPAutoLoginStrategy.class);

	private CustomerFacade customerFacade;
	private GUIDCookieStrategy guidCookieStrategy;
	private RememberMeServices rememberMeServices;

	@Resource(name = "authenticationManager")
	private AuthenticationManager authManager;

	@Override
	public void login(final Object otpData, final HttpServletRequest request, final HttpServletResponse response)
	{

		final Authentication authenticationToken = new OTPAuthenticationToken(otpData);
		try
		{
			final Authentication authentication = getAuthenticationManager().authenticate(authenticationToken);
			SecurityContextHolder.getContext().setAuthentication(authentication);
			getCustomerFacade().loginSuccess();
			getGuidCookieStrategy().setCookie(request, response);
			getRememberMeServices().loginSuccess(request, response, authenticationToken);
		}
		catch (final Exception e)
		{
			LOG.error(e.getMessage());
			SecurityContextHolder.getContext().setAuthentication(null);
		}
	}

	protected AuthenticationManager getAuthenticationManager()
	{
		// XXX Auto-generated method stub
		return authManager;
	}

	public void setAuthenticationManager(final AuthenticationManager authenticationManager)
	{
		// XXX Auto-generated method stub
		this.authManager = authenticationManager;
	}

	protected CustomerFacade getCustomerFacade()
	{
		return customerFacade;
	}

	@Required
	public void setCustomerFacade(final CustomerFacade customerFacade)
	{
		this.customerFacade = customerFacade;
	}

	protected GUIDCookieStrategy getGuidCookieStrategy()
	{
		return guidCookieStrategy;
	}

	@Required
	public void setGuidCookieStrategy(final GUIDCookieStrategy guidCookieStrategy)
	{
		this.guidCookieStrategy = guidCookieStrategy;
	}

	protected RememberMeServices getRememberMeServices()
	{
		return rememberMeServices;
	}

	@Required
	public void setRememberMeServices(final RememberMeServices rememberMeServices)
	{
		this.rememberMeServices = rememberMeServices;
	}

}
