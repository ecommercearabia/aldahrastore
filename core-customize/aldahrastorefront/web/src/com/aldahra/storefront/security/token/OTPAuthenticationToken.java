/**
 *
 */
package com.aldahra.storefront.security.token;

import de.hybris.platform.commercefacades.user.data.OTPData;

import org.springframework.security.authentication.AbstractAuthenticationToken;


/**
 * @author monzer
 *
 */
public class OTPAuthenticationToken extends AbstractAuthenticationToken
{

	Object otpData;
	Object uid;
	Object credentials;
	Object details;


	/**
	 * @param arg0
	 */
	public OTPAuthenticationToken(final Object otpData)
	{
		super(null);
		final OTPData data = (OTPData) otpData;
		this.uid = data.getUid();
		this.credentials = data.getOtpCode();
		this.details = data.getMobileNumber();
		setAuthenticated(false);
	}

	/**
	 * @param arg0
	 * @param uid
	 * @param credentials
	 * @param detials
	 */
	public OTPAuthenticationToken(final Object uid, final Object credentials, final Object detials)
	{
		super(null);
		this.uid = uid;
		this.credentials = credentials;
		this.details = detials;
	}

	@Override
	public Object getCredentials()
	{
		return this.credentials;
	}

	@Override
	public Object getPrincipal()
	{
		return this.uid;
	}

	@Override
	public Object getDetails()
	{
		return this.details;
	}

}
