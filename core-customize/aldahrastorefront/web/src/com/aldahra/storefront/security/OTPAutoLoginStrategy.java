/**
 *
 */
package com.aldahra.storefront.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author monzer
 *
 */
public interface OTPAutoLoginStrategy
{

	void login(Object username, HttpServletRequest request, final HttpServletResponse response);

}
