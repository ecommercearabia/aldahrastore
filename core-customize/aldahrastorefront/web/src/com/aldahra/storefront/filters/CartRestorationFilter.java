/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.storefront.filters;

import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.util.Config;

import java.io.IOException;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.filter.OncePerRequestFilter;

import com.aldahra.core.service.CartHistoryService;
import com.aldahra.storefront.security.cookie.CartRestoreCookieGenerator;


/**
 * Filter that the restores the user's cart. This is a spring configured filter that is executed by the
 * PlatformFilterChain.
 */
public class CartRestorationFilter extends OncePerRequestFilter
{
	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(CartRestorationFilter.class);

	private static final String CART_HISTORY_ALLOWED_URL_PATTERNS = "cart.history.allowed.url.patterns";

	private CartRestoreCookieGenerator cartRestoreCookieGenerator;
	private CartService cartService;
	private CartFacade cartFacade;
	private BaseSiteService baseSiteService;
	private UserService userService;
	private SessionService sessionService;

	@Resource(name = "cartHistoryService")
	private CartHistoryService cartHistoryService;

	@Override
	public void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
			final FilterChain filterChain) throws IOException, ServletException
	{
		if (getUserService().isAnonymousUser(getUserService().getCurrentUser()))
		{
			processAnonymousUser(request, response);
		}
		else
		{
			restoreCartWithNoCode();
		}
		if (getCartService().hasSessionCart() && checkCartHistoryValidRequest(request))
		{
			final CartModel cartModel = cartService.getSessionCart();
			cartHistoryService.updateCartHistory(cartModel, SalesApplication.WEB, getPath(request));
		}
		filterChain.doFilter(request, response);
	}

	/**
	 * @param request
	 * @return
	 */
	protected boolean checkCartHistoryValidRequest(final HttpServletRequest request)
	{
		if (request == null || !request.getMethod().equalsIgnoreCase("post"))
		{
			LOG.info("Request is null or not a post request");
			return false;
		}
		final String protectionExcludeUrls = Config.getParameter(CART_HISTORY_ALLOWED_URL_PATTERNS);
		LOG.info("CartRestorationFilter: Cart listener patterns are: {}", protectionExcludeUrls);
		final Set<String> protectionExcludeUrlSet = org.springframework.util.StringUtils
				.commaDelimitedListToSet(protectionExcludeUrls);
		if (CollectionUtils.isEmpty(protectionExcludeUrlSet) || request == null)
		{
			LOG.info("CartRestorationFilter: Cart listener patterns are empty because property was not found!");
			return false;
		}
		LOG.info("CartRestorationFilter: Request Path URL {}", request.getRequestURI());
		for (final String pattern : protectionExcludeUrlSet)
		{
			if (request.getRequestURI() != null && request.getRequestURI().matches(pattern))
			{
				LOG.info("CartRestorationFilter: Cart operation occured for this URL {}", request.getRequestURI());
				return true;
			}
		}
		return false;
	}

	protected String getPath(final HttpServletRequest request)
	{
		return StringUtils.defaultString(request.getRequestURI());
	}

	protected void restoreCartWithNoCode()
	{
		if ((!getCartService().hasSessionCart() && getSessionService().getAttribute(WebConstants.CART_RESTORATION) == null)
				|| (getCartService().hasSessionCart() && !getBaseSiteService().getCurrentBaseSite()
						.equals(getBaseSiteService().getBaseSiteForUID(getCartService().getSessionCart().getSite().getUid()))))
		{
			getSessionService().setAttribute(WebConstants.CART_RESTORATION_SHOW_MESSAGE, Boolean.TRUE);
			try
			{
				getSessionService().setAttribute(WebConstants.CART_RESTORATION, getCartFacade().restoreSavedCart(null));
			}
			catch (final CommerceCartRestorationException e)
			{
				if (LOG.isDebugEnabled())
				{
					LOG.debug(e.getMessage());
				}
				getSessionService().setAttribute(WebConstants.CART_RESTORATION, WebConstants.CART_RESTORATION_ERROR_STATUS);
			}
		}
	}

	protected void processAnonymousUser(final HttpServletRequest request, final HttpServletResponse response)
	{
		if (getCartService().hasSessionCart() && getBaseSiteService().getCurrentBaseSite()
				.equals(getBaseSiteService().getBaseSiteForUID(getCartService().getSessionCart().getSite().getUid())))
		{
			final String guid = getCartService().getSessionCart().getGuid();

			if (!StringUtils.isEmpty(guid))
			{
				getCartRestoreCookieGenerator().addCookie(response, guid);
			}
		}
		else if (request.getSession().isNew() || (getCartService().hasSessionCart() && !getBaseSiteService().getCurrentBaseSite()
				.equals(getBaseSiteService().getBaseSiteForUID(getCartService().getSessionCart().getSite().getUid()))))
		{
			processRestoration(request);
		}
	}

	protected void processRestoration(final HttpServletRequest request)
	{
		String cartGuid = null;

		if (request.getCookies() != null)
		{
			final String anonymousCartCookieName = getCartRestoreCookieGenerator().getCookieName();

			for (final Cookie cookie : request.getCookies())
			{
				if (anonymousCartCookieName.equals(cookie.getName()))
				{
					cartGuid = cookie.getValue();
					break;
				}
			}
		}

		if (!StringUtils.isEmpty(cartGuid))
		{
			getSessionService().setAttribute(WebConstants.CART_RESTORATION_SHOW_MESSAGE, Boolean.TRUE);
			try
			{
				getSessionService().setAttribute(WebConstants.CART_RESTORATION, getCartFacade().restoreSavedCart(cartGuid));
			}
			catch (final CommerceCartRestorationException e)
			{
				if (LOG.isDebugEnabled())
				{
					LOG.debug(e.getMessage());
				}
				getSessionService().setAttribute(WebConstants.CART_RESTORATION_ERROR_STATUS,
						WebConstants.CART_RESTORATION_ERROR_STATUS);
			}
		}
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	protected CartRestoreCookieGenerator getCartRestoreCookieGenerator()
	{
		return cartRestoreCookieGenerator;
	}

	@Required
	public void setCartRestoreCookieGenerator(final CartRestoreCookieGenerator cartRestoreCookieGenerator)
	{
		this.cartRestoreCookieGenerator = cartRestoreCookieGenerator;
	}

	protected CartFacade getCartFacade()
	{
		return cartFacade;
	}

	@Required
	public void setCartFacade(final CartFacade cartFacade)
	{
		this.cartFacade = cartFacade;
	}

	protected BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	@Required
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected CartService getCartService()
	{
		return cartService;
	}

	@Required
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}
}
