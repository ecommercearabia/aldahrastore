/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.storefront.checkout.steps.validation.impl;



import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.commercefacades.order.data.CartData;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.aldahra.aldahratimeslotfacades.exception.TimeSlotException;
import com.aldahra.facades.exception.ExpressOrderException;
import com.aldahra.facades.facade.CustomAcceleratorCheckoutFacade;
import com.aldahra.facades.facade.CustomCheckoutFlowFacade;
import com.aldahra.storefront.checkout.steps.validation.AbstractCheckoutStepValidator;
import com.aldahra.storefront.checkout.steps.validation.ValidationResults;



public class ResponsiveSummaryCheckoutStepValidator extends AbstractCheckoutStepValidator
{
	private static final Logger LOGGER = Logger.getLogger(ResponsiveSummaryCheckoutStepValidator.class);

	@Resource(name = "customCheckoutFlowFacade")
	private CustomCheckoutFlowFacade customCheckoutFlowFacade;

	@Resource(name = "defaultAcceleratorCheckoutFacade")
	private CustomAcceleratorCheckoutFacade defaultAcceleratorCheckoutFacade;


	@Resource(name = "acceleratorCheckoutFacade")
	private CustomAcceleratorCheckoutFacade customAcceleratorCheckoutFacade;


	@Override
	public ValidationResults validateOnEnter(final RedirectAttributes redirectAttributes)
	{
		final ValidationResults cartResult = checkCartAndDelivery(redirectAttributes);
		if (cartResult != null)
		{
			return cartResult;
		}
		if (getCheckoutFacade().hasShippingItems() && getCheckoutFlowFacade().hasNoDeliveryAddress())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"checkout.multi.deliveryAddress.notprovided");
			return ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS;
		}

		if (getCheckoutFlowFacade().hasNoDeliveryMode())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"checkout.multi.deliveryMethod.notprovided");
			return ValidationResults.REDIRECT_TO_DELIVERY_METHOD;
		}
		if (getCustomCheckoutFlowFacade().hasNoPaymentMode())
		{
			return ValidationResults.REDIRECT_TO_PAYMENT_METHOD;
		}

		if ("card".equalsIgnoreCase(getCustomAcceleratorCheckoutFacade().getCheckoutCart().getPaymentMode().getCode())
				&& !getCustomCheckoutFlowFacade().hasPaymentProvider())
		{
			return ValidationResults.REDIRECT_TO_PAYMENT_METHOD;
		}

		if (!getCheckoutFacade().containsTaxValues())
		{
			LOGGER.error(String.format(
					"Cart %s does not have any tax values, which means the tax cacluation was not properly done, placement of order can't continue",
					getCheckoutFacade().getCheckoutCart().getCode()));
			return ValidationResults.REDIRECT_TO_PAYMENT_METHOD;
		}

		if (!getCheckoutFacade().getCheckoutCart().isCalculated())
		{
			LOGGER.error(String.format("Cart %s has a calculated flag of FALSE, placement of order can't continue",
					getCheckoutFacade().getCheckoutCart().getCode()));
			return ValidationResults.REDIRECT_TO_PAYMENT_METHOD;
		}


		try
		{
			if (getDefaultAcceleratorCheckoutFacade().isExpressOrder())
			{
				return ValidationResults.SUCCESS;
			}
		}
		catch (final ExpressOrderException e1)
		{
		}

		try
		{
			if (getCustomCheckoutFlowFacade().hasNoTimeSlot())
			{
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
						"checkout.multi.timeSlot.notprovided");
				return ValidationResults.REDIRECT_TO_TIME_SLOT;
			}
		}
		catch (final TimeSlotException e)
		{
			switch (e.getTimeSlotExceptionType())
			{
				case NO_DELIVERY_AREA_SELECTED:
				case NO_DELIVERY_METHOD_SELECTED:
				case NO_TIMESLOT_CONFIGURATIONS_AVAILABLE:
					GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
							"checkout.multi.timeSlot.notprovided");
					return ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS;

				case INVALID_CHOSEN_TIMESLOT:
					GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
							"checkout.multi.timeSlot.invalid");
					return ValidationResults.REDIRECT_TO_TIME_SLOT;


			}
		}

		return ValidationResults.SUCCESS;
	}

	protected ValidationResults checkPaymentMethodAndPickup(final RedirectAttributes redirectAttributes)
	{
		if (getCustomCheckoutFlowFacade().hasNoPaymentInfo())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"checkout.multi.paymentDetails.notprovided");
			return ValidationResults.REDIRECT_TO_PAYMENT_METHOD;
		}

		final CartData cartData = getCustomAcceleratorCheckoutFacade().getCheckoutCart();

		if (!getCustomAcceleratorCheckoutFacade().hasShippingItems())
		{
			cartData.setDeliveryAddress(null);
		}

		if (!getCustomAcceleratorCheckoutFacade().hasPickUpItems() && "pickup".equals(cartData.getDeliveryMode().getCode()))
		{
			return ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS;
		}
		return null;
	}

	protected ValidationResults checkCartAndDelivery(final RedirectAttributes redirectAttributes)
	{
		if (!getCustomCheckoutFlowFacade().hasValidCart())
		{
			LOGGER.info("Missing, empty or unsupported cart");
			return ValidationResults.REDIRECT_TO_CART;
		}

		if (getCheckoutFlowFacade().hasNoDeliveryAddress())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"checkout.multi.deliveryAddress.notprovided");
			return ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS;
		}

		if (getCheckoutFlowFacade().hasNoDeliveryMode())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"checkout.multi.deliveryMethod.notprovided");
			return ValidationResults.REDIRECT_TO_DELIVERY_METHOD;
		}
		return null;
	}

	public CustomCheckoutFlowFacade getCustomCheckoutFlowFacade()
	{
		return customCheckoutFlowFacade;
	}


	public CustomAcceleratorCheckoutFacade getCustomAcceleratorCheckoutFacade()
	{
		return customAcceleratorCheckoutFacade;
	}

	public CustomAcceleratorCheckoutFacade getDefaultAcceleratorCheckoutFacade()
	{
		return defaultAcceleratorCheckoutFacade;
	}



}
