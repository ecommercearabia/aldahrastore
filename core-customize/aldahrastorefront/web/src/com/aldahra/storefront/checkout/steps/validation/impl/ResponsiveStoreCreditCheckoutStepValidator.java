package com.aldahra.storefront.checkout.steps.validation.impl;


import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.order.CartService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.math.BigDecimal;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.aldahra.aldahrastorecredit.service.StoreCreditService;
import com.aldahra.facades.facade.CustomCheckoutFlowFacade;
import com.aldahra.storefront.checkout.steps.validation.AbstractCheckoutStepValidator;
import com.aldahra.storefront.checkout.steps.validation.ValidationResults;


public class ResponsiveStoreCreditCheckoutStepValidator extends AbstractCheckoutStepValidator
{
	private static final Logger LOGGER = Logger.getLogger(ResponsiveStoreCreditCheckoutStepValidator.class);

	@Resource(name = "customCheckoutFlowFacade")
	private CustomCheckoutFlowFacade customCheckoutFlowFacade;

	@Resource(name = "storeCreditService")
	private StoreCreditService storeCreditService;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	private CartService cartService;

	public CustomCheckoutFlowFacade getCustomCheckoutFlowFacade()
	{
		return customCheckoutFlowFacade;
	}

	@Override
	public ValidationResults validateOnEnter(final RedirectAttributes redirectAttributes)
	{
		if (!getCustomCheckoutFlowFacade().hasValidCart())
		{
			LOGGER.info("Missing, empty or unsupported cart");
			return ValidationResults.REDIRECT_TO_CART;
		}

		if (getCheckoutFacade().hasShippingItems() && getCheckoutFlowFacade().hasNoDeliveryAddress())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"checkout.multi.deliveryAddress.notprovided");
			return ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS;
		}

		if (getCheckoutFlowFacade().hasNoDeliveryMode())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"checkout.multi.deliveryMethod.notprovided");
			return ValidationResults.REDIRECT_TO_DELIVERY_METHOD;
		}
		final BaseStoreModel baseStore = baseStoreService.getCurrentBaseStore();

		if (!Boolean.TRUE.equals(baseStore.getStoreCreditEnabled()))
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"checkout.multi.storeCredit.notEnabled");
			return ValidationResults.REDIRECT_TO_PAYMENT_METHOD;
		}

		final BigDecimal storeCreditAmount = storeCreditService.getStoreCreditAmountByCurrentUserAndCurrentBaseStore();

		if (storeCreditAmount == null || storeCreditAmount.doubleValue() <= 0)
		{
			return ValidationResults.REDIRECT_TO_PAYMENT_METHOD;
		}

		return ValidationResults.SUCCESS;
	}
}
