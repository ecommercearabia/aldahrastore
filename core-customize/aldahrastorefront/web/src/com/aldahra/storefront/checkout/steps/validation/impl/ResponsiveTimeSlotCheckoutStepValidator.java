/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.storefront.checkout.steps.validation.impl;


import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.aldahra.aldahratimeslotfacades.exception.TimeSlotException;
import com.aldahra.aldahratimeslotfacades.validation.TimeSlotValidationService;
import com.aldahra.facades.exception.ExpressOrderException;
import com.aldahra.facades.facade.CustomAcceleratorCheckoutFacade;
import com.aldahra.facades.facade.CustomCheckoutFlowFacade;
import com.aldahra.storefront.checkout.steps.validation.AbstractCheckoutStepValidator;
import com.aldahra.storefront.checkout.steps.validation.ValidationResults;


public class ResponsiveTimeSlotCheckoutStepValidator extends AbstractCheckoutStepValidator
{
	private static final Logger LOGGER = Logger.getLogger(ResponsiveTimeSlotCheckoutStepValidator.class);

	@Resource(name = "customCheckoutFlowFacade")
	private CustomCheckoutFlowFacade customCheckoutFlowFacade;

	@Resource(name = "timeSlotValidationService")
	private TimeSlotValidationService timeSlotValidationService;

	@Resource(name = "defaultAcceleratorCheckoutFacade")
	private CustomAcceleratorCheckoutFacade defaultAcceleratorCheckoutFacade;


	public CustomCheckoutFlowFacade getCustomCheckoutFlowFacade()
	{
		return customCheckoutFlowFacade;
	}

	@Override
	public ValidationResults validateOnEnter(final RedirectAttributes redirectAttributes)
	{
		if (!getCustomCheckoutFlowFacade().hasValidCart())
		{
			LOGGER.info("Missing, empty or unsupported cart");
			return ValidationResults.REDIRECT_TO_CART;
		}

		if (getCheckoutFacade().hasShippingItems() && getCheckoutFlowFacade().hasNoDeliveryAddress())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"checkout.multi.deliveryAddress.notprovided");
			return ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS;
		}

		if (getCheckoutFlowFacade().hasNoDeliveryMode())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"checkout.multi.deliveryMethod.notprovided");
			return ValidationResults.REDIRECT_TO_DELIVERY_METHOD;
		}

		try
		{
			if (!getCustomCheckoutFlowFacade().isTimeSlotEnabledByCurrentSite())
			{
				return ValidationResults.REDIRECT_TO_PAYMENT_METHOD;
			}

		}
		catch (final TimeSlotException e)
		{
			switch (e.getTimeSlotExceptionType())
			{
				case NO_DELIVERY_AREA_SELECTED:
				case NO_DELIVERY_METHOD_SELECTED:
				case NO_TIMESLOT_CONFIGURATIONS_AVAILABLE:
					GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
							"checkout.multi.timeSlot.notprovided");
					return ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS;

				case INVALID_CHOSEN_TIMESLOT:
					GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
							"checkout.multi.timeSlot.invalid");
					return ValidationResults.REDIRECT_TO_TIME_SLOT;
			}
		}

		return ValidationResults.SUCCESS;
	}

	@Override
	public ValidationResults validateOnExit()
	{

		try
		{
			if (getDefaultAcceleratorCheckoutFacade().isExpressOrder())
			{
				return ValidationResults.SUCCESS;
			}
		}
		catch (final ExpressOrderException e1)
		{
		}

		try
		{
			if (!getCustomCheckoutFlowFacade().isTimeSlotEnabledByCurrentSite() || getCustomCheckoutFlowFacade().hasNoTimeSlot())
			{
				return ValidationResults.SUCCESS;
			}
		}
		catch (final TimeSlotException e)
		{
			switch (e.getTimeSlotExceptionType())
			{
				case NO_DELIVERY_AREA_SELECTED:
				case NO_DELIVERY_METHOD_SELECTED:
				case NO_TIMESLOT_CONFIGURATIONS_AVAILABLE:
					return ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS;
				case INVALID_CHOSEN_TIMESLOT:
					return ValidationResults.REDIRECT_TO_TIME_SLOT;
			}
		}

		return ValidationResults.SUCCESS;
	}

	/**
	 * @return the defaultAcceleratorCheckoutFacade
	 */
	protected CustomAcceleratorCheckoutFacade getDefaultAcceleratorCheckoutFacade()
	{
		return defaultAcceleratorCheckoutFacade;
	}


}
