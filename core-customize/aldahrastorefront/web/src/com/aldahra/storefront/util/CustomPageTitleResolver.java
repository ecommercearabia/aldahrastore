/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.storefront.util;

import de.hybris.platform.acceleratorservices.storefront.util.PageTitleResolver;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.core.model.product.ProductModel;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;


/**
 * Resolves page title according to page, search text, current category or product
 */
public class CustomPageTitleResolver extends PageTitleResolver
{

	@Override
	public String resolveCategoryPageTitle(final CategoryModel category)
	{
		final StringBuilder stringBuilder = new StringBuilder();

		final String firstTitleWord = StringUtils.isNotBlank(category.getSeoCategorytTitle()) ? category.getSeoCategorytTitle()
				: category.getName();

		stringBuilder.append(firstTitleWord).append(TITLE_WORD_SEPARATOR);

		final CMSSiteModel currentSite = getCmsSiteService().getCurrentSite();
		if (currentSite != null)
		{
			stringBuilder.append(currentSite.getName());
		}

		return StringEscapeUtils.escapeHtml(stringBuilder.toString());
	}

	/**
	 * creates page title for given code
	 */
	@Override
	public String resolveProductPageTitle(final ProductModel product)
	{
		// Lookup categories
		// Lookup site (or store)
		final CMSSiteModel currentSite = getCmsSiteService().getCurrentSite();

		// Construct page title
		final String identifier = product.getName();
		final String articleNumber = product.getCode();
		final String productName = StringUtils.isEmpty(identifier) ? articleNumber : identifier;
		final String firstTitleWord = StringUtils.isNotBlank(product.getSeoProductTitle()) ? product.getSeoProductTitle()
				: productName;
		final StringBuilder builder = new StringBuilder(firstTitleWord);

		if (currentSite != null)
		{
			builder.append(TITLE_WORD_SEPARATOR).append(currentSite.getName());
		}


		return StringEscapeUtils.escapeHtml(builder.toString());
	}

}
