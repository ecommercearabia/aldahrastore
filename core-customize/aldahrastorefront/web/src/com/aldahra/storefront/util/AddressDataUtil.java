package com.aldahra.storefront.util;

import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.AreaData;
import de.hybris.platform.commercefacades.user.data.CityData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import com.aldahra.aldahrauserfacades.area.facade.AreaFacade;
import com.aldahra.aldahrauserfacades.city.facade.CityFacade;
import com.aldahra.aldahrauserfacades.country.facade.CountryFacade;
import com.aldahra.storefront.form.AddressForm;



/**
 * The Class AddressDataUtil.
 *
 * @author mnasro
 */
@Component("customAddressDataUtil")
public class AddressDataUtil
{

	/** The user facade. */
	@Resource(name = "userFacade")
	private UserFacade userFacade;

	/** The user service. */
	@Resource(name = "userService")
	private UserService userService;

	/** The i 18 N facade. */
	@Resource(name = "i18NFacade")
	private I18NFacade i18NFacade;

	/** The customer facade. */
	@Resource(name = "customCustomerFacade")
	private CustomerFacade customerFacade;

	/** The city facade. */
	@Resource(name = "cityFacade")
	private CityFacade cityFacade;

	/** The country facade. */
	@Resource(name = "countryFacade")
	private CountryFacade countryFacade;

	/** The area facade. */
	@Resource(name = "areaFacade")
	private AreaFacade areaFacade;

	/** The address reverse converter. */
	@Resource(name = "addressReverseConverter")
	private Converter<AddressData, AddressModel> addressReverseConverter;


	/**
	 * fill customer data to address form.
	 *
	 * @param addressForm
	 *           the address form
	 * @param countryIsoCode
	 *           the country iso code
	 * @return the prepared address form for current customer data
	 */

	public void getPreparedAddressFormForCurrentCustomerData(final AddressForm addressForm, final String countryIsoCode)
	{
		final CustomerData customerData = customerFacade.getCurrentCustomer();
		if (!userService.isAnonymousUser(userService.getCurrentUser()))
		{
			addressForm.setFirstName(customerData.getFirstName());
			addressForm.setLastName(customerData.getLastName());
			addressForm.setTitleCode(customerData.getTitleCode());
			addressForm.setMobile(customerData.getMobileNumber());
			addressForm
					.setMobileCountry(customerData.getMobileCountry() == null ? null : customerData.getMobileCountry().getIsocode());
		}
	}

	/**
	 * Gets the prepared address form for address data.
	 *
	 * @param addressForm
	 *           the address form
	 * @param addressData
	 *           the address data
	 * @return the prepared address form for address data
	 */
	public void getPreparedAddressFormForAddressData(final AddressForm addressForm, final AddressData addressData)
	{
		addressForm.setFirstName(addressData.getFirstName());
		addressForm.setLastName(addressData.getLastName());
		addressForm.setTitleCode(addressData.getTitleCode());
		addressForm.setLine1(addressData.getLine1());
		addressForm.setLine2(addressData.getLine2());
		addressForm.setPhone(addressData.getPhone());
		addressForm.setTownCity(addressData.getTown() == null ? null : addressData.getTown());
		addressForm.setCityCode(addressData.getCity() == null ? null : addressData.getCity().getCode());
		addressForm.setMobile(addressData.getMobileNumber());
		addressForm.setMobileCountry(addressData.getMobileCountry() == null ? null : addressData.getMobileCountry().getIsocode());
		addressForm.setCountryIso(addressData.getCountry() == null ? null : addressData.getCountry().getIsocode());
		addressForm.setPostcode(addressData.getPostalCode());
		addressForm.setAreaCode(addressData.getArea() == null ? null : addressData.getArea().getCode());

	}

	/**
	 * Convert basic.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	public void convertBasic(final AddressData source, final AddressForm target)
	{
		target.setAddressId(source.getId());
		target.setTitleCode(source.getTitleCode());
		target.setFirstName(source.getFirstName());
		target.setLastName(source.getLastName());
		target.setLine1(source.getLine1());
		target.setLine2(source.getLine2());
		target.setTownCity(source.getTown());
		target.setPostcode(source.getPostalCode());
		target.setCountryIso(source.getCountry().getIsocode());
		target.setAreaCode(source.getArea() == null ? null : source.getArea().getCode());
		target.setCityCode(source.getCity() == null ? null : source.getCity().getCode());
		target.setMobile(source.getMobileNumber());
		target.setMobileCountry(source.getMobileCountry() == null ? null : source.getMobileCountry().getIsocode());

	}

	/**
	 * Convert.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	public void convert(final AddressData source, final AddressForm target)
	{
		convertBasic(source, target);
		target.setSaveInAddressBook(Boolean.valueOf(source.isVisibleInAddressBook()));
		target.setShippingAddress(Boolean.valueOf(source.isShippingAddress()));
		target.setBillingAddress(Boolean.valueOf(source.isBillingAddress()));
		target.setPhone(source.getPhone());
		target.setAddressName(source.getAddressName());
		target.setLatitude(source.getLatitude());
		target.setLongitude(source.getLongitude());
		target.setNearestLandmark(source.getNearestLandmark());
		target.setBuildingName(source.getBuildingName());
		target.setApartmentNumber(source.getApartmentNumber());
		target.setStreetName(source.getStreetName());
		if (source.getRegion() != null && !StringUtils.isEmpty(source.getRegion().getIsocode()))
		{
			target.setRegionIso(source.getRegion().getIsocode());
		}
	}

	/**
	 * Convert reverse.
	 *
	 * @param source
	 *           the source
	 * @return the address model
	 */
	public AddressModel convertReverse(final AddressData source)
	{
		return addressReverseConverter.convert(source);
	}

	/**
	 * Convert to visible address data.
	 *
	 * @param addressForm
	 *           the address form
	 * @return the address data
	 */
	public AddressData convertToVisibleAddressData(final AddressForm addressForm)
	{
		final AddressData addressData = convertToAddressData(addressForm);
		addressData.setVisibleInAddressBook(true);
		return addressData;
	}

	/**
	 * Convert to address data.
	 *
	 * @param addressForm
	 *           the address form
	 * @return the address data
	 */
	public AddressData convertToAddressData(final AddressForm addressForm)
	{
		final AddressData addressData = new AddressData();
		addressData.setId(addressForm.getAddressId());
		addressData.setTitleCode(addressForm.getTitleCode());
		addressData.setFirstName(addressForm.getFirstName());
		addressData.setLastName(addressForm.getLastName());
		addressData.setLine1(addressForm.getLine1());
		addressData.setLine2(addressForm.getLine2());
		addressData.setPostalCode(addressForm.getPostcode());
		addressData.setBillingAddress(false);
		addressData.setShippingAddress(true);
		addressData.setPhone(addressForm.getPhone());
		addressData.setMobileNumber(addressForm.getMobile());
		addressData.setTown(addressForm.getTownCity());
		addressData.setAddressName(addressForm.getAddressName());
		addressData.setNearestLandmark(addressForm.getNearestLandmark());
		addressData.setLatitude(addressForm.getLatitude());
		addressData.setLongitude(addressForm.getLongitude());
		addressData.setBuildingName(addressForm.getBuildingName());
		addressData.setStreetName(addressForm.getStreetName());
		addressData.setApartmentNumber(addressForm.getApartmentNumber());

		if (!StringUtils.isEmpty(addressForm.getCityCode()))
		{
			final Optional<CityData> city = cityFacade.get(addressForm.getCityCode());
			if (city.isPresent())
			{
				addressData.setCity(city.get());
			}

			addressData.setTown(addressData.getCity() == null ? null : addressData.getCity().getName());
		}

		if (!StringUtils.isEmpty(addressForm.getAreaCode()))
		{
			final Optional<AreaData> area = areaFacade.get(addressForm.getAreaCode());
			if (area.isPresent())
			{
				addressData.setArea(area.get());
			}
		}

		if (addressForm.getMobileCountry() != null)
		{
			final CountryData countryData = getI18NFacade().getCountryForIsocode(addressForm.getMobileCountry());
			addressData.setMobileCountry(countryData);
		}

		if (addressForm.getCountryIso() != null)
		{
			final CountryData countryData = getI18NFacade().getCountryForIsocode(addressForm.getCountryIso());
			addressData.setCountry(countryData);
		}
		if (addressForm.getRegionIso() != null && !StringUtils.isEmpty(addressForm.getRegionIso()))
		{
			final RegionData regionData = getI18NFacade().getRegion(addressForm.getCountryIso(), addressForm.getRegionIso());
			addressData.setRegion(regionData);
		}
		if (addressForm.getAddressId() != null)
		{
			addressData.setDefaultAddress(userFacade.isDefaultAddress(addressForm.getAddressId()));
		}

		return addressData;
	}


	/**
	 * Gets the user facade.
	 *
	 * @return the userFacade
	 */
	public UserFacade getUserFacade()
	{
		return userFacade;
	}


	/**
	 * Gets the i 18 N facade.
	 *
	 * @return the i18NFacade
	 */
	public I18NFacade getI18NFacade()
	{
		return i18NFacade;
	}


	/**
	 * Gets the customer facade.
	 *
	 * @return the customerFacade
	 */
	public CustomerFacade getCustomerFacade()
	{
		return customerFacade;
	}



}
