/**
 *
 */
package com.aldahra.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.product.data.PriceData;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aldahra.aldahrastorecredit.exception.StoreCreditException;
import com.aldahra.aldahrastorecreditfacades.StoreCreditHistoryData;
import com.aldahra.aldahrastorecreditfacades.facade.StoreCreditFacade;


/**
 * @author mnasro
 *
 */
@Controller
@Scope("tenant")
@RequestMapping(value = "/my-account/store-credit")
public class StoreCreditController extends AbstractPageController
{
	private static final String BREADCRUMBS_ATTR = "breadcrumbs";
	@Resource(name = "storeCreditFacade")
	private StoreCreditFacade storeCreditFacade;

	@Resource(name = "accountBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

	private static final String STORE_CREDIT_VIEW = "/my-account/store-credit";


	@RequestMapping(method = RequestMethod.GET)
	@RequireHardLogIn
	public String getStoreCreditPage(final Model model) throws CMSItemNotFoundException
	{
		Optional<List<StoreCreditHistoryData>> storeCreditHistoryData;
		try
		{
			storeCreditHistoryData = storeCreditFacade.getStoreCreditHistoryByCurrentUser();
		}
		catch (final StoreCreditException e)
		{
			GlobalMessages.addErrorMessage(model, "storeCredit.error.notAvailable");

			return REDIRECT_PREFIX + "/";
		}

		final Optional<List<StoreCreditHistoryData>> storeCreditHistoryByCurrentUser = storeCreditFacade
				.getStoreCreditHistoryByCurrentUser();
		if (storeCreditHistoryByCurrentUser.isPresent() && !storeCreditHistoryByCurrentUser.get().isEmpty())
		{
			final List<StoreCreditHistoryData> reversedStoreCreditHistoryData = storeCreditHistoryByCurrentUser.get();
			model.addAttribute("storeCreditHistories", reversedStoreCreditHistoryData);
		}
		final Optional<PriceData> storeCreditAmount = storeCreditFacade.getStoreCreditAmountByCurrentUserAndCurrentBaseStore();

		model.addAttribute("storeCreditAmount", storeCreditAmount.isPresent() ? storeCreditAmount.get() : null);
		storeCmsPageInModel(model, getContentPageForLabelOrId(STORE_CREDIT_VIEW));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(STORE_CREDIT_VIEW));
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs("text.account.storecredit"));
		return getViewForPage(model);

	}
}
