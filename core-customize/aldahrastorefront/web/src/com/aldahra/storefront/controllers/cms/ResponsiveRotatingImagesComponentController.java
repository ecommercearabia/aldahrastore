package com.aldahra.storefront.controllers.cms;

import de.hybris.platform.acceleratorcms.model.components.SimpleResponsiveBannerComponentModel;
import de.hybris.platform.acceleratorfacades.device.ResponsiveMediaFacade;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.aldahra.aldahracomponents.data.SimpleResponsiveBannerComponentData;
import com.aldahra.aldahracomponents.model.ResponsiveRotatingImagesComponentModel;
import com.aldahra.storefront.controllers.ControllerConstants;


/**
 * The Class ResponsiveRotatingImagesComponentController.
 *
 * @author amjad.shati@erabia.com
 */
@Controller("ResponsiveRotatingImagesComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.ResponsiveRotatingImagesComponent)
public class ResponsiveRotatingImagesComponentController
		extends AbstractAcceleratorCMSComponentController<ResponsiveRotatingImagesComponentModel>
{

	/** The responsive media facade. */
	@Resource(name = "responsiveMediaFacade")
	private ResponsiveMediaFacade responsiveMediaFacade;

	/** The commerce common I18N service. */
	@Resource(name = "commerceCommonI18NService")
	private CommerceCommonI18NService commerceCommonI18NService;


	@Resource(name = "simpleResponsiveBannerConverter")
	private Converter<SimpleResponsiveBannerComponentModel, SimpleResponsiveBannerComponentData> simpleResponsiveBannerConverter;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final ResponsiveRotatingImagesComponentModel component)
	{
		final List<SimpleResponsiveBannerComponentData> banners = new ArrayList<>();
		if (component.getBanners() != null)
		{
			component.getBanners().stream().forEach(banner -> {
				if (Boolean.TRUE.equals(banner.getVisible()))
				{
					final SimpleResponsiveBannerComponentData bannerData = new SimpleResponsiveBannerComponentData();
					simpleResponsiveBannerConverter.convert(banner, bannerData);
					banners.add(bannerData);
				}
			});
		}

		model.addAttribute("banners", banners);
		model.addAttribute("timeout", component.getTimeout());
		model.addAttribute("effect", component.getEffect());
		model.addAttribute("title", component.getTitle());
		//		model.addAttribute("displayNavigation", component.getDisplayNavigation());
		//		model.addAttribute("displayPagination", component.getDisplayPagination());
		//		model.addAttribute("autoRotate", component.getAutoRotate());
		model.addAttribute("content", component.getContent());

	}
}

