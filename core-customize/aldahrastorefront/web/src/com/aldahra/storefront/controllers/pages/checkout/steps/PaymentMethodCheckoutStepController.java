/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.storefront.controllers.pages.checkout.steps;


import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateQuoteCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.util.AddressDataUtil;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.NoCardPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.NoCardTypeData;
import de.hybris.platform.commercefacades.order.data.PaymentModeData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.aldahra.aldahrastorecredit.enums.StoreCreditModeType;
import com.aldahra.aldahrastorecredit.exception.StoreCreditException;
import com.aldahra.aldahrastorecreditfacades.data.StoreCreditModeData;
import com.aldahra.aldahrauserfacades.user.facade.CustomUserFacade;
import com.aldahra.storefront.checkout.steps.CheckoutStep;
import com.aldahra.storefront.controllers.ControllerConstants;
import com.aldahra.storefront.form.PaymentDetailsForm;
import com.aldahra.storefront.form.StoreCreditForm;


@Controller
@RequestMapping(value = "/checkout/multi/payment-method")
public class PaymentMethodCheckoutStepController extends AbstractCheckoutStepController
{
	protected static final Map<String, String> CYBERSOURCE_SOP_CARD_TYPES = new HashMap<>();
	private static final String PAYMENT_METHOD = "payment-method";
	private static final String CART_DATA_ATTR = "cartData";
	private static final String SUPPORTED_PAYMENT_MODES_ATTR = "supportedPaymentModes";
	private static final String STORECREDIT_SCAMOUNT_CHECK_LIMIT_INVALID_KEY = "storeCredit.scAmount.check.limit.invalid";
	private static final String STORECREDIT_SCAMOUNT_CHECK_LIMIT_MSG_KEY = "storeCredit.scAmount.check.limit.msg";



	private static final Logger LOGGER = Logger.getLogger(PaymentMethodCheckoutStepController.class);

	@Resource(name = "addressDataUtil")
	private AddressDataUtil addressDataUtil;

	@Resource(name = "storeCreditValidator")
	private Validator storeCreditValidator;

	@Resource(name = "userFacade")
	private CustomUserFacade customUserFacade;

	@Resource(name = "cartService")
	private CartService cartService;


	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	protected CustomUserFacade getCustomUserFacade()
	{
		return customUserFacade;
	}

	@Override
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	@RequireHardLogIn
	@PreValidateQuoteCheckoutStep
	@PreValidateCheckoutStep(checkoutStep = PAYMENT_METHOD)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		final Optional<PriceData> availableBalanceStoreCredit = getCheckoutFacade().getAvailableBalanceStoreCreditAmount();

		populateStoreCreditLimitMsg(model);
		model.addAttribute("availableBalanceStoreCredit",
				availableBalanceStoreCredit.isPresent() ? availableBalanceStoreCredit.get() : null);

		final Optional<PriceData> storeCreditAmountFullRedeem = getCheckoutFacade().getStoreCreditAmountFullRedeem();

		model.addAttribute("storeCreditAmountFullRedeem",
				storeCreditAmountFullRedeem.isPresent() ? storeCreditAmountFullRedeem.get() : null);

		getCheckoutFacade().setDeliveryModeIfAvailable();
		setupAddPaymentPage(model);

		setCheckoutStepLinksForModel(model, getCheckoutStep());
		setupSilentOrderPostPage(model);

		return ControllerConstants.Views.Pages.MultiStepCheckout.AddPaymentMethodPage;
	}

	private boolean isCartContainsAppliedCoupon()
	{
		return CollectionUtils.isEmpty(cartService.getSessionCart().getAppliedCouponCodes()) ? false : true;
	}


	@RequestMapping(value = "/store-credit/choose", method = RequestMethod.GET)
	@RequireHardLogIn
	@PreValidateQuoteCheckoutStep
	@PreValidateCheckoutStep(checkoutStep = PAYMENT_METHOD)
	public String redirectToPaymentPage(final Model model, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException
	{
		return REDIRECT_PREFIX + "/checkout/multi/payment-method/add";
	}

	@RequestMapping(value = "/store-credit/choose", method = RequestMethod.POST)
	@RequireHardLogIn
	public String doSelectStoreCredit(final StoreCreditForm storeCreditForm, final Model model,
			final RedirectAttributes redirectAttributes, final BindingResult bindingResult)
			throws CMSItemNotFoundException, CommerceCartModificationException
	{
		storeCreditValidator.validate(storeCreditForm, bindingResult);
		populateStoreCreditLimitMsg(model);

		if (bindingResult.hasErrors())
		{
			final Optional<PriceData> availableBalanceStoreCredit = getCheckoutFacade().getAvailableBalanceStoreCreditAmount();

			model.addAttribute("availableBalanceStoreCredit",
					availableBalanceStoreCredit.isPresent() ? availableBalanceStoreCredit.get() : null);

			final Optional<PriceData> storeCreditAmountFullRedeem = getCheckoutFacade().getStoreCreditAmountFullRedeem();

			model.addAttribute("storeCreditAmountFullRedeem",
					storeCreditAmountFullRedeem.isPresent() ? storeCreditAmountFullRedeem.get() : null);

			getCheckoutFacade().setDeliveryModeIfAvailable();
			setupAddPaymentPage(model);

			setCheckoutStepLinksForModel(model, getCheckoutStep());
			setupSilentOrderPostPage(model);

			if (isHasStoreCreditLimit(bindingResult))
			{

				final double totalPriceWithStoreCreditLimit = baseStoreService.getCurrentBaseStore() == null ? 0.0d
						: baseStoreService.getCurrentBaseStore().getTotalPriceWithStoreCreditLimit();
				final Object[] arg = new Object[]
				{ new Double(totalPriceWithStoreCreditLimit) };
				GlobalMessages.addMessage(model, GlobalMessages.ERROR_MESSAGES_HOLDER, STORECREDIT_SCAMOUNT_CHECK_LIMIT_INVALID_KEY,
						arg);
			}

			return ControllerConstants.Views.Pages.MultiStepCheckout.AddPaymentMethodPage;
		}

		//		if (isCartContainsAppliedCoupon() && !StoreCreditModeType.REDEEM_NONE.getCode().equals(storeCreditForm.getSctCode()))
		//		{
		//			final Optional<PriceData> availableBalanceStoreCredit = getCheckoutFacade().getAvailableBalanceStoreCreditAmount();
		//
		//			model.addAttribute("availableBalanceStoreCredit",
		//					availableBalanceStoreCredit.isPresent() ? availableBalanceStoreCredit.get() : null);
		//
		//			final Optional<PriceData> storeCreditAmountFullRedeem = getCheckoutFacade().getStoreCreditAmountFullRedeem();
		//
		//			model.addAttribute("storeCreditAmountFullRedeem",
		//					storeCreditAmountFullRedeem.isPresent() ? storeCreditAmountFullRedeem.get() : null);
		//
		//			setupAddPaymentPage(model);
		//			setCheckoutStepLinksForModel(model, getCheckoutStep());
		//			setupSilentOrderPostPage(model);
		//
		//			GlobalMessages.addErrorMessage(model, "checkout.error.storecredit.coupon.invalid");
		//					return ControllerConstants.Views.Pages.MultiStepCheckout.AddPaymentMethodPage;
		//		}


		if (StoreCreditModeType.REDEEM_SPECIFIC_AMOUNT.getCode().equals(storeCreditForm.getSctCode()))
		{
			final double scAmountValue = Double.parseDouble(storeCreditForm.getScAmount());

			getCheckoutFacade().setStoreCreditMode(StoreCreditModeType.REDEEM_SPECIFIC_AMOUNT.getCode(),
					Double.valueOf(scAmountValue));
		}
		else
		{
			getCheckoutFacade().setStoreCreditMode(storeCreditForm.getSctCode(), null);
		}

		setupAddPaymentPage(model);
		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		model.addAttribute("cartData", cartData);
		setCheckoutStepLinksForModel(model, getCheckoutStep());

		return getCheckoutStep().currentStep();
	}

	/**
	 * @param bindingResult
	 * @return
	 */
	private boolean isHasStoreCreditLimit(final BindingResult bindingResult)
	{
		if(bindingResult ==null || CollectionUtils.isEmpty(bindingResult.getAllErrors()))
		{
			return false;
		}
		for (final ObjectError objectError : bindingResult.getAllErrors())
		{
			if (STORECREDIT_SCAMOUNT_CHECK_LIMIT_INVALID_KEY.equalsIgnoreCase(objectError.getCode()))
			{
				return true;
			}
		}

		return false;
	}

	@ModelAttribute("supportedStoreCreditModes")
	public Collection<StoreCreditModeData> getSupportedStoreCreditModes()
	{
		try
		{
			final Optional<List<StoreCreditModeData>> supportedStoreCreditModes = getCheckoutFacade().getSupportedStoreCreditModes();
			return supportedStoreCreditModes.isPresent() ? supportedStoreCreditModes.get() : Collections.emptyList();
		}
		catch (final StoreCreditException e)
		{
			return Collections.emptyList();
		}
	}

	protected void setupSilentOrderPostPage(final Model model)
	{
		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		model.addAttribute(CART_DATA_ATTR, cartData);
		model.addAttribute("deliveryAddress", cartData.getDeliveryAddress());
		final Optional<List<PaymentModeData>> supportedPaymentModes = getCheckoutFacade().getSupportedPaymentModesForStoreFront();

		if (supportedPaymentModes.isPresent())
		{
			model.addAttribute(SUPPORTED_PAYMENT_MODES_ATTR, supportedPaymentModes.get());
		}

		final PaymentDetailsForm paymentDetailsForm = new PaymentDetailsForm();
		paymentDetailsForm.setPaymentModeCode(cartData.getPaymentMode() != null ? cartData.getPaymentMode().getCode() : null);
		model.addAttribute("paymentDetailsForm", paymentDetailsForm);
	}

	protected boolean checkPaymentSubscription(final Model model, final PaymentDetailsForm paymentDetailsForm,
			final NoCardPaymentInfoData newPaymentSubscription)
	{
		if (newPaymentSubscription != null && StringUtils.isNotBlank(newPaymentSubscription.getId()))
		{
			if (paymentDetailsForm.getSaveInAccount() && getCustomUserFacade().getNoCardPaymentInfos(true).isPresent()
					&& getCustomUserFacade().getNoCardPaymentInfos(true).get().size() <= 1)
			{
				getCustomUserFacade().setDefaultPaymentInfo(newPaymentSubscription);
			}
			getCheckoutFacade().setGeneralPaymentDetails(newPaymentSubscription.getId());
		}
		else
		{
			GlobalMessages.addErrorMessage(model, "checkout.multi.paymentMethod.createSubscription.failedMsg");
			return false;
		}
		return true;
	}

	@RequestMapping(value =
	{ "/add" }, method = RequestMethod.POST)
	@RequireHardLogIn
	public String add(final Model model, @Valid
	final PaymentDetailsForm paymentDetailsForm, final BindingResult bindingResult) throws CMSItemNotFoundException
	{
		getPaymentDetailsValidator().validate(paymentDetailsForm, bindingResult);
		getCheckoutFacade().removePaymentSubscription();
		setupAddPaymentPage(model);

		if (bindingResult.hasErrors())
		{
			setupSilentOrderPostPage(model);
			GlobalMessages.addErrorMessage(model, "checkout.error.paymentethod.formentry.invalid");
			return ControllerConstants.Views.Pages.MultiStepCheckout.AddPaymentMethodPage;
		}

		final String paymentModeCode = paymentDetailsForm.getPaymentModeCode();
		final Optional<AddressData> address = getCheckoutFacade().getCartAddress();
		final AddressData addressData = address.isPresent() ? address.get() : null;

		if (addressData != null)
		{
			addressData.setShippingAddress(Boolean.TRUE);
			addressData.setBillingAddress(Boolean.TRUE);
		}

		getAddressVerificationFacade().verifyAddressData(addressData);

		switch (paymentDetailsForm.getPaymentModeCode().toLowerCase())
		{
			case "pis":

			case "cod":

			case "ccod":

			case "continue":
				final NoCardPaymentInfoData noCardPaymentInfoData = new NoCardPaymentInfoData();
				final NoCardTypeData noCardTypeData = new NoCardTypeData();
				noCardTypeData.setCode(paymentDetailsForm.getPaymentModeCode().toUpperCase());
				noCardPaymentInfoData.setNoCardTypeData(noCardTypeData);
				noCardPaymentInfoData.setDefaultPaymentInfo(true);
				noCardPaymentInfoData.setBillingAddress(addressData);
				noCardPaymentInfoData.setSaved(true);

				final Optional<NoCardPaymentInfoData> createPaymentSubscription = getCheckoutFacade()
						.createPaymentSubscription(noCardPaymentInfoData);

				if (createPaymentSubscription.isPresent())
				{

				}

				if (!checkPaymentSubscription(model, paymentDetailsForm, createPaymentSubscription.get()))
				{
					setupSilentOrderPostPage(model);
					return ControllerConstants.Views.Pages.MultiStepCheckout.AddPaymentMethodPage;
				}

				getCheckoutFacade().saveBillingAddress(addressData);
				model.addAttribute("paymentId", noCardPaymentInfoData.getId());

				break;

			case "card":

				getCheckoutFacade().saveBillingAddress(addressData);

				//				final CCPaymentInfoData ccPaymentInfoData = new CCPaymentInfoData();
				//				fillInPaymentData(paymentDetailsForm, ccPaymentInfoData);
				//				ccPaymentInfoData.setBillingAddress(addressData);
				//
				//				final CCPaymentInfoData newPaymentSubscription = getCheckoutFacade().createPaymentSubscription(ccPaymentInfoData);
				//				if (!checkPaymentSubscription(model, paymentDetailsForm, newPaymentSubscription))
				//				{
				//					return ControllerConstants..MultiStepCheckout.AddPaymentMethodPage;
				//				}
				//				model.addAttribute("paymentId", ccPaymentInfoData.getId());

				//				cartService.getSessionCart();


				break;

			default:
				break;
		}
		getCheckoutFacade().setPaymentMode(paymentDetailsForm.getPaymentModeCode());

		return getCheckoutStep().nextStep();
	}

	@RequestMapping(value = "/back", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}


	protected void populateStoreCreditLimitMsg(final Model model) throws CMSItemNotFoundException
	{

		if (baseStoreService.getCurrentBaseStore() != null
				&& !baseStoreService.getCurrentBaseStore().isEnableCheckTotalPriceWithStoreCreditLimit())
		{
			return;
		}

		final double totalPriceWithStoreCreditLimit = baseStoreService.getCurrentBaseStore() == null ? 0.0d
				: baseStoreService.getCurrentBaseStore().getTotalPriceWithStoreCreditLimit();

		final Object[] arg = new Object[]
		{ new Double(totalPriceWithStoreCreditLimit) };
		final String storeCreditLimitMsg = getMessageSource().getMessage(STORECREDIT_SCAMOUNT_CHECK_LIMIT_MSG_KEY,
				arg,
				getI18nService().getCurrentLocale());
		model.addAttribute("storeCreditLimitMsg",storeCreditLimitMsg);
	}


	protected void setupAddPaymentPage(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute("metaRobots", "noindex,nofollow");
		model.addAttribute("hasNoPaymentInfo", Boolean.valueOf(getCheckoutFlowFacade().hasNoPaymentInfo()));
		prepareDataForPage(model);
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.paymentMethod.breadcrumb"));
		final ContentPageModel contentPage = getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL);
		storeCmsPageInModel(model, contentPage);
		setUpMetaDataForContentPage(model, contentPage);
		setCheckoutStepLinksForModel(model, getCheckoutStep());
	}



	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(PAYMENT_METHOD);
	}


	protected CartModel getCart()
	{
		return cartService.hasSessionCart() ? cartService.getSessionCart() : null;
	}


}
