/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.storefront.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateQuoteCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.DeliveryModeData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.AreaData;
import de.hybris.platform.commercefacades.user.data.CityData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.aldahra.aldahrauserfacades.country.facade.CountryFacade;
import com.aldahra.facades.exception.ExpressOrderException;
import com.aldahra.facades.facade.CustomAcceleratorCheckoutFacade;
import com.aldahra.storefront.checkout.steps.CheckoutStep;
import com.aldahra.storefront.checkout.steps.validation.ValidationResults;
import com.aldahra.storefront.controllers.ControllerConstants;
import com.aldahra.storefront.form.AddressForm;
import com.aldahra.storefront.util.AddressDataUtil;


@Controller
@RequestMapping(value = "/checkout/multi/delivery-address")
public class DeliveryAddressCheckoutStepController extends AbstractCheckoutStepController
{
	private static final String DELIVERY_ADDRESS = "delivery-address";
	private static final String SHOW_SAVE_TO_ADDRESS_BOOK_ATTR = "showSaveToAddressBook";
	private static final String ADDRESS_ERROR = "address.error.formentry.invalid";

	@Resource(name = "defaultAcceleratorCheckoutFacade")
	private CustomAcceleratorCheckoutFacade defaultAcceleratorCheckoutFacade;

	@Resource(name = "customAddressDataUtil")
	private AddressDataUtil addressDataUtil;

	@Resource(name = "countryFacade")
	private CountryFacade countryFacade;

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "addressConverter")
	private Converter<AddressModel, AddressData> addressConverter;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "customCustomerFacade")
	private CustomerFacade customerFacade;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Override
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	@RequireHardLogIn
	@PreValidateQuoteCheckoutStep
	@PreValidateCheckoutStep(checkoutStep = DELIVERY_ADDRESS)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		getCheckoutFacade().setDeliveryAddressIfAvailable();
		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		model.addAttribute("selectedDeliveryAddress", cartData.getDeliveryAddress());

		populateCommonModelAttributes(model, cartData, getNewAddressForm());

		setDefaultAddressIfAvailable();
		setDefaultDeliveryModeIfAvailable();

		return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
	}

	/**
	 *
	 */
	private void setDefaultDeliveryModeIfAvailable()
	{
		final List<? extends DeliveryModeData> supportedDeliveryModes = getCheckoutFacade().getSupportedDeliveryModes();

		if (supportedDeliveryModes != null && !supportedDeliveryModes.isEmpty())
		{
			//Set first delivery method
			getCheckoutFacade().setDeliveryMode(supportedDeliveryModes.get(0).getCode());
		}

	}

	protected AddressForm getNewAddressForm()
	{
		final AddressForm addressForm = new AddressForm();
		addressForm.setMobileCountry(getCmsSiteService().getCurrentSite().getDefaultMobileCountry() == null ? null
				: getCmsSiteService().getCurrentSite().getDefaultMobileCountry().getIsocode());

		addressForm.setCountryIso(getCmsSiteService().getCurrentSite().getDefaultCountryAddress() == null ? null
				: getCmsSiteService().getCurrentSite().getDefaultCountryAddress().getIsocode());

		if (!userService.isAnonymousUser(userService.getCurrentUser()))
		{
			final CustomerData currentCustomerData = customerFacade.getCurrentCustomer();

			addressForm.setFirstName(currentCustomerData.getFirstName());
			addressForm.setLastName(currentCustomerData.getLastName());
			addressForm.setTitleCode(currentCustomerData.getTitleCode());
			addressForm.setMobileCountry(
					currentCustomerData.getMobileCountry() == null ? null : currentCustomerData.getMobileCountry().getIsocode());
			addressForm.setMobile(currentCustomerData.getMobileNumber() == null ? null : currentCustomerData.getMobileNumber());
		}

		final Optional<List<CityData>> cities = getCityFacade().getAll();
		if (cities.isPresent() && cities.get().size() == 1)
		{
			addressForm.setCityCode(cities.get().get(0).getCode());
		}

		return addressForm;
	}

	public String checkAddressId(final String id)
	{
		return id == null || id.isBlank() || id.indexOf(',') > -1 ? null : id;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@RequireHardLogIn
	public String add(final AddressForm addressForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		addressForm.setChosenAddressId(checkAddressId(addressForm.getChosenAddressId()));
		if (StringUtils.isBlank(addressForm.getChosenAddressId()))
		{
			addressForm.setChosenAddressId(addressForm.getAddressId());
		}

		getAddressValidator().validate(addressForm, bindingResult);
		populateCommonModelAttributes(model, cartData, addressForm);

		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, ADDRESS_ERROR);
			return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
		}

		if (StringUtils.isEmpty(addressForm.getChosenAddressId()))
		{
			final AddressData newAddress = addressDataUtil.convertToAddressData(addressForm);

			processAddressVisibilityAndDefault(addressForm, newAddress);


			getUserFacade().addAddress(newAddress);

			final AddressData previousSelectedAddress = getCheckoutFacade().getCheckoutCart().getDeliveryAddress();
			// Set the new address as the selected checkout delivery address
			getCheckoutFacade().setDeliveryAddress(newAddress);
			if (previousSelectedAddress != null && !previousSelectedAddress.isVisibleInAddressBook())
			{ // temporary address should be removed
				getUserFacade().removeAddress(previousSelectedAddress);
			}

			// Set the new address as the selected checkout delivery address
			getCheckoutFacade().setDeliveryAddress(newAddress);
		}
		else if (StringUtils.isNotEmpty(addressForm.getChosenAddressId()))
		{
			final CartModel sessionCart = cartService.getSessionCart();
			final AddressData addressData = addressDataUtil.convertToVisibleAddressData(addressForm);

			getUserFacade().editAddress(addressData);

			final AddressModel chosenAddress = modelService.get(PK.parse(addressData.getId()));
			modelService.refresh(chosenAddress);

			sessionCart.setDeliveryAddress(chosenAddress);
			modelService.save(sessionCart);
		}
		else if (cartService.getSessionCart().getDeliveryAddress() == null)
		{
			GlobalMessages.addErrorMessage(model, ADDRESS_ERROR);
			return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
		}
		final List<? extends DeliveryModeData> supportedDeliveryModes = getCheckoutFacade().getSupportedDeliveryModes();
		if (CollectionUtils.isEmpty(supportedDeliveryModes))
		{
			GlobalMessages.addErrorMessage(model, "address.error.shippingmethod.invalid");
			return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
		}

		//Set first delivery method
		getCheckoutFacade().setDeliveryMode(supportedDeliveryModes.get(0).getCode());
		try
		{
			cartData.setExpress(getDefaultAcceleratorCheckoutFacade().isExpressOrder());
		}
		catch (final ExpressOrderException e)
		{
			cartData.setExpress(false);
		}
		return getCheckoutStep().nextStep();
	}

	protected void processAddressVisibilityAndDefault(final AddressForm addressForm, final AddressData newAddress)
	{
		if (addressForm.getSaveInAddressBook() != null)
		{
			newAddress.setVisibleInAddressBook(addressForm.getSaveInAddressBook().booleanValue());
			if (addressForm.getSaveInAddressBook().booleanValue() && CollectionUtils.isEmpty(getUserFacade().getAddressBook()))
			{
				newAddress.setDefaultAddress(true);
			}
		}
		else if (getCheckoutCustomerStrategy().isAnonymousCheckout())
		{
			newAddress.setDefaultAddress(true);
			newAddress.setVisibleInAddressBook(true);
		}
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	@RequireHardLogIn
	public String editAddressForm(@RequestParam("editAddressCode")
	final String editAddressCode, final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		final ValidationResults validationResults = getCheckoutStep().validate(redirectAttributes);
		if (getCheckoutStep().checkIfValidationErrors(validationResults))
		{
			return getCheckoutStep().onValidation(validationResults);
		}

		AddressData addressData = null;
		if (StringUtils.isNotEmpty(editAddressCode))
		{
			addressData = getCheckoutFacade().getDeliveryAddressForCode(editAddressCode);
		}

		final AddressForm addressForm = getNewAddressForm();
		final boolean hasAddressData = addressData != null;
		if (hasAddressData)
		{
			addressDataUtil.convert(addressData, addressForm);
		}

		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		populateCommonModelAttributes(model, cartData, addressForm);

		if (addressData != null)
		{
			model.addAttribute(SHOW_SAVE_TO_ADDRESS_BOOK_ATTR, Boolean.valueOf(!addressData.isVisibleInAddressBook()));
		}

		return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	@RequireHardLogIn
	public String edit(final AddressForm addressForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		getAddressValidator().validate(addressForm, bindingResult);

		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		populateCommonModelAttributes(model, cartData, addressForm);

		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, ADDRESS_ERROR);
			return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
		}

		final AddressData newAddress = addressDataUtil.convertToAddressData(addressForm);

		processAddressVisibility(addressForm, newAddress);

		newAddress.setDefaultAddress(CollectionUtils.isEmpty(getUserFacade().getAddressBook())
				|| getUserFacade().getAddressBook().size() == 1 || Boolean.TRUE.equals(addressForm.getDefaultAddress()));

		getUserFacade().editAddress(newAddress);
		getCheckoutFacade().setDeliveryModeIfAvailable();
		getCheckoutFacade().setDeliveryAddress(newAddress);

		return getCheckoutStep().nextStep();
	}

	protected void processAddressVisibility(final AddressForm addressForm, final AddressData newAddress)
	{

		if (addressForm.getSaveInAddressBook() == null)
		{
			newAddress.setVisibleInAddressBook(true);
		}
		else
		{
			newAddress.setVisibleInAddressBook(Boolean.TRUE.equals(addressForm.getSaveInAddressBook()));
		}
	}

	@RequestMapping(value = "/remove", method =
	{ RequestMethod.GET, RequestMethod.POST })
	@RequireHardLogIn
	public String removeAddress(@RequestParam("addressCode")
	final String addressCode, final RedirectAttributes redirectModel, final Model model) throws CMSItemNotFoundException
	{
		if (getCheckoutFacade().isRemoveAddressEnabledForCart())
		{
			final AddressData addressData = new AddressData();
			addressData.setId(addressCode);
			getUserFacade().removeAddress(addressData);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
					"account.confirmation.address.removed");
		}
		final ContentPageModel multiCheckoutSummaryPage = getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL);
		storeCmsPageInModel(model, multiCheckoutSummaryPage);
		setUpMetaDataForContentPage(model, multiCheckoutSummaryPage);
		model.addAttribute("addressForm", new AddressForm());

		return getCheckoutStep().currentStep();
	}

	@RequestMapping(value = "/select", method = RequestMethod.POST)
	@RequireHardLogIn
	public String doSelectSuggestedAddress(final AddressForm addressForm, final RedirectAttributes redirectModel)
	{
		final Set<String> resolveCountryRegions = org.springframework.util.StringUtils
				.commaDelimitedListToSet(Config.getParameter("resolve.country.regions"));

		final AddressData selectedAddress = addressDataUtil.convertToAddressData(addressForm);
		final CountryData countryData = selectedAddress.getCountry();

		if (!resolveCountryRegions.contains(countryData.getIsocode()))
		{
			selectedAddress.setRegion(null);
		}

		if (addressForm.getSaveInAddressBook() != null)
		{
			selectedAddress.setVisibleInAddressBook(addressForm.getSaveInAddressBook().booleanValue());
		}

		if (Boolean.TRUE.equals(addressForm.getEditAddress()))
		{
			getUserFacade().editAddress(selectedAddress);
		}
		else
		{
			getUserFacade().addAddress(selectedAddress);
		}

		final AddressData previousSelectedAddress = getCheckoutFacade().getCheckoutCart().getDeliveryAddress();
		// Set the new address as the selected checkout delivery address
		getCheckoutFacade().setDeliveryAddress(selectedAddress);
		if (previousSelectedAddress != null && !previousSelectedAddress.isVisibleInAddressBook())
		{ // temporary address should be removed
			getUserFacade().removeAddress(previousSelectedAddress);
		}

		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "checkout.multi.address.added");

		return getCheckoutStep().nextStep();
	}


	/**
	 * This method gets called when the "Use this Address" button is clicked. It sets the selected delivery address on
	 * the checkout facade - if it has changed, and reloads the page highlighting the selected delivery address.
	 *
	 * @param selectedAddressCode
	 *           - the id of the delivery address.
	 *
	 * @return - a URL to the page to load.
	 */
	@RequestMapping(value = "/select", method = RequestMethod.GET)
	@RequireHardLogIn
	public String doSelectDeliveryAddress(@RequestParam("selectedAddressCode")
	final String selectedAddressCode, final RedirectAttributes redirectAttributes)
	{
		final ValidationResults validationResults = getCheckoutStep().validate(redirectAttributes);
		if (getCheckoutStep().checkIfValidationErrors(validationResults))
		{
			return getCheckoutStep().onValidation(validationResults);
		}
		if (StringUtils.isNotBlank(selectedAddressCode))
		{
			final AddressData selectedAddressData = getCheckoutFacade().getDeliveryAddressForCode(selectedAddressCode);
			final boolean hasSelectedAddressData = selectedAddressData != null;
			if (hasSelectedAddressData)
			{
				setDeliveryAddress(selectedAddressData);
			}
		}
		return getCheckoutStep().nextStep();
	}

	protected void setDeliveryAddress(final AddressData selectedAddressData)
	{
		final AddressData cartCheckoutDeliveryAddress = getCheckoutFacade().getCheckoutCart().getDeliveryAddress();
		if (isAddressIdChanged(cartCheckoutDeliveryAddress, selectedAddressData))
		{
			getCheckoutFacade().setDeliveryAddress(selectedAddressData);
			if (cartCheckoutDeliveryAddress != null && !cartCheckoutDeliveryAddress.isVisibleInAddressBook())
			{ // temporary address should be removed
				getUserFacade().removeAddress(cartCheckoutDeliveryAddress);
			}
		}
	}

	@RequestMapping(value = "/back", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}

	protected String getBreadcrumbKey()
	{
		return "checkout.multi." + getCheckoutStep().getProgressBarId() + ".breadcrumb";
	}

	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(DELIVERY_ADDRESS);
	}

	protected void populateCommonModelAttributes(final Model model, final CartData cartData, final AddressForm addressForm)
			throws CMSItemNotFoundException
	{

		model.addAttribute("cartData", cartData);
		model.addAttribute("addressForm", addressForm);

		model.addAttribute("deliveryAddresses", getDeliveryAddresses(cartData.getDeliveryAddress()));
		model.addAttribute("noAddress", Boolean.valueOf(getCheckoutFlowFacade().hasNoDeliveryAddress()));
		model.addAttribute("addressFormEnabled", Boolean.valueOf(getCheckoutFacade().isNewAddressEnabledForCart()));
		model.addAttribute("removeAddressEnabled", Boolean.valueOf(getCheckoutFacade().isRemoveAddressEnabledForCart()));
		model.addAttribute(SHOW_SAVE_TO_ADDRESS_BOOK_ATTR, Boolean.TRUE);
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, getResourceBreadcrumbBuilder().getBreadcrumbs(getBreadcrumbKey()));
		model.addAttribute("metaRobots", "noindex,nofollow");
		if (StringUtils.isNotBlank(addressForm.getCountryIso()))
		{
			model.addAttribute("regions", getI18NFacade().getRegionsForCountryIso(addressForm.getCountryIso()));
			model.addAttribute("country", addressForm.getCountryIso());
		}
		final Optional<List<CountryData>> mobileCountries = countryFacade.getMobileCountriesByCuruntSite();
		model.addAttribute("mobileCountries", mobileCountries.isPresent() ? mobileCountries.get() : null);
		if (StringUtils.isNotBlank(addressForm.getCityCode()))
		{
			final Optional<List<AreaData>> areas = getAreaFacade().getByCityCode(addressForm.getCityCode());
			if (areas.isPresent())
			{
				model.addAttribute("areas", areas.get());
			}
		}
		else
		{
			model.addAttribute("areas", Collections.emptyList());
		}
		final Optional<List<CityData>> cities = getCityFacade().getAll();
		model.addAttribute("cities", cities.isPresent() ? cities.get() : null);
		final String siteID = getCmsSiteService().getCurrentSite() == null ? "" : getCmsSiteService().getCurrentSite().getUid();
		final boolean isAreaVisible = getConfigurationService().getConfiguration()
				.getBoolean("website.address.area.visible." + siteID);
		model.addAttribute("isAreaVisible", isAreaVisible);
		prepareDataForPage(model);
		final ContentPageModel multiCheckoutSummaryPage = getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL);
		storeCmsPageInModel(model, multiCheckoutSummaryPage);
		setUpMetaDataForContentPage(model, multiCheckoutSummaryPage);
		setCheckoutStepLinksForModel(model, getCheckoutStep());
	}

	protected void setDefaultAddressIfAvailable()
	{
		final CartModel cartModel = cartService.getSessionCart();

		if (cartModel.getUser() != null && cartModel.getDeliveryAddress() == null)
		{
			final AddressModel currentUserDefaultShipmentAddress = cartModel.getUser().getDefaultShipmentAddress();
			if (currentUserDefaultShipmentAddress != null)
			{
				getCheckoutFacade().setDeliveryAddress(addressConverter.convert(currentUserDefaultShipmentAddress));
			}
		}
	}

	protected CustomAcceleratorCheckoutFacade getDefaultAcceleratorCheckoutFacade()
	{
		return defaultAcceleratorCheckoutFacade;
	}

}
