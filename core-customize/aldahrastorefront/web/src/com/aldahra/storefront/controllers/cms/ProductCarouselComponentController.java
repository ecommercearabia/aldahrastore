/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.storefront.controllers.cms;

import de.hybris.platform.cms2lib.model.components.ProductCarouselComponentModel;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.ProductSearchFacade;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.aldahra.aldahracomponentsfacades.facade.CustomProductCarouselFacade;
import com.aldahra.storefront.controllers.ControllerConstants;


/**
 * Controller for CMS ProductReferencesComponent.
 */
@Controller("ProductCarouselComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.ProductCarouselComponent)
public class ProductCarouselComponentController extends AbstractAcceleratorCMSComponentController<ProductCarouselComponentModel>
{
	protected static final List<ProductOption> PRODUCT_OPTIONS = Arrays.asList(ProductOption.BASIC, ProductOption.PRICE,
			ProductOption.STOCK, ProductOption.SUMMARY, ProductOption.PROMOTIONS, ProductOption.VARIANT_FULL);

	private static final Logger LOG = Logger.getLogger(ProductCarouselComponentController.class);

	@Resource(name = "productSearchFacade")
	private ProductSearchFacade<ProductData> productSearchFacade;

	@Resource(name = "customProductCarouselFacade")
	private CustomProductCarouselFacade customProductCarouselFacade;

	@Resource(name = "userService")
	private UserService userService;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final ProductCarouselComponentModel component)
	{
		final List<ProductData> products = new ArrayList<>();

		products.addAll(collectLinkedProducts(component));
		products.addAll(collectSearchProducts(component));
		final boolean isAnonymousUser = isAnonymousUser();
		model.addAttribute("title", component.getTitle());
		model.addAttribute("productData", products);

		model.addAttribute("isAnonymousUser", isAnonymousUser());

	}

	/**
	 * @return
	 */
	private boolean isAnonymousUser()
	{
		final UserModel currentUser = userService.getCurrentUser();
		if (currentUser == null)
		{
			LOG.info("ProductCarouselComponentController -> currentUser is null");
			return true;
		}

		final boolean anonymousUser = userService.isAnonymousUser(currentUser);

		LOG.info("ProductCarouselComponentController -> currentUser isAnonymousUser [" + anonymousUser + "]");

		return anonymousUser;
	}

	protected List<ProductData> collectLinkedProducts(final ProductCarouselComponentModel component)
	{
		return customProductCarouselFacade.collectProducts(component, PRODUCT_OPTIONS);
	}

	protected List<ProductData> collectSearchProducts(final ProductCarouselComponentModel component)
	{
		final SearchQueryData searchQueryData = new SearchQueryData();
		searchQueryData.setValue(component.getSearchQuery());
		final String categoryCode = component.getCategoryCode();

		if (searchQueryData.getValue() != null && categoryCode != null)
		{
			final SearchStateData searchState = new SearchStateData();
			searchState.setQuery(searchQueryData);

			final PageableData pageableData = new PageableData();
			pageableData.setPageSize(100); // Limit to 100 matching results

			return productSearchFacade.categorySearch(categoryCode, searchState, pageableData).getResults();
		}

		return Collections.emptyList();
	}
}
