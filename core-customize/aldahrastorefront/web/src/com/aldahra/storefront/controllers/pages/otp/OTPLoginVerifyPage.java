/**
 *
 */
package com.aldahra.storefront.controllers.pages.otp;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.OTPData;
import de.hybris.platform.commerceservices.customer.TokenInvalidatedException;

import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.aldahra.aldahraotp.enums.OTPVerificationTokenType;
import com.aldahra.aldahraotp.model.OTPVerificationTokenModel;
import com.aldahra.storefront.form.OTPForm;
import com.aldahra.storefront.security.OTPAutoLoginStrategy;


/**
 * @author mnasro
 *
 */
@Controller
@Scope("tenant")
public class OTPLoginVerifyPage extends AbstractLoginVerifyPage
{
	@Resource(name = "otpAutoLoginStrategy")
	private OTPAutoLoginStrategy autoLoginStrategy;

	@Override
	@RequestMapping(method = RequestMethod.POST, value = "/login/otp")
	public String generatOTPLoginToken(final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		return super.generatOTPLoginToken(model, request, response);
	}

	@Override
	@RequestMapping(method = RequestMethod.GET, value = "/login/verify")
	public String showVerifyPage(@RequestParam(required = true)
	final String token, final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		return super.showVerifyPage(token, model, request, response);
	}

	@Override
	@RequestMapping(method = RequestMethod.POST, value = "/login/verify/resend")
	public String resendOTPCode(@RequestParam(required = true)
	final String token, final Model model, final HttpServletRequest request, final HttpServletResponse response,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		return super.resendOTPCode(token, model, request, response, redirectModel);
	}

	@Override
	@RequestMapping(method = RequestMethod.GET, value = "/login/verify/change-number")
	public String getChangeMobileNumberView(@RequestParam(required = true)
	String token, final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		token = getEncodeToken(token);

		return super.getChangeMobileNumberView(token, model, request, response);
	}

	@Override
	@RequestMapping(method = RequestMethod.POST, value = "/login/verify/send")
	public String send(@RequestParam(required = true)
	final String token, final OTPForm otpForm, final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel, final BindingResult bindingResult)
			throws CMSItemNotFoundException
	{
		return super.send(token, otpForm, model, request, response, redirectModel, bindingResult);
	}


	@Override
	@RequestMapping(method = RequestMethod.POST, value = "/login/verify")
	public String verify(@RequestParam(required = false)
	final String token, final OTPForm otpForm, final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel, final BindingResult bindingResult)
			throws CMSItemNotFoundException, TokenInvalidatedException
	{
		return super.verify(token, otpForm, model, request, response, redirectModel, bindingResult);
	}

	@Override
	public String getThankyou(@RequestParam(required = false)
	final String token, final OTPForm otpForm, final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		return super.getThankyou(token, otpForm, model, request, response, redirectModel);
	}

	@Override
	protected String getOTPViewURL()
	{
		return "/login/verify";
	}

	@Override
	protected OTPVerificationTokenType getOTPVerificationTokenType()
	{
		return OTPVerificationTokenType.LOGIN;
	}

	@Override
	protected String getFailedErrorLabelOrIdPage()
	{
		return "login";
	}

	@Override
	protected String getFailedErrorView()
	{
		return "/login";
	}

	@Override
	protected String doAction(final Model model, final RedirectAttributes redirectModel,
			final OTPVerificationTokenModel otpVerificationToken, final HttpServletRequest request,
			final HttpServletResponse response, final BindingResult bindingResult) throws CMSItemNotFoundException
	{
		final OTPData otpData = (OTPData) otpVerificationToken.getData();


		otpData.setMobileCountry(otpVerificationToken.getMobileCountry());
		otpData.setMobileNumber(otpVerificationToken.getMobileNumber());
		final Optional<CustomerData> customerByIdentifier = getCustomerFacade()
				.getCustomerByIdentifier(otpVerificationToken.getMobileNumber());
		if (customerByIdentifier.isEmpty())
		{
			return getNotFoundPage(model, response);
		}

		otpData.setUid(customerByIdentifier.get().getUid());

		autoLoginStrategy.login(otpData, request, response);

		return REDIRECT_PREFIX + getSuccessRedirect(request, response);

	}



	@Override
	protected String getVerifyActionURL()
	{
		return "/login/verify";
	}

	@Override
	protected String getChangeNumberActionURL()
	{
		return "/login/verify/change-number";
	}

	@Override
	protected String getResendVerifyActionURL()
	{
		return "/login/verify/resend";
	}

	@Override
	protected String getSendVerifyActionURL()
	{
		return "/login/verify/send";
	}


}