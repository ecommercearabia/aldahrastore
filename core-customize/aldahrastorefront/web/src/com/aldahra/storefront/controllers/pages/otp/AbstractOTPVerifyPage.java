package com.aldahra.storefront.controllers.pages.otp;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commerceservices.customer.TokenInvalidatedException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import org.apache.logging.log4j.util.Strings;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.aldahra.aldahraotp.context.OTPContext;
import com.aldahra.aldahraotp.context.OTPContextV2;
import com.aldahra.aldahraotp.enums.OTPVerificationTokenType;
import com.aldahra.aldahraotp.exception.OTPException;
import com.aldahra.aldahraotp.model.OTPVerificationTokenModel;
import com.aldahra.aldahrauserfacades.country.facade.CountryFacade;
import com.aldahra.aldahrauserfacades.customer.facade.CustomCustomerFacade;
import com.aldahra.storefront.controllers.ControllerConstants;
import com.aldahra.storefront.form.OTPForm;
import com.aldahra.storefront.form.validation.OTPValidator;
/**
 * The Class AbstractOTPVerifyPage.
 *
 * @author @author mnasro
 */
public abstract class AbstractOTPVerifyPage extends AbstractPageController
{

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(AbstractOTPVerifyPage.class);

	/** The Constant FORM_GLOBAL_ERROR. */
	protected static final String FORM_GLOBAL_ERROR = "form.global.error";

	/** The Constant MESSAGE_NOT_SENT_MESSAGE. */
	private static final String MESSAGE_NOT_SENT_MESSAGE = "otp.message.not.sent.message";

	/** The otp context V 2. */
	@Resource(name = "otpContextV2")
	private OTPContextV2 otpContextV2;

	/** The country facade. */
	@Resource(name = "countryFacade")
	private CountryFacade countryFacade;

	/** The otp context. */
	@Resource(name = "otpContext")
	private OTPContext otpContext;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "httpSessionRequestCache")
	private HttpSessionRequestCache httpSessionRequestCache;

	@Resource(name = "customCustomerFacade")
	private CustomCustomerFacade customerFacade;

	/** The session service. */
	@Resource(name = "sessionService")
	private SessionService sessionService;

	protected abstract OTPValidator getOTPValidator();


	/**
	 * Gets the verify action URL.
	 *
	 * @return the verify action URL
	 */
	protected abstract String getVerifyActionURL();

	/**
	 * Gets the change number action URL.
	 *
	 * @return the change number action URL
	 */
	protected abstract String getChangeNumberActionURL();

	/**
	 * Gets the resend verify action URL.
	 *
	 * @return the resend verify action URL
	 */
	protected abstract String getResendVerifyActionURL();

	/**
	 * Gets the send verify action URL.
	 *
	 * @return the send verify action URL
	 */
	protected abstract String getSendVerifyActionURL();

	/**
	 * Gets the failed error label or id page.
	 *
	 * @return the failed error label or id page
	 */
	protected abstract String getFailedErrorLabelOrIdPage();

	/**
	 * Gets the OTP view URL.
	 *
	 * @return the OTP view URL
	 */
	protected abstract String getOTPViewURL();

	//	/**
	//	 * Gets the page label or id.
	//	 *
	//	 * @return the page label or id
	//	 */
	//	protected abstract String getPageLabelOrId();

	/**
	 * Gets the failed error view.
	 *
	 * @return the failed error view
	 */
	protected abstract String getFailedErrorView();

	/**
	 * Gets the OTP verification token type.
	 *
	 * @return the OTP verification token type
	 */
	protected abstract OTPVerificationTokenType getOTPVerificationTokenType();

	/**
	 * Do action.
	 *
	 * @param model
	 *           the model
	 * @param redirectModel
	 *           the redirect model
	 * @param otpVerificationTokenModel
	 *           the otp verification token model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param bindingResult
	 *           the binding result
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	protected abstract String doAction(final Model model, final RedirectAttributes redirectModel,
			final OTPVerificationTokenModel otpVerificationTokenModel, final HttpServletRequest request,
			final HttpServletResponse response, final BindingResult bindingResult) throws CMSItemNotFoundException;

	protected boolean validateOTPEnabilityAndType()
	{
		return getOTPVerificationTokenType() != null && getOtpContextV2().isEnabledByCurrentSite(getOTPVerificationTokenType());
	}
	/**
	 * Validate.
	 *
	 * @param token
	 *           the token
	 * @param model
	 *           the model
	 * @param response
	 *           the response
	 * @return the pair
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	protected Pair<String, OTPVerificationTokenModel> validate(final String token, final Model model,
			final HttpServletResponse response) throws CMSItemNotFoundException
	{
		Optional<OTPVerificationTokenModel> otpVerificationToken = Optional.empty();
		try
		{
			if (!validateOTPEnabilityAndType() || StringUtils.isBlank(token)
					|| !getOtpContextV2().verifyTokenByCurrentCustomer(token))
			{
				return Pair.of(getNotFoundPage(model, response), null);
			}
			else
			{
				otpVerificationToken = getOtpContextV2().getTokenByCurrentCustomer(token);
			}
		}
		catch (final TokenInvalidatedException e)
		{
			sessionService.setAttribute(OTPConstants.Actions.ErrorMessegeKey.REGISTRATION, MESSAGE_NOT_SENT_MESSAGE);

			return Pair.of(REDIRECT_PREFIX + getFailedErrorView(), null);
		}

		if (otpVerificationToken.isEmpty())
		{
			sessionService.setAttribute(OTPConstants.Actions.ErrorMessegeKey.REGISTRATION, MESSAGE_NOT_SENT_MESSAGE);
			return Pair.of(REDIRECT_PREFIX + getFailedErrorView(), null);
		}

		return Pair.of(null, otpVerificationToken.get());
	}

	/**
	 * Show verify page.
	 *
	 * @param token
	 *           the token
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	public String showVerifyPage(final String token, final Model model, final HttpServletRequest request,
			final HttpServletResponse response) throws CMSItemNotFoundException
	{

		final Pair<String, OTPVerificationTokenModel> validate = validate(token, model, response);

		if (Strings.isNotBlank(validate.getLeft()))
		{
			return validate.getLeft();
		}

		final OTPVerificationTokenModel otpVerificationToken = validate.getRight();

		final OTPForm otpForm = new OTPForm();

		otpForm.setMobileCountry(otpVerificationToken.getMobileCountry());
		otpForm.setMobileNumber(otpVerificationToken.getMobileNumber());

		model.addAttribute("otpForm", otpForm);
		model.addAttribute("mobileNumber", otpForm.getMobileNumber());
		model.addAttribute("otpToken", getEncodeToken(otpVerificationToken.getToken()));
		model.addAttribute(OTPConstants.OTP_VIEW, OTPConstants.Views.VERIFY_CODE_VIEW);

		return getView(model);

	}

	/**
	 * Gets the decode token.
	 *
	 * @param token
	 *           the token
	 * @return the decode token
	 */
	protected String getDecodeToken(String token)
	{
		if (StringUtils.isBlank(token))
		{
			return token;
		}
		try
		{
			token = URLDecoder.decode(token, StandardCharsets.UTF_8.toString());
		}
		catch (final UnsupportedEncodingException e)
		{
			LOG.error(String.format("token from otpVerificationTokenModel could not be decoded : %s", token), e);
		}
		return token;
	}

	/**
	 * Gets the encode token.
	 *
	 * @param token
	 *           the token
	 * @return the encode token
	 */
	protected String getEncodeToken(String token)
	{
		if (StringUtils.isBlank(token))
		{
			return token;
		}
		try
		{
			token = URLEncoder.encode(token, StandardCharsets.UTF_8.toString());
		}
		catch (final UnsupportedEncodingException e)
		{
			LOG.error(String.format("token from otpVerificationTokenModel could not be decoded : %s", token), e);
		}
		return token;
	}

	/**
	 * Send OTP code.
	 *
	 * @param model
	 *           the model
	 * @param mobileCountry
	 *           the mobile country
	 * @param mobileNumber
	 *           the mobile number
	 * @param data
	 *           the data
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	private String sendOTPCode(final Model model, final String mobileCountry, final String mobileNumber, final Object data)
			throws CMSItemNotFoundException
	{
		try
		{
			final Optional<OTPVerificationTokenModel> otpVerificationTokenModel = getOtpContextV2()
					.sendOTPCodeByCurrentSiteAndCustomer(getOTPVerificationTokenType(), mobileCountry, mobileNumber, data);
			if (otpVerificationTokenModel.isEmpty())
			{
				GlobalMessages.addErrorMessage(model, MESSAGE_NOT_SENT_MESSAGE);
				return handleError(model);
			}

			final String encode = getEncodeToken(otpVerificationTokenModel.get().getToken());
			return REDIRECT_PREFIX + getOTPViewURL() + "?token=" + encode;
		}
		catch (final OTPException ex)
		{
			switch (ex.geType())
			{
				case DISABLED:
				case SERVICE_UNAVAILABLE:
				case CMS_SITE_NOT_FOUND:
				case OTP_CONFIG_UNAVAILABLE:
				case OTP_TYPE_NOTE_FOUND:
				case VERIFICATION_ERROR:
				case MESSAGE_CAN_NOT_BE_SENT:
					GlobalMessages.addErrorMessage(model, MESSAGE_NOT_SENT_MESSAGE);
			}
			return handleError(model);
		}
	}

	/**
	 * Send.
	 *
	 * @param token
	 *           the token
	 * @param otpForm
	 *           the otp form
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param redirectModel
	 *           the redirect model
	 * @param bindingResult
	 *           the binding result
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	public String send(String token, final OTPForm otpForm, final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel, final BindingResult bindingResult)
			throws CMSItemNotFoundException
	{
		token = getDecodeToken(token);
		final Pair<String, OTPVerificationTokenModel> validate = validate(token, model, response);

		if (Strings.isNotBlank(validate.getLeft()))
		{
			return validate.getLeft();
		}

		final OTPVerificationTokenModel otpVerificationToken = validate.getRight();

		getOTPValidator().validateSend(otpForm, bindingResult);

		if (bindingResult.hasErrors())
		{
			model.addAttribute("otpForm", otpForm);
			model.addAttribute("mobileNumber", otpForm.getMobileNumber());
			model.addAttribute("otpToken", getEncodeToken(otpVerificationToken.getToken()));
			model.addAttribute(OTPConstants.OTP_VIEW, OTPConstants.Views.SEND_CODE_VIEW);
			GlobalMessages.addErrorMessage(model, FORM_GLOBAL_ERROR);
			return handleError(model);
		}

		return sendOTPCode(model, otpForm.getMobileCountry(), otpForm.getMobileNumber(), otpVerificationToken.getData());
	}

	/**
	 * Resend OTP code.
	 *
	 * @param token
	 *           the token
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param redirectModel
	 *           the redirect model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	public String resendOTPCode(String token, final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		token = getDecodeToken(token);
		final Pair<String, OTPVerificationTokenModel> validate = validate(token, model, response);

		if (Strings.isNotBlank(validate.getLeft()))
		{
			return validate.getLeft();
		}

		final OTPVerificationTokenModel otpVerificationToken = validate.getRight();

		return sendOTPCode(model, otpVerificationToken.getMobileCountry(), otpVerificationToken.getMobileNumber(),
				otpVerificationToken.getData());

	}

	/**
	 * Gets the change mobile number view.
	 *
	 * @param token
	 *           the token
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @return the change mobile number view
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	public String getChangeMobileNumberView(@RequestParam(required = false)
	String token, final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		token = getDecodeToken(token);

		final Pair<String, OTPVerificationTokenModel> validate = validate(token, model, response);

		if (Strings.isNotBlank(validate.getLeft()))
		{
			return validate.getLeft();
		}

		final OTPVerificationTokenModel otpVerificationToken = validate.getRight();

		final OTPForm otpForm = new OTPForm();

		otpForm.setMobileCountry(otpVerificationToken.getMobileCountry());
		otpForm.setMobileNumber(otpVerificationToken.getMobileNumber());

		model.addAttribute("otpForm", otpForm);
		model.addAttribute("otpToken", getEncodeToken(otpVerificationToken.getToken()));
		model.addAttribute(OTPConstants.OTP_VIEW, OTPConstants.Views.SEND_CODE_VIEW);

		return getView(model);
	}

	/**
	 * Verify.
	 *
	 * @param token
	 *           the token
	 * @param otpForm
	 *           the otp form
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param redirectModel
	 *           the redirect model
	 * @param bindingResult
	 *           the binding result
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 * @throws TokenInvalidatedException
	 *            the token invalidated exception
	 */
	public String verify(String token, final OTPForm otpForm, final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel, final BindingResult bindingResult)
			throws CMSItemNotFoundException, TokenInvalidatedException
	{

		token = getDecodeToken(token);

		final Pair<String, OTPVerificationTokenModel> validate = validate(token, model, response);

		if (Strings.isNotBlank(validate.getLeft()))
		{
			return validate.getLeft();
		}

		final OTPVerificationTokenModel otpVerificationToken = validate.getRight();

		otpForm.setMobileCountry(otpVerificationToken.getMobileCountry());
		otpForm.setMobileNumber(otpVerificationToken.getMobileNumber());
		getOTPValidator().validate(otpForm, bindingResult);

		if (bindingResult.hasErrors())
		{
			model.addAttribute("otpForm", otpForm);
			model.addAttribute("mobileNumber", otpForm.getMobileNumber());
			model.addAttribute("otpToken", getEncodeToken(otpVerificationToken.getToken()));
			model.addAttribute(OTPConstants.OTP_VIEW, OTPConstants.Views.VERIFY_CODE_VIEW);
			GlobalMessages.addErrorMessage(model, "otp.otpCode.format.invalid");
			return handleError(model);
		}


		final String returnDoAction = doAction(model, redirectModel, otpVerificationToken, request, response, bindingResult);
		getOtpContextV2().removeTokenByCurrentCustomer(otpVerificationToken.getToken());

		return returnDoAction;
	}

	/**
	 * Gets the thankyou.
	 *
	 * @param token
	 *           the token
	 * @param otpForm
	 *           the otp form
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param redirectModel
	 *           the redirect model
	 * @return the thankyou
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	public String getThankyou(String token, final OTPForm otpForm, final Model model, final HttpServletRequest request,
			final HttpServletResponse response,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		if (StringUtils.isNotBlank(token))
		{
			token = getDecodeToken(token);
		}
		final String errorKey = (String) sessionService.getAttribute(OTPConstants.Actions.ErrorMessegeKey.THANK_YOU);
		if (StringUtils.isNotBlank(errorKey))
		{
			sessionService.removeAttribute(OTPConstants.Actions.ErrorMessegeKey.THANK_YOU);
			return getView(model);
		}

		return REDIRECT_PREFIX + "/";
	}

	/**
	 * Gets the mobile countries.
	 *
	 * @return the mobile countries
	 */
	@ModelAttribute("mobileCountries")
	public Collection<CountryData> getMobileCountries()
	{
		final Optional<List<CountryData>> mobileCountries = countryFacade.getMobileCountriesByCuruntSite();
		return mobileCountries.isPresent() ? mobileCountries.get() : null;
	}

	/**
	 * Gets the OTP verify action URL.
	 *
	 * @return the OTP verify action URL
	 */
	@ModelAttribute("otpVerifyActionURL")
	public String getOTPVerifyActionURL()
	{
		return getVerifyActionURL();
	}

	/**
	 * Gets the OTP change number action URL.
	 *
	 * @return the OTP change number action URL
	 */
	@ModelAttribute("otpChangeNumberActionURL")
	public String getOTPChangeNumberActionURL()
	{
		return getChangeNumberActionURL();
	}

	/**
	 * Gets the OTP resend verify action URL.
	 *
	 * @return the OTP resend verify action URL
	 */
	@ModelAttribute("otpResendVerifyActionURL")
	public String getOTPResendVerifyActionURL()
	{
		return getResendVerifyActionURL();
	}

	/**
	 * Gets the OTP send verify action URL.
	 *
	 * @return the OTP send verify action URL
	 */
	@ModelAttribute("otpSendVerifyActionURL")
	public String getOTPSendVerifyActionURL()
	{
		return getSendVerifyActionURL();
	}

	/**
	 * Gets the view.
	 *
	 * @param model
	 *           the model
	 * @return the view
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	private String getView(final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(getPageLabelOrId()));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(getPageLabelOrId()));
		return getViewForPage(model);
	}


	/**
	 * Handle error.
	 *
	 * @param model
	 *           the model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	protected String handleError(final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(getPageLabelOrId()));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(getPageLabelOrId()));

		return getViewForPage(model);
	}

	/**
	 * Handle redirect error.
	 *
	 * @param model
	 *           the model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	protected String handleRedirectError(final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getCmsPage(getFailedErrorLabelOrIdPage()));
		setUpMetaDataForContentPage(model, (ContentPageModel) getCmsPage(getFailedErrorLabelOrIdPage()));
		return getViewForPage(model);
	}

	/**
	 * Gets the not found page.
	 *
	 * @param model
	 *           the model
	 * @param response
	 *           the response
	 * @return the not found page
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	protected String getNotFoundPage(final Model model, final HttpServletResponse response) throws CMSItemNotFoundException
	{
		prepareNotFoundPage(model, response);
		return ControllerConstants.Views.Pages.Error.ErrorNotFoundPage;
	}

	/**
	 * Gets the cms page.
	 *
	 * @param labelOrIdPage
	 *           the label or id page
	 * @return the cms page
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	protected AbstractPageModel getCmsPage(final String labelOrIdPage) throws CMSItemNotFoundException
	{
		return getContentPageForLabelOrId(labelOrIdPage);
	}


	/**
	 * Returns the Spring bean with name <code>beanName</code> and of type <code>beanClass</code>.
	 *
	 * @param <T>
	 *           type of the bean
	 * @param httpRequest
	 *           the http request
	 * @param beanName
	 *           name of the bean
	 * @param beanClass
	 *           expected type of the bean
	 * @return the bean matching the given arguments or <code>null</code> if no bean could be resolved
	 */
	public static <T> T getSpringBean(final HttpServletRequest httpRequest, final String beanName, final Class<T> beanClass)
	{
		return RequestContextUtils.findWebApplicationContext(httpRequest, httpRequest.getSession().getServletContext())
				.getBean(beanName, beanClass);
	}

	protected String getPageLabelOrId()
	{
		// XXX Auto-generated method stub
		return "/verify";
	}

	protected String getSuccessRedirect(final HttpServletRequest request, final HttpServletResponse response)
	{
		if (httpSessionRequestCache.getRequest(request, response) != null)
		{
			return httpSessionRequestCache.getRequest(request, response).getRedirectUrl();
		}
		return "/";
	}

	/**
	 * @return the otpContextV2
	 */
	protected OTPContextV2 getOtpContextV2()
	{
		return otpContextV2;
	}

	/**
	 * @return the customerFacade
	 */
	@Override
	protected CustomCustomerFacade getCustomerFacade()
	{
		return customerFacade;
	}

	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}
}
