/**
 *
 */
package com.aldahra.storefront.controllers.pages.otp;

/**
 * The Interface OTPConstants.
 *
 * @author mnasro
 */
public interface OTPConstants
{

	/** The otp view. */
	String OTP_VIEW = "OTP_VIEW"; //NOSONAR

	/**
	 * The Interface Actions.
	 */
	interface Actions
	{

		/**
		 * The Interface ErrorMessegeKey.
		 */
		interface ErrorMessegeKey
		{

			/** The registration. */
			String REGISTRATION = "REGISTRATION_OTP_ErrorMessegeKey"; //NOSONAR
			/** The thank_you. */
			String THANK_YOU = "THANK_YOU_OTP_ErrorMessegeKey"; //NOSONAR

		}
	}

	/**
	 * The Interface Views.
	 */
	interface Views
	{

		/** The verify code view. */
		String VERIFY_CODE_VIEW="VERIFY_CODE_VIEW"; //NOSONAR

		/** The send code view. */
		String SEND_CODE_VIEW="SEND_CODE_VIEW"; //NOSONAR

		/** The thank you view. */
		String THANK_YOU_VIEW="THANK_YOU_VIEW"; //NOSONAR
	}


}
