/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.storefront.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateQuoteCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.order.CartService;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.aldahra.aldahrastorecredit.enums.StoreCreditModeType;
import com.aldahra.aldahrastorecredit.exception.StoreCreditException;
import com.aldahra.aldahrastorecreditfacades.data.StoreCreditModeData;
import com.aldahra.storefront.checkout.steps.CheckoutStep;
import com.aldahra.storefront.controllers.ControllerConstants;
import com.aldahra.storefront.form.StoreCreditForm;


@Controller
@RequestMapping(value = "/checkout/multi/store-credit")
public class StoreCreditCheckoutStepController extends AbstractCheckoutStepController
{
	private static final String STORE_CREDIT = "store-credit";

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "storeCreditValidator")
	private Validator storeCreditValidator;

	@RequestMapping(value = "/choose", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	@PreValidateQuoteCheckoutStep
	@PreValidateCheckoutStep(checkoutStep = STORE_CREDIT)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		final CartData cartData = getCheckoutFacade().getCheckoutCart();


		final Optional<PriceData> availableBalanceStoreCredit = getCheckoutFacade().getAvailableBalanceStoreCreditAmount();

		model.addAttribute("availableBalanceStoreCredit",
				availableBalanceStoreCredit.isPresent() ? availableBalanceStoreCredit.get() : null);

		final Optional<PriceData> storeCreditAmountFullRedeem = getCheckoutFacade().getStoreCreditAmountFullRedeem();

		model.addAttribute("storeCreditAmountFullRedeem",
				storeCreditAmountFullRedeem.isPresent() ? storeCreditAmountFullRedeem.get() : null);


		model.addAttribute("cartData", cartData);
		this.prepareDataForPage(model);
		final ContentPageModel multiCheckoutSummaryPage = getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL);
		storeCmsPageInModel(model, multiCheckoutSummaryPage);
		setUpMetaDataForContentPage(model, multiCheckoutSummaryPage);
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.timeSlot.breadcrumb"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		setCheckoutStepLinksForModel(model, getCheckoutStep());

		return ControllerConstants.Views.Pages.MultiStepCheckout.ChooseStoreCreditPage;
	}

	@ModelAttribute("supportedStoreCreditModes")
	public Collection<StoreCreditModeData> getSupportedStoreCreditModes()
	{
		try
		{
			final Optional<List<StoreCreditModeData>> supportedStoreCreditModes = getCheckoutFacade().getSupportedStoreCreditModes();
			return supportedStoreCreditModes.isPresent() ? supportedStoreCreditModes.get() : Collections.emptyList();
		}
		catch (final StoreCreditException e)
		{
			return Collections.emptyList();
		}
	}


	@RequestMapping(value = "/choose", method = RequestMethod.POST)
	@RequireHardLogIn
	public String doSelectStoreCredit(final StoreCreditForm storeCreditForm, final Model model,
			final RedirectAttributes redirectAttributes, final BindingResult bindingResult)
			throws CMSItemNotFoundException, CommerceCartModificationException
	{
		storeCreditValidator.validate(storeCreditForm, bindingResult);

		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "checkout.error.storecredit.entry.invalid");
			return ControllerConstants.Views.Pages.MultiStepCheckout.ChooseStoreCreditPage;
		}

		if (StoreCreditModeType.REDEEM_SPECIFIC_AMOUNT.getCode().equals(storeCreditForm.getSctCode()))
		{
			final double scAmountValue = Double.parseDouble(storeCreditForm.getScAmount());

			getCheckoutFacade().setStoreCreditMode(StoreCreditModeType.REDEEM_SPECIFIC_AMOUNT.getCode(),
					Double.valueOf(scAmountValue));
		}
		else
		{
			getCheckoutFacade().setStoreCreditMode(storeCreditForm.getSctCode(), null);
		}

		setupAddPaymentPage(model);
		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		model.addAttribute("cartData", cartData);
		setCheckoutStepLinksForModel(model, getCheckoutStep());

		return getCheckoutStep().nextStep();
	}

	@RequestMapping(value = "/back", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}

	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(STORE_CREDIT);
	}

	protected void setupAddPaymentPage(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute("metaRobots", "noindex,nofollow");
		model.addAttribute("hasNoPaymentInfo", Boolean.valueOf(getCheckoutFlowFacade().hasNoPaymentInfo()));
		prepareDataForPage(model);
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.storecredit.breadcrumb"));
		final ContentPageModel contentPage = getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL);
		storeCmsPageInModel(model, contentPage);
		setUpMetaDataForContentPage(model, contentPage);
		setCheckoutStepLinksForModel(model, getCheckoutStep());
	}


}
