/**
 *
 */
package com.aldahra.storefront.controllers.pages.otp;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.customer.TokenInvalidatedException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.aldahra.aldahraotp.context.OTPContextV2;
import com.aldahra.aldahraotp.enums.OTPVerificationTokenType;
import com.aldahra.aldahraotp.model.OTPVerificationTokenModel;
import com.aldahra.storefront.controllers.pages.CheckoutRegisterController;
import com.aldahra.storefront.form.OTPForm;


/**
 * The Class OTPCheckoutRegisterVerifyPage.
 *
 * @author @author mnasro
 */
@Controller
@Scope("tenant")
public class OTPCheckoutRegisterVerifyPage extends AbstractRegisterVerifyPage
{

	/** The otp context V 2. */
	@Resource(name = "otpContextV2")
	private OTPContextV2 otpContextV2;

	/**
	 * Show verify page.
	 *
	 * @param token
	 *           the token
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@Override
	@RequestMapping(method = RequestMethod.GET, value = "/register/verify/checkout")
	public String showVerifyPage(@RequestParam(required = true)
	final String token, final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		return super.showVerifyPage(token, model, request, response);
	}

	/**
	 * Resend OTP code.
	 *
	 * @param token
	 *           the token
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param redirectModel
	 *           the redirect model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@Override
	@RequestMapping(method = RequestMethod.POST, value = "/register/verify/resend/checkout")
	public String resendOTPCode(@RequestParam(required = true)
	final String token, final Model model, final HttpServletRequest request, final HttpServletResponse response,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		return super.resendOTPCode(token, model, request, response, redirectModel);
	}

	/**
	 * Gets the change mobile number view.
	 *
	 * @param token
	 *           the token
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @return the change mobile number view
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@Override
	@RequestMapping(method = RequestMethod.GET, value = "/register/verify/change-number/checkout")
	public String getChangeMobileNumberView(@RequestParam(required = true)
	final String token, final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		return super.getChangeMobileNumberView(token, model, request, response);
	}


	/**
	 * Send.
	 *
	 * @param token
	 *           the token
	 * @param otpForm
	 *           the otp form
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param redirectModel
	 *           the redirect model
	 * @param bindingResult
	 *           the binding result
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@Override
	@RequestMapping(method = RequestMethod.POST, value = "/register/verify/send/checkout")
	public String send(@RequestParam(required = true)
	final String token, final OTPForm otpForm, final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel, final BindingResult bindingResult)
			throws CMSItemNotFoundException
	{
		return super.send(token, otpForm, model, request, response, redirectModel, bindingResult);
	}

	/**
	 * Verify.
	 *
	 * @param token
	 *           the token
	 * @param otpForm
	 *           the otp form
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param redirectModel
	 *           the redirect model
	 * @param bindingResult
	 *           the binding result
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 * @throws TokenInvalidatedException
	 *            the token invalidated exception
	 */
	@Override
	@RequestMapping(method = RequestMethod.POST, value = "/register/verify/checkout")
	public String verify(@RequestParam(required = true)
	final String token, final OTPForm otpForm, final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel, final BindingResult bindingResult)
			throws CMSItemNotFoundException, TokenInvalidatedException
	{
		return super.verify(token, otpForm, model, request, response, redirectModel, bindingResult);
	}

	/**
	 * Gets the thankyou.
	 *
	 * @param token
	 *           the token
	 * @param otpForm
	 *           the otp form
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param redirectModel
	 *           the redirect model
	 * @return the thankyou
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@Override
	public String getThankyou(@RequestParam(required = false)
	final String token, final OTPForm otpForm, final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		return super.getThankyou(token, otpForm, model, request, response, redirectModel);
	}

	/**
	 * Gets the OTP view URL.
	 *
	 * @return the OTP view URL
	 */
	@Override
	protected String getOTPViewURL()
	{
		return "/register/verify/checkout";
	}

	/**
	 * Gets the OTP verification token type.
	 *
	 * @return the OTP verification token type
	 */
	@Override
	protected OTPVerificationTokenType getOTPVerificationTokenType()
	{
		return OTPVerificationTokenType.CHECKOUT_REGISTRATION;
	}

	/**
	 * Gets the failed error label or id page.
	 *
	 * @return the failed error label or id page
	 */
	@Override
	protected String getFailedErrorLabelOrIdPage()
	{
		return "register";
	}

	/**
	 * Gets the failed error view.
	 *
	 * @return the failed error view
	 */
	@Override
	protected String getFailedErrorView()
	{
		return "/register";
	}


	/**
	 * Gets the register page controller.
	 *
	 * @param httpRequest
	 *           the http request
	 * @return the register page controller
	 */
	protected static CheckoutRegisterController getRegisterPageController(final HttpServletRequest httpRequest)
	{
		return getSpringBean(httpRequest, "checkoutRegisterController", CheckoutRegisterController.class);
	}

	/**
	 * Do action.
	 *
	 * @param model
	 *           the model
	 * @param redirectModel
	 *           the redirect model
	 * @param otpVerificationToken
	 *           the otp verification token
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param bindingResult
	 *           the binding result
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@Override
	protected String doAction(final Model model, final RedirectAttributes redirectModel,
			final OTPVerificationTokenModel otpVerificationToken, final HttpServletRequest request,
			final HttpServletResponse response, final BindingResult bindingResult) throws CMSItemNotFoundException
	{

		final RegisterData data = (RegisterData) otpVerificationToken.getData();
		return getRegisterPageController(request).registrationCustomer(model, redirectModel, data, request, response,
				bindingResult);
	}


	/**
	 * Gets the verify action URL.
	 *
	 * @return the verify action URL
	 */
	@Override
	protected String getVerifyActionURL()
	{
		return "/register/verify/checkout";
	}

	/**
	 * Gets the change number action URL.
	 *
	 * @return the change number action URL
	 */
	@Override
	protected String getChangeNumberActionURL()
	{
		return "/register/verify/change-number/checkout";
	}

	/**
	 * Gets the resend verify action URL.
	 *
	 * @return the resend verify action URL
	 */
	@Override
	protected String getResendVerifyActionURL()
	{
		return "/register/verify/resend/checkout";
	}

	/**
	 * Gets the send verify action URL.
	 *
	 * @return the send verify action URL
	 */
	@Override
	protected String getSendVerifyActionURL()
	{
		return "/register/verify/send/checkout";
	}

}
