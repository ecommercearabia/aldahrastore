/**
 *
 */
package com.aldahra.storefront.controllers.misc;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;

import java.util.Optional;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.aldahra.aldahrawishlistfacade.data.ResponseData;
import com.aldahra.aldahrawishlistfacade.mapper.WishlistResponseMapper;



/**
 *
 *
 * @author mohammad-abu-muhasien
 */
@RestController
@RequestMapping("/wishlists")
public class WishlistController extends AbstractController
{

	/** The wishlist response mapper. */
	@Resource(name = "wishlistResponseMapper")
	private WishlistResponseMapper wishlistResponseMapper;

	/**
	 * Gets the wishlists.
	 *
	 * @return the wishlists
	 */
	@GetMapping
	public ResponseData getWishlists()
	{
		final Optional<ResponseData> wishLists = wishlistResponseMapper.getWishLists();
		return validateResponseData(wishLists);
	}

	/**
	 * Gets the wishlist by PK.
	 *
	 * @param pk
	 *           the pk
	 * @return the wishlist by PK
	 */
	@GetMapping("/{pk}")
	public ResponseData getWishlistByPK(@PathVariable
	final String pk)
	{
		final Optional<ResponseData> wishlistByPK = wishlistResponseMapper.getWishlistByPK(pk);
		return validateResponseData(wishlistByPK);
	}

	/**
	 * Gets the default wishlist.
	 *
	 * @return the default wishlist
	 */
	@GetMapping("/default")
	public ResponseData getDefaultWishlist()
	{
		final Optional<ResponseData> defaultWishList = wishlistResponseMapper.getDefaultWishList();
		return validateResponseData(defaultWishList);
	}

	/**
	 * Gets the wish list entries.
	 *
	 * @param pk
	 *           the pk
	 * @param currentPage
	 *           the current page
	 * @param pageSize
	 *           the page size
	 * @return the wish list entries
	 */
	@GetMapping("/{pk}/entries")
	public ResponseData getWishListEntries(@PathVariable
	final String pk, @RequestParam(value = "currentPage", defaultValue = "0")
	final int currentPage, @RequestParam(value = "pageSize", defaultValue = "5")
	final int pageSize)
	{
		final Optional<ResponseData> wishListEntries = wishlistResponseMapper.getWishListEntries(pk, currentPage, pageSize);
		return validateResponseData(wishListEntries);

	}

	/**
	 * Removes the wish list entry by product code and PK.
	 *
	 * @param pk
	 *           the pk
	 * @param productCode
	 *           the product code
	 * @return the response data
	 */
	@DeleteMapping("/{pk}/entries/{productCode}")
	public ResponseData removeWishListEntryByProductCodeAndPK(@PathVariable
	final String pk, @PathVariable
	final String productCode)
	{
		final Optional<ResponseData> removeWishlistEntryForProduct = wishlistResponseMapper.removeWishlistEntryForProduct(pk,
				productCode);
		return validateResponseData(removeWishlistEntryForProduct);
	}

	@DeleteMapping("/entries/{productCode}")
	public ResponseData removeDefaultWishListEntryByProductCode(@PathVariable
	final String productCode)
	{
		final Optional<ResponseData> removeWishlistEntryForProduct = wishlistResponseMapper
				.removeWishlistEntryForProduct(productCode);
		return validateResponseData(removeWishlistEntryForProduct);
	}



	/**
	 * Edits the wishlist name.
	 *
	 * @param pk
	 *           the pk
	 * @param newName
	 *           the new name
	 * @return the response data
	 */
	@PutMapping("/{pk}")
	public ResponseData editWishlistName(@PathVariable
	final String pk, @PathVariable
	@RequestParam(value = "newName", defaultValue = "Wishlist")
	final String newName)
	{
		final Optional<ResponseData> editWishlistName = wishlistResponseMapper.editWishlistName(newName, pk);
		return validateResponseData(editWishlistName);
	}

	/**
	 * Adds the wishlist entry.
	 *
	 * @param productCode
	 *           the product code
	 * @param pk
	 *           the pk
	 * @return the response data
	 */
	@PutMapping("/{pk}/entries/{productCode}")
	public ResponseData addWishlistEntry(@PathVariable
	final String productCode, @PathVariable
	final String pk)
	{
		final Optional<ResponseData> addWishlistEntry = wishlistResponseMapper.addWishlistEntry(productCode, pk);
		return validateResponseData(addWishlistEntry);


	}

	@PutMapping("/entries/{productCode}")
	public ResponseData addDefaultWishlistEntry(@PathVariable
	final String productCode)
	{
		final Optional<ResponseData> addWishlistEntry = wishlistResponseMapper.addEntryToDefaultWishlistByProductCode(productCode);
		return validateResponseData(addWishlistEntry);


	}

	/**
	 * Adds the all entries to cart.
	 *
	 * @param pk
	 *           the pk
	 * @return the response data
	 */
	@PostMapping("/{pk}/add-all-to-cart")
	public ResponseData addAllEntriesToCart(@PathVariable
	final String pk)
	{
		final Optional<ResponseData> addAllEntriesToCart = wishlistResponseMapper.addAllEntriesToCart(pk);
		return validateResponseData(addAllEntriesToCart);
	}

	/**
	 * Removes the all wishlist entries.
	 *
	 * @param pk
	 *           the pk
	 * @return the response data
	 */
	@DeleteMapping("/{pk}/entries/remove")
	public ResponseData removeAllWishlistEntries(@PathVariable
	final String pk)
	{
		final Optional<ResponseData> removeAllWishlistEntries = wishlistResponseMapper.removeAllWishlistEntries(pk);
		return validateResponseData(removeAllWishlistEntries);
	}



	/**
	 * Checks if is product in wish list by PK.
	 *
	 * @param pk
	 *           the pk
	 * @param productCode
	 *           the product code
	 * @return the response data
	 */
	@GetMapping("/{pk}/entries/{productCode}/isfound")
	public ResponseData isProductInWishListByPK(@PathVariable
	final String pk, @PathVariable
	final String productCode)
	{
		final Optional<ResponseData> isProductInWishList = wishlistResponseMapper.isProductInWishList(pk, productCode);
		return validateResponseData(isProductInWishList);

	}

	/**
	 * Checks if is product in wish list.
	 *
	 * @param productCode
	 *           the product code
	 * @return the response data
	 */
	@GetMapping("/entries/{productCode}/isfound")
	public ResponseData isProductInWishList(@PathVariable
	final String productCode)
	{

		final Optional<ResponseData> isProductInWishList = wishlistResponseMapper.isProductInWishList(productCode);
		return validateResponseData(isProductInWishList);
	}


	/**
	 * Removes the wishlist.
	 *
	 * @param pk
	 *           the pk
	 * @return the response data
	 */
	@DeleteMapping("/{pk}")

	public ResponseData removeWishlist(@PathVariable
	final String pk)
	{
		final Optional<ResponseData> removeWishListByPK = wishlistResponseMapper.removeWishListByPK(pk);
		return validateResponseData(removeWishListByPK);
	}


	/**
	 * Creates the wishlist.
	 *
	 * @param name
	 *           the name
	 * @param description
	 *           the description
	 * @param defaultWL
	 *           the default WL
	 * @return the response data
	 */
	@PostMapping("/create")

	public ResponseData createWishlist(@RequestParam(value = "name", required = true)
	final String name, @RequestParam(value = "description", required = false)
	final String description, @RequestParam(value = "defaultWL", required = false, defaultValue = "false")
	final Boolean defaultWL)
	{
		final Optional<ResponseData> createWishlist = wishlistResponseMapper.createWishlist(name, description, defaultWL);
		return validateResponseData(createWishlist);
	}

	/**
	 * Validate response data.
	 *
	 * @param responseData
	 *           the response data
	 * @return the response data
	 */
	private ResponseData validateResponseData(final Optional<ResponseData> responseData)
	{
		if (!responseData.isPresent())
		{
			return null;
		}
		return responseData.get();
	}

}




