/**
 *
 */
package com.aldahra.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.order.InvalidCartException;

import java.text.ParseException;
import java.util.Optional;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.aldahra.aldahrareferralcode.exception.ReferralCodeException;
import com.aldahra.aldahrareferralcode.service.ReferralCodeService;
import com.aldahra.aldahrareferralcodefacades.ReferralCodeData;
import com.aldahra.aldahrareferralcodefacades.facade.ReferralCodeFacade;




/**
 * @author mnasro
 *
 */
@Controller
@Scope("tenant")
@RequestMapping(value = "/my-account/referral-code")
public class ReferralCodeController extends AbstractPageController
{
	private static final String BREADCRUMBS_ATTR = "breadcrumbs";


	@Resource(name = "accountBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

	@Resource(name = "referralCodeFacade")
	private ReferralCodeFacade referralCodeFacade;

	@Resource(name = "referralCodeService")
	private ReferralCodeService referralCodeService;

	private static final String REFERRAL_CODE_VIEW = "/my-account/referral-code";
	private static final String MY_ACCOUNT_VIEW = "/my-account";


	@RequestMapping(method = RequestMethod.GET)
	@RequireHardLogIn
	public String getReferralCodePage(final Model model) throws CMSItemNotFoundException
	{

		final boolean enabledByCurrentBaseStore = referralCodeFacade.isEnabledByCurrentBaseStore();
		if (!enabledByCurrentBaseStore)
		{
			GlobalMessages.addMessage(model, GlobalMessages.ERROR_MESSAGES_HOLDER, "text.account.referralcode.enable.error", null);

			return REDIRECT_PREFIX + "/my-account";
		}
		final Optional<ReferralCodeData> referralCodeData = referralCodeFacade.getByCurrentCustomer();
		if (!referralCodeData.isEmpty())
		{
			model.addAttribute("referralCode", referralCodeData.get());
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(REFERRAL_CODE_VIEW));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(REFERRAL_CODE_VIEW));
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs("text.account.referralcode"));
		return getViewForPage(model);

	}

	@RequestMapping(value = "/generate", method = RequestMethod.POST)
	@RequireHardLogIn
	public String generate(final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException, InvalidCartException, ParseException, CommerceCartModificationException
	{
		try
		{
			referralCodeService.generateByCurrentBaseStoreAndCurrentCustomer();
		}
		catch (final ReferralCodeException ex)
		{

			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "referralcode.error.generate",
					new Object[]
					{ ex });
		}
		return REDIRECT_PREFIX + MY_ACCOUNT_VIEW;
	}

	@RequestMapping(value = "/generate", method = RequestMethod.GET)
	@RequireHardLogIn
	public String generateGET(final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException, InvalidCartException, ParseException, CommerceCartModificationException
	{
		try
		{
			referralCodeService.generateByCurrentBaseStoreAndCurrentCustomer();
		}
		catch (final ReferralCodeException ex)
		{

			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "referralcode.error.generate",
					new Object[]
					{ ex });
		}
		return REDIRECT_PREFIX + MY_ACCOUNT_VIEW;
	}
}
