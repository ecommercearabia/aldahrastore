/**
 *
 */
package com.aldahra.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.aldahra.aldahrapayment.ccavenue.entry.CustomerPaymentOptionData;
import com.aldahra.aldahrapayment.ccavenue.exception.PaymentException;
import com.aldahra.aldahrapayment.context.PaymentContext;
import com.aldahra.storefront.controllers.ControllerConstants;


/**
 * @author husam.dababneh@erabia.com
 *
 */
@Controller
@RequestMapping("/my-account/manage-saved-cards")
public class ManageSavedCardsPageController extends AbstractPageController
{
	private static final String BREADCRUMBS_ATTR = "breadcrumbs";
	private static final String MANAGE_SAVED_CARDS_PAGE_LABEL = "/my-account/manage-saved-cards";
	private static final String REDIRECT_MANAGE_SAVED_CARDS_PAGE = REDIRECT_PREFIX + "/my-account/manage-saved-cards";
	private static final String CUSTOMER_CARD_ID_PATH_VARIABLE_PATTERN = "/{customerCardId:.*}";

	private static final Logger LOG = Logger.getLogger(ManageSavedCardsPageController.class);

	@Resource(name = "paymentContext")
	private PaymentContext paymentContext;

	@Resource(name = "accountBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

	@Resource(name = "messageSource")
	private MessageSource messageSource;

	@Resource(name = "i18nService")
	private I18NService i18nService;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@GetMapping
	@RequireHardLogIn
	public String getManageSavedCards(final Model model, final HttpServletResponse response) throws CMSItemNotFoundException
	{
		if (!isEnableManageSavedCards())
		{
			return getNotFoundPage(model, response);
		}

		try
		{
			final List<CustomerPaymentOptionData> customerPaymentOptions = getPaymentContext()
					.getCustomerPaymentOptionsByCurrentStoreAndCustomer();
			model.addAttribute("customerPaymentOptions", customerPaymentOptions);

		}
		catch (final PaymentException e)
		{
			LOG.error("ManageSavedCardsPageController -> getManageSavedCards:PaymentException Type[" + e.getType() + "] , Message["
					+ e.getMessage() + "]");

			//TODO Mnasro set manage.saved.cards.failure.result.error , manage.saved.cards.invalid.response.error and manage.saved.cards.bad.request.error msg on the base EN and AR
			switch (e.getType())
			{
				case FAILURE_RESULT:
					GlobalMessages.addConfMessage(model, "manage.saved.cards.failure.result.error");
					break;
				case INVALID_RESPONSE_SYNTAX_EXPECTED_JSON:
					GlobalMessages.addConfMessage(model, "manage.saved.cards.invalid.response.error");
					break;
				case BAD_REQUEST:
					GlobalMessages.addConfMessage(model, "manage.saved.cards.bad.request.error");
					break;
				default:
					GlobalMessages.addConfMessage(model, "manage.saved.cards.bad.request.error");
					break;
			}
		}
		model.addAttribute(BREADCRUMBS_ATTR, getAccountBreadcrumbBuilder().getBreadcrumbs("text.account.manageSavedCards"));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getView(model, "/my-account/manage-saved-cards");
	}

	@PostMapping(value = "/delete/" + CUSTOMER_CARD_ID_PATH_VARIABLE_PATTERN)
	@RequireHardLogIn
	public String deleteCustomerPaymentOption(@PathVariable("customerCardId")
	final String customerCardId, final HttpServletRequest request, final HttpServletResponse response,
			final RedirectAttributes redirectModel, final Model model)
	{
		try
		{
			paymentContext.deleteCustomerPaymentOptionByCurrentStoreAndCustomer(customerCardId);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.INFO_MESSAGES_HOLDER, "manage.saved.cards.is.deleted",
					new Object[]
					{ customerCardId });
		}
		catch (final PaymentException e)
		{
			LOG.error("ManageSavedCardsPageController -> deleteCustomerPaymentOption:PaymentException Type[" + e.getType()
					+ "] , Message[" + e.getMessage() + "]");

			switch (e.getType())
			{
				case FAILURE_RESULT:

					GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
							"manage.saved.cards.failure.result.delete.error", new Object[]
							{ customerCardId });
					break;
				case INVALID_RESPONSE_SYNTAX_EXPECTED_JSON:
					GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
							"manage.saved.cards.invalid.response.delete.error", new Object[]
							{ customerCardId });
					break;

				case BAD_REQUEST:
					GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
							"manage.saved.cards.bad.request.delete.error", new Object[]
							{ customerCardId });
					break;

				default:
					GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
							"manage.saved.cards.bad.request.delete.error", new Object[]
							{ customerCardId });
					break;
			}
		}

		return REDIRECT_MANAGE_SAVED_CARDS_PAGE;
	}


	private String getMessage(final String msgKey, final Object[] msgArg)
	{
		return getMessageSource().getMessage(msgKey, msgArg, getI18nService().getCurrentLocale());
	}

	private String getView(final Model model, final String pageLabel) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(pageLabel));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(pageLabel));
		return getViewForPage(model);
	}

	protected boolean isEnableManageSavedCards()
	{
		final CMSSiteModel currentSite = getCmsSiteService().getCurrentSite();

		return currentSite == null ? false : currentSite.isEnableManageSavedCards();
	}

	/**
	 * Gets the not found page.
	 *
	 * @param model
	 *           the model
	 * @param response
	 *           the response
	 * @return the not found page
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	protected String getNotFoundPage(final Model model, final HttpServletResponse response) throws CMSItemNotFoundException
	{
		prepareNotFoundPage(model, response);
		return ControllerConstants.Views.Pages.Error.ErrorNotFoundPage;
	}

	/**
	 * @return the paymentContext
	 */
	protected PaymentContext getPaymentContext()
	{
		return paymentContext;
	}

	/**
	 * @return the accountBreadcrumbBuilder
	 */
	protected ResourceBreadcrumbBuilder getAccountBreadcrumbBuilder()
	{
		return accountBreadcrumbBuilder;
	}

	/**
	 * @return the cmsSiteService
	 */
	@Override
	protected CMSSiteService getCmsSiteService()
	{
		return cmsSiteService;
	}


}
