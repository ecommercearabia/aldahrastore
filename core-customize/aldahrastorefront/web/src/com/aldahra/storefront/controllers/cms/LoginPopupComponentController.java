package com.aldahra.storefront.controllers.cms;

import de.hybris.platform.acceleratorstorefrontcommons.forms.LoginForm;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.aldahra.aldahracomponents.model.LoginPopupComponentModel;
import com.aldahra.storefront.controllers.ControllerConstants;


/**
 * @author amjad.shati@erabia.com
 *
 */
@Controller("LoginPopupComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.LoginPopupComponent)
public class LoginPopupComponentController extends AbstractAcceleratorCMSComponentController<LoginPopupComponentModel>
{

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final LoginPopupComponentModel component)
	{
		model.addAttribute("loginForm", new LoginForm());
	}

}
