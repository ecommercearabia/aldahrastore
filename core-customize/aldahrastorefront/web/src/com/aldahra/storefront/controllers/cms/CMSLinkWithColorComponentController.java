package com.aldahra.storefront.controllers.cms;

import de.hybris.platform.acceleratorstorefrontcommons.tags.Functions;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.aldahra.aldahracomponents.enums.TopBannerTextAlignment;
import com.aldahra.aldahracomponents.model.CMSLinkWithColorComponentModel;
import com.aldahra.storefront.controllers.ControllerConstants;


/**
 * @author amjad.shati@erabia.com
 *
 */
@Controller("CMSLinkWithColorComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.CMSLinkWithColorComponent)
public class CMSLinkWithColorComponentController extends AbstractAcceleratorCMSComponentController<CMSLinkWithColorComponentModel>
{
	private Converter<CategoryModel, CategoryData> categoryUrlConverter;
	private Converter<ProductModel, ProductData> productUrlConverter;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final CMSLinkWithColorComponentModel component)
	{
		model.addAttribute("content", component.getContent());
		model.addAttribute("backgroundColor", component.getBackgroundColor());
		model.addAttribute("textColor", component.getTextColor());
		model.addAttribute("url", getEncodedUrl(component));
		model.addAttribute("linkName", component.getLinkName());

		model.addAttribute("topBannerTextalignment",
				component.getAlignment() != null ? component.getAlignment().getCode().toLowerCase()
						: TopBannerTextAlignment.CENTER.getCode().toLowerCase());

	}

	protected String getEncodedUrl(final CMSLinkComponentModel component)
	{
		final String url = getUrl(component);
		//		final String encodedUrl = UrlSupport.resolveUrl(url, null, pageContext);
		return url;

	}

	protected String getUrl(final CMSLinkComponentModel component)
	{
		// Call the function getUrlForCMSLinkComponent so that this code is only in one place
		return Functions.getUrlForCMSLinkComponent(component, getProductUrlConverter(), getCategoryUrlConverter());
	}

	protected Converter<ProductModel, ProductData> getProductUrlConverter()
	{
		return productUrlConverter;
	}

	protected Converter<CategoryModel, CategoryData> getCategoryUrlConverter()
	{
		return categoryUrlConverter;
	}
}
