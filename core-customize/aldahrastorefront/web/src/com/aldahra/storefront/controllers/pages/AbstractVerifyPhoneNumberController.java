package com.aldahra.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.aldahra.aldahraotp.context.OTPContext;
import com.aldahra.aldahraotp.entity.SessionData;
import com.aldahra.aldahraotp.exception.OTPException;
import com.aldahra.storefront.controllers.ControllerConstants;
import com.aldahra.storefront.form.OTPForm;
import com.aldahra.storefront.form.validation.OTPValidator;


/**
 *
 * @author @author mnasro
 *
 */
public abstract class AbstractVerifyPhoneNumberController extends AbstractRegisterPageController
{
	private HttpSessionRequestCache httpSessionRequestCache;

	private static final String FORM_GLOBAL_ERROR = "form.global.error";

	private static final String OTP_CONFIG_ERROR_MESSAGE = "otp.config.error.";

	private static final String OTP_PINCODE_ERROR_MESSAGE = "otp.pinCode.format.invalid";

	private static final String TOO_MANY_REQUESTS_MESSAGE = "otp.too.many.requests.message";

	private static final String INVALID_CONFIG_MESSAGE = "otp.invalid.config.message";

	private static final String MESSAGE_NOT_SENT_MESSAGE = "otp.message.not.sent.message";

	private static final String RESEND_PINCODE_MESSAGE = "otp.resend.message";


	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "otpContext")
	private OTPContext otpContext;

	@Resource(name = "otpValidator")
	private OTPValidator otpValidator;

	protected abstract String getLoginView();

	protected abstract String getReturnView();

	protected abstract Logger getLOG();
	@Override
	protected String getView()
	{
		return ControllerConstants.Views.Pages.Account.AccountVerifyPhoneNumber;
	}
	public String changeNumber(final Model model, final HttpServletRequest request, final HttpServletResponse response,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		sessionService.removeAttribute("showThankYouPage");
		sessionService.removeAttribute("isSend");
		return REDIRECT_PREFIX + getReturnView();
	}

	public String resend(final Model model, final HttpServletRequest request, final HttpServletResponse response,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		if (!otpContext.isEnabledByCurrentSite()
				|| otpContext.getSessionData(ControllerConstants.Actions.RegistrationOTP.SessionSaveDataKey).isEmpty()
				|| !(otpContext.getSessionData(ControllerConstants.Actions.RegistrationOTP.SessionSaveDataKey).get()
						.getData() instanceof RegisterData))
		{
			prepareNotFoundPage(model, response);
			return ControllerConstants.Views.Pages.Error.ErrorNotFoundPage;
		}
		final RegisterData data = (RegisterData) otpContext
				.getSessionData(ControllerConstants.Actions.RegistrationOTP.SessionSaveDataKey).get().getData();

		try
		{
			otpContext.sendOTPCodeByCurrentSiteAndSessionData(data.getMobileCountry(), data.getMobileNumber(),
					otpContext.getSessionData(ControllerConstants.Actions.RegistrationOTP.SessionSaveDataKey).get());
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, RESEND_PINCODE_MESSAGE);
			return REDIRECT_PREFIX + getReturnView();
		}
		catch (final OTPException ex)
		{
			switch (ex.geType())
			{
				case DISABLED:
				case SERVICE_UNAVAILABLE:
				case CMS_SITE_NOT_FOUND:
				case OTP_CONFIG_UNAVAILABLE:
				case OTP_TYPE_NOTE_FOUND:
					GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, INVALID_CONFIG_MESSAGE);
					break;
			}
			return REDIRECT_PREFIX + getLoginView();
		}

	}
	public String send(final OTPForm otpForm, final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel, final BindingResult bindingResult)
			throws CMSItemNotFoundException
	{

		if (!otpContext.isEnabledByCurrentSite()
				|| otpContext.getSessionData(ControllerConstants.Actions.RegistrationOTP.SessionSaveDataKey).isEmpty()
				|| !(otpContext.getSessionData(ControllerConstants.Actions.RegistrationOTP.SessionSaveDataKey).get()
						.getData() instanceof RegisterData))
		{
			prepareNotFoundPage(model, response);
			return ControllerConstants.Views.Pages.Error.ErrorNotFoundPage;
		}

		otpValidator.validateSend(otpForm, bindingResult);

		if (bindingResult.hasErrors())
		{
			model.addAttribute("otpForm", otpForm);
			GlobalMessages.addErrorMessage(model, FORM_GLOBAL_ERROR);
			return handleError(model);
		}
		final SessionData data = otpContext.getSessionData(ControllerConstants.Actions.RegistrationOTP.SessionSaveDataKey).get();
		final RegisterData registerData = (RegisterData) data.getData();
			registerData.setMobileNumber(otpForm.getMobileNumber());
			registerData.setMobileCountry(otpForm.getMobileCountry());
		data.setData(registerData);


		try
		{
			otpContext.sendOTPCodeByCurrentSiteAndSessionData(otpForm.getMobileCountry(), otpForm.getMobileNumber(), data);

			return REDIRECT_PREFIX + getReturnView();
		}
		catch (final OTPException ex)
		{
			switch (ex.geType())
			{
				case DISABLED:
				case SERVICE_UNAVAILABLE:
				case CMS_SITE_NOT_FOUND:
				case OTP_CONFIG_UNAVAILABLE:
				case OTP_TYPE_NOTE_FOUND:
					GlobalMessages.addErrorMessage(model, INVALID_CONFIG_MESSAGE);
					break;
			}
			model.addAttribute("otpForm", otpForm);
			return handleError(model);
		}

	}

	public String verify(final OTPForm otpForm, final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel, final BindingResult bindingResult)
			throws CMSItemNotFoundException
	{
		final Optional<SessionData> sessionData = otpContext
				.getSessionData(ControllerConstants.Actions.RegistrationOTP.SessionSaveDataKey);

		otpForm.setMobileCountry(((RegisterData) sessionData.get().getData()).getMobileCountry());
		otpForm.setMobileNumber(((RegisterData) sessionData.get().getData()).getMobileNumber());
		otpValidator.validate(otpForm, bindingResult);

		if (bindingResult.hasErrors())
		{
			model.addAttribute("otpForm", otpForm);
			model.addAttribute("isSend", sessionService.getAttribute("isSend"));
			GlobalMessages.addErrorMessage(model, "otp.otpCode.format.invalid");
			return handleError(model);
		}

		if (sessionData.isEmpty() || !(sessionData.get().getData() instanceof RegisterData))
		{
			GlobalMessages.addErrorMessage(model, FORM_GLOBAL_ERROR);
			return handleError(model);
		}

		sessionService.removeAttribute("showThankYouPage");
		sessionService.removeAttribute("isSend");
		return registrationCustomer(model, redirectModel, (RegisterData) sessionData.get().getData(), request, response,
				bindingResult);

	}

	public String showVerificationPage(final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{

		if (!otpContext.isEnabledByCurrentSite()
				|| otpContext.getSessionData(ControllerConstants.Actions.RegistrationOTP.SessionSaveDataKey).isEmpty()
				|| otpContext.getSessionData(ControllerConstants.Actions.RegistrationOTP.SessionSaveDataKey).get().getData() == null
				|| !(otpContext.getSessionData(ControllerConstants.Actions.RegistrationOTP.SessionSaveDataKey).get()
						.getData() instanceof RegisterData))
		{
			prepareNotFoundPage(model, response);
			return ControllerConstants.Views.Pages.Error.ErrorNotFoundPage;
		}
		if (sessionService.getAttribute("showThankYouPage") != null)
		{
			model.addAttribute("thankYou", sessionService.getAttribute("showThankYouPage"));
			return getView(model);
		}
		model.addAttribute("otpFor", "register");
		final SessionData sessionData = otpContext.getSessionData(ControllerConstants.Actions.RegistrationOTP.SessionSaveDataKey)
				.get();

		final OTPForm otpForm = new OTPForm();

		otpForm.setMobileCountry(((RegisterData) sessionData.getData()).getMobileCountry());
		otpForm.setMobileNumber(((RegisterData) sessionData.getData()).getMobileNumber());

		model.addAttribute("otpForm", otpForm);
		model.addAttribute("isSend", sessionService.getAttribute("isSend"));
		model.addAttribute("mobileNumber", otpForm.getMobileNumber());

		return getView(model);
	}

	public String verifyThankyou(final OTPForm otpForm, final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{

		if (sessionService.getAttribute("showThankYouPage") == null)
		{
			return REDIRECT_PREFIX + "/";
		}
		sessionService.removeAttribute("showThankYouPage");
		otpContext.removeSessionData(ControllerConstants.Actions.RegistrationOTP.SessionSaveDataKey);
		model.addAttribute("thankYou", Boolean.TRUE);
		return getView(model);
	}


	/**
	 * @param model
	 * @param pageControllerConstants
	 * @return
	 * @throws CMSItemNotFoundException
	 */
	private String getView(final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(getReturnView()));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(getReturnView()));
		return getViewForPage(model);
	}

	@Override
	protected AbstractPageModel getCmsPage() throws CMSItemNotFoundException
	{
		return getContentPageForLabelOrId(getReturnView());
	}

	@Override
	protected String getSuccessRedirect(final HttpServletRequest request, final HttpServletResponse response)
	{
		sessionService.setAttribute("showThankYouPage", Boolean.TRUE);
		return PHONENUMBER_VERIFICATION_PAGE_LABEL + "/thank-you";
	}

	@Resource(name = "httpSessionRequestCache")
	public void setHttpSessionRequestCache(final HttpSessionRequestCache accHttpSessionRequestCache)
	{
		this.httpSessionRequestCache = accHttpSessionRequestCache;
	}


	protected String handleError(final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(getReturnView()));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(getReturnView()));

		return getViewForPage(model);
	}
}
