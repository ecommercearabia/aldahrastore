/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.storefront.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateQuoteCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.store.services.BaseStoreService;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Optional;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.aldahra.aldahraorderfacades.data.OrderNoteData;
import com.aldahra.aldahratimeslot.enums.TimeSlotConfigType;
import com.aldahra.aldahratimeslotfacades.TimeSlotData;
import com.aldahra.aldahratimeslotfacades.TimeSlotInfoData;
import com.aldahra.aldahratimeslotfacades.exception.TimeSlotException;
import com.aldahra.aldahratimeslotfacades.facade.TimeSlotFacade;
import com.aldahra.core.enums.DeliveryModeType;
import com.aldahra.facades.exception.ExpressOrderException;
import com.aldahra.facades.exception.enums.ExpressOrderExceptionType;
import com.aldahra.facades.facade.CustomAcceleratorCheckoutFacade;
import com.aldahra.storefront.checkout.steps.CheckoutStep;
import com.aldahra.storefront.controllers.ControllerConstants;
import com.aldahra.storefront.form.DeliveryModeForm;
import com.aldahra.storefront.form.TimeSlotForm;
import com.aldahra.storefront.form.validation.TimeSlotValidator;


@Controller
@RequestMapping(value = "/checkout/multi/time-slot")
public class TimeSlotCheckoutStepController extends AbstractCheckoutStepController
{
	private static final Logger LOG = Logger.getLogger(TimeSlotCheckoutStepController.class);
	private static final String TIME_SLOT = "time-slot";

	@Resource(name = "timeSlotFacade")
	private TimeSlotFacade timeSlotFacade;

	@Resource(name = "i18nService")
	private I18NService i18nService;

	@Resource(name = "timeSlotValidator")
	private TimeSlotValidator timeSlotValidator;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "defaultAcceleratorCheckoutFacade")
	private CustomAcceleratorCheckoutFacade defaultAcceleratorCheckoutFacade;

	@Resource(name = "cartService")
	private CartService cartService;
	@Resource(name = "messageSource")
	private MessageSource messageSource;
	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@RequestMapping(value = "/choose", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	@PreValidateQuoteCheckoutStep
	@PreValidateCheckoutStep(checkoutStep = TIME_SLOT)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		populateCommonModelAttributes(model, getCheckoutFacade().getCheckoutCart(), new TimeSlotForm());

		if (isExpressOrder(model))
		{
			model.addAttribute("deliveryModeTypes", getCheckoutFacade().getDeliveryModeTypes());

			return ControllerConstants.Views.Pages.MultiStepCheckout.ChooseDeliveryModeTypePage;
		}


		return displayTimeSlot(model, redirectAttributes);
	}



	/**
	 * @return
	 */
	private String displayTimeSlot(final Model model, final RedirectAttributes redirectAttributes)
	{
		final CartData cartData = getCheckoutFacade().getCheckoutCart();

		Optional<TimeSlotData> timeSlotData = Optional.empty();
		try
		{
			//			if (TimeSlotConfigType.BY_AREA.equals(cmsSiteService.getCurrentSite().getTimeSlotConfigType()))
			//			{
			//
			//			}
			//			else if (TimeSlotConfigType.BY_DELIVERYMODE.equals(cmsSiteService.getCurrentSite().getTimeSlotConfigType()))
			//			{
			//
			//			}
			final AddressData cartAddress = getCartAddress();

			if ((TimeSlotConfigType.BY_AREA.equals(cmsSiteService.getCurrentSite().getTimeSlotConfigType())
					&& ((cartAddress == null || cartAddress.getArea() == null)))
					|| (TimeSlotConfigType.BY_DELIVERYMODE.equals(cmsSiteService.getCurrentSite().getTimeSlotConfigType())
							&& (cartData.getDeliveryMode() == null)))
			{
				return REDIRECT_PREFIX + "/checkout/multi/delivery-address/add";
			}

			timeSlotData = getTimeSlotData(cartData);

		}
		catch (final TimeSlotException e)
		{
			LOG.error(e.getMessage(), e);
			return next(redirectAttributes);
		}

		model.addAttribute("timeSlot", timeSlotData.get());
		return ControllerConstants.Views.Pages.MultiStepCheckout.ChooseTimeSlotPage;
	}



	private AddressData getCartAddress()
	{
		final Optional<AddressData> cartAddress = defaultAcceleratorCheckoutFacade.getCartAddress();

		return cartAddress.isPresent() ? cartAddress.get() : null;
	}


	private Optional<TimeSlotData> getTimeSlotData(final CartData cartData) throws TimeSlotException
	{
		final AddressData cartAddress = getCartAddress();
		/*
		 * if (timeSlotData.isEmpty()) { return next(redirectAttributes); }
		 */
		final boolean byArea = TimeSlotConfigType.BY_AREA.equals(cmsSiteService.getCurrentSite().getTimeSlotConfigType());
		return byArea ? timeSlotFacade.getTimeSlotDataByArea(cartAddress.getArea().getCode())
				: timeSlotFacade.getTimeSlotData(cartData.getDeliveryMode().getCode());
	}

	/**
	 * This method gets called when the "Use Selected Time Slot" button is clicked. It sets the selected time slot on the
	 * checkout facade and reloads the page highlighting the selected time slot.
	 *
	 * @return - a URL to the page to load.
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/select", method = RequestMethod.POST)
	@RequireHardLogIn
	public String doSelectTimeSlot(@Valid
	final TimeSlotForm timeSlotForm, final Model model, final RedirectAttributes redirectModel, final BindingResult bindingResult)
			throws CMSItemNotFoundException
	{
		String expressOrderErrorMsg = "";
		try
		{
			if (DeliveryModeType.EXPRESS.equals(getDefaultAcceleratorCheckoutFacade().getDeliveryModeType()))
			{
				getDefaultAcceleratorCheckoutFacade().isExpressOrder();
				return getCheckoutStep().nextStep();
			}
		}
		catch (final ExpressOrderException e1)
		{
			expressOrderErrorMsg = getExpressMsg(e1.getType());
		}

		timeSlotValidator.validate(timeSlotForm, bindingResult);

		populateCommonModelAttributes(model, getCheckoutFacade().getCheckoutCart(), timeSlotForm);

		if (bindingResult.hasErrors())
		{
			final AddressData cartAddress = getCartAddress();
			isExpressOrder(redirectModel);
			GlobalMessages.addErrorMessage(model, "checkout.error.timeslot.entry.invalid");
			final CartData cartData = getCheckoutFacade().getCheckoutCart();
			Optional<TimeSlotData> timeSlot = Optional.empty();
			try
			{
				if (TimeSlotConfigType.BY_AREA.equals(cmsSiteService.getCurrentSite().getTimeSlotConfigType()) && cartAddress != null
						&& cartAddress.getArea() != null)
				{
					timeSlot = timeSlotFacade.getTimeSlotDataByArea(cartAddress.getArea().getCode());
				}
				else
				{
					timeSlot = timeSlotFacade.getTimeSlotData(cartData.getDeliveryMode().getCode());
				}
			}
			catch (final TimeSlotException e)
			{
				LOG.error(e.getMessage(), e);
				return next(redirectModel);
			}
			if (timeSlot.isEmpty())
			{
				return next(redirectModel);
			}
			model.addAttribute("expressOrderErrorMsg", expressOrderErrorMsg);
			model.addAttribute("timeSlot", timeSlot.get());

			return ControllerConstants.Views.Pages.MultiStepCheckout.ChooseTimeSlotPage;
		}
		final TimeSlotInfoData timeSlotInfoData = convertForm(timeSlotForm);

		getCheckoutFacade().setTimeSlot(timeSlotInfoData);

		if (StringUtils.isNotEmpty(timeSlotForm.getNote()))
		{
			getCheckoutFacade().setDeliveryInstructions(timeSlotForm.getNote());
		}

		return getCheckoutStep().nextStep();
	}

	protected boolean isExpressOrder(final Model model)
	{

		boolean isExpressOrder = false;
		String expressOrderErrorMsg = null;

		try
		{
			isExpressOrder = getDefaultAcceleratorCheckoutFacade().isExpressOrder();
		}
		catch (final ExpressOrderException e)
		{


			expressOrderErrorMsg = getExpressMsg(e.getType());
			model.addAttribute("expressOrderErrorMsg", expressOrderErrorMsg);

			isExpressOrder = false;
		}
		//		model.addAttribute("isExpressOrder", isExpressOrder);
		return isExpressOrder;
	}

	/**
	 * @param type
	 */
	private String getExpressMsg(final ExpressOrderExceptionType type)
	{
		switch (type)
		{
			case CITY_NOT_EXPRESS:
				return getBaseStoreService().getCurrentBaseStore().getExpressDeliveryAreaNotExpress();

			case AREA_NOT_EXPRESS:
				return getBaseStoreService().getCurrentBaseStore().getExpressDeliveryAreaNotExpress();

			case PRODUCTS_NOT_EXPRESS:
				return "";

			case TIME_NOT_EXPRESS:
				return getBaseStoreService().getCurrentBaseStore().getExpressDeliveryInvalidTimeMessage();

			case WEIGHT_EXCEEDED:
				return getBaseStoreService().getCurrentBaseStore().getExpressDeliveryWeightLimitExceeded();

			case PRODUCTS_MIXED_EXPRESS:
				return getBaseStoreService().getCurrentBaseStore().getExpressDeliveryMixedItemExpress();

		}
		return null;
	}

	@ModelAttribute("isExpressOrderWithoutValidationOnTime")
	public boolean isExpressOrderWithoutValidationOnTime()
	{
		return getDefaultAcceleratorCheckoutFacade().isExpressOrderWithoutValidationOnTime();
	}

	@ModelAttribute("supportedOrderNotes")
	public Collection<OrderNoteData> getSupportedOrderNotes()
	{
		return getCheckoutFacade().getSupportedOrderNotes();
	}

	protected void populateCommonModelAttributes(final Model model, final CartData cartData, final TimeSlotForm timeSlotForm)
			throws CMSItemNotFoundException
	{
		model.addAttribute("cartData", cartData);
		model.addAttribute("timeSlotForm", timeSlotForm);
		model.addAttribute("deliveryAddresses", getDeliveryAddresses(getCartAddress()));

		prepareDataForPage(model);
		final ContentPageModel multiCheckoutSummaryPage = getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL);
		storeCmsPageInModel(model, multiCheckoutSummaryPage);
		setUpMetaDataForContentPage(model, multiCheckoutSummaryPage);
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.timeSlot.breadcrumb"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		setCheckoutStepLinksForModel(model, getCheckoutStep());
	}

	@RequestMapping(value = "/back", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}

	@RequestMapping(value = "/select-delivery", method = RequestMethod.POST)
	@RequireHardLogIn
	public String doSelectDeliveryType(final DeliveryModeForm storeCreditForm, final Model model,
			final RedirectAttributes redirectAttributes, final BindingResult bindingResult) throws CMSItemNotFoundException
	{
		populateCommonModelAttributes(model, getCheckoutFacade().getCheckoutCart(), new TimeSlotForm());

		model.addAttribute("selectDeliveryModeForm", storeCreditForm);

		if (storeCreditForm.getDeliveryModeCode() == null || storeCreditForm.getDeliveryModeCode().isBlank()
				|| storeCreditForm.getDeliveryModeCode().isEmpty())
		{

			//			GlobalMessages.addMessage(model, GlobalMessages.ERROR_MESSAGES_HOLDER, "checkout.missing.delivery.value.error", null);
			model.addAttribute("warningMsg",
					messageSource.getMessage("checkout.missing.delivery.value.error", null, i18nService.getCurrentLocale()));
			model.addAttribute("deliveryModeTypes", getCheckoutFacade().getDeliveryModeTypes());
			return ControllerConstants.Views.Pages.MultiStepCheckout.ChooseDeliveryModeTypePage;
		}
		if (DeliveryModeType.EXPRESS.getCode().equals(storeCreditForm.getDeliveryModeCode()))
		{
			getCheckoutFacade().setDeliveryModeType(DeliveryModeType.EXPRESS);
			//model.addAttribute("deliveryModeType", DeliveryModeType.EXPRESS.getCode());
			model.addAttribute("isExpressOrder", true);

		}

		if (DeliveryModeType.NORMAL.getCode().equals(storeCreditForm.getDeliveryModeCode()))
		{
			getCheckoutFacade().setDeliveryModeType(DeliveryModeType.NORMAL);
			//model.addAttribute("deliveryModeType", DeliveryModeType.NORMAL.getCode());
			model.addAttribute("isExpressOrder", false);
		}
		return displayTimeSlot(model, redirectAttributes);

	}

	@RequestMapping(value = "/select-delivery", method = RequestMethod.GET)
	@RequireHardLogIn
	public String selectDeliveryPage(final Model model, final RedirectAttributes redirectAttributes)
	{
		model.addAttribute("deliveryModeTypes", getCheckoutFacade().getDeliveryModeTypes());
		return ControllerConstants.Views.Pages.MultiStepCheckout.ChooseDeliveryModeTypePage;

	}

	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(TIME_SLOT);
	}

	private TimeSlotInfoData convertForm(final TimeSlotForm timeSlotForm)
	{
		final TimeSlotInfoData data = new TimeSlotInfoData();
		data.setPeriodCode(timeSlotForm.getPeriodCode());
		data.setStart(LocalTime.parse(timeSlotForm.getStart(), DateTimeFormatter.ofPattern("H:mm")));
		data.setEnd(LocalTime.parse(timeSlotForm.getEnd(), DateTimeFormatter.ofPattern("H:mm")));
		data.setDay(timeSlotForm.getDay());
		data.setDate(timeSlotForm.getDate());
		return data;
	}

	/**
	 * @return the timeSlotFacade
	 */
	public TimeSlotFacade getTimeSlotFacade()
	{
		return timeSlotFacade;
	}

	/**
	 * @param timeSlotFacade
	 *           the timeSlotFacade to set
	 */
	public void setTimeSlotFacade(final TimeSlotFacade timeSlotFacade)
	{
		this.timeSlotFacade = timeSlotFacade;
	}

	/**
	 * @return the timeSlotValidator
	 */
	public TimeSlotValidator getTimeSlotValidator()
	{
		return timeSlotValidator;
	}

	/**
	 * @param timeSlotValidator
	 *           the timeSlotValidator to set
	 */
	public void setTimeSlotValidator(final TimeSlotValidator timeSlotValidator)
	{
		this.timeSlotValidator = timeSlotValidator;
	}

	/**
	 * @return the defaultAcceleratorCheckoutFacade
	 */
	protected CustomAcceleratorCheckoutFacade getDefaultAcceleratorCheckoutFacade()
	{
		return defaultAcceleratorCheckoutFacade;
	}

	/**
	 * @return the cartService
	 */
	protected CartService getCartService()
	{
		return cartService;
	}

	/**
	 * @return the baseStoreService
	 */
	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}




}
