/**
 *
 */
package com.aldahra.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.PaginationData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.SortData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.aldahra.aldahrawishlist.exception.WishlistException;
import com.aldahra.aldahrawishlistfacade.data.WishlistData;
import com.aldahra.aldahrawishlistfacade.data.WishlistEntryData;
import com.aldahra.aldahrawishlistfacade.facade.WishlistFacade;


/**
 * @author yazeed-hammad
 *
 */
@Controller
@Scope("tenant")
@RequestMapping("/my-account/my-wishlist")
public class WishlistPageController extends AbstractSearchPageController
{
	private final Logger LOG = Logger.getLogger(WishlistPageController.class);
	private static final String MY_WISHLIST_CMS_PAGE = "/my-account/my-wishlist";

	@Resource(name = "wishlistFacade")
	public WishlistFacade wishlistFacade;

	@Resource(name = "cmsSiteService")
	public CMSSiteService cmsSiteService;

	private static final String ADD_ALL_ENTRIES_TO_CART_PATH_VARIABLE_PATTERN = "/addAllEntriesToCart";
	private static final String REMOVE_ALL_ENTRIES = "/removeall";
	private static final String PAGINATION_NUMBER_OF_RESULTS_COUNT = "pagination.number.results.count";
	private static final String PRODUCT_CODE_PATH_VARIABLE_PATTERN = "/{productCode:.*}";
	private static final String REDIRECT_WISHLIST_URL = REDIRECT_PREFIX + "/my-account/my-wishlist";
	private static final String SORT_BY_NAME_ASC = "byNameAsc";
	private static final String SORT_BY_NAME_DESC = "byNameDesc";

	/**
	 * @param model
	 * @param pageControllerConstants
	 * @return
	 * @throws CMSItemNotFoundException
	 */
	private String getView(final Model model, final String pageLabel) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(pageLabel));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(pageLabel));
		return getViewForPage(model);
	}

	@GetMapping
	@RequireHardLogIn
	public String getWishlist(@RequestParam(value = "page", defaultValue = "0", required = false)
	final int page, @RequestParam(value = "show", defaultValue = "Page", required = false)
	final ShowMode showMode, @RequestParam(value = "sort", defaultValue = "byNameAsc", required = false)
	final String sortCode, final Model model) throws CMSItemNotFoundException
	{
		WishlistData defaultWishListData = null;
		try
		{
			final Optional<WishlistData> defaultWishList = wishlistFacade.getDefaultWishList();
			defaultWishListData = defaultWishList.isPresent() ? defaultWishList.get() : null;

		}
		catch (final WishlistException e)
		{
			LOG.error(String.format("Error when fetching defaultWishList : %s", e.getMessage()));
			return getView(model, MY_WISHLIST_CMS_PAGE);
		}
		model.addAttribute("wishlistSelected", defaultWishListData);
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

		final PageableData pageableData = createPageableData(page, getItemsPerPage(), sortCode, showMode);
		final de.hybris.platform.cms2.data.PageableData obj = new de.hybris.platform.cms2.data.PageableData();
		obj.setSort(pageableData.getSort());
		obj.setPageSize(pageableData.getPageSize());
		obj.setCurrentPage(pageableData.getCurrentPage());

		List<WishlistEntryData> sortedWishlistEntries = null;
		int total = 0;
		try
		{
			total = wishlistFacade.getWishListEntriesCount(defaultWishListData.getPk(), obj);
		}
		catch (final WishlistException e)
		{
			// XXX Auto-generated catch block
			LOG.error(String.format("Error when fetching defaultWishList : %s", e.getMessage()));
		}

		if (total != 0)
		{
			try
			{
				final Optional<List<WishlistEntryData>> sortedWishlist = wishlistFacade.getWishListEntries(
						defaultWishListData.getPk(), obj, Arrays.asList(ProductOption.BASIC, ProductOption.PRICE, ProductOption.STOCK));
				sortedWishlistEntries = sortedWishlist.isPresent() ? sortedWishlist.get() : null;
			}
			catch (final WishlistException e)
			{
				LOG.error(String.format("Error when fetching defaultWishList : %s", e.getMessage()));
			}
		}



		if (sortedWishlistEntries != null && !sortedWishlistEntries.isEmpty())
		{

			final SearchPageData<WishlistEntryData> pagedWishlistEntries = new SearchPageData<WishlistEntryData>();
			pagedWishlistEntries.setResults(sortedWishlistEntries);
			pagedWishlistEntries.setPagination(createPagination(pageableData, total));
			final List<SortData> result = buildSorts(sortCode);

			pagedWishlistEntries.setSorts(result);
			populateModel(model, pagedWishlistEntries, showMode);
		}

		return getView(model, MY_WISHLIST_CMS_PAGE);

	}


	/**
	 * @return
	 */
	private int getItemsPerPage()
	{
		return cmsSiteService.getCurrentSite().getNumberOfWishlistItemsPerPage() != null
				? cmsSiteService.getCurrentSite().getNumberOfWishlistItemsPerPage().intValue()
				: 5;
	}

	protected <T> PaginationData createPagination(final PageableData pageableData, final int total)
	{
		final PaginationData paginationData = new PaginationData();
		paginationData.setPageSize(pageableData.getPageSize());
		paginationData.setSort(pageableData.getSort());
		paginationData.setTotalNumberOfResults(total);

		// Calculate the number of pages
		paginationData.setNumberOfPages(
				(int) Math.ceil(((double) paginationData.getTotalNumberOfResults()) / paginationData.getPageSize()));

		// Work out the current page, REDIRECT_WISHLIST_URLfixing any invalid page values
		paginationData.setCurrentPage(Math.max(0, Math.min(paginationData.getNumberOfPages(), pageableData.getCurrentPage())));

		return paginationData;
	}

	protected PaginationData createPageable(final int pageNumber, final int pageSize, final ShowMode showMode)
	{
		final PaginationData pageableData = new PaginationData();
		pageableData.setCurrentPage(pageNumber);

		if (ShowMode.All == showMode)
		{
			pageableData.setPageSize(MAX_PAGE_LIMIT);
		}
		else
		{
			pageableData.setPageSize(pageSize);
		}
		return pageableData;
	}

	protected List<SortData> buildSorts(final String sortCode)
	{
		final List<SortData> result = new ArrayList<>();
		final SortData sortDataByNameAsc = new SortData();
		sortDataByNameAsc.setCode(SORT_BY_NAME_ASC);
		//sortDataByNameAsc.setAsc(SORT_BY_NAME_ASC.equals(sortCode));

		final SortData sortDataByNameDesc = new SortData();
		sortDataByNameDesc.setCode(SORT_BY_NAME_DESC);
		//sortDataByNameDesc.setAsc(SORT_BY_NAME_DESC.equals(sortCode));

		result.add(sortDataByNameDesc);
		result.add(sortDataByNameAsc);
		return result;
	}

	@Override
	protected void populateModel(final Model model, final SearchPageData<?> searchPageData, final ShowMode showMode)
	{
		final int numberPagesShown = getSiteConfigService().getInt(PAGINATION_NUMBER_OF_RESULTS_COUNT, 5);

		model.addAttribute("numberPagesShown", Integer.valueOf(numberPagesShown));
		model.addAttribute("searchPageData", searchPageData);
		model.addAttribute("isShowAllAllowed", calculateShowAll(searchPageData, showMode));
		model.addAttribute("isShowPageAllowed", calculateShowPaged(searchPageData, showMode));
	}


	@Override
	protected Boolean calculateShowAll(final SearchPageData<?> searchPageData, final ShowMode showMode)
	{
		return Boolean.valueOf((showMode != ShowMode.All && //
				searchPageData.getPagination().getTotalNumberOfResults() > searchPageData.getPagination().getPageSize())
				&& isShowAllAllowed(searchPageData));
	}

	@Override
	protected Boolean calculateShowPaged(final SearchPageData<?> searchPageData, final ShowMode showMode)
	{
		return Boolean.valueOf(showMode == ShowMode.All && (searchPageData.getPagination().getNumberOfPages() > 1
				|| searchPageData.getPagination().getPageSize() == getMaxSearchPageSize()));
	}

	/**
	 * Special case, when total number of results > {@link #MAX_PAGE_LIMIT}
	 */
	@Override
	protected boolean isShowAllAllowed(final SearchPageData<?> searchPageData)
	{
		return searchPageData.getPagination().getNumberOfPages() > 1
				&& searchPageData.getPagination().getTotalNumberOfResults() < MAX_PAGE_LIMIT;
	}


	@PostMapping(value = "/remove/" + PRODUCT_CODE_PATH_VARIABLE_PATTERN)
	@RequireHardLogIn
	public String removeWishlistEntryByProductCode(@PathVariable("productCode")
	final String productCode, final HttpServletRequest request, final HttpServletResponse response, final Model model)
			throws IOException, WishlistException
	{
		wishlistFacade.removeWishlistEntryForProduct(productCode);

		return REDIRECT_WISHLIST_URL;
	}

	@PostMapping(value = "/add/" + PRODUCT_CODE_PATH_VARIABLE_PATTERN)
	@RequireHardLogIn
	public String addWishlistEntryByProductCode(@PathVariable("productCode")
	final String productCode, final HttpServletRequest request, final HttpServletResponse response, final Model model)
			throws IOException, WishlistException
	{
		wishlistFacade.addEntryToDefaultWishlistByProductCode(productCode, null, null, null);

		return REDIRECT_WISHLIST_URL;
	}

	@PostMapping(value = ADD_ALL_ENTRIES_TO_CART_PATH_VARIABLE_PATTERN)
	@RequireHardLogIn
	public String addAllEntriesToCart(final Model model) throws CMSItemNotFoundException, WishlistException
	{
		try
		{
			wishlistFacade.addAllDefaultWishlistEntryToCart();
			//			model.addAttribute("addResult", Boolean.TRUE);
		}
		catch (final CommerceCartModificationException e)
		{
			//			model.addAttribute("addResult", Boolean.FALSE);
		}

		return REDIRECT_WISHLIST_URL;
	}


	@PostMapping(value = REMOVE_ALL_ENTRIES)
	@RequireHardLogIn
	public String removeAllEntriesForDefaultWishlist(final Model model) throws CMSItemNotFoundException, WishlistException
	{
		wishlistFacade.removeAllEntriesForDefaultWishlist();

		return REDIRECT_WISHLIST_URL;
	}

}
