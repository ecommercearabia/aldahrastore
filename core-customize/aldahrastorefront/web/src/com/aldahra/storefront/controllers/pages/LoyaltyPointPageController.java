package com.aldahra.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.aldahra.core.loyaltypoint.data.LoyaltyPointHistoryData;
import com.aldahra.facades.facade.LoyaltyPointFacade;


/**
 * @author mnasro
 *
 */
@Controller
@Scope("tenant")
@RequestMapping(value = "/my-account/loyalty-point")
public class LoyaltyPointPageController extends AbstractPageController
{
	private static final String BREADCRUMBS_ATTR = "breadcrumbs";
	@Resource(name = "loyaltyPointFacade")
	private LoyaltyPointFacade loyaltyPointFacade;

	@Resource(name = "accountBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

	private static final String LOYALTY_POINT_VIEW = "/my-account/loyalty-point";

	@RequestMapping(method = RequestMethod.GET)
	@RequireHardLogIn
	public String getLoyaltyPointPage(final Model model, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{

		if (!getLoyaltyPointFacade().isLoyaltyPointsEnabledByCurrentStore())
		{
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "loyaltypoint.error.notAvailable");
			return REDIRECT_PREFIX + "/";
		}


		final List<LoyaltyPointHistoryData> loyaltyPointHistoryDatas = getLoyaltyPointFacade()
				.getLoyaltyPointHistoryByCurrentCustomer();
		model.addAttribute("loyaltyPointHistoryDatas", loyaltyPointHistoryDatas);


		final double loyaltyPoints = getLoyaltyPointFacade().getLoyaltyPointsByCurrentCustomer();

		model.addAttribute("loyaltyPoints", loyaltyPoints);
		storeCmsPageInModel(model, getContentPageForLabelOrId(LOYALTY_POINT_VIEW));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(LOYALTY_POINT_VIEW));
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs("text.account.loyaltypoint"));
		return getViewForPage(model);

	}

	/**
	 * @return the loyaltyPointFacade
	 */
	protected LoyaltyPointFacade getLoyaltyPointFacade()
	{
		return loyaltyPointFacade;
	}

	/**
	 * @return the accountBreadcrumbBuilder
	 */
	protected ResourceBreadcrumbBuilder getAccountBreadcrumbBuilder()
	{
		return accountBreadcrumbBuilder;
	}
}
