/**
 *
 */
package com.aldahra.storefront.controllers.pages.otp;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.user.data.OTPData;

import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.Model;

import com.aldahra.aldahraotp.model.OTPVerificationTokenModel;
import com.aldahra.storefront.form.validation.OTPLoginValidator;
import com.aldahra.storefront.form.validation.OTPValidator;


/**
 * @author mnasro
 *
 */
public abstract class AbstractLoginVerifyPage extends AbstractOTPVerifyPage
{
	@Resource(name = "otpLoginValidator")
	private OTPLoginValidator otpLoginValidator;


	/**
	 * @return the otpValidator
	 */
	@Override
	public OTPValidator getOTPValidator()
	{
		return otpLoginValidator;
	}

	public String generatOTPLoginToken(final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		if (!validateOTPEnabilityAndType() || getCmsSiteService().getCurrentSite() == null
				|| !getCmsSiteService().getCurrentSite().isActiveLoginWithOTP()
				|| getCmsSiteService().getCurrentSite().getDefaultMobileCountry() == null)
		{
			return getNotFoundPage(model, response);
		}

		final OTPData otpData = new OTPData();
		otpData.setMobileCountry(getCmsSiteService().getCurrentSite().getDefaultMobileCountry().getIsocode());
		otpData.setMobileNumber("");

		final Optional<OTPVerificationTokenModel> otpVerificationTokenModel = getOtpContextV2()
				.generateTokenCurrentCustomer(getOTPVerificationTokenType(), otpData, otpData.getMobileCountry());

		if (otpVerificationTokenModel.isEmpty())
		{
			return getNotFoundPage(model, response);
		}

		final String token = getEncodeToken(otpVerificationTokenModel.get().getToken());

		return REDIRECT_PREFIX + getChangeNumberActionURL() + "?token=" + token;

	}


}
