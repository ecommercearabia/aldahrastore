let lazyloadingAddtocart = true;

if (window.location.search) {
	var myString = window.location.search;
	var newlist = myString.split('=');
	if (newlist[0] == '?cart') {
		var items_org = newlist[1];
		var item_org = newlist[1].split('&');
		var item = []
		for (i = 0; i < item_org.length; i++) {
			if (item_org[i].length)
				item.push(item_org[i]);
		}
		var skusAsJSON = [];
		for (i = 0; i < item.length; i++) {

			var itemdata = item[i].split('--');
			var itemcode = itemdata[0];
			var itemtotel = itemdata[1];
			skusAsJSON.push({"product": {"code": itemcode}, "quantity": itemtotel});
		}
		var datanew = JSON.stringify({"cartEntries": skusAsJSON});
		if (item.length > 1) {
			$('.cont_item').css("display", "none")
		} else {

			$('.cont_items').css("display", "none")
		}

		$('.confirm_log').fadeIn("300", function () {
			$('.confirm_log').css("display", "block");
		});
		$('.darkback_confirm').fadeIn("300", function () {
			$('.darkback_confirm').css("display", "block");
		});

		$('.cen_confirm').click(function () {
			$('.confirm_log').fadeOut("300", function () {
				$('.confirm_log').css("display", "none");
			});
			$('.darkback_confirm').fadeOut("300", function () {
				$('.darkback_confirm').css("display", "none");
			});
		});


		$('.comp_confirm').click(function () {
			$.ajax({
				url: $('#addcarturlid').val(),
				type: 'POST',
				dataType: 'json',
				contentType: 'application/json',
				data: datanew,
				async: false,
				success: function (response) {
					//ACC.product.displayAddToCartPopup(response);
					ACC.minicart.updateMiniCartDisplay();
				},
				error: function (jqXHR, textStatus, errorThrown) {
					// log the error to the console
				}
			});

			$('.confirm_log').fadeOut("300", function () {
				$('.confirm_log').css("display", "none");
			});
			$('.darkback_confirm').fadeOut("300", function () {
				$('.darkback_confirm').css("display", "none");
			});
		});


	}
}

if (window.location.href.indexOf("register") > -1) {

	if (window.location.search) {
		var myString = window.location.search;
		var newlist = myString.split('=');

		var newlistdata = newlist[1].split('&');

		$('#register\\.referralCode').val(newlistdata[0])


	}
}

$("#change-country option, #mcountry option, #addresscountry option").each(function () {
	$(this).addClass("option-with-flag");
	$(this).attr("data-content", "<span class='inline-flag flag " + $(this).val().toLowerCase() + "'></span><span class='text'>" + $(this).html() + "</span>");
});
$('#change-country').selectpicker('refresh');
$('#mcountry').selectpicker('refresh');
$('#addresscountry').selectpicker('refresh');
$('select').selectpicker();

$('#date').datepicker({
	language: 'en',
	dateFormat: 'dd/mm/yyyy'
});


var ias = $.ias({
	container: ".product__listing.product__grid",
	item: ".product-item",
	delay: 500,
	pagination: ".pagination-bar.top .pagination",
	next: ".pagination-bar.top .pagination-next a"
});
ias.extension(new IASSpinnerExtension());
ias.extension(new IASTriggerExtension({
	text: 'Load More',
	offset: '10',
	html: '<div class="ias-trigger ias-trigger-next" ><a class="btn btn-primary btn-block">{text}</a></div>'// override text when no pages left
})); // shows a trigger after page 3
ias.extension(new IASNoneLeftExtension({
	text: '',
}));
$('.js-update-entry-quantity-input').attr('autocomplete', 'off');
ias.on('rendered', function () {


	ACC.product.bindToAddToCartForm();
	ACC.product.bindToAddToCartStorePickUpForm();
	ACC.product.enableStorePickupButton();
	ACC.product.enableAddToCartButton();
	lazyloadingAddtocart = false;
	$('.addproductitem').on('submit', function (e) {

		e.preventDefault();
		var formelement = $(this)
		var url = $(this).attr('action');
		console.log('url --->', url)
		console.log('data --->', formelement.serialize())
		$.ajax({
			url: url,
			type: 'POST',


			data: formelement.serialize(),
			async: false,
			success: function (data) {

				ACC.product.displayAddToCartPopup(data, formelement)
			}
		});

	});
	$(".js-enable-btn").click(function () {


		var select_this_cart = $(this).parents(".actions")

		setTimeout(function () {
			var html_content_add_to_car_box = $("#addedtocartno").find('.cart_popup_error_msg').text();
			//console.log(html_content_add_to_car_box.length)
			if (html_content_add_to_car_box.length === 0 || html_content_add_to_car_box.length == 7) {

				var add_to_cart = select_this_cart.find('.addedtocart')

				$(add_to_cart).fadeIn("300", function () {
					$(add_to_cart).css("display", "block");
				});
				setTimeout(() => $(add_to_cart).fadeOut("slow"), 2000);
			} else if (html_content_add_to_car_box.length === 49 || html_content_add_to_car_box.length == 34) {
				var add_to_cart = select_this_cart.find(".addedtocarterror");
				$(add_to_cart).fadeIn("300", function () {
					$(add_to_cart).css("display", "block");
				});
				setTimeout(() => $(add_to_cart).fadeOut("slow"), 3000);

			} else if (html_content_add_to_car_box.length === 86 || html_content_add_to_car_box.length == 70) {
				var add_to_cart = select_this_cart.find(".addedtocartinfo");
				$(add_to_cart).fadeIn("300", function () {
					$(add_to_cart).css("display", "block");
				});
				setTimeout(() => $(add_to_cart).fadeOut("slow"), 5000);

			}

		}, 250);


	});
//	if($('.stock-0.selected').length){
//		$('.stock-0.selected').each(function() {
//				var list = $(this).parents('.list');
//			$(list).find('.size_change').each(function(index) {
//
//				if($(this).data('stock') > 0){
//					$(this).trigger('click');
//					return false;
//
//
//				}
//
//			});
//
//
//		});
//		}

});
ias.on('load', function (event) {
	TweenMax.staggerTo('.slice', 1, {
		opacity: 1,
		repeat: 1000
	}, 0.1);
});
$('.link_ar').click(function () {
	$('.lang-selector').val('ar');
	$(".lang-form").submit();

})
$('.link_en').click(function () {
	$('.lang-selector').val('en');
	$(".lang-form").submit();


})

//popup form_login in home_page
$(".registration .login_action").click(function () {
	$(".darkback").fadeIn("300", function () {
		$('.darkback').removeClass("hidden");
	});
	$(".login_box").fadeIn("300", function () {
		$('.login_box').removeClass("hidden");

	});

	$('.registration').addClass("click_border");

});

$('.darkback').click(function () {
	$(".login_box").fadeOut("slow").fadeOut("slow", function () {
		$('.login_box').addClass("hidden");
		$('.darkback').addClass("hidden");

	});
	$('.registration').removeClass("click_border");
});
//popup personal_info in home_page in sm and xs
$(".navigation--bottom .personal_info").click(function () {
	// $(".navigation--bottom .NAVcompONENT").toggle();


});


// popup personal_info in home_pageNAVcompONENT
$(".navigation--top .personal_info").click(function () {
//	$(".darkback").fadeIn("300", function () {$('.darkback').removeClass("hidden");});
//	$(".navigation--top .NAVcompONENT").fadeIn("300", function () {
//		$('.navigation--top .NAVcompONENT').removeClass("hidden");
//
//	});


	var href = $("a.mydas_b").attr('href');
	window.location.href = href;


});

$('.darkback').click(function () {
////	$(".NAVcompONENT").fadeOut("slow").fadeOut("slow", function () {
////		$('.NAVcompONENT').addClass("hidden");
////		$('.darkback').addClass("hidden");
//
//	});

});

$(".mobile-action .title").click(function () {
	$(".mobile-action .footer__nav--links").removeClass('activefooter');
	$('.mobile-action').find(".title span").removeClass('fa-minus').addClass('fa-plus')
	$(this).parent('.footer__nav--container').find('.footer__nav--links').toggleClass('activefooter');
	$(this).parent('.footer__nav--container').find('.title span').removeClass('fa-plus').addClass('fa-minus')
});


//set palceholder  in register page


/*let label_fisrt_name= $('.first-name label').text().trim();
$('[id*="register.firstName"]').attr("placeholder", label_fisrt_name);

let label_last_name= $('.last-name label').text().trim();
$('[id*="register.lastName"]').attr("placeholder", label_last_name);

let label_mobile_name= $('.mobile_number label').text().trim();
$('[id*="register.mobileNumber"]').attr("placeholder", label_mobile_name);

let label_email_name= $('.email label').text().trim();
$('[id*="register.email"]').attr("placeholder", label_email_name);
*/
let label_pass = $('.pass label').text().trim();
$('[id*="password"]').attr("placeholder", label_pass);

let label_pass_con = $('.pass_con label').text().trim();
$('[id*="register.checkPwd"]').attr("placeholder", label_pass_con);


// set placeholder in login page
/*let label_email_login=$('.email_login label').text().trim();

$('[id*="j_username').attr("placeholder", label_email_login);*/


//set palceholder password in login_form in home_page
let label = $('.password_login .control-label ').text().trim();
if (!label) {
	label = $('.password-holder .control-label').text().trim();
}
$('[id*="j_password"]').attr("placeholder", label);


/*let label_email_loginpop=$('.user_name_popup label').text().trim();


$('[id*="j_username').attr("placeholder", label_email_loginpop);

*/


$(".js-offcanvas-links").sticky({topSpacing: 0});
$('.js-offcanvas-links').on('sticky-start', function () {
	$(".sticky-wrapper").css('opacity', '.5');
	$(".sticky-wrapper").animate({
		opacity: 1
	})

});
$('.js-offcanvas-links').on('sticky-end', function () {

	$(".sticky-wrapper").css('opacity', '.5');
	$(".sticky-wrapper").animate({
		opacity: 1
	})

});
//$('.js-offcanvas-links').on('sticky-update', function() { console.log("Update"); });
//$('.js-offcanvas-links').on('sticky-bottom-reached', function() { console.log("Bottom reached"); });
//$('.js-offcanvas-links').on('sticky-bottom-unreached', function() { console.log("Bottom unreached"); });


if ($('.smartEditComponent').length) {

	$('.sticky-wrapper').addClass('disable_sticky')
}

$("#fade-example .js-tabs-link").aniTabs();


//add checked in radio in slot time
$("input[name='selector_selector']").click(function () {
	var data_code = $("input[name='selector_selector']:checked").data('code');
	var data_start = $("input[name='selector_selector']:checked").data('start');
	var data_end = $("input[name='selector_selector']:checked").data('end');
	var data_day = $("input[name='selector_selector']:checked").data('day');
	var data_date = $("input[name='selector_selector']:checked").data('date');
	$("#periodCode").attr("value", data_code)
	$("#start").attr("value", data_start)
	$("#end").attr("value", data_end);
	$("#day").attr("value", data_day);
	$("#date").attr("value", data_date);

});

//qty in cartpage

$('.plus').click(function () {

	var div_item = $(this).closest('.item__list--item');
	var total_data = $(this).parent().parent().parent().parent();


	var form = $(this).closest('form');
	form.find('.minus').removeClass('not_allow_a');
	var className = '.' + $(form).attr('class');
	var max_qty = form.find('input[name= quantity ]').attr('max');

	var productCode = form.find('input[name=quantity]').val();
	var initialCartQuantity = form.find('input[name=initialQuantity]').val();

	var newCartQuantity = parseFloat(initialCartQuantity) + 1;


	if (max_qty >= newCartQuantity) {

		form.find('input[name= quantity ]').val(newCartQuantity);


		//form.find('input[name= quantity ]').val(newCartQuantity);
		var cal_total = div_item.find('.item__price').html().trim().replace(/[^0-9.]/g, "");

		var totalVal = parseFloat(cal_total) * newCartQuantity
		div_item.find('.js-item-total').html('AED ' + totalVal.toFixed(2))
		form.find('input[name=initialQuantity]').val(newCartQuantity);
		if (newCartQuantity == max_qty) {

			form.find('.plus').addClass('not_allow_a');

		}


		var url = form.attr('action');
		//console.log(form.serialize())
		$.ajax({
			url: url,
			type: 'POST',
			data: form.serialize(),
			async: true,
			success: function (data) {
				var total_data = $(data).find('.js-cart-totals').html();
				

				$('.js-cart-totals').html(total_data);
				if($('.alert-dismissable.getAccAlert').length){
					
					var head_data = $(data).find('.cart-header.border').html();
					var rowpad_t = $(data).find('.cart__actions').html();
					$('.cart-header.border').html(head_data);
					$('.cart__actions').html(rowpad_t);
					ACC.checkout.bindCheckO();
				}
				

				ACC.minicart.updateMiniCartDisplay();


			}


		});
	}

});
$('.minus').click(function () {
	var div_item = $(this).closest('.item__list--item');
	var total_data = $(this).parent().parent().parent().parent();
	var form = $(this).closest('form');
	var className = '.' + $(form).attr('class');
	form.find('.plus').removeClass('not_allow_a');
	var min_qty = 1;
	var productCode = form.find('input[name=quantity]').val();
	var initialCartQuantity = form.find('input[name=initialQuantity]').val();

	var newCartQuantity = parseFloat(initialCartQuantity) - 1;

	if (newCartQuantity >= 1) {

		form.find('input[name= quantity ]').val(newCartQuantity);
		var cal_total = div_item.find('.item__price').html().trim().replace(/[^0-9.]/g, "");

		var totalVal = parseFloat(cal_total) * newCartQuantity
		div_item.find('.js-item-total').html('AED ' + totalVal.toFixed(2))

		var url = form.attr('action');

		if (newCartQuantity == min_qty) {

			form.find('.minus').addClass('not_allow_a');

		}
		form.find('input[name=initialQuantity]').val(newCartQuantity)
		//console.log(form.serialize())
		$.ajax({
			url: url,
			type: 'POST',
			data: form.serialize(),
			async: true,
			success: function (data) {
				var total_data = $(data).find('.js-cart-totals').html();
				// var qty= form.find('input[name=initialQuantity]').val();
				//var total_item_temp= $(data).find(className).parent().parent();
				//var total_item= total_item_temp.find('.js-item-total').html()
				// div_item.find('.js-item-total').html(total_item)
				$('.js-cart-totals').html(total_data);
				if($('.alert-dismissable.getAccAlert').length){
					var head_data = $(data).find('.cart-header.border').html();
					var rowpad_t = $(data).find('.cart__actions').html();
					$('.cart-header.border').html(head_data);
					$('.cart__actions').html(rowpad_t);
					ACC.checkout.bindCheckO();

				}
				ACC.minicart.updateMiniCartDisplay();


				//  ACC.product.displayAddToCartPopup(data,formelement)
			}


		});

	}
//    	var form = $(this).closest('form');
//    	var productCode = form.find('input[name=productCode]').val();
//    	var initialCartQuantity = form.find('input[name=initialQuantity]').val();
//    	var newCartQuantity = parseFloat(initialCartQuantity) - 1;
//    	form.find('input[name=quantity]').val(newCartQuantity);
//
//    	  form.submit();
});

var myset_time_out;
$(".js-update-entry-quantity-input").on("keyup", function () {
	clearTimeout(myset_time_out);
	var w_check_value = true;
	var min_qty = 1;
	var div_item = $(this).closest('.item__list--item');

	var total_data = $(this).parent().parent().parent().parent();

	var final_value = parseInt($(this).val());
	var form = $(this).closest('form');

	var className = '.' + $(form).attr('class');
	var max_qty = form.find('input[name= quantity ]').attr('max');
	var value = $(this).val();
	if (value == '0') {
		$(this).val(1);
		final_value = 1
	}
	var input = /^\d*$/.test(value)
	if (!value.length) {
		var input = this;
		w_check_value = false;

		myset_time_out = setTimeout(function () {
			$(input).val(1);
			var cal_total = div_item.find('.item__price').html().trim().replace(/[^0-9.]/g, "");

			var totalVal = parseFloat(cal_total) * 1
			div_item.find('.js-item-total').html('AED ' + totalVal.toFixed(2))
			form.find('input[name=initialQuantity]').val(1);
			var url = form.attr('action');
			if (final_value - 1 <= min_qty) {

				form.find('.minus').addClass('not_allow_a');

			}
			//  form.find('input[name=initialQuantity]').val(1)
			$.ajax({
				url: url,
				type: 'POST',
				data: form.serialize(),
				async: true,
				success: function (data) {
					var total_data = $(data).find('.js-cart-totals').html();
					//var qty= 1;
					//var total_item_temp= $(data).find(className).parent().parent();
					//var total_item= total_item_temp.find('.js-item-total').html()
					//div_item.find('.js-item-total').html(total_item)
					$('.js-cart-totals').html(total_data);
					if($('.alert-dismissable.getAccAlert').length){
					
						var head_data = $(data).find('.cart-header.border').html();
					var rowpad_t = $(data).find('.cart__actions').html();
					$('.cart-header.border').html(head_data);
					$('.cart__actions').html(rowpad_t);
					ACC.checkout.bindCheckO();
					}
					ACC.minicart.updateMiniCartDisplay();


					//  ACC.product.displayAddToCartPopup(data,formelement)
				}


			});


		}, 2000);
	}
	if (value.length && !input) {
		$(this).val(1);
		final_value = 1;
	}

	var value_qty = parseInt(value)
	if (value_qty > parseInt(max_qty)) {
		$(this).val(parseInt(max_qty));
		final_value = parseInt(max_qty)
	}
	if (w_check_value) {
		//  console.log(final_value)


		$(input).val(final_value);
		var cal_total = div_item.find('.item__price').html().trim().replace(/[^0-9.]/g, "");

		var totalVal = parseFloat(cal_total) * final_value
		div_item.find('.js-item-total').html('AED ' + totalVal.toFixed(2))
		form.find('input[name=initialQuantity]').val(final_value);
		if (final_value - 1 <= min_qty) {

			form.find('.minus').addClass('not_allow_a');

		} else {

			form.find('.minus').removeClass('not_allow_a');
		}
		if (final_value + 1 >= parseInt(max_qty)) {

			form.find('.plus').addClass('not_allow_a');

		} else {

			form.find('.plus').removeClass('not_allow_a');
		}
		var url = form.attr('action');
		//console.log(form.serialize())
		form.find('input[name=initialQuantity]').val(final_value)
		$.ajax({
			url: url,
			type: 'POST',
			data: form.serialize(),
			async: false,
			success: function (data) {
				var total_data = $(data).find('.js-cart-totals').html();
				//var qty= final_value;
				//var total_item_temp= $(data).find(className).parent().parent();
				//var total_item= total_item_temp.find('.js-item-total').html()
				//div_item.find('.js-item-total').html(total_item)
				$('.js-cart-totals').html(total_data);
				if($('.alert-dismissable.getAccAlert').length){
					
					var head_data = $(data).find('.cart-header.border').html();
					var rowpad_t = $(data).find('.cart__actions').html();
					$('.cart-header.border').html(head_data);
					$('.cart__actions').html(rowpad_t);
					ACC.checkout.bindCheckO();

				}
				ACC.minicart.updateMiniCartDisplay();
				//form.find('input[name=initialQuantity]').val(parseInt(qty)-1)

				//  ACC.product.displayAddToCartPopup(data,formelement)
			}


		});


	}


});



$('.nav__link--drill__down').click(function () {

	$(".navigation__overflow").animate({scrollTop: 0});
});


$("input[name='storeCredit']").click(function () {
	var checked_item = $("input[name='storeCredit']:checked").attr('id');
	$("#sctCode").val(checked_item)
	if (checked_item == "REDEEM_SPECIFIC_AMOUNT") {
		$(".storeCreditAmount").removeClass("hidden");

	} else {

		$(".storeCreditAmount").addClass("hidden");

	}

});


$('.facet-nav select').on('change', function () {
	var val = $(this).children("option:selected").val();

	var cla = $(this).attr('class');
	var formul = $("." + cla + " input[value='" + val + "']").parents('form');
	if (formul.length)
		formul.submit();
	else {
		var val_a = $("." + cla + " option[value='" + val + "']").html();

		var listItems_a = $("." + cla).find(" li");

		for (let li_a of listItems_a) {
			let product_a_text = $(li_a).find('.facet__text a').text();

			let product_a = $(li_a).find('.facet__text a')
			if (product_a_text == val_a) {
				$(product_a).get(0).click();

			}
		}

	}
});


//facet-CountryOfOrigin
var listItems = $(".multi_sel li");
for (let li of listItems) {
	let product = $(li).attr('id');

	var item_prod = $('.facet-CountryOfOrigin .dropdown-menu li');
	for (let item of item_prod) {
		var text = $(item).find('.text').html();
		if (text == product) {
			$(item).find('a').addClass("selected");


		}

	}

}

//facet-price
var listItems = $(".multi_sel li");
for (let li of listItems) {
	let product = $(li).attr('id');

	var item_prod = $('.facet-price .dropdown-menu li');
	for (let item of item_prod) {
		var text = $(item).find('.text').html();
		if (text == product) {
			$(item).find('a').addClass("selected");


		}

	}

}


//facet-ProductLabel
var listItems = $(".multi_sel li");
for (let li of listItems) {
	let product = $(li).attr('id');

	var item_prod = $('.facet-ProductLabel .dropdown-menu li');
	for (let item of item_prod) {
		var text = $(item).find('.text').html();

		if (text == product) {
			$(item).find('a').addClass("selected");


		}

	}

}
//facet-category
var listItems = $(".multi_sel li");
for (let li of listItems) {
	let product = $(li).attr('id');

	var item_prod = $('.facet-category .dropdown-menu li');
	for (let item of item_prod) {
		var text = $(item).find('.text').html();

		if (text == product) {
			$(item).find('a').addClass("selected");


		}

	}

}

//facet-Organic
var listItems = $(".multi_sel li");
for (let li of listItems) {
	let product = $(li).attr('id');

	var item_prod = $('.facet-Organic .dropdown-menu li');
	for (let item of item_prod) {
		var text = $(item).find('.text').html();

		if (text == product) {
			$(item).find('a').addClass("selected");


		}

	}

}


//input sort toggle in mobile
$(".click_sort").click(function () {

	$(".sort_responsive_input").toggle();


})

//payment method on load
var payment_ul = $('#selectPaymentMethodForm ul li');
for (let li of payment_ul) {
	var nameClass = $(li).hasClass("selected")
	if (nameClass) {
		var input_pay = $("input[name='paymentmothed']").parent('label');
		for (let label_pay of input_pay) {
			if ($(label_pay).text().trim() == $(li).find('.text').text().trim()) {
				$(label_pay).find('input').attr('checked', true)


			}

		}
	}

}
//payment method in change value
$("input[name='paymentmothed']").click(function () {


	$("#selectPaymentMethodForm select.form-control").val($(this).val());


});

//social icon popup
$(".social_popup").click(function () {
	$(".darkback").fadeIn("300", function () {
		$('.darkback').removeClass("hidden");
	});
	$(".popup_social").fadeIn("300", function () {
		$('.popup_social').removeClass("hidden");

	});


});

$('.darkback').click(function () {
	$(".popup_social").fadeOut("slow").fadeOut("slow", function () {
		$('.popup_social').addClass("hidden");
		$('.darkback').addClass("hidden");

	});

});


if ($(".global-alerts").length != 0) {
	setTimeout(() => $('.global-alerts .alert').fadeOut('slow'), 10000);

}


//$(".js-enable-btn").click(function(){
//	var select_this_cart=$(this).parents(".actions")
//
//	setTimeout(function(){
//		 var html_content_add_to_car_box=$("#addedtocartno").find('.cart_popup_error_msg').text();
//	 //console.log(html_content_add_to_car_box.length)
//	 	 if(html_content_add_to_car_box.length === 0 || html_content_add_to_car_box.length === 7){
//
//	 		var add_to_cart= select_this_cart.find('.addedtocart')
//
//		 	$(add_to_cart).fadeIn("300", function () {$(add_to_cart).css("display","block");});
//		      setTimeout(() =>$(add_to_cart).fadeOut("slow"), 2000);
//	 	 }else if (html_content_add_to_car_box.length ===49 || html_content_add_to_car_box.length === 34)
//	 		 {
//	 		var add_to_cart= select_this_cart.find(".addedtocarterror");
//		 	$(add_to_cart).fadeIn("300", function () {$(add_to_cart).css("display","block");});
//		      setTimeout(() =>$(add_to_cart).fadeOut("slow"), 3000);
//
//	 		 } else if (html_content_add_to_car_box.length ===86 || html_content_add_to_car_box.length === 70){
//	 			 var add_to_cart= select_this_cart.find(".addedtocartinfo");
//			 	$(add_to_cart).fadeIn("300", function () {$(add_to_cart).css("display","block");});
//			      setTimeout(() =>$(add_to_cart).fadeOut("slow"), 5000);
//
//	 		 }
//
//	}, 250);
//
//
//});

$(".btn_pdp_in").click(function () {
	var select_this_cart = $(this).parents(".addcart_pdp")

	setTimeout(function () {
		var html_content_add_to_car_box = $("#addedtocartno").find('.cart_popup_error_msg').text();
		//console.log(html_content_add_to_car_box.length)
		if (html_content_add_to_car_box.length === 0 || html_content_add_to_car_box.length === 7) {

			var add_to_cart = select_this_cart.find('.addedtocart')

			$(add_to_cart).fadeIn("300", function () {
				$(add_to_cart).css("display", "block");
			});
			setTimeout(() => $(add_to_cart).fadeOut("slow"), 2000);
		} else if (html_content_add_to_car_box.length === 49 || html_content_add_to_car_box.length === 34) {
			var add_to_cart = select_this_cart.find(".addedtocarterror");
			$(add_to_cart).fadeIn("300", function () {
				$(add_to_cart).css("display", "block");
			});
			setTimeout(() => $(add_to_cart).fadeOut("slow"), 3000);

		} else if (html_content_add_to_car_box.length === 86 || html_content_add_to_car_box.length === 70) {
			var add_to_cart = select_this_cart.find(".addedtocartinfo");
			$(add_to_cart).fadeIn("300", function () {
				$(add_to_cart).css("display", "block");
			});
			setTimeout(() => $(add_to_cart).fadeOut("slow"), 5000);

		}

	}, 250);


});

$(".nav-cart-js").click(function () {

	$(".darkback").fadeIn("300", function () {
		$('.darkback').removeClass("hidden");
	});
	$('.cart_header_component').removeClass("hidden")
	$('.cart_header_component').slideDown(350, function () {


	});

});
$('.darkback').add('.close_cart_component').click(function () {
	$('.darkback').addClass("hidden");

	$('.cart_header_component').slideUp(350, function () {

		$('.cart_header_component').addClass("hidden")
	});

});
$(function () {
	var cart_nav = $(".cart_header_component");
	$(window).scroll(function () {
		var scroll = $(window).scrollTop();

		if (scroll >= 100) {
			cart_nav.addClass("cart_header_component_sti");
		} else {
			cart_nav.removeClass("cart_header_component_sti")
		}
	});
});


$('.js-cartItemDetailBtn').click(function () {
	$(this).parents('.js-cartItemDetailGroup').find('.js-execute-entry-action-button').click();


})
$("#mobileCountry option, #acountry option").each(function () {
	$(this).addClass("option-with-flag");
	$(this).attr("data-content", "<span class='inline-flag flag " + $(this).val().toLowerCase() + "'></span><span class='text'>" + $(this).html() + "</span>");
});

$('#mobileCountry').selectpicker('refresh');
$('#acountry').selectpicker('refresh');
//map_checkout_


$('body').on('click', function (e) {

	$('.bootstrap-select').removeClass('open');

});
//data-newfrom="newfrom"
$('#addresssavedAddresses').change(function () {

	$('.newformaddress').addClass("hidden");
	var addressTitle = $(this).find("option:selected").data("title");
var lastname= $(this).find("option:selected").data("lastname");
var firstname= $(this).find("option:selected").data("firstname");
var line1= $(this).find("option:selected").data("line1");
var buildingname= $(this).find("option:selected").data("buildingname");
var countryname= $(this).find("option:selected").data("countryname");
var city=$(this).find("option:selected").data("testcity");
var apartmentnumber=$(this).find("option:selected").data("apartmentnumber");
var mobileNumber=$(this).find("option:selected").data("testmobile");
var citynames=$(this).find("option:selected").data("cityname");
$('.default_Add').html(addressTitle.bold()+" "+firstname.bold()+" "+lastname.bold()+" "+'<br />'+ line1+'<br />'+buildingname+'<br />'+apartmentnumber+'<br />'+countryname+'<br />'+citynames+'<br />'+city+'<br />'+mobileNumber);

})
$('#AddNewAdd').click(function () {
	$("#addresssavedAddresses option").attr("selected", false);
	$("#addresssavedAddresses option[data-newfrom='newfrom']").attr('selected', 'selected');
	$('#addresssavedAddresses').selectpicker('refresh');

	$('.newformaddress').removeClass("hidden");

});

function fun_AllowOnlyAmountAndDot(txt) {
	if (event.keyCode > 47 && event.keyCode < 58 || event.keyCode == 46) {
		var txtbx = document.getElementById(txt);
		var amount = document.getElementById(txt).value;
		var present = 0;
		var count = 0;

		if (amount.indexOf(".", present) || amount.indexOf(".", present + 1)) ;
		{

		}


		do {
			present = amount.indexOf(".", present);
			if (present != -1) {
				count++;
				present++;
			}
		}
		while (present != -1);
		if (present == -1 && amount.length == 0 && event.keyCode == 46) {
			event.keyCode = 0;

			return false;
		}

		if (count >= 1 && event.keyCode == 46) {

			event.keyCode = 0;

			return false;
		}
		if (count == 1) {
			var lastdigits = amount.substring(amount.indexOf(".") + 1, amount.length);
			if (lastdigits.length >= 500) {

				event.keyCode = 0;
				return false;
			}
		}
		return true;
	} else {
		event.keyCode = 0;

		return false;
	}

}

if (/Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent)) {
	$('select').selectpicker('mobile');
}
var checkarea;
$(document).on("change", '#cityId', function () {
	$('#us2-address').val('');
	checkarea = false;
	var val_city_change = $('#cityId').val();
	area_zoom = false;
	city_zoom = true;
	$.ajax({
		url: "https://maps.googleapis.com/maps/api/geocode/json?address=" + val_city_change + "&key=AIzaSyBDnqcqkmG9MqNZR6QKsB8V-mg-NNa71t8",

		success: function (data) {


		}
	});

});


$(document).on("change", '#areaId', function () {
	checkarea = true;
	var val_area_change = $('#areaId option:selected').text();
	area_zoom = true;
	city_zoom = false;
	$.ajax({
		url: "https://maps.googleapis.com/maps/api/geocode/json?address=" + val_area_change + '%20' + $('#cityId').val() + "&key=AIzaSyBDnqcqkmG9MqNZR6QKsB8V-mg-NNa71t8",

		success: function (data) {


		}
	});


});


if ($('.AllowAutoComplete').length) {
	var AllowAutoComplete = $('.AllowAutoComplete').val().trim()

	if (AllowAutoComplete == "true") {

		let placeSearch;
		let autocomplete;
		const componentForm = {
			street_number: "short_name",
			route: "long_name",
			locality: "long_name",
			administrative_area_level_1: "short_name",
			country: "long_name",
			postal_code: "short_name",
		};

		function initAutocomplete() {
			// Create the autocomplete object, restricting the search predictions to
			// geographical location types.
			autocomplete = new google.maps.places.Autocomplete(
				document.getElementById("streetName"),
				{
					types: [],
					componentRestrictions: {country: 'AE'}
				}
			);
			// Avoid paying for data that you don't need by restricting the set of
			// place fields that are returned to just the address components.
			autocomplete.setFields(["address_component"]);
			// When the user selects an address from the drop-down, populate the
			// address fields in the form.
			//autocomplete.addListener("place_changed", fillInAddress);
		}

		function fillInAddress() {
			// Get the place details from the autocomplete object.
			const place = autocomplete.getPlace();

			for (const component in componentForm) {
				document.getElementById(component).value = "";
				document.getElementById(component).disabled = false;
			}

			// Get each component of the address from the place details,
			// and then fill-in the corresponding field on the form.
			for (const component of place.address_components) {
				const addressType = component.types[0];

				if (componentForm[addressType]) {
					const val = component[componentForm[addressType]];
					document.getElementById(addressType).value = val;
				}
			}
		}


	}
}
if ($('.promo_endDate').length) {
	if ($('.language-ar').length) {
		document.addEventListener('DOMContentLoaded', () => {

			// Unix timestamp (in seconds) to count down to
			var getDate = $('.promo_endDate').val();


			var d = new Date(getDate);
			var EndTime = new Date(d).getTime() / 1000;

			// Set up FlipDown
			var flipdown = new FlipDown(EndTime, {
				headings: ["ايام", "ساعات", "دقائق", "ثواني"]
			})
				// Start the countdown
				.start()

				// Do something when the countdown ends
				.ifEnded(() => {
					$('.promo_endDate').css("display", "none")
				});


		});
	} else {

		document.addEventListener('DOMContentLoaded', () => {

			// Unix timestamp (in seconds) to count down to
			var getDate = $('.promo_endDate').val();


			var d = new Date(getDate);
			var EndTime = new Date(d).getTime() / 1000;

			// Set up FlipDown
			var flipdown = new FlipDown(EndTime)
				// Start the countdown
				.start()

				// Do something when the countdown ends
				.ifEnded(() => {
					$('.promo_endDateCont').css("display", "none")
				});


		});


	}
}


//					<iframe id="ytplayer" type="text/html" width="720" height="405"
//						src="https://www.youtube.com/embed/M7lc1UVf-VE?autoplay=1&loop=1&color=white"
//						frameborder="0" allowfullscreen>
//


var boolPopupenable = $('#popUpEnable').val();
var Session = $('#popUpPerSession').val();

Session = (Session === "true");

var Cookie_popOut = true;
var youTubeId = $('#youtubePopUpId').val();
if (Session) {

	Cookie_popOut = !$.cookie("cookie-popupYoutubeId")
}


if (Cookie_popOut && boolPopupenable && $('.page-homepage').length && youTubeId) {


	$(document).on("ready", function () {

		setTimeout(function () {
			onYouTubeIframeAPIReady()
		}, 3000);


	});

	delete player;
	var player, time_update_interval = 0;

	var youTubeId = $('#youtubePopUpId').val();
	setTimeout(function () {
			function onYouTubeIframeAPIReady() {
				player = new YT.Player('video-placeholder', {
					width: 300,
					height: 300,
					videoId: youTubeId,
					playerVars: {
						color: 'white', 'autoplay': 1, 'controls': 1, mute: 1

					},
					events: {
						onReady: initialize,
					}
				});


				//console.log(player.playVideo())
			}

			onYouTubeIframeAPIReady()


		}


		, 500)

	function onPlayerReady(event) {
		$(".cont_popup").removeClass("hidden");
		$(".overlay_popup").removeClass("hidden");

		event.target.setVolume(0);
		event.target.playVideo();
	}

	function initialize() {
		// Update the controls on load
		updateTimerDisplay();
		updateProgressBar();

		// Clear any old interval.

		$(".cont_popup").removeClass("hidden");
		$(".overlay_popup").removeClass("hidden");
		$('#play').trigger("click")

		clearInterval(time_update_interval);
		// Start interval to update elapsed time display and
		// the elapsed part of the progress bar every second.
		var time_update_interval = setInterval(function () {
			var currentTime = formatTime(player.getCurrentTime());
			var fullTime = formatTime(player.getDuration());
			if (currentTime == fullTime) {
				$(".cont_popup").addClass("hidden");
				$('.overlay_popup').css("display", "none");
				if (Session) {
					$.cookie('cookie-popupYoutubeId', true, {path: '/'});
				}
				clearInterval(time_update_interval);

			}

		}, 1000);

		$('#play').trigger("click")

	}


	// This function is called by initialize()
	function updateTimerDisplay() {
		// Update current time text display.


		$('#current-time').text(formatTime(player.getCurrentTime()));
		$('#duration').text(formatTime(player.getDuration()));
	}


	// This function is called by initialize()
	function updateProgressBar() {
		// Update the value of our progress bar accordingly.
		$('#progress-bar').val();


	}


	// Progress bar

	$('#progress-bar').on('mouseup touchend', function (e) {

		// Calculate the new time for the video.
		// new time in seconds = total duration in seconds * ( value of range input / 100 )
		var newTime = player.getDuration() * (e.target.value / 100);

		// Skip video to new time.
		player.seekTo(newTime);

	});


	// Playback

	$('#play').on('click', function () {
		player.playVideo();
	});


	$('#pause').on('click', function () {
		player.pauseVideo();
	});


	// Sound volume


	$('#mute-toggle').on('click', function () {
		var mute_toggle = $(this);

		if (player.isMuted()) {
			player.unMute();
			mute_toggle.text('volume_up');
		} else {
			player.mute();
			mute_toggle.text('volume_off');
		}
	});

	$('#volume-input').on('change', function () {
		player.setVolume($(this).val());
	});


	// Other options


	$('#speed').on('change', function () {
		player.setPlaybackRate($(this).val());
	});

	$('#quality').on('change', function () {
		player.setPlaybackQuality($(this).val());
	});


	// Playlist

	$('#next').on('click', function () {
		player.nextVideo()
	});

	$('#prev').on('click', function () {
		player.previousVideo()
	});


	// Load video

	$('.thumbnail').on('click', function () {

		var url = $(this).attr('data-video-id');

		player.cueVideoById(url);

	});


	// Helper Functions

	function formatTime(time) {
		time = Math.round(time);

		var minutes = Math.floor(time / 60),
			seconds = time - minutes * 60;

		seconds = seconds < 10 ? '0' + seconds : seconds;

		return minutes + ":" + seconds;
	}


	$('pre code').each(function (i, block) {
		hljs.highlightBlock(block);
	});


	//setTimeout(function(){player.playVideo()},2500)


	$('body').on('click', '.overlay_popup', function () {

		$('.overlay_popup').css("display", "none");
		$('.cont_popup').css("display", "none");
		player.mute();
		clearInterval(time_update_interval);

		if (Session) {
			$.cookie('cookie-popupYoutubeId', true, {path: '/'});
		}


	});


	$('body').on('click', '.close_popup', function () {

		$('.overlay_popup').css("display", "none");
		$('.cont_popup').css("display", "none");
		player.mute();
		clearInterval(time_update_interval);

		if (Session) {
			$.cookie('cookie-popupYoutubeId', true, {path: '/'});
		}


	});


}


if ($('.referralDiv').length || $('.referralCode').length) {

	var textMsg = $('.textMsg').val()
	var textCode = $('.ReferralCodeText').text();
	var domain = window.location.hostname
	var textMsg2 = $('.textMsg2').val()
	var text = textMsg + textCode + ' ' + textMsg2 + '  ';
	//var fburl = 'http://www.facebook.com/dialog/send?app_id=2709220926023460&amp;link=https://' + domain + '/en/register?referralCode=' + textCode + '&amp;redirect_uri=https://' + domain;
	//$('.fbBtn').attr("href", fburl)
	var twurl = 'https://twitter.com/intent/tweet?original_referer=https://Fdeveloper.twitter.com&ref_src=twsrc%5Etfw&text=' + text + '&tw_p=tweetbutton&url=https://' + domain + '/en/register?referralCode=' + textCode;
	$('.twBtn').attr("href", twurl)
	var whatsurlweb = 'https://web.whatsapp.com/send?text=' + text + ' https://' + domain + '/en/register?referralCode=' + textCode;
	$('.whatsBtnWeb').attr("href", whatsurlweb)

	var whatsurl = 'whatsapp://send?text=' + text + ' https://' + domain + '/en/register?referralCode=' + textCode;
	$('.whatsBtn').attr("href", whatsurl)


}

$(".referralBtn").click(function () {
	$('.ReferralPage .boxreferral').addClass('SpaceShareDiv')
	$(".SharesocialMediaBox").fadeIn("300", function () {
		$('.SharesocialMediaBox').css("display", "block");


	});

});


$('#inline-popups').magnificPopup({
	delegate: 'a',
	removalDelay: 500, //delay removal by X to allow out-animation
	callbacks: {
		beforeOpen: function () {

			$('.ui-tagify-wrap').css('border-color', '#fff');
			$('.errorReferralForm').addClass('hidden');
			$('.msgsuccess').addClass('hidden');
			$('.conBottomFooter').removeClass('conBottomFooterOnSuccess');
			$('.darkbacks').addClass('hidden');
			$('.InputTagEmail').addClass('hidden');

			var textCode = $('.ReferralCodeText').text();
			var domain = window.location.hostname
			var urlcopy;
			if ($('.language-en').length) {
				urlcopy = 'https://' + domain + '/en/register?referralCode=' + textCode
			} else {
				urlcopy = 'https://' + domain + '/ar/register?referralCode=' + textCode
			}

			$('.textUrl').text(urlcopy)

			$('#myInput').val(urlcopy)
			this.st.mainClass = this.st.el.attr('data-effect');


		}
	},
	midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
});


$('.copyBtn').click(function () {
	var domain = window.location.hostname

	var textCode = $('.ReferralCodeText').text();
	var urlcopy;
	if ($('.language-en').length) {
		urlcopy = 'https://' + domain + '/en/register?referralCode=' + textCode
	} else {
		urlcopy = 'https://' + domain + '/ar/register?referralCode=' + textCode
	}

	$('.textUrl').text(urlcopy)

	$('#myInput').val(urlcopy)
	var copyText = document.getElementById("myInput");
	copyText.select();
	copyText.setSelectionRange(0, 99999);
	console.log(document.execCommand("copy"));

	var tooltip = document.getElementById("myTooltip");

	var copyid = $('.copyIDLabelText').val()
	tooltip.innerHTML = copyid;

});


$('.copyBtn').mouseout(function () {
	var tooltip = document.getElementById("myTooltip");
	var copy = $('.copyLabelText').val()

	tooltip.innerHTML = copy;
});


$(".emailFormIcon").click(function () {
	$('.ui-tagify-wrap').css('border-color', '#fff');
	$('.errorReferralForm').addClass('hidden');
	$('.tagify').val('');

	$('.ui-tagify-tag').each(function () {
		$(this).remove();
	});


	$('.msgsuccess').addClass('hidden');
	$(".darkbacks").fadeIn("300", function () {
		$('.darkbacks').removeClass("hidden");
	});
	$(".InputTagEmail").fadeIn("300", function () {
		$('.InputTagEmail').removeClass("hidden");

	});


});

$('.darkbacks').click(function () {
	$(".InputTagEmail").fadeOut("slow").fadeOut("slow", function () {
		$('.InputTagEmail').addClass("hidden");
		$('.darkbacks').addClass("hidden");

	});

});


$('.cancelBtn').click(function () {
	$(".InputTagEmail").fadeOut("slow").fadeOut("slow", function () {
		$('.InputTagEmail').addClass("hidden");
		$('.darkbacks').addClass("hidden");

	});

});


$('.closeIconEmail').click(function () {
	$(".InputTagEmail").fadeOut("slow").fadeOut("slow", function () {
		$('.InputTagEmail').addClass("hidden");
		$('.darkbacks').addClass("hidden");

	});

});


$("input[name='radioCartPickup']").click(function () {
	var val =$(this).val();
	$('#shipmenttype-selector').val(val)
	$('#shipmenttype-form').submit();

})
$('#shipmenttype-selector').change(function(){

			$('#shipmenttype-form').submit();
		});

$('.click-search').click(function () {
    $('.search-section').toggleClass('hidden');
})
$('.sort-by-click').click(function () {
    $('.sort_responsive_input').toggleClass('hidden');
})
$('.number-of-item-cart').html($('.hidden-sm .nav-items-total').html())

		
		$(".show_more").on("click", function () {
			$(this).addClass("hidden");
			$(".show_less").removeClass("hidden");
			$(".dots").addClass("hidden");  
			$(".more").addClass("viewdata");
		  });
		  $(".show_less").on("click", function () {
			$(this).addClass("hidden");
			$(".show_more").removeClass("hidden");
			$(".dots").removeClass("hidden");  
			  $(".more").removeClass("viewdata");
		  });


		  if($('.more').length){
			  $(".show_more").removeClass("hidden");
		  }
		  $(".express_btn").on("click", function () {
				$(".express_i").toggleClass("hidden");
			  });
			  
if($("#updateProfileForm").length){
	$("#updateProfileForm input.form-control").each(function() {
	if(!$(this).val() || $(this).val() === ''){
		$( this ).parents('.form-group').addClass( "has-error" );
	}
});
$("#updateProfileForm select.form-control").each(function() {
	if(!$(this).val() || $(this).val() === ''){
		$( this ).parents('.form-group').addClass( "has-error" );
	}
});
}