ACC.minicart = {
	
	_autoload: [
		"bindMiniCart"
	],

	bindMiniCart: function(){

		
		$(document).on("click",".js-mini-cart-link-js", function(e){
			e.preventDefault();
			var url = $(this).data("miniCartUrl");
			var cartName = ($(this).find(".js-mini-cart-count").html() != 0) ? $(this).data("miniCartName"):$(this).data("miniCartEmptyName");
			$.ajax({url: url, success: function(result){
			    $(".main_cart_component").html(result);
			  }});
			
			/*ACC.colorbox.open(ACC.common.encodeHtml(cartName),{
				href: url,
				maxWidth:"100%",
				width:"380px",
				opacity:0,
				right:0,
				bottom:0,
				initialWidth :"380px",
				onComplete: function(){
					

					if($(".global-alerts").length != 0) {
						 setTimeout(() =>$('.global-alerts').fadeOut('slow'), 5000);
						
					}
				}
			});*/
		});
		

		$(document).on("click",".js-mini-cart-close-button", function(e){
			e.preventDefault();
			$('.darkback').addClass("hidden");
			
			$('.cart_header_component').slideUp( 350, function() {
				
				$('.cart_header_component').addClass("hidden")
			});
		});
	},

	updateMiniCartDisplay: function(){
		var cartItems = $(".js-mini-cart-link").data("miniCartItemsText");
		var miniCartRefreshUrl = $(".js-mini-cart-link").data("miniCartRefreshUrl");
		$.ajax({
			url: miniCartRefreshUrl,
			cache: false,
			type: 'GET',
			dataType: 'json',
			success: function(jsonData){
				var $cartItems = $("<span>").addClass("items-desktop hidden-xs hidden-sm").text(" " + cartItems);
				var $numberItem = $("<span>").addClass("nav-items-total").text(jsonData.miniCartCount).append($cartItems);
				$(".js-mini-cart-link .js-mini-cart-count").empty();
				$(".js-mini-cart-link .js-mini-cart-count").append($numberItem);
				$(".js-mini-cart-link .js-mini-cart-price").text(jsonData.miniCartPrice);	
			}
		});
	}

};
