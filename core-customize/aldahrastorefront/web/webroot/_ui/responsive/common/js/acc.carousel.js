ACC.carousel = {

	_autoload: [
		["bindCarousel", $(".js-owl-carousel").length >0],
		"bindJCarousel"
	],

	carouselConfig:{
		"default-1":{

		},
		"default-2":{
			
		},
		"default-3":{
			
		},
		"default-4":{
			
		},
		"default-5":{
			
		},
		"default":{

		},
		"rotating-image":{

		},
		"lazy-reference":{
		
		},
		"slot-time":{
			
		}
	},

	bindCarousel: function(){
		
		$(".js-owl-carousel").each(function(){
			var $c = $(this);
			var autoRotate = $(this).parent('.carousel__component').find('.autoRotate').val();
			if(autoRotate === 'true'){
				autoRotate = '1000';
			}
			else{
				autoRotate = 'false';
			}
			var displayNavigation = $(this).parents('.carousel-component').find('.displayNavigation').val();
			if(displayNavigation === 'true'){
				displayNavigation = 'true';
			}
			else{
				displayNavigation = 'false';
			}
			var displayPagination = $(this).parents('.carousel-component').find('.displayPagination').val();
			if(displayPagination === 'true'){
				displayPagination = 'true';
			}
			else{
				displayPagination = 'false';
			}
			
			$.each(ACC.carousel.carouselConfig,function(key,config){
				if($c.hasClass("js-owl-"+key)){
					var $e = $(document).find(".js-owl-"+key);
					//$e.owlCarousel(config);
				if(key == 'rotating-image'){
					$e.owlCarousel({
						dots:true,
						nav:true,
						
						navText : ["<span class='far fa-chevron-circle-left'></span>", "<span class='far fa-chevron-circle-right'></span>"],
						items:1,
						autoplay:true,
						loop:true,
						rewind:true,
						autoplayTimeout:5000,
						dots:false,
						nav:false,
						animateOut: 'fadeOut',
					    animateIn: 'fadeIn',
					    autoplayHoverPause:false,
					    smartSpeed:450,
					    mouseDrag:false,
					    touchDrag:false,
					    
				        }); 
					
				}	
				if(key == 'default-3'){
					$e.owlCarousel({
						nav:true,
						margin:30,
						center:true,
						autoplay:true,
						loop:true,
						 autoplayHoverPause:true,
						rewind:true,
						slideTransition:'linear',
						items:3,
						navText : ["<span class='far fa-chevron-circle-left'></span>", "<span class='far fa-chevron-circle-right'></span>"],
						dots:true,
						responsive : {
						    0 : {
						    	items:1,
						    	dots:false
						    },
						    480 : {
						    	items:2,
						    	dots:false
						    },
						    768 : {
						    	items:3,
						    
						    }
						}
						
						
				        }); 
				}
				if(key == 'default-2'){
					$e.owlCarousel({
						nav:true,
						margin:30,
						center:true,
						autoplay:true,
						loop:true,
						 autoplayHoverPause:true,
						rewind:true,
						slideTransition:'linear',
						items:2,
						navText : ["<span class='far fa-chevron-circle-left'></span>", "<span class='far fa-chevron-circle-right'></span>"],
						dots:true,
						responsive : {
						    0 : {
						    	items:1,
						    	dots:false
						    },
						    480 : {
						    	items:2,
						    	dots:false
						    },
						    768 : {
						    	items:2,
						    	
						    }
						}
						
						
				        }); 
				}
				if(key == 'default-1'){
					$e.owlCarousel({
						nav:true,
						margin:30,
						center:true,
						autoplay:true,
						loop:true,
						 autoplayHoverPause:true,
						animateOut: 'fadeOut',
					    animateIn: 'fadeIn',
					    smartSpeed:450,
					    mouseDrag:false,
					    touchDrag:false,
						rewind:true,
						slideTransition:'linear',
						items:1,
						navText : ["<span class='far fa-chevron-circle-left'></span>", "<span class='far fa-chevron-circle-right'></span>"],
						dots:displayPagination,
						responsive : {
						    0 : {
						    	items:1,
						    	dots:false
						    },
						    480 : {
						    	items:1,
						    	dots:false
						    },
						    768 : {
						    	items:1,
						   
						    }
						}
						
						
				        }); 
				}
				if(key == 'default-4'){
					$e.owlCarousel({
						nav:true,
						margin:30,
						center:true,
						 autoplayHoverPause:true,
						autoplay:true,
						loop:true,
						rewind:true,
						slideTransition:'linear',
						items:4,
						navText : ["<span class='far fa-chevron-circle-left'></span>", "<span class='far fa-chevron-circle-right'></span>"],
						dots:displayPagination,
						responsive : {
						    0 : {
						    	items:1,
						    	dots:false
						    },
						    480 : {
						    	items:2,
						    	dots:false
						    },
						    768 : {
						    	items:4,
						    
						    }
						}
						
						
				        }); 
				}
				if(key == 'default-5'){
					$e.owlCarousel({
						nav:true,
						margin:30,
						center:true,
						autoplay:true,
						loop:true,
						 autoplayHoverPause:true,
						slideTransition:'linear',
						rewind:true,
						items:5,
						navText : ["<span class='far fa-chevron-circle-left'></span>", "<span class='far fa-chevron-circle-right'></span>"],
						dots:true,
						responsive : {
							0 : {
						    	items:1,
						    	dots:false
						    },
						    480 : {
						    	items:2,
						    	dots:false
						    },
						    768 : {
						    	items:3,
						    	
						    },
						    
						    900 : {
						    	items:4
						    }
						    ,
						    1020 : {
						    	items:5
						    }
						}
						
						
				        }); 
				}
				if(key == 'default'){
					$e.owlCarousel({
						nav:true,
						margin:30,
						center:false,
						
						loop:false,
						
						slideTransition:'linear',
						rewind:false,
						items:4,
						navText :  ["<span class='fas fa-chevron-left'></span>", "<span class='fas fa-chevron-right'></span>"],
						dots:true,
						responsive : {
						    0 : {
						    	items:1,
						    	margin:5,
						    	stagePadding:40,
						    	dots:false,
						    	loop:false,
						    },
						   450 : {
						    	items:2,
						    	margin:10,
						    	dots:false
						    },
						   
						    
						    900 : {
						    	items:3
						    }
						    ,
						    1200 : {
						    	items:4
						    }
						}
						
						
				        }); 
				}
				if(key == 'lazy-reference'){
					$e.owlCarousel({
						nav:true,
						margin:30,
						center:true,
						 autoplayHoverPause:true,
						autoplay:true,
						loop:true,
						rewind:true,
						items:3,
						navText : ["<span class='far fa-chevron-circle-left'></span>", "<span class='far fa-chevron-circle-right'></span>"],
						dots:displayPagination,
						responsive : {
						    0 : {
						    	items:1,
						    	dots:false,
						    	
						    },
						    480 : {
						    	items:2,
						    	dots:false
						    },
						    768 : {
						    	items:3,
						    	
						    }
						}
						
						
				        }); 
				}if(key == 'slot-time'){
					$e.owlCarousel({
						nav:true,
						margin:8,
					
						center:false,
						 autoplayHoverPause:true,
						autoplay:false,
						loop:false,
						rewind:false,
						items:3,
						navText : ["<span class='fas fa-chevron-left'></span>", "<span class='fas fa-chevron-right'></span>"],
						dots:false,
						
						responsive : {
						    0 : {
						    	items:1,
						    	dots:false
						    },
						    480 : {
						    	items:2,
						    	dots:false
						    },
						    
						    1000 : {
						    	items:3,
						    	
						    }
						}
						
						
				        }); 
				}
				
				
				
				
				}
			});
		});

	},
	
	bindJCarousel: function ()
	{
		
		
		$(".modal").colorbox({
			onComplete: function ()
			{
				ACC.common.refreshScreenReaderBuffer();
			},
			onClosed: function ()
			{
				ACC.common.refreshScreenReaderBuffer();
			}
		});
		$('.svw').each( function(){
	          $( this).waitForImages( function(){
	               $(this).slideView({toolTip: true, ttOpacity: 0.6, autoPlay: true, autoPlayTime: 8000});
	          });
	    });
	}

};
