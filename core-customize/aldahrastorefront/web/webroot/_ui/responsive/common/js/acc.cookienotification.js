ACC.coookienotification = {
    _autoload: [
    	["bindCookieNotificationClick", $(".js-cookie-notification-accept").length != 0]
    ],

    bindCookieNotificationClick:function(){
        $('.js-cookie-notification-accept').on("click",function(){
        	localStorage.setItem("cookie-notification", "ACCEPTED");
        	$.cookie('cookie-notification', "ACCEPTED", {expires: 365,path:'/'});
        	$('#js-cookie-notification').hide();
        });
    }
};

