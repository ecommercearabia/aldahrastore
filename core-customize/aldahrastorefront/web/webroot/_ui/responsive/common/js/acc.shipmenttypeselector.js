ACC.shipmentType = {

	_autoload: [
		"bindShipmentTypeSelector"
	],

	bindShipmentTypeSelector: function (){

		$('#shipmenttype-selector').change(function(){

			$('#shipmenttype-form').submit();
		});

	}
};
