	
	var area_zoom=false;
	var city_zoom=false;
(function($) {

 var check_input= true;
    var cureentpos= {
        lat:23.490185613291676,
        long:53.915918018749984
    }
    var pre= {
        lat:0,
        long:0
    }
var def_streetName=true;
    var def_bul_name=true;
   var street_name_val='';
    var build_name_val='';
    var ne_name_val='';
    var streetnumber='';
    var city='';
    var sublocality=''; 
    var res_full_add='';
    var area_main='';
    var area_sub='';
    var allcity = $('select#cityId option')
    var city_name= [];
    var ajax_result_sub_area=[];
    var ajax_result_main_area=[];
    var ajax_result_city=[];
    for(let i=1;i<allcity.length;i++){
    	
    	
    	city_name.push($(allcity[i]).attr('value'))
    }
  
    
    var allarea = $('select#areaId option')
    var area_name= []
    for(let i=1;i<allarea.length;i++){
    	
    	
    	area_name.push($(allarea[i]).attr('value'))
    }
  

    
    function GMapContext(domElement, options) {
    	
        var _map = new google.maps.Map(domElement, options);
        var _marker = new google.maps.Marker({
            position: new google.maps.LatLng(54.19335, -3.92695),
            map: _map,
            title: "Drag Me",
            visible: options.markerVisible,
            draggable: options.markerDraggable,
            icon: options.markerIcon !== undefined ? options.markerIcon : undefined
        });
       
        return {
            map: _map,
            marker: _marker,
            zoom:10,
            circle: null,
            location: _marker.position,
            radius: options.radius,
            locationName: options.locationName,
            addressComponents: {
                formatted_address: '',
                
            },
            
            settings: options.settings,
            domContainer: domElement,
            geodecoder: new google.maps.Geocoder()
        };
        
        
    }
    var GmUtility = {
        drawCircle: function(gmapContext, center, radius, options) {
            if (gmapContext.circle != null) {
                gmapContext.circle.setMap(null);
            }
            if (radius > 0) {
                radius *= 1;
                options = $.extend({
                    strokeColor: "#0000FF",
                    strokeOpacity: .35,
                    strokeWeight: 2,
                    fillColor: "#0000FF",
                    fillOpacity: .2
                }, options);
                options.map = gmapContext.map;
                options.radius = radius;
                options.center = center;
                gmapContext.circle = new google.maps.Circle(options);
                return gmapContext.circle;
            }
            return null;
        },
        setPosition: function(gMapContext, location, callback) {
            gMapContext.location = location;
            gMapContext.marker.setPosition(location);
            gMapContext.map.panTo(location);
           
            this.drawCircle(gMapContext, location, gMapContext.radius, {});
            if (gMapContext.settings.enableReverseGeocode) {
                this.updateLocationName(gMapContext, callback);
            } else {
                if (callback) {
                    callback.call(this, gMapContext);
                }
            }
        },
        locationFromLatLng: function(lnlg) {
            return {
                latitude: lnlg.lat(),
                longitude: lnlg.lng()
            };
        },
        addressByFormat: function(addresses, format) {
            var result = null;
        
            for (var i = addresses.length - 1; i >= 0; i--) {
                if (addresses[i].types.indexOf(format) >= 0) {
                    result = addresses[i];
                }
            }  
            return result || addresses[0];
        },
        updateLocationName: function(gmapContext, callback) {
           
            gmapContext.geodecoder.geocode({
                latLng: gmapContext.marker.position
            }, function(results, status) {
            	
                var out_side=false;
                if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
                    out_side=true;
                    var address = GmUtility.addressByFormat(results, gmapContext.settings.addressFormat);
                  
                    gmapContext.locationName = address.formatted_address;
                    gmapContext.addressComponents = GmUtility.address_component_from_google_geocode(address.address_components,gmapContext);
                } else if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
                    out_side=true;
                    return setTimeout(function() {
                       
                        GmUtility.updateLocationName(gmapContext, callback);
                    }, 1e3);
                }
                if (callback) {
                    callback.call(this, gmapContext);
                }
// if(!out_side){
// $("#outside").css("display","block");
// check_input=false;
//                    
//                  
//
// }
            });
        },
        address_component_from_google_geocode: function(address_components ,gmapContext) {
           area_main='';
            area_sub='';
        	street_name_val='';
        	build_name_val='';
        	ne_name_val='';
            var result = {};
            var check_inside=false;
             streetnumber='';
             city='';
             sublocality='';
              res_full_add='';
              city_val='';
              ajax_result_main_area=[];
              ajax_result_sub_area=[];
              ajax_result_city=[];
              $.ajax({url: 'https://maps.googleapis.com/maps/api/geocode/json?latlng='+ gmapContext.location.lat()+','+gmapContext.location.lng() +'&key=AIzaSyBDnqcqkmG9MqNZR6QKsB8V-mg-NNa71t8', success: function(result){
            	for(let k=0;k<result.results.length;k++){
            		for(let i=0;i<result.results[k].address_components.length;i++){
            		if(result.results[k].address_components[i].types.indexOf("neighborhood") >= 0)
            		{
            			ajax_result_main_area.push(result.results[k].address_components[i].short_name)
            			
            		}
            		if(result.results[k].address_components[i].types.indexOf("sublocality") >= 0)
            		{
            			ajax_result_sub_area.push(result.results[k].address_components[i].short_name)
            			
            		}
            		if(result.results[k].address_components[i].types.indexOf("locality") >= 0)
            		{ ajax_result_city.push(result.results[k].address_components[i].short_name)
            		
            			
            		}
            		
            		
            		
            		}
            		
            	}
          
                 if(ajax_result_city.length ){
      				var check_city= false
      				for(var i=0;i<ajax_result_city.length;i++){
      					
      				var city_option=ajax_result_city[i].toUpperCase().trim();
      				city_option= city_option.split(' ').join('_');
      				city_option= city_option.split('-').join('_');
      				city_option= city_option.split("'").join('');
      				if( city_name.includes(city_option) &&!check_city){
      					check_city=true;
      				
      					$("#cityId").val(city_option);
      					$('#cityId').selectpicker('refresh');
      					
      					$.ajax({
      						type : "GET",
      						url : ACC.config.encodedContextPath + "/misc/city/" + city_option + "/areas",
      						dataType : "json",
      						success : function(response) {
      						
      		                    var emptyListLBL = $('#areaId option:first').html();
      		                    var options = '';
      		                    $('#areaId').html('');
      	                        var emptyOption = '<option value="" class="hideinpopup">' + emptyListLBL + '</option>';
                             $('#areaId').append(emptyOption);
      	                        for (i = 0; i < response.data.length; i++) {
      	                            var code = response.data[i].code;
      	                            var name = response.data[i].name;
      	                            options += '<option value="' + code + '">' + name + '</option>';
      	                        }
      	                        $('#areaId').append(options);
      	                        $('#areaId').selectpicker('refresh');
      	                       
      	                        if(area_main.length){
      	                        	 var check_area = false;
      	                     		for(var i=0;i<ajax_result_main_area.length;i++){
      	            				var area_option=ajax_result_main_area[i].toUpperCase().trim();
      	            				area_option= area_option.split(' ').join('_');
      	            				area_option= area_option.split('-').join('_');
      	            				area_option= area_option.split("'").join('');
      	            				if( area_name.includes(area_option) && !check_area){
      	            					check_area = true;
      	            					
      	            					$("#areaId").val(area_option);
      	            					$("#areaId option[value="+area_option+"]").prop('selected', true)
      	            					$('#areaId').selectpicker('refresh');
      	            					
      	            				}else{
      	            					$("#areaId").val();
     	            					$('#areaId').selectpicker('refresh');
      	            					
      	            				}
      	            				
      	                     		}
      	                     		if( !check_area &&area_sub.length){
      	                     			
      	                     			
         	            					 var check_area = false;
         	            					 for(var i=0;i<ajax_result_sub_area.length;i++){
         	            				var area_option=ajax_result_sub_area[i].toUpperCase().trim();
         	            				area_option= area_option.split(' ').join('_');
         	            				area_option= area_option.split('-').join('_');
         	            				area_option= area_option.split("'").join('');
         	            				
         	            				if( area_name.includes(area_option) && !check_area){
         	            					check_area = true;
         	            				
         	            					$("#areaId").val(area_option);
         	            					$('#areaId').selectpicker('refresh');
         	            					$("#areaId option[value="+area_option+"]").prop('selected', true)
         	            				}else{
         	            					$("#areaId").val();
         	            					$('#areaId').selectpicker('refresh');
         	            				}
         	            				}
         	            				
         						
      	                     			
      	                     		}
      	                     		else{$("#areaId").val();
                  					$('#areaId').selectpicker('refresh');}
      	                     		
      	                     		
      	                     		
      	                     		
      	            			
      						}else{
      	            				if(area_sub.length){
      	            					 var check_area = false;
      	            					 for(var i=0;i<ajax_result_sub_area.length;i++){
      	            				var area_option=ajax_result_sub_area[i].toUpperCase().trim();
      	            				area_option= area_option.split(' ').join('_');
      	            				area_option= area_option.split('-').join('_');
      	            				area_option= area_option.split("'").join('');
      	            				
      	            				if( area_name.includes(area_option) && !check_area){
      	            					check_area = true;
      	            					
      	            					$("#areaId").val(area_option);
      	            					$('#areaId').selectpicker('refresh');
      	            					$("#areaId option[value="+area_option+"]").prop('selected', true)
      	            				}else{ 
      	            					
      	            					$("#areaId").val();
      	            					$('#areaId').selectpicker('refresh');
      	            				}
      	            				}
      	            				
      						}
      	            				else{
      	            					
      	            					$("#areaId").val();
      	            					$('#areaId').selectpicker('refresh');
      	            				}
      	            			}
      						}
      				});
      					
      				}
      			}
      			}else{
      				
      				$("#cityId").val('');
      				$('#cityId').selectpicker('refresh');
      				
      			
      			}
                 
                 
  }});
              
            for (var i = address_components.length - 1; i >= 0; i--) {
                var component = address_components[i];
            
              
    if(address_components[i].types.indexOf("locality") >= 0){
        check_inside=true
    }
 
      
                if (component.types.indexOf("point_of_interest") >= 0) {
                	
                	ne_name_val = component.short_name;
                } else if (component.types.indexOf("premise") >= 0) {
                	build_name_val=component.short_name;
                    result.streetNumber = component.short_name;
                }
                
                else if (component.types.indexOf("route") >= 0) {
               if(component.long_name!=='Unnamed Road'){
            	   
            	   street_name_val=component.long_name;
               }
                	
                    //result.streetName = component.short_name;
                }
                else if (component.types.indexOf("street_number") >= 0) {
                    streetnumber=component.short_name;
                   }               
                else if (component.types.indexOf("locality") >= 0  ) {
                	city_val = component.short_name;
                	 result.city = component.short_name;
                 
                }
                 else if (component.types.indexOf("sublocality") >= 0) {
                	 area_sub=component.short_name
                	 result.sublocality = component.short_name;
                } else if (component.types.indexOf("neighborhood") >= 0) {
                	area_main=component.short_name
                    result.stateOrProvince = component.short_name;
                } else if (component.types.indexOf("country") >= 0) {
                
                	if(component.short_name!=="AE"){
                   	 $("#outside").css("display","block")
                   	 check_input=false;
                   	$('#us2-address').val('');
                   	 
                	}
                   					else{
                   						
                   						setTimeout(() =>$('#outside').fadeOut('slow'), 100);
                                   		check_input=true;
                   						
                   					}
                    result.country = component.long_name;
                }
                
            }
// if(! check_inside){
// $("#outside").css("display","block");
// check_input=false;
//                 
//                
// }
          if(street_name_val.length){  	
            result.addressLine1 = [ streetnumber, street_name_val ].join(" ").trim();}
           
            if(checkarea){
            	updateInputValues({latitudeInput: $('#latitude'),
                    longitudeInput: $('#longitude'),
                    radiusInput: $('#us2-radius'),
                    locationNameInput: $('#us2-address')}, gmapContext) 
            	
            }
           
            let full_address=[];
            for(var i in result){
            	full_address.push(result[i])
            }
          
            for(var k=full_address.length -1;k >= 0;k-- ){
            	if(k !==0)res_full_add+=full_address[k]+'-';
            	else{
            		res_full_add+=full_address[k];
            	}}
           
            return result;
        }
    };
    function geoLocationError() {
        $("#block_geo").css("display","block")
        setTimeout(() =>$('#block_geo').fadeOut('slow'), 5000);
    }
                        function  geoLocationSuccess(position) {
       
        pre.lat = position.coords.latitude;
       pre.long = position.coords.longitude;
      $("#latitude").val(pre.lat)
      $("#longitude").val(pre.long)
      $('#us2').locationpicker({
        location: {
            latitude:pre.lat,
            longitude:pre.long
        },
        radius: 0,
        inputBinding: {
            latitudeInput: $('#latitude'),
            longitudeInput: $('#longitude'),
            radiusInput: $('#us2-radius'),
            locationNameInput: $('#us2-address')
        },
        enableAutocomplete: true,
        autocompleteOptions: {
            types: [],
            componentRestrictions: {country: 'AE'}
        }
         
           
       
    });

    }
    $(document).ready(function() { 
                       
        $("#geo").click(function(){


geocoder =   new  window.google.maps.Geocoder();                    
if (navigator.geolocation) {
navigator.geolocation.getCurrentPosition(geoLocationSuccess, geoLocationError);
} 
});
});
 
    function isPluginApplied(domObj) {
        return getContextForElement(domObj) != undefined;
    }
    function getContextForElement(domObj) {
        return $(domObj).data("locationpicker");
    }
    function updateInputValues(inputBinding, gmapContext) {
 
        if (!inputBinding) return;
        var currentLocation = GmUtility.locationFromLatLng(gmapContext.marker.position);
        if (inputBinding.latitudeInput) {
            inputBinding.latitudeInput.val(currentLocation.latitude).change();
        }
        if (inputBinding.longitudeInput) {
            inputBinding.longitudeInput.val(currentLocation.longitude).change();
        }
        if (inputBinding.radiusInput) {
            inputBinding.radiusInput.val(gmapContext.radius).change();
        }
        if (inputBinding.locationNameInput) {
        	
        	if($('#longitude').val() == 54.065219534058286 && $('#latitude').val() == 23.58238473915374){
        		
        		$('#us2-address').val('');
        		
        	}else{
        		if(check_input){
        		
        			if(def_streetName){
        				def_streetName=false;
        		}else{
        				
        				if(streetnumber.length){
        					var sublocality_val=''
        					if(sublocality.length){sublocality_val=' - '+ sublocality }
        					
        					$("#streetName").val(res_full_add)
        					
        						
        						
        					
        					}else{
            					var sublocality_val=''
                					if(sublocality.length){sublocality_val=' - '+ sublocality }
            					
            					
            					$('#streetName').val(res_full_add);
            					
            					
            				}
          				
        			}
        			
        			if(def_bul_name){
        				def_bul_name=false
            		
        			}else{
        				
        				if(build_name_val.length)$("#buildingName").val(build_name_val);
            			else
            				{
            				$("#buildingName").val(ne_name_val);
            				}
        			}
        			
        			
        			
        	
        			

        			
        			
        			
        			
        		     inputBinding.locationNameInput.val(gmapContext.locationName).change();
        		        	}
        		
        		else{
        		        		$('#us2-address').val('');
        		        		
        		        	}
        		
        	}
        	
        }
    }
    function setupInputListenersInput(inputBinding, gmapContext) {
        if (inputBinding) {
            if (inputBinding.radiusInput) {
                inputBinding.radiusInput.on("change", function(e) {
                    var radiusInputValue = $(this).val();
                    if (!e.originalEvent || isNaN(radiusInputValue)) {
                        return;
                    }
                    gmapContext.radius = radiusInputValue;
                    GmUtility.setPosition(gmapContext, gmapContext.location, function(context) {
                        context.settings.onchanged.apply(gmapContext.domContainer, [ GmUtility.locationFromLatLng(context.location), context.radius, false ]);
                    });
                });
            }
            if (inputBinding.locationNameInput && gmapContext.settings.enableAutocomplete) {
                var blur = false;
                var options = {
                 
                  types: [],
                  componentRestrictions: {country: 'AE'}
                };
               
                gmapContext.autocomplete = new google.maps.places.Autocomplete(inputBinding.locationNameInput.get(0), options);
                google.maps.event.addListener(gmapContext.autocomplete, "place_changed", function() {
                    blur = false;
                  
                    var place = gmapContext.autocomplete.getPlace();
                   
                    if (!place.geometry) {
                        gmapContext.settings.onlocationnotfound(place.name);
                        return;
                    }
                    GmUtility.setPosition(gmapContext, place.geometry.location, function(context) {
                        updateInputValues(inputBinding, context);
                        context.settings.onchanged.apply(gmapContext.domContainer, [ GmUtility.locationFromLatLng(context.location), context.radius, false ]);
                    });
                });
                if (gmapContext.settings.enableAutocompleteBlur) {
                    inputBinding.locationNameInput.on("change", function(e) {
                        if (!e.originalEvent) {
                            return;
                        }
                        blur = true;
                    });
                    inputBinding.locationNameInput.on("blur", function(e) {
                        if (!e.originalEvent) {
                            return;
                        }
                        setTimeout(function() {
                            var address = $(inputBinding.locationNameInput).val();
                            if (address.length > 5 && blur) {
                                blur = false;
                                gmapContext.geodecoder.geocode({
                                    address: address
                                }, function(results, status) {
                                    if (status == google.maps.GeocoderStatus.OK && results && results.length) {
                                        GmUtility.setPosition(gmapContext, results[0].geometry.location, function(context) {
                                            updateInputValues(inputBinding, context);
                                            context.settings.onchanged.apply(gmapContext.domContainer, [ GmUtility.locationFromLatLng(context.location), context.radius, false ]);
                                        });
                                    }
                                });
                            }
                        }, 1e3);
                    });
                }
            }
            if (inputBinding.latitudeInput) {
                inputBinding.latitudeInput.on("change", function(e) {
                   
                    var latitudeInputValue = $(this).val();
                    if (!e.originalEvent || isNaN(latitudeInputValue)) {
                        return;
                    }
                    GmUtility.setPosition(gmapContext, new google.maps.LatLng(latitudeInputValue, gmapContext.location.lng()), function(context) {
                        context.settings.onchanged.apply(gmapContext.domContainer, [ GmUtility.locationFromLatLng(context.location), context.radius, false ]);
                        updateInputValues(gmapContext.settings.inputBinding, gmapContext);
                    });
                });
            }
            if (inputBinding.longitudeInput) {
                inputBinding.longitudeInput.on("change", function(e) {
                    var longitudeInputValue = $(this).val();
                    if (!e.originalEvent || isNaN(longitudeInputValue)) {
                        return;
                    }
                    GmUtility.setPosition(gmapContext, new google.maps.LatLng(gmapContext.location.lat(), longitudeInputValue), function(context) {
                        context.settings.onchanged.apply(gmapContext.domContainer, [ GmUtility.locationFromLatLng(context.location), context.radius, false ]);
                        updateInputValues(gmapContext.settings.inputBinding, gmapContext);
                    });
                });
            }
        }
    }
    function autosize(gmapContext) {
        google.maps.event.trigger(gmapContext.map, "resize");
        setTimeout(function() {
            gmapContext.map.setCenter(gmapContext.marker.position);
        }, 300);
    }
    function updateMap(gmapContext, $target, options) {
    	
    	if(area_zoom == true) gmapContext.map.setZoom(14);
    	if(city_zoom === true) gmapContext.map.setZoom(12);
    
        var settings = $.extend({}, $.fn.locationpicker.defaults, options), latNew = settings.location.latitude, lngNew = settings.location.longitude, radiusNew = settings.radius, latOld = gmapContext.settings.location.latitude, lngOld = gmapContext.settings.location.longitude, radiusOld = gmapContext.settings.radius;
        if (latNew == latOld && lngNew == lngOld && radiusNew == radiusOld) return;
        gmapContext.settings.location.latitude = latNew;
        gmapContext.settings.location.longitude = lngNew;
        gmapContext.radius = radiusNew;
        GmUtility.setPosition(gmapContext, new google.maps.LatLng(gmapContext.settings.location.latitude, gmapContext.settings.location.longitude), function(context) {
            setupInputListenersInput(gmapContext.settings.inputBinding, gmapContext);
            context.settings.oninitialized($target);
        });
    }
    $.fn.locationpicker = function(options, params) {
        if (typeof options == "string") {
            var _targetDomElement = this.get(0);
            if (!isPluginApplied(_targetDomElement)) return;
            var gmapContext = getContextForElement(_targetDomElement);
            switch (options) {
              case "location":
                if (params == undefined) {
                    var location = GmUtility.locationFromLatLng(gmapContext.location);
                    location.radius = gmapContext.radius;
                    location.name = gmapContext.locationName;
                    return location;
                } else {
                    if (params.radius) {
                        gmapContext.radius = params.radius;
                    }
                    GmUtility.setPosition(gmapContext, new google.maps.LatLng(params.latitude, params.longitude), function(gmapContext) {
                        updateInputValues(gmapContext.settings.inputBinding, gmapContext);
                    });
                }
                break;

              case "subscribe":
                if (params == undefined) {
                    return null;
                } else {
                    var event = params.event;
                    var callback = params.callback;
                    if (!event || !callback) {
                     
                        return null;
                    }
                    google.maps.event.addListener(gmapContext.map, event, callback);
                }
                break;

              case "map":
                if (params == undefined) {
                    var locationObj = GmUtility.locationFromLatLng(gmapContext.location);
                    locationObj.formattedAddress = gmapContext.locationName;
                    locationObj.addressComponents = gmapContext.addressComponents;
                    return {
                        map: gmapContext.map,
                        marker: gmapContext.marker,
                        location: locationObj,
                      
                    };
                } else {
                    return null;
                }

              case "autosize":
                autosize(gmapContext);
                return this;
            }
            return null;
        }
        return this.each(function() {
            var $target = $(this);
            if (isPluginApplied(this)) {
                updateMap(getContextForElement(this), $(this), options);
                return;
            }
            var settings = $.extend({}, $.fn.locationpicker.defaults, options);
        
            var gmapContext = new GMapContext(this, $.extend({}, settings.mapOptions, {
                zoom: settings.zoom,
                center: new google.maps.LatLng(settings.location.latitude, settings.location.longitude),
                mapTypeId: settings.mapTypeId,
                mapTypeControl: false,
                styles: settings.styles,
                disableDoubleClickZoom: false,
                scrollwheel: settings.scrollwheel,
                streetViewControl: false,
                radius: settings.radius,
                locationName: settings.locationName,
                settings: settings,
                autocompleteOptions: settings.autocompleteOptions,
                addressFormat: settings.addressFormat,
                draggable: settings.draggable,
                markerIcon: settings.markerIcon,
                markerDraggable: settings.markerDraggable,
                markerVisible: settings.markerVisible
            }));
            $target.data("locationpicker", gmapContext);
            function displayMarkerWithSelectedArea() {
                GmUtility.setPosition(gmapContext, gmapContext.marker.position, function(context) {
                	setTimeout(() =>$('.tools-style').fadeOut('slow'), 100);
                    var currentLocation = GmUtility.locationFromLatLng(gmapContext.location);
                    updateInputValues(gmapContext.settings.inputBinding, gmapContext);
                    context.settings.onchanged.apply(gmapContext.domContainer, [ currentLocation, context.radius, true ]);
                });
            }
            if (settings.markerInCenter) {
                gmapContext.map.addListener("bounds_changed", function() {
                    if (!gmapContext.marker.dragging) {
                        gmapContext.marker.setPosition(gmapContext.map.center);
                        updateInputValues(gmapContext.settings.inputBinding, gmapContext);
                    }
                });
                gmapContext.map.addListener("idle", function() {
                    if (!gmapContext.marker.dragging) {
                        displayMarkerWithSelectedArea();
                    }
                });
            }
            google.maps.event.addListener(gmapContext.marker, "drag", function(event) {
                updateInputValues(gmapContext.settings.inputBinding, gmapContext);
            });
            google.maps.event.addListener(gmapContext.marker, "dragend", function(event) {
                displayMarkerWithSelectedArea();
            });
            GmUtility.setPosition(gmapContext, new google.maps.LatLng(settings.location.latitude, settings.location.longitude), function(context) {
                updateInputValues(settings.inputBinding, gmapContext);
                setupInputListenersInput(settings.inputBinding, gmapContext);
                context.settings.oninitialized($target);
            });
        });
    };
    
    
    $.fn.locationpicker.defaults = {
        location: {
            latitude: 40.7324319,
            longitude: -73.82480777777776
        },
        locationName: "",
        radius: 0,
    zoom:8,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: [],
        mapOptions: {
        	 zoom: 8
        },
        scrollwheel: true,
        inputBinding: {
            latitudeInput: null,
            longitudeInput: null,
            radiusInput: null,
            locationNameInput: null
        },
        enableAutocomplete: false,
        enableAutocompleteBlur: false,
        autocompleteOptions: {
            types: ["postal_code"],
        },
        addressFormat: 'postal_code',
        enableReverseGeocode: true,
        draggable: true,
        onchanged: function(currentLocation, radius, isMarkerDropped) {},
        onlocationnotfound: function(locationName) {},
        oninitialized: function(component) {},
        markerIcon: undefined,
        markerDraggable: true,
        markerVisible: true
    };
})(jQuery);
