<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<spring:htmlEscape defaultHtmlEscape="true"/>

<c:if test="${fn:length(supportedShipmentTypes) > 1}">
    <spring:url value="/cart/shipment-type" var="setShipmentTypeActionUrl"/>
    <spring:url value="/cart/shipment-type" var="setShipmentTypeActionUrl"/>
    <form:form action="${setShipmentTypeActionUrl}" method="post" id="shipmenttype-form">
        <div class="form-group">
            <spring:theme code="text.shipmenttype" var="shipmentTypeText"/>
            <label class="control-label sr-only" for="shipmenttype-selector">${shipmentTypeText}</label>

            <select name="code" id="shipmenttype-selector" class="form-control hidden">
                <c:forEach items="${supportedShipmentTypes}" var="shipmentType">
                    <c:choose>
                        <c:when test="${shipmentType.code == currentShipmentType.code}">
                            <option value="${fn:escapeXml(shipmentType.code)}" selected="selected"
                                    lang="${fn:escapeXml(shipmentType.code)}">${fn:escapeXml(shipmentType.name)}</option>
                        </c:when>
                        <c:otherwise>
                            <option value="${fn:escapeXml(shipmentType.code)}"
                                    lang="${fn:escapeXml(shipmentType.code)}">
                                    ${fn:escapeXml(shipmentType.name)}
                            </option>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </select>


            <div class="pickUpInCart">

                <c:forEach items="${supportedShipmentTypes}" var="shipmentType">

                    <c:if test="${shipmentType.code == 'PICKUP_IN_STORE'}">
                        <c:set value="" var="activeRbtn" />
                        <c:if test="${currentShipmentType.code  == 'PICKUP_IN_STORE'}">
                            <c:set value="activeR" var="activeRbtn" />
                        </c:if>
                        <label class="container ${activeRbtn}">
                            <div class="content-check">
                                <span class="icon-header-pickup"><i
                                        class="far fa-store-alt"></i></span>
                                <div class="content-div-text">
                                    <div class="header-div-pickup">
                                            ${fn:escapeXml(shipmentType.name)}


                                    </div>



                                </div>
                            </div>

                            <c:choose>
                                <c:when test="${currentShipmentType.code =='PICKUP_IN_STORE'}">
                                    <input type="radio" checked="checked" value="${fn:escapeXml(shipmentType.code)}"
                                           lang="${fn:escapeXml(shipmentType.code)}" name="radioCartPickup">
                                    <span class="checkmark"></i></span>

                                </c:when>
                                <c:otherwise>
                                    <input type="radio" value="${fn:escapeXml(shipmentType.code)}"
                                           lang="${fn:escapeXml(shipmentType.code)}" name="radioCartPickup">
                                    <span class="checkmark"></span>
                                </c:otherwise>

                            </c:choose>


                        </label>

                    </c:if>
                    <c:if test="${shipmentType.code != 'PICKUP_IN_STORE'}">
                        <c:set value="" var="activeRbtn" />
                        <c:if test="${currentShipmentType.code  != 'PICKUP_IN_STORE'}">
                            <c:set value="activeR" var="activeRbtn" />
                        </c:if>
                        <label class="container ${activeRbtn}">

                            <div class="content-check">
													<span class="icon-header-pickup">

                                                        <i class="far fa-truck"></i>
                                                    </span>
                                <div class="content-div-text">
                                    <div class="header-div-pickup">${fn:escapeXml(shipmentType.name)}</div>

                                    <div class="massage-div-pickup">



                                    </div>

                                </div>
                            </div>


                            <c:choose>
                                <c:when test="${currentShipmentType.code !='PICKUP_IN_STORE'}">
                                    <input type="radio" checked="checked" value="${fn:escapeXml(shipmentType.code)}"
                                           lang="${fn:escapeXml(shipmentType.code)}" name="radioCartPickup">
                                    <span class="checkmark"></span>

                                </c:when>
                                <c:otherwise>
                                    <input type="radio" value="${fn:escapeXml(shipmentType.code)}"
                                           lang="${fn:escapeXml(shipmentType.code)}" name="radioCartPickup">
                                    <span class="checkmark"></span>
                                </c:otherwise>

                            </c:choose>
                        </label>
                    </c:if>

                </c:forEach>
            </div>


        </div>
    </form:form>
    <c:choose>
        <c:when test="${currentShipmentType.code == 'PICKUP_IN_STORE'}">
            <div class="cartData">

                <h5>${fn:escapeXml(cartData.deliveryPointOfService.displayName)}</h5>
                    ${fn:escapeXml(cartData.deliveryPointOfService.address.formattedAddress)}&nbsp;
                <a target="_blank" href="https://www.google.com/maps/dir/?api=1&travelmode=driving&layer=traffic&destination=${fn:escapeXml(cartData.deliveryPointOfService.address.latitude)},${fn:escapeXml(cartData.deliveryPointOfService.address.longitude)}"><spring:theme code="store.link.title"/></a>
            </div>
        </c:when>
        <c:otherwise></c:otherwise>

    </c:choose>
</c:if>
