<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="galleryImages" required="true" type="java.util.List" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<spring:htmlEscape defaultHtmlEscape="true"/>


<c:choose>
    <c:when test="${galleryImages == null || galleryImages.size() == 0}">

        <div class="col-md-8 col-sm-12 col-xs-12 active pull-right">
            <spring:theme code="img.missingProductImage.responsive.product" var="imagePath" htmlEscape="false"/>
            <c:choose>
                <c:when test="${originalContextPath ne null}">
                    <c:choose>
                        <c:when test='${fn:startsWith(imagePath, originalContextPath)}'>
                            <c:url value="${imagePath}" var="imageUrl" context="/"/>
                        </c:when>
                        <c:otherwise>
                            <c:url value="${imagePath}" var="imageUrl" context="${originalContextPath}"/>
                        </c:otherwise>
                    </c:choose>
                </c:when>
                <c:otherwise>
                    <c:url value="${imagePath}" var="imageUrl"/>
                </c:otherwise>
            </c:choose>
            <img src="${fn:escapeXml(imageUrl)}"/>
        </div>

    </c:when>
    <c:otherwise>
        <div class="app-figure" id="zoom-fig">


            <c:forEach items="${galleryImages}" var="container" varStatus="varStatus" end="0" begin="0" step="1">

                <a id="Zoom-1" class="MagicZoom col-md-8 col-sm-12 col-xs-12 active pull-right main_zoom"
                   data-options="zoomWidth: 50%; zoomMode: magnifier; zoomDistance: 10;"
                   href="${fn:escapeXml(container.superZoom.url)}"
                   data-zoom-image-2x="${fn:escapeXml(container.superZoom.url)}"
                   data-image-2x="${fn:escapeXml(container.zoom.url)}"
                >
                    <c:set value="none" var="display"/>
                    <c:if test="${not empty product.productLabel}"><c:set value="inline" var="display"/></c:if>
                    <div class="carousel__item--label label_image"
                         style="display:${display}">${product.productLabel}</div>


                    <img src="${fn:escapeXml(container.superZoom.url)}"
                         srcset="${fn:escapeXml(container.superZoom.url)}"
                         alt="" style="max-width:600px;"/><span class="smile"></span><c:set value="hidden"
                                                                                            var="isin"></c:set>


                        <%--                 <c:if test="${not empty product.productLabel}"> --%>
                        <%-- 				<div class="ribbon2 top ${not empty product.productLabelPosition ? product.productLabelPosition : 'top right'}" style="background-color:${not empty product.productLabelColor ? product.productLabelColor : '#000'}">${fn:escapeXml(product.productLabel)}</div> --%>
                        <%-- 				</c:if> --%>


                    <div class="div-label-cla">
                        <c:if test="${not empty product.discount.percentage}">
                            <div class="promo_label">-<fmt:formatNumber type="NUMBER" value="${product.discount.percentage}"
                                                                        maxFractionDigits="0"/>%
                            </div>
                        </c:if>

                        <c:if test="${product.frozen}">
                            <div class="label-frozen" title='<spring:theme
                                                        code="plp.title.frozen"/>'>
                                <div class="thumb"></div>

                            </div>
                        </c:if>
                        <c:if test="${product.chilled}">
                            <div class="label-chilled" title='<spring:theme
                                                        code="plp.title.chilled"/>'>
                                <div class="thumb"></div>

                            </div>
                        </c:if>
                        <c:if test="${product.dry}">
                            <div class="label-Dry" title='<spring:theme
                                                        code="plp.title.dry"/>'>
                                <div class="thumb"></div>

                            </div>
                        </c:if>

                    </div>
                </a>


            </c:forEach>
                <%--                 <c:if test="${not empty product.youtubeIds}"> --%>
            <!--                   <div data-slide-id="video-1" class="zoom-gallery-slide video-slide"> -->
                <%--         <iframe width="100%" height="315" src="https://www.youtube.com/embed/${product.youtubeIds}" frameborder="0" allowfullscreen></iframe> --%>
            <!--     </div> -->
                <%--                 </c:if> --%>
            <product:productGalleryThumbnail galleryImages="${galleryImages}"/>


            <c:url value="javascript:;" var="link"></c:url>
            <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
                <c:url value="/login" var="link"></c:url>
            </sec:authorize>
            <c:set value="" var="isout"></c:set>
            <c:set value="hidden" var="isin"></c:set>
            <c:if test="${product.inWishlist}">
                <c:set value="" var="isin"></c:set>
                <c:set value="hidden" var="isout"></c:set>
            </c:if>
            <c:if test="${!product.inWishlist}">
                <c:set value="hidden" var="isin"></c:set>
                <c:set value="" var="isout"></c:set>
            </c:if>


            <span class="wishlist_icon">
	<a href="${link}" title="wishlist" class="removeWishlistEntry wishlistbtn  ${isin}"
       data-productcode="${product.code}" data-pk="8796093055677"><i class="fas fa-heart font_w"></i></a>
	<a href="${link}" title="wishlist" class="addWishlistEntry wishlistbtn ${isout}" data-productcode="${product.code}"
       data-pk="8796093055677"><i class="far fa-heart"></i></a>
                </span>


        </div>
    </c:otherwise>
</c:choose>
