<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="productCarouselComponentData" required="true"
              type="com.aldahra.aldahracomponents.data.ProductCarouselComponentData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<spring:htmlEscape defaultHtmlEscape="true"/>
<c:url value="javascript:;" var="wishlist_link"></c:url>
                                    <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
                                        <c:url value="/login" var="wishlist_link"></c:url>
                                    </sec:authorize>
<c:choose>
    <c:when test="${not empty productCarouselComponentData.products}">
        <div class="carousel__component">
            <div class="carousel__component--headline hidden">${fn:escapeXml(productCarouselComponentData.title)}</div>

            <c:choose>
                <c:when test="${productCarouselComponentData.popup}">
                    <div class="carousel__component--carousel owl-carousel js-owl-carousel js-owl-lazy-reference js-owl-carousel-reference owl-theme">
                        <div id="quickViewTitle" class="quickView-header display-none">
                            <div class="headline">
                                <span class="headline-text"><spring:theme code="popup.quick.view.select"/></span>
                            </div>
                        </div>
                        <c:forEach items="${productCarouselComponentData.products}" var="product">

                            <c:url value="${product.url}/quickView" var="productQuickViewUrl"/>
                            <div class="carousel__item">
                                <a href="${productQuickViewUrl}" class="js-reference-item">
                                    <div class="carousel__item--thumb">
                                        <product:productPrimaryReferenceImage product="${product}" format="zoom"/>
                                    </div>
                                    <div class="carousel__item--name">

                                        <c:choose>

                                            <c:when test="${fn:length(product.name) > 50}">
                                                <c:out value="${fn:substring(product.name, 0, 50)}..."/>
                                            </c:when>
                                            <c:otherwise>
                                                ${fn:escapeXml(product.name)}
                                            </c:otherwise>
                                        </c:choose>

                                    </div>
                                    <div class="carousel__item--price"><format:fromPrice
                                            priceData="${product.price}"/></div>
                                </a>
                            </div>
                        </c:forEach>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="carousel__component--carousel js-owl-carousel owl-carousel js-owl-default owl-theme">
                        <c:forEach items="${productCarouselComponentData.products}" var="product">

                            <c:url value="${product.url}" var="productUrl"/>

                            <div class="carousel__item ${product.code}">


                                <div class="carousel__item--thumb">


                                    <div class="div-label-cla">
                                        <c:if test="${not empty product.discount.percentage}">
                                            <div class="promo_label">-<fmt:formatNumber type="NUMBER"
                                                                                        value="${product.discount.percentage}"
                                                                                        maxFractionDigits="0"/>%
                                            </div>
                                        </c:if>
                                        <c:if test="${product.dry}">
                                            <div class="label-Dry" title='<spring:theme
                                                        code="plp.title.dry"/>'>
                                                <div class="thumb"></div>

                                            </div>
                                        </c:if>
                                        <c:if test="${product.chilled}">
                                            <div class="label-chilled" title='<spring:theme
                                                        code="plp.title.chilled"/>'>
                                                <div class="thumb"></div>

                                            </div>
                                        </c:if>
                                        <c:if test="${product.frozen}">
                                            <div class="label-frozen" title='<spring:theme
                                                        code="plp.title.frozen"/>'>
                                                <div class="thumb"></div>

                                            </div>
                                        </c:if>

                                    </div>


                                   
                                    <c:set value="" var="isout"></c:set>
                                    <c:set value="hidden" var="isin"></c:set>
                                    <c:if test="${product.inWishlist}">
                                        <c:set value="" var="isin"></c:set>
                                        <c:set value="hidden" var="isout"></c:set>
                                    </c:if>
                                    <c:if test="${!product.inWishlist}">
                                        <c:set value="hidden" var="isin"></c:set>
                                        <c:set value="" var="isout"></c:set>
                                    </c:if>

                                    <span class="wishlist_icon">
	<a href="${wishlist_link}" title="wishlist" class="removeWishlistEntry wishlistbtn ${isin}"
       data-productcode="${product.code}" data-pk="8796093055677"><i class="fas fa-heart"></i></a>
	<a href="${wishlist_link}" title="wishlist" class="addWishlistEntry wishlistbtn ${isout}" data-productcode="${product.code}"
       data-pk="8796093055677"><i class="far fa-heart"></i></a>
				</span>
                                    <a href="${productUrl}">
                                        <c:if test="${not empty product.productLabel}">
                                            <div class="carousel__item--label">${product.productLabel}</div>
                                        </c:if>
                                        <product:productPrimaryImage product="${product}" format="zoom"/>

                                        <span class="smile smile_owl "></span>
                                    </a>
                                </div>

                                <div class="cont_detail_carousel">
                                    <a href="${productUrl}">
                                        <div class="cont_href_div">

                                            <div class="carousel__item--name">

                                                <c:choose>
                                                    <c:when test="${fn:length(product.name) > 44}">
                                                        <c:out value="${fn:substring(product.name, 0, 44)}..."/>
                                                    </c:when>
                                                    <c:otherwise>
                                                        ${fn:escapeXml(product.name)}
                                                    </c:otherwise>
                                                </c:choose>

                                            </div>


                                            <c:if test="${not empty product.countryOfOrigin}">
                                                <div class="carousel__item--countryoforigin hidden">${product.countryOfOrigin}</div>
                                                <c:set value="${fn:replace(product.countryOfOriginIsocode, ' ', '-')}"
                                                       var="countryOfOrigin"></c:set>
                                                <div class="carousel__item--countryoforiginisocode"><i
                                                        class="flagicon ${fn:toLowerCase(countryOfOrigin)}"></i>${product.countryOfOrigin}
                                                    /
                                                </div>
                                            </c:if>
                                            <div class="carousel__item--unitofmeasure ">${product.unitOfMeasure}</div>
                                            <div class="carousel__item--unitofmeasuredescription hidden">${product.unitOfMeasureDescription}</div>
                                        </div>
                                    </a>
                                    <div class="price_counter"><c:if
                                            test="${product.stock.stockLevelStatus.code eq 'outOfStock'}"><span
                                            class="outstock"><i class="fas fa-times-circle"></i><spring:theme
                                            code='product.variants.out.of.stock'/></span></c:if>
                                        <div class="carousel__item--price">
                                            <c:choose>
                                                <c:when test="${not empty product.discount}">

                                                    <p class="price"><format:fromPrice
                                                            priceData="${product.discount.discountPrice}"/></p>
                                                    <span class="scratched"><format:fromPrice
                                                            priceData="${product.discount.price}"/> <span
                                                            class="line_dis"></span></span>
                                                </c:when>
                                                <c:otherwise>

                                                    <p class="price"><format:fromPrice
                                                            priceData="${product.price}"/></p>
                                                    <span class="scratched"></span>
                                                </c:otherwise>
                                            </c:choose>
                                        </div>

                                        <product:addtocartcarousel showQuantityBox="true" product="${product}"/>
                                        <c:if test="${product.express}">
												<span class="expressdelivery"><img
                                                        src="${fn:escapeXml(themeResourcePath)}/images/express.png"/>
												</span>
                                        </c:if>
                                    </div>
                                </div>


                            </div>

                        </c:forEach>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
    </c:when>

    <c:otherwise>
        <component:emptyComponent/>
    </c:otherwise>
</c:choose>

