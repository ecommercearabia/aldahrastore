<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>


<div class="bundle">
	<ycommerce:testId code="productDetails_promotion_label">
		<c:if test="${not empty product.potentialPromotions}">
			<c:choose>
				<c:when test="${not empty product.potentialPromotions[0].couldFireMessages}">
					<p class="promotion">${ycommerce:sanitizeHTML(product.potentialPromotions[0].couldFireMessages[0])}</p>
				</c:when>
				<c:otherwise>
					<c:forEach items="${product.potentialPromotions}" var="promotion">
					
					<c:if test="${not empty promotion.endDate && promotion.enablePromotionCountDownTimer}">
						<div class="btn-block promo_endDateCont">
							<p class="time_m"><i class="fal fa-clock"></i> <spring:theme code="TimeEndOffer" text="time remaining"/></p>
							

							<input type="hidden" value=' <fmt:formatDate pattern = "hh:mm:ss yyyy-MM-dd" 
         value = "${promotion.endDate}" />' class="promo_endDate hidden"/>
						      <div id="flipdown" class="flipdown"></div>
						</div>
						
						      </c:if>
						<p class="promotion">${ycommerce:sanitizeHTML(promotion.description)}</p>
					</c:forEach>
				</c:otherwise>
			</c:choose>
		</c:if>
	</ycommerce:testId>
</div>