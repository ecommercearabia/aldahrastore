<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/responsive/action" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="showQuantityBox" required="false" type="java.lang.Boolean" %>
<spring:htmlEscape defaultHtmlEscape="true" />



<c:set var="isForceInStock" value="${product.stock.stockLevelStatus.code eq 'inStock' and empty product.stock.stockLevel}"/>
<c:choose> 
  <c:when test="${isForceInStock}">
    <c:set var="maxQty" value="FORCE_IN_STOCK"/>
  </c:when>
  <c:otherwise>
    <c:set var="maxQty" value="${product.stock.stockLevel}"/>
  </c:otherwise>
</c:choose>
<c:set var="qtyMinus" value="1" />

<div class="addtocart-component">
		
		<c:if test="${product.stock.stockLevel gt 0}">
			<c:set var="productStockLevel">${product.stock.stockLevel}&nbsp;
				<spring:theme code="product.variants.in.stock"/>
			</c:set>
		</c:if>
		<c:if test="${product.stock.stockLevelStatus.code eq 'lowStock'}">
			<c:set var="productStockLevel">
				<spring:theme code="product.variants.only.left" arguments="${product.stock.stockLevel}"/>
			</c:set>
		</c:if>
		<c:if test="${isForceInStock}">
			<c:set var="productStockLevel">
				<spring:theme code="product.variants.available"/>
			</c:set>
		</c:if>
<!-- 		<div class="stock-wrapper clearfix"> -->
<%-- 			${productStockLevel} --%>
<!-- 		</div> -->
		 <div class="actions">
        <c:if test="${multiDimensionalProduct}" >
                <c:url value="${product.url}/orderForm" var="productOrderFormUrl"/>
                <a href="${productOrderFormUrl}" class="btn btn-default btn-block btn-icon js-add-to-cart glyphicon-list-alt">
                    <spring:theme code="order.form" />
                </a>
        </c:if>
        

<c:if test="${not product.multidimensional }">
    <c:url value="/cart/add" var="addToCartUrl"/>
	<spring:url value="${product.url}/configuratorPage/{/configuratorType}" var="configureProductUrl" htmlEscape="false">
		<spring:param name="configuratorType" value="${configuratorType}" />
	</spring:url>

	<form:form id="" action="${addToCartUrl}" method="post" class="add_to_cart_form pdpAddtocart">

        <ycommerce:testId code="addToCartButton">
            <input type="hidden" name="productCodePost" value="${fn:escapeXml(product.code)}"/>
            <input type="hidden" name="productNamePost" value="${fn:escapeXml(product.name)}"/>
            <input type="hidden" name="productPostPrice" value="${product.price.value}"/>
            <c:if test="${showQuantityBox}">
            
            <div class="qty-selector input-group js-qty-selector ">
					<span >
						<button class="js-qty-selector-minus btnqty" aria-label="minus" type="button" <c:if test="${qtyMinus <= 1}"><c:out value="disabled='disabled'"/></c:if> ><span class="far fa-minus" aria-hidden="true"></span></button>
					</span>
					
					<label>
                        <span class="visually-hidden">Product Quantity Counter</span>
					<input type="text" maxlength="3" autocomplete="off" class="js-qty-selector-input" size="1" value="${qtyMinus}" data-max="${maxQty}" data-min="1" name="qty" />
                    </label>
                        <span class="">
						<button class="js-qty-selector-plus btnqty" aria-label="plus" type="button"><span class="far fa-plus" aria-hidden="true"></span></button>
					</span>
				</div>

</c:if>
            <c:choose>
                <c:when test="${product.stock.stockLevelStatus.code eq 'outOfStock' }">
                    <button type="submit" class="btn btnicon"
                            aria-disabled="true" disabled="disabled" title="<spring:theme code='product.variants.out.of.stock'/>">
                            <span class="Add_cart_icon"></span>
                    </button>
                </c:when>
                <c:otherwise>
                    <button type="submit" class="btn btnicon js-enable-btn" title="<spring:theme code='basket.add.to.basket'/>"
                            disabled="disabled">
                            <span class="Add_cart_icon"></span>
                    </button>
                </c:otherwise>
            </c:choose>
        </ycommerce:testId>
    </form:form>

    <form:form id="" action="${configureProductUrl}" method="get" class="configure_form">
        <c:if test="${product.configurable}">
            <c:choose>
                <c:when test="${product.stock.stockLevelStatus.code eq 'outOfStock' }">
                    <button id="configureProduct" type="button" class="btn btn-primary btn-block"
                            disabled="disabled">
                        <spring:theme code="basket.configure.product"/>
                    </button>
                </c:when>
                <c:otherwise>
                    <button id="configureProduct" type="button" class="btn btn-primary btn-block js-enable-btn" disabled="disabled"
                            onclick="location.href='${configureProductUrl}'">
                        <spring:theme code="basket.configure.product"/>
                    </button>
                </c:otherwise>
            </c:choose>
        </c:if>
    </form:form>
</c:if>
 
 <div class="addedtocart">
	<spring:theme code="basket.added.to.basket"/>
</div>
<div class="addedtocarterror">
	
</div>
<div class="addedtocartinfo">
	
</div>
    </div>
   
</div>
