<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ attribute name="galleryImages" required="true" type="java.util.List" %>

   <div class="selectors_pdp col-md-4 col-sm-12 col-xs-12  ">

    <c:forEach items="${galleryImages}" var="container" varStatus="varStatus">
    <a 
                data-zoom-id="Zoom-1" class="item_banner"
                href="${fn:escapeXml(container.superZoom.url)}"
                data-image="${fn:escapeXml(container.superZoom.url)}"
                data-zoom-image-2x="${fn:escapeXml(container.superZoom.url)}"
                data-image-2x="${fn:escapeXml(container.zoom.url)}"
            >
                <img srcset="${fn:escapeXml(container.product.url)}" src="${fn:escapeXml(container.thumbnail.url)}"/>
     <span class="smile"></span>       </a>
           
    </c:forEach>
<%--      <c:if test="${not empty product.youtubeIds}"> --%>
<!--     <a data-slide-id="video-1" href="javascript:;" class="mz-youtube"> -->
<!--             <span class="glyphicon glyphicon-play"></span> -->
<!--         </a> -->
<%--         </c:if> --%>
</div>
