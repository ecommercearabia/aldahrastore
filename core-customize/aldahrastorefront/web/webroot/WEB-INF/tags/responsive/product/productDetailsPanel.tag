<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="row  ${product.code}">
    <div class="col-xs-10 col-xs-push-1 col-sm-6 col-sm-push-0 col-md-7 cont_gallary_img">


        <product:productImagePanel galleryImages="${galleryImages}"/>
    </div>
    <div class="clearfix hidden-sm hidden-md hidden-lg"></div>
    <div class="col-sm-6 col-md-5">


        <div class="product-main-info">
            <div class="row">
                <div class="col-xs-12">
                    <div class="product-details">
                        <div class="btn-block pull-left">
                            <c:if test="${product.organic}">
                                <div class="organic pos_label_filter"><span class="eco_position"><spring:theme
                                        code="plp.organic.title" text="Organic"/><i class="fas fa-leaf"></i></span>
                                </div>
                            </c:if>
                            <c:if test="${product.vegan}">
                                <div class="organic pos_label_filter vegan"><span class="eco_position"> <spring:theme
                                        code="plp.Vegan.title" text="Vegan"/><i class="fas fa-leaf"></i></span></div>
                            </c:if>
                            <c:if test="${product.glutenFree}">
                                <div class="organic pos_label_filter glutenFree"><span
                                        class="eco_position"><spring:theme code="plp.GlutenFree.title"
                                                                           text="Gluten Free"/><i
                                        class="fas fa-leaf"></i></span></div>
                            </c:if>

                        </div>
                        <product:productReviewSummary product="${product}" showLinks="true"/>


                        <ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">
                            <h1 class="name">${fn:escapeXml(product.name)}</h1>
                            <div class="id_code"><span class="sku">ID</span><span
                                    class="code">${fn:escapeXml(product.code)}</span></div>
                        </ycommerce:testId>
                        <c:if test="${ not empty product.unitOfMeasureDescription}">
                            <div class="unitdescription">
                                    ${product.unitOfMeasureDescription}
                            </div>
                        </c:if>
                        <span  class="nutritionFacts" hidden="hidden">${ycommerce:sanitizeHTML(product.nutritionFacts)}</span>
                        <br><span class="code_summary">${ycommerce:sanitizeHTML(product.summary)}</span>
                        <c:if test="${ not empty product.storageInstructions}">
                            <div class="unitdescription">
                                <span><spring:theme
                                        code="pdp.storageInstructions"/>:</span>&nbsp;${product.storageInstructions}
                            </div>
                        </c:if>
                        <c:if test="${not empty product.countryOfOrigin}">
                            <c:set value="${fn:replace(product.countryOfOriginIsocode, ' ', '-')}"
                                   var="countryOfOrigin"></c:set>
                            <p><i class="flagicon ${fn:toLowerCase(countryOfOrigin)}"></i>${product.countryOfOrigin}</p>
                        </c:if>
                        <product:productPromotionSection product="${product}"/>

                        <ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">


                            <c:choose>

                                <c:when test="${not empty product.discount}">
                                    <c:set value="display_in" var="display_inline"></c:set>
                                    <p class="price">
                                        <format:fromPrice
                                                priceData="${product.discount.discountPrice}"/>
                                    </p>


                                    <span class="scratched"><format:fromPrice
                                            priceData="${product.discount.price}"/><span
                                            class="line_discount ${display_inline} "></span></span>
                                    <%-- 										<span class="discount_style">(${product.discount.percentage})</span> --%>

                                </c:when>
                                <c:otherwise>
                                    <span class="scratched"></span>
                                    <product:productPricePanel product="${product}"/>

                                </c:otherwise>

                            </c:choose>
                        </ycommerce:testId>

                        <cms:pageSlot position="VariantSelector" var="component" element="div"
                                      class="page-details-variants-select">
                            <cms:component component="${component}" element="div"
                                           class="yComponentWrapper page-details-variants-select-component"/>
                        </cms:pageSlot>
                        <cms:pageSlot position="AddToCart" var="component" element="div"
                                      class="page-details-variants-select">
                            <cms:component component="${component}" element="div"
                                           class="yComponentWrapper page-details-add-to-cart-component"/>
                        </cms:pageSlot>


                        <c:if test="${product.express}">
												<span class="expressdelivery"><img
                                                        src="${fn:escapeXml(themeResourcePath)}/images/express.png"/>
                                                    <span class="expressdeliveryLabel"> <spring:theme
                                                            code="plp.title.express"/></span>
                                                            
												</span>
													<c:url value="/express-info" var="expresslink"></c:url>
													<a href="javascript:;"  class="express_btn"><i class="fa fa-question-circle express_icon" aria-hidden="true"></i></a>
												<span class="express_i hidden">
													<span class="express_arrow_box"><spring:theme code="express.msg"/><br/><a href="${expresslink}" target="_blank" class="expresslink"><spring:theme code="express.link"/></a></span>
												</span>
                        </c:if>
                    </div>
                </div>


            </div>


        </div>
    </div>

</div>
</div>
