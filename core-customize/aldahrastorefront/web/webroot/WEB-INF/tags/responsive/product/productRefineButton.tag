<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="styleClass" required="true" type="java.lang.String" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true"/>
<spring:theme code="search.nav.selectRefinements.title" var="selectRefinementsHtml"/>

<span class="js-show-facets" data-select-refinements-title="${selectRefinementsHtml}">
    <i class="fal fa-filter"></i> <spring:theme code="Filter" text="Filter"/></span>
</span>
