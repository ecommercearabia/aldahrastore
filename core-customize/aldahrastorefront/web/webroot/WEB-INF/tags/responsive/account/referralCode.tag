<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="customer" required="true" type="de.hybris.platform.commercefacades.user.data.CustomerData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>




<spring:htmlEscape defaultHtmlEscape="true" />

<c:url var="termsAndConditionsUrl" value="/termsAndConditions" />

<c:url var="generateReferralCodeUrl" value="/my-account/referral-code/generate" scope="page"/>

<spring:url var="referralCodeURL" value="{contextPath}/my-account/referral-code" htmlEscape="false" >


	<spring:param name="contextPath" value="${request.contextPath}" />
</spring:url>
<div class="cont_box">
<div class="right-side">
<div class="left-side">
<div class="fal fa-gift"></div></div>
	<div class="headline">
	
	<div class="head_myaccount">	
		
		<spring:theme code="myaccount.referralcode.title"/>
		</div>
	</div>
	
	<div class="body">
	
	<input type="hidden" class="textMsg" value="<spring:theme code='msg.referralShare' arguments = '${baseStore.referralCodeNewAppliedRewardAmount}'/>">
	<input type="hidden" class="textMsg2" value="<spring:theme code='msg.referralShareFooter'/>">
		 <div class="myaccount_subtitle referralDiv"> 
		 

<c:choose >
	<c:when test="${empty referralCode}">
	<c:choose>
		<c:when test="${cmsSite.referralGeneratePostType}">
		<form action="${generateReferralCodeUrl}" id="reorderForm" method="POST">
		<br>
	<p><strong><spring:theme code="text.account.referral.info" arguments = '${baseStore.referralCodeNewAppliedRewardAmount}'/></strong></p>
	
<%-- 			<p><spring:theme code="msg.TitleBtnGenerateShare"/></p> --%>
		
			<button type="submit" class="btn btn-primary btn-block re-order" id="reorderButton">
				<spring:theme code="text.account.referralcode.generate"/>
			</button>
		</form>
			</c:when>
			<c:otherwise>
			<form action="${generateReferralCodeUrl}" id="reorderForm" method="GET">
			<br>
	<p><strong><spring:theme code="text.account.referral.info" arguments = '${baseStore.referralCodeNewAppliedRewardAmount}'/></strong></p>
	
<%-- 			<p><spring:theme code="msg.TitleBtnGenerateShare"/></p> --%>
			<button type="submit" class="btn btn-primary btn-block re-order" id="reorderButton">
				<spring:theme code="text.account.referralcode.generate"/>
			</button>
		</form>
			
			</c:otherwise>
		
		</c:choose>
	</c:when>
</c:choose>  
<c:choose >
	<c:when test="${not empty referralCode}">
<span class="ReferralCodeText btn-block text-center">${referralCode.code}</span>
		<div class="referralCode"> <div id="inline-popups">
    <a href="#test-popup" data-effect="mfp-zoom-in"><span class="textshareLinkPopup"><spring:theme code="msg.TitleShareBtn"/></span><i class="fas fa-share-alt"></i></a>

  </div>
  </div>
	</c:when>
</c:choose> 
		 
		 
		 
		 
		 <div id="test-popup" class="white-popup mfp-with-anim mfp-hide">
<div class="row">
<div class="col-xs-12 col-md-6 hidden-sm hidden-xs leftSide" >

<div class="animationPage ">

  <div class="first"><img class="ImgLeft" src="${fn:escapeXml(themeResourcePath)}/images/left.jpg"></div>
</div>

</div>
<div class="col-xs-12 col-md-6 rightSide">
<div class="animationPage">
  <div class="second">
  <img class="ImgwinAndGiveEN" src="${fn:escapeXml(themeResourcePath)}/images/win.png">
  <img class="ImgwinAndGiveAR" src="${fn:escapeXml(themeResourcePath)}/images/win_ar.png">
  </div>
 </div>
<div class="animationPage">
  <div class="third"><p><spring:theme code='msg.referralTitle'/></p></div>
  <div class="fourth"><p><spring:theme code='msg.referralSubTitle'/></p></div>
</div>
<div class="animationPage">
  <div class="fifth">
  <div class="fiftText">
   <div id="textUrl" class="textUrl">
  
  </div>
  
  <div class="tooltip">
<a href="javaScript:;" class="btn copyBtn" >
 
  <i class="fal fa-copy"></i>
   <span class="tooltiptext" id="myTooltip"><spring:theme code='msg.CopyBtn'/></span>
  </a>
</div>
  </div>
  <input type="text"  id="myInput" >
<input type="hidden" class="copyLabelText" value="<spring:theme code='msg.CopyBtn'/>" />
<input type="hidden" class="copyIDLabelText" value="<spring:theme code='msg.CopyIdBtn'/>" />
  
 
    
  </div>
  <div class="sixth">
  
   <div class="socialContent">
   <div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v13.0&appId=700774864653998&autoLogAppEvents=1" nonce="ywRETNx8"></script>
		 <a href="javascript:;" class="fbBtn messenger-wrap" target="_blank" onclick="messengerShare()">
		 <img class="ImgLeft" src="${fn:escapeXml(themeResourcePath)}/images/refer_facebook.png">
		 
		 	</a>
			


<!-- 		 	 <a href="" class="twBtn" target="_blank"> -->
<%-- 				<img class="ImgLeft" src="${fn:escapeXml(themeResourcePath)}/images/refer_tweet.png"> --%>

<!-- 		 </a> -->
		  	  	 <a href="" class="whatsBtn hidden-md hidden-lg"data-action="share/whatsapp/share" target="_blank">
		<img class="ImgLeft" src="${fn:escapeXml(themeResourcePath)}/images/refer_whatsapp.png">
		 </a>
		 
		 <a href="" class="whatsBtnWeb hidden-sm hidden-xs"data-action="share/whatsapp/share" target="_blank">
		<img class="ImgLeft" src="${fn:escapeXml(themeResourcePath)}/images/refer_whatsapp.png">
		 </a>
		 <a href="javaScript:;" class="emailFormIcon">
		<i class="fal fa-envelope"></i>
		 </a>
		 </div>
		 <div class="darkbacks hidden"></div>
		 <div class="InputTagEmail hidden">
		<label class="labelEmailReferral"><spring:theme code='msg.EmailInputLabel'/><input name='EmailTag' placeholder='write Emails' class="tagify" />
		<span class="errorReferralForm hidden"><spring:theme code='msg.EmailInputLabelinValid'/></span>
		 </label>
		 <span class="closeIconEmail"><i class="fal fa-times"></i></span>
		 
		 <div class="conBottomFooter">
		
		 <button class="btn btn-primary SendEmails" type="submit"><spring:theme code='msg.EmailFromSend'/></button>
		
		
		<button class="btn btn-primary cancelBtn"><spring:theme code='msg.EmailFromcancel'/></button>
		 <span class="msgsuccess"><spring:theme code='msg.successEmailSend'/></span>
		 </div>
		 </div>
  
  </div>
  <div class="seven">
  <fmt:formatNumber value="${baseStore.referralCodeNewAppliedRewardAmount}"  var="referralCodeNewAppliedRewardAmount"/>
<fmt:formatNumber value="${referralCode.percentage}"  var="percentage"/>	

  <c:if test="${referralCode.fixedRewardAmount>0}">
  <span class="descriptionReferral"><spring:theme code='msg.EmailFooterTitle' arguments="${referralCode.fixedRewardAmount}"/></span>
  </c:if>

  <span class="termsCon"><a href="${termsAndConditionsUrl}"><spring:theme code='msg.EmailFooterTerms'/></a></span>
  
  </div>
  
  
  
</div>
</div>
</div>
</div>
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		
		 
		 
		 <div class="SharesocialMediaBox">
		 		 <a href="javascript:;" class="fbBtn messenger-wrap" target="_blank" onclick="messengerShare()">
		 <i class="fab fa-facebook-f"></i></a>
		 
<!-- 		 	 <a href="" class="twBtn" target="_blank"> -->
<!-- 		 <i class="fab fa-twitter"></i> -->
<!-- 		 </a> -->
		  	 <a href="" class="whatsBtn hidden-md hidden-lg"data-action="share/whatsapp/share" target="_blank">
		<i class="fab fa-whatsapp"></i>
		 </a>
		 
		 <a href="" class="whatsBtnWeb hidden-sm hidden-xs"data-action="share/whatsapp/share" target="_blank">
		<i class="fab fa-whatsapp"></i>
		 </a>
		 	 
		 
		 </div>
		 </div>
		
	</div>
	
	</div>

</div>
 <script>
window.fbAsyncInit = function() {
    FB.init({
        appId: '700774864653998',
        xfbml: true,
        version: 'v2.9'
    });
    FB.AppEvents.logPageView();
};
function isMobile() {
    const toMatch = [/Android/i, /webOS/i, /iPhone/i, /iPad/i, /iPod/i, /BlackBerry/i, /Windows Phone/i];
    return toMatch.some((toMatchItem) => {
        return navigator.userAgent.match(toMatchItem);
    });
}
function messengerShare() {
	var textCode = $('.ReferralCodeText').text();
	var domain = window.location.hostname;
    url = 'https://' + domain + '/en/register?referralCode=' + textCode;
	url2 = 'https://' + domain;
    if (isMobile()) {
        window.location.href = "fb-messenger://share/?link=" + url;
    } else {
        FB.ui({
            method: 'send',
            link: url,
            redirect_uri: url2
        });
    }
}
</script>