<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="customer" required="true" type="de.hybris.platform.commercefacades.user.data.CustomerData" %>
<spring:url var="personalDetailsURL" value="{contextPath}/my-account/update-profile" htmlEscape="false" >
	<spring:param name="contextPath" value="${request.contextPath}" />
</spring:url>
<div class="cont_box">
<div class="right-side">
<div class="left-side">
<div class="fal fa-user"></div></div>
	<div class="headline">
	
	<div class="head_myaccount">	
		
		<spring:theme code="personal.details"/>
		
		</div>
	</div>
	<div class="body">
	
		<div class="myaccount_title"><spring:theme code="myaccount.name"/></div> <div class="myaccount_subtitle"> ${customer.firstName}&nbsp;${customer.lastName}</div>
		<div class="myaccount_title"><spring:theme code="myaccount.mobile"/></div> <div class="myaccount_subtitle">${customer.mobileNumber}</div>
	
	</div>
	</div>
<a href="${personalDetailsURL}" class="link_box_cont" >	</a>
</div>
