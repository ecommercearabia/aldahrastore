<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="defaultAddress" required="false" type="de.hybris.platform.commercefacades.user.data.AddressData" %>

<div class="cont_box">

<div class="left-side">
	<div class="fal fa-credit-card-front"></div></div>
	<div class="right-side">
	<div class="headline">


	<div class="head_myaccount">
	<spring:theme code="manage.saved.cards"/>
	</div>
	
	
	</div>
	<div class="body">
		
	</div>

</div>
<c:url value="/my-account/manage-saved-cards" var="manageSavedCardsURL" />
<a href="${manageSavedCardsURL}" class="link_box_cont"></a>
</div>
