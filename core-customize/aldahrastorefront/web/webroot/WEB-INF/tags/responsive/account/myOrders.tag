<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="numberOfOrders" required="true" type="java.lang.Integer" %>
<spring:url var="orderHistoryURL" value="{contextPath}/my-account/orders" htmlEscape="false" >
	<spring:param name="contextPath" value="${request.contextPath}" />
</spring:url>

<div class="cont_box"><div class="left-side">
	<div class="fal fa-clock"></div></div>
	<div class="right-side">
	<div class="headline">
	
		<div class="head_myaccount">	
		<spring:theme code="my.orders"/></div>
		
		<div class="order_dis">
		<spring:theme code="number.of.orders" arguments="${numberOfOrders}"/></div>
	</div>
	<div class="body">
		
<!-- sLorem ipsum dolor sit amet, consectetur adipiscing elit, -->
<!--  sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  -->
	</div>
	</div>
<a href="${orderHistoryURL}" class="link_box_cont">	</a>
</div>