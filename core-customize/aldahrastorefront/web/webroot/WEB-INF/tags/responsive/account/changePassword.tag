<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<spring:url var="changePasswordURL" value="{contextPath}/my-account/update-password" htmlEscape="false" >
	<spring:param name="contextPath" value="${request.contextPath}" />
</spring:url>



<div class="cont_box">

<div class="left-side">
	<div class="fal fa-lock-alt"></div></div>
	<div class="right-side">
	<div class="headline">


	<div class="head_myaccount">
	<spring:theme code="change.password"/>
	</div>
	
	
	</div>
	<div class="body">
		
	</div>

</div>
<a href="${changePasswordURL}" class="link_box_cont"></a>
</div>
