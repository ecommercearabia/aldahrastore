<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="cont_box orangeback">

	<div class="left-side">
		<div class="fal fa-credit-card-front"></div>
	</div>
	<div class="right-side">
		<div class="headline">


			<div class="head_myaccount">
				<spring:theme code="loyalty.points" />
			</div>
			<div class="order_dis">
			<c:if test="${not empty loyaltyPoints}">
		<p class="head_myaccount">
			<spring:theme code='text.account.loyaltypoint.availablebalance' />
			<span class="red_color">: ${loyaltyPoints}</span>
		</p>
	</c:if>
			</div>

		</div>
		<div class="body">
		<c:url value="/my-account/loyalty-point" var="loyaltyPointURL" />
	<a href="${loyaltyPointURL}" class="link_box_cont"></a>
	
		</div>

	</div>
	
</div>
