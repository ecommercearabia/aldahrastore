<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sefamElement"
	tagdir="/WEB-INF/tags/responsive/sefamElement"%>
<%@ taglib prefix="otp" tagdir="/WEB-INF/tags/responsive/otp"%>
<spring:htmlEscape defaultHtmlEscape="true" />

<div class="row ">
	<div class="col-md-6">
		<img class="btn-block otpimg"
			src="${fn:escapeXml(themeResourcePath)}/images/OTP_food.png">
	</div>
	<div class="col-md-6">
		<div class="boxotp">

			<c:url var="otpVerifyActionURL" value="${otpVerifyActionURL}" />
			<c:url var="changeNumberActionURL"  value="${otpChangeNumberActionURL}" />
			<c:url var="resendVerifyActionURL"  value="${otpResendVerifyActionURL}" />
<br/>

			<div class="row">
				<div class="col-md-12 text-center">
					<p class="otp_headline">
						<spring:theme code="otp.verify.code.title" />
					</p>
					<form:form method="GET" id="changeNumber" class="otpForm"
						action="${changeNumberActionURL}">
						  <input type="text" value="${otpToken}" name="token" hidden="hidden"><br><br>
						
						<p>
							<spring:theme code="otp.verify.code.verify.description" />&nbsp;${mobileNumber}

							<button type="submit" class="btn btnotpchange" value="Change Number">
								<i class="far fa-edit"></i>
							</button>

						</p>
					</form:form>
				</div>
			</div>
			<form:form method="post" id="otpForm" class="otpForm"
				modelAttribute="otpForm" action="${otpVerifyActionURL}">
										  <input type="text" value="${otpToken}" name="token" hidden="hidden"><br><br>
				
				<div class="row">
					<div class="col-md-12">
						<formElement:formInputBox idKey="otpCode" labelKey="otp.code"
							path="otpCode" inputCSS="form-control" mandatory="true" />
					</div>
					<div class="col-md-12 pull-right">
						<spring:theme code="otp.verify.btn" var="verifyBtnTxt" />
						<input type="submit" class="btn btn-primary btn-block"
							name="verify" value="${verifyBtnTxt}"/>
					</div>
				</div>
			</form:form>

			<form:form method="post" id="resend" class="resend" 
				action="${resendVerifyActionURL}">
										  <input type="text" value="${otpToken}" name="token" hidden="hidden"><br><br>
				
				<div class="row"><br/>
					<div class="col-md-12 text-center">
						<p><spring:theme code="otp.verify.code.not.received"/> <button type="submit" class="btn btnotpchange" ><spring:theme code="otp.verify.code.resend"/></button></p>
					</div>
				</div>
			</form:form>
		</div>
	</div>
</div>
