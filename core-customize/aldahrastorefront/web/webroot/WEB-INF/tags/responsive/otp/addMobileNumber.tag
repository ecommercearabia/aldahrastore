<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="otp" tagdir="/WEB-INF/tags/responsive/otp"%>
<spring:htmlEscape defaultHtmlEscape="true" />

<div class="row ">
	<div class="col-md-6"><img class="btn-block otpimg" src="${fn:escapeXml(themeResourcePath)}/images/OTP_food.png"></div>
	<div class="col-md-6">
<div class="boxotp">
<c:url var="otpAddPhoneNumberURL" value="/otp-login/add-phone-number" />
<form:form method="post" id="otpForm" class="otpForm" modelAttribute="otpForm" action="${otpAddPhoneNumberURL}">
<div class="row">
	<div class="col-md-12 text-center">
	<p class="otp_headline"><spring:theme code="otp.verify.code.title" /></p>
	<p><spring:theme code="otp.verify.code.send.description" /></p>
	</div>

</div>
<div class="row">


	<div class="col-md-12">
	
	<div class="col-md-12">
		<formElement:formInputBox idKey="otpForm.email"
		labelKey="otp.login.email" path="email"
		inputCSS="form-control" mandatory="true" />
	</div>
	
	<formElement:formCountrySelectBoxDefaultEnabled idKey="mobileCountry"
		labelKey="register.mobile.country.number"
		selectCSSClass="form-control countrypicker f16"
		path="mobileCountry" mandatory="true" skipBlank="false"
		skipBlankMessageKey="form.select.empty" items="${mobileCountries}" />
	</div>
	<div class="col-md-12">
		<formElement:formInputBox idKey="mobileNumber"
		labelKey="otp.mobile.number" path="mobileNumber"
		inputCSS="form-control" mandatory="true" />
		
	</div>
	
</div>
<div class="row">
<div class="col-md-6 pull-right">
		<button type="submit" class="btn btn-primary btn-block">
		<spring:theme code="phone.number.send.verify" />
	</button>
</div>
</div>

</form:form>

</div>
</div>
</div>