<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<spring:htmlEscape defaultHtmlEscape="true" />
<c:url value="/" var="link" />
<div class="row ">
	<div class="col-md-12 text-center">
		<h2 class="otp_headline ">
			<spring:theme code="otp.thanks.msg" />
		</h2>
		<br /> <img class="btn-block otpimg text-center"
			src="${fn:escapeXml(themeResourcePath)}/images/OTP_food.png">
	</div>
	<div class="col-md-12 text-center">
		<br />
		<div class="orderBackBtn">
			<a href="${link}" class="btn btn-default"> <spring:theme
					code="otp.start.shopping" />
			</a>
		</div>
	</div>
</div>
<div class="padd">
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-5 col-lg-4 pull-right"></div>
	</div>
</div>