<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="otp" tagdir="/WEB-INF/tags/responsive/otp"%>
<spring:htmlEscape defaultHtmlEscape="true" />
<c:choose>
	<c:when test="${not empty referralCodeData}">
		<otp:referralCodeThankYou/>
	</c:when>

	<c:otherwise>
		<otp:generalThankYou/>
	</c:otherwise>
</c:choose>
