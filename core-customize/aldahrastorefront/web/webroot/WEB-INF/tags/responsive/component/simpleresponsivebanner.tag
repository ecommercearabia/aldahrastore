<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ attribute name="urlLink" required="false" type="java.lang.String"%>
<%@ attribute name="medias" required="false" type="java.util.List" %>
<%@ attribute name="header" required="false" type="java.lang.String"%>
<%@ attribute name="content" required="false" type="java.lang.String"%>
<%@ attribute name="linkTarget" required="false" type="java.lang.String"%>
<%@ attribute name="youtubeId" required="false" type="java.lang.String"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="defult" value=""/>
<c:forEach items="${medias}" var="media">
	<c:choose>
		<c:when test="${empty imagerData}">
			<c:set var="imagerData">"${ycommerce:encodeJSON(media.width)}":"${ycommerce:encodeJSON(media.url)}"</c:set>
		</c:when>
		<c:otherwise>
			<c:set var="defult" value="${media.url}"/>
			<c:set var="imagerData">${imagerData},"${ycommerce:encodeJSON(media.width)}":"${ycommerce:encodeJSON(media.url)}"</c:set>
		</c:otherwise>
	</c:choose>
		<c:set var="img" value="${ycommerce:encodeJSON(media.url)}"/>
	<c:if test="${empty altText}">
		<c:set var="altTextHtml" value="${fn:escapeXml(media.altText)}"/>
	</c:if>
</c:forEach>

<c:url value="${urlLink}" var="simpleResponsiveBannerUrl" />

<c:choose >
	<c:when test="${not empty youtubeId}">
	<c:set var="rand" value="${Math.random() *  Math.random() * 1000000000 + loop.index}"></c:set>
		<fmt:parseNumber var = "i" integerOnly = "true" type = "number" value = "${rand}" />
		<div class="fack">
			<img src="${defult}" class="js-responsive-image" data-media='${fn:escapeXml(imagerDataJson)}'  alt='${altTextHtml}' style="">
		<div class="player class" style="    position: absolute; z-index:100;
    top: 0;
    overflow: hidden;
    bottom: 0;
    left: 0;
    right: 0;"></div>
		</div>
	</c:when>
		
		
		<c:otherwise>
	<div class="simple-banner banner__component--responsive">
	<c:set var="imagerDataJson" value="{${imagerData}}"/>
	<c:choose>
		<c:when test="${empty simpleResponsiveBannerUrl || simpleResponsiveBannerUrl eq '#'}">
			<img src="${defult}" class="js-responsive-image" data-media='${fn:escapeXml(imagerDataJson)}' alt='${altTextHtml}'  style="">
		</c:when>
		<c:otherwise>
			<a href="${fn:escapeXml(simpleResponsiveBannerUrl)}">
				<img src="${defult}" class="js-responsive-image" data-media='${fn:escapeXml(imagerDataJson)}'  alt='${altTextHtml}' style="">
			</a>
		</c:otherwise>
	</c:choose>
</div>
</c:otherwise>


</c:choose>

