 <%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ taglib prefix="cms" tagdir="/WEB-INF/tags/responsive/template/cms" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>

<template:javaScriptVariables/>

<c:set var="commonResourcePathHtml" value="${fn:escapeXml(commonResourcePath)}"/>
<c:choose>
	<c:when test="${wro4jEnabled}">
	
	<script src="https://www.youtube.com/iframe_api" ></script>
	
		<script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDnqcqkmG9MqNZR6QKsB8V-mg-NNa71t8&callback=initAutocomplete&libraries=places&v=weekly"
      defer
    ></script>
	  	<script src="${fn:escapeXml(contextPath)}/wro/all_responsive.js"></script>
	  	<script src="${fn:escapeXml(contextPath)}/wro/addons_responsive.js"></script>
	</c:when>
	<c:otherwise>
		<%-- jquery --%>
		<script src="${commonResourcePathHtml}/js/jquery-3.2.1.min.js"></script>
		    <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/8.6/highlight.min.js"></script>

		
		<%-- bootstrap --%>
	<!-- 	<script type="text/javascript" src="${commonResourcePath}/bootstrap/dist/js/bootstrap.min.js"></script>
		 -->
<!-- 	 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDnqcqkmG9MqNZR6QKsB8V-mg-NNa71t8&libraries=places"></script>  -->
		
		
		<script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDnqcqkmG9MqNZR6QKsB8V-mg-NNa71t8&callback=initAutocomplete&libraries=places&v=weekly"
      defer
    ></script>

		
		<%-- plugins --%>
			<script src="${commonResourcePathHtml}/js/enquire.min.js"></script>
		<script src="${commonResourcePathHtml}/js/Imager.min.js"></script>
		<script src="${commonResourcePathHtml}/js/purify.min.js"></script>
		<script src="${commonResourcePathHtml}/js/jquery.blockUI-2.66.min.js"></script>
		<script src="${commonResourcePathHtml}/js/jquery.colorbox-min.js"></script>
		<script src="${commonResourcePathHtml}/js/jquery.form.min.js"></script>
		<script src="${commonResourcePathHtml}/js/jquery.hoverIntent.min.js"></script>
		<script src="${commonResourcePathHtml}/js/jquery.pstrength.custom-1.2.0.min.js"></script>
		<script src="${commonResourcePathHtml}/js/jquery.syncheight.custom.min.js"></script>
		<script src="${commonResourcePathHtml}/js/jquery.tabs.custom.min.js"></script>
		<script src="${commonResourcePathHtml}/js/jquery-ui-1.12.1.min.js"></script>
		<script src="${commonResourcePathHtml}/js/jquery.zoom.custom.min.js"></script>
	
		<script src="${commonResourcePathHtml}/js/owl.carousel.custom.js"></script>		
		<script src="${commonResourcePathHtml}/js/jquery.tmpl-1.0.0pre.min.js"></script>
		<script src="${commonResourcePathHtml}/js/jquery.currencies.min.js"></script>
		<script src="${commonResourcePathHtml}/js/jquery.waitforimages.min.js"></script>
		<script src="${commonResourcePathHtml}/js/jquery.slideviewer.custom.1.2.min.js"></script>
			<script src="${commonResourcePathHtml}/js/datepicker.min.js"></script>
			 <script src="${commonResourcePathHtml}/js/jquery.sticky.min.js"></script> 
			  <script src="${commonResourcePathHtml}/js/anitabs.min.js"></script> 
			


  		<%-- Custom ACC JS --%>
	<script src="${commonResourcePathHtml}/js/bootstrap-select.min.js"></script>
<%-- 		<script src="${commonResourcePathHtml}/js/acc.address.min.js"></script> --%>
<%-- 		<script src="${commonResourcePathHtml}/js/acc.autocomplete.min.js"></script> --%>
<%-- 		<script src="${commonResourcePathHtml}/js/acc.carousel.min.js"></script> --%>
<%-- 		<script src="${commonResourcePathHtml}/js/acc.cart.min.js"></script> --%>
<%-- 		<script src="${commonResourcePathHtml}/js/acc.cartitem.js"></script> --%>
<%-- 		<script src="${commonResourcePathHtml}/js/acc.checkout.min.js"></script> --%>
		
		
<%-- 		<script src="${commonResourcePathHtml}/js/acc.colorbox.min.js"></script> --%>
<%-- 		<script src="${commonResourcePathHtml}/js/acc.common.min.js"></script> --%>
<%-- 		<script src="${commonResourcePathHtml}/js/alljs.min.js"></script> --%>
<%-- 		<script src="${commonResourcePathHtml}/js/acc.global.min.js"></script> --%>
		
<%-- 		<script src="${commonResourcePathHtml}/js/acc.imagegallery.min.js"></script> --%>
		
<%-- 		<script src="${commonResourcePathHtml}/js/acc.minicart.min.js"></script> --%>
<%-- 		<script src="${commonResourcePathHtml}/js/acc.navigation.min.js"></script> --%>
		
		
<%-- 		<script src="${commonResourcePathHtml}/js/acc.payment.min.js"></script> --%>
		
<%-- 		<script src="${commonResourcePathHtml}/js/acc.pickupinstore.min.js"></script> --%>
<script src="${commonResourcePathHtml}/js/_autoload.min.js"></script>
<script src="${commonResourcePathHtml}/js/merge_min.js"></script>
		<script src="${commonResourcePathHtml}/js/acc.product.min.js"></script>
		<script src="${commonResourcePathHtml}/js/flipdown.js"></script>

		 		<script src="${commonResourcePathHtml}/js/acc.address.js"></script>
<%-- 		<script src="${commonResourcePathHtml}/js/acc.productDetail.min.js"></script> --%>
<%-- 		<script src="${commonResourcePathHtml}/js/acc.quickview.min.js"></script> --%>
<%-- 		<script src="${commonResourcePathHtml}/js/acc.ratingstars.min.js"></script> --%>
		
<%-- 		<script src="${commonResourcePathHtml}/js/acc.refinements.min.js"></script> --%>
		
<%-- 		<script src="${commonResourcePathHtml}/js/acc.silentorderpost.min.js"></script> --%>
<%-- 		<script src="${commonResourcePathHtml}/js/acc.tabs.min.js"></script> --%>
<%-- 		<script src="${commonResourcePathHtml}/js/acc.termsandconditions.min.js"></script> --%>
		
<%-- 		<script src="${commonResourcePathHtml}/js/acc.storefinder.js"></script> --%>
		
<%-- 		<script src="${commonResourcePathHtml}/js/acc.productorderform.min.js"></script> --%>
<%-- 		<script src="${commonResourcePathHtml}/js/acc.savedcarts.js"></script> --%>
<%-- 		<script src="${commonResourcePathHtml}/js/acc.multidgrid.min.js"></script> --%>
<%-- 		<script src="${commonResourcePathHtml}/js/acc.quickorder.min.js"></script> --%>
<%-- 		<script src="${commonResourcePathHtml}/js/acc.quote.min.js"></script> --%>
<%-- 		<script src="${commonResourcePathHtml}/js/acc.consent.min.js"></script> --%>
		
<%-- 		<script src="${commonResourcePathHtml}/js/acc.closeaccount.min.js"></script>							 --%>
<%-- 		<script src="${commonResourcePathHtml}/js/acc.csv-import.min.js"></script> --%>
		
<%-- 		<script src="${commonResourcePathHtml}/js/locationpicker.jquery.min.js"></script>  --%>


<script src="${commonResourcePathHtml}/js/magiczoomplus.js"></script>
 <script src="${commonResourcePathHtml}/js/acc.mywishlist.min.js"></script> 
  <script src="${commonResourcePathHtml}/js/jquery.magnific-popup.js"></script> 
  <script src="${commonResourcePathHtml}/js/jQuery.tagify.min.js"></script> 
		
		
		<script src="${commonResourcePathHtml}/js/infinite-scroll.pkgd.min.js"></script>
		<script src="${commonResourcePathHtml}/js/tweenmax.js"></script>
		<script src="${commonResourcePathHtml}/js/acc.consent.js"></script>

		<script src="${commonResourcePathHtml}/js/js.js"></script>
		<%-- Cms Action JavaScript files --%>
		<c:forEach items="${cmsActionsJsFiles}" var="actionJsFile">
		    <script src="${commonResourcePathHtml}/js/cms/${fn:escapeXml(actionJsFile)}"></script>
		</c:forEach>
		
		<%-- AddOn JavaScript files --%>
		<c:forEach items="${addOnJavaScriptPaths}" var="addOnJavaScript">
		    <script src="${fn:escapeXml(addOnJavaScript)}"></script>
		</c:forEach>
		
	</c:otherwise>
</c:choose>


<cms:previewJS cmsPageRequestContextData="${cmsPageRequestContextData}" />
