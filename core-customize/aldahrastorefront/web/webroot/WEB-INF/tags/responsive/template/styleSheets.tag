<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ taglib prefix="cms" tagdir="/WEB-INF/tags/responsive/template/cms" %>

<link rel="preload" as="style" onload="this.rel = 'stylesheet'" type="text/css"
      href="//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic"/>
<link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;700;900&display=swap" rel="preload"
      as="style" onload="this.rel = 'stylesheet'">

<c:choose>

    <c:when test="${not wro4jEnabled}">


        <c:choose>
            <c:when test="${fn:escapeXml(currentLanguage.isocode) eq 'ar'}">
                <link rel="stylesheet" type="text/css" media="all"
                      href="${fn:escapeXml(themeResourcePath)}/css/rtlstyle.css"/>

            </c:when>
            <c:otherwise>
                <link rel="stylesheet" type="text/css" media="all"
                      href="${fn:escapeXml(contextPath)}/wro/${fn:escapeXml(themeName)}_responsive.css"/>

            </c:otherwise>
        </c:choose>

        <link rel="stylesheet" type="text/css" media="all"
              href="${fn:escapeXml(contextPath)}/wro/addons_responsive.css"/>
        <%-- 		<link rel="stylesheet" type="text/css" media="all" href="${fn:escapeXml(contextPath)}/wro/all_responsive.css" /> --%>
        <%-- 		<link rel="stylesheet" type="text/css" media="all" href="${fn:escapeXml(contextPath)}/wro/${fn:escapeXml(themeName)}_responsive.css" /> --%>
        <%-- 		<link rel="stylesheet" type="text/css" media="all" href="${fn:escapeXml(contextPath)}/wro/addons_responsive.css" /> --%>
    </c:when>
    <c:otherwise>
        <%-- Theme CSS files --%>

        <c:choose>
            <c:when test="${fn:escapeXml(currentLanguage.isocode) eq 'ar'}">

                <link rel="stylesheet" type="text/css" media="all"
                      href="${fn:escapeXml(themeResourcePath)}/css/rtlstyle.css"/>
            </c:when>
            <c:otherwise>

                <link rel="stylesheet" type="text/css" media="all"
                      href="${fn:escapeXml(themeResourcePath)}/css/style.css"/>


            </c:otherwise>
        </c:choose>


        <%--  AddOn Common CSS files --%>

    </c:otherwise>
</c:choose>

<%--  AddOn Theme CSS files --%>
<c:forEach items="${addOnThemeCssPaths}" var="addOnThemeCss">
    <link rel="preload" as="style" onload="this.rel = 'stylesheet'" type="text/css" media="all"
          href="${fn:escapeXml(addOnThemeCss)}"/>
</c:forEach>

<cms:previewCSS cmsPageRequestContextData="${cmsPageRequestContextData}"/>
