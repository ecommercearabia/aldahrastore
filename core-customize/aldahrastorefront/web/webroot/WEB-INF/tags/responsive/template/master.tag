<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ attribute name="pageTitle" required="false" rtexprvalue="true" %>
<%@ attribute name="metaDescription" required="false" %>
<%@ attribute name="metaKeywords" required="false" %>
<%@ attribute name="pageCss" required="false" fragment="true" %>
<%@ attribute name="pageScripts" required="false" fragment="true" %>
<%@ attribute name="galleryImages" required="false" type="java.util.List" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="analytics" tagdir="/WEB-INF/tags/shared/analytics" %>
<%@ taglib prefix="addonScripts" tagdir="/WEB-INF/tags/responsive/common/header" %>
<%@ taglib prefix="generatedVariables" tagdir="/WEB-INF/tags/shared/variables" %>
<%@ taglib prefix="debug" tagdir="/WEB-INF/tags/shared/debug" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="htmlmeta" uri="http://hybris.com/tld/htmlmeta" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="googletagmanager" tagdir="/WEB-INF/tags/addons/aldahragoogletagmanager/shared" %>

<spring:htmlEscape defaultHtmlEscape="true"/>

<!DOCTYPE html>
<html lang="${fn:escapeXml(currentLanguage.isocode)}">
<head prefix=
              "og: http://ogp.me/ns#
    fb: http://ogp.me/ns/fb#
    product: http://ogp.me/ns/product#">

    <title>
        ${not empty pageTitle ? pageTitle : not empty cmsPage.title ? fn:escapeXml(cmsPage.title) : 'Accelerator Title'}
    </title>
    <c:if test="${not empty cmsSite.headerMetaTagScriptTop}">
        ${cmsSite.headerMetaTagScriptTop}
    </c:if>
    <link rel="alternate" href="https://www.foodcrowd.com/en" hreflang="en"/>
    <link rel="alternate" href="https://www.foodcrowd.com/ar" hreflang="ar"/>
    <%-- Meta Content --%>
    <c:choose>
        <c:when test="${cmsPage.uid eq 'homepage'}">
            <c:url value="https://foodcrowd.com/${fn:escapeXml(currentLanguage.isocode)}" var="pageUrl"/>
        </c:when>
        <c:otherwise>
            <c:url value="https://foodcrowd.com/${fn:escapeXml(currentLanguage.isocode)}${requestScope['javax.servlet.forward.servlet_path']}" var="pageUrl"/>
        </c:otherwise>
    </c:choose>


    <link rel="canonical" href="${pageUrl}">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5">
    <meta name="format-detection" content="telephone=no">
    <%-- Additional meta tags --%>
    <htmlmeta:meta items="${metatags}"/>
    <c:if test="${not empty cmsPage.uid &&  (cmsPage.uid eq 'productDetails')}">

    </c:if>
    <c:choose>
        <c:when test="${not empty cmsPage.uid &&  (cmsPage.uid eq 'productDetails')}">


            <meta property="og:type" content="product">
            <meta property="og:title" content="${product.name}"/>
            <c:url value="https://www.foodcrowd.com${requestScope['javax.servlet.forward.servlet_path']}"
                   var="productUrl"/>
            <meta property="og:url" content="${productUrl}"/>
            <c:forEach items="${galleryImages}" var="container" varStatus="varStatus" end="0" begin="0" step="1">
                <meta property="og:image" content="${fn:escapeXml(container.zoom.url)}"/>
            </c:forEach>
            <meta property="og:image:width" content="1200">
            <meta property="og:image:height" content="627">
            <meta property="og:description" content="${product.summary}">
            <meta property="product:price.amount"
                  content="${fn:escapeXml(product.price.value)}"/>
            <meta property="product:price.currency" content="AED">


        </c:when>
        <c:otherwise>


            <meta property="og:type" content="website">
            <meta property="og:title" content="${pageTitle}"/>
            <c:url value="https://www.foodcrowd.com${requestScope['javax.servlet.forward.servlet_path']}"
                   var="pageUrl"/>
            <meta property="og:url" content="${pageUrl}"/>


            <meta property="og:image"
                  content="https://www.foodcrowd.com${fn:escapeXml(themeResourcePath)}/images/share_img.png"/>
        </c:otherwise>
    </c:choose>


    <%-- Favourite Icon --%>
    <spring:theme code="img.favIcon" text="/" var="favIconPath"/>

    <c:choose>
        <%-- if empty webroot, skip originalContextPath, simply use favIconPath --%>
        <c:when test="${fn:length(originalContextPath) eq 1}">
            <link rel="shortcut icon" type="image/x-icon" media="all" href="${favIconPath}"/>
        </c:when>
        <c:otherwise>
            <link rel="shortcut icon" type="image/x-icon" media="all" href="${originalContextPath}${favIconPath}"/>
        </c:otherwise>
    </c:choose>
    <c:if test="${not empty gtmContainerId}">
        <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                '//www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', '${gtmContainerId}');</script>
    </c:if>
    <c:if test="${not empty cmsSite.googleVerificationCode}">
        <meta name="google-site-verification" content="${cmsSite.googleVerificationCode}"/>
    </c:if>
    <%-- CSS Files Are Loaded First as they can be downloaded in parallel --%>
    <template:styleSheets/>

    <%-- Inject any additional CSS required by the page --%>
    <jsp:invoke fragment="pageCss"/>
    <analytics:analytics/>
    <generatedVariables:generatedVariables/>
    <c:if test="${not empty cmsSite.headerMetaTagScriptBottom}">
        ${cmsSite.headerMetaTagScriptBottom}
    </c:if>
</head>

<body class="${pageBodyCssClasses} ${cmsPageRequestContextData.liveEdit ? ' yCmsLiveEdit' : ''} language-${fn:escapeXml(currentLanguage.isocode)}">

<c:if test="${not empty cmsSite.bodyMetaTagScriptTop}">
    ${cmsSite.bodyMetaTagScriptTop}
</c:if>

<img src="${fn:escapeXml(themeResourcePath)}/images/share_img.png" class="hidden">
<googletagmanager:googleTagManager/>

<%-- Inject the page body here --%>
<jsp:doBody/>


<form name="accessiblityForm">
    <input type="hidden" id="accesibility_refreshScreenReaderBufferField"
           name="accesibility_refreshScreenReaderBufferField" value=""/>
</form>
<div id="ariaStatusMsg" class="skip" role="status" aria-relevant="text" aria-live="polite"></div>

<%-- Load JavaScript required by the site --%>
<template:javaScript/>

<%-- Inject any additional JavaScript required by the page --%>
<jsp:invoke fragment="pageScripts"/>

<%-- Inject CMS Components from addons using the placeholder slot--%>
<addonScripts:addonScripts/>

<c:if test="${not empty cmsSite.bodyMetaTagScriptBottom}">
    ${cmsSite.bodyMetaTagScriptBottom}
</c:if>

</body>

<debug:debugFooter/>

</html>
