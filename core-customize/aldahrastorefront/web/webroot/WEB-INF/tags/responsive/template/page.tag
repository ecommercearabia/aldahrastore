<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true"%>
<%@ attribute name="pageTitle" required="false" rtexprvalue="true"%>
<%@ attribute name="pageCss" required="false" fragment="true"%>
<%@ attribute name="pageScripts" required="false" fragment="true"%>
<%@ attribute name="hideHeaderLinks" required="false"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/responsive/common/header"%>
<%@ taglib prefix="footer" tagdir="/WEB-INF/tags/responsive/common/footer"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<spring:htmlEscape defaultHtmlEscape="true" />

<template:master pageTitle="${pageTitle}">

	<jsp:attribute name="pageCss">
		<jsp:invoke fragment="pageCss" />
	</jsp:attribute>

	<jsp:attribute name="pageScripts">
		<jsp:invoke fragment="pageScripts" />
	</jsp:attribute>

	<jsp:body>
		
		<main data-currency-iso-code="${fn:escapeXml(currentCurrency.isocode)}">
			<spring:theme code="text.skipToContent" var="skipToContent" />
			<a href="#skip-to-content" class="skiptocontent" data-role="none">${fn:escapeXml(skipToContent)}</a>
			<spring:theme code="text.skipToNavigation" var="skipToNavigation" />
			<a href="#skiptonavigation" class="skiptonavigation" data-role="none">${fn:escapeXml(skipToNavigation)}</a>


			<header:header hideHeaderLinks="${hideHeaderLinks}" />


			<div id="addedtocartno" class="hidden">
	
</div>
			
			<a id="skip-to-content"></a>
		
			<div class="main__inner-wrapper">
				<common:globalMessages />
				<cart:cartRestoration />
				<jsp:doBody />
			</div>
<product:addToCartTitle/>
<input type="hidden" id="wishlistpk" value=""/>
<div class="darkback_confirm"></div>
<div class="confirm_log">
			<div class="header"> <div class="title"><spring:theme code="text_Added_to_cart" text="Added to cart" /></div> <span class="close_confirm cen_confirm"><i class="fal fa-times"></i></span></div>
			<div class="cont_confirm_cart">
			<div class="cont_item">
			<spring:theme code="cont_item_confirm" text="Are you sure to add this item in your cart?" />
			</div>
			<div class="cont_items">
			<spring:theme code="cont_items_confirm" text="Are you sure to add this items in your cart?" />
			</div>
			 </div>
			<div class="row section_button">
			<div class="col-xs-6">
			<button class="btn btn-primary btn-block comp_confirm"><spring:theme code="text_Yes" text="Yes" /></button>
			</div>
			<div class="col-xs-6 ">
			<button class="btn btn-default btn-block cen_confirm"><spring:theme code="text_No" text="No" /></button>
			</div>
			</div>
			</div>
			<footer:footer />
			<c:url var="link" value="/cart/addQuickOrder"/>
			<input id="addcarturlid" value="${link}" type="hidden" />
			<div class="hidden">
			
			</div>
			<c:if test="${cmsSite.popUpEnable}" >
			<div class="cont_popup hidden">
			<span class="close_popup">
			<i class="far fa-times"></i>
			</span>
			<div class="main_popup">
			<div id="video-placeholder"></div>
			</div>
			</div>
			<div class="overlay_popup hidden"></div>
			
			 <input id="youtubePopUpId" value="${cmsSite.youtubePopUpId}" type="hidden" />
 <input id="popUpEnable" value="${cmsSite.popUpEnable}" type="hidden" />
  <input id="popUpPerSession" value="${cmsSite.popUpPerSession}" type="hidden" />
			</c:if>


		</main>

	</jsp:body>

</template:master>
