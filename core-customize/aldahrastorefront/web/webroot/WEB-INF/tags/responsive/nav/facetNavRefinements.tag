<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="pageData" required="true" type="de.hybris.platform.commerceservices.search.facetdata.FacetSearchPageData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>

<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


  

                            <div class="col-md-2 pull-left hidden-sm hidden-xs">
                                <label class="control-label hidden " for="sortForm${top ? '1' : '2'}">
                                    <spring:theme code="${themeMsgKey}.sortTitle"/>
                                </label>

                                <form id="sortForm${top ? '1' : '2'}" name="sortForm${top ? '1' : '2'}" method="get"
                                      action="#">
                                    <select id="sortOptions${top ? '1' : '2'}" name="sort" class="form-control">
                                        <c:if test="${not empty themeMsgKey}"><option disabled><spring:theme code="${themeMsgKey}.sortTitle"/></option></c:if>
                                        <c:forEach items="${searchPageData.sorts}" var="sort">
                                            <option value="${fn:escapeXml(sort.code)}" ${sort.selected? 'selected="selected"' : ''}>
                                                <c:choose>
                                                    <c:when test="${not empty sort.name}">
                                                        ${fn:escapeXml(sort.name)}
                                                    </c:when>
                                                    <c:otherwise>
                                                        <spring:theme code="${themeMsgKey}.sort.${sort.code}"/>
                                                    </c:otherwise>
                                                </c:choose>
                                            </option>
                                        </c:forEach>
                                    </select>
                                    <c:catch var="errorException">
                                        <spring:eval expression="searchPageData.currentQuery.query"
                                                     var="dummyVar"/><%-- This will throw an exception is it is not supported --%>
                                                     <!-- searchPageData.currentQuery.query.value is html output encoded in the backend -->
                                        <input type="hidden" name="q" value="${searchPageData.currentQuery.query.value}"/>
                                    </c:catch>
										
                                    <c:if test="${supportShowAll}">
                                        <ycommerce:testId code="searchResults_showAll_link">
                                            <input type="hidden" name="show" value="Page"/>
                                        </ycommerce:testId>
                                    </c:if>
                                    <c:if test="${supportShowPaged}">
                                        <ycommerce:testId code="searchResults_showPage_link">
                                            <input type="hidden" name="show" value="All"/>
                                        </ycommerce:testId>
                                    </c:if>
                                    <c:if test="${not empty additionalParams}">
                                        <c:forEach items="${additionalParams}" var="entry">
                                            <input type="hidden" name="${fn:escapeXml(entry.key)}" value="${fn:escapeXml(entry.value)}"/>
                                        </c:forEach>
                                    </c:if>
                                </form>
                            </div>
                         
                            
                
<c:forEach items="${pageData.facets}" var="facet">
	<c:choose>
		<c:when test="${facet.code eq 'availableInStores'}">
			<nav:facetNavRefinementStoresFacet facetData="${facet}" userLocation="${userLocation}"/>
		</c:when>
		<c:otherwise>
			<nav:facetNavRefinementFacet facetData="${facet}"/>
		</c:otherwise>
	</c:choose>
</c:forEach>            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                       