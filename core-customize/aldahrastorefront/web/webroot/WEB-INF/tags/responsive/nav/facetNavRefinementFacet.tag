<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="facetData" required="true" type="de.hybris.platform.commerceservices.search.facetdata.FacetData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true" />
<c:set var="facetDatacode" value="${fn:replace(fn:replace(fn:replace(facetData.code,' ', ''), '&', ''),',','')}" />

					
					
					
		<c:choose>
						<c:when test="${facetDatacode eq 'GlutenFree' || facetDatacode eq 'Vegan' || facetDatacode eq 'Organic'}">

<c:if test="${not empty facetData.values}">
<ycommerce:testId code="facetNav_title_${facetData.name}">
	<div class="facet js-facet pull-right toggle_facet">
	
		<div class="facet__name js-facet-name hidden">
			<span class="glyphicon facet__arrow"></span>
			<spring:theme code="search.nav.facetTitle" arguments="${facetData.name}"/>
		</div>


		<div class="facet__values js-facet-values js-facet-form">

			<c:if test="${not empty facetData.topValues}">

				
				
				
				
				<ul class="facet__list js-facet-list js-facet-top-values">
					<c:forEach items="${facetData.topValues}" var="facetValue">
						
						
						<li>
						
							<c:if test="${facetData.multiSelect}">
							
								<form action="#" method="get">
								
								<!-- facetValue.query.query.value and searchPageData.freeTextSearch are html output encoded in the backend -->
									<input type="hidden" name="q" value="${facetValue.query.query.value}"/>
									<input type="hidden" name="text" value="${searchPageData.freeTextSearch}"/>
									<label>
										<input class="facet__list__checkbox" type="checkbox" ${facetValue.selected ? 'checked="checked"' : ''} class="facet-checkbox" />
										<span class="facet__list__label">
											<span class="facet__list__mark"></span>
											<span class="facet__list__text">
												${fn:escapeXml(facetValue.name)}
												<ycommerce:testId code="facetNav_count">
													<span class="facet__value__count"><spring:theme code="search.nav.facetValueCount" arguments="${facetValue.count}"/></span>
												</ycommerce:testId>
											</span>
										</span>
									</label>
								</form>
							</c:if>
							<c:if test="${not facetData.multiSelect}">
							
								<c:url value="${facetValue.query.url}" var="facetValueQueryUrl"/>
								<span class="facet__text">
								<!-- searchPageData.freeTextSearch is html output encoded in the backend -->
									<a href="${fn:escapeXml(facetValueQueryUrl)}&amp;text=${searchPageData.freeTextSearch}">${fn:escapeXml(facetValue.name)}</a>&nbsp;
									<ycommerce:testId code="facetNav_count">
										<span class="facet__value__count"><spring:theme code="search.nav.facetValueCount" arguments="${facetValue.count}"/></span>
									</ycommerce:testId>
								</span>
							</c:if>
						</li>
					</c:forEach>
				</ul>
			</c:if>
			
<!--workspace -->

			<ul class="facet-${facetDatacode} facet__list js-facet-list <c:if test="${not empty facetData.topValues}">facet__list--hidden js-facet-list-hidden</c:if>">
			 <div class="checkbox-group-1">
				
				<c:forEach items="${facetData.values}" var="facetValue">
					<c:if test="${fn:escapeXml(facetValue.name) eq 'true'}">
					<li>
						<c:if test="${facetData.multiSelect}">
							<ycommerce:testId code="facetNav_selectForm">
							<form action="#" method="get"class="${facetDatacode}">
							<!-- facetValue.query.query.value and searchPageData.freeTextSearch are html output encoded in the backend -->
								<input type="hidden" name="q" value="${facetValue.query.query.value}"/>
								<input type="hidden" name="text" value="${searchPageData.freeTextSearch}"/>
								 <div class="toggle_product_list">
								<div class="toggle-button">

                                           
                                                    <input type="checkbox" id="${facetDatacode}" ${facetValue.selected ? 'checked' : ''}  class="facet__list__checkbox js-facet-checkbox toggle-button__input" />
                                                    <label for="${facetDatacode}">
                                                     <i class="far fa-leaf"></i><span class="label_organic"> 
                                                     <c:if test="${facetDatacode eq 'Organic'}">
                                                     <spring:theme code="plp.organic.title" text="Organic"/>
                                                     </c:if>
                                                       <c:if test="${facetDatacode eq 'Vegan'}">
                                                     <spring:theme code="plp.Vegan.title" text="Vegan"/>
                                                     </c:if>
                                                      <c:if test="${facetDatacode eq 'GlutenFree'}">
                                                     <spring:theme code="plp.GlutenFree.title" text="GlutenFree"/>
                                                     </c:if>
                                                     </span>
                                                        <div class="toggle-button__switch is-checked"></div>
                                                    </label>
                                                

                                        </div>
								
								</div>
								
								
								

							</form>
							</ycommerce:testId>
						</c:if>
						<c:if test="${not facetData.multiSelect}">
							<c:url value="${facetValue.query.url}" var="facetValueQueryUrl"/>
							<span class="facet__text">
								<a href="${fn:escapeXml(facetValueQueryUrl)}">${fn:escapeXml(facetValue.name)}</a>
								<ycommerce:testId code="facetNav_count">
									<span class="facet__value__count"><spring:theme code="search.nav.facetValueCount" arguments="${facetValue.count}"/></span>
								</ycommerce:testId>
							</span>
						</c:if>
					</li>
					</c:if>
				</c:forEach>
				</div>
			</ul>

			<c:if test="${not empty facetData.topValues}">
			
				<span class="facet__values__more js-more-facet-values">
					<a href="#" class="js-more-facet-values-link" ><spring:theme code="search.nav.facetShowMore_${facetData.code}" /></a>
				</span>
				<span class="facet__values__less js-less-facet-values">
					<a href="#" class="js-less-facet-values-link"><spring:theme code="search.nav.facetShowLess_${facetData.code}" /></a>
				</span>
			</c:if>
		</div>
	</div>
</ycommerce:testId>
</c:if>

		
		
		
		</c:when>
		<c:otherwise>
		<div class="facet-nav ${facetDatacode} col-md-2 hidden-sm hidden-xs">
	<c:set value="" var="multiselect" />
	<c:set value="" var="data-icon" />
	
<c:if test="${facetData.multiSelect}">
				<c:set value="multiple" var="multiselect" />
				<c:set value="" var="data-icon" />
				
				</c:if>
<select class="facet-${facetDatacode}" ${multiselect}  title="<spring:theme code='search.nav.facetTitle' arguments='${facetData.name}'/>">

			 	<c:forEach items="${facetData.values}" var="facetValue">
					<c:choose>
						<c:when test="${facetData.code != 'category'}">
			
							<option value="${facetValue.query.query.value}">${fn:escapeXml(facetValue.name)}</option>
						
							<c:if test="${facetData.multiSelect}">
							
							</c:if>
							<c:if test="${not facetData.multiSelect}"></c:if>
							
						</c:when>
						<c:otherwise>
							<c:if test="${facetValue.code != '1'}">
								<c:choose>
									<c:when test="${not empty currentCategory}">
										<c:if test="${facetValue.code != currentCategory.code}">
											<option value="${facetValue.query.query.value}">${fn:escapeXml(facetValue.name)}</option>
						
											<c:if test="${facetData.multiSelect}">
								
											</c:if>
											<c:if test="${not facetData.multiSelect}"></c:if>
										</c:if>
									</c:when>
									<c:otherwise>
										<option value="${facetValue.query.query.value}">${fn:escapeXml(facetValue.name)}</option>
						
										<c:if test="${facetData.multiSelect}">
								
										</c:if>
										<c:if test="${not facetData.multiSelect}"></c:if>
									</c:otherwise>
								</c:choose>
							</c:if>
						</c:otherwise>	
					</c:choose>
				</c:forEach>
					</select>
					</div>
		</c:otherwise>
					</c:choose>
					
					
					
					
					
					<div class="hidden-md hidden-lg">
<c:if test="${not empty facetData.values}">
<ycommerce:testId code="facetNav_title_${facetData.name}">
	<div class="facet js-facet">
	
		<div class="facet__name js-facet-name">
			<span class="glyphicon facet__arrow"></span>
			<spring:theme code="search.nav.facetTitle" arguments="${facetData.name}"/>
		</div>


		<div class="facet__values js-facet-values js-facet-form">

			<c:if test="${not empty facetData.topValues}">
				
				
				
				
				
				<ul class="facet__list js-facet-list js-facet-top-values">
					<c:forEach items="${facetData.topValues}" var="facetValue">
						<li>
						
							<c:if test="${facetData.multiSelect}">
								<form action="#" method="get" id="">
								<!-- facetValue.query.query.value and searchPageData.freeTextSearch are html output encoded in the backend -->
									<input type="hidden" name="q" value="${facetValue.query.query.value}"/>
									<input type="hidden" name="text" value="${searchPageData.freeTextSearch}"/>
									<label>
										<input class="facet__list__checkbox" type="checkbox" ${facetValue.selected ? 'checked="checked"' : ''} class="facet-checkbox" />
										<span class="facet__list__label">
											<span class="facet__list__mark"></span>
											<span class="facet__list__text">
												${fn:escapeXml(facetValue.name)}
												<ycommerce:testId code="facetNav_count">
													<span class="facet__value__count"><spring:theme code="search.nav.facetValueCount" arguments="${facetValue.count}"/></span>
												</ycommerce:testId>
											</span>
										</span>
									</label>
								</form>
							</c:if>
							<c:if test="${not facetData.multiSelect}">
							
								<c:url value="${facetValue.query.url}" var="facetValueQueryUrl"/>
								<span class="facet__text">
								<!-- searchPageData.freeTextSearch is html output encoded in the backend -->
									<a href="${fn:escapeXml(facetValueQueryUrl)}&amp;text=${searchPageData.freeTextSearch}">${fn:escapeXml(facetValue.name)}</a>&nbsp;
									<ycommerce:testId code="facetNav_count">
										<span class="facet__value__count"><spring:theme code="search.nav.facetValueCount" arguments="${facetValue.count}"/></span>
									</ycommerce:testId>
								</span>
							</c:if>
						</li>
					</c:forEach>
				</ul>
			</c:if>
			
<!--workspace -->

			<ul class="facet-${facetDatacode} facet__list js-facet-list <c:if test="${not empty facetData.topValues}">facet__list--hidden js-facet-list-hidden</c:if>">
			 <div class="checkbox-group-1">
				
				<c:forEach items="${facetData.values}" var="facetValue">
					<li>
						<c:if test="${facetData.multiSelect}">
							<ycommerce:testId code="facetNav_selectForm">
							<form action="#" method="get">
							<!-- facetValue.query.query.value and searchPageData.freeTextSearch are html output encoded in the backend -->
								<input type="hidden" name="q" value="${facetValue.query.query.value}"/>
								<input type="hidden" name="text" value="${searchPageData.freeTextSearch}"/>
								<label>
									<input type="checkbox" ${facetValue.selected ? 'checked="checked"' : ''}  class="facet__list__checkbox js-facet-checkbox sr-only" />
									<span class="facet__list__label">
										<span class="facet__list__mark"></span>
										<span class="facet__list__text">
											${fn:escapeXml(facetValue.name)}&nbsp;
											<ycommerce:testId code="facetNav_count">
												<span class="facet__value__count"><spring:theme code="search.nav.facetValueCount" arguments="${facetValue.count}"/></span>
											</ycommerce:testId>
										</span>
									</span>
								</label>
							</form>
							</ycommerce:testId>
						</c:if>
						<c:if test="${not facetData.multiSelect}">
							<c:url value="${facetValue.query.url}" var="facetValueQueryUrl"/>
							<span class="facet__text">
								<a href="${fn:escapeXml(facetValueQueryUrl)}">${fn:escapeXml(facetValue.name)}</a>
								<ycommerce:testId code="facetNav_count">
									<span class="facet__value__count"><spring:theme code="search.nav.facetValueCount" arguments="${facetValue.count}"/></span>
								</ycommerce:testId>
							</span>
						</c:if>
					</li>
				</c:forEach>
				</div>
			</ul>

			<c:if test="${not empty facetData.topValues}">
			
				<span class="facet__values__more js-more-facet-values">
					<a href="#" class="js-more-facet-values-link" ><spring:theme code="search.nav.facetShowMore_${facetData.code}" /></a>
				</span>
				<span class="facet__values__less js-less-facet-values">
					<a href="#" class="js-less-facet-values-link"><spring:theme code="search.nav.facetShowLess_${facetData.code}" /></a>
				</span>
			</c:if>
		</div>
	</div>
</ycommerce:testId>
</c:if>
</div>

