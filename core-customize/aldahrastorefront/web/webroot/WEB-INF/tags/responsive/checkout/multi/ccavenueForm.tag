<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="supportedPaymentData" required="true" type="com.aldahra.aldahrapayment.entry.PaymentRequestData" %>

	<center class="card_checkout">
		<br>
      	<!-- width required mininmum 482px -->
       	<iframe  width="100%" height="500" scrolling="No" frameborder="0"  id="paymentFrame" src="${supportedPaymentData.scriptSrc}">
       
	  	</iframe>
	</center>

	
	<script type="text/javascript">
    	$(document).ready(function(){
    		$('iframe#paymentFrame').load(function() {
				 window.addEventListener('message', function(e) {
			    	 $("#paymentFrame").css("height",e.data['newHeight']+'px'); 	 
			 	 }, false);
			 }); 
    	});
	</script>