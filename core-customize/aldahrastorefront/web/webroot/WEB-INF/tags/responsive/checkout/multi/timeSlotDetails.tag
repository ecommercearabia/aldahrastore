<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="day" required="true" type="com.aldahra.aldahratimeslotfacades.TimeSlotDayData" %>
<%@ attribute name="selectedSlot" required="false" type="com.aldahra.aldahratimeslotfacades.TimeSlotInfoData" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="carousel__item">


<div class="slot_time">
	
<div class="title_slot_time">
	<span class="day">${day.day}</span>
	<span class="date">${day.date}</span>
	</div>
	<c:forEach items="${day.periods}" var="period">
	<c:set value="" var="unavailable"></c:set>
	<c:set value="" var="unavailable_label"></c:set>
	<c:set value="active" var="unavailable_class"></c:set>
	<c:if test="${!period.enabled}">
			<c:set value="disabled" var="unavailable"></c:set>
			<c:set value="label_disabled" var="unavailable_label"></c:set>
			<c:set value="no_action" var="unavailable_class"></c:set>
		</c:if>
<div class="item_time_slot ${unavailable_class}">
	
		<c:set value="" var="selected"></c:set>
		<c:if test="${not empty selectedSlot.periodCode && selectedSlot.periodCode eq period.code  && empty unavailable}">
			<c:set value="checked='checked'" var="selected"></c:set>
			
		</c:if>
		
		<input type="radio" ${unavailable}  id="${period.code}" name="selector_selector" class="hidden"
		 data-start= "${period.start}" data-end="${period.end}" data-code="${period.code}" data-day="${day.day}"
		 data-date="${day.date}" ${selected}/>
		 
<%-- yy ${selectedSlot.periodCode} yy --%>
		 
		<label  for="${period.code}"class="select_st ${unavailable_label}" >
		<span class="${unavailable}"></span>
		 
		${period.intervalFormattedValue}
		<c:if test="${!period.enabled}" >
			<span class="empty">unavailable</span>
		</c:if>
		</label>
		
	</div>	
	</c:forEach>
</div>
</div>
