<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="paymentMethod" required="true" type="de.hybris.platform.commercefacades.order.data.PaymentModeData" %>
<%@ attribute name="isSelected" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<option value="${fn:escapeXml(paymentMethod.code)}" ${isSelected ? 'selected="selected"' : ''}>
	${fn:escapeXml(paymentMethod.code)}&nbsp;-&nbsp;${fn:escapeXml(paymentMethod.name)}
</option>