<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="actionNameKey" required="true"
	type="java.lang.String"%>
<%@ attribute name="action" required="true" type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>

<spring:htmlEscape defaultHtmlEscape="true" />

<spring:url value="/login/register/termsandconditions"
	var="getTermsAndConditionsUrl" />

<div class="user-register__headline">
	<spring:theme code="register.new.customer" />
</div>
<p class="user-register_info">
	<spring:theme code="register.description" />
</p>

<form:form method="post" modelAttribute="registerForm"
	action="${action}">
	<div class="row">
		<div class="col-md-6 col-sm-12">
			<formElement:formInputBox idKey="register.firstName"
				labelKey="register.firstName" path="firstName"
				inputCSS="form-control" mandatory="true"
				placeholder="register.firstName" />
		</div>
		<div class="col-md-6 col-sm-12">
			<formElement:formInputBox idKey="register.lastName"
				labelKey="register.lastName" path="lastName" inputCSS="form-control"
				mandatory="true" placeholder="register.lastName" />
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 col-xs-6 title">
			<formElement:formSelectBoxDefaultEnabled idKey="register.title"
				labelKey="register.title" selectCSSClass="form-control"
				path="titleCode" mandatory="true" skipBlank="false"
				skipBlankMessageKey="form.select.none" items="${titles}" />
		</div>
		<div class="col-md-6 col-xs-6">
			<c:if test="${cmsSite.nationalityCustomerEnabled}">
				<div
					class="<c:if test="${cmsSite.nationalityCustomerHidden}">hidden</c:if>">
					<formElement:formSelectBoxDefaultEnabled idKey="nationality"
						labelKey="register.nationality" selectCSSClass="form-control f16"
						path="nationality"
						mandatory="${cmsSite.nationalityCustomerRequired}"
						skipBlank="false" skipBlankMessageKey="form.select.none"
						items="${nationalities}" />
				</div>
			</c:if>
		</div>

	</div>

	<div class="row">
		<div class="col-md-6 col-sm-12">

			<formElement:formCountrySelectBoxDefaultEnabled idKey="mobileCountry"
				labelKey="register.mobileCountry" selectCSSClass="form-control f16 "
				path="mobileCountry" mandatory="true" skipBlank="false"
				skipBlankMessageKey="form.select.empty" items="${mobileCountries}" />
		</div>

		<div class="col-md-6 col-sm-12">
			<formElement:formInputBox idKey="register.mobileNumber"
				labelKey="register.mobileNumber" path="mobileNumber"
				inputCSS="form-control" mandatory="true"
				placeholder="register.mobileNumber" />
		</div>
	</div>
	<c:if test="${baseStore.referralCodeEnable }">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<formElement:formInputBox idKey="register.referralCode"
					labelKey="register.referralCode" path="referralCode"
					inputCSS="form-control" mandatory="false"
					placeholder="Referral Code" />
				<div class="tooltip">
					<i class="far fa-info-circle"></i>
					<div class="top hidden-md hidden-lg">
						<div class="text-content">

							<spring:theme code='msg.InfoTooltip' />

						</div>
						<i></i>
					</div>


					<div class="right hidden-sm hidden-xs">
						<div class="text-content">

							<spring:theme code='msg.InfoTooltip' />

						</div>
						<i></i>
					</div>
				</div>

			</div>
		</div>

	</c:if>

	<div class="row">
		<div class="col-md-12 col-sm-12">
			<formElement:formInputBox idKey="register.email"
				labelKey="register.email" path="email" inputCSS="form-control"
				mandatory="true" placeholder="register.email" />
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<div class="pass">
				<formElement:formPasswordBox idKey="password"
					labelKey="register.pwd" path="pwd"
					inputCSS="form-control password-strength" mandatory="true" />
			</div>
		</div>
	</div>
	<div class="row">

		<c:if test="${ not empty consentTemplateData }">
			<div class="col-md-12 col-sm-12">
				<form:hidden path="consentForm.consentTemplateId"
					value="${consentTemplateData.id}" />
				<form:hidden path="consentForm.consentTemplateVersion"
					value="${consentTemplateData.version}" />
				<div class="checkbox">
					<label class="control-label uncased"> <form:checkbox
							path="consentForm.consentGiven" disabled="true" /> <c:out
							value="${consentTemplateData.description}" />

					</label>
				</div>
				<div class="help-block">
					<spring:theme code="registration.consent.link" />
				</div>
			</div>
		</c:if>

		<div class="col-md-12 col-sm-12">
			<spring:theme code="register.termsConditions"
				arguments="${getTermsAndConditionsUrl}" var="termsConditionsHtml"
				htmlEscape="false" />
			<template:errorSpanField path="termsCheck">
				<div class="checkbox">
					<label class="control-label uncased"> <form:checkbox
							id="registerChkTermsConditions" path="termsCheck" disabled="true" />
						${ycommerce:sanitizeHTML(termsConditionsHtml)}
					</label>
				</div>
			</template:errorSpanField>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<input type="hidden" id="recaptchaChallangeAnswered"
				value="${fn:escapeXml(requestScope.recaptchaChallangeAnswered)}" />
			<div
				class="form_field-elements control-group js-recaptcha-captchaaddon"></div>
			<div class="form-actions clearfix btn_register">
				<ycommerce:testId code="register_Register_button">
					<button type="submit" class="btn btn-default ">
						<spring:theme code='${actionNameKey}' />
					</button>
				</ycommerce:testId>
			</div>
		</div>
	</div>
	<!-- -------------------------------------------- -->
</form:form>
