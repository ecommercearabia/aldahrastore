<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>


<footer>
    <div class="row margin_top no-margin">
    	<div class="col-md-3  col-xs-12 logo">
    		 <cms:pageSlot position="Footer" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
    	</div>
    	<div class="col-md-6 col-xs-12">
    		<cms:pageSlot position="FooterNavLinks" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
    	</div>
    	<div class="col-md-3 col-xs-12 social">
    	 <cms:pageSlot position="FooterAppStoreLinks" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
    	<cms:pageSlot position="FooterFollowUsLinks" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
   
    	</div>
    </div>
    <div class="row no-margin">
    <div class="col-md-12 support_visa">
    <cms:pageSlot position="FooterPaymentLinks" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
    <span class="smile"></span>
    </div>
    <div class="col-md-12 copy_right">
     <cms:pageSlot position="CopyrightFooter" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
    </div>
    </div>
    
    
    
   
</footer>

