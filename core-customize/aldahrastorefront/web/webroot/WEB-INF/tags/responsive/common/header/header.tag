<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="hideHeaderLinks" required="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/responsive/common/header"  %>

<spring:htmlEscape defaultHtmlEscape="true" />

<cms:pageSlot position="TopHeaderSlot" var="component" element="div" class="bottom_fexed" >
	<cms:component component="${component}" />
</cms:pageSlot>
		
<header class="js-mainHeader js-offcanvas-links">

<div class="branding-mobile hidden-md hidden-lg">
			<div class="js-mobile-logo">
				<%--populated by JS acc.navigation--%>
			</div>
		</div>
	<nav class="navigation navigation--top">
		<div class="row hidden-xs hidden-sm">
			<div class="col-sm-12 col-md-12 head">
<script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5f09cb4dadf39b00123aa8e6&product=inline-share-buttons&cms=website' async='async'></script>

					<div class="row d-flex">
					<div class="col-xs-3 col-md-4 comm_nav">
					<div class="content_communication"><a href="tel:80036632" class="tel"><i class="call"></i><spring:theme code="header.link.call" text="Call : 8001 FOOD"/> </a> <a href="https://api.whatsapp.com/send?phone=97180036632&text=Hi!" aria-label="whatsApp" class="whats_app_icon"><i class="fab fa-whatsapp"></i></a>
<!-- 					<div class="sharethis-inline-share-buttons"></div> -->
					<div  class=" social_popup"><div class="content_share"><i class="fas fa-share-alt"></i> <spring:theme code="header.link.tell.about.us" text="Tell your Friends About Us"/></div>
									<div class="popup_social hidden">
					<c:url value="https://foodcrowd.com" var="links"></c:url>
					<div data-network="twitter" class="st-custom-button social_twitter"><i class="fab fa-twitter"></i></div>
					<div data-network="facebook" class="st-custom-button social_facebook"><i class="fab fa-facebook-f"></i></div>
					<div data-network="linkedin" class="st-custom-button social_linkedin"><i class="fab fa-linkedin"></i></div>
					<div data-network="pinterest" class="st-custom-button social_pinterest"><i class="fab fa-pinterest"></i></div>
					<div data-network="whatsapp" class="st-custom-button social_whatsapp"><i class="fab fa-whatsapp"></i></div> 
					
					</div>
					
					  </div> 
					
					
					</div>
					</div>
					<div class="col-xs-4 col-md-4 center_text_head  text-center">
					<cms:pageSlot position="TopAlert" var="component" element="div" >
					<cms:component component="${component}" />
					</cms:pageSlot>
					</div>
					
					<div class="col-xs-3 col-md-4 set_account_div">
						<ul class="top-links">
						<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')" >
								<li class="  liOffcanvas registration" role="tab">
									<ycommerce:testId code="header_Login_link">
										<c:url value="/login" var="loginUrl" />
										
										<a class="login_action" href="javascript:;">
										<i class="user"></i>
											<spring:theme code="header.link.login" />
										</a>
									</ycommerce:testId>
									<div class="login_box hidden">
									<ycommerce:testId code="header_Login_link">
								<cms:pageSlot position="LoginPopup" var="component" limit="1">
							 	<cms:component component="${component}" />
							</cms:pageSlot>
							</ycommerce:testId>
									</div>
									<div class="darkback hidden"></div>
									
								</li>
							</sec:authorize>
							 <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')" >
								<li class=" liOffcanvas_logout">
							
									<ycommerce:testId code="header_signOut">
										<c:url value="/logout" var="logoutUrl"/>
										<a href="${fn:escapeXml(logoutUrl)}" class="logout">
											<i class="far fa-sign-out-alt"></i>
											<spring:theme code="header.link.logout" />
										</a>
									</ycommerce:testId>
								</li>
							</sec:authorize>

							 <cms:pageSlot position="HeaderLinks" var="link">
								 <cms:component component="${link}" element="li" class="loginaccount" />
							 </cms:pageSlot>
							 
							<li class="lang">	
					<header:languageSelector languages="${languages}" currentLanguage="${currentLanguage}"/> 	
					</li>
							
							
					</ul>
					
</div>
					
				
				
			</div>
		
		</div>
		</div>
		<div class="row  ">
			<div class="col-xs-3 pull-left col-lg-3 col-md-3 hidden-xs hidden-sm logo_img">
			
				<div class="nav__left js-site-logo">
					<cms:pageSlot position="SiteLogo" var="logo" limit="1">
						<cms:component component="${logo}" element="div" class="yComponentWrapper"/>
					</cms:pageSlot>
				</div>
			</div>
			<div class="col-md-4 col-lg-5 col-xs-12 col-sm-12">
			<div class="site-search">
								<cms:pageSlot position="SearchBox" var="component">
									<cms:component component="${component}" element="div"/>
								</cms:pageSlot>
							</div>
						</div>	
					<div class="col-xs-4 pull-right col-lg-4 col-md-5 hidden-xs hidden-sm nav_btn">
					
					<ul class="nav__links nav__links--shop_info links_com_myC ">
					<li class="Community_page"><a href="${cmsSite.communityPageUrl}?lang=${fn:escapeXml(currentLanguage.isocode)}" rel="noopener" target="_blank" class="btn btn-block btn-primary"><i class="fal fa-hands-heart"></i><spring:theme code="header.link.community.page" text="Community page"/></a></li>
						<li> 
							<cms:pageSlot position="MiniCart" var="cart" element="div" class="componentContainer">
								<cms:component component="${cart}" element="div"/>
							</cms:pageSlot>
							
						</li>
						
						
					</ul>
						</div>
						
		<!--  	<div class="col-sm-12 col-md-8" style="display:none">	
				<div class="nav__right">
					<ul class="nav__links nav__links--account">
						<c:if test="${empty hideHeaderLinks}">
							<c:if test="${uiExperienceOverride}">
								<li class="backToMobileLink">
									<c:url value="/_s/ui-experience?level=" var="backToMobileStoreUrl" />
									<a href="${fn:escapeXml(backToMobileStoreUrl)}">
										<spring:theme code="text.backToMobileStore" />
									</a>
								</li>
							</c:if>

							<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
								<c:set var="maxNumberChars" value="25" />
								<c:if test="${fn:length(user.firstName) gt maxNumberChars}">
									<c:set target="${user}" property="firstName"
										value="${fn:substring(user.firstName, 0, maxNumberChars)}..." />
								</c:if>

								<li class="logged_in js-logged_in">
									<ycommerce:testId code="header_LoggedUser">
										<spring:theme code="header.welcome" arguments="${user.firstName},${user.lastName}" />
									</ycommerce:testId>
								</li>
							</sec:authorize>

							 <cms:pageSlot position="HeaderLinks" var="link">
								 <cms:component component="${link}" element="li" />
							 </cms:pageSlot>

							<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')" >
								<li class="liOffcanvas">
									<ycommerce:testId code="header_Login_link">
										<c:url value="/login" var="loginUrl" />
										<a href="${fn:escapeXml(loginUrl)}">
											<spring:theme code="header.link.login" />
										</a>
									</ycommerce:testId>
								</li>
								<li class="liOffcanvas">
								<ycommerce:testId code="header_Login_link">
								<cms:pageSlot position="LoginPopup" var="component" limit="1">
							 	<cms:component component="${component}" />
							</cms:pageSlot>
							</ycommerce:testId>
							</li>
							</sec:authorize>

							<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')" >
								<li class="liOffcanvas">
									<ycommerce:testId code="header_signOut">
										<c:url value="/logout" var="logoutUrl"/>
										<a href="${fn:escapeXml(logoutUrl)}">
											<spring:theme code="header.link.logout" />
										</a>
									</ycommerce:testId>
								</li>
							</sec:authorize>
							
						</c:if>
					</ul>
				</div>
			</div>
			 -->
		</div>
	</nav>
	
	<%-- a hook for the my account links in desktop/wide desktop--%>
	<div class="hidden-xs hidden-sm js-secondaryNavAccount collapse" id="accNavComponentDesktopOne">
		<ul class="nav__links">

		</ul>
	</div>

	<div class="hidden-xs hidden-sm js-secondaryNavCompany collapse" id="accNavComponentDesktopTwo">
		<ul class="nav__links js-nav__links">

		</ul>
	</div>
	
	<nav class="navigation navigation--middle js-navigation--middle">
		<div class="container-fluid">
			<div class="row">
				<div class="mobile__nav__row mobile__nav__row--table">
					<div class="mobile__nav__row--table-group">
						<div class="mobile__nav__row--table-row">
							<div class="mobile__nav__row--table-cell visible-xs hidden-sm">
								<button class="mobile__nav__row--btn btn mobile__nav__row--btn-menu js-toggle-sm-navigation color_icon_nav_mobile"
										type="button">
									<span class="far fa-align-justify"></span>
								</button>
							</div>

							<div class="mobile__nav__row--table-cell visible-xs visible-sm mobile__nav__row--seperator ">
								<ycommerce:testId code="header_search_activation_button">
									<a href="javascript:;"	class="mobile__nav__row--btn btn mobile__nav__row--btn-search js-toggle-xs-search hidden-md hidden-lg color_icon_nav_mobile" type="button" aria-label="btn nav">
										<span class="far fa-search"></span>
									</a>
								</ycommerce:testId>
							</div>

							<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')" >
								<ycommerce:testId code="header_StoreFinder_link">
									<div class="mobile__nav__row--table-cell hidden-sm hidden-md hidden-lg mobile__nav__row--seperator ">
										<c:url value="/login" var="login"/>
										<a href="${fn:escapeXml(login)}" class="mobile__nav__row--btn mobile__nav__row--btn-location btn color_icon_nav_mobile" aria-label="nav icon">
											<span class="far fa-user"></span>
										</a>
									</div>
								</ycommerce:testId>
							</sec:authorize>
							<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
								<ycommerce:testId code="header_StoreFinder_link">
									<div class="mobile__nav__row--table-cell hidden-sm hidden-md hidden-lg mobile__nav__row--seperator ">
										<c:url value="/logout" var="logout"/>
										<a href="${fn:escapeXml(logout)}" aria-label="nav icon" class="mobile__nav__row--btn mobile__nav__row--btn-location btn color_icon_nav_mobile">
											<span class="far fa-lock"></span>
										</a>
									</div>
								</ycommerce:testId>
							</sec:authorize>

							<cms:pageSlot position="MiniCart" var="cart" element="div" class="miniCartSlot componentContainer mobile__nav__row--table hidden-sm hidden-md hidden-lg color_icon_nav_mobile">
								<cms:component component="${cart}" element="div" class="mobile__nav__row--table-cell" />
							</cms:pageSlot>

						</div>
					</div>
				</div>
			</div>
		<!-- 	<div class="row desktop__nav">
				<div class="nav__left col-xs-12 col-sm-6">
					<div class="row">
						<div class="col-sm-2 hidden-xs visible-sm mobile-menu">
							<button class="btn js-toggle-sm-navigation" type="button">
								<span class="far fa-align-justify"></span>
							</button>
						</div>
						<div class="col-sm-10">
							<div class="site-search">
								<cms:pageSlot position="SearchBox" var="component">
									<cms:component component="${component}" element="div"/>
								</cms:pageSlot>
							</div>
						</div>
					</div>
				</div>
				<div class="nav__right col-xs-6 col-xs-6 hidden-xs">
					<ul class="nav__links nav__links--shop_info">
						<li>
							<cms:pageSlot position="MiniCart" var="cart" element="div" class="componentContainer">
								<cms:component component="${cart}" element="div"/>
							</cms:pageSlot>
						</li>
					</ul>
				</div>
			</div>
			-->
		</div>
	</nav>
		<div class="cart_header_component hidden">
							<div class="head_cart_component"><h3><spring:theme code="text.cart"/></h3><i class="fal fa-times close_cart_component"></i></div>
							<div class="main_cart_component"></div>
							</div>
	<a id="skiptonavigation" href="javascript:;" aria-label="link"></a>
	<cms:pageSlot position="PromotionSection" var="feature" class="" element="div">
		<cms:component component="${feature}" />
	</cms:pageSlot>
	<nav:topNavigation />

</header>

<cms:pageSlot position="BottomHeaderSlot" var="component" element="div"	class="container-fluid">
	<cms:component component="${component}" />
</cms:pageSlot>
