<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>

<spring:htmlEscape defaultHtmlEscape="true" />
<spring:url value="/login/register/termsandconditions" var="getTermsAndConditionsUrl" htmlEscape="false"/>

<div class="checkout-success">
	<div class="checkout-success__body">
		<div class="checkout-success__body__headline">
			<spring:theme code="checkout.orderConfirmation.thankYouForOrder" />
		</div>
		<p class="order_number"><spring:theme code="text.account.order.orderNumberLabel"/> ${fn:escapeXml(orderData.code)}</p>
		<p class="order_detail"><spring:theme code="checkout.orderConfirmation.copySentToShort"/> ${fn:escapeXml(email)}</p>
		<div>
			<img src="${fn:escapeXml(themeResourcePath)}/images/thanku_food.png" class="img_checkout_page"/>
		</div>
		<spring:url var="orderDetailsURL" value="{contextPath}/my-account/order/${fn:escapeXml(orderData.code)}" htmlEscape="false" >
			<spring:param name="contextPath" value="${request.contextPath}" />
		</spring:url>
		<div class="center">
			<a href="${orderDetailsURL}" class="btn_checkout_page"><spring:theme code="view.order.details"/></a>
		</div>
	</div>
</div>