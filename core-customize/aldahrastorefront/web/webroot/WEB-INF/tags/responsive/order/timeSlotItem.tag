<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true"/>
<div>
    <div class="label-order">
        <spring:theme code="text.timeSlot"/>
    </div>
    ${fn:escapeXml(order.timeSlotInfoData.day)} - ${fn:escapeXml(order.timeSlotInfoData.start)} - ${fn:escapeXml(order.timeSlotInfoData.end)}
    <br>
    ${fn:escapeXml(order.timeSlotInfoData.date)}
</div>