<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="address" required="true" type="de.hybris.platform.commercefacades.user.data.AddressData" %>
<%@ attribute name="storeAddress" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${not storeAddress }">
	<c:if test="${not empty address.title}">
	    ${fn:escapeXml(address.title)}&nbsp;
	</c:if>
	${fn:escapeXml(address.firstName)}&nbsp;${fn:escapeXml(address.lastName)}
	<br>
</c:if>
<c:choose>
	<c:when test="${not empty address.streetName}">
		${fn:escapeXml(address.streetName)} <br>
	</c:when>
	<c:otherwise>
		${fn:escapeXml(address.line1)} <br>
	</c:otherwise>
</c:choose>
<c:if test="${not empty address.buildingName}">
	<spring:theme code="address.field.buildingName" />:&nbsp;${fn:escapeXml(address.buildingName)}<br>
</c:if>
<c:if test="${not empty address.apartmentNumber}">
	<spring:theme code="address.field.apartmentNumber" />:&nbsp;${fn:escapeXml(address.apartmentNumber)}<br>
</c:if>
<c:if test="${not empty address.country.name}">
	${fn:escapeXml(address.country.name)} <br>
</c:if>
<c:if test="${not empty address.city.name}">
	${fn:escapeXml(address.city.name)} <br>
</c:if>
<c:if test="${not empty address.area.name}">
	${fn:escapeXml(address.area.name)} <br>
</c:if>
<c:if test="${not empty address.mobileNumber}">
	${fn:escapeXml(address.mobileNumber)} <br>
</c:if>