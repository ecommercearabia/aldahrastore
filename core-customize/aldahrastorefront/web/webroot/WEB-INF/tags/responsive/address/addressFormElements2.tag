<%@ attribute name="regions" required="true" type="java.util.List"%>
<%@ attribute name="country" required="false" type="java.lang.String"%>
<%@ attribute name="tabIndex" required="false" type="java.lang.Integer"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<c:choose>
	<c:when test="${country == 'US'}">
	
	
			<div class="row">
			<div class="col-md-12 col-sm-12">	
		<formElement:formInputBox idKey="address.addressName" labelKey="address.addressName" path="addressName" inputCSS="form-control" mandatory="true" />
	</div>
	<div class="col-md-2 col-sm-12">
		<formElement:formSelectBoxDefaultEnabled idKey="address.title" labelKey="address.title" path="titleCode" mandatory="true" skipBlank="false" skipBlankMessageKey="address.title.none" items="${titles}" selectedValue="${addressForm.titleCode}" selectCSSClass="form-control"/>
		</div>
		<div class="col-md-5 col-sm-12">
		<formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="firstName" inputCSS="form-control" mandatory="true" />
		</div>
		<div class="col-md-5 col-sm-12">
		<formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="lastName" inputCSS="form-control" mandatory="true" />
		</div>
		<div class="col-md-6 col-sm-12">
		<formElement:formInputBox idKey="address.line1" labelKey="address.line1" path="line1" inputCSS="form-control" mandatory="true" />
		</div>
		<div class="col-md-6 col-sm-12">
		<formElement:formInputBox idKey="address.line2" labelKey="address.line2" path="line2" inputCSS="form-control" mandatory="false"/>
		</div>
		<div class="col-md-6 col-sm-12">
		<formElement:formCountrySelectBoxDefaultEnabled idKey="address.mobileCountry" labelKey="address.mobileCountry" selectCSSClass="form-control" path="mobileCountry" mandatory="true" skipBlank="false" skipBlankMessageKey="form.select.empty" items="${mobileCountries}" />
		</div>
		<div class="col-md-6 col-sm-12">
		<formElement:formInputBox idKey="address.mobileNumber" labelKey="address.mobileNumber" path="mobile" inputCSS="form-control" mandatory="true" />
		</div>
		<div class="col-md-6 col-sm-12">
		<formElement:formInputBox idKey="address.townCity" labelKey="address.townCity" path="townCity" inputCSS="form-control" mandatory="true" />
	</div>
		<div class="col-md-6 col-sm-12">
		<formElement:formSelectBox idKey="address.region" labelKey="address.state" path="regionIso" mandatory="true" skipBlank="false" skipBlankMessageKey="address.state" items="${regions}" itemValue="${useShortRegionIso ? 'isocodeShort' : 'isocode'}" selectedValue="${addressForm.regionIso}" selectCSSClass="form-control"/>
		</div>
		<div class="col-md-6 col-sm-12">
		<formElement:formInputBox idKey="address.postcode" labelKey="address.zipcode" path="postcode" inputCSS="form-control" mandatory="true" />
        </div>
		
	</div>
	</c:when>
	<c:when test="${country == 'CA'}">
	
		<formElement:formInputBox idKey="address.addressName" labelKey="address.addressName" path="addressName" inputCSS="form-control" mandatory="true" />
		<formElement:formSelectBoxDefaultEnabled idKey="address.title" labelKey="address.title" path="titleCode" mandatory="true" skipBlank="false" skipBlankMessageKey="address.title.none" items="${titles}" selectedValue="${addressForm.titleCode}" selectCSSClass="form-control"/>
		<formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="firstName" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="lastName" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.line1" labelKey="address.line1" path="line1" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.line2" labelKey="address.line2" path="line2" inputCSS="form-control" mandatory="false"/>
		<formElement:formCountrySelectBoxDefaultEnabled idKey="address.mobileCountry" labelKey="address.mobileCountry" selectCSSClass="form-control" path="mobileCountry" mandatory="true" skipBlank="false" skipBlankMessageKey="form.select.empty" items="${mobileCountries}" />
		<formElement:formInputBox idKey="address.mobileNumber" labelKey="address.mobileNumber" path="mobile" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.nearestLandmark" labelKey="address.nearestLandmark" path="nearestLandmark" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.townCity" labelKey="address.townCity" path="townCity" inputCSS="form-control" mandatory="true" />
		<formElement:formSelectBox idKey="address.region" labelKey="address.province" path="regionIso" mandatory="true" skipBlank="false" skipBlankMessageKey="address.state" items="${regions}" itemValue="${useShortRegionIso ? 'isocodeShort' : 'isocode'}" selectedValue="${addressForm.regionIso}" selectCSSClass="form-control"/>
	</c:when>
	<c:when test="${country == 'CN'}">
	
		<formElement:formInputBox idKey="address.addressName" labelKey="address.addressName" path="addressName" inputCSS="form-control" mandatory="true" />
		<formElement:formSelectBox idKey="address.region" labelKey="address.province" path="regionIso" mandatory="true" skipBlank="false" skipBlankMessageKey="address.selectProvince" items="${regions}" itemValue="${useShortRegionIso ? 'isocodeShort' : 'isocode'}" selectedValue="${addressForm.regionIso}" selectCSSClass="form-control"/>
		<formElement:formInputBox idKey="address.townCity" labelKey="address.townCity" path="townCity" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.line1" labelKey="address.street" path="line1" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.line2" labelKey="address.building" path="line2" inputCSS="form-control" mandatory="false"/>
		<formElement:formCountrySelectBoxDefaultEnabled idKey="address.mobileCountry" labelKey="address.mobileCountry" selectCSSClass="form-control" path="mobileCountry" mandatory="true" skipBlank="false" skipBlankMessageKey="form.select.empty" items="${mobileCountries}" />
		<formElement:formInputBox idKey="address.mobileNumber" labelKey="address.mobileNumber" path="mobile" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.nearestLandmark" labelKey="address.nearestLandmark" path="nearestLandmark" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="lastName" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="firstName" inputCSS="form-control" mandatory="true" />
		<formElement:formSelectBoxDefaultEnabled idKey="address.title" labelKey="address.title" path="titleCode" mandatory="true" skipBlank="false" skipBlankMessageKey="address.title.none" items="${titles}" selectedValue="${addressForm.titleCode}" selectCSSClass="form-control"/>
	</c:when>
	<c:when test="${country == 'JP'}">
	
		<formElement:formInputBox idKey="address.addressName" labelKey="address.addressName" path="addressName" inputCSS="form-control" mandatory="true" />
		<formElement:formSelectBoxDefaultEnabled idKey="address.title" labelKey="address.title" path="titleCode" mandatory="true" skipBlank="false" skipBlankMessageKey="address.title.none" items="${titles}" selectedValue="${addressForm.titleCode}" selectCSSClass="form-control"/>
		<formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="lastName" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="firstName" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.line1" labelKey="address.furtherSubarea" path="line1" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.line2" labelKey="address.subarea" path="line2" inputCSS="form-control" mandatory="true"/>
		<formElement:formCountrySelectBoxDefaultEnabled idKey="address.mobileCountry" labelKey="address.mobileCountry" selectCSSClass="form-control" path="mobileCountry" mandatory="true" skipBlank="false" skipBlankMessageKey="form.select.empty" items="${mobileCountries}" />
		<formElement:formInputBox idKey="address.mobileNumber" labelKey="address.mobileNumber" path="mobile" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.townCity" labelKey="address.townJP" path="townCity" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.nearestLandmark" labelKey="address.nearestLandmark" path="nearestLandmark" inputCSS="form-control" mandatory="true" />
		<formElement:formSelectBox idKey="address.region" labelKey="address.prefecture" path="regionIso" mandatory="true" skipBlank="false" skipBlankMessageKey="address.selectPrefecture" items="${regions}" itemValue="${useShortRegionIso ? 'isocodeShort' : 'isocode'}" selectedValue="${addressForm.regionIso}" selectCSSClass="form-control"/>
	</c:when>
	<c:otherwise>
		<div class="row">
		
		<div class="col-md-12 col-sm-12">
		
		<formElement:formSelectBoxDefaultEnabled idKey="cityId" labelKey="address.city" path="cityCode" mandatory="true" skipBlank="false" skipBlankMessageKey="form.select.empty" items="${cities}" selectCSSClass="form-control"/>
		</div>
		
		<c:if test="${isAreaVisible}">
			<div class="col-md-12 col-sm-12">
			
			
			<formElement:formSelectBoxDefaultEnabled idKey="areaId" labelKey="address.areaCode" path="areaCode" mandatory="true" skipBlank="false" skipBlankMessageKey="form.select.empty" items="${areas}" selectCSSClass="form-control" datalivesearch="true"/>
	       </div>
       </c:if>
		
		<div class="col-md-12 col-sm-12 address_line_hid">
		  
                       

                       
                       		<formElement:formInputBox idKey="address.addressName" labelKey="address.addressName" path="addressName" inputCSS="form-control" mandatory="true" placeholder="address.addressName" />
                       
	
		

                    
              <%--      <div class="cont_map">
                                           <div class="geo_place">
                         <a href="javascript:;" id="geo" title="<spring:theme code='my.location' text='set my location'/>"><i class="fal fa-location"></i></a>
                         </div>
                    <div id="us2"></div>
                    <span class="tools-style"><i class="fas fa-map-marker-alt"></i><spring:theme code="text.move.pin" text="you can move the map pointer to your address" /></span></div> --%>
 		
 		<div class="row">
                    
                    </div>
                    <div id="outside" class="getAccAlertmap"><spring:theme code="place.inside" text="We do not delivery to this area yet, please select an area within Abu Dhabi/Al Ain"/></div>
                    <div id="block_geo" class="getAccAlertmap"> <spring:theme code="unable.geo-location" text="unable to load geo-location data "/></div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="m-t-small hidden">
                        <label class="p-r-small col-sm-1 control-label">Lat.:</label>

                        <div class="col-sm-1 ">
                            
                            <formElement:formInputBox idKey="latitude" labelKey="latitude" path="latitude" inputCSS="form-control hidden" mandatory="false" placeholder="latitude"/>
                        </div>
                        <label class="p-r-small col-sm-1 control-label">Long.:</label>

                        <div class="col-sm-1">
                           
                            <formElement:formInputBox idKey="longitude" labelKey="longitude" path="longitude" inputCSS="form-control hidden" mandatory="false" placeholder="longitude"/>
                        </div>
                    </div>
		</div> 
		
			<div class="col-md-12 col-sm-12">
				<formElement:formInputBox idKey="us2-address" labelKey="address.line1" path="line1" inputCSS="form-control" mandatory="true" placeholder="address.line1"  />
			</div>
		<div class="col-md-4 col-sm-12">
		<formElement:formSelectBoxDefaultEnabled idKey="address.title" labelKey="address.title" path="titleCode" mandatory="true" skipBlank="false" skipBlankMessageKey="address.title.none" items="${titles}" selectedValue="${addressForm.titleCode}" selectCSSClass="form-control"/>
		</div>
		<div class="col-md-4 col-sm-12">
		<formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="firstName" inputCSS="form-control" mandatory="true" placeholder="address.firstName"/>
		</div>
		<div class="col-md-4 col-sm-12">
		<formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="lastName" inputCSS="form-control" mandatory="true" placeholder="address.surname"/>
		</div>
		<div class="col-md-6 col-sm-12">
		<formElement:formCountrySelectBoxDefaultEnabled idKey="mobileCountry" labelKey="address.mobileCountry" selectCSSClass="form-control countrypicker f16 pointer_event" path="mobileCountry" mandatory="true" skipBlank="true" skipBlankMessageKey="form.select.empty" items="${mobileCountries}" />
		</div>
		<div class="col-md-6 col-sm-12">
		<formElement:formInputBox idKey="address.mobileNumber" labelKey="address.mobileNumber" path="mobile" inputCSS="form-control" mandatory="true" placeholder="address.mobileNumber" />
		</div>
		
       <div class="col-md-12 col-sm-12 hidden">
		<formElement:formInputBox idKey="address.line2" labelKey="address.line2" path="line2" inputCSS="form-control" mandatory="false" placeholder="address.line2"/>
		</div>
			<c:if test="${cmsSite.customStreetNameAddressEnabled }">
			<div class="col-md-6 col-sm-12 <c:if test="${cmsSite.customStreetNameAddressHidden}">hidden</c:if>">
		         <input type="hidden" class="AllowAutoComplete" value="true"/>
		        <formElement:formInputBox idKey="streetName" labelKey="custom.address.streetName" path="streetName" inputCSS="form-control" mandatory="${cmsSite.customStreetNameAddressRequired}" placeholder="custom.address.streetName" />
		       <span class="notestreetName"> <i class="fas fa-star-of-life"></i><spring:theme code="custom.address.streetName.note" /></span>
		     </div>
		
		</c:if>
		<c:if test="${cmsSite.buildingNameAddressEnabled }">
			<div class="col-md-6 col-sm-12 <c:if test="${cmsSite.buildingNameAddressHidden}">hidden</c:if>">
		       	<formElement:formInputBox idKey="buildingName" labelKey="address.buildingName" path="buildingName" inputCSS="form-control" mandatory="${cmsSite.buildingNameAddressRequired}" placeholder="address.buildingName" />
	        </div>
		</c:if>
         
     <c:if test="${cmsSite.apartmentNumberAddressEnabled}">
            <div class="col-md-6 col-sm-12 <c:if test="${cmsSite.apartmentNumberAddressHidden}">hidden</c:if>">
        		<formElement:formInputBox idKey="address.apartmentNumber" labelKey="address.apartmentNumber" path="apartmentNumber" inputCSS="form-control" mandatory="${cmsSite.apartmentNumberAddressRequired}" placeholder="address.apartmentNumber" />
        	</div>
        </c:if>

		<div class="col-sm-12">
        <formElement:formInputBox idKey="address.nearestLandmark" labelKey="address.nearestLandmark" path="nearestLandmark" inputCSS="form-control" mandatory="true" placeholder="address.nearestLandmark" />
        </div>
        <div class="col-md-6 col-sm-12" hidden>
        <formElement:formInputBox idKey="address.longitude" labelKey="address.longitude" path="longitude" inputCSS="form-control" mandatory="true" placeholder="address.longitude" />
        </div>
        <div class="col-md-6 col-sm-12" hidden>
        <formElement:formInputBox idKey="address.latitude" labelKey="address.latitude" path="latitude" inputCSS="form-control" mandatory="true" placeholder="address.latitude" />
        </div>
      </div>  
	</c:otherwise>
</c:choose>

