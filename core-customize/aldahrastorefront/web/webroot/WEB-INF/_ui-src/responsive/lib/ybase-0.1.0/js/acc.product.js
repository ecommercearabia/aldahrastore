ACC.product = {

    _autoload: [
        "bindToAddToCartForm",
        "enableStorePickupButton",
        "enableVariantSelectors",
        "bindFacets"
    ],


    bindFacets: function () {
        $(document).on("click", ".js-show-facets", function (e) {
            e.preventDefault();
            var selectRefinementsTitle = $(this).data("selectRefinementsTitle");
            var colorBoxTitleHtml = ACC.common.encodeHtml(selectRefinementsTitle);
            ACC.colorbox.open(colorBoxTitleHtml, {
                href: ".js-product-facet",
                inline: true,
                width: "480px",
                onComplete: function () {
                    $(document).on("click", ".js-product-facet .js-facet-name", function (e) {
                        e.preventDefault();
                        $(".js-product-facet  .js-facet").removeClass("active");
                        $(this).parents(".js-facet").addClass("active");
                        $.colorbox.resize()
                    })
                },
                onClosed: function () {
                    $(document).off("click", ".js-product-facet .js-facet-name");
                }
            });
        });
        enquire.register("screen and (min-width:" + screenSmMax + ")", function () {
            $("#cboxClose").click();
        });
    },


    enableAddToCartButton: function () {
        $('.js-enable-btn').each(function () {
            if (!($(this).hasClass('outOfStock') || $(this).hasClass('out-of-stock'))) {
                $(this).prop("disabled", false);
            }
        });
    },

    enableVariantSelectors: function () {
        $('.variant-select').prop("disabled", false);
    },

    bindToAddToCartForm: function () {
        var addToCartForm = $(' .pdpAddtocart');
        addToCartForm.ajaxForm({
        	beforeSubmit:ACC.product.showRequest,
        	success: ACC.product.displayAddToCartPopup
         });    
        setTimeout(function(){
        	$ajaxCallEvent  = true;
         }, 2000);
        
     },
     showRequest: function(arr, $form, options) {  
    	 if($ajaxCallEvent)
    		{
    		 $ajaxCallEvent = false;
    		 return true;
    		}   	
    	 return false;
 
    },

    bindToAddToCartStorePickUpForm: function () {
        var addToCartStorePickUpForm = $('#colorbox #add_to_cart_storepickup_form');
        addToCartStorePickUpForm.ajaxForm({success: ACC.product.displayAddToCartPopup});
    },

    enableStorePickupButton: function () {
        $('.js-pickup-in-store-button').prop("disabled", false);
    },

    displayAddToCartPopup: function (cartResult, statusText, xhr, formElement) {
    	$ajaxCallEvent=true;
    
        if (typeof ACC.minicart.updateMiniCartDisplay == 'function') {
            ACC.minicart.updateMiniCartDisplay();
        }
//        var titleHeader = "Massage successfully";

//         ACC.colorbox.open(titleHeader, {
//            html: "<div>add product successfully</div>",
//           className:'minicart',
//            bottom:0,
//            right:0,
//            opacity:0,
//            overlayClose:false,
//            fixed:true,
//            width: "350px"
//        });
//         setTimeout(() =>$('#colorbox').colorbox.close(), 1000000);
       
        
    
        $("#addedtocartno").html(cartResult.addToCartLayer);
        var html_cont_add_to_cart=$("#addedtocartno").find('.cart_popup_error_msg').text();
      var prod_code_id = $("#addedtocartno").find('.productCode_cart_props').text();
        if(html_cont_add_to_cart.length ==0 ){
        	var add_to_cart= $('.'+prod_code_id).find('.addedtocart')
	 		 
		 	$(add_to_cart).fadeIn("300", function () {$(add_to_cart).css("display","block");});  
		      setTimeout(() =>$(add_to_cart).fadeOut("slow"), 2000);
        }else if(html_cont_add_to_cart.length <=65){
        	$('.'+prod_code_id).find('.addedtocarterror').html(html_cont_add_to_cart)
        	var add_to_cart= $('.'+prod_code_id).find(".addedtocarterror");
		 	$(add_to_cart).fadeIn("300", function () {$(add_to_cart).css("display","block");});  
		      setTimeout(() =>$(add_to_cart).fadeOut("slow"), 3000); 
        }else if(html_cont_add_to_cart.length >=66 ){
        	$('.'+prod_code_id).find('.addedtocartinfo').html(html_cont_add_to_cart);
        	var add_to_cart= $('.'+prod_code_id).find(".addedtocartinfo");
		 	$(add_to_cart).fadeIn("300", function () {$(add_to_cart).css("display","block");});  
		      setTimeout(() =>$(add_to_cart).fadeOut("slow"), 5000); 
        }
        $("#addedtocart").fadeIn("300", function () {$('#addedtocart').css("display","block");});  
        setTimeout(() =>$('#addedtocart').fadeOut("slow"), 2000);
        var productCode = $('[name=productCodePost]', formElement).val();
        var quantityField = $('[name=qty]', formElement).val();
        
        var quantity = 1;
        if (quantityField != undefined) {
            quantity = quantityField;
        }

        var cartAnalyticsData = cartResult.cartAnalyticsData;

        var cartData = {
            "cartCode": cartAnalyticsData.cartCode,
            "productCode": productCode, "quantity": quantity,
            "productPrice": cartAnalyticsData.productPostPrice,
            "productName": cartAnalyticsData.productName
        };
        ACC.track.trackAddToCart(productCode, quantity, cartData);
    }
};

$(document).ready(function () {
	if($(".page-homepage").length) {
		$("input[name=CSRFToken]").val(ACC.config.CSRFToken);
		}
	$ajaxCallEvent = true;
	ACC.product.enableAddToCartButton();
	
	 $('.addproductitem').on('submit', function (e) {

	        e.preventDefault();
	        if(lazyloadingAddtocart){
	        var formelement = $(this)
	        	var url = $(this).attr('action');
	        	console.log('url --->',url)
	        	console.log('data --->',formelement.serialize())
	        $.ajax({
	      	   url: url,
	  		   type: 'POST',
	  	
	  		  
	  		   data: $(this).serialize() ,
	  		   async: false,
	          success: function (data) {
	          
	            ACC.product.displayAddToCartPopup(data,formelement)
	          }
	        });
	        }
	      });
	 
	
});