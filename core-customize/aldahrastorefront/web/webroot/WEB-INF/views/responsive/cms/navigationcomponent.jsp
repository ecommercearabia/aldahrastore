<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set value="${fn:escapeXml(component.styleClass)}" var="navigationClassHtml" />

<c:if test="${component.visible}">
<c:url value="/my-account" var="account"></c:url>
<div class="nav_account">
<a href="${account}" class="personal_info"><span class="boder_personal_info"><i class="user"></i><c:out value="${component.navigationNode.title}"/></span></a>


</div>
    <div class="${navigationClassHtml} js-${navigationClassHtml} NAVcompONENT" data-title="${fn:escapeXml(component.navigationNode.title)}">
        <nav class="${navigationClassHtml}__child-wrap ">
            <c:if test="${not empty component.navigationNode.title }">
                
                <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
								<c:set var="maxNumberChars" value="25" />
								<c:if test="${fn:length(user.firstName) gt maxNumberChars}">
									<c:set target="${user}" property="firstName"
										value="${fn:substring(user.firstName, 0, maxNumberChars)}..." />
								</c:if>

								<li class="logged_in js-logged_in">
									<ycommerce:testId code="header_LoggedUser">
										<spring:theme code="header.welcome" arguments="${user.firstName},${user.lastName}" />
									</ycommerce:testId>
								</li>
							</sec:authorize>
							<c:url value="/my-account" var="account"></c:url>
                    <li><a href="${account}"class="mydas_b"><c:out value="${component.navigationNode.title}"/></a></li>
                
            </c:if>
            <c:forEach items="${component.navigationNode.children}" var="topLevelChild">
                <c:forEach items="${topLevelChild.entries}" var="entry">
                    <li>
                        <cms:component component="${entry.item}" evaluateRestriction="true" />
                    </li>
                </c:forEach>
            </c:forEach>
           
        </nav>
    </div>
    	<div class="darkback hidden"></div>
</c:if>