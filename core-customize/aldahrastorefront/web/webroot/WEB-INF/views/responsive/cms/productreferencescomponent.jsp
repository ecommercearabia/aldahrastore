<%@ page trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<spring:htmlEscape defaultHtmlEscape="true"/>
<c:url value="javascript:;" var="wishlist_link"></c:url>
                            <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
                                <c:url value="/login" var="wishlist_link"></c:url>
                            </sec:authorize>
<c:choose>
    <c:when test="${not empty productReferences and component.maximumNumberProducts > 0}">
        <div class="carousel__component">

            <div class="carousel__component--headline"><h2>${fn:escapeXml(component.title)}</h2></div>

            <div class="carousel__component--carousel js-owl-carousel owl-carousel js-owl-default owl-theme">


                <c:forEach end="${component.maximumNumberProducts}" items="${productReferences}" var="productReference">
                    <c:url value="${productReference.target.url}" var="productUrl"/>
                    <div class="carousel__item ${productReference.target.code}">


                        <div class="carousel__item--thumb">


                            <div class="div-label-cla">

                                <c:if test="${not empty productReference.target.discount.percentage}">
                                    <div class="promo_label">- <fmt:formatNumber type="NUMBER"
                                                                                 value="${productReference.target.discount.percentage}"
                                                                                 maxFractionDigits="0"/>%
                                    </div>


                                </c:if>
                                <c:if test="${productReference.target.frozen}">
                                    <div class="label-frozen" title='<spring:theme
                                                        code="plp.title.frozen"/>'>
                                        <div class="thumb"></div>

                                    </div>
                                </c:if>
                                <c:if test="${productReference.target.chilled}">
                                    <div class="label-chilled" title='<spring:theme
                                                        code="plp.title.chilled"/>'>
                                        <div class="thumb"></div>

                                    </div>
                                </c:if>
                                <c:if test="${productReference.target.dry}">
                                    <div class="label-Dry" title='<spring:theme
                                                        code="plp.title.dry"/>'>
                                        <div class="thumb"></div>

                                    </div>
                                </c:if>

                            </div>
                            
                            <c:set value="" var="isout"></c:set>
                            <c:set value="hidden" var="isin"></c:set>
                            <c:if test="${productReference.target.inWishlist}">
                                <c:set value="" var="isin"></c:set>
                                <c:set value="hidden" var="isout"></c:set>
                            </c:if>
                            <c:if test="${!productReference.target.inWishlist}">
                                <c:set value="hidden" var="isin"></c:set>
                                <c:set value="" var="isout"></c:set>
                            </c:if>

                            <span class="wishlist_icon">
	<a href="${wishlist_link}" title="wishlist" class="removeWishlistEntry wishlistbtn ${isin}"
       data-productcode="${productReference.target.code}" data-pk=""><i class="fas fa-heart"></i></a>
	<a href="${wishlist_link}" title="wishlist" class="addWishlistEntry wishlistbtn ${isout}"
       data-productcode="${productReference.target.code}" data-pk=""><i class="far fa-heart"></i></a>
				</span>
                            <a href="${productUrl}">
                                <c:if test="${not empty productReference.target.productLabel}">
                                    <div class="carousel__item--label">${productReference.target.productLabel}</div>
                                </c:if>
                                <product:productPrimaryImage product="${productReference.target}" format="zoom"/>

                                <span class="smile smile_owl "></span>
                            </a>
                        </div>

                        <div class="cont_detail_carousel">
                            <a href="${productUrl}">
                                <div class="cont_href_div">

                                    <div class="carousel__item--name">

                                        <c:choose>
                                            <c:when test="${fn:length(productReference.target.name) > 44}">
                                                <c:out value="${fn:substring(productReference.target.name, 0, 44)}..."/>
                                            </c:when>
                                            <c:otherwise>
                                                ${fn:escapeXml(productReference.target.name)}
                                            </c:otherwise>
                                        </c:choose>

                                    </div>


                                    <c:if test="${not empty productReference.target.countryOfOrigin}">
                                        <div class="carousel__item--countryoforigin hidden">${productReference.target.countryOfOrigin}</div>
                                        <c:set value="${fn:replace(productReference.target.countryOfOriginIsocode, ' ', '-')}"
                                               var="countryOfOrigin"></c:set>
                                        <div class="carousel__item--countryoforiginisocode"><i
                                                class="flagicon ${fn:toLowerCase(countryOfOrigin)}"></i>${productReference.target.countryOfOrigin}
                                            /
                                        </div>
                                    </c:if>
                                    <div class="carousel__item--unitofmeasure hidden">${productReference.target.unitOfMeasure}</div>
                                    <div class="carousel__item--unitofmeasuredescription">${productReference.target.unitOfMeasureDescription}</div>
                                </div>
                            </a>
                            <div class="price_counter">
                            	<c:if test="${productReference.target.stock.stockLevelStatus.code eq 'outOfStock'}">
                            		<span class="outstock">
                            			<i class="fas fa-times-circle"></i>
                            			<spring:theme code='product.variants.out.of.stock'/>
                            		</span>
                            	</c:if>
                            	<div class="carousel__item--price">
                                    <c:choose>
                                        <c:when test="${not empty productReference.target.discount}">

                                            <p class="price"><format:fromPrice
                                                    priceData="${productReference.target.discount.discountPrice}"/></p>
                                            <span class="scratched"><format:fromPrice
                                                    priceData="${productReference.target.discount.price}"/><span
                                                    class="line_dis"></span></span>
                                        </c:when>
                                        <c:otherwise>

                                            <p class="price"><format:fromPrice
                                                    priceData="${productReference.target.price}"/></p>
                                            <span class="scratched"></span>
                                        </c:otherwise>
                                    </c:choose>

                                </div>
                                <product:addtocartcarousel showQuantityBox="true" product="${productReference.target}"/>
                                <c:if test="${product.express}">
												<span class="expressdelivery"><img
                                                        src="${fn:escapeXml(themeResourcePath)}/images/express.png"/>
												</span>
                                </c:if>
                            </div>
                        </div>
                        </a>

                    </div>

                </c:forEach>
            </div>
        </div>
    </c:when>

    <c:otherwise>
        <component:emptyComponent/>
    </c:otherwise>
</c:choose>
