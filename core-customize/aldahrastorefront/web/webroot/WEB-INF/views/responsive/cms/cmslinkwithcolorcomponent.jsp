<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<c:if test="${component.visible}">
	<div class="topBanner"
		style="background:${backgroundColor};color:${textColor};text-align:${topBannerTextalignment}">
		<c:choose>
			<c:when test="${not empty url}">
				<a href="${url}" title="${linkName}" ${target.code eq 'SAMEWINDOW' ?  "target='_blank'" : "" } style="color:${textColor};">
					${content}
				</a>
			</c:when>
			<c:otherwise>
                <span style="color:${textColor};">${content}</span>

            </c:otherwise>

		</c:choose>
	</div>
</c:if>




