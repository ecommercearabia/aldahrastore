<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<spring:htmlEscape defaultHtmlEscape="true" />

<c:choose>
	<c:when test="${not empty tabbedProductCarouselComponentData.productCarouselComponents}">
	
	<h2 class="headline">${tabbedProductCarouselComponentData.title}</h2>
	<div class="tabs js-tabs">
    <div class="tabs-nav js-tabs-nav" id="fade-example">
	<ul class="tabs-nav__list  scroll" role="tablist">
	
	<c:forEach items="${tabbedProductCarouselComponentData.productCarouselComponents}" var="productCarouselComponentData" varStatus="loop">
		<c:set value="" var="start_tab"></c:set>
		<c:if test="${loop.index eq 0}">
			<c:set value="active" var="start_tab"></c:set>
		</c:if>
		<li class="tabs-nav__item js-tabs-item ${start_tab}" role="tab"><a class="tabs-nav__link js-tabs-link " data-toggle="tab" href="#tab-${loop.index}" role="link" data-tab="tab-${loop.index}">${productCarouselComponentData.title}</a></li>
	</c:forEach>
	</ul>
	<!--  <span class="tabs-nav__drag js-tabs-drag"></span> -->
	</div>
	<div class="tabs-content js-tabs-wrap">
		<c:forEach items="${tabbedProductCarouselComponentData.productCarouselComponents}" var="productCarouselComponentData" varStatus="loop">
		<c:set value="" var="start_tab"></c:set>
		<c:if test="${loop.index eq 0}">
			<c:set value="active" var="start_tab"></c:set>
		</c:if>
		 <div class="tab js-tabs-content ${start_tab}" id="tab-${loop.index}" role="tabpanel">
			<product:productCarouselComponent productCarouselComponentData="${productCarouselComponentData}"/>
		</div>
		
		</c:forEach>
	</div>
	
	</div>
	</c:when>

	<c:otherwise>
		<component:emptyComponent/>
	</c:otherwise>
</c:choose>
