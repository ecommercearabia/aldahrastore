<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>

<div id="product-facet" class="hidden-sm hidden-xs product__facet js-product-facet">
    <nav:facetNavAppliedFilters pageData="${searchPageData}"/>
  <div class="filterSection">  <nav:facetNavRefinements pageData="${searchPageData}"/></div>
</div>