<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/responsive/action"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<spring:htmlEscape defaultHtmlEscape="true" />

<c:set var="isForceInStock"
	value="${product.stock.stockLevelStatus.code eq 'inStock' and empty product.stock.stockLevel}" />
<c:choose>
	<c:when test="${isForceInStock}">
		<c:set var="maxQty" value="1000" />
	</c:when>
	<c:otherwise>
		<c:set var="maxQty" value="${product.stock.stockLevel}" />
	</c:otherwise>
</c:choose>

<c:set var="qtyMinus" value="1" />

<div class="addtocart-component addcart_pdp">

	<c:if test="${empty showAddToCart ? true : showAddToCart}">
		<div class="actions">
			<c:if test="${multiDimensionalProduct}">
				<c:url value="${product.url}/orderForm" var="productOrderFormUrl" />
				<a href="${productOrderFormUrl}"
					class="btn btn-default btn-block btn-icon js-add-to-cart glyphicon-list-alt">
					<spring:theme code="order.form" />
				</a>
			</c:if>
			<action:actions element="div" parentComponent="${component}" />
		</div>
		<div class="qty-selector input-group js-qty-selector qty_pdp">
			<span class=""> <button class="js-qty-selector-minus btnqty" type="button" aria-label="minus"
				<c:if test="${qtyMinus <= 1}"><c:out value="disabled='disabled'"/></c:if>><span
					class="far fa-minus" aria-hidden="true"></span>
				</button> </span> <input type="text" maxlength="3" class="js-qty-selector-input"
				size="1" value="${fn:escapeXml(qtyMinus)}"
				data-max="${fn:escapeXml(maxQty)}" data-min="1"
				name="pdpAddtoCartInput" id="pdpAddtoCartInput" /> <span class="">
					<button class="js-qty-selector-plus btnqty" aria-label="plus" type="button"><span
						class="far fa-plus" aria-hidden="true"></span></span>
			</span>
		</div>
		<br />
	</c:if>
	<c:if test="${product.stock.stockLevel gt 0}">
		<c:set var="productStockLevelHtml">
			<i class="fas fa-check-circle"></i>
			<spring:theme code="product.variants.in.stock" />
		</c:set>
	</c:if>
	<%-- 		<c:if test="${product.stock.stockLevel le 4}"> --%>
	<%-- 			<c:set var="productStockLevelHtml">${fn:escapeXml(product.stock.stockLevel)}&nbsp; --%>
	<%-- 				<i class="fas fa-check-circle"></i><spring:theme code="product.variants.in.stock"/> --%>
	<%-- 			</c:set> --%>
	<%-- 		</c:if> --%>
	<c:if test="${product.stock.stockLevel eq 0}">
		<c:set var="productStockLevelHtml">
			<i class="fas fa-times-circle red"></i>
			<spring:theme code="product.variants.out.of.stock" />
		</c:set>
	</c:if>
	<%-- 		<c:if test="${product.stock.stockLevelStatus.code eq 'lowStock'}"> --%>
	<%-- 			<c:set var="productStockLevelHtml"> --%>
	<%-- 				<i class="fas fa-check-circle"></i> <spring:theme code="product.variants.only.left" arguments="${product.stock.stockLevel}"/> --%>
	<%-- 			</c:set> --%>
	<%-- 		</c:if> --%>
	<c:if test="${isForceInStock}">
		<i class="fas fa-check-circle"></i>
		<spring:theme code="product.variants.in.stock" />
	</c:if>
	<div class="stock-wrapper clearfix">${productStockLevelHtml}</div>

 <div class="addedtocart">
	<spring:theme code="basket.added.to.basket"/>
</div>
<div class="addedtocarterror">
	
</div>
<div class="addedtocartinfo">
	
</div>
</div>
