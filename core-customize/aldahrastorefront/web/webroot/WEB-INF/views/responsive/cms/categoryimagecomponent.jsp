<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div class="row">
    <c:if test="${not empty wideImage}">
        <div class="col-md-12 col-sm-12 hidden-xs">
            <img title="${fn:escapeXml(wideImage.altText)}" alt="${fn:escapeXml(wideImage.altText)}"
                 src="${fn:escapeXml(wideImage.url)}">
        </div>
    </c:if>
    <c:if test="${not empty responsiveImage}">
        <div class="col-md-12 col-sm-12 col-xs-12 hidden-sm hidden-md hidden-lg">
            <img title="${fn:escapeXml(responsiveImage.altText)}" alt="${fn:escapeXml(responsiveImage.altText)}"
                 src="${fn:escapeXml(responsiveImage.url)}">
        </div>
    </c:if>
    <c:if test="${not empty categoryName}">
        <div class="headline">
            <h1>${categoryName}</h1>
        </div>
    </c:if>

    <c:if test="${not empty categoryContent }">

        <div class="headline2"> 
            ${categoryContent}
            <a href="javascript:;" class="show_more hidden"><spring:theme code="review.show.more"/></a> <a href="javascript:;" class="show_less hidden"><spring:theme code="review.show.less"/></a>
        </div>
    </c:if>


</div>

