<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>

<c:url value="${component.urlLink}" var="bannerUrl" />
<c:url value="${component.external}" var="external" />

<div class="banner__component simple-banner mega-hover">


	<img title="${fn:escapeXml(component.media.altText)}"
		alt="${fn:escapeXml(component.media.altText)}"
		src="${fn:escapeXml(component.media.url)}">


</div>
<c:if test="${not empty component.content}">
	<h3>${component.content}</h3>
</c:if>