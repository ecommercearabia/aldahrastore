<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">

<div class="row">
    <div class="col-sm-6">
	    <div class="checkout-headline">
            <span class="far fa-lock"></span>
            <spring:theme code="checkout.multi.secure.checkout" />
        </div>
        <multi-checkout:checkoutSteps checkoutSteps="${checkoutSteps}" progressBarId="${progressBarId}">
            <jsp:body>
                <ycommerce:testId code="checkoutStepOne">
                    <div class="checkout-shipping">
<%--                             <multi-checkout:shipmentItems cartData="${cartData}" showDeliveryAddress="false" /> --%>

                            <div class="checkout-indent">
                            
                                <div class="headline">
                                    <spring:theme code="checkout.summary.shippingAddress" /></div>
                                                        <c:if test="${not empty cartData.deliveryAddress}">
                                                               <div class="default_Add"> 
                                                                   <strong><c:if test="${ not empty cartData.deliveryAddress.title }"> ${fn:escapeXml(cartData.deliveryAddress.title)}&nbsp;</c:if>
                                                                ${fn:escapeXml(cartData.deliveryAddress.firstName)}&nbsp;
                                                                ${fn:escapeXml(cartData.deliveryAddress.lastName)}&nbsp;</strong>
                                                                <br>
                                                                <c:choose>
                                                                	<c:when test="${not empty cartData.deliveryAddress.streetName}">
                                                                		${fn:escapeXml(cartData.deliveryAddress.streetName)} <br>
                                                                	</c:when>
                                                                	<c:otherwise>
                                                                		${fn:escapeXml(cartData.deliveryAddress.line1)} <br>
                                                                	</c:otherwise>
                                                                </c:choose>
                                                                <c:if test="${not empty cartData.deliveryAddress.buildingName}">
                                                                	&nbsp;${fn:escapeXml(cartData.deliveryAddress.buildingName)}<br>
                                                                </c:if>
                                                                <c:if test="${not empty cartData.deliveryAddress.apartmentNumber}">
                                                                	&nbsp;${fn:escapeXml(cartData.deliveryAddress.apartmentNumber)}<br>
                                                                </c:if>
                                                                ${fn:escapeXml(cartData.deliveryAddress.country.name)}<br>
                                                                <c:if test="${not empty cartData.deliveryAddress.city}">
																	${fn:escapeXml(cartData.deliveryAddress.city.name)}
																<br></c:if>
																
																<c:if test="${not empty cartData.deliveryAddress.area}">
																	${fn:escapeXml(cartData.deliveryAddress.area.name)}
																<br></c:if>
																<c:if test="${not empty cartData.deliveryAddress.nearestLandmark}">${fn:escapeXml(cartData.deliveryAddress.nearestLandmark)}<br>
															</c:if>	<c:if test="${not empty cartData.deliveryAddress.mobileNumber}">${fn:escapeXml(cartData.deliveryAddress.mobileNumber)}<br></c:if>
																</div>
																<spring:theme code="checkout.multi.deliveryAddress.default.is.shown"/>
														</c:if>
                                    <address:addressFormSelector supportedCountries="${countries}"
                                        regions="${regions}" cancelUrl="${currentStepUrl}"
                                        country="${country}" />


                                        <address:suggestedAddresses selectedAddressUrl="/checkout/multi/delivery-address/select" />
                                         <multi-checkout:pickupGroups cartData="${cartData}" />
                                 <button id="addressSubmit" type="button"
                        class="btn btn-primary btn-block checkout-next"><spring:theme code="checkout.multi.deliveryAddress.continue"/></button>
                            </div>

                               
                    </div>


                   
                </ycommerce:testId>
            </jsp:body>
        </multi-checkout:checkoutSteps>
    </div>

    <div class="col-sm-6 hidden-xs">
		<multi-checkout:checkoutOrderDetails cartData="${cartData}" showDeliveryAddress="false" showPaymentInfo="false" showTaxEstimate="false" showTax="true" />
    </div>

    <div class="col-sm-12 col-lg-12">
        <cms:pageSlot position="SideContent" var="feature" element="div" class="checkout-help">
            <cms:component component="${feature}"/>
        </cms:pageSlot>
    </div>
</div>

</template:page>
