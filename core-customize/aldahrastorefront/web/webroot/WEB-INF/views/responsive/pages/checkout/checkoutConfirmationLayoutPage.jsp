<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order"%>
<template:page pageTitle="${pageTitle}">
    <cms:pageSlot position="SideContent" var="feature" class="accountPageSideContent" element="div">
        <cms:component component="${feature}" element="div" class="accountPageSideContent-component"/>
    </cms:pageSlot>
    <cms:pageSlot position="TopContent" var="feature" element="div" class="accountPageTopContent">
        <cms:component component="${feature}" element="div" class="accountPageTopContent-component"/>
    </cms:pageSlot>
    <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')"><div class="account-section">
        <cms:pageSlot position="BodyContent" var="feature" element="div" class="account-section-content checkout__confirmation__content">
            <cms:component component="${feature}" element="div" class="checkout__confirmation__content--component"/>
        </cms:pageSlot>
    </div></sec:authorize>
    <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')"><div class="account-section">
        <div class="account-section-content checkout__confirmation__content">
        	<div class="checkout__confirmation__content--component">
        		<order:orderConfirmation/>
        	</div>
        </div>
    </div></sec:authorize>
    <cms:pageSlot position="BottomContent" var="feature" element="div" class="accountPageBottomContent">
        <cms:component component="${feature}" element="div" class="accountPageBottomContent-component"/>
    </cms:pageSlot>
</template:page>