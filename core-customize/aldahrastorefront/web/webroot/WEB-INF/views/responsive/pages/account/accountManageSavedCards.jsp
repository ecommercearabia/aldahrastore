<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="wishlist" tagdir="/WEB-INF/tags/responsive/wishlist" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<spring:htmlEscape defaultHtmlEscape="true"/>


<c:choose>
    <c:when test="${not empty customerPaymentOptions}">
        <table class="orderhistory-list-table responsive-table manageSavedCard">
            <tr class="account-orderhistory-table-head responsive-table-head hidden-xs">
                <th><spring:theme code="customer.card.name.label"/></th>
                <th><spring:theme code="customer.card.type.label"/></th>
                <th><spring:theme code="customer.card.id.label"/></th>
                <th><spring:theme code="customer.email.label"/></th>
                <th><spring:theme code="customer.payopt.type.label"/></th>
                <th><spring:theme code="customer.card.label.label"/></th>
                <th><spring:theme code="customer.card.no.label"/></th>
                <th><spring:theme code='customer.payment.options.delete.btn'/></th>
            </tr>
            <c:forEach items="${customerPaymentOptions}" var="customerPaymentOption">
                <tr class="responsive-table-item">
                    <td class="hidden-sm hidden-md hidden-lg"><spring:theme code="customer.card.name.label"/>:</td>
                    <td class="responsive-table-cell"> ${customerPaymentOption.customerCardName}</td>
                    <td class="hidden-sm hidden-md hidden-lg"><spring:theme code="customer.card.type.label"/>:</td>
                    <td class="responsive-table-cell"> ${customerPaymentOption.customerCardType}</td>
                    <td class="hidden-sm hidden-md hidden-lg"><spring:theme code="customer.card.id.label"/>:</td>
                    <td class="responsive-table-cell"> ${customerPaymentOption.customerCardId}</td>
                    <td class="hidden-sm hidden-md hidden-lg"><spring:theme code="customer.email.label"/>:</td>
                    <td class="responsive-table-cell email-cards"> ${customerPaymentOption.customerEmail}</td>
                    <td class="hidden-sm hidden-md hidden-lg"><spring:theme code="customer.payopt.type.label"/>:</td>
                    <td class="responsive-table-cell"><spring:theme
                            code="customer.payopt.type.${customerPaymentOption.customerPayoptType}"/></td>
                    <td class="hidden-sm hidden-md hidden-lg"><spring:theme code="customer.card.label.label"/>:</td>
                    <td class="responsive-table-cell"> ${customerPaymentOption.customerCardLabel}</td>
                    <td class="hidden-sm hidden-md hidden-lg"><spring:theme code="customer.card.no.label"/>:</td>
                    <td class="responsive-table-cell">${customerPaymentOption.customerCardNo}</td>
                    <td class="hidden"><spring:theme code='customer.payment.options.delete.btn'/></td>
                    <td class="responsive-table-cell delete-card">
                        <c:url value="/my-account/manage-saved-cards/delete/${customerPaymentOption.customerCardId}"
                               var="deleteURL"/>
                        <form:form method="post" action="${deleteURL }">

                            <button type="submit" class="btn btn-default ">
                                <spring:theme code='customer.payment.options.delete.btn'/>
                            </button>
                        </form:form>
                    </td>
                </tr>
            </c:forEach>

        </table>
    </c:when>
    <c:otherwise>
        <spring:theme code="customer.payment.options.is.empty"/>
    </c:otherwise>
</c:choose>