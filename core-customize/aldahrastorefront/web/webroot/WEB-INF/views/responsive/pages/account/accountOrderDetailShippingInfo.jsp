<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<spring:htmlEscape defaultHtmlEscape="true" />

<div class="account-orderdetail well well-tertiary">
 
    <c:choose>
<c:when test="${not empty orderData.paymentMode.code && fn:toLowerCase(orderData.paymentMode.code) != 'cod' && fn:toLowerCase(orderData.paymentMode.code) != 'continue' && fn:toLowerCase(orderData.paymentMode.code) != 'pis'}">
      <div class="well-headline">
        <spring:theme code="text.account.order.orderDetails.billingInformtion" />
    </div>
    <ycommerce:testId code="orderDetails_paymentDetails_section">
        <div class="well-content">
            <div class="row">
                <div class="col-sm-12 col-md-9">
                    <div class="row">
                        <c:if test="${not empty orderData.paymentInfo}">
                            <div class="col-sm-6 col-md-4 order-payment-data">
                                <order:paymentDetailsItem order="${orderData}"/>
                            </div>
                        </c:if>
                    </div>
                </div>
            </div>
        </div>
    </ycommerce:testId>
    </c:when>
<c:otherwise>
   <div class="well-headline">
        <spring:theme code="text.account.order.orderDetails.paymentMode" />
    </div>
	<ycommerce:testId code="orderDetails_paymentDetails_section">
        <div class="well-content">
            <div class="row">
                <div class="col-sm-12 col-md-9">
                    <div class="row">
                        <c:if test="${not empty orderData.paymentMode.name}">
                            <div class="col-sm-6 col-md-4 order-payment-data">
                                ${orderData.paymentMode.name}
                            </div>
                        </c:if>
                    </div>
                </div>
            </div>
        </div>
    </ycommerce:testId>
</c:otherwise>
</c:choose>
</div>
