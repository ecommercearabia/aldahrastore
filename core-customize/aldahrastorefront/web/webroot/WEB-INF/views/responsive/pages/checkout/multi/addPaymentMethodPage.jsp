<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="multiCheckout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<spring:htmlEscape defaultHtmlEscape="true" />

<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">

    <div class="row">
        <div class="col-sm-6">
            <div class="checkout-headline">
                <span class="fal fa-lock-alt"></span>
                <spring:theme code="checkout.multi.secure.checkout"/>
            </div>
            <multiCheckout:checkoutSteps checkoutSteps="${checkoutSteps}" progressBarId="${progressBarId}">
                <jsp:body>
                    <ycommerce:testId code="checkoutStepFour">
					
                        <div class="checkout-paymentmethod">
                            <div class="checkout-indent">
                            <c:if test="${availableBalanceStoreCredit.value > 0}">
                            	<div class="headline"><spring:theme code="checkout.summary.storeCredit.selectStoreCreditForOrder" /></div>
                            	<c:if test="${not empty storeCreditLimitMsg }"><div class="alert alert-danger">${storeCreditLimitMsg}</div></c:if>
							<spring:url var="selectStoreCreditUrl" value="{contextPath}/checkout/multi/payment-method/store-credit/choose" htmlEscape="false" >
								<spring:param name="contextPath" value="${request.contextPath}" />
							</spring:url>
								<form:form id="selectStoreCreditForm" action="${fn:escapeXml(selectStoreCreditUrl)}" method="post" modelAttribute="storeCreditForm">
								<div class="form-group">
								
									<c:forEach items="${supportedStoreCreditModes}" var="storeCreditMode" varStatus="loop">
										<c:set var="select_defult" value=""/>
										<c:set var="select_input" value="hidden"/>
										
										<c:if test="${cartData.storeCreditMode.storeCreditModeType.code eq 'REDEEM_SPECIFIC_AMOUNT'}">
										<c:set var="select_input" value=""/>
										</c:if>
										
										<c:if test="${cartData.storeCreditMode.storeCreditModeType.code eq storeCreditMode.storeCreditModeType.code}">
										<c:set var="select_defult" value="checked"/>
										</c:if>
										<label class="container-radio" for="${storeCreditMode.storeCreditModeType.code}">
											${storeCreditMode.name}
										
										<input type="radio" name="storeCredit" id="${storeCreditMode.storeCreditModeType.code}" value="" ${select_defult}/>
										<span class="checkmark"></span></label><br/>
									</c:forEach>
									<br>
									 <input id="sctCode" name="sctCode" value="${cartData.storeCreditMode.storeCreditModeType.code}" />
									<div class="storeCreditAmount ${select_input}">
									<input id="scAmount" type="number" min="0" name="scAmount" class="form-control" placeholder="storeCreditAmount" value="${cartData.storeCreditAmount.value}" onkeypress="return fun_AllowOnlyAmountAndDot(this.id);"/><br>
									</div>
									
									<div class="balance"><span class="available"><spring:theme code="checkout.multi.storeCredit.availableBalance"/></span> <span class="aed">${availableBalanceStoreCredit.formattedValue}</span><span class="smile"></span></div>
								</div>
							</form:form>
							<button id="storeCreditSubmit" type="button" class="btn btn-primary btn-block checkout-next"><spring:theme code="checkout.multi.storeCredit.confirm"/></button>
                            </c:if>
                                <div class="headline hidden"><spring:theme code="checkout.multi.paymentMethod"/></div>
							<spring:url var="selectPaymentMethodUrl" value="{contextPath}/checkout/multi/payment-method/add" htmlEscape="false" >
								<spring:param name="contextPath" value="${request.contextPath}" />
							</spring:url>
<%-- 									<c:if test="${availableBalanceStoreCredit.value > 0}"> --%>
<%-- 										<spring:theme code="checkout.multi.paymentMethodWithStoreCredit"/> --%>
<%-- 									</c:if> --%>

								
								<form:form method="post" modelAttribute="paymentDetailsForm" action="${selectPaymentMethodUrl}" id="selectPaymentMethodForm">
								
								<formElement:formSelectBoxDefaultEnabled idKey="payment.method."
									labelKey="payment.method.title" selectCSSClass="form-control hidden"
									path="paymentModeCode" mandatory="true" skipBlank="false"
									skipBlankMessageKey="form.select.none" items="${supportedPaymentModes}" />
								</form:form>
								
								<c:forEach items="${supportedPaymentModes}" var="type">
								
								<label class="container-radio" for="${type.code}">
								<input id="${type.code}" type="radio" name="paymentmothed"  value="${type.code}"/>
								<c:if test="${type.code eq 'ccod'}"><i class="fal fa-scanner-keyboard"></i> ${type.name}</c:if>
								<c:if test="${type.code eq 'cod'}"><i class="fal fa-sack-dollar"></i> ${type.name}</c:if>
								<c:if test="${type.code eq 'card'}"><i class="fal fa-credit-card"></i> ${type.name}</c:if>
								<c:if test="${type.code eq 'pis'}"><i class="fas fa-truck-pickup"></i> ${type.name}</c:if>
								<c:if test="${type.code eq 'continue'}">${type.name}</c:if>
								<span class="checkmark"></span>
								</label>
								
								</c:forEach>
								

<%-- 								<form id="selectPaymentMethodForm" action="${fn:escapeXml(selectPaymentMethodUrl)}" method="post"> --%>
<!-- 									<div class="form-group"> -->
<%-- 										<multi-checkout:paymentMethodSelector paymentMethods="${supportedPaymentModes}" selectedPaymentMethodId="${cartData.paymentMode.code}"/> --%>
<!-- 									</div> -->
<%-- 								</form> --%>

                            </div>
                            <button id="paymentMethodSubmit" type="button" class="btn btn-primary btn-block "><spring:theme code="checkout.multi.deliveryMethod.continue"/></button>
                        </div>
					

                    </ycommerce:testId>
                    
               </jsp:body>

            </multiCheckout:checkoutSteps>
		</div>

        <div class="col-sm-6 hidden-xs">
            <multiCheckout:checkoutOrderDetails cartData="${cartData}" showDeliveryAddress="true" showPaymentInfo="false" showTaxEstimate="false" showTax="true" />
        </div>

		<div class="col-sm-12 col-lg-12">
			<cms:pageSlot position="SideContent" var="feature" element="div" class="checkout-help">
				<cms:component component="${feature}"/>
			</cms:pageSlot>
		</div>
	</div>

</template:page>
