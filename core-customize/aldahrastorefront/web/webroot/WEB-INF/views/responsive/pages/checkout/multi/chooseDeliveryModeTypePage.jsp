<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">

<div class="row">
    <div class="col-sm-6">
        <div class="checkout-headline">
            <span class="fal fa-lock-alt"></span>
            <spring:theme code="checkout.multi.secure.checkout" />
        </div>
		<multi-checkout:checkoutSteps checkoutSteps="${checkoutSteps}" progressBarId="${progressBarId}">
			<jsp:body>
				<ycommerce:testId code="checkoutStepThree">
					<div class="checkout-shipping">
						<div class="checkout-indent">
							
							<spring:url var="selectDeliveryModeUrl" value="{contextPath}/checkout/multi/time-slot/select-delivery" htmlEscape="false" >
								<spring:param name="contextPath" value="${request.contextPath}" />
							</spring:url>
								
                      <form:form id="selectDeliveryModeForm" action="${fn:escapeXml(selectDeliveryModeUrl)}" method="POST" modelAttribute="selectDeliveryModeForm">
                              		<div class="form-group">
                              <br>
                              		<c:if test="${not empty warningMsg}">
                              		     <p style="color:red;"><strong> ${warningMsg}</strong></p>
                              		</c:if>
                              	<br/>
                                <div><strong>${baseStore.deliveryModeMessage}</strong></div>								
                             <br/>
                           <c:forEach items="${deliveryModeTypes}" var="deliveryModeType" varStatus="loop">
                             	<c:set var="select_defult" value=""/>
						     	<input type="radio" name="deliveryModeCode" id="${deliveryModeType.code}" value="${deliveryModeType.code}" ${select_defult}/>
										
							 <label class="container-radio" for="${deliveryModeType.code}"> ${deliveryModeType.name}
										
							 <span class="checkmark"></span></label><br/>
						</c:forEach>									  
						 
												
						<button id="deliveryModeSubmit" type="button" class="btn btn-primary btn-block checkout-next express-btn">
						<spring:theme code="checkout.multi.deliveryMode.confirm"/>
						</button>
							</div>

							</form:form>

						</div>
						</div>
					
				</ycommerce:testId>
			</jsp:body>
		</multi-checkout:checkoutSteps>
    </div>

    <div class="col-sm-6 hidden-xs">
		<multi-checkout:checkoutOrderDetails cartData="${cartData}" showDeliveryAddress="true" showPaymentInfo="false" showTaxEstimate="false" showTax="true" />
    </div>

    <div class="col-sm-12 col-lg-12">
        <cms:pageSlot position="SideContent" var="feature" element="div" class="checkout-help">
            <cms:component component="${feature}"/>
        </cms:pageSlot>
    </div>
</div>

</template:page>
