<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<template:page pageTitle="${pageTitle}">

		<div class="container__full">
		<div class="row">
		<div class="col-lg-2 col-md-1"></div>
<cms:pageSlot position="ExpressInfoSection1" var="feature">
       <cms:component component="${feature}" element="div" class="yComponentWrapper col-lg-8 col-md-10 col-sm-12 col-xs-12"/>
</cms:pageSlot>
<div class="col-lg-2 col-md-1"></div>
	</div>
	</div>



</template:page>