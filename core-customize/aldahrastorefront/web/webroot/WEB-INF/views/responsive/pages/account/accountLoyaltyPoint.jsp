<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<spring:htmlEscape defaultHtmlEscape="true" />

<c:url  value="/loyalty-info" var="loyaltyInfoUrl" />
	
	
	
<div class="row">
	<div class="col-md-6">
	<div class="row">
		<cms:pageSlot position="SideContent" var="feature" element="div" class="col-md-5 col-sm-7 col-xs-8">
			<cms:component component="${feature}" />
		</cms:pageSlot>

		
		</div>
	<p><br/><spring:theme code='text.account.loyaltypoint.welcome'/></p>
	<p><spring:theme code='text.account.loyaltypoint.cashinfo'/></p>
	<br>
	<p><b><spring:theme code='text.account.loyaltypoint.earn'/></b></p>
	<br>
	<p><spring:theme code='text.account.loyaltypoint.earn2'/></p>
	<br>		
	<p> <spring:theme code='text.account.loyaltypoint.infoclick'/>
	
      <a href="${loyaltyInfoUrl}" target="_blank" style="text-decoration: underline">
      <spring:theme code='text.account.loyaltypoint.infohere'/></a>
 
      <spring:theme code='text.account.loyaltypoint.infomore'/>
	</p>
	</div>
		<div class="container-lg col-md-6">
<br/>
<br>
<br/>
<br>
<br/><br>
<p><b><spring:theme code='text.account.loyaltypoint.availablebalance'/> <span class="red_color"> ${loyaltyPoints}</span></b></p>
<%-- <fmt:formatDate value="${order.created}" dateStyle="short" timeStyle="medium" type="time" pattern = "hh:mm" /> <br/> --%>
<%-- 						<fmt:formatDate value="${order.created}" dateStyle="long" timeStyle="long" type="date" pattern = "EEEE, dd MMMM YYYY"/> <br/> --%>

<div class="account-overview-table">
				<table class="orderhistory-list-table responsive-table">
					<tr class="account-orderhistory-table-head responsive-table-head hidden-xs">
					 <th id="header6" class="responsive-table-cell head_account_cridit"><spring:theme code='text.account.loyaltypoint.ordernumber'/></th>
						<th id="header1" class="responsive-table-cell head_account_cridit"><spring:theme code='text.account.loyaltypoint.date'/></th>
<%-- 	                <th id="header2" class="head_account_cridit"><spring:theme code='text.account.loyaltypoint.time'/></th> --%>
	                        <th id="header3" class="responsive-table-cell head_account_cridit"><spring:theme code='text.account.loyaltypoint.points'/></th>
	                        <th id="header4" class="responsive-table-cell head_account_cridit"><spring:theme code='text.account.loyaltypoint.balance.points'/></th>
	                        <th id="header5" class="responsive-table-cell head_account_cridit"><spring:theme code='text.account.loyaltypoint.cashearned'/></th>
	                       
					</tr>
					
		 
					<c:forEach items="${loyaltyPointHistoryDatas}" var="loyaltyPointHistory">
						<tr class="responsive-table-item">
							<ycommerce:testId code="orderHistoryItem_orderDetails_link">
								<td class="responsive-table-cell responsive-table-cell-bold">
								<span class="hidden-sm hidden-md hidden-lg"><spring:theme code='text.account.loyaltypoint.ordernumber'/></span>
								<c:url value="/my-account/order/${ycommerce:encodeUrl(loyaltyPointHistory.orderCode)}" var="orderURL" />
																		<a href="${orderURL}" class="red_color responsive-table-link ">
										${fn:escapeXml(loyaltyPointHistory.orderCode)}
									</a>
								</td>
								<td class="responsive-table-cell">
								<span class="hidden-sm hidden-md hidden-lg"><spring:theme code='text.account.loyaltypoint.date'/></span>
									<fmt:formatDate value="${loyaltyPointHistory.dateOfPurchase}" dateStyle="long" timeStyle="long" type="date" pattern = "MMM dd, YYYY"/>
								
								</td>
<%-- 								<td class="hidden-sm hidden-md hidden-lg"><spring:theme code='text.account.loyaltypoint.time'/></td>																 --%>
<!-- 								<td class="responsive-table-cell status"> -->
<%-- 								<fmt:formatDate value="${loyaltyPointHistory.dateOfPurchase}" dateStyle="short" timeStyle="long" type="time" pattern = "hh:mm a" /> --%>
								
<!-- 								</td> -->
								
								<td class="responsive-table-cell">
								<span class="hidden-sm hidden-md hidden-lg"><spring:theme code='text.account.loyaltypoint.points'/></span>
									<fmt:formatNumber type="number" maxFractionDigits="0" value="${loyaltyPointHistory.points}" />
								</td>
								
								<td class="responsive-table-cell">
								<span class="hidden-sm hidden-md hidden-lg"><spring:theme code='text.account.loyaltypoint.balance.points'/></span>
									
								<fmt:formatNumber type="number" maxFractionDigits="0" value="${loyaltyPointHistory.balancePoints}" />
									
								</td>
								
								<td class="responsive-table-cell">
								<span class="hidden-sm hidden-md hidden-lg"><spring:theme code='text.account.loyaltypoint.cashearned'/></span>
								<c:if test="${loyaltyPointHistory.actionType.code == 'REDEEM' }">
									<span>${loyaltyPointHistory.amount.currencyIso}</span>	
									<fmt:formatNumber type="number" maxFractionDigits="0" value="${loyaltyPointHistory.amount.value}" />
													
								</c:if>
								</td>
								
								
							</ycommerce:testId>
						</tr>
					</c:forEach>
					<c:if test="${empty loyaltyPointHistoryDatas}"><tr class="responsive-table-item"><td colspan="10" class="noData"><spring:theme code='msg.noDataItem'/></td></tr></c:if>
				</table>
            </div>

</div>
</div>


