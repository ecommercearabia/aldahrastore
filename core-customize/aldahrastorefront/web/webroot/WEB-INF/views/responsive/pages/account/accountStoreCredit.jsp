<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>

<spring:htmlEscape defaultHtmlEscape="true" />
<!-- <h2 class="account-section-header"> -->
<!-- 	<div class="row"> -->
<!-- 		<div class="container-lg col-md-6"> -->
<%-- 			<spring:theme code="text.account.storecredit" /> --%>
<!-- 		</div> -->
<!-- 	</div> -->
<!-- </h2> -->
<div class="row">

		<div class="container-lg col-md-6 text-center">
<br/><br/><br/>
<h2><b><spring:theme code='text.account.storecredit.availablebalance'/> <span class="red_color"> ${storeCreditAmount.formattedValue}</span></b></h2>
<br/><br/><br/>
<%-- <fmt:formatDate value="${order.created}" dateStyle="short" timeStyle="medium" type="time" pattern = "hh:mm" /> <br/> --%>
<%-- 						<fmt:formatDate value="${order.created}" dateStyle="long" timeStyle="long" type="date" pattern = "EEEE, dd MMMM YYYY"/> <br/> --%>

<!-- <div class="account-overview-table"> -->
<!-- 				<table class="orderhistory-list-table responsive-table"> -->
<!-- 					<tr class="account-orderhistory-table-head responsive-table-head hidden-xs"> -->
<%-- 						<th id="header1" class="head_account_cridit"><spring:theme code='text.account.storecredit.date'/></th> --%>
<%-- 	                        <th id="header2" class="head_account_cridit"><spring:theme code='text.account.storecredit.time'/></th> --%>
<%-- 	                        <th id="header3" class="head_account_cridit"><spring:theme code='text.account.storecredit.amount'/></th> --%>
<%-- 	                        <th id="header4" class="head_account_cridit"><spring:theme code='text.account.storecredit.balance'/></th> --%>
<%-- 	                        <th id="header4" class="head_account_cridit"><spring:theme code='text.account.storecredit.ordernumber'/></th> --%>
<!-- 					</tr> -->
<%-- 					<c:forEach items="${storeCreditHistories}" var="storeCreditHistory"> --%>
<!-- 						<tr class="responsive-table-item"> -->
<%-- 							<ycommerce:testId code="orderHistoryItem_orderDetails_link"> --%>
<%-- 								<td class="hidden-sm hidden-md hidden-lg"><spring:theme code='text.account.storecredit.date'/></td> --%>
<!-- 								<td class="responsive-table-cell"> -->
<%-- 									<fmt:formatDate value="${storeCreditHistory.dateOfPurchase}" dateStyle="long" timeStyle="long" type="date" pattern = "MMM dd, YYYY"/> --%>
								
<!-- 								</td> -->
<%-- 								<td class="hidden-sm hidden-md hidden-lg"><spring:theme code='text.account.storecredit.time'/></td>																 --%>
<!-- 								<td class="status"> -->
<%-- 								<fmt:formatDate value="${storeCreditHistory.dateOfPurchase}" dateStyle="short" timeStyle="long" type="time" pattern = "hh:mm a" /> --%>
								
<!-- 								</td> -->
<%-- 								<td class="hidden-sm hidden-md hidden-lg"><spring:theme code='text.account.storecredit.amount'/></td> --%>
<!-- 								<td class="responsive-table-cell"> -->
<%-- 									<format:fromPrice priceData="${storeCreditHistory.amount}"/> --%>
<!-- 								</td> -->
<%-- 								<td class="hidden-sm hidden-md hidden-lg"><spring:theme code='text.account.storecredit.balance'/></td> --%>
<!-- 								<td class="responsive-table-cell"> -->
<%-- 									<format:fromPrice priceData="${storeCreditHistory.balance}"/> --%>
<!-- 								</td> -->
<%-- 								<td class="hidden-sm hidden-md hidden-lg"><spring:theme code='text.account.storecredit.ordernumber'/></td> --%>
<!-- 								<td class="responsive-table-cell responsive-table-cell-bold"> -->
								
<%-- 								<c:url value="/my-account/order/${ycommerce:encodeUrl(storeCreditHistory.orderCode)}" var="orderURL" /> --%>
<%-- 																		<a href="${orderURL}" class="red_color responsive-table-link "> --%>
<%-- 										${fn:escapeXml(storeCreditHistory.orderCode)} --%>
<!-- 									</a> -->
<!-- 								</td> -->
<%-- 							</ycommerce:testId> --%>
<!-- 						</tr> -->
<%-- 					</c:forEach> --%>
<%-- 					<c:if test="${empty storeCreditHistories}"><tr class="responsive-table-item"><td colspan="10" class="noData"><spring:theme code='msg.noDataItem'/></td></tr></c:if> --%>
<!-- 				</table> -->
<!--             </div> -->

</div>
</div>


