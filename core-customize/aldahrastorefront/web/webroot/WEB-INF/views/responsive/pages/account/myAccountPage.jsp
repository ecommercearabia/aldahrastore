<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="account" tagdir="/WEB-INF/tags/responsive/account" %>
<spring:htmlEscape defaultHtmlEscape="true" />
<c:set var="TemplateReferral" value=""/>
<c:set var="TemplateCard" value=""/>
<c:if test="${baseStore.referralCodeEnable || cmsSite.enableManageSavedCards }">
	<c:set var="TemplateReferral" value="referralCode"/>
</c:if>


<c:if test="${ cmsSite.enableManageSavedCards }">
	<c:set var="TemplateCard" value="ACard"/>
</c:if>

<div class="row">

	<div class="col-md-4 col-sm-12">
		<div class="row">
			<div class="col-md-12 col-sm-6 col-xs-12">	<account:myOrders numberOfOrders="${numberOfOrders}"/></div>
			<div class="col-md-12 col-sm-6 col-xs-12"><account:personalDetails customer="${customerData}"/></div>
			<div class="col-md-12 col-sm-6 col-xs-12"><account:deliveryAddress defaultAddress="${customerData.defaultShippingAddress}"/></div>
			<div class="col-md-12 col-sm-6 col-xs-12">	<account:emailAddress email="${customerData.displayUid}"/></div>
			<div class="col-md-12 col-sm-6 col-xs-12">	<account:changePassword /></div>
			<c:if test="${cmsSite.enableManageSavedCards }">
				<div class="col-md-12 col-sm-6 col-xs-12">	<account:manageSavedCards /></div>
			</c:if>
			<div class="col-md-12 col-sm-6 col-xs-12 ${TemplateReferral}">
				<account:referralCode customer="${customerData}"/>
			</div>
			<c:if test="${baseStore.referralCodeEnable }">

			</c:if>
			
			<div class="col-md-12 col-sm-6 col-xs-12">	<account:consent /></div>
			
			
		</div>

	</div>
	<div class="col-md-8 col-sm-12">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<account:storeCredit amount="${storeCreditAmount}" /></div>
			
			<c:if test="${baseStore.enableLoyaltyPoint }">
				<div class="col-md-6 col-sm-6 col-xs-12">	<account:loyaltypoint /></div>
			</c:if>
			<div class="col-xs-12 referralCode ${TemplateCard}">
				<account:myWishlist wishlistEntries="${wishlistEntries}"/></div>
		</div>
	</div>

</div>