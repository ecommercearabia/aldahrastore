<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<spring:htmlEscape defaultHtmlEscape="true" />

<h2 class="account-section-header">
	<div class="row">
		<div class="container-lg col-md-6">
			<spring:theme code="text.account.profile.updatePersonalDetails" />
		</div>
	</div>
</h2>
<div class="row">
	<div class="container-lg col-md-6">
		<div class="account-section-content">
			<div class="account-section-form">
				<form:form action="update-profile" method="post"
					modelAttribute="updateProfileForm" >
					
					<input type="text" name="backtocart" value="${backToCart}" hidden="hidden">
					
					<formElement:formInputBox idKey="profile.backToCart"
						labelKey="profile.backToCart" path="backToCart" inputCSS="hidden"  disabled="true"   labelCSS="hidden"/>
						
						
<div class="row"><div class="col-sm-12 col-md-12">
							<formElement:formSelectBoxDefaultEnabled idKey="profile.title"
								labelKey="profile.title" path="titleCode" mandatory="true"
								skipBlank="false" skipBlankMessageKey="form.select.none"
								items="${titleData}" selectCSSClass="form-control" />
						</div></div>

<div class="row"><div class="col-md-12 col-sm-12">
					<formElement:formInputBox idKey="profile.firstName"
						labelKey="profile.firstName" path="firstName" inputCSS="text"
						mandatory="true" />
</div></div>
<div class="row"><div class="col-md-12 col-sm-12">
					<formElement:formInputBox idKey="profile.lastName"
						labelKey="profile.lastName" path="lastName" inputCSS="text"
						mandatory="true" />
</div></div>
<div class="row">
						<c:if test="${cmsSite.nationalityCustomerEnabled}">
							<div class="col-sm-12 col-md-12">
								<div
									class="<c:if test="${cmsSite.nationalityCustomerHidden}">hidden</c:if>">

									<formElement:formSelectBoxDefaultEnabled
										idKey="profile.nationality" labelKey="profile.nationality"
										selectCSSClass="form-control" path="nationality"
										mandatory="${cmsSite.nationalityCustomerRequired}"
										skipBlank="false" skipBlankMessageKey="form.select.none"
										items="${nationalities}" />
								</div>
							</div>
						</c:if>

						<c:if test="${cmsSite.nationalityIdCustomerEnabled}">
							<div class="col-sm-12 col-md-12">
								<div
									class="<c:if test="${cmsSite.nationalityIdCustomerHidden}">hidden</c:if>">
									<div class="checkbox profilecheckbox">


										<label class="control-label uncased"> <span
											class="hidden"> <form:checkbox id="nationalIdBox"
													path="hasNationalId" disabled="false" /></span> <label
											class="el-switch"> <input type="checkbox"
												name="switchNationalform" id="switchNationalform"> <span
												class="el-switch-style"></span> <span class="margin-r"><spring:theme
														code="profile.emirates.id.label" /></span>
										</label>
											<div class="FormNational">
												<formElement:formInputBox idKey="profile.national"
													placeholder="profile.national" labelKey="profile.national"
													path="nationalityId" inputCSS="form-control"
													mandatory="${cmsSite.nationalityIdCustomerRequired}" />
												<button class="btn">
													<spring:theme code="Verify" />
												</button>
											</div>
										</label>


									</div>
								</div>
							</div>
						</c:if>
						
					</div>

<div class="row"><div class="col-md-12 col-sm-12">
					<formElement:formCountrySelectBoxDefaultEnabled
						idKey="mobileCountry" labelKey="profile.mobileCountry"
						selectCSSClass="form-control countrypicker f16"
						path="mobileCountry" mandatory="true" skipBlank="false"
						skipBlankMessageKey="form.select.empty" items="${mobileCountries}" />
</div></div>
<div class="row"><div class="col-md-12 col-sm-12">
					<formElement:formInputBox idKey="profile.mobileNumber"
						labelKey="profile.mobileNumber" path="mobileNumber"
						inputCSS="form-control" mandatory="true" />
					</div></div>

					<div class="row">
						<div class="col-sm-6 col-sm-push-6">
							<div class="accountActions">
								<ycommerce:testId
									code="personalDetails_savePersonalDetails_button">
									<button type="submit" class="btn btn-primary btn-block">
										<spring:theme code="text.account.profile.saveUpdates"
											text="Save Updates" />
									</button>
								</ycommerce:testId>
							</div>
						</div>

						<div class="col-sm-6 col-sm-pull-6">
							<div class="accountActions">
								<ycommerce:testId
									code="personalDetails_cancelPersonalDetails_button">
									<button type="button"
										class="btn btn-default btn-block backToHome">
										<spring:theme code="text.account.profile.cancel" text="Cancel" />
									</button>
								</ycommerce:testId>
							</div>
						</div>
					</div>

				</form:form>
			</div>
		</div>
	</div>
</div>
