<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%-- Verified that there's a pre-existing bug regarding the setting of showTax; created issue  --%>
<div class="row">
    <div class="col-xs-12 col-sm-8 col-sm-offset-3  col-md-6 col-md-offset-0">
        <div class="cart-voucher">
        	<c:if test="${not empty bankOffers}">
        		<br>
        			<b><spring:theme code="cart.bank.offers"/>:</b>
			        			
        		<c:forEach var="offer" items="${bankOffers}">
        			<br>
        			<img src="${fn:escapeXml(offer.icon.url)}" alt="" title=""/>&nbsp;<span>${offer.message}</span>
        			
        		</c:forEach>
        	</c:if>
         <c:if test="${cartData.shipmentType.code ne 'PICKUP_IN_STORE'}">
        	
            <cart:cartVoucher cartData="${cartData}"/>
            </c:if>
        </div>
    </div>
    <div class="col-xs-12 col-md-6 col-lg-6">
        <div class="cart-totals">
            <cart:cartTotals cartData="${cartData}" showTax="false"/>
            <cart:ajaxCartTotals/>
        </div>
    </div>
</div>