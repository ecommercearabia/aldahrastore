<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="otp" tagdir="/WEB-INF/tags/responsive/otp"%>
<c:choose>
	<c:when test="${OTP_VIEW eq 'THANK_YOU_VIEW'}">
		<otp:thankYou />
	</c:when>
	<c:when test="${OTP_VIEW eq 'VERIFY_CODE_VIEW'}">
		<otp:verifyCode />
	</c:when>
	<c:when test="${OTP_VIEW eq 'SEND_CODE_VIEW'}">
		<otp:sendCode />
	</c:when>
</c:choose>

