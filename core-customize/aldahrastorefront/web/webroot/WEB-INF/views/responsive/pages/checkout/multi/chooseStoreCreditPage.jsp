<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<spring:htmlEscape defaultHtmlEscape="true" />

<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">

<div class="row">
    <div class="col-sm-6">
        <div class="checkout-headline">
            <span class="fal fa-lock-alt"></span>
            <spring:theme code="checkout.multi.secure.checkout" />
        </div>
		<multi-checkout:checkoutSteps checkoutSteps="${checkoutSteps}" progressBarId="${progressBarId}">
			<jsp:body>
				<ycommerce:testId code="checkoutStepThree">
					<div class="checkout-shipping checkout_store">
						<multi-checkout:shipmentItems cartData="${cartData}" showDeliveryAddress="true" />
						<div class="checkout-indent">
							<div class="headline"><spring:theme code="checkout.summary.storeCredit.selectStoreCreditForOrder" /></div>
							<spring:url var="selectStoreCreditUrl" value="{contextPath}/checkout/multi/store-credit/choose" htmlEscape="false" >
								<spring:param name="contextPath" value="${request.contextPath}" />
							</spring:url>
							<div class="storeCredit_div">
							<form:form id="selectStoreCreditForm" action="${fn:escapeXml(selectStoreCreditUrl)}" method="post" modelAttribute="storeCreditForm">
								<div class="form-group">
									<c:forEach items="${supportedStoreCreditModes}" var="storeCreditMode" varStatus="loop">
										<label class="container-radio" for="${storeCreditMode.storeCreditModeType.code}">
											${storeCreditMode.storeCreditModeType.code}
										
										<input type="radio" name="storeCredit" id="${storeCreditMode.storeCreditModeType.code}" value=""/>
										<span class="checkmark"></span></label><br/>
									</c:forEach>
									<br>
									 <input id="sctCode" name="sctCode" value="${cartData.storeCreditMode.storeCreditModeType.code}"/>
									<div class="storeCreditAmount"><input id="scAmount" type="number" min="1" name="scAmount" class="form-control" placeholder="storeCreditAmount" value="${cartData.storeCreditAmount.value}"/><br></div>
									
									<div class="balance"><span class="available">availableBalanceStoreCredit:</span> <span class="aed">${availableBalanceStoreCredit.formattedValue}</span><span class="smile"></span></div>
								</div>
							</form:form>
							</div>
						</div>
					</div>

					<button id="storeCreditSubmit" type="button" class="btn btn-primary btn-block checkout-next"><spring:theme code="checkout.multi.storeCredit.continue"/></button>
				</ycommerce:testId>
			</jsp:body>
		</multi-checkout:checkoutSteps>
    </div>

    <div class="col-sm-6 hidden-xs">
		<multi-checkout:checkoutOrderDetails cartData="${cartData}" showDeliveryAddress="true" showPaymentInfo="false" showTaxEstimate="false" showTax="true" />
    </div>

    <div class="col-sm-12 col-lg-12">
        <cms:pageSlot position="SideContent" var="feature" element="div" class="checkout-help">
            <cms:component component="${feature}"/>
        </cms:pageSlot>
    </div>
</div>

</template:page>
