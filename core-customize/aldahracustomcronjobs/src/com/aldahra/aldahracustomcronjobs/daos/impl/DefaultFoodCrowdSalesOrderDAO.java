/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahracustomcronjobs.daos.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.aldahra.aldahracustomcronjobs.daos.FoodCrowdSalesOrderDAO;



/**
 * @author mohammedbaker
 */
public class DefaultFoodCrowdSalesOrderDAO implements FoodCrowdSalesOrderDAO
{

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(DefaultFoodCrowdSalesOrderDAO.class);

	/** The flexible search service. */
	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	/**
	 * Gets the orders by query.
	 *
	 * @param query
	 *           the query
	 * @return the orders by query
	 */
	@Override
	public List<OrderModel> getOrdersByQuery(final String query)
	{
		if (Objects.isNull(query))
		{
			LOG.error("query is null.");
			throw new IllegalArgumentException("query is null.");
		}

		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query);

		final SearchResult<OrderModel> searchResult = flexibleSearchService.search(flexibleSearchQuery);
		if (searchResult != null)
		{
			return searchResult.getResult();
		}

		return Collections.emptyList();
	}

}
