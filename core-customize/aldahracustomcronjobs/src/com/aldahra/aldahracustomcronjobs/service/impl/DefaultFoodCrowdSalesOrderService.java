/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahracustomcronjobs.service.impl;

import de.hybris.platform.core.model.order.OrderModel;

import java.util.List;
import java.util.Objects;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.aldahra.aldahracustomcronjobs.daos.FoodCrowdSalesOrderDAO;
import com.aldahra.aldahracustomcronjobs.service.FoodCrowdSalesOrderService;


/**
 * @author mohammedbaker
 */
public class DefaultFoodCrowdSalesOrderService implements FoodCrowdSalesOrderService
{

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(DefaultFoodCrowdSalesOrderService.class);

	/** The food crowd sales order DAO. */
	@Resource(name = "foodcrowdSalesOrderDAO")
	private FoodCrowdSalesOrderDAO foodCrowdSalesOrderDAO;


	/**
	 * Gets the orders by query.
	 *
	 * @param query
	 *           the query
	 * @return the orders by query
	 */
	@Override
	public List<OrderModel> getOrdersByQuery(final String query)
	{
		if (Objects.isNull(query))
		{
			LOG.error("query is null.");
			throw new IllegalArgumentException("query is null.");
		}

		return foodCrowdSalesOrderDAO.getOrdersByQuery(query);
	}

}
