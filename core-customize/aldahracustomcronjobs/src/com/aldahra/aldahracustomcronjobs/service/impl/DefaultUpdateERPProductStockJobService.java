/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahracustomcronjobs.service.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.aldahra.aldahracustomcronjobs.model.UpdateProductStockLevelCronJobModel;
import com.aldahra.aldahracustomcronjobs.service.UpdateERPProductStockJobService;
import com.aldahra.aldahraerpclientservices.erp.impl.entries.SiteStockLines;
import com.aldahra.aldahraerpclientservices.erp.response.ProductStockResponse;
import com.aldahra.aldahraerpclientservices.erp.response.SiteStockResponse;
import com.aldahra.aldahraerpclientservices.exception.ERPWSServiceException;
import com.aldahra.aldahraerpclientservices.service.AldahraERPClientProductStockService;
import com.aldahra.aldahraerpclientservices.service.AldahraERPClientWSProviderService;
import com.aldahra.aldahraerpclientwebservices.model.AldahraERPClientWebServiceProviderModel;
import com.aldahra.aldahraerpclientwebservices.model.AldahraERPProductStockServiceProviderModel;
import com.aldahra.core.beans.AllStockLevelAttributes;
import com.aldahra.core.beans.StockLevelQuantityAttributes;
import com.aldahra.core.service.CustomStockService;
import com.aldahra.core.service.UpdateProductStockLevelService;
import com.google.common.base.Preconditions;


/**
 * @author monzer
 */
public class DefaultUpdateERPProductStockJobService implements UpdateERPProductStockJobService
{
	private static final String CANNOT_PROCEED_WITH_NO_PRODUCT = "Cannot proceed with no product";
	private static final String ERPWS_PROVIDER_IS_NOT_AVAIALABEL = "ERPWS Provider is not available";
	private static final String THE_UPDATE_PRODUCT_STOCK_JOB_S_STORE_CANNOT_BE_NULL = "The Update product stock job's store cannot be null";

	private static final Logger LOG = LoggerFactory.getLogger(DefaultUpdateERPProductStockJobService.class);

	@Resource(name = "erpClientWSProviderService")
	private AldahraERPClientWSProviderService erpClientWSProviderService;

	@Resource(name = "erpClientStockService")
	private AldahraERPClientProductStockService erpClientService;

	@Resource(name = "updateProductStockLevelService")
	private UpdateProductStockLevelService updateProductStockLevelService;

	@Resource(name = "defaultStockService")
	private CustomStockService customStockService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Override
	public void getERPProductStockAndUpdate(final List<String> productCodes, final UpdateProductStockLevelCronJobModel cronjob)
	{
		Preconditions.checkArgument(!CollectionUtils.isEmpty(productCodes), "Cannot proceed with no codes");
		Preconditions.checkArgument(cronjob != null && cronjob.getStore() != null,
				THE_UPDATE_PRODUCT_STOCK_JOB_S_STORE_CANNOT_BE_NULL);
		final Optional<AldahraERPClientWebServiceProviderModel> providerOptional = erpClientWSProviderService
				.getActive(cronjob.getStore(), AldahraERPProductStockServiceProviderModel.class);
		Preconditions.checkArgument(providerOptional.isPresent(), ERPWS_PROVIDER_IS_NOT_AVAIALABEL);
		final AldahraERPClientWebServiceProviderModel provider = providerOptional.get();
		validateProductStockProvider(provider);

		final AldahraERPProductStockServiceProviderModel productStockProvider = (AldahraERPProductStockServiceProviderModel) provider;
		List<ProductStockResponse> productStock;
		try
		{
			productStock = erpClientService.getProductsStock(productCodes, productStockProvider);
		}
		catch (final ERPWSServiceException e)
		{
			LOG.error(e.getMessage());
			return;
		}
		if (CollectionUtils.isEmpty(productStock))
		{
			LOG.error("Cannot proceed with empty Product response list");
			return;
		}

		cronjob.getWarehouses().stream()
				.forEach(warehouse -> productStock.stream().filter(stock -> stock.isHasStock()).forEach(stock -> {
					final StockLevelQuantityAttributes fields = new StockLevelQuantityAttributes();
					fields.setResponse(stock.getResponse().toString());
					fields.setEnableSaveStockResponse(cronjob.isEnableSaveStockResponse());
					fields.setAutoCreate(cronjob.isAutoCreate());
					fields.setRemoveInventoryEvents(cronjob.getRemoveInventoryEvents());
					updateProductStockLevelService.updateStockLevelForProduct(stock.getProductCode(),
							cronjob.getProductCatalogVersion(), warehouse, stock.getStock(), fields);
				}));

	}

	@Override
	public void getERPProductStockAndUpdate(final UpdateProductStockLevelCronJobModel cronjob,
			final List<? extends ProductModel> products)
	{
		Preconditions.checkArgument(!CollectionUtils.isEmpty(products), "Cannot proceed with no products");
		Preconditions.checkArgument(cronjob != null && cronjob.getStore() != null,
				THE_UPDATE_PRODUCT_STOCK_JOB_S_STORE_CANNOT_BE_NULL);
		final List<String> productCodes = products.stream().map(ProductModel::getCode).collect(Collectors.toList());
		getERPProductStockAndUpdate(productCodes, cronjob);
	}

	/**
	 *
	 */
	private void validateProductStockProvider(final AldahraERPClientWebServiceProviderModel providerModel)
	{
		Preconditions.checkArgument(providerModel instanceof AldahraERPProductStockServiceProviderModel,
				"Not a Product stock provider");
		final AldahraERPProductStockServiceProviderModel provider = (AldahraERPProductStockServiceProviderModel) providerModel;
		Preconditions.checkArgument(StringUtils.isNotBlank(provider.getUsername()), "Provider username is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(provider.getPassword()), "Provider password is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(provider.getServicePoolName()), "Provider pool name is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(provider.getStockPublicName()), "Provider public name is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(provider.getStockSiteCode()), "Provider Stock Site is null");
	}

	@Override
	public void getERPProductStockAndUpdate(final ProductModel product, final UpdateProductStockLevelCronJobModel cronjob)
	{
		Preconditions.checkArgument(product != null, CANNOT_PROCEED_WITH_NO_PRODUCT);
		Preconditions.checkArgument(cronjob != null && cronjob.getStore() != null,
				THE_UPDATE_PRODUCT_STOCK_JOB_S_STORE_CANNOT_BE_NULL);
		getERPProductStockAndUpdate(product.getCode(), cronjob);
	}

	@Override
	public void getERPProductStockAndUpdate(final String productCode, final UpdateProductStockLevelCronJobModel cronjob)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(productCode), CANNOT_PROCEED_WITH_NO_PRODUCT);
		Preconditions.checkArgument(cronjob != null && cronjob.getStore() != null,
				THE_UPDATE_PRODUCT_STOCK_JOB_S_STORE_CANNOT_BE_NULL);
		final Optional<AldahraERPClientWebServiceProviderModel> providerOptional = erpClientWSProviderService
				.getActive(cronjob.getStore(), AldahraERPProductStockServiceProviderModel.class);
		Preconditions.checkArgument(providerOptional.isPresent(), ERPWS_PROVIDER_IS_NOT_AVAIALABEL);
		final AldahraERPClientWebServiceProviderModel provider = providerOptional.get();
		validateProductStockProvider(provider);

		final AldahraERPProductStockServiceProviderModel poroductStockProvider = (AldahraERPProductStockServiceProviderModel) provider;
		Optional<ProductStockResponse> productStock = Optional.empty();
		try
		{
			productStock = erpClientService.getProductStock(productCode, poroductStockProvider);
		}
		catch (final ERPWSServiceException e)
		{
			LOG.error(e.getMessage());
			return;
		}
		if (productStock.isEmpty() || !productStock.get().isHasStock())
		{
			LOG.warn("Cannot proceed with empty Product Response");
			return;
		}
		final ProductStockResponse stock = productStock.get();

		cronjob.getWarehouses().stream().forEach(warehouse -> {
			final StockLevelQuantityAttributes fields = new StockLevelQuantityAttributes();
			fields.setResponse(stock.getResponse().toString());
			fields.setEnableSaveStockResponse(cronjob.isEnableSaveStockResponse());
			fields.setAutoCreate(cronjob.isAutoCreate());
			fields.setRemoveInventoryEvents(cronjob.getRemoveInventoryEvents());
			updateProductStockLevelService.updateStockLevelForProduct(stock.getProductCode(), cronjob.getProductCatalogVersion(),
					warehouse, stock.getStock(), fields);
		});

	}

	@Override
	public void getProductERPStockAndUpdate(final ProductModel product, final UpdateProductStockLevelCronJobModel cronjob)
	{
		Preconditions.checkArgument(product != null, CANNOT_PROCEED_WITH_NO_PRODUCT);
		Preconditions.checkArgument(cronjob != null && cronjob.getStore() != null,
				THE_UPDATE_PRODUCT_STOCK_JOB_S_STORE_CANNOT_BE_NULL);
		getProductERPStockAndUpdate(product.getCode(), cronjob);
	}

	@Override
	public void getProductERPStockAndUpdate(final String productCode, final UpdateProductStockLevelCronJobModel cronjob)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(productCode), CANNOT_PROCEED_WITH_NO_PRODUCT);
		Preconditions.checkArgument(cronjob != null && cronjob.getStore() != null,
				THE_UPDATE_PRODUCT_STOCK_JOB_S_STORE_CANNOT_BE_NULL);
		final Optional<AldahraERPClientWebServiceProviderModel> providerOptional = erpClientWSProviderService
				.getActive(cronjob.getStore(), AldahraERPProductStockServiceProviderModel.class);
		Preconditions.checkArgument(providerOptional.isPresent(), ERPWS_PROVIDER_IS_NOT_AVAIALABEL);
		final AldahraERPClientWebServiceProviderModel provider = providerOptional.get();
		validateProductStockProvider(provider);

		final AldahraERPProductStockServiceProviderModel poroductStockProvider = (AldahraERPProductStockServiceProviderModel) provider;
		Optional<SiteStockResponse> productStock = Optional.empty();
		try
		{
			productStock = erpClientService.getProductERPStock(productCode, poroductStockProvider);
		}
		catch (final ERPWSServiceException e)
		{
			LOG.error(e.getMessage());
			return;
		}
		if (productStock.isEmpty() || !productStock.get().isHasStock())
		{
			LOG.warn("Cannot proceed with empty Product Response");
			return;
		}
		final SiteStockResponse stock = productStock.get();

		cronjob.getWarehouses().stream().forEach(warehouse -> updateStockForWarehouse(cronjob, stock, warehouse));

	}


	public boolean isNotStockLevelIsSiteStock(final StockLevelModel stockLevel, final SiteStockResponse stock)
	{
		return stock.getStockLines().stream().filter(e -> isStockLevelEqualsSiteStockLine(stockLevel, e))
				.collect(Collectors.toList()).size() == 0;
	}

	/**
	 *
	 */
	private boolean isStockLevelEqualsSiteStockLine(final StockLevelModel stockLevel, final SiteStockLines e)
	{
		// NOTE(Husam Dababneh): Add "equals" if necessary
		return (e.getBinNumber().equals(stockLevel.getBinNumber()) && e.getLotNumber().equals(stockLevel.getLotNumber()));
	}

	private void updateStockForWarehouse(final UpdateProductStockLevelCronJobModel cronjob, final SiteStockResponse stock,
			final WarehouseModel warehouse)
	{
		removeUnUpdatedStockLevels(stock);

		final AllStockLevelAttributes fields = new AllStockLevelAttributes();
		fields.setResponse(stock.getResponse().toString());
		fields.setEnableSaveStockResponse(cronjob.isEnableSaveStockResponse());
		fields.setAutoCreate(cronjob.isAutoCreate());
		fields.setRemoveInventoryEvents(cronjob.getRemoveInventoryEvents());
		fields.setResetStockReserved(cronjob.isResetStockReserved());
		stock.getStockLines().stream().forEach(stockLine -> {
			fields.setBinNumber(stockLine.getBinNumber());
			fields.setLotNumber(stockLine.getLotNumber());
			fields.setExpirationDate(parseToDate(stockLine.getExpirationDate()));
			fields.setUnitOfMeasure(stockLine.getUnitOfMeasure());
			updateProductStockLevelService.updateStockLevelForProductWithExtraData(stock.getProductCode(),
					cronjob.getProductCatalogVersion(), warehouse, stockLine.getQuantity(), fields);
		});
	}

	private void removeUnUpdatedStockLevels(final SiteStockResponse stock)
	{
		// Remove Un-updated StockLevels
		final List<StockLevelModel> stockLevelsByProductCode = getCustomStockService()
				.getStockLevelsByProductCode(stock.getProductCode());

		// NOTE(Husam Dababneh): This list will be deleted
		// 							 so we are removing the Stock Levels that we are sure it
		//                       will be updated by the cronJob

		final List<StockLevelModel> stockLevelWillNotBeUpdated = stockLevelsByProductCode.stream()
				.filter(e -> isNotStockLevelIsSiteStock(e, stock)).collect(Collectors.toList());

		getModelService().removeAll(stockLevelWillNotBeUpdated);
	}

	/**
	 *
	 */
	private Date parseToDate(final String expirationDate)
	{
		final SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");

		if (Strings.isBlank(expirationDate))
		{
			return null;
		}

		try
		{
			return format.parse(expirationDate);
		}
		catch (final ParseException e)
		{
			LOG.error(e.getMessage());
			return null;
		}
	}

	/**
	 * @return the customStockService
	 */
	public CustomStockService getCustomStockService()
	{
		return customStockService;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

}
