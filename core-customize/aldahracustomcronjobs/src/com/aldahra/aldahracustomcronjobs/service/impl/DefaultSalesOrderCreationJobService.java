/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahracustomcronjobs.service.impl;

import de.hybris.platform.core.model.order.OrderModel;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.aldahra.aldahracustomcronjobs.service.SalesOrderCreationJobService;
import com.aldahra.aldahraerpclientservices.service.AldahraERPClientSalesOrderService;
import com.aldahra.aldahraerpclientservices.service.AldahraERPClientWSProviderService;
import com.aldahra.aldahraerpclientwebservices.model.AldahraERPClientWebServiceProviderModel;
import com.aldahra.aldahraerpclientwebservices.model.AldahraERPSalesOrderServiceProviderModel;
import com.google.common.base.Preconditions;

/**
 *
 */
public class DefaultSalesOrderCreationJobService implements SalesOrderCreationJobService
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultSalesOrderCreationJobService.class);

	@Resource(name = "erpClientWSProviderService")
	private AldahraERPClientWSProviderService erpClientWSProviderService;

	@Resource(name = "erpClientSalesOrderService")
	private AldahraERPClientSalesOrderService erpSalesOrderService;

	@Override
	public void createSalesOrder(final OrderModel order)
	{
		Preconditions.checkArgument(order != null, "Order is null");
		Preconditions.checkArgument(order.getStore() != null, "Base Store is null");
		final Optional<AldahraERPClientWebServiceProviderModel> providerOptional = erpClientWSProviderService
				.getActive(order.getStore(), AldahraERPSalesOrderServiceProviderModel.class);
		Preconditions.checkArgument(providerOptional.isPresent(), "ERPWS Provider is not avaialabel");
		final AldahraERPClientWebServiceProviderModel provider = providerOptional.get();
		if (!validateProductStockProvider(provider))
		{
			LOG.error("Provider model is not valid");
			return;
		}
		final AldahraERPSalesOrderServiceProviderModel salesOrderProvider = (AldahraERPSalesOrderServiceProviderModel) provider;
		erpSalesOrderService.createSalesOrder(order, salesOrderProvider);
	}

	@Override
	public void createSalesOrder(final List<OrderModel> orders)
	{
		Preconditions.checkArgument(!CollectionUtils.isEmpty(orders), "Order list is null");
		orders.stream().forEach(order -> createSalesOrder(order));
	}

	/**
	 *
	 */
	private boolean validateProductStockProvider(final AldahraERPClientWebServiceProviderModel providerModel)
	{
		Preconditions.checkArgument(providerModel != null, "ERPWS provider model is null");
		Preconditions.checkArgument(providerModel instanceof AldahraERPSalesOrderServiceProviderModel,
				"Not a sales order provider");
		final AldahraERPSalesOrderServiceProviderModel provider = (AldahraERPSalesOrderServiceProviderModel) providerModel;
		Preconditions.checkArgument(StringUtils.isNotBlank(provider.getUsername()), "Provider username is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(provider.getPassword()), "Provider password is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(provider.getServicePoolName()), "Provider pool name is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(provider.getSalesOrderPublicName()), "Provider public name is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(provider.getSalesOrderSiteCode()), "Provider Stock Site is null");
		return true;
	}

}
