/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahracustomcronjobs.service.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;

import java.util.Collection;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aldahra.aldahracustomcronjobs.service.ProductMinimumAmountService;
import com.aldahra.aldahrafulfillment.atp.strategy.impl.CustomWarehousingAvailabilityCalculationStrategy;
import com.aldahra.core.service.CustomStockService;
import com.google.common.base.Preconditions;


/**
 * @author monzer
 */
public class DefaultProductMinimumAmountService implements ProductMinimumAmountService
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultProductMinimumAmountService.class);

	@Resource(name = "stockService")
	private CustomStockService stockService;

	@Resource(name = "commerceStockLevelCalculationStrategy")
	private CustomWarehousingAvailabilityCalculationStrategy warehousingAvailabilityCalculationStrategy;

	@Override
	public int calculateAvailabilityForProduct(final ProductModel product)
	{
		Preconditions.checkArgument(product != null, "Cannot get stock levels for empty product");
		final Collection<StockLevelModel> allStockLevels = stockService.getAllStockLevels(product);
		final Long calculateAvailability = warehousingAvailabilityCalculationStrategy.calculateAvailability(allStockLevels);
		return calculateAvailability.intValue();
	}

}
