/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahracustomcronjobs.service.impl;

import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aldahra.aldahracustomcronjobs.model.SendPurchaseRequestForProductMinimumAmountModel;
import com.aldahra.aldahracustomcronjobs.service.ProductMinimumAmountService;
import com.aldahra.aldahracustomcronjobs.service.PurchaseRequestCreationJobService;
import com.aldahra.aldahraerpclientservices.erp.response.PurchaseResponse;
import com.aldahra.aldahraerpclientservices.exception.ERPWSServiceException;
import com.aldahra.aldahraerpclientservices.service.AldahraERPClientPurchaseRequestService;
import com.aldahra.aldahraerpclientservices.service.AldahraERPClientWSProviderService;
import com.aldahra.aldahraerpclientwebservices.model.AldahraERPClientWebServiceProviderModel;
import com.aldahra.aldahraerpclientwebservices.model.AldahraERPProductPurchaseRequestServiceProviderModel;
import com.google.common.base.Preconditions;


/**
 * @author monzer
 */
public class DefaultPurchaseRequestCreationJobService implements PurchaseRequestCreationJobService
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultPurchaseRequestCreationJobService.class);

	@Resource(name = "productMinimumAmountService")
	private ProductMinimumAmountService productMinimumAmountService;

	@Resource(name = "erpClientPurchaseRequestService")
	private AldahraERPClientPurchaseRequestService erpClientPurcahseRequestService;

	@Resource(name = "erpClientWSProviderService")
	private AldahraERPClientWSProviderService erpClientWSProviderService;

	@Override
	public void createPurchaseRequestForMinimumAmountProducts(final ProductModel product,
			final SendPurchaseRequestForProductMinimumAmountModel cronjob)
	{
		Preconditions.checkArgument(product != null, "Cannot proceed with purchase request with empty product");

		final int availabilityForProduct = productMinimumAmountService.calculateAvailabilityForProduct(product);
		if (availabilityForProduct > product.getMinimumAmount())
		{
			LOG.info("Product {} with {} available stock has not reached the minimum amount {} yet!", product.getCode(),
					availabilityForProduct, product.getMinimumAmount());
			return;
		}

		LOG.info("Product {} with {} available stock has reached the minimum amount {}, sending a purchase request",
				product.getCode(),
				availabilityForProduct, product.getMinimumAmount());

		final Optional<AldahraERPClientWebServiceProviderModel> providerOptional = erpClientWSProviderService
				.getActive(cronjob.getStore(), AldahraERPProductPurchaseRequestServiceProviderModel.class);
		Preconditions.checkArgument(providerOptional.isPresent(), "ERPWS Provider is not avaialabel");
		final AldahraERPClientWebServiceProviderModel provider = providerOptional.get();

		if (!validateProductStockProvider(provider))
		{
			LOG.error("Provider is not valid");
			return;
		}
		final AldahraERPProductPurchaseRequestServiceProviderModel purchaseRequestProvider = (AldahraERPProductPurchaseRequestServiceProviderModel) provider;
		Optional<PurchaseResponse> purchaseRequest = Optional.empty();
		try
		{
			purchaseRequest = erpClientPurcahseRequestService
					.sendPurchaseRequestForProductsUnderMinimumAmount(product, purchaseRequestProvider);
		}
		catch (final ERPWSServiceException e)
		{
			LOG.error(e.getMessage());
			return;
		}
		if (purchaseRequest.isEmpty())
		{
			LOG.error("Cannot proceed with empty Product Response");
			return;
		}
		final PurchaseResponse purchase = purchaseRequest.get();

	}

	@Override
	public void createPurchaseRequestForMinimumAmountProducts(final List<ProductModel> products,
			final SendPurchaseRequestForProductMinimumAmountModel cronjob)
	{
		Preconditions.checkArgument(CollectionUtils.isEmpty(products), "Cannot proceed with purchase requests with empty products");
		products.stream().forEach(product -> createPurchaseRequestForMinimumAmountProducts(product, cronjob));

	}

	/**
	 *
	 */
	private boolean validateProductStockProvider(final AldahraERPClientWebServiceProviderModel providerModel)
	{
		Preconditions.checkArgument(providerModel instanceof AldahraERPProductPurchaseRequestServiceProviderModel,
				"Not a Purchase request provider");
		final AldahraERPProductPurchaseRequestServiceProviderModel provider = (AldahraERPProductPurchaseRequestServiceProviderModel) providerModel;
		Preconditions.checkArgument(StringUtils.isNotBlank(provider.getUsername()), "Provider username is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(provider.getPassword()), "Provider password is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(provider.getServicePoolName()), "Provider pool name is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(provider.getReceivingSiteCode()), "Provider receiving site is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(provider.getRequestUserCode()), "Provider request user is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(provider.getSupplierCode()), "Provider supplier code is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(provider.getSiteCode()), "Provider Site code is null");
		return true;
	}

}
