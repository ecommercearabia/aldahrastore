/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahracustomcronjobs.service;

import de.hybris.platform.core.model.order.OrderModel;

import java.util.List;


/**
 * @author monzer
 */
public interface SalesOrderCreationJobService
{
	void createSalesOrder(OrderModel order);

	void createSalesOrder(List<OrderModel> orders);

}
