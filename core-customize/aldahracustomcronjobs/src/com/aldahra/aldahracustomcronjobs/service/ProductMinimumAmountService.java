/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahracustomcronjobs.service;

import de.hybris.platform.core.model.product.ProductModel;

/**
 * @author monzer
 */
public interface ProductMinimumAmountService
{

	int calculateAvailabilityForProduct(ProductModel product);

}
