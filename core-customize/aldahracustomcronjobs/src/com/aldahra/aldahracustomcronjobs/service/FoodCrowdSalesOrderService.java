/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahracustomcronjobs.service;

import de.hybris.platform.core.model.order.OrderModel;

import java.util.List;


/**
 * @author mohammedbaker
 */
public interface FoodCrowdSalesOrderService
{

	/**
	 * Gets the orders by query.
	 *
	 * @param query
	 *           the query
	 * @return the orders by query
	 */
	public List<OrderModel> getOrdersByQuery(String query);

}
