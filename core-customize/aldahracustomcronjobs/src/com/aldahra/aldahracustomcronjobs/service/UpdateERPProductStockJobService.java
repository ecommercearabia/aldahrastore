/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahracustomcronjobs.service;

import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

import com.aldahra.aldahracustomcronjobs.model.UpdateProductStockLevelCronJobModel;



/**
 * @author monzer
 */
public interface UpdateERPProductStockJobService
{
	/**
	 * Gets the ERP stock quantity and Updates Hybris stock quantity only for a bulk of products
	 */
	void getERPProductStockAndUpdate(List<String> productCodes, UpdateProductStockLevelCronJobModel cronjob);

	/**
	 * Gets the ERP stock quantity and Updates Hybris stock quantity only for a bulk of products
	 */
	void getERPProductStockAndUpdate(UpdateProductStockLevelCronJobModel cronjob, List<? extends ProductModel> products);

	/**
	 * Gets the ERP stock quantity and Updates Hybris stock quantity only for a single products
	 */
	void getERPProductStockAndUpdate(ProductModel product, UpdateProductStockLevelCronJobModel cronjob);

	/**
	 * Gets the ERP stock quantity and Updates Hybris stock quantity only for a single products
	 */
	void getERPProductStockAndUpdate(String productCode, UpdateProductStockLevelCronJobModel cronjob);

	/**
	 * Gets the ERP stock and Updates Hybris stock with the extra stock fields (Quantity, Bin Number, Lot Number,
	 * ExpiryDate)
	 */
	void getProductERPStockAndUpdate(ProductModel product, UpdateProductStockLevelCronJobModel cronjob);

	/**
	 * Uses the new ERP stock service with the extra attributes
	 */
	void getProductERPStockAndUpdate(String productCode, UpdateProductStockLevelCronJobModel cronjob);

}
