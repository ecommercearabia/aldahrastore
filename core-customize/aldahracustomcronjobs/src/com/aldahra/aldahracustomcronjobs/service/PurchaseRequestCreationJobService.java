/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahracustomcronjobs.service;

import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

import com.aldahra.aldahracustomcronjobs.model.SendPurchaseRequestForProductMinimumAmountModel;


/**
 *
 */
public interface PurchaseRequestCreationJobService
{

	void createPurchaseRequestForMinimumAmountProducts(ProductModel product,
			SendPurchaseRequestForProductMinimumAmountModel cronjob);

	void createPurchaseRequestForMinimumAmountProducts(List<ProductModel> product,
			SendPurchaseRequestForProductMinimumAmountModel cronjob);

}
