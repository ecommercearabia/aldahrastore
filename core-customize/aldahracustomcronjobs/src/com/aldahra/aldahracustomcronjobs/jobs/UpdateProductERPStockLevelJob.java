/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahracustomcronjobs.jobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aldahra.aldahracustomcronjobs.model.UpdateProductStockLevelCronJobModel;
import com.aldahra.aldahracustomcronjobs.service.UpdateERPProductStockJobService;
import com.aldahra.aldahraproduct.model.GroceryVariantProductModel;
import com.aldahra.core.service.GroceryProductService;


/**
 * @author monzer NEW UpdateProductStockLevelCronJob
 */
public class UpdateProductERPStockLevelJob extends AbstractJobPerformable<UpdateProductStockLevelCronJobModel>
{

	private static final Logger LOG = LoggerFactory.getLogger(UpdateProductERPStockLevelJob.class);

	@Resource(name = "updateProductStockJobService")
	private UpdateERPProductStockJobService updateProductStockJobService;

	@Resource(name = "groceryProductService")
	private GroceryProductService groceryProductService;

	@Override
	public PerformResult perform(final UpdateProductStockLevelCronJobModel cronjob)
	{
		final List<GroceryVariantProductModel> allProducts = groceryProductService
				.getAllByCatalogVersion(cronjob.getProductCatalogVersion());
		//		final List<String> allProducts = Arrays.asList("V203013NA", "V203012NA", "P200050AE", "P200059AE", "P200008AE",
		//				"P200003MY");

		allProducts.stream().forEach(product -> updateProductStockJobService.getProductERPStockAndUpdate(product, cronjob));

		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

}
