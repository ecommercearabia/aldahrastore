/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahracustomcronjobs.jobs;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;

import com.aldahra.aldahracustomcronjobs.model.SendSalesOrderCronJobModel;
import com.aldahra.aldahracustomcronjobs.service.FoodCrowdSalesOrderService;
import com.aldahra.aldahracustomcronjobs.service.SalesOrderCreationJobService;



/**
 * @author mohammedbaker
 */
public class SendSalesOrderJob extends AbstractJobPerformable<SendSalesOrderCronJobModel>
{

	/** The Constant LOG. */
	protected static final Logger LOG = Logger.getLogger(SendSalesOrderJob.class);

	/** The food crowd sales order service. */
	@Resource(name = "foodcrowdSalesOrderService")
	private FoodCrowdSalesOrderService foodCrowdSalesOrderService;

	@Resource(name = "salesOrderCreationJobService")
	private SalesOrderCreationJobService salesOrderCreationJobService;
	/**
	 * Perform.
	 *
	 * @param cjm
	 *           the cjm
	 * @return the perform result
	 */
	@Override
	public PerformResult perform(final SendSalesOrderCronJobModel cjm)
	{
		if (StringUtils.isBlank(cjm.getSalesOrderQuery()))
		{
			return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
		}
		final List<OrderModel> ordersByQuery = foodCrowdSalesOrderService.getOrdersByQuery(cjm.getSalesOrderQuery());
		if (CollectionUtils.isEmpty(ordersByQuery))
		{
			LOG.warn("Orders retrieved by the query is 0 orders");
		}
		for (final OrderModel order : ordersByQuery)
		{
			if (!CollectionUtils.isEmpty(order.getEntries()) || OrderStatus.CANCELLED.equals(order.getStatus())
					|| OrderStatus.CANCELLING.equals(order.getStatus()))
			{
				try
				{
					salesOrderCreationJobService.createSalesOrder(order);
				}
				catch (final Exception e)
				{
					LOG.error(e.getMessage());
				}
			}else {
				LOG.error("Order " + order.getCode() + " is not sent because the order is canceled or has no entries");
			}
		}
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

}
