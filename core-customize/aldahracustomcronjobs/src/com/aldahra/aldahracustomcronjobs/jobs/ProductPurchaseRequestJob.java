/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahracustomcronjobs.jobs;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.aldahra.aldahracustomcronjobs.model.SendPurchaseRequestForProductMinimumAmountModel;
import com.aldahra.aldahracustomcronjobs.service.PurchaseRequestCreationJobService;
import com.aldahra.aldahraproduct.model.GroceryVariantProductModel;
import com.aldahra.core.service.GroceryProductService;


/**
 *
 */
public class ProductPurchaseRequestJob extends AbstractJobPerformable<SendPurchaseRequestForProductMinimumAmountModel>
{

	private static final Logger LOG = LoggerFactory.getLogger(ProductPurchaseRequestJob.class);

	@Resource(name = "groceryProductService")
	private GroceryProductService groceryProductService;

	@Resource(name = "purchaseRequestCreationJobService")
	private PurchaseRequestCreationJobService purchaseRequestCreationJobService;

	@Override
	public PerformResult perform(final SendPurchaseRequestForProductMinimumAmountModel cronjob)
	{
		PerformResult performResult = null;
		if (cronjob == null || cronjob.getProductCatalogVersion() == null || CollectionUtils.isEmpty(cronjob.getWarehouses()))
		{
			performResult = new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
		}
		final List<GroceryVariantProductModel> allProducts = groceryProductService
				.getAllByCatalogVersion(cronjob.getProductCatalogVersion());
		for (final ProductModel product : allProducts)
		{
			LOG.info("Checking the minimum amount for the {} product", product.getCode());
			purchaseRequestCreationJobService.createPurchaseRequestForMinimumAmountProducts(product, cronjob);
		}
		performResult = new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		return performResult;
	}

}
