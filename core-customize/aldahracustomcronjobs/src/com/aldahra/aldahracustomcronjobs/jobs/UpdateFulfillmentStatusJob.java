/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahracustomcronjobs.jobs;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.warehousing.shipping.service.WarehousingShippingService;
import de.hybris.platform.warehousing.taskassignment.services.WarehousingConsignmentWorkflowService;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;

import com.aldahra.aldahracustomcronjobs.model.OrderProcessingCronJobModel;
import com.aldahra.aldahrafulfillment.context.FulfillmentContext;
import com.aldahra.aldahrafulfillment.exception.FulfillentException;
import com.aldahra.aldahrafulfillment.service.CustomConsignmentService;
import com.aldahra.aldahraotp.context.OTPContext;
import com.aldahra.core.exception.LoyaltyPointException;
import com.aldahra.core.service.LoyaltyPointService;
import com.aldahra.core.service.WarehousingDeliveryService;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class UpdateFulfillmentStatusJob extends AbstractJobPerformable<OrderProcessingCronJobModel>
{

	private static final String THE_CONSIGNMENT_STATUS_IS_NOT_DELIVERY_COMPLETED = "The ConsignmentStatus is not [DELIVERY_COMPLETED] : ";
	protected static final Logger LOG = Logger.getLogger(UpdateFulfillmentStatusJob.class);
	private static final String CRONJOB_FINISHED = "UpdateFulfillmentStatusJob is Finished ...";
	protected static final String SHIPPING_TEMPLATE_CODE = "NPR_Shipping";

	@Resource(name = "cronJobService")
	private CronJobService cronJobService;

	@Resource(name = "customConsignmentService")
	private CustomConsignmentService customConsignmentService;

	@Resource(name = "fulfillmentContext")
	private FulfillmentContext fulfillmentContext;

	@Resource(name = "shipaFulfillmentStatusMap")
	private Map<String, ConsignmentStatus> shipaFulfillmentStatusMap;

	@Resource(name = "otpContext")
	private OTPContext otpContext;

	@Resource(name = "loyaltyPointService")
	private LoyaltyPointService loyaltyPointService;

	@Resource(name = "warehousingConsignmentWorkflowService")
	private WarehousingConsignmentWorkflowService warehousingConsignmentWorkflowService;

	@Resource(name = "warehousingDeliveryService")
	private WarehousingDeliveryService warehousingDeliveryService;

	@Resource(name = "warehousingShippingService")
	private WarehousingShippingService warehousingShippingService;

	/**
	 * @return the warehousingDeliveryService
	 */
	protected WarehousingDeliveryService getWarehousingDeliveryService()
	{
		return warehousingDeliveryService;
	}

	/**
	 * @return the warehousingConsignmentWorkflowService
	 */
	protected WarehousingConsignmentWorkflowService getWarehousingConsignmentWorkflowService()
	{
		return warehousingConsignmentWorkflowService;
	}


	@Override
	public PerformResult perform(final OrderProcessingCronJobModel cronjob)
	{
		LOG.info("UpdateFulfillmentStatusJob is Starting ...");
		final List<ConsignmentModel> consignments = customConsignmentService.getConsignmentsByStoreAndNotStatus(cronjob.getStore(),
				Arrays.asList(ConsignmentStatus.DELIVERY_COMPLETED, ConsignmentStatus.READY_FOR_SHIPPING,
						ConsignmentStatus.CANCELLED),
				false);

		if (CollectionUtils.isEmpty(consignments))
		{
			LOG.info("No consignments found.");
			LOG.info(CRONJOB_FINISHED);
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
		LOG.info("UpdateFulfillmentStatusJob -> consignments [" + consignments.size() + "]");
		consignments.stream().filter(Objects::nonNull).forEach(this::checkAndChangeConsignmentStatus);

		LOG.info(CRONJOB_FINISHED);
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	private void checkAndChangeConsignmentStatus(final ConsignmentModel consignment)
		{
		LOG.info("UpdateFulfillmentStatusJob -> checkAndChangeConsignmentStatus -> consignment [" + consignment.getCode() + "]");

		String deliveryStatus = null;
		try
			{
			final Optional<String> optionalStatus = fulfillmentContext.getStatus(consignment);
			if (optionalStatus.isEmpty())
				{
				LOG.error("Empty status for consignment: " + consignment.getCode());
				return;
				}
			deliveryStatus = optionalStatus.get();

			LOG.info("UpdateFulfillmentStatusJob -> checkAndChangeConsignmentStatus -> consignment [" + consignment.getCode()
					+ "] , fulfillment Status is[" + deliveryStatus + "]");

			}
			catch (final IllegalArgumentException | FulfillentException e)
		{
			LOG.error("Error getting status for consignment: " + consignment.getCode(), e);
			return;
		}


		changeConsignmentStatus(consignment, deliveryStatus);
		}


	private void changeConsignmentStatus(final ConsignmentModel consignment, final String deliveryStatus)
	{
		final ConsignmentStatus mappedStatus = shipaFulfillmentStatusMap.get(deliveryStatus);
		if (mappedStatus == null)
		{
			LOG.error("Error changing status for consignment: " + consignment.getCode());
			LOG.error("No ConsignmentStatus mapping found for delivery status: " + deliveryStatus);
			return;
		}
		saveConsignment(consignment, mappedStatus);
		if (ConsignmentStatus.DELIVERY_COMPLETED.equals(mappedStatus))
		{
			getWarehousingShippingService().confirmShipConsignment(consignment);
		}
		if (consignment.isSentDeliveryConfirmationNotification())
		{
			return;
		}

		addLoyaltyPoints(consignment);
		sendSMSDeliveryConfirmation(consignment);
		sendWhatsappDeliveryConfirmation(consignment);

		modelService.save(consignment);
	}


	/**
	 *
	 */
	private void addLoyaltyPoints(final ConsignmentModel consignment)
	{
		if (ConsignmentStatus.DELIVERY_COMPLETED.equals(consignment.getStatus()))
		{
			try
			{
				if (consignment.getOrder() != null && consignment.getOrder().getStore() != null
						&& loyaltyPointService.isLoyaltyPointsEnabled(consignment.getOrder().getStore()))
				{
					loyaltyPointService.addLoyaltyPoints(consignment);
				}
			}
			catch (final LoyaltyPointException e)
			{
				LOG.error("we have a error to add LoyaltyPoints with Consignment code[ " + consignment.getCode() + "] :  "
						+ e.getMessage());
			}
			return;
		}

		LOG.warn(THE_CONSIGNMENT_STATUS_IS_NOT_DELIVERY_COMPLETED + consignment.getCode() + ", " + consignment.getStatus());

	}

	/**
	 *
	 */
	private void sendSMSDeliveryConfirmation(final ConsignmentModel consignment)
	{

		LOG.info("Starting to Send SMS Delivery Confirmation for consignment: " + consignment.getCode() + ", "
				+ consignment.getStatus() + " ...");

		if (ConsignmentStatus.DELIVERY_COMPLETED.equals(consignment.getStatus()))
		{
			otpContext.sendDeliveryConfirmationSMSMessage(consignment);
			consignment.setSentDeliveryConfirmationNotification(true);
			modelService.save(consignment);
			return;
		}

		LOG.warn(THE_CONSIGNMENT_STATUS_IS_NOT_DELIVERY_COMPLETED + consignment.getCode() + ", " + consignment.getStatus());
	}



	private void sendWhatsappDeliveryConfirmation(final ConsignmentModel consignment)
	{
		LOG.info("Starting to Send SMS Delivery Confirmation for consignment: " + consignment.getCode() + ", "
				+ consignment.getStatus() + " ...");

		if (ConsignmentStatus.DELIVERY_COMPLETED.equals(consignment.getStatus()))
		{
			try
			{
				otpContext.sendOrderDeliveredWhatsappMessage(consignment);
			}
			catch (final Exception e)
			{
				LOG.error(e.getMessage(), e);
			}
			return;
		}

		LOG.warn(THE_CONSIGNMENT_STATUS_IS_NOT_DELIVERY_COMPLETED + consignment.getCode() + ", " + consignment.getStatus());


	}

	private void saveConsignment(final ConsignmentModel consignment, final ConsignmentStatus newStatus)
	{
		LOG.info("Consignment Previous Status[" + consignment.getStatus().getCode() + "], New Status[" + newStatus.getCode() + "]");
		modelService.refresh(consignment);
		consignment.setStatus(newStatus);
		modelService.save(consignment);
	}


	protected WarehousingShippingService getWarehousingShippingService()
	{
		return warehousingShippingService;
	}
}
