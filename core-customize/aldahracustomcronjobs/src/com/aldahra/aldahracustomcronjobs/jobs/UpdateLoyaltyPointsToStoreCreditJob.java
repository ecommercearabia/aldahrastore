/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahracustomcronjobs.jobs;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aldahra.aldahracustomcronjobs.model.UpdateLoyaltyPointsToStoreCreditCronJobModel;
import com.aldahra.core.exception.LoyaltyPointException;
import com.aldahra.core.service.CustomCustomerService;
import com.aldahra.core.service.LoyaltyPointService;


/**
 * @author Tuqa
 */
public class UpdateLoyaltyPointsToStoreCreditJob extends AbstractJobPerformable<UpdateLoyaltyPointsToStoreCreditCronJobModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(UpdateLoyaltyPointsToStoreCreditJob.class);

	@Resource(name = "loyaltyPointService")
	private LoyaltyPointService loyaltyPointService;

	@Resource(name = "customCustomerService")
	private CustomCustomerService customCustomerService;


	@Override
	public PerformResult perform(final UpdateLoyaltyPointsToStoreCreditCronJobModel cronjob)
	{

		final BaseStoreModel store = cronjob.getStore();
		if (!getLoyaltyPointService().isLoyaltyPointsEnabled(store))
		{
			LOG.warn("Loyalty Points is Disabled ");
			return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
		}

		final List<CustomerModel> customers = getCustomCustomerService()
				.getAllCustomersAboveLoyaltyPointLimit(store.getConvertLimitLoyaltyPoints());

		customers.stream().forEach(customer -> {
			try
			{
				getLoyaltyPointService().redeemLoyaltyPoints(store, customer);
				LOG.info("Loyalty Points updated to StoreCreditJob for customer : " + customer.getCustomerID());
			}
			catch (final LoyaltyPointException e)
			{
				LOG.info("Loyalty Points can not updated to StoreCreditJob for customer : " + customer.getCustomerID());
			}
		});

		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}


	protected LoyaltyPointService getLoyaltyPointService()
	{
		return loyaltyPointService;
	}

	protected CustomCustomerService getCustomCustomerService()
	{
		return customCustomerService;
	}

}
