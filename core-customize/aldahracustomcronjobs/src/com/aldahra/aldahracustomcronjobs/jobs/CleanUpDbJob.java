/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahracustomcronjobs.jobs;

import static java.lang.String.format;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.jobs.maintenance.MaintenanceCleanupStrategy;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.joda.time.Days;
import org.joda.time.LocalDateTime;

import com.aldahra.aldahracustomcronjobs.model.CleanUpItemConfigModel;
import com.aldahra.aldahracustomcronjobs.model.DbCleanUpCronjobModel;
/**
 *
 */
public class CleanUpDbJob implements MaintenanceCleanupStrategy<ItemModel, DbCleanUpCronjobModel>
{

   private static final Logger LOG = Logger.getLogger(CleanUpDbJob.class);

	@Resource(name = "modelService")
	private ModelService modelService;

	@Override
	public FlexibleSearchQuery createFetchQuery(final DbCleanUpCronjobModel cjm)
	{

		return createQuery(cjm.getItemsConfig());
	}

   private FlexibleSearchQuery createQuery(final Set<CleanUpItemConfigModel> itemsConfig) {
      final StringBuilder builder = new StringBuilder();

      final Map<String, Object> params = new HashMap<>();

      if (itemsConfig.size() > 1) {

          builder.append("SELECT item.pk FROM (");

          final Iterator<CleanUpItemConfigModel> itemConfigModels = itemsConfig.iterator();

          for (int i = 0; i < itemsConfig.size(); i++) {
              final CleanUpItemConfigModel cleanUpItemConfig = itemConfigModels.next();
              final String dateAlias = "date" + i;
              builder.append("{{ ");
              builder.append("SELECT {" + ItemModel.PK + "} FROM {" + cleanUpItemConfig.getItem().getCode() + "}");
				  builder.append(" WHERE {" + ItemModel.MODIFIEDTIME + "} < ?" + dateAlias);

              if ("ImpExMedia".equalsIgnoreCase(cleanUpItemConfig.getItem().getCode())) {
                  builder.append(" AND {" + MediaModel.REMOVABLE + "} = 1");
              }

              builder.append(" }}");
              params.put(dateAlias, getThresholdDate(cleanUpItemConfig.getDays()));

              if (itemConfigModels.hasNext()) {
                  builder.append(" UNION ");
              }
          }

          builder.append(") item");

      } else {
          final CleanUpItemConfigModel cleanUpItemConfig = itemsConfig.iterator().next();

          builder.append("SELECT {" + ItemModel.PK + "} FROM {" + cleanUpItemConfig.getItem().getCode() + "}");
			 builder.append(" WHERE {" + ItemModel.MODIFIEDTIME + "} < ?date");

			 params.put("date", getThresholdDate(cleanUpItemConfig.getDays()));

      }
      final FlexibleSearchQuery query = new FlexibleSearchQuery(builder);
		query.addQueryParameters(params);
      if (LOG.isDebugEnabled()) {
			LOG.debug(format("Query is [%s] with params [%s]", query.getQuery(), query.getQueryParameters().toString()));
      }
      return query;
  }

  private Date getThresholdDate(final Integer days) {
      return new LocalDateTime().minus(Days.days(days)).toDate();
  }

	@Override
	public void process(final List<ItemModel> elements)
	{
		  if (LOG.isDebugEnabled()) {
           LOG.debug(format("Found %d items to remove", elements.size()));
       }
       modelService.removeAll(elements);

	}

}
