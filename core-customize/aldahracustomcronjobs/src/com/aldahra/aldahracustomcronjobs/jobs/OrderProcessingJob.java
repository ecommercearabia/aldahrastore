/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahracustomcronjobs.jobs;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.aldahra.aldahracustomcronjobs.model.CreateShipmentCronJobModel;
import com.aldahra.aldahracustomcronjobs.model.OrderProcessingCronJobModel;
import com.aldahra.aldahratimeslot.service.TimeSlotService;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class OrderProcessingJob extends AbstractJobPerformable<OrderProcessingCronJobModel>
{
	protected static final Logger LOG = Logger.getLogger(OrderProcessingJob.class);
	private static final String CRONJOB_FINISHED = "OrderProcessingJob is Finished ...";
	@Resource(name = "timeSlotService")
	private TimeSlotService timeSlotService;

	@Resource(name = "cronJobService")
	private CronJobService cronJobService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Override
	public PerformResult perform(final OrderProcessingCronJobModel cronjob)
	{
		LOG.info("OrderProcessingJob is Starting ...");
		final List<OrderModel> orders = timeSlotService.getOrdersByInterval(cronjob.getStore(), cronjob.getTimezone(),
				cronjob.getIntervalLength());
		if (CollectionUtils.isEmpty(orders))
		{
			LOG.info("No orders found within the CronJob interval");
			LOG.info(CRONJOB_FINISHED);
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
		orders.stream().filter(Objects::nonNull).forEach(o -> {
			final Set<ConsignmentModel> consignments = getConsignmentsForOrder(o);
			if (consignments.isEmpty())
			{
				LOG.info("No Consignments found for order code: " + o.getCode());
				return;
			}
			consignments.stream().filter(Objects::nonNull).forEach(c -> {
				if (!StringUtils.isEmpty(c.getTrackingID()) && c.getCarrierDetails() != null)
				{
					LOG.info("A shipment has been already created for consignment code: " + c.getCode());
					return;
				}
				startCreateShipmentCronJob(c, o.getCode());
			});
		});
		LOG.info(CRONJOB_FINISHED);
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}


	private Set<ConsignmentModel> getConsignmentsForOrder(final OrderModel order)
	{
		if (CollectionUtils.isEmpty(order.getConsignments()))
		{
			LOG.info("No consignments found for order: " + order.getCode());
			return Collections.emptySet();
		}
		return order.getConsignments();
	}


	private void startCreateShipmentCronJob(final ConsignmentModel consignment, final String orderCode)
	{
		final CreateShipmentCronJobModel shipmentCronJob = createCronJobFor(consignment, orderCode);
		consignment.setShipmentCronJob(shipmentCronJob);
		modelService.save(consignment);
		cronJobService.performCronJob(shipmentCronJob);
	}

	private CreateShipmentCronJobModel createCronJobFor(final ConsignmentModel consignment, final String orderCode)
	{
		final CreateShipmentCronJobModel cronjob = modelService.create(CreateShipmentCronJobModel.class);
		final String cronJobCode = "create-shipment-cronjob-" + orderCode + "-" + consignment.getCode() + "-"
				+ System.currentTimeMillis();
		cronjob.setCode(cronJobCode);
		cronjob.setActive(Boolean.TRUE);
		cronjob.setConsignments(Collections.singleton(consignment));
		cronjob.setLogToDatabase(false);
		cronjob.setLogToFile(true);
		cronjob.setJob(cronJobService.getJob("createShipmentJob"));
		modelService.save(cronjob);
		modelService.refresh(cronjob);
		LOG.info("CreateShipmentCronJob created with code: " + cronJobCode);
		return cronjob;
	}
}
