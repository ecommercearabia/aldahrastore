/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahracustomcronjobs.jobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aldahra.aldahracustomcronjobs.model.UpdateProductStockLevelCronJobModel;
import com.aldahra.aldahracustomcronjobs.service.UpdateERPProductStockJobService;
import com.aldahra.aldahraproduct.model.GroceryVariantProductModel;
import com.aldahra.core.service.GroceryProductService;
import com.google.common.collect.Lists;


/**
 * @author monzer OLD UpdateProductStockLevelCronJob
 */
public class UpdateProductStockLevelJob extends AbstractJobPerformable<UpdateProductStockLevelCronJobModel>
{

	private static final Logger LOG = LoggerFactory.getLogger(UpdateProductStockLevelJob.class);

	@Resource(name = "updateProductStockJobService")
	private UpdateERPProductStockJobService updateProductStockJobService;

	@Resource(name = "groceryProductService")
	private GroceryProductService groceryProductService;

	@Override
	public PerformResult perform(final UpdateProductStockLevelCronJobModel cronjob)
	{
		final List<GroceryVariantProductModel> allProducts = groceryProductService
				.getAllByCatalogVersion(cronjob.getProductCatalogVersion());
		//		final List<String> allProducts = Arrays.asList("V203013NA", "V203012NA", "P200050AE", "P200059AE", "P200008AE",
		//				"P200003MY");
		final int buldSize = cronjob.getProductBulkSize() != null && cronjob.getProductBulkSize().intValue() > 0
				? cronjob.getProductBulkSize().intValue()
				: 5;
		final List<List<GroceryVariantProductModel>> partitionedProducts = Lists.partition(allProducts, buldSize);
		//		final List<List<String>> partitionedProducts = Lists.partition(allProducts, buldSize);
		partitionedProducts.stream()
				.forEach(partition -> updateProductStockJobService.getERPProductStockAndUpdate(cronjob, partition));

		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

}
