<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
 Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
-->
<items 	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
			xsi:noNamespaceSchemaLocation="items.xsd">
	<collectiontypes>
		<collectiontype elementtype="Consignment" code="consignmentsSet" type="set" autocreate="true"/>
	</collectiontypes>
	
	<relations>
		<relation code="DbCleanUpCronjob2CleanUpItemConfig" localized="false" generate="true" autocreate="true">
            <sourceElement type="DbCleanUpCronjob" cardinality="one" qualifier="job">
                <modifiers read="true" write="true" search="true" initial="true" optional="true" unique="true"/>
            </sourceElement>
            <targetElement type="CleanUpItemConfig" cardinality="many" qualifier="itemsConfig" collectiontype="set">
                <modifiers read="true" write="true" search="true" optional="true" partof="true"/>
            </targetElement>
        </relation>
	</relations>
	
	<itemtypes>
		<itemtype code="OrderProcessingCronJob" extends="CronJob" autocreate="true" generate="true">
			<attributes>
				<attribute qualifier="store" type="BaseStore">
					<description>Instance of Base Store Model</description>
					<modifiers read="true" write="true" optional="false" />
					<persistence type="property" />
				</attribute>
				<attribute qualifier="intervalLength" type="long">
					<modifiers read="true" write="true" optional="false" />
					<persistence type="property" />
					<description>Interval Length to for checking orders in seconds</description>
					<defaultvalue>Long.valueOf(7200)</defaultvalue>
				</attribute>
				<attribute qualifier="timezone" type="java.lang.String">
					<modifiers read="true" write="true" optional="false" />
					<persistence type="property" />
					<description>Timezone of the cronjob</description>
					<defaultvalue>java.lang.String.valueOf("Asia/Dubai")</defaultvalue>
				</attribute>
			</attributes>
		</itemtype>
		
		<itemtype code="CreateShipmentCronJob" extends="CronJob" autocreate="true" generate="true">
			<attributes>
				<attribute qualifier="consignments" type="consignmentsSet">
					<modifiers read="true" write="true" optional="false" />
					<persistence type="property" />
					<description>Consignments to create shipments for</description>
				</attribute>
			</attributes>
		</itemtype>
		
		<itemtype code="Consignment" autocreate="false" generate="false">
			<attributes>
				<attribute type="CreateShipmentCronJob" qualifier="shipmentCronJob">
					<description>Instance of CreateShipmentCronJob Model</description>
					<modifiers read="true" write="true" optional="true" />
					<persistence type="property" />
				</attribute>
			</attributes>
		</itemtype>
		
		<itemtype code="SendSalesOrderCronJob" extends="CronJob" autocreate="true" generate="true">
			<attributes>
				<attribute qualifier="salesOrderQuery" type="java.lang.String">
 					<modifiers read="true" write="true" optional="false" />
 					<persistence type="property" >
 						<columntype>
							<value>HYBRIS.LONG_STRING</value>
 						</columntype>
 					</persistence>
					<description>Flexible Search Query to get the orders</description>
				</attribute>
			</attributes>
		</itemtype>
		
		<itemtype code="UpdateProductStockLevelCronJob"
			extends="CronJob">
			<attributes>
				<attribute qualifier="warehouses" type="Warehouses">
					<modifiers read="true" write="true" optional="true" />
					<persistence type="property" />
					<description>warehouses</description>
				</attribute>
				<attribute qualifier="autoCreate" type="boolean">
					<modifiers read="true" write="true" optional="true" />
					<persistence type="property" />
					<description>autoCreate</description>
					<defaultvalue>Boolean.TRUE</defaultvalue>
				</attribute>
				<attribute qualifier="productCatalogVersion"
					type="CatalogVersion">
					<description>Product Catalog Version</description>
					<modifiers initial="true" optional="false" />
					<persistence type="property" />
				</attribute>
				<attribute qualifier="fixedStock" type="long">
					<modifiers read="true" write="true" search="true"
						optional="true" />
					<persistence type="property" />
					<description>Fixed Stock</description>
				</attribute>
				<attribute qualifier="updateFixedStock" type="boolean">
					<modifiers read="true" write="true" optional="true" />
					<persistence type="property" />
					<description>autoCreate</description>
					<defaultvalue>Boolean.TRUE</defaultvalue>
				</attribute>
				<attribute qualifier="removeInventoryEvents"
					type="java.lang.Boolean">
					<modifiers read="true" write="true" search="true"
						optional="true" />
					<persistence type="property" />
					<description>Remove inventory events</description>
					<defaultvalue>Boolean.TRUE</defaultvalue>
				</attribute>
				<attribute qualifier="resetStockReserved" type="boolean">
					<persistence type="property" />
					<defaultvalue>Boolean.FALSE</defaultvalue>
				</attribute>
				<attribute qualifier="enableSaveStockResponse" type="boolean">
					<persistence type="property" />
					<defaultvalue>Boolean.FALSE</defaultvalue>
				</attribute>
				<attribute qualifier="store" type="BaseStore">
					<modifiers read="true" write="true" optional="false" />
					<persistence type="property" />
					<description>The base store  which the cronjob runs on</description>
				</attribute>
				<attribute qualifier="productBulkSize" type="java.lang.Integer">
					<modifiers read="true" write="true" optional="true" />
					<persistence type="property" />
					<description>The number of products would be sent in each request</description>
				</attribute>
			</attributes>
		</itemtype>
		
		<itemtype code="SendPurchaseRequestForProductMinimumAmount" extends="CronJob" autocreate="true" generate="true">
			<attributes>
				<attribute qualifier="warehouses" type="Warehouses">
					<modifiers read="true" write="true" optional="true" />
					<persistence type="property" />
					<description>warehouses</description>
				</attribute>
				<attribute qualifier="productCatalogVersion"
					type="CatalogVersion">
					<description>Product Catalog Version</description>
					<modifiers initial="true" optional="false" />
					<persistence type="property" />
				</attribute>
				<attribute qualifier="store" type="BaseStore">
					<modifiers read="true" write="true" optional="false" />
					<persistence type="property" />
					<description>The base store  which the cronjob runs on</description>
				</attribute>
			</attributes>
		</itemtype>
		
		<itemtype code="DbCleanUpCronjob" extends="CronJob" generate="true" autocreate="true">
                <description>Cronjob required to clean up database tables</description>
            </itemtype>
            <itemtype code="CleanUpItemConfig" generate="true" autocreate="true">
                <description>DbCleanUpCronjob item config</description>
                <deployment table="cleanupitemconfig" typecode="15576"/>
                <attributes>
                    <attribute qualifier="item" type="ComposedType">
                        <modifiers unique="true" optional="false" write="true" initial="true"
                                   read="true" removable="true" search="true"/>
                        <persistence type="property"/>
                    </attribute>
                    <attribute qualifier="days" type="java.lang.Integer">
                        <modifiers optional="false" write="true" read="true" removable="true" initial="true"/>
                        <persistence type="property"/>
                    </attribute>
                </attributes>
                <indexes>
                    <index name="itemIndex">
                        <key attribute="item"/>
                    </index>
                </indexes>
            </itemtype>
       <itemtype code="UpdateLoyaltyPointsToStoreCreditCronJob" extends="CronJob" autocreate="true" generate="true">
			<attributes>
				<attribute qualifier="store" type="BaseStore">
					<description>Instance of Base Store Model</description>
					<modifiers read="true" write="true" optional="false" />
					<persistence type="property" />
				</attribute>
				</attributes>
		</itemtype>
            
	</itemtypes>
</items>
