package com.aldahra.aldahraorderfacades.constants;

@SuppressWarnings({"deprecation","squid:CallToDeprecatedMethod"})
public class AldahraorderfacadesConstants extends GeneratedAldahraorderfacadesConstants
{
	public static final String EXTENSIONNAME = "aldahraorderfacades";
	
	private AldahraorderfacadesConstants()
	{
		//empty
	}
	
	
}
