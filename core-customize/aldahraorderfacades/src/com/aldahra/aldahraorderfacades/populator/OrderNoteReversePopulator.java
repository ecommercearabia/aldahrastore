/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraorderfacades.populator;

import de.hybris.platform.converters.Populator;

import org.springframework.util.StringUtils;

import com.aldahra.aldahraorder.model.OrderNoteEntryModel;
import com.aldahra.aldahraorderfacades.data.OrderNoteData;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class OrderNoteReversePopulator implements Populator<OrderNoteData, OrderNoteEntryModel>
{

	@Override
	public void populate(final OrderNoteData data, final OrderNoteEntryModel model)
	{
		if (data != null && !StringUtils.isEmpty(data.getCode()))
		{
			model.setCode(data.getCode());
			model.setNote(data.getNote());
		}
	}

}
