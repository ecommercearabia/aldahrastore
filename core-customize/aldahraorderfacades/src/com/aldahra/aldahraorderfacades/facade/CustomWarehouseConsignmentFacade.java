/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraorderfacades.facade;


import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.warehousingfacades.order.WarehousingConsignmentFacade;



/**
 *
 */
public interface CustomWarehouseConsignmentFacade extends WarehousingConsignmentFacade
{
	public SearchPageData<ConsignmentData> searchConsignmentByCode(PageableData pageableData, String code);

}
