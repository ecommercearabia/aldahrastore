/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraorderfacades.facade;


import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.ordermanagementfacades.order.OmsOrderFacade;



/**
 *
 */
public interface CustomOmsOrderFacade extends OmsOrderFacade
{
	public SearchPageData<OrderData> searchOrderByCode(PageableData pageableData, String code);

}
