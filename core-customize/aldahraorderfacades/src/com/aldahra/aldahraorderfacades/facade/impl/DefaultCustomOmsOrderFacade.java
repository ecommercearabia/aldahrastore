/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraorderfacades.facade.impl;

import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordermanagementfacades.order.impl.DefaultOmsOrderFacade;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.aldahra.aldahraorder.dao.impl.OrderByNullVersionIdWithCodePagedDao;
import com.aldahra.aldahraorderfacades.facade.CustomOmsOrderFacade;


/**
 *
 */
public class DefaultCustomOmsOrderFacade extends DefaultOmsOrderFacade
		implements CustomOmsOrderFacade
{

	@Resource(name = "orderByNullVersionIdWithCodePagedDao")
	private OrderByNullVersionIdWithCodePagedDao orderByNullVersionIdWithCodePagedDao;

	/**
	 * @return the orderByNullVersionIdWithCodePagedDao
	 */
	protected OrderByNullVersionIdWithCodePagedDao getOrderByNullVersionIdWithCodePagedDao()
	{
		return orderByNullVersionIdWithCodePagedDao;
	}

	@Override
	public SearchPageData<OrderData> searchOrderByCode(final PageableData pageableData, final String code)
	{
		if (!StringUtils.isBlank(code))
		{
			final Map<String, String> param = new HashMap<>();
			param.put("code", code);
			final SearchPageData<OrderModel> orderSearchPageData = getOrderByNullVersionIdWithCodePagedDao().findCustom(param,
					pageableData);
			return convertSearchPageData(orderSearchPageData, getOrderConverter());
		}
		else
		{
			return getOrders(pageableData);
		}

	}


}
