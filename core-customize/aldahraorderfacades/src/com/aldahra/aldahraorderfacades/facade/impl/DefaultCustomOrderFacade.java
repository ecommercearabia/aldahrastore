/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraorderfacades.facade.impl;

import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;

import com.aldahra.aldahraorderfacades.facade.CustomOrderFacade;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class DefaultCustomOrderFacade implements CustomOrderFacade
{
	@Resource(name = "customerAccountService")
	private CustomerAccountService customerAccountService;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "userService")
	private UserService userService;

	@Override
	public int getNumberOfOrders(final OrderStatus... statuses)
	{
		final CustomerModel currentCustomer = (CustomerModel) userService.getCurrentUser();
		final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
		final List<OrderModel> orderList = customerAccountService.getOrderList(currentCustomer, currentBaseStore, statuses);
		return CollectionUtils.isEmpty(orderList) ? 0 : orderList.size();
	}

}
