/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraorderfacades.facade.impl;

import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.warehousingfacades.order.impl.DefaultWarehousingConsignmentFacade;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.aldahra.aldahraorder.dao.impl.DefaultConsignmentByCodePagedDao;
import com.aldahra.aldahraorderfacades.facade.CustomWarehouseConsignmentFacade;


/**
 *
 */
public class DefaultCustomWarehouseConsignmentFacade extends DefaultWarehousingConsignmentFacade
		implements CustomWarehouseConsignmentFacade
{

	@Resource(name = "consignmentByCodePagedDao")
	private DefaultConsignmentByCodePagedDao consignmentByCodePagedDao;

	/**
	 * @return the orderByNullVersionIdWithCodePagedDao
	 */
	protected DefaultConsignmentByCodePagedDao getConsignmentByCodePagedDao()
	{
		return consignmentByCodePagedDao;
	}

	@Override
	public SearchPageData<ConsignmentData> searchConsignmentByCode(final PageableData pageableData, final String code)
	{
		if (!StringUtils.isBlank(code))
		{
			final Map<String, String> param = new HashMap<>();
			param.put("code", code);
			final SearchPageData<ConsignmentModel> consignmentSearchPageData = getConsignmentByCodePagedDao().findCustom(param,
					pageableData);

			return convertSearchPageData(consignmentSearchPageData, getConsignmentConverter());
		}
		else
		{
			return getConsignments(pageableData);
		}

	}


}
