/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraorderfacades.facade.impl;

import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;

import com.aldahra.aldahraorder.model.OrderNoteModel;
import com.aldahra.aldahraorder.service.OrderNoteService;
import com.aldahra.aldahraorderfacades.data.OrderNoteData;
import com.aldahra.aldahraorderfacades.facade.OrderNoteFacade;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class DefaultOrderNoteFacade implements OrderNoteFacade
{
	@Resource(name = "orderNoteService")
	private OrderNoteService orderNoteService;

	@Resource(name = "orderNoteConverter")
	private Converter<OrderNoteModel, OrderNoteData> orderNoteConverter;

	@Override
	public List<OrderNoteData> getOrderNotesByCurrentSite()
	{
		final List<OrderNoteModel> orderNotes = orderNoteService.getOrderNotesByCurrentSite();
		if (CollectionUtils.isEmpty(orderNotes))
		{
			return Collections.emptyList();
		}
		return orderNoteConverter.convertAll(orderNotes);
	}

}
