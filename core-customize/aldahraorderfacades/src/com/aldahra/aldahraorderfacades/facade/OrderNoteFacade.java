/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraorderfacades.facade;

import java.util.List;

import com.aldahra.aldahraorderfacades.data.OrderNoteData;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface OrderNoteFacade
{
	public List<OrderNoteData> getOrderNotesByCurrentSite();
}
