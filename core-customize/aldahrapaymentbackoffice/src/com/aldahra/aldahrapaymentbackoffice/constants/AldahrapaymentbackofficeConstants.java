/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.aldahra.aldahrapaymentbackoffice.constants;

/**
 * Global class for all Ybackoffice constants. You can add global constants for your extension into this class.
 */
public final class AldahrapaymentbackofficeConstants extends GeneratedAldahrapaymentbackofficeConstants
{
	public static final String EXTENSIONNAME = "aldahrapaymentbackoffice";

	private AldahrapaymentbackofficeConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
