/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.aldahra.aldahrapaymentbackoffice.services;

/**
 * Hello World AldahrapaymentbackofficeService
 */
public class AldahrapaymentbackofficeService
{
	public String getHello()
	{
		return "Hello";
	}
}
