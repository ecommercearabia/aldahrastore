/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.omsfulfillment.constants;

@SuppressWarnings({"deprecation","PMD","squid:CallToDeprecatedMethod"})
public class AldahraOmsfulfillmentConstants extends GeneratedAldahraOmsfulfillmentConstants
{
	public static final String EXTENSIONNAME = "aldahraomsfulfillment";
	
	private AldahraOmsfulfillmentConstants()
	{
		//empty
	}
	
	
}
