/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.omsfulfillment.cancellation;

import de.hybris.platform.warehousing.cancellation.strategy.impl.WarehousingOrderCancelStateMappingStrategy;

public class SapCpiOmsOrderCancelStateMappingStrategy extends WarehousingOrderCancelStateMappingStrategy {
   // Restore the default functionality from the warehousing extension
}
