/**
 *
 */
package com.aldahra.aldahrauser.city.service.impl;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import com.aldahra.aldahrauser.city.dao.CityDao;
import com.aldahra.aldahrauser.city.service.CityService;
import com.aldahra.aldahrauser.model.CityModel;


/**
 * The Class DefaultCityService.
 *
 * @author mnasro
 */
public class DefaultCityService implements CityService
{

	/** The city dao. */
	@Resource(name = "cityDao")
	private CityDao cityDao;


	protected CityDao getCityDao()
	{
		return cityDao;
	}


	/**
	 * Gets the by country isocode.
	 *
	 * @param isoCode
	 *           the iso code
	 * @return the by country isocode
	 */
	@Override
	public Optional<List<CityModel>> getByCountryIsocode(final String isoCode)
	{
		return getCityDao().findCitesByIsocode(isoCode);
	}


	/**
	 * Gets the city
	 *
	 * @param code
	 *           the code
	 * @return the city model
	 */
	@Override
	public Optional<CityModel> get(final String code)
	{
		return getCityDao().find(code);
	}

	/**
	 * Gets the all city
	 *
	 * @return the all city
	 */
	@Override
	public Optional<List<CityModel>> getAll()
	{
		return getCityDao().findAll();
	}


}
