/**
 *
 */
package com.aldahra.aldahrauser.city.service;

import java.util.List;
import java.util.Optional;

import com.aldahra.aldahrauser.model.CityModel;


/**
 * The Interface CityService.
 *
 * @author mnasro
 */
public interface CityService
{
	/**
	 * Gets the by country isocode.
	 *
	 * @param isoCode
	 *           the iso code
	 * @return the by country isocode
	 */
	public Optional<List<CityModel>> getByCountryIsocode(final String isoCode);


	/**
	 * Gets the all city
	 *
	 * @return the all city
	 */
	public Optional<List<CityModel>> getAll();


	/**
	 * Gets the city by code
	 *
	 * @param code
	 *           the code
	 * @return the city model
	 */
	public Optional<CityModel> get(final String code);
}
