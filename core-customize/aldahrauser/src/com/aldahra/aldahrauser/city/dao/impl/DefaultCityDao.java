/**
 *
 */
package com.aldahra.aldahrauser.city.dao.impl;

import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;

import com.aldahra.aldahrauser.city.dao.CityDao;
import com.aldahra.aldahrauser.model.CityModel;


/**
 * The Class DefaultCityDao.
 *
 * @author mnasro
 */
public class DefaultCityDao extends DefaultGenericDao<CityModel> implements CityDao
{

	/** The common I 18 N service. */
	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	// ds
	/**
	 * Instantiates a new default city dao.
	 */
	public DefaultCityDao()
	{
		super(CityModel._TYPECODE);
	}

	/**
	 * Find cites by isocode.
	 *
	 * @param isoCode
	 *           the iso code
	 * @return the list
	 */
	@Override
	public Optional<List<CityModel>> findCitesByIsocode(final String isoCode)
	{
		ServicesUtil.validateParameterNotNull(isoCode, "isoCode must not be null");
		final Map<String, Object> params = new HashMap<>();
		params.put(CityModel.COUNTRY, getCommonI18NService().getCountry(isoCode));
		return Optional.ofNullable(find(params));
	}

	/**
	 * Find City
	 *
	 * @param code
	 *           the code
	 * @return the city model
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see com.sefam.ysefamb2ccore.dao.city.CityDao#find(java.lang.String)
	 */
	@Override
	public Optional<CityModel> find(final String code)
	{
		ServicesUtil.validateParameterNotNull(code, "code must not be null");
		final Map<String, Object> params = new HashMap<>();
		params.put(CityModel.CODE, code);
		final List<CityModel> find = find(params);
		return CollectionUtils.isEmpty(find) ? Optional.empty() : Optional.ofNullable(find.get(0));
	}

	/**
	 * Gets the common I 18 N service.
	 *
	 * @return the commonI18NService
	 */
	public CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Override
	public Optional<List<CityModel>> findAll()
	{
		final List<CityModel> find = find();
		return CollectionUtils.isEmpty(find) ? Optional.empty() : Optional.ofNullable(find);
	}

}
