/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrauser.dao;

import de.hybris.platform.core.model.user.CustomerModel;

import java.util.List;


/**
 *
 */
public interface CustomerMobileNumberDao
{

	List<CustomerModel> getCustomersByMobileNumber(String mobileNumber);

}
