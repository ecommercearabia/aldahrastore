/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrauser.dao;

import java.util.List;
import java.util.Optional;

import com.aldahra.aldahrauser.model.NationalityModel;


/**
 * The Interface NationalityDao.
 *
 * @author Tuqa
 */
public interface NationalityDao
{

	/**
	 * Find.
	 *
	 * @param code
	 *           the code
	 * @return the optional
	 */
	public Optional<NationalityModel> find(String code);


	/**
	 * Find all.
	 *
	 * @return the list
	 */
	public List<NationalityModel> findAll();
}
