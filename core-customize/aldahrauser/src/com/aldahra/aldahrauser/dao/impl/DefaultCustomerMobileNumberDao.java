/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrauser.dao.impl;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import com.aldahra.aldahrauser.dao.CustomerMobileNumberDao;
import com.google.common.base.Preconditions;

/**
 * @author monzer
 */
public class DefaultCustomerMobileNumberDao implements CustomerMobileNumberDao
{

	private static final String PHONE_QUERY = "SELECT {pk} FROM {" + CustomerModel._TYPECODE + "} WHERE {"
			+ CustomerModel.MOBILENUMBER + "}=?" + CustomerModel.MOBILENUMBER;

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	@Override
	public List<CustomerModel> getCustomersByMobileNumber(final String mobileNumber)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(mobileNumber), "Empty mobile number");

		final FlexibleSearchQuery query = new FlexibleSearchQuery(PHONE_QUERY);
		query.addQueryParameter(CustomerModel.MOBILENUMBER, mobileNumber);

		final SearchResult<CustomerModel> searchResult = flexibleSearchService.<CustomerModel> search(query);
		if (searchResult.getCount() <= 0 || CollectionUtils.isEmpty(searchResult.getResult()))
		{
			return Collections.emptyList();
		}

		return searchResult.getResult();
	}

}
