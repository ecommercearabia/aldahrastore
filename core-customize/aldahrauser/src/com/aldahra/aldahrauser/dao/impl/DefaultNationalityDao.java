/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrauser.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.util.CollectionUtils;

import com.aldahra.aldahrauser.dao.NationalityDao;
import com.aldahra.aldahrauser.model.NationalityModel;


/**
 * The Class DefaultNationalityDao.
 *
 * @author Tuqa
 */
public class DefaultNationalityDao extends DefaultGenericDao<NationalityModel> implements NationalityDao
{
	private ServicesUtil servicesUtil;

	/**
	 * @return the servicesUtil
	 */
	public ServicesUtil getServicesUtil()
	{
		return servicesUtil;
	}

	/**
	 *
	 * @param typecode
	 *           the typecode
	 */
	public DefaultNationalityDao(final String typecode)
	{
		super(typecode);

	}

	/**
	 * Instantiates a new default area dao.
	 */
	public DefaultNationalityDao()
	{
		super(NationalityModel._TYPECODE);
	}

	/**
	 * Find.
	 *
	 * @param code
	 *           the code
	 * @return the optional
	 */
	@Override
	public Optional<NationalityModel> find(final String code)
	{

		getServicesUtil().validateParameterNotNull(code, "code must not be null");
		final Map<String, Object> params = new HashMap<>();
		params.put(NationalityModel.CODE, code);
		final List<NationalityModel> find = find(params);
		return find.stream().findFirst();
	}

	/**
	 * Find all.
	 *
	 * @return the list
	 */
	@Override
	public List<NationalityModel> findAll()
	{
		final List<NationalityModel> find = find();
		return CollectionUtils.isEmpty(find) ? Collections.emptyList() : find;
	}

}
