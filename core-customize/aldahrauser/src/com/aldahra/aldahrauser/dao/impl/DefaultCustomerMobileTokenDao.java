/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrauser.dao.impl;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import com.aldahra.aldahrauser.dao.CustomerMobileTokenDao;
import com.aldahra.aldahrauser.model.MobileTokenModel;
import com.google.common.base.Preconditions;


/**
 *
 */
public class DefaultCustomerMobileTokenDao implements CustomerMobileTokenDao
{

	private static final String MOBILE_TOKEN_QUERY = "SELECT {pk} FROM {" + MobileTokenModel._TYPECODE + "} WHERE {"
			+ MobileTokenModel.TOKEN + "}=?" + MobileTokenModel.TOKEN;


	private static final String MOBILE_TOKEN_BY_CUSTOMER_QUERY = "SELECT {pk} FROM {" + MobileTokenModel._TYPECODE + "} WHERE {"
			+ CustomerModel._TYPECODE + "}=?" + MobileTokenModel.CUSTOMER;

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	@Override
	public Optional<MobileTokenModel> getMobileToken(final String mobileToken)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(mobileToken), "Empty mobile number");

		final FlexibleSearchQuery query = new FlexibleSearchQuery(MOBILE_TOKEN_QUERY);
		query.addQueryParameter(MobileTokenModel.TOKEN, mobileToken);

		final SearchResult<MobileTokenModel> searchResult = flexibleSearchService.<MobileTokenModel> search(query);
		if (searchResult.getCount() <= 0 || CollectionUtils.isEmpty(searchResult.getResult()))
		{
			return Optional.empty();
		}

		return Optional.of(searchResult.getResult().get(0));
	}

	@Override
	public List<MobileTokenModel> getCustomerMobileTokens(final CustomerModel customer)
	{
		Preconditions.checkNotNull(customer, "Customer Is null");

		final FlexibleSearchQuery query = new FlexibleSearchQuery(MOBILE_TOKEN_BY_CUSTOMER_QUERY);
		query.addQueryParameter(MobileTokenModel.CUSTOMER, customer.getPk());

		final SearchResult<MobileTokenModel> searchResult = flexibleSearchService.<MobileTokenModel> search(query);
		if (searchResult.getCount() <= 0 || CollectionUtils.isEmpty(searchResult.getResult()))
		{
			return Collections.emptyList();
		}

		return searchResult.getResult();
	}

}
