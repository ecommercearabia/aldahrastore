/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrauser.dao;

import de.hybris.platform.core.model.user.CustomerModel;

import java.util.List;
import java.util.Optional;

import com.aldahra.aldahrauser.model.MobileTokenModel;


/**
 * @author monzer
 */
public interface CustomerMobileTokenDao
{

	Optional<MobileTokenModel> getMobileToken(String mobileToken);

	List<MobileTokenModel> getCustomerMobileTokens(CustomerModel customer);

}
