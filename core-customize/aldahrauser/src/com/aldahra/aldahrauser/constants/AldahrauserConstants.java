/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrauser.constants;

/**
 * Global class for all Aldahrauser constants. You can add global constants for your extension into this class.
 */
public final class AldahrauserConstants extends GeneratedAldahrauserConstants
{
	public static final String EXTENSIONNAME = "aldahrauser";

	private AldahrauserConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "aldahrauserPlatformLogo";
}
