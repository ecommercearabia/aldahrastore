package com.aldahra.aldahrauser.attribute.handlers;

import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import org.apache.log4j.Logger;

import com.aldahra.aldahrauser.model.MobileTokenModel;


public class TokenEncodingHandler extends AbstractDynamicAttributeHandler<String, MobileTokenModel>
{

	private static final Logger LOG = Logger.getLogger(TokenEncodingHandler.class);

	@Override
	public String get(final MobileTokenModel model)
	{
		String token = model.getToken();
		if (token == null)
		{
			return null;
		}

		try
		{
			token = URLEncoder.encode(token, StandardCharsets.UTF_8.toString());
		}
		catch (final UnsupportedEncodingException e)
		{
			LOG.error(String.format("token from TokenEncodingHandler could not be encoded : %s", token), e);
		}
		return token;
	}


}
