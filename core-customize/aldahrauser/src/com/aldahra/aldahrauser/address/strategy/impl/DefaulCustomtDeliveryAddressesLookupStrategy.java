package com.aldahra.aldahrauser.address.strategy.impl;

import de.hybris.platform.commerceservices.strategies.DeliveryAddressesLookupStrategy;
import de.hybris.platform.commerceservices.strategies.impl.DefaultDeliveryAddressesLookupStrategy;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.util.CollectionUtils;


public class DefaulCustomtDeliveryAddressesLookupStrategy
		extends DefaultDeliveryAddressesLookupStrategy
		implements DeliveryAddressesLookupStrategy
{

	@Override
	public List<AddressModel> getDeliveryAddressesForOrder(final AbstractOrderModel abstractOrder,
			final boolean visibleAddressesOnly)
	{
		final List<AddressModel> addressesForOrder = new ArrayList<>();
		if (abstractOrder != null)
		{
			final UserModel user = abstractOrder.getUser();
			if (user instanceof CustomerModel)
			{
				List<AddressModel> allAddresses;
				if (visibleAddressesOnly)
				{
					allAddresses = getCustomerAccountService().getAddressBookDeliveryEntries((CustomerModel) user);
				}
				else
				{
					allAddresses = getCustomerAccountService().getAllAddressEntries((CustomerModel) user);
				}
				final Collection<CountryModel> deliveryCountries = abstractOrder.getStore().getDeliveryCountries();
				List<String> isoCodes = null;
				if (!CollectionUtils.isEmpty(allAddresses) && !CollectionUtils.isEmpty(deliveryCountries))
				{
					isoCodes = deliveryCountries.stream().map(c -> c.getIsocode()).collect(Collectors.toList());
					for (final AddressModel address : allAddresses)
					{
						if (isoCodes.contains(address.getCountry().getIsocode()))
						{
							addressesForOrder.add(address);
						}
					}
				}

				// If the user had no addresses, check the order for an address in case it's a guest checkout.
				if (getCheckoutCustomerStrategy().isAnonymousCheckout() && addressesForOrder.isEmpty()
						&& abstractOrder.getDeliveryAddress() != null)
				{
					addressesForOrder.add(abstractOrder.getDeliveryAddress());
				}
			}
		}
		return addressesForOrder;
	}
}
