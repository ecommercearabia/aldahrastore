/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrauser.service;

import de.hybris.platform.core.model.user.CustomerModel;

import java.util.List;
import java.util.Optional;

import com.aldahra.aldahrauser.model.MobileTokenModel;


/**
 * @author monzer
 */
public interface CustomerMobileTokenService
{

	Optional<MobileTokenModel> getCustomersByMobileToken(String mobileToken);

	boolean isValidMobileToken(String mobileToken);

	void updateCurrentCustomerMobileToken(String mobileToken);

	void updateCustomerMobileToken(CustomerModel customer, String mobileToken);

	List<MobileTokenModel> getCurrentCustomerMobileTokens();

}
