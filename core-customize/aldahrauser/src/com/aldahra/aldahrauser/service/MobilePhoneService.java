/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrauser.service;

import de.hybris.platform.core.model.user.CustomerModel;

import java.util.List;
import java.util.Optional;


/**
 * The Interface MobilePhoneService.
 *
 * @author mnasro
 */
public interface MobilePhoneService
{

	/**
	 * Validate and normalize phone number by iso code.
	 *
	 * @param countryIsoCode
	 *           the country iso code
	 * @param number
	 *           the number
	 * @return the string
	 */
	public Optional<String> validateAndNormalizePhoneNumberByIsoCode(final String countryIsoCode, final String number);

	public Optional<String> validateAndNormalizePhoneNumberByCurrentSite(final String number);

	public List<CustomerModel> getCustomersByMobileNumber(String mobileNumber);

	public boolean isMobileNumberValidByCurrentCustomer(String mobileNumber);

}
