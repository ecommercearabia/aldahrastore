/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrauser.service;

import de.hybris.platform.core.model.user.CustomerModel;

import java.util.Collection;


/**
 *
 */
public interface SingatureIdEncoderService
{

	public String encode(CustomerModel customer, String plainSignature, String signatureEncoding);

	public String decode(CustomerModel customer);

	public boolean isSupportedEncoding(String encoding);

	public Collection<String> getSupportedEncodings();

	public boolean isValid(CustomerModel customer, String plainSignature);

	public void setSignatureIdForCustomer(CustomerModel customer, String plainSignatureId);

}
