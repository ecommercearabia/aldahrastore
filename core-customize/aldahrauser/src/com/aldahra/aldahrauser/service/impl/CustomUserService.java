/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrauser.service.impl;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.servicelayer.user.impl.DefaultUserService;


/**
 * @author monzer
 */
public class CustomUserService extends DefaultUserService implements UserService
{

	@Override
	public void setPassword(final UserModel user, final String plainPassword, final String encoding)
	{
		super.setPassword(user, plainPassword, encoding);
		getModelService().save(user);
		getModelService().refresh(user);
		if (user instanceof CustomerModel)
		{
			final CustomerModel customer = (CustomerModel) user;
			customer.setSignatureId(null);
			getModelService().save(customer);

		}
	}

}
