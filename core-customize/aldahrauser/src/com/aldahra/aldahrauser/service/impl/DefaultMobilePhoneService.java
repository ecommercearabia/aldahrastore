package com.aldahra.aldahrauser.service.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import com.aldahra.aldahrauser.dao.CustomerMobileNumberDao;
import com.aldahra.aldahrauser.service.MobilePhoneService;
import com.google.common.base.Preconditions;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;


/**
 * The Class DefaultMobilePhoneService.
 *
 * @author mnasro
 */
public class DefaultMobilePhoneService implements MobilePhoneService
{
	/** The common I 18 N service. */
	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "customerMobileNumberDao")
	private CustomerMobileNumberDao customerMobileNumberDao;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	/**
	 * Validate and normalize phone number by iso code.
	 *
	 * @param countryIsoCode
	 *           the country iso code
	 * @param number
	 *           the number
	 * @return the Optional<String>
	 */
	@Override
	public Optional<String> validateAndNormalizePhoneNumberByIsoCode(final String countryIsoCode, final String number)
	{
		validateParameterNotNull(countryIsoCode, "countryIsoCode cannot be null");
		validateParameterNotNull(number, "number cannot be null");

		final PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
		Phonenumber.PhoneNumber phoneNumber = null;
		try
		{
			phoneNumber = phoneNumberUtil.parse(number, countryIsoCode);
		}
		catch (final NumberParseException e)
		{
			return Optional.empty();
		}

		final boolean isValid = phoneNumberUtil.isValidNumber(phoneNumber);

		if (!isValid)
		{
			return Optional.empty();
		}
		final String e164FormatNumber = phoneNumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164);
		if (e164FormatNumber == null)
		{
			return Optional.empty();
		}
		return Optional.ofNullable(e164FormatNumber.replaceAll("\\+", ""));
	}

	@Override
	public List<CustomerModel> getCustomersByMobileNumber(final String mobileNumber)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(mobileNumber), "Mobile Number is empty");
		return customerMobileNumberDao.getCustomersByMobileNumber(mobileNumber);
	}

	@Override
	public boolean isMobileNumberValidByCurrentCustomer(final String mobileNumber)
	{
		final UserModel currentUser = getUserService().getCurrentUser();
		if (!(currentUser instanceof CustomerModel))
		{
			return false;
		}

		final List<CustomerModel> customers = getCustomersByMobileNumber(mobileNumber);

		if (getUserService().isAnonymousUser(currentUser))
		{
			return CollectionUtils.isEmpty(customers);
		}
		else
		{
			final CustomerModel currentCustomer = (CustomerModel) currentUser;
			for (final CustomerModel customerModel : customers)
			{
				if (!currentCustomer.getUid().equals(customerModel.getUid()))
				{
					return false;
				}
			}
			return true;
		}
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	@Override
	public Optional<String> validateAndNormalizePhoneNumberByCurrentSite(final String number)
	{
		validateParameterNotNull(number, "number cannot be null");
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		validateParameterNotNull(currentSite, "currentSite cannot be null");
		String defaultMobileCountry = null;
		if (cmsSiteService.getCurrentSite() == null || cmsSiteService.getCurrentSite().getDefaultMobileCountry() == null
				|| StringUtils.isBlank(cmsSiteService.getCurrentSite().getDefaultMobileCountry().getIsdcode()))
		{
			return Optional.empty();
		}
		defaultMobileCountry = cmsSiteService.getCurrentSite().getDefaultMobileCountry().getIsocode();
		return validateAndNormalizePhoneNumberByIsoCode(defaultMobileCountry, number);
	}

}
