/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrauser.service.impl;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.aldahra.aldahrauser.dao.CustomerMobileTokenDao;
import com.aldahra.aldahrauser.model.MobileTokenModel;
import com.aldahra.aldahrauser.service.CustomerMobileTokenService;
import com.google.common.base.Preconditions;


/**
 * @author monzer
 */
public class DefaultCustomerMobileTokenService implements CustomerMobileTokenService
{

	/**
	 *
	 */
	private static final String MOBILE_TOKEN_CANNOT_BE_EMPTY = "Mobile token cannot be empty";

	@Resource(name = "customerMobileTokenDao")
	private CustomerMobileTokenDao customerMobileTokenDao;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "userService")
	private UserService userService;

	@Override
	public Optional<MobileTokenModel> getCustomersByMobileToken(final String mobileToken)
	{
		return getCustomerMobileTokenDao().getMobileToken(mobileToken);
	}

	@Override
	public boolean isValidMobileToken(final String mobileToken)
	{
		if (StringUtils.isBlank(mobileToken))
		{
			return false;
		}
		final UserModel currentUser = getUserService().getCurrentUser();
		if (!(currentUser instanceof CustomerModel))
		{
			return false;
		}

		final Optional<MobileTokenModel> tokens = getCustomersByMobileToken(mobileToken);

		if (tokens.isEmpty())
		{
			return true;
		}
		else
		{
			final CustomerModel currentCustomer = (CustomerModel) currentUser;
			return currentCustomer.getMobileTokens().stream()
					.anyMatch(token -> !Objects.isNull(token) && mobileToken.equals(token.getToken()));
		}
	}

	protected Optional<MobileTokenModel> checkTokenForCustomer(final CustomerModel customer, final String mobileToken)
	{
		return customer.getMobileTokens().stream().filter(token -> !Objects.isNull(token) && mobileToken.equals(token.getToken()))
				.findAny();
	}

	@Override
	public void updateCurrentCustomerMobileToken(final String mobileToken)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(mobileToken), MOBILE_TOKEN_CANNOT_BE_EMPTY);

		final UserModel currentUser = getUserService().getCurrentUser();
		Preconditions.checkArgument(!Objects.isNull(currentUser) && currentUser instanceof CustomerModel,
				"No customers in the current session");

		final CustomerModel currentCustomer = (CustomerModel) currentUser;
		updateCustomerMobileToken(currentCustomer, mobileToken);
	}

	@Override
	public void updateCustomerMobileToken(final CustomerModel customer, final String mobileToken)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(mobileToken), MOBILE_TOKEN_CANNOT_BE_EMPTY);
		Preconditions.checkArgument(!Objects.isNull(customer), "Customer is empty!");
		//Preconditions.checkArgument(isValidMobileToken(mobileToken), "Mobile token " + mobileToken + " is invalid");

		final Optional<MobileTokenModel> tokenByMobileToken = getCustomersByMobileToken(mobileToken);
		if (tokenByMobileToken.isPresent() && !tokenByMobileToken.get().getCustomer().getUid().equals(customer.getUid()))
		{
			removeTokenFromOtherUsers(customer, tokenByMobileToken.get());
			changeTokenToCustomer(customer, tokenByMobileToken.get());
			return;
		}
		else if (tokenByMobileToken.isPresent() && tokenByMobileToken.get().getCustomer().getUid().equals(customer.getUid()))
		{
			return; // [Monzer]: In this case, the token would be on the customer already and no need to remove it and set it againn.
		}

		final MobileTokenModel token = getModelService().create(MobileTokenModel.class);
		token.setToken(mobileToken);
		token.setCustomer(customer);
		getModelService().save(token);

		final Set<MobileTokenModel> tokens = new HashSet<>();
		if (!CollectionUtils.isEmpty(customer.getMobileTokens()))
		{
			tokens.addAll(customer.getMobileTokens());
		}
		tokens.add(token);
		customer.setMobileTokens(tokens);
		getModelService().save(customer);
	}

	private void changeTokenToCustomer(final CustomerModel customer, final MobileTokenModel tokenByMobileToken)
	{
		tokenByMobileToken.setCustomer(customer);
		final Set<MobileTokenModel> tokens = new HashSet<>();
		if (!CollectionUtils.isEmpty(customer.getMobileTokens()))
		{
			tokens.addAll(customer.getMobileTokens());
		}
		tokens.add(tokenByMobileToken);
		customer.setMobileTokens(tokens);
		modelService.saveAll(customer, tokenByMobileToken);
	}

	private void removeTokenFromOtherUsers(final CustomerModel customer, final MobileTokenModel mobileToken)
	{
		final Optional<MobileTokenModel> customerToken = checkTokenForCustomer(customer, mobileToken.getToken());
		if (customerToken.isEmpty())
		{
			return;
		}
		final MobileTokenModel mobileTokenModel = customerToken.get();

		final Set<MobileTokenModel> tokens = new HashSet<>();
		if (!CollectionUtils.isEmpty(customer.getMobileTokens()))
		{
			tokens.remove(mobileTokenModel);
		}
		customer.setMobileTokens(tokens);
		modelService.saveAll(customer, customerToken.get());
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @return the customerMobileTokenDao
	 */
	public CustomerMobileTokenDao getCustomerMobileTokenDao()
	{
		return customerMobileTokenDao;
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}


	@Override
	public List<MobileTokenModel> getCurrentCustomerMobileTokens()
	{
		final UserModel currentUser = getUserService().getCurrentUser();
		Preconditions.checkArgument(!Objects.isNull(currentUser) && currentUser instanceof CustomerModel,
				"No customers in the current session");
		return getCustomerMobileTokenDao().getCustomerMobileTokens((CustomerModel) currentUser);
	}

}
