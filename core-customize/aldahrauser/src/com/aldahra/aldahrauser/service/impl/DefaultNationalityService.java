/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrauser.service.impl;


import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import com.aldahra.aldahrauser.dao.NationalityDao;
import com.aldahra.aldahrauser.model.NationalityModel;
import com.aldahra.aldahrauser.service.NationalityService;


/**
 * @author Tuqa
 */
public class DefaultNationalityService implements NationalityService
{
	@Resource(name = "nationalityDao")
	private NationalityDao nationalityDao;

	/** The cms site service. */
	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	/**
	 * @return the nationalityDao
	 */
	public NationalityDao getNationalityDao()
	{
		return nationalityDao;
	}

	/**
	 * @return the cmsSiteService
	 */
	public CMSSiteService getCmsSiteService()
	{
		return cmsSiteService;
	}

	@Override
	public List<NationalityModel> getAll()
	{
		return getNationalityDao().findAll();
	}

	@Override
	public Optional<NationalityModel> get(final String code)
	{
		return getNationalityDao().find(code);
	}

	@Override
	public List<NationalityModel> getBySite(final CMSSiteModel cmsSiteModel)
	{

		if (cmsSiteModel == null)
		{
			return Collections.emptyList();
		}
		return new ArrayList<>(cmsSiteModel.getNationalities());

	}

	@Override
	public List<NationalityModel> getByCurrentSite()
	{

		final CMSSiteModel site = getCmsSiteService().getCurrentSite();

		return getBySite(site);
	}

}
