/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrauser.service.impl;

import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.persistence.security.EJBCannotDecodePasswordException;
import de.hybris.platform.persistence.security.PasswordEncoder;
import de.hybris.platform.persistence.security.PasswordEncoderFactory;
import de.hybris.platform.persistence.security.PlainTextPasswordEncoder;
import de.hybris.platform.servicelayer.user.exceptions.CannotDecodePasswordException;
import de.hybris.platform.util.Config;

import java.util.Collection;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.aldahra.aldahrauser.service.SingatureIdEncoderService;


/**
 * @author monzer
 */
public class DefaultSignatureIdEncoderService implements SingatureIdEncoderService
{

	private PasswordEncoderFactory encoderFactory;

	private final String defaultPasswordEncoding = "*";

	@Override
	public String encode(final CustomerModel customer, final String plainTextSignature, final String signatureEncoding)
	{
		return this.encoderFactory.getEncoder(signatureEncoding).encode(customer.getUid(), plainTextSignature);
	}

	@Override
	public String decode(final CustomerModel customer) throws CannotDecodePasswordException
	{
		try
		{
			return this.encoderFactory.getEncoder(customer.getPasswordEncoding()).decode(customer.getSignatureId());

		}
		catch (final EJBCannotDecodePasswordException var4)
		{
			throw new CannotDecodePasswordException(var4);
		}
	}

	@Override
	public boolean isSupportedEncoding(final String encoding)
	{
		return this.encoderFactory.isSupportedEncoding(encoding);
	}

	@Override
	public boolean isValid(final CustomerModel customer, final String plainSignature)
	{

		return !this.validatePasswordEmptiness(plainSignature) ? false
				: this.encoderFactory.getEncoder(customer.getPasswordEncoding()).check(customer.getUid(), customer.getSignatureId(),
						plainSignature);

	}

	private boolean validatePasswordEmptiness(final String signature)
	{
		return !StringUtils.isEmpty(signature) || this.acceptEmptyPassword();
	}

	private boolean acceptEmptyPassword()
	{
		return Registry.getCurrentTenant().getConfig().getBoolean("customer.password.acceptEmpty", false);
	}

	public boolean isValid(final CustomerModel customer, final String encoding, final String encodedSignature)
	{
		if (!this.validatePasswordEmptiness(encodedSignature))
		{

			return false;
		}
		else
		{
			final PasswordEncoder encoder = this.encoderFactory.getEncoder(encoding != null ? encoding : "*");

			if (encoder instanceof PlainTextPasswordEncoder)
			{
				final PasswordEncoder tokenEncoder = this.encoderFactory.getEncoder(Config.getParameter("login.token.encoder"));
				return tokenEncoder.encode(customer.getUid(), customer.getSignatureId()).equals(encodedSignature);
			}
			else
			{
				return encodedSignature.equals(customer.getEncodedPassword() == null ? "" : customer.getEncodedPassword());
			}
		}
	}

	@Override
	public void setSignatureIdForCustomer(final CustomerModel customer, final String plainSignatureId)
	{
		final String realEncoding = this.getRealEncoding(customer.getPasswordEncoding());
		customer.setSignatureId(this.encode(customer, plainSignatureId, realEncoding));
		customer.setPasswordEncoding(realEncoding);
	}

	protected String getRealEncoding(final String optionalEncoding)
	{
		return !StringUtils.isBlank(optionalEncoding) && !"*".equalsIgnoreCase(optionalEncoding) ? optionalEncoding
				: this.getDefaultPasswordEncoding();
	}

	@Override
	public Collection<String> getSupportedEncodings()
	{
		return this.encoderFactory.getSupportedEncodings();
	}

	@Required
	public void setEncoderFactory(final PasswordEncoderFactory encoderFactory)
	{
		this.encoderFactory = encoderFactory;
	}

	public String getDefaultPasswordEncoding()
	{
		return this.defaultPasswordEncoding;
	}

}
