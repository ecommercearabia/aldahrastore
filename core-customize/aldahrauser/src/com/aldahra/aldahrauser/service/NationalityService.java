/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrauser.service;

import de.hybris.platform.cms2.model.site.CMSSiteModel;

import java.util.List;
import java.util.Optional;

import com.aldahra.aldahrauser.model.NationalityModel;


/**
 * @author Tuqa
 */
public interface NationalityService
{

	public List<NationalityModel> getAll();

	public Optional<NationalityModel> get(final String code);

	public List<NationalityModel> getBySite(CMSSiteModel cmsSiteModel);

	public List<NationalityModel> getByCurrentSite();
}
