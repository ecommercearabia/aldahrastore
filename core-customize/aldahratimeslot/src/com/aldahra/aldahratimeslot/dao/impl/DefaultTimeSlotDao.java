/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahratimeslot.dao.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.store.BaseStoreModel;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.aldahra.aldahratimeslot.dao.TimeSlotDao;
import com.aldahra.aldahratimeslot.model.PeriodModel;
import com.aldahra.aldahratimeslot.model.TimeSlotInfoModel;
import com.google.common.base.Preconditions;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class DefaultTimeSlotDao extends DefaultGenericDao<TimeSlotInfoModel> implements TimeSlotDao
{
	public DefaultTimeSlotDao()
	{
		super(TimeSlotInfoModel._TYPECODE);
	}

	@Override
	public List<TimeSlotInfoModel> get(final BaseStoreModel store, final String start, final String end, final String date,
			final String timezone)
	{
		Preconditions.checkArgument(start != null, "start must not be null.");
		Preconditions.checkArgument(end != null, "end must not be null.");
		Preconditions.checkArgument(date != null, "date must not be null.");
		Preconditions.checkArgument(timezone != null, "timezone must not be null.");
		Preconditions.checkArgument(store != null, "store must not be null.");

		final StringBuffer query = new StringBuffer();
		query.append("SELECT {info.pk} FROM {TimeSlotInfo AS info JOIN Order AS o on {info.pk} = {o.timeSlotInfo}} WHERE ");
		query.append("{info.start} =");
		query.append("\'");
		query.append(start);
		query.append("\'");
		query.append(" AND {info.end} =");
		query.append("\'");
		query.append(end);
		query.append("\'");
		query.append(" AND {info.date} =");
		query.append("\'");
		query.append(date);
		query.append("\'");
		query.append(" AND {info.timezone} =");
		query.append("\'");
		query.append(timezone);
		query.append("\'");
		query.append(" AND {o.store} =");
		query.append("\'");
		query.append(store.getPk().toString());
		query.append("\'");


		final SearchResult<TimeSlotInfoModel> result = getFlexibleSearchService().search(new FlexibleSearchQuery(query.toString()));

		if (!CollectionUtils.isEmpty(result.getResult()))
		{
			return result.getResult();
		}
		return Collections.emptyList();
	}

	@Override
	public List<OrderModel> get(final BaseStoreModel store, final String timezone, final long interval)
	{
		Preconditions.checkArgument(store != null, "store must not be null.");
		Preconditions.checkArgument(timezone != null, "timezone must not be null.");
		Preconditions.checkArgument(interval != 0, "interval must not be zero or less.");

		final ZonedDateTime zonedDateTime = ZonedDateTime.ofInstant(Instant.now(), ZoneId.of(timezone)).minusSeconds(interval);

		final StringBuffer query = new StringBuffer();
		query.append("SELECT {o.pk} FROM {Order AS o JOIN TimeSlotInfo AS info on {o.timeSlotInfo} = {info.pk}} WHERE ");
		query.append("{info.date} =");
		query.append("\'");
		query.append(zonedDateTime.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		query.append("\'");
		query.append(" AND {info.startHour}+{info.startMinute}*0.01 >=");
		query.append(zonedDateTime.getHour() + zonedDateTime.getMinute() * 0.01);
		query.append(" AND {o.store} =");
		query.append(store.getPk().toString());


		final SearchResult<OrderModel> result = getFlexibleSearchService().search(new FlexibleSearchQuery(query.toString()));

		if (!CollectionUtils.isEmpty(result.getResult()))
		{
			return result.getResult();
		}
		return Collections.emptyList();
	}

	@Override
	public Optional<PeriodModel> findPeriod(final String code)
	{
		Preconditions.checkArgument(!StringUtils.isEmpty(code), "code must not be null.");
		final StringBuffer query = new StringBuffer();
		query.append("SELECT {pk} FROM {");
		query.append(PeriodModel._TYPECODE);
		query.append("} WHERE ");
		query.append("{code} =");
		query.append("\'");
		query.append(code);
		query.append("\'");
		final SearchResult<PeriodModel> result = getFlexibleSearchService().search(new FlexibleSearchQuery(query.toString()));

		if (!CollectionUtils.isEmpty(result.getResult()))
		{
			return Optional.ofNullable(result.getResult().get(0));
		}
		return Optional.empty();
	}

}
