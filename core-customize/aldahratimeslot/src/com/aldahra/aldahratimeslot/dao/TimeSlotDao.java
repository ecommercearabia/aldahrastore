/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahratimeslot.dao;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;
import java.util.Optional;

import com.aldahra.aldahratimeslot.model.PeriodModel;
import com.aldahra.aldahratimeslot.model.TimeSlotInfoModel;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface TimeSlotDao
{

	public Optional<PeriodModel> findPeriod(final String code);

	/**
	 * Retrieves TimeSlotInfoModel by the related Time Slot.
	 *
	 * @return List of timeSlotInfo, List<TimeSlotInfoModel>
	 */
	public List<TimeSlotInfoModel> get(final BaseStoreModel store, final String start, final String end, final String date,
			final String timezone);

	/**
	 * Retrieves OrderModel by the related Time Interval.
	 *
	 * @return List of orders, List<OrderModel>
	 */
	public List<OrderModel> get(final BaseStoreModel store, final String timezone, final long interval);

}
