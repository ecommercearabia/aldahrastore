/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahratimeslot.service.impl;

import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commerceservices.delivery.DeliveryService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;

import javax.annotation.Resource;

import org.apache.logging.log4j.util.Strings;
import org.springframework.util.StringUtils;

import com.aldahra.aldahratimeslot.dao.TimeSlotDao;
import com.aldahra.aldahratimeslot.enums.TimeSlotConfigType;
import com.aldahra.aldahratimeslot.model.PeriodModel;
import com.aldahra.aldahratimeslot.model.TimeSlotInfoModel;
import com.aldahra.aldahratimeslot.model.TimeSlotModel;
import com.aldahra.aldahratimeslot.service.TimeSlotService;
import com.aldahra.aldahrauser.area.service.AreaService;
import com.aldahra.aldahrauser.model.AreaModel;
import com.aldahra.core.enums.ShipmentType;
import com.google.common.base.Preconditions;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class DefaultTimeSlotService implements TimeSlotService
{
	private static final String ZONE_DELIVERY_MODE_ILLEGAL_ARGS = "zoneDeliveryModeCode must not be null or empty.";
	private static final String AREA_ILLEGAL_ARGS = "areaCode must not be null or empty.";
	private static final String PERIOD_CODE_ILLEGAL_ARGS = "periodCode must not be null or empty.";

	@Resource(name = "deliveryService")
	private DeliveryService deliveryService;

	@Resource(name = "timeSlotDao")
	private TimeSlotDao timeSlotDao;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "areaService")
	private AreaService areaService;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Override
	public Optional<TimeSlotModel> getActive(final String zoneDeliveryModeCode)
	{
		Preconditions.checkArgument(!StringUtils.isEmpty(zoneDeliveryModeCode), ZONE_DELIVERY_MODE_ILLEGAL_ARGS);
		final ZoneDeliveryModeModel zoneDeliveryModeModel = (ZoneDeliveryModeModel) deliveryService
				.getDeliveryModeForCode(zoneDeliveryModeCode);
		if (zoneDeliveryModeModel != null)
		{
			return Optional.ofNullable(zoneDeliveryModeModel.getTimeSlot());
		}
		return Optional.ofNullable(null);
	}

	@Override
	public boolean isTimeSlotEnabled(final String zoneDeliveryModeCode)
	{
		Preconditions.checkArgument(!StringUtils.isEmpty(zoneDeliveryModeCode), ZONE_DELIVERY_MODE_ILLEGAL_ARGS);
		final ZoneDeliveryModeModel deliveryMode = (ZoneDeliveryModeModel) deliveryService
				.getDeliveryModeForCode(zoneDeliveryModeCode);
		return deliveryMode != null && deliveryMode.getTimeSlot() != null && deliveryMode.getTimeSlot().isActive();
	}

	@Override
	public boolean isTimeSlotEnabledByArea(final String areaCode)
	{
		Preconditions.checkArgument(!StringUtils.isEmpty(areaCode), AREA_ILLEGAL_ARGS);
		final Optional<AreaModel> optional = areaService.get(areaCode);
		return optional.isPresent() && optional.get().getTimeSlot() != null && optional.get().getTimeSlot().isActive();
	}

	@Override
	public int getNumberOfOrdersByTimeSlot(final BaseStoreModel store, final String start, final String end, final String date,
			final String timezone)
	{
		return timeSlotDao.get(store, start, end, date, timezone).size();
	}

	@Override
	public List<OrderModel> getOrdersByInterval(final BaseStoreModel store, final String timezone, final long interval)
	{
		return timeSlotDao.get(store, timezone, interval);
	}

	@Override
	public void saveTimeSlotInfo(final TimeSlotInfoModel info, final CartModel cart, final String date, final LocalTime start)
	{
		Optional<TimeSlotModel> timeSlot;

		final AddressModel cartAddress = getCartAddress(cart);
		if (TimeSlotConfigType.BY_AREA.equals(cmsSiteService.getCurrentSite().getTimeSlotConfigType()) && cartAddress != null
				&& cartAddress.getArea() != null)
		{
			timeSlot = getActiveByArea(cartAddress.getArea().getCode());
		}
		else
		{
			timeSlot = getActive(cart.getDeliveryMode().getCode());
		}
		final String timezone = timeSlot.get().getTimezone();
		info.setTimezone(timezone);

		final ZonedDateTime startDateTime = ZonedDateTime.of(LocalDate.parse(date, DateTimeFormatter.ofPattern("dd/MM/yyyy")),
				start, ZoneId.of(timezone));

		info.setStartDate(Date.from(startDateTime.toInstant()));
		info.setStartHour(startDateTime.getHour());
		info.setStartMinute(startDateTime.getMinute());

		modelService.save(info);
		modelService.refresh(info);
		cart.setTimeSlotInfo(info);
		cart.setTimeslotStartDate(getDateFronString(info.getDate(), info.getStart(), timezone));
		cart.setTimeslotEndDate(getDateFronString(info.getDate(), info.getEnd(), timezone));
		modelService.save(cart);
	}

	private Date getDateFronString(final String date, final String time, final String timeZone)
	{
		if (Strings.isBlank(date) || Strings.isBlank(time) || Strings.isBlank(timeZone))
		{
			return null;
		}

		final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		dateFormat.setTimeZone(TimeZone.getTimeZone(ZoneId.of(timeZone)));

		try
		{
			return dateFormat.parse(date.concat(" ").concat(time));
		}
		catch (final ParseException ex)
		{
			return null;
		}
	}

	@Override
	public Optional<TimeSlotModel> getActiveByArea(final String areaCode)
	{
		Preconditions.checkArgument(!StringUtils.isEmpty(areaCode), AREA_ILLEGAL_ARGS);
		final Optional<AreaModel> optional = areaService.get(areaCode);
		if (optional.isPresent())
		{
			return Optional.ofNullable(optional.get().getTimeSlot());
		}
		return Optional.ofNullable(null);
	}

	@Override
	public Optional<PeriodModel> getPeriod(final String periodCode)
	{
		Preconditions.checkArgument(!StringUtils.isEmpty(periodCode), PERIOD_CODE_ILLEGAL_ARGS);
		return timeSlotDao.findPeriod(periodCode);
	}

	private AddressModel getCartAddress(final CartModel cart)
	{
		if (cart == null || cart.getShipmentType() == null)
		{
			return null;
		}
		if (ShipmentType.PICKUP_IN_STORE.equals(cart.getShipmentType()))
		{

			return getDefaultDeliveryPointOfServiceAddress(cart);

		}
		else
		{
			return cart.getDeliveryAddress();
		}
	}


	private AddressModel getDefaultDeliveryPointOfServiceAddress(final CartModel cart)
	{
		return cart.getSite().getDefaultDeliveryPointOfService() == null
				|| cart.getSite().getDefaultDeliveryPointOfService().getAddress() == null ? null
						: cart.getSite().getDefaultDeliveryPointOfService().getAddress();

	}
}
