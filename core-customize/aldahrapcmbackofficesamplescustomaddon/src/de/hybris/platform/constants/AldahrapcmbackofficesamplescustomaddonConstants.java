/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved
 */
package de.hybris.platform.constants;

/**
 * Global class for all Aldahrapcmbackofficesamplescustomaddon constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings("deprecation")
public final class AldahrapcmbackofficesamplescustomaddonConstants extends GeneratedAldahrapcmbackofficesamplescustomaddonConstants
{
	public static final String EXTENSIONNAME = "aldahrapcmbackofficesamplescustomaddon";

	private AldahrapcmbackofficesamplescustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
