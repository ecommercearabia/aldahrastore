/**
 *
 */
package com.aldahra.aldahrastorecreditfacades.facade.impl;

import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import com.aldahra.aldahrastorecredit.enums.StoreCreditModeType;
import com.aldahra.aldahrastorecredit.model.StoreCreditModeModel;
import com.aldahra.aldahrastorecredit.service.StoreCreditModeService;
import com.aldahra.aldahrastorecreditfacades.data.StoreCreditModeData;
import com.aldahra.aldahrastorecreditfacades.facade.StoreCreditModeFacade;


/**
 * @author mnasro
 *
 */
public class DefaultStoreCreditModeFacade implements StoreCreditModeFacade
{
	@Resource(name = "storeCreditModeService")
	private StoreCreditModeService storeCreditModeService;

	@Resource(name = "storeCreditModeConverter")
	private Converter<StoreCreditModeModel, StoreCreditModeData> storeCreditModeConverter;

	@Override
	public Optional<StoreCreditModeData> getStoreCreditMode(final String storeCreditModeTypeCode)
	{
		if (storeCreditModeTypeCode == null)
		{
			throw new IllegalArgumentException("storeCreditModeTypeCode cannot must be null");
		}
		final StoreCreditModeType storeCreditModeType = StoreCreditModeType.valueOf(storeCreditModeTypeCode);
		final StoreCreditModeModel storeCreditMode = storeCreditModeService.getStoreCreditMode(storeCreditModeType);
		if (storeCreditMode == null)
		{
			return Optional.empty();
		}

		return Optional.ofNullable(storeCreditModeConverter.convert(storeCreditMode));
	}


	@Override
	public Optional<List<StoreCreditModeData>> getSupportedStoreCreditModesCurrentBaseStore()
	{

		final List<StoreCreditModeModel> storeCreditModeList = storeCreditModeService
				.getSupportedStoreCreditModesCurrentBaseStore();
		if (storeCreditModeList == null)
		{
			return null;
		}

		final Iterator itr = storeCreditModeList.iterator();
		while (itr.hasNext())
		{
			final StoreCreditModeModel storeCreditModeModel = (StoreCreditModeModel) itr.next();
			if (storeCreditModeModel.getActive() == null || Boolean.FALSE.equals(storeCreditModeModel.getActive()))
			{
				itr.remove();
			}
		}

		return Optional.ofNullable(storeCreditModeConverter.convertAll(storeCreditModeList));
	}


	@Override
	public boolean isStoreCreditModeSupportedByCurrentBaseStore(final String StoreCreditTypeCode)
	{
		final Optional<List<StoreCreditModeData>> supportedStoreCreditModes = getSupportedStoreCreditModesCurrentBaseStore();

		if (!supportedStoreCreditModes.isPresent())
		{
			return false;
		}
		final List<String> storeCreditModeCodes = new ArrayList();
		for (final StoreCreditModeData storeCreditMode : supportedStoreCreditModes.get())
		{
			storeCreditModeCodes.add(storeCreditMode.getStoreCreditModeType().getCode());
		}
		return storeCreditModeCodes.contains(StoreCreditTypeCode);
	}


}
