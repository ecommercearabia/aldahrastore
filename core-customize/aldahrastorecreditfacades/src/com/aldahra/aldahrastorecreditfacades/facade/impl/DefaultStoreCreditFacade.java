/**
 *
 */
package com.aldahra.aldahrastorecreditfacades.facade.impl;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.store.BaseStoreModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;

import com.aldahra.aldahrastorecredit.model.StoreCreditHistoryModel;
import com.aldahra.aldahrastorecredit.service.StoreCreditService;
import com.aldahra.aldahrastorecreditfacades.StoreCreditHistoryData;
import com.aldahra.aldahrastorecreditfacades.facade.StoreCreditFacade;


/**
 * @author mnasro
 *
 */
public class DefaultStoreCreditFacade implements StoreCreditFacade
{
	@Resource(name = "storeCreditService")
	private StoreCreditService storeCreditService;

	@Resource(name = "storeCreditHistoryConverter")
	private Converter<StoreCreditHistoryModel, StoreCreditHistoryData> storeCreditModeTypeConverter;

	@Resource(name = "commerceCommonI18NService")
	private CommerceCommonI18NService commerceCommonI18NService;

	@Resource(name = "priceDataFactory")
	private PriceDataFactory priceDataFactory;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.erabia.erabiastorecredit.facades.StoreCreditFacade#getStoreCreditAmountByCurrentUser()
	 */
	@Override
	public Optional<PriceData> getStoreCreditAmountByCurrentUserAndCurrentBaseStore()
	{
		final BigDecimal storeCreditAmount = storeCreditService.getStoreCreditAmountByCurrentUserAndCurrentBaseStore();

		return Optional.ofNullable(getPriceData(storeCreditAmount == null ? new BigDecimal(0) : storeCreditAmount,
				PriceDataType.BUY, commerceCommonI18NService.getDefaultCurrency()));
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.erabia.erabiastorecredit.facades.StoreCreditFacade#getStoreCreditAmountByCurrentUserBaseStore(de.hybris.
	 * platform .store.BaseStoreModel)
	 */
	@Override
	public Optional<PriceData> getStoreCreditAmountByCurrentUser(final BaseStoreModel baseStoreModel)
	{
		final BigDecimal storeCreditAmount = storeCreditService.getStoreCreditAmountByCurrentUser(baseStoreModel);

		return Optional.ofNullable(getPriceData(storeCreditAmount == null ? new BigDecimal(0) : storeCreditAmount,
				PriceDataType.BUY, commerceCommonI18NService.getDefaultCurrency()));
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.erabia.erabiastorecredit.facades.StoreCreditFacade#getStoreCreditHistoryByCurrentUser()
	 */
	@Override
	public Optional<List<StoreCreditHistoryData>> getStoreCreditHistoryByCurrentUser()
	{
		final List<StoreCreditHistoryModel> storeCreditHistoryByCurrentUser = storeCreditService
				.getStoreCreditHistoryByCurrentUser();

		if (CollectionUtils.isEmpty(storeCreditHistoryByCurrentUser))
		{
			return Optional.empty();
		}
		final List<StoreCreditHistoryModel> storeCreditHistoryTemp = new ArrayList<>();
		java.util.Collections.reverse(storeCreditHistoryTemp);

		final List<StoreCreditHistoryData> storeCreditHistoryDatas = storeCreditModeTypeConverter
				.convertAll(storeCreditHistoryTemp);
		return Optional.ofNullable(storeCreditHistoryDatas);

	}

	private PriceData getPriceData(final BigDecimal value, final PriceDataType priceType, final CurrencyModel currency)
	{
		if (currency == null || value == null)
		{
			return null;
		}
		return priceDataFactory.create(priceType, value, currency);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.erabia.erabiastorecredit.facades.StoreCreditFacade#getStoreCreditAmount(de.hybris.platform.core.model.order
	 * .AbstractOrderModel)
	 */
	@Override
	public Optional<PriceData> getStoreCreditAmount(final AbstractOrderModel abstractOrderModel)
	{
		final BigDecimal storeCreditAmount = storeCreditService.getStoreCreditAmount(abstractOrderModel);

		return Optional.ofNullable(getPriceData(storeCreditAmount == null ? new BigDecimal(0) : storeCreditAmount,
				PriceDataType.BUY, abstractOrderModel.getCurrency()));
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.erabia.erabiastorecredit.facades.StoreCreditFacade#getStoreCreditAmountFullRedeem(de.hybris.platform.core.
	 * model.order.AbstractOrderModel)
	 */
	@Override
	public Optional<PriceData> getStoreCreditAmountFullRedeem(final AbstractOrderModel abstractOrderModel)
	{
		final double totalPrice = abstractOrderModel.getTotalPrice() == null ? 0.0
				: abstractOrderModel.getTotalPrice().doubleValue() + (abstractOrderModel.getStoreCreditAmount() == null ? 0.0
						: abstractOrderModel.getStoreCreditAmount().doubleValue());
		final double paymentCost = abstractOrderModel.getPaymentCost() == null ? 0.0
				: abstractOrderModel.getPaymentCost().doubleValue();
		final double deliveryCost = abstractOrderModel.getDeliveryCost() == null ? 0.0
				: abstractOrderModel.getDeliveryCost().doubleValue();
		final boolean cscaidc = abstractOrderModel.getStore().getCalcStoreCreditAmountIncludeDeliveryCost() == null ? false
				: abstractOrderModel.getStore().getCalcStoreCreditAmountIncludeDeliveryCost().booleanValue();
		final boolean cscaipc = abstractOrderModel.getStore().getCalcStoreCreditAmountIncludePaymentCost() == null ? false
				: abstractOrderModel.getStore().getCalcStoreCreditAmountIncludePaymentCost().booleanValue();

		double totalCost = totalPrice;
		if (cscaidc)
		{
			totalCost += deliveryCost;
		}
		if (cscaipc)
		{
			totalCost += paymentCost;
		}


		final boolean enableCheckTotalPriceWithStoreCreditLimit = abstractOrderModel.getStore() == null ? false
				: abstractOrderModel.getStore().isEnableCheckTotalPriceWithStoreCreditLimit();
		if (enableCheckTotalPriceWithStoreCreditLimit)
		{
			final double totalPriceWithStoreCreditLimit = abstractOrderModel.getStore() == null ? 0.0
					: abstractOrderModel.getStore().getTotalPriceWithStoreCreditLimit();
			totalCost -= totalPriceWithStoreCreditLimit;
		}



		final BigDecimal storeCreditAmountOS = storeCreditService.getStoreCreditAmount(abstractOrderModel);
		final double storeCreditAmount = calculateStoreCreditAmount(storeCreditAmountOS.doubleValue(), totalCost);
		final PriceData storeCreditAmountFullRedeem = getPriceData(new BigDecimal(storeCreditAmount), PriceDataType.BUY,
				abstractOrderModel.getCurrency());
		return Optional.ofNullable(storeCreditAmountFullRedeem);
	}

	public double calculateStoreCreditAmount(final double storeCreditAmount, final double totalOrderAmount)
	{
		return storeCreditAmount >= totalOrderAmount ? totalOrderAmount : storeCreditAmount;
	}

}
