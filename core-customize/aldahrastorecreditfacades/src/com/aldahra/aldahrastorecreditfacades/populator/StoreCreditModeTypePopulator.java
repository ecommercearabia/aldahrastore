/**
 *
 */
package com.aldahra.aldahrastorecreditfacades.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.type.TypeService;

import javax.annotation.Resource;

import com.aldahra.aldahrastorecredit.enums.StoreCreditModeType;
import com.aldahra.aldahrastorecreditfacades.data.StoreCreditModeTypeData;


/**
 * @author mnasro
 *
 */
public class StoreCreditModeTypePopulator implements Populator<StoreCreditModeType, StoreCreditModeTypeData>
{
	@Resource(name = "typeService")
	private TypeService typeService;

	@Override
	public void populate(final StoreCreditModeType source, final StoreCreditModeTypeData target)
	{
		target.setCode(source.getCode());
		target.setName(typeService.getEnumerationValue(source).getName());
	}
}
