/**
 *
 */
package com.aldahra.aldahrastorecreditfacades.populator;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.math.BigDecimal;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.aldahra.aldahrastorecredit.model.StoreCreditHistoryModel;
import com.aldahra.aldahrastorecreditfacades.StoreCreditHistoryData;


/**
 * @author ralmanasreh
 *
 */
public class StoreCreditHistoryPopulator implements Populator<StoreCreditHistoryModel, StoreCreditHistoryData>
{

	@Resource(name = "commerceCommonI18NService")
	private CommerceCommonI18NService commerceCommonI18NService;

	@Resource(name = "priceDataFactory")
	private PriceDataFactory priceDataFactory;

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void populate(final StoreCreditHistoryModel source, final StoreCreditHistoryData target) throws ConversionException
	{
		Assert.notNull(source, "Parameter StoreCreditHistoryModel source cannot be null.");
		Assert.notNull(target, "Parameter StoreCreditHistoryData target cannot be null.");
		final PriceData amount = getPriceData(source.getAmount(), PriceDataType.BUY,
				commerceCommonI18NService.getDefaultCurrency());
		final PriceData balance = getPriceData(source.getBalance(), PriceDataType.BUY,
				commerceCommonI18NService.getDefaultCurrency());
		target.setAmount(amount);
		target.setBalance(balance);
		target.setDateOfPurchase(source.getDateOfPurchase());
		target.setOrderCode(source.getOrderCode());
	}

	private PriceData getPriceData(final BigDecimal value, final PriceDataType priceType, final CurrencyModel currency)
	{
		if (currency == null)
		{
			return null;
		}
		return priceDataFactory.create(priceType, value, currency);
	}
}
