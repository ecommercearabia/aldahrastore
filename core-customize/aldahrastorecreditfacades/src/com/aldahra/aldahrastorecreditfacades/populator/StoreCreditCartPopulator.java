/**
 *
 */
package com.aldahra.aldahrastorecreditfacades.populator;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.math.BigDecimal;

import javax.annotation.Resource;

import com.aldahra.aldahrastorecredit.model.StoreCreditModeModel;
import com.aldahra.aldahrastorecreditfacades.data.StoreCreditModeData;


/**
 * @author mnasro
 *
 */
public class StoreCreditCartPopulator implements Populator<AbstractOrderModel, AbstractOrderData>
{
	@Resource(name = "storeCreditModeConverter")
	private Converter<StoreCreditModeModel, StoreCreditModeData> storeCreditModeConverter;


	@Resource(name = "priceDataFactory")
	private PriceDataFactory priceDataFactory;

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void populate(final AbstractOrderModel source, final AbstractOrderData target) throws ConversionException
	{
		if (source != null)
		{
			if (source.getStoreCreditMode() != null)
			{
				target.setStoreCreditMode(storeCreditModeConverter.convert(source.getStoreCreditMode()));
			}
			if (source.getStoreCreditAmount() != null)
			{
				target.setStoreCreditAmount(getPriceData(new BigDecimal(source.getStoreCreditAmount().doubleValue()),
						PriceDataType.BUY, source.getCurrency()));
			}
			target.setStoreCreditAmountSelected(source.getStoreCreditAmountSelected());

		}

	}

	private PriceData getPriceData(final BigDecimal value, final PriceDataType priceType, final CurrencyModel currency)
	{
		if (currency == null)
		{
			return null;
		}
		return priceDataFactory.create(priceType, value, currency);
	}
}
