/**
 *
 */
package com.aldahra.aldahraauthoritycustomwebservices.controllers;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.aldahra.aldahraauthoritycustomwebservices.AuthorityGroupWsDTO;
import com.aldahra.aldahraauthoritycustomwebservices.AuthorityViewWsDTO;
import com.aldahra.aldahrafacades.authority.AuthorityGroupData;
import com.aldahra.facades.facade.WarehousingAndOMSAuthorityFacade;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * @author monzer
 *
 */
@Controller
@RequestMapping(value = "/authorities")
@Api(tags = "Authorities")
public class AuthorityGroupController extends AuthorityBaseController
{

	@Resource(name = "warehousingAndOMSAuthorityFacade")
	private WarehousingAndOMSAuthorityFacade warehousingAndOMSAuthorityFacade;

	@Secured({ WAREHOUSE_AGENT_GROUP, WAREHOUSE_MANAGER_GROUP, WAREHOUSE_ADMINISTRATOR_GROUP ,CUSTOMER_SUPPORT_AGENT_GROUP, CUSTOMER_SUPPORT_MANAGER_GROUP, CUSTOMER_SUPPORT_ADMINISTRATOR_GROUP })
	@RequestMapping(value = "user/{userUid}/authority-group", method = RequestMethod.GET, produces =
	{ MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	@ApiOperation(value = "Gets the authority group for a user", response = AuthorityGroupWsDTO.class)
	public AuthorityGroupWsDTO getAuthoritiesForUser(@ApiParam(value = "The User Uid", required = true)
	@PathVariable
	final String userUid)
	{
		final AuthorityGroupData groupData = warehousingAndOMSAuthorityFacade.getAllAuthorityGroupsForUser(userUid);
		return dataMapper.map(groupData, AuthorityGroupWsDTO.class);
	}

	@Secured(
	{ WAREHOUSE_AGENT_GROUP, WAREHOUSE_MANAGER_GROUP, WAREHOUSE_ADMINISTRATOR_GROUP, CUSTOMER_SUPPORT_AGENT_GROUP,
			CUSTOMER_SUPPORT_MANAGER_GROUP, CUSTOMER_SUPPORT_ADMINISTRATOR_GROUP })
	@RequestMapping(value = "user/{userUid}/views", method = RequestMethod.GET, produces =
	{ MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	@ApiOperation(value = "Gets the authority group for a user", response = AuthorityViewWsDTO.class)
	public AuthorityViewWsDTO getViewsForUser(@ApiParam(value = "The User Uid", required = true)
	@PathVariable
	final String userUid)
	{
		final List<String> groupData = warehousingAndOMSAuthorityFacade.getAllViewsAvailabelForUser(userUid);
		final AuthorityViewWsDTO views = new AuthorityViewWsDTO();
		views.setAuthorities(groupData);
		return views;
	}

}
