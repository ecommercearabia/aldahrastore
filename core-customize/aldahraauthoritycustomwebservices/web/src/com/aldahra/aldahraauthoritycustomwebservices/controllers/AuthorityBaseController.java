/**
 *
 */
package com.aldahra.aldahraauthoritycustomwebservices.controllers;

import de.hybris.platform.webservicescommons.mapping.DataMapper;

import javax.annotation.Resource;


/**
 * @author monzer
 *
 */
public abstract class AuthorityBaseController
{

	public static final String WAREHOUSE_AGENT_GROUP = "ROLE_WAREHOUSEAGENTGROUP";
	public static final String WAREHOUSE_MANAGER_GROUP = "ROLE_WAREHOUSEMANAGERGROUP";
	public static final String WAREHOUSE_ADMINISTRATOR_GROUP = "ROLE_WAREHOUSEADMINISTRATORGROUP";
	public static final String CUSTOMER_SUPPORT_AGENT_GROUP = "ROLE_CUSTOMERSUPPORTAGENTGROUP";
	public static final String CUSTOMER_SUPPORT_MANAGER_GROUP = "ROLE_CUSTOMERSUPPORTMANAGERGROUP";
	public static final String CUSTOMER_SUPPORT_ADMINISTRATOR_GROUP = "ROLE_CUSTOMERSUPPORTADMINISTRATORGROUP";
	public static final String ADMIN_GROUP = "ROLE_ADMINGROUP";

	@Resource(name = "dataMapper")
	protected DataMapper dataMapper;

}
