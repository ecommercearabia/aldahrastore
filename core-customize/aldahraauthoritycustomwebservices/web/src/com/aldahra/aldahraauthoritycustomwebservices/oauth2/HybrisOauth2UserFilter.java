package com.aldahra.aldahraauthoritycustomwebservices.oauth2;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;


/**
 * Filter sets current user by userService depending on current principal. <br>
 * This should happen only when there is a customer context. Anonymous credentials are also applicable, because special
 * 'anonymous' user is available for that purpose. Customer context is not available during client credential flow.
 *
 *
 *
 */
public class HybrisOauth2UserFilter implements Filter
{
	private static final String ROLE_ANONYMOUS = "ROLE_ANONYMOUS";
	private static final String ROLE_CUSTOMERMANAGERGROUP = "ROLE_CUSTOMERMANAGERGROUP";
	private static final String ROLE_WAREHOUSE_AGENT_GROUP = "ROLE_WAREHOUSEAGENTGROUP";
	private static final String ROLE_WAREHOUSE_MANAGER_GROUP = "ROLE_WAREHOUSEMANAGERGROUP";
	private static final String ROLE_WAREHOUSE_ADMINISTRATOR_GROUP = "ROLE_WAREHOUSEADMINISTRATORGROUP";
	private static final String ROLE_CUSTOMER_SUPPORT_AGENT_GROUP = "ROLE_CUSTOMERSUPPORTAGENTGROUP";
	private static final String ROLE_CUSTOMER_SUPPORT_MANAGER_GROUP = "ROLE_CUSTOMERSUPPORTMANAGERGROUP";
	private static final String ROLE_CUSTOMER_SUPPORT_ADMINISTRATOR_GROUP = "ROLE_CUSTOMERSUPPORTADMINISTRATORGROUP";
	private static final String ROLE_ADMIN_GROUP = "ROLE_ADMINGROUP";

	@Resource(name = "userService")
	private UserService userService;


	@Override
	public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
			throws IOException, ServletException
	{
		final Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		if (containsRole(auth, ROLE_ANONYMOUS) || containsRole(auth, ROLE_ADMIN_GROUP)
				|| containsRole(auth, ROLE_CUSTOMERMANAGERGROUP) || containsRole(auth, ROLE_WAREHOUSE_AGENT_GROUP)
				|| containsRole(auth, ROLE_WAREHOUSE_MANAGER_GROUP) || containsRole(auth, ROLE_WAREHOUSE_ADMINISTRATOR_GROUP)
				|| containsRole(auth, ROLE_CUSTOMER_SUPPORT_AGENT_GROUP) || containsRole(auth, ROLE_CUSTOMER_SUPPORT_MANAGER_GROUP)
				|| containsRole(auth, ROLE_CUSTOMER_SUPPORT_ADMINISTRATOR_GROUP)

		)
		{
			final UserModel userModel = userService.getUserForUID((String) auth.getPrincipal());
			userService.setCurrentUser(userModel);
		}
		chain.doFilter(request, response);
	}

	@Override
	public void destroy()
	{
		// YTODO Auto-generated method stub
	}

	@Override
	public void init(final FilterConfig arg0) throws ServletException
	{
		// YTODO Auto-generated method stub
	}

	protected static boolean containsRole(final Authentication auth, final String role)
	{
		for (final GrantedAuthority ga : auth.getAuthorities())
		{
			if (ga.getAuthority().equals(role))
			{
				return true;
			}
		}
		return false;
	}
}
