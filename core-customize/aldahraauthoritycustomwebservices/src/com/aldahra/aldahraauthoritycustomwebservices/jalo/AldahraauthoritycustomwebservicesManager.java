/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraauthoritycustomwebservices.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;
import com.aldahra.aldahraauthoritycustomwebservices.constants.AldahraauthoritycustomwebservicesConstants;

public class AldahraauthoritycustomwebservicesManager extends GeneratedAldahraauthoritycustomwebservicesManager
{
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger( AldahraauthoritycustomwebservicesManager.class.getName() );
	
	public static final AldahraauthoritycustomwebservicesManager getInstance()
	{
		final ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (AldahraauthoritycustomwebservicesManager) em.getExtension(AldahraauthoritycustomwebservicesConstants.EXTENSIONNAME);
	}
	
}
