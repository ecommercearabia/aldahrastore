/**
 *
 */
package com.aldahra.facades.facade;

import de.hybris.platform.commercefacades.voucher.VoucherFacade;
import de.hybris.platform.commercefacades.voucher.exceptions.VoucherOperationException;
import de.hybris.platform.core.model.order.CartModel;


/**
 * @author mohammad.jaradat@erabia.com
 *
 */
public interface CustomVoucherFacade extends VoucherFacade
{
	public void removeAllCouponsFromCurrentCart() throws VoucherOperationException;

	public void removeAllCoupons(CartModel cart) throws VoucherOperationException;

}
