/**
 *
 */
package com.aldahra.facades.facade;

import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.CartModel;

import java.util.List;

import com.aldahra.core.cart.data.CartTimeSlotData;
import com.aldahra.core.enums.ShipmentType;
import com.aldahra.facades.order.data.ShipmentTypeData;


/**
 * @author mnasro
 *
 */
public interface CustomCartFacade extends CartFacade
{
	public CartModificationData addToCart(final String code, final long quantity, final ShipmentType shipmentType)
			throws CommerceCartModificationException;

	public CartModificationData updateShipmentTypeForCartEntry(final long entryNumber, final ShipmentType shipmentType)
			throws CommerceCartModificationException;

	public List<CartModificationData> updateShipmentTypeForAllCartEntry(final ShipmentType shipmentType)
			throws CommerceCartModificationException;

	public List<ShipmentTypeData> getSupportedShipmentTypes();

	public ShipmentTypeData getCurrentShipmentType();

	public CartTimeSlotData getCartTimeSlotDataForCurrentCart();

	public CartTimeSlotData getCartTimeSlotData(CartModel cartModel);



}
