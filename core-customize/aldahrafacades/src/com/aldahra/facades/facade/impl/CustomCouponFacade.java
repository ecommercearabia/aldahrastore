/**
 *
 */
package com.aldahra.facades.facade.impl;

import de.hybris.platform.commercefacades.voucher.exceptions.VoucherOperationException;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.couponfacades.facades.impl.DefaultCouponFacade;
import de.hybris.platform.voucher.VoucherService;

import java.util.Collection;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.aldahra.core.enums.ShipmentType;
import com.aldahra.facades.facade.CustomVoucherFacade;



/**
 * @author Tuqa
 *
 */
public class CustomCouponFacade extends DefaultCouponFacade implements CustomVoucherFacade
{
	private static final Logger LOGGER = Logger.getLogger(CustomCouponFacade.class);

	@Resource(name = "voucherService")
	private VoucherService voucherService;


	/**
	 * @return the voucherService
	 */
	protected VoucherService getVoucherService()
	{
		return voucherService;
	}

	@Override
	public void applyVoucher(final String voucherCode) throws VoucherOperationException
	{
		if (!getCartService().hasSessionCart() || getCartService().getSessionCart() == null
				|| !ShipmentType.PICKUP_IN_STORE.equals(getCartService().getSessionCart().getShipmentType())
				|| getCartService().getSessionCart().getSite() == null
				|| !getCartService().getSessionCart().getSite().isDisableCouponForPickUpInStore())
		{
			super.applyVoucher(voucherCode);
			return;
		}
		else
		{
			LOGGER.info("coupon removed from cart ");
			throw new VoucherOperationException("Cant apply voucher in pickup in store shipment type.");
		}

	}

	@Override
	public void removeAllCouponsFromCurrentCart() throws VoucherOperationException
	{
		if (getCartService().hasSessionCart())
		{
			LOGGER.error("Can NOT apply coupon when site or cart are null");
			throw new VoucherOperationException("Can NOT apply coupon when site or cart are null");
		}
		removeAllCoupons(getCartService().getSessionCart());
	}

	@Override
	public void removeAllCoupons(final CartModel cartModel) throws VoucherOperationException
	{
		if (cartModel == null )
		{
			LOGGER.error("Can NOT apply coupon when site or cart are null");
			throw new VoucherOperationException("Can NOT apply coupon when site or cart are null");
		}
		final Collection<String> vouchers = getVoucherService().getAppliedVoucherCodes(cartModel);

		final StringBuilder voucherOperationExceptionMsgs = new StringBuilder();
		for (final String voucher : vouchers)
		{
			try
			{
				this.releaseVoucher(voucher);
			}
			catch (final VoucherOperationException e)
			{
				voucherOperationExceptionMsgs.append(e.getMessage());
				voucherOperationExceptionMsgs.append(" ,");
			}
		}
		if (StringUtils.isNotBlank(voucherOperationExceptionMsgs.toString()))
		{
			throw new VoucherOperationException(voucherOperationExceptionMsgs.toString());
		}

	}

}
