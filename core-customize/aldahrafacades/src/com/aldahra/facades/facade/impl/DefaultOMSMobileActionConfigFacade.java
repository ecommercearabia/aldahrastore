/**
 *
 */
package com.aldahra.facades.facade.impl;

import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.util.localization.Localization;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.aldahra.Ordermanagement.service.OMSMobileActionConfigService;
import com.aldahra.aldahrafacades.dto.config.ActionConfigData;
import com.aldahra.aldahrafacades.dto.config.OMSActionConfigData;
import com.aldahra.facades.facade.OMSMobileActionConfigFacade;

/**
 * @author monzer
 *
 */
public class DefaultOMSMobileActionConfigFacade implements OMSMobileActionConfigFacade
{
	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "customerAccountService")
	private CustomerAccountService customerAccountService;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "omsMobileActionConfigService")
	private OMSMobileActionConfigService omsMobileActionConfigService;

	@Resource(name = "orderGenericDao")
	private GenericDao<OrderModel> orderGenericDao;

	private static final String ORDER_NOT_FOUND_FOR_USER_AND_BASE_STORE = "Order with guid %s not found for current user in current BaseStore";

	@Override
	public OMSActionConfigData getOMSActionConfigForOrder(final String orderCode)
	{
		final OrderModel orderModel = getOrderModelForCode(orderCode);
		final boolean enabledCancelOrderAction = omsMobileActionConfigService.canPerformOMSMobileCancelOrderActionForOrder(orderModel);
		final boolean enabledCreateReturnRequestAction = omsMobileActionConfigService
				.canPerformOMSMobileCreateReturnRequestActionForOrder(orderModel);
		final boolean enabledCustomManualPaymentCaptureAction = omsMobileActionConfigService
				.canPerformOMSMobileCustomManualPaymentCaptureActionForOrder(orderModel);
		final boolean enabledCustomManualPaymentVoidAction = omsMobileActionConfigService
				.canPerformOMSMobileCustomManualPaymentVoidActionForOrder(orderModel);
		final boolean enabledManualDeliveryCostCommitAction = omsMobileActionConfigService
				.canPerformOMSMobileManualDeliveryCostCommitActionForOrder(orderModel);
		final boolean enabledManualPaymentReauthactionAction = omsMobileActionConfigService
				.canPerformOMSMobileManualPaymentReauthactionActionForOrder(orderModel);
		final boolean enabledManualTaxCommitAction = omsMobileActionConfigService.canPerformOMSMobileManualTaxCommitActionForOrder(orderModel);
		final boolean enabledManualTaxRequoteAction = omsMobileActionConfigService
				.canPerformOMSMobileManualTaxRequoteActionForOrder(orderModel);
		final boolean enabledManualTaxVoidAction = omsMobileActionConfigService.canPerformOMSMobileManualTaxVoidActionForOrder(orderModel);

		return buildOMSActionConfigData(enabledCancelOrderAction, enabledCreateReturnRequestAction,
				enabledCustomManualPaymentCaptureAction, enabledCustomManualPaymentVoidAction, enabledManualDeliveryCostCommitAction,
				enabledManualPaymentReauthactionAction, enabledManualTaxCommitAction, enabledManualTaxRequoteAction,
				enabledManualTaxVoidAction);
	}

	private OMSActionConfigData buildOMSActionConfigData(final boolean enabledCancelOrderAction,
			final boolean enabledCreateReturnRequestAction, final boolean enabledCustomManualPaymentCaptureAction,
			final boolean enabledCustomManualPaymentVoidAction, final boolean enabledManualDeliveryCostCommitAction,
			final boolean enabledManualPaymentReauthactionAction, final boolean enabledManualTaxCommitAction,
			final boolean enabledManualTaxRequoteAction, final boolean enabledManualTaxVoidAction)
	{
		final OMSActionConfigData config = new OMSActionConfigData();
		config.setCancelOrderActionConfig(buildActionConfigData(enabledCancelOrderAction));
		config.setCreateReturnreQuestActionConfig(buildActionConfigData(enabledCreateReturnRequestAction));
		config.setCustomManualPaymentCaptureActionConfig(buildActionConfigData(enabledCustomManualPaymentCaptureAction));
		config.setCustommanualPaymentVoidActionConfig(buildActionConfigData(enabledCustomManualPaymentVoidAction));
		config.setManualDeliveryCostCommitactionActionConfig(buildActionConfigData(enabledManualDeliveryCostCommitAction));
		config.setManualPaymentReauthactionActionConfig(buildActionConfigData(enabledManualPaymentReauthactionAction));
		config.setManualTaxCommitActionConfig(buildActionConfigData(enabledManualTaxCommitAction));
		config.setManualTaxRequoteActionConfig(buildActionConfigData(enabledManualTaxRequoteAction));
		config.setManualTaxVoidActionConfig(buildActionConfigData(enabledManualTaxVoidAction));
		return config;

	}

	private ActionConfigData buildActionConfigData(final boolean enabled)
	{
		final ActionConfigData actionConfig = new ActionConfigData();
		actionConfig.setEnabled(enabled);
		actionConfig.setClickable(enabled); //For now
		return actionConfig;
	}

	protected OrderModel getOrderModelForCode(final String orderCode)
	{
		final Map<String, String> params = new HashMap<>();
		params.put(OrderModel.CODE, orderCode);

		final List<OrderModel> resultSet = discardOrderSnapshot(getOrderGenericDao().find(params));

		if (resultSet.isEmpty())
		{
			throw new ModelNotFoundException(
					String.format(Localization.getLocalizedString("ordermanagementfacade.orders.validation.missing.code"), orderCode));
		}
		if (resultSet.size() > 1)
		{
			throw new AmbiguousIdentifierException(String
					.format(Localization.getLocalizedString("ordermanagementfacade.orders.validation.multiple.code"), orderCode));
		}
		return resultSet.get(0);
	}

	protected List<OrderModel> discardOrderSnapshot(final List<OrderModel> orders)
	{
		if (CollectionUtils.isEmpty(orders))
		{
			return new ArrayList<>();
		}
		return orders.stream().filter(order -> Objects.isNull(order.getVersionID())).collect(Collectors.toList());
	}

	protected UserService getUserService()
	{
		return userService;
	}

	protected CustomerAccountService getCustomerAccountService()
	{
		return customerAccountService;
	}

	/**
	 * @return the baseStoreService
	 */
	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	/**
	 * @return the omsMobileActionConfigService
	 */
	public OMSMobileActionConfigService getOmsMobileActionConfigService()
	{
		return omsMobileActionConfigService;
	}

	/**
	 * @return the orderGenericDao
	 */
	public GenericDao<OrderModel> getOrderGenericDao()
	{
		return orderGenericDao;
	}

	/**
	 * @return the orderNotFoundForUserAndBaseStore
	 */
	public static String getOrderNotFoundForUserAndBaseStore()
	{
		return ORDER_NOT_FOUND_FOR_USER_AND_BASE_STORE;
	}


}
