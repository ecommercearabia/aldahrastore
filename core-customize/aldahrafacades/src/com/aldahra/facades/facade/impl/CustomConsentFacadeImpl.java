/**
 *
 */
package com.aldahra.facades.facade.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.consent.impl.DefaultConsentFacade;
import de.hybris.platform.commerceservices.model.consent.ConsentTemplateModel;
import de.hybris.platform.core.model.user.CustomerModel;

import com.aldahra.facades.facade.CustomConsentFacade;


/**
 * @author mnasro
 *
 */
public class CustomConsentFacadeImpl extends DefaultConsentFacade implements CustomConsentFacade
{

	@Override
	public void giveConsent(final String consentTemplateId, final Integer consentTemplateVersion,
			final CustomerModel customerModel)
	{
		validateParameterNotNull(consentTemplateId, "Parameter consentTemplateId must not be null");
		validateParameterNotNull(consentTemplateVersion, "Parameter consentTemplateVersion must not be null");
		validateParameterNotNull(customerModel, "Parameter customerModel must not be null");

		final BaseSiteModel baseSite = getBaseSiteService().getCurrentBaseSite();

		final ConsentTemplateModel consentTemplate = getCommerceConsentService().getConsentTemplate(consentTemplateId,
				consentTemplateVersion, baseSite);
		getCommerceConsentService().giveConsent(customerModel, consentTemplate);
	}

}
