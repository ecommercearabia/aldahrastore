/**
 *
 */
package com.aldahra.facades.facade.impl;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.warehousingfacades.order.impl.DefaultWarehousingConsignmentFacade;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.aldahra.aldahrafulfillment.context.FulfillmentContext;
import com.aldahra.aldahrafulfillment.exception.FulfillentException;
import com.aldahra.facades.facade.CustomWarehousingConsignmentFacade;
import com.google.common.base.Preconditions;

/**
 * @author monzer
 *
 */
public class DefaultCustomWarehousingConsignmentFacade extends DefaultWarehousingConsignmentFacade
		implements CustomWarehousingConsignmentFacade
{

	@Resource(name = "fulfillmentContext")
	private FulfillmentContext fulfillmentContext;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Override
	public Optional<String> createShipment(final String consignmentCode) throws FulfillentException
	{
		final ConsignmentModel consignmentModel = getAndValidateConsignmentForCode(consignmentCode);
		final Optional<String> trackingId = fulfillmentContext.createShipment(consignmentModel);
		return trackingId;
	}

	@Override
	public Optional<byte[]> printConsignmentAWB(final String consignmentCode) throws FulfillentException
	{
		final ConsignmentModel consignmentModel = getAndValidateConsignmentForCode(consignmentCode);
		final Optional<byte[]> awb = fulfillmentContext.printAWB(consignmentModel);
		return awb;
	}

	@Override
	public void updateConsignmentTrackingId(final String consignmentCode, final String trackingId)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(trackingId), "Tracking id cannot be null");
		final ConsignmentModel consignmentModel = getAndValidateConsignmentForCode(consignmentCode);
		consignmentModel.setTrackingID(trackingId);
		modelService.save(consignmentModel);
	}

	private ConsignmentModel getAndValidateConsignmentForCode(final String consignmentCode) {
		Preconditions.checkArgument(StringUtils.isNotBlank(consignmentCode), "Cannot proceed with an empty consignment code");
		final ConsignmentModel consignmentModel = getConsignmentModelForCode(consignmentCode);
		Preconditions.checkArgument(consignmentModel != null, "Cannot proceed without consignment model");
		return consignmentModel;
	}

}
