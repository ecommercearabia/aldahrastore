/**
 *
 */
package com.aldahra.facades.facade.impl;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.store.BaseStoreModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;

import com.aldahra.core.loyaltypoint.data.LoyaltyPointHistoryData;
import com.aldahra.core.model.LoyaltyPointHistoryModel;
import com.aldahra.core.service.LoyaltyPointService;
import com.aldahra.facades.facade.LoyaltyPointFacade;
import com.google.common.base.Preconditions;


/**
 * @author mnasro
 *
 */
public class DefaultLoyaltyPointFacade implements LoyaltyPointFacade
{
	private static final String BASESTORE_MUSTN_T_BE_NULL = "baseStoreModel mustn't be null or empty";
	private static final String CUSTOMER_MUSTN_T_BE_NULL = "customerModel mustn't be null or empty";

	@Resource(name = "loyaltyPointService")
	private LoyaltyPointService loyaltyPointService;


	/**
	 * @return the loyaltyPointService
	 */
	protected LoyaltyPointService getLoyaltyPointService()
	{
		return loyaltyPointService;
	}


	@Resource(name = "loyaltyPointHistoryConverter")
	private Converter<LoyaltyPointHistoryModel, LoyaltyPointHistoryData> loyaltyPointHistoryConverter;


	@Override
	public List<LoyaltyPointHistoryData> getLoyaltyPointHistory(final CustomerModel customerModel)
	{
		return getLoyaltyPointHistoryData(getLoyaltyPointService().getLoyaltyPointHistory(customerModel));
	}

	@Override
	public List<LoyaltyPointHistoryData> getLoyaltyPointHistoryByCurrentCustomer()
	{
		return getLoyaltyPointHistoryData(getLoyaltyPointService().getLoyaltyPointHistoryByCurrentCustomer());
	}

	@Override
	public double getLoyaltyPoints(final CustomerModel customerModel)
	{
		Preconditions.checkArgument(customerModel != null, CUSTOMER_MUSTN_T_BE_NULL);
		return customerModel.getLoyaltyPoints();
	}

	@Override
	public double getLoyaltyPointsByCurrentCustomer()
	{

		return getLoyaltyPointService().getLoyaltyPointsByCurrentCustomer();
	}

	@Override
	public boolean isLoyaltyPointsEnabled(final BaseStoreModel baseStoreModel)
	{
		return getLoyaltyPointService().isLoyaltyPointsEnabled(baseStoreModel);
	}

	@Override
	public boolean isLoyaltyPointsEnabledByCurrentStore()
	{
		return getLoyaltyPointService().isLoyaltyPointsEnabledByCurrentStore();
	}

	/**
	 *
	 */
	private List<LoyaltyPointHistoryData> getLoyaltyPointHistoryData(final List<LoyaltyPointHistoryModel> loyaltyPointHistory)
	{
		if (CollectionUtils.isEmpty(loyaltyPointHistory))
		{
			return Collections.emptyList();
		}
		final List<LoyaltyPointHistoryModel> loyaltyPointHistoryTemp = new ArrayList<>(loyaltyPointHistory);

		java.util.Collections.reverse(loyaltyPointHistoryTemp);

		return getLoyaltyPointHistoryConverter().convertAll(loyaltyPointHistoryTemp);
	}

	/**
	 * @return the loyaltyPointHistoryConverter
	 */
	protected Converter<LoyaltyPointHistoryModel, LoyaltyPointHistoryData> getLoyaltyPointHistoryConverter()
	{
		return loyaltyPointHistoryConverter;
	}
}
