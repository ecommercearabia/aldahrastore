package com.aldahra.facades.facade.impl;

import de.hybris.platform.commercefacades.user.data.DeliveryModeTypeData;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import com.aldahra.core.enums.DeliveryModeType;
import com.aldahra.facades.facade.DeliveryModeTypeFacade;


/**
 * @author mnasro
 *
 */
public class DefaultDeliveryModeTypeFacade implements DeliveryModeTypeFacade
{

	@Resource(name = "deliveryModeTypeConverter")
	private Converter<DeliveryModeType, DeliveryModeTypeData> deliveryModeTypeConverter;

	@Override
	public Optional<DeliveryModeTypeData> getDeliveryModeType(final String deliveryModeTypeCode)
	{
		if (deliveryModeTypeCode == null)
		{
			throw new IllegalArgumentException("deliveryModeTypeCode cannot must be null");
		}

		DeliveryModeType deliveryModeType = null;
		try
		{
			deliveryModeType = DeliveryModeType.valueOf(deliveryModeTypeCode);
		}
		catch (final Exception e)
		{
			return Optional.empty();
		}


		return Optional.ofNullable(deliveryModeTypeConverter.convert(deliveryModeType));
	}


	@Override
	public List<DeliveryModeTypeData> getSupportedDeliveryModeTypes()
	{

		final List<DeliveryModeType> deliveryModeTypes = Arrays.asList(DeliveryModeType.EXPRESS, DeliveryModeType.NORMAL);

		return deliveryModeTypeConverter.convertAll(deliveryModeTypes);
	}


}