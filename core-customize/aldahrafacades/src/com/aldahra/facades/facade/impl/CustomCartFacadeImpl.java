/**
 *
 */
package com.aldahra.facades.facade.impl;

import de.hybris.platform.commercefacades.order.data.AddToCartParams;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.order.impl.DefaultCartFacade;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.AddressService;

import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;

import com.aldahra.aldahrauser.model.AreaModel;
import com.aldahra.aldahrauser.model.CityModel;
import com.aldahra.core.cart.data.CartTimeSlotData;
import com.aldahra.core.enums.ShipmentType;
import com.aldahra.core.model.TimeRangeModel;
import com.aldahra.facades.facade.CustomCartFacade;
import com.aldahra.facades.order.data.ShipmentTypeData;
import com.google.common.base.Preconditions;


/**
 * @author mnasro
 *
 */
public class CustomCartFacadeImpl extends DefaultCartFacade implements CustomCartFacade
{

	@Resource(name = "shipmentTypeConverter")
	private Converter<ShipmentType, ShipmentTypeData> shipmentTypeConverter;
	@Resource(name = "addressService")
	private AddressService addressService;




	private static final String CITY_NOT_EXPRESS = "The Selected City is not Available for Express delivery";


	private static final Logger LOG = Logger.getLogger(CustomCartFacadeImpl.class);

	/**
	 * @return the shipmentTypeConverter
	 */
	protected Converter<ShipmentType, ShipmentTypeData> getShipmentTypeConverter()
	{
		return shipmentTypeConverter;
	}



	@Override
	public CartModificationData addToCart(final String code, final long quantity, final ShipmentType shipmentType)
			throws CommerceCartModificationException
	{
		final AddToCartParams params = new AddToCartParams();
		params.setProductCode(code);
		params.setQuantity(quantity);
		params.setShipmentType(shipmentType == null ? null : shipmentType.getCode());

		return this.addToCart(params);

	}

	@Override
	public CartModificationData updateShipmentTypeForCartEntry(final long entryNumber, final ShipmentType shipmentType)
			throws CommerceCartModificationException
	{



		final AddToCartParams dto = new AddToCartParams();
		dto.setShipmentType(shipmentType != null ? shipmentType.getCode() : null);
		dto.setStoreId(getStoreId(shipmentType));
		final CommerceCartParameter parameter = getCommerceCartParameterConverter().convert(dto);
		parameter.setEnableHooks(true);
		parameter.setEntryNumber(entryNumber);
		CommerceCartModification commerceCartModification = null;
		if (parameter.getPointOfService() == null)
		{
			commerceCartModification = getCommerceCartService().updateToShippingModeForCartEntry(parameter);
		}
		else
		{
			commerceCartModification = getCommerceCartService().updatePointOfServiceForCartEntry(parameter);
		}
		return getCartModificationConverter().convert(commerceCartModification);
	}

	@Override
	public List<CartModificationData> updateShipmentTypeForAllCartEntry(final ShipmentType shipmentType)
			throws CommerceCartModificationException
	{
		final CartData sessionCart = getSessionCart();

		if (sessionCart == null || CollectionUtils.isEmpty(sessionCart.getEntries()))
		{
			return Collections.emptyList();
		}

		final List<CartModificationData> cartModificationDataList = new ArrayList<>();

		for (final OrderEntryData orderEntryData : sessionCart.getEntries())
		{
			cartModificationDataList.add(updateShipmentTypeForCartEntry(orderEntryData.getEntryNumber(), shipmentType));
		}


		resetDataByShipmentType(shipmentType);

		return cartModificationDataList;
	}

	/**
	 * @param shipmentType
	 */
	private void resetDataByShipmentType(final ShipmentType shipmentType)
	{
		if (shipmentType == null)
		{
			return;
		}
		final CartModel cartModel = getSessionCartModel();
		if (cartModel == null)
		{
			return;
		}
		final AddressModel defaultDeliveryPOSAddressModel = getDefaultDeliveryPointOfServiceAddressModel(cartModel);

		if (ShipmentType.PICKUP_IN_STORE.equals(cartModel.getShipmentType()) && defaultDeliveryPOSAddressModel != null)
		{
			final AddressModel addressModel = addressService.cloneAddressForOwner(defaultDeliveryPOSAddressModel,
					cartModel.getUser());

			addressModel.setBillingAddress(Boolean.valueOf(true));
			addressModel.setShippingAddress(Boolean.valueOf(true));
			addressModel.setVisibleInAddressBook(Boolean.valueOf(false));
			addressModel.setOwner(cartModel.getUser());

			getModelService().save(addressModel);
			getModelService().refresh(addressModel);
			cartModel.setDeliveryAddress(addressModel);
			cartModel.setPaymentAddress(addressModel);
			cartModel.setTimeslotEndDate(null);
			cartModel.setTimeslotStartDate(null);
			cartModel.setTimeSlotInfo(null);
			getModelService().save(cartModel);
			return;

		}
		cartModel.setDeliveryAddress(null);
		cartModel.setPaymentAddress(null);
		cartModel.setTimeslotEndDate(null);
		cartModel.setTimeslotStartDate(null);
		cartModel.setTimeSlotInfo(null);
		getModelService().save(cartModel);

	}

	private AddressModel getDefaultDeliveryPointOfServiceAddressModel(final CartModel cart)
	{
		return cart.getSite().getDefaultDeliveryPointOfService() == null
				|| cart.getSite().getDefaultDeliveryPointOfService().getAddress() == null ? null
						: cart.getSite().getDefaultDeliveryPointOfService().getAddress();

	}




	/**
	 * @param shipmentType
	 * @return
	 */
	private String getStoreId(final ShipmentType shipmentType)
	{

		return ShipmentType.PICKUP_IN_STORE.equals(getShipmentType(shipmentType)) && hasSessionCart()
				? getDefaultDeliveryPointOfServiceId()
				: null;
	}

	/**
	 * @return
	 */
	private String getDefaultDeliveryPointOfServiceId()
	{

		return getCartService().getSessionCart().getStore() != null
				&& getCartService().getSessionCart().getSite().getDefaultDeliveryPointOfService() != null
						? getCartService().getSessionCart().getSite().getDefaultDeliveryPointOfService().getName()
						: null;

	}

	/**
	 * @param shipmentType
	 */
	private ShipmentType getShipmentType(final ShipmentType shipmentType)
	{
		if (shipmentType != null)
		{
			return shipmentType;
		}

		if (hasSessionCart())
		{
			if (getCartService().getSessionCart().getShipmentType() != null)
			{
				return getCartService().getSessionCart().getShipmentType();
			}
			else if (getCartService().getSessionCart().getSite() != null
					&& getCartService().getSessionCart().getSite().getDefaultShipmentType() != null)
			{
				return getCartService().getSessionCart().getSite().getDefaultShipmentType();
			}
		}
		return ShipmentType.DELIVERY;
	}

	@Override
	public List<ShipmentTypeData> getSupportedShipmentTypes()
	{
		return getShipmentTypeConverter().convertAll(Arrays.asList(ShipmentType.values()));
	}

	@Override
	public ShipmentTypeData getCurrentShipmentType()
	{
		final ShipmentType currentShipmentType = getCurrentShipmentTypeModle();

		return getShipmentTypeConverter().convert(currentShipmentType);
	}

	private ShipmentType getCurrentShipmentTypeModle()
	{
		if (hasSessionCart() && getCartService().getSessionCart().getShipmentType() != null)
		{
			return getCartService().getSessionCart().getShipmentType();
		}
		else if (hasSessionCart() && getCartService().getSessionCart().getSite() != null
				&& getCartService().getSessionCart().getSite().getDefaultShipmentType() != null)
		{
			return getCartService().getSessionCart().getSite().getDefaultShipmentType();
		}
		return ShipmentType.DELIVERY;
	}

	@Override
	public CartTimeSlotData getCartTimeSlotDataForCurrentCart()
	{

		final CartModel cart = getCartService().getSessionCart();
		LOG.debug("Get Cart  TimeSlot Data For Current Cart ........");

		return getCartTimeSlotData(cart);
	}



	@Override
	public CartTimeSlotData getCartTimeSlotData(final CartModel cart)
	{
		Preconditions.checkArgument(cart != null, "cart is null");

		if (cart.getDeliveryMode() == null || cart.getDeliveryAddress() == null || cart.getDeliveryAddress().getCity() == null
				|| cart.getDeliveryAddress().getArea() == null || cart.getStore() == null
				|| CollectionUtils.isEmpty(cart.getStore().getExpressDeliveryTimes()) || CollectionUtils.isEmpty(cart.getEntries()))
		{
			return null;
		}

		final CartTimeSlotData cartTimeSlotData = new CartTimeSlotData();

		final List<String> messages = new ArrayList<>();
		cartTimeSlotData.setMessages(messages);
		final CityModel city = cart.getDeliveryAddress().getCity();
		if (!hasShippingItems(cart))
		{
			cartTimeSlotData.setMessages(messages);
			cartTimeSlotData.setShowTimeSlot(true);
			return cartTimeSlotData;
		}

		boolean anyProductIsExpress = false;
		boolean anyProductIsNotExpress = false;
		LOG.info("Check if products are express");

		for (final AbstractOrderEntryModel entry : cart.getEntries())
		{
			if (entry.getProduct().isExpress())
			{
				anyProductIsExpress = true;
			}
			else
			{
				anyProductIsNotExpress = true;
			}
		}

		if (anyProductIsExpress && anyProductIsNotExpress)
		{
			cartTimeSlotData.getMessages().add(cart.getStore().getExpressDeliveryMixedItemExpress());
			cartTimeSlotData.setShowTimeSlot(true);
			return cartTimeSlotData;
		}
		else if (anyProductIsNotExpress)
		{
			cartTimeSlotData.setShowTimeSlot(true);
			return cartTimeSlotData;
		}

		final AreaModel area = cart.getDeliveryAddress().getArea();
		if (!area.isExpressArea())
		{
			cartTimeSlotData.getMessages().add(cart.getStore().getExpressDeliveryAreaNotExpress());
			cartTimeSlotData.setShowTimeSlot(true);
			return cartTimeSlotData;
		}

		if (!city.isExpress())
		{
			LOG.info("City is not express");
			cartTimeSlotData.getMessages().add(CITY_NOT_EXPRESS);
			cartTimeSlotData.setShowTimeSlot(true);
			return cartTimeSlotData;
		}


		final Set<TimeRangeModel> timesRanges = cart.getStore().getExpressDeliveryTimes();
		final LocalTime time = LocalTime.now();
		if (timesRanges.stream().noneMatch(range -> isTimeEnclosed(range, time)))
		{
			cartTimeSlotData.getMessages().add(cart.getStore().getExpressDeliveryInvalidTimeMessage());
			cartTimeSlotData.setShowTimeSlot(true);
			return cartTimeSlotData;
		}



		final double maximumWeight = cart.getStore().getExpressDeliveryWeight() < 1.0 ? 10
				: cart.getStore().getExpressDeliveryWeight();
		final double weight = cart.getEntries().stream().mapToDouble(p -> p.getProduct().getWeight() * p.getQuantity()).sum();

		if (weight > maximumWeight)
		{
			cartTimeSlotData.getMessages().add(cart.getStore().getExpressDeliveryWeightLimitExceeded());
			cartTimeSlotData.setShowTimeSlot(true);
			return cartTimeSlotData;
		}
		if (cartTimeSlotData.getMessages().isEmpty())
		{
			cartTimeSlotData.getMessages().add(cart.getStore().getExpressDeliveryOrderMessage());
			cartTimeSlotData.setShowTimeSlot(false);
			return cartTimeSlotData;
		}
		if (cartTimeSlotData.getMessages().isEmpty())
		{
			cartTimeSlotData.getMessages().add(cart.getStore().getExpressDeliveryOrderMessage());
			cartTimeSlotData.setShowTimeSlot(false);
			return cartTimeSlotData;
		}
		return cartTimeSlotData;


	}

	private boolean isTimeEnclosed(final TimeRangeModel range, final LocalTime time)
	{
		try
		{
			final LocalTime start = LocalTime.parse(range.getStart());
			final LocalTime end = LocalTime.parse(range.getEnd());
			return (start.isBefore(time) && end.isAfter(time));
		}
		catch (final DateTimeParseException e)
		{
			return false;
		}
	}

	public boolean hasShippingItems(final CartModel cart)
	{
		return hasItemsMatchingPredicate(e -> e.getDeliveryPointOfService() == null, cart);
	}

	protected boolean hasItemsMatchingPredicate(final Predicate<AbstractOrderEntryModel> predicate, final CartModel cart)
	{
		if (cart != null && !CollectionUtils.isEmpty(cart.getEntries()))
		{
			for (final AbstractOrderEntryModel entry : cart.getEntries())
			{
				if (predicate.test(entry))
				{
					return true;
				}
			}
		}
		return false;
	}


	private CartModel getSessionCartModel()
	{
		return hasSessionCart() ? getCartService().getSessionCart() : null;
	}
}
