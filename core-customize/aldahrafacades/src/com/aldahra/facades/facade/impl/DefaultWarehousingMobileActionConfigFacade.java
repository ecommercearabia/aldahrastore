/**
 *
 */
package com.aldahra.facades.facade.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateIfSingleResult;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;
import de.hybris.platform.util.localization.Localization;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.aldahra.Ordermanagement.service.WarehousingMobileActionConfigService;
import com.aldahra.aldahrafacades.dto.config.ActionConfigData;
import com.aldahra.aldahrafacades.dto.config.WarehousingActionConfigData;
import com.aldahra.facades.facade.WarehousingMobileActionConfigFacade;

/**
 * @author monzer
 *
 */
public class DefaultWarehousingMobileActionConfigFacade implements WarehousingMobileActionConfigFacade
{

	@Resource(name = "warehousingMobileActionConfigService")
	private WarehousingMobileActionConfigService warehousingMobileActionConfigService;

	@Resource(name = "consignmentGenericDao")
	private GenericDao<ConsignmentModel> consignmentGenericDao;

	@Override
	public WarehousingActionConfigData getWarehousingActionsConfigForConsignment(
			final String consignmentCode)
	{
		final ConsignmentModel consignmentModel = getConsignmentModelForCode(consignmentCode);

		final boolean confirmShipmentActionConfig = warehousingMobileActionConfigService
				.canPerformWarehousingMobileConfirmShipmentActionConfig(consignmentModel);
		final boolean createShipmentActionConfig = warehousingMobileActionConfigService
				.canPerformWarehousingMobileCreateShipmentActionConfig(consignmentModel);
		final boolean packActionConfig = warehousingMobileActionConfigService
				.canPerformWarehousingMobilePackSlipActionConfig(consignmentModel);
		final boolean pickActionConfig = warehousingMobileActionConfigService
				.canPerformWarehousingMobilePickSlipActionConfig(consignmentModel);
		final boolean printAwbActionConfig = warehousingMobileActionConfigService
				.canPerformWarehousingMobilePrintShipmentAwbActionConfig(consignmentModel);
		final boolean reallocateActionConfig = warehousingMobileActionConfigService
				.canPerformWarehousingMobileReallocateActionConfig(consignmentModel);
		final boolean updateShipmentActionConfig = warehousingMobileActionConfigService
				.canPerformWarehousingMobileUpdateShipmentActionConfig(consignmentModel);
		final boolean exportFormsActionConfig = warehousingMobileActionConfigService
				.canPerformWarehousingMobileExportFormsActionConfig(consignmentModel);
		final boolean printReturnFormActionConfig = warehousingMobileActionConfigService
				.canPerformWarehousingMobilePrintReturnFormActionConfig(consignmentModel);
		final boolean printReturnShippingLabelActionConfig = warehousingMobileActionConfigService
				.canPerformWarehousingMobilePrintReturnShippingLabelActionConfig(consignmentModel);

		return buildWarehousingActionConfigData(confirmShipmentActionConfig, createShipmentActionConfig, packActionConfig,
				pickActionConfig, printAwbActionConfig, reallocateActionConfig, updateShipmentActionConfig, exportFormsActionConfig,
				printReturnFormActionConfig, printReturnShippingLabelActionConfig);
	}

	private WarehousingActionConfigData buildWarehousingActionConfigData(final boolean confirmShipmentActionConfig,
			final boolean createShipmentActionConfig, final boolean packActionConfig, final boolean pickActionConfig,
			final boolean printAwbActionConfig, final boolean reallocateActionConfig, final boolean updateShipmentActionConfig,
			final boolean exportFormsActionConfig, final boolean printReturnFormActionConfig,
			final boolean printReturnShippingLabelActionConfig)
	{
		final WarehousingActionConfigData config = new WarehousingActionConfigData();
		config.setConfirmShipmentActionConfig(buildActionConfigData(confirmShipmentActionConfig));
		config.setCreateShipmentActionConfig(buildActionConfigData(createShipmentActionConfig));
		config.setUpdateShipmentActionConfig(buildActionConfigData(updateShipmentActionConfig));
		config.setPickSlipActionConfig(buildActionConfigData(packActionConfig));
		config.setPackSlipActionConfig(buildActionConfigData(pickActionConfig));
		config.setPrintShipmentAwbActionConfig(buildActionConfigData(printAwbActionConfig));
		config.setReallocateActionConfig(buildActionConfigData(reallocateActionConfig));
		config.setExportFormsActionConfig(buildActionConfigData(exportFormsActionConfig));
		config.setPrintReturnFormActionConfig(buildActionConfigData(printReturnFormActionConfig));
		config.setPrintReturnShippingLabelActionConfig(buildActionConfigData(printReturnShippingLabelActionConfig));
		return config;
	}

	private ActionConfigData buildActionConfigData(final boolean enabled)
	{
		final ActionConfigData actionConfig = new ActionConfigData();
		actionConfig.setEnabled(enabled);
		actionConfig.setClickable(enabled); //For now
		return actionConfig;
	}

	protected ConsignmentModel getConsignmentModelForCode(final String code)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put(ConsignmentModel.CODE, code);

		final List<ConsignmentModel> consignments = getConsignmentGenericDao().find(params);
		validateIfSingleResult(consignments,
				String.format(Localization.getLocalizedString("warehousingfacade.consignments.validation.missing.code"), code),
				String.format(Localization.getLocalizedString("warehousingfacade.consignments.validation.multiple.code"), code));

		return consignments.get(0);
	}

	/**
	 * @return the consignmentGenericDao
	 */
	protected GenericDao<ConsignmentModel> getConsignmentGenericDao()
	{
		return consignmentGenericDao;
	}

}
