/**
 *
 */
package com.aldahra.facades.facade.impl;

import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;

import com.aldahra.aldahrafacades.authority.AuthorityData;
import com.aldahra.aldahrafacades.authority.AuthorityGroupData;
import com.aldahra.core.service.WarehousingAndOMSAuthorityService;
import com.aldahra.facades.facade.WarehousingAndOMSAuthorityFacade;
import com.google.common.collect.Lists;
import com.hybris.backoffice.model.user.BackofficeRoleModel;


/**
 * @author monzer
 *
 */
public class DefaultWarehousingAndOMSAuthorityFacade implements WarehousingAndOMSAuthorityFacade
{

	@Resource(name = "warehousingAndOMSAuthorityService")
	private WarehousingAndOMSAuthorityService warehousingAndOMSAuthorityService;

	@Resource(name = "userService")
	private UserService userService;

	private final String CUSTOMER_SUPPORT = "CUSTOMERSUPPORT";
	private final String WAREHOUSING = "WAREHOUSE";

	@Override
	public AuthorityGroupData getAllAuthorityGroupsForCurrentUser()
	{
		final List<BackofficeRoleModel> authorities = warehousingAndOMSAuthorityService.getAuthorityGroupForCurrentUser();
		return wrapBackofficeRoleModel(authorities);
	}

	@Override
	public AuthorityGroupData getAllAuthorityGroupsForUser(final String userUid)
	{
		final UserModel user = userService.getUserForUID(userUid);
		final List<BackofficeRoleModel> authorities = warehousingAndOMSAuthorityService.getAuthorityGroupForUser(user);
		return wrapBackofficeRoleModel(authorities);
	}

	protected AuthorityGroupData wrapBackofficeRoleModel(final List<BackofficeRoleModel> backofficeRoleModels)
	{
		final List<AuthorityData> authorityGroups = new ArrayList();
		backofficeRoleModels.stream().forEach(backofficeRoleModel -> {
			final AuthorityData authorityGroup = new AuthorityData();
			authorityGroup.setCode(backofficeRoleModel.getUid());
			authorityGroup.setName(backofficeRoleModel.getDisplayName());
			authorityGroup.setDescription(backofficeRoleModel.getDescription());

			final MediaModel profilePicture = backofficeRoleModel.getProfilePicture();

			if (profilePicture != null)
			{
				authorityGroup.setThumbnailURL(profilePicture.getURL());
			}

			final Collection<String> authorities = backofficeRoleModel.getAuthorities();

			if (authorities != null)
			{
				authorityGroup.setAuthorities(new ArrayList<>(backofficeRoleModel.getAuthorities()));
			}
			authorityGroups.add(authorityGroup);
		});
		final AuthorityGroupData groupData = new AuthorityGroupData();
		groupData.setAuthorities(authorityGroups);

		return groupData;
	}

	@Override
	public List<String> getAllViewsAvailabelForUser(final String userUid)
	{
		final AuthorityGroupData groupData = getAllAuthorityGroupsForUser(userUid);
		if (groupData == null)
		{
			return Collections.EMPTY_LIST;
		}
		if (CollectionUtils.isEmpty(groupData.getAuthorities()))
		{
			return Collections.EMPTY_LIST;
		}
		final Set<String> views = new HashSet();
		groupData.getAuthorities().stream().forEach(authority -> {
			if (authority.getCode().toLowerCase().contains(CUSTOMER_SUPPORT.toLowerCase()))
			{
				views.add(CUSTOMER_SUPPORT);
			}
			if (authority.getCode().toLowerCase().contains(WAREHOUSING.toLowerCase()))
			{
				views.add(WAREHOUSING);
			}
		});
		return Lists.newArrayList(views);
	}

}
