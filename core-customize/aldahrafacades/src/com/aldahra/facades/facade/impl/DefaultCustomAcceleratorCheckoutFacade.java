/*
 *
 */
package com.aldahra.facades.facade.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.acceleratorfacades.order.impl.DefaultAcceleratorCheckoutFacade;
import de.hybris.platform.acceleratorfacades.payment.data.PaymentSubscriptionResultData;
import de.hybris.platform.acceleratorservices.model.payment.CCPaySubValidationModel;
import de.hybris.platform.acceleratorservices.payment.dao.CreditCardPaymentSubscriptionDao;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.PaymentSubscriptionResultItem;
import de.hybris.platform.acceleratorservices.payment.enums.DecisionsEnum;
import de.hybris.platform.acceleratorservices.payment.strategies.CreditCardPaymentInfoCreateStrategy;
import de.hybris.platform.acceleratorservices.payment.strategies.PaymentTransactionStrategy;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.NoCardPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.PaymentModeData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.DeliveryModeTypeData;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.enums.PaymentStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.NoCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.order.PaymentModeService;
import de.hybris.platform.payment.dto.BillingInfo;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import com.aldahra.aldahraorder.model.OrderNoteEntryModel;
import com.aldahra.aldahraorder.service.OrderNoteService;
import com.aldahra.aldahraorderfacades.data.OrderNoteData;
import com.aldahra.aldahraorderfacades.facade.OrderNoteFacade;
import com.aldahra.aldahrapayment.ccavenue.exception.PaymentException;
import com.aldahra.aldahrapayment.context.PaymentContext;
import com.aldahra.aldahrapayment.context.PaymentProviderContext;
import com.aldahra.aldahrapayment.customer.service.CustomCustomerAccountService;
import com.aldahra.aldahrapayment.entry.PaymentRequestData;
import com.aldahra.aldahrapayment.entry.PaymentResponseData;
import com.aldahra.aldahrapayment.model.PaymentProviderModel;
import com.aldahra.aldahrastorecredit.service.StoreCreditModeService;
import com.aldahra.aldahrastorecredit.service.StoreCreditService;
import com.aldahra.aldahrastorecreditfacades.data.StoreCreditModeData;
import com.aldahra.aldahrastorecreditfacades.facade.StoreCreditFacade;
import com.aldahra.aldahrastorecreditfacades.facade.StoreCreditModeFacade;
import com.aldahra.aldahratimeslot.model.TimeSlotInfoModel;
import com.aldahra.aldahratimeslot.service.TimeSlotService;
import com.aldahra.aldahratimeslotfacades.TimeSlotData;
import com.aldahra.aldahratimeslotfacades.TimeSlotInfoData;
import com.aldahra.aldahratimeslotfacades.exception.TimeSlotException;
import com.aldahra.aldahratimeslotfacades.facade.TimeSlotFacade;
import com.aldahra.aldahrauser.model.AreaModel;
import com.aldahra.aldahrauser.model.CityModel;
import com.aldahra.core.enums.DeliveryModeType;
import com.aldahra.core.enums.ShipmentType;
import com.aldahra.core.model.TimeRangeModel;
import com.aldahra.facades.exception.ExpressOrderException;
import com.aldahra.facades.exception.enums.ExpressOrderExceptionType;
import com.aldahra.facades.facade.CustomAcceleratorCheckoutFacade;
import com.aldahra.facades.facade.CustomCartFacade;
import com.aldahra.facades.facade.DeliveryModeTypeFacade;
import com.aldahra.facades.order.data.ShipmentTypeData;
import com.google.common.base.Preconditions;


/**
 * The Class DefaultCustomAcceleratorCheckoutFacade.
 *
 * @author mnasro
 *
 */
public class DefaultCustomAcceleratorCheckoutFacade extends DefaultAcceleratorCheckoutFacade
		implements CustomAcceleratorCheckoutFacade
{

	/**
	 *
	 */
	private static final String CONTINUE = "CONTINUE";

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(DefaultCustomAcceleratorCheckoutFacade.class);

	/** The Constant SUBSCRIPTION_INFO_DATA_CANNOT_BE_NULL_MSG. */
	private static final String SUBSCRIPTION_INFO_DATA_CANNOT_BE_NULL_MSG = "SubscriptionInfoData cannot be null";

	/** The Constant SIGNATURE_DATA_CANNOT_BE_NULL_MSG. */
	private static final String SIGNATURE_DATA_CANNOT_BE_NULL_MSG = "SignatureData cannot be null";

	/** The Constant PAYMENT_INFO_DATA_CANNOT_BE_NULL_MSG. */
	private static final String PAYMENT_INFO_DATA_CANNOT_BE_NULL_MSG = "PaymentInfoData cannot be null";

	/** The Constant ORDER_INFO_DATA_CANNOT_BE_NULL_MSG. */
	private static final String ORDER_INFO_DATA_CANNOT_BE_NULL_MSG = "OrderInfoData cannot be null";

	/** The Constant CUSTOMER_INFO_DATA_CANNOT_BE_NULL_MSG. */
	private static final String CUSTOMER_INFO_DATA_CANNOT_BE_NULL_MSG = "CustomerInfoData cannot be null";

	/** The Constant AUTH_REPLY_DATA_CANNOT_BE_NULL_MSG. */
	private static final String AUTH_REPLY_DATA_CANNOT_BE_NULL_MSG = "AuthReplyData cannot be null";

	/** The Constant DECISION_CANNOT_BE_NULL_MSG. */
	private static final String DECISION_CANNOT_BE_NULL_MSG = "Decision cannot be null";

	/** The Constant CREATE_SUBSCRIPTION_RESULT_CANNOT_BE_NULL_MSG. */
	private static final String CREATE_SUBSCRIPTION_RESULT_CANNOT_BE_NULL_MSG = "CreateSubscriptionResult cannot be null";

	@Resource(name = "deliveryModeTypeFacade")
	private DeliveryModeTypeFacade deliveryModeTypeFacade;

	@Resource(name = "timeSlotFacade")
	private TimeSlotFacade timeSlotFacade;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	/** The payment mode converter. */
	@Resource(name = "paymentModeConverter")
	private Converter<PaymentModeModel, PaymentModeData> paymentModeConverter;

	/** The customer account service. */
	@Resource(name = "customerAccountService")
	private CustomCustomerAccountService customerAccountService;

	/** The no card payment info converter. */
	@Resource(name = "noCardPaymentInfoConverter")
	private Converter<NoCardPaymentInfoModel, NoCardPaymentInfoData> noCardPaymentInfoConverter;

	/** The payment context. */
	@Resource(name = "paymentContext")
	private PaymentContext paymentContext;

	/** The payment provider context. */
	@Resource(name = "paymentProviderContext")
	private PaymentProviderContext paymentProviderContext;

	/** The payment transaction strategy. */
	@Resource(name = "paymentTransactionStrategy")
	private PaymentTransactionStrategy paymentTransactionStrategy;

	/** The credit card payment info create strategy. */
	@Resource(name = "creditCardPaymentInfoCreateStrategy")
	private CreditCardPaymentInfoCreateStrategy creditCardPaymentInfoCreateStrategy;

	/** The credit card payment subscription dao. */
	@Resource(name = "creditCardPaymentSubscriptionDao")
	private CreditCardPaymentSubscriptionDao creditCardPaymentSubscriptionDao;

	/** The payment subscription result data converter. */
	@Resource(name = "paymentSubscriptionResultDataConverter")
	private Converter<PaymentSubscriptionResultItem, PaymentSubscriptionResultData> paymentSubscriptionResultDataConverter;


	/** The payment mode service. */
	@Resource(name = "paymentModeService")
	private PaymentModeService paymentModeService;

	/** The address reverse converter. */
	@Resource(name = "addressReverseConverter")
	private Converter<AddressData, AddressModel> addressReverseConverter;

	@Resource(name = "timeSlotInfoReverseConverter")
	private Converter<TimeSlotInfoData, TimeSlotInfoModel> timeSlotInfoReverseConverter;

	@Resource(name = "timeSlotService")
	private TimeSlotService timeSlotService;

	@Resource(name = "storeCreditModeFacade")
	private StoreCreditModeFacade storeCreditModeFacade;

	@Resource(name = "storeCreditFacade")
	private StoreCreditFacade storeCreditFacade;

	@Resource(name = "storeCreditModeService")
	private StoreCreditModeService storeCreditModeService;
	@Resource(name = "storeCreditService")
	private StoreCreditService storeCreditService;

	@Resource(name = "orderNoteFacade")
	private OrderNoteFacade orderNoteFacade;

	@Resource(name = "orderNoteReverseConverter")
	private Converter<OrderNoteData, OrderNoteEntryModel> orderNoteReverseConverter;

	@Resource(name = "orderNoteService")
	private OrderNoteService orderNoteService;

	/**
	 * @return the customCartFacade
	 */
	public CustomCartFacade getCustomCartFacade()
	{
		return (CustomCartFacade) getCartFacade();
	}

	/**
	 * Gets the payment mode service.
	 *
	 * @return the payment mode service
	 */
	protected PaymentModeService getPaymentModeService()
	{
		return paymentModeService;
	}

	/**
	 * Checks for no payment info.
	 *
	 * @return true, if successful
	 */
	@Override
	public boolean hasNoPaymentInfo()
	{
		final CartData cartData = getCheckoutCart();
		return cartData == null || (cartData.getPaymentInfo() == null && cartData.getNoCardPaymentInfo() == null);
	}

	/**
	 * Gets the supported payment modes.
	 *
	 * @return the supported payment modes
	 */
	@Override
	public Optional<List<PaymentModeData>> getSupportedPaymentModes()
	{
		final CartModel cartModel = getCart();
		if (cartModel == null)
		{
			return Optional.empty();
		}
		if (cartModel.getDeliveryMode() == null)
		{
			LOG.error("No DeliveryMode select For current cart code : " + cartModel.getCode());
			return Optional.empty();
		}

		if (CollectionUtils.isEmpty(cartModel.getDeliveryMode().getSupportedPaymentModes()))
		{
			LOG.error("No Payment Modes Defined For delivery Mode Code : " + cartModel.getDeliveryMode().getCode());
			return Optional.empty();
		}
		if (cartModel.getTotalPrice() == 0)
		{
			final List<PaymentModeModel> supportedPaymentModes = cartModel.getDeliveryMode().getSupportedPaymentModes().stream()
					.filter(Objects::nonNull).filter(p -> Boolean.TRUE.equals(p.getActive()) && CONTINUE.equalsIgnoreCase(p.getCode()))
					.collect(Collectors.toList());

			return Optional.ofNullable(paymentModeConverter.convertAll(supportedPaymentModes));
		}
		final List<PaymentModeModel> supportedPaymentModes = cartModel.getDeliveryMode().getSupportedPaymentModes().stream()
				.filter(Objects::nonNull).filter(p -> Boolean.TRUE.equals(p.getActive()) && !CONTINUE.equalsIgnoreCase(p.getCode()))
				.collect(Collectors.toList());

		return Optional.ofNullable(paymentModeConverter.convertAll(supportedPaymentModes));
	}

	@Override
	public Optional<List<PaymentModeData>> getSupportedPaymentModesForStoreFront()
	{
		final Optional<List<PaymentModeData>> supportedPaymentModes = this.getSupportedPaymentModes();
		if (supportedPaymentModes.isEmpty())
		{
			return Optional.empty();
		}
		final List<PaymentModeData> storefrontPaymentModes = supportedPaymentModes.get().stream()
				.filter(pm -> Boolean.TRUE.equals(pm.isActive()) && Boolean.TRUE.equals(pm.isActiveForStorefront()))
				.collect(Collectors.toList());
		return Optional.ofNullable(storefrontPaymentModes);
	}

	@Override
	public Optional<List<PaymentModeData>> getSupportedPaymentModesForOCC()
	{
		setDeliveryModeIfAvailable();
		final Optional<List<PaymentModeData>> supportedPaymentModes = this.getSupportedPaymentModes();
		if (supportedPaymentModes.isEmpty())
		{
			return Optional.empty();
		}
		final List<PaymentModeData> occPaymentModes = supportedPaymentModes.get().stream()
				.filter(pm -> Boolean.TRUE.equals(pm.isActive()) && Boolean.TRUE.equals(pm.isActiveForOCC()))
				.collect(Collectors.toList());
		return Optional.ofNullable(occPaymentModes);
	}

	@Override
	public Optional<List<PaymentModeData>> getSupportedPaymentModesForOCCForDevice(final SalesApplication deviceType)
	{
		final Optional<List<PaymentModeData>> supportedPaymentModesForOCC = getSupportedPaymentModesForOCC();

		if (deviceType == null)
		{
			return supportedPaymentModesForOCC;
		}

		return supportedPaymentModesForOCC.isPresent() ? Optional.of(supportedPaymentModesForOCC.get().stream()
				.filter(mode -> mode.getDeviceType().contains(deviceType)).collect(Collectors.toList())) : Optional.empty();
	}

	@Override
	public void removePaymentSubscription()
	{
		if (!checkIfCurrentUserIsTheCartUser())
		{
			return;
		}

		getCustomerAccountService().removePaymentSubscription(getCart());

	}

	/**
	 * Creates the payment subscription.
	 *
	 * @param paymentInfoData
	 *           the payment info data
	 * @return the optional
	 */
	@Override
	public Optional<NoCardPaymentInfoData> createPaymentSubscription(final NoCardPaymentInfoData paymentInfoData)
	{
		validateParameterNotNullStandardMessage("paymentInfoData", paymentInfoData);
		final AddressData billingAddressData = paymentInfoData.getBillingAddress();
		validateParameterNotNullStandardMessage("billingAddressData", billingAddressData);
		if (!checkIfCurrentUserIsTheCartUser())
		{
			return Optional.empty();
		}

		final BillingInfo billingInfo = new BillingInfo();
		billingInfo.setCity(billingAddressData.getTown());
		billingInfo.setCountry(billingAddressData.getCountry() == null ? null : billingAddressData.getCountry().getIsocode());
		billingInfo.setRegion(billingAddressData.getRegion() == null ? null : billingAddressData.getRegion().getIsocode());
		billingInfo.setFirstName(billingAddressData.getFirstName());
		billingInfo.setLastName(billingAddressData.getLastName());
		billingInfo.setEmail(billingAddressData.getEmail());
		billingInfo.setPhoneNumber(billingAddressData.getPhone());
		billingInfo.setPostalCode(billingAddressData.getPostalCode());
		billingInfo.setStreet1(billingAddressData.getLine1());
		billingInfo.setStreet2(billingAddressData.getLine2());

		final String noCardTypeCode = paymentInfoData.getNoCardTypeData() == null ? null
				: paymentInfoData.getNoCardTypeData().getCode();

		final Optional<NoCardPaymentInfoModel> noCardPaymentInfo = getCustomerAccountService().createPaymentSubscription(
				getCurrentUserForCheckout(), billingInfo, billingAddressData.getTitleCode(), getPaymentProvider(),
				paymentInfoData.isSaved(), noCardTypeCode);

		if (noCardPaymentInfo.isEmpty())
		{
			return Optional.empty();
		}

		return Optional.ofNullable(getNoCardPaymentInfoConverter().convert(noCardPaymentInfo.get()));

	}

	/**
	 * Sets the general payment details.
	 *
	 * @param paymentInfoId
	 *           the payment info id
	 * @return true, if successful
	 */
	@Override
	public boolean setGeneralPaymentDetails(final String paymentInfoId)
	{
		validateParameterNotNullStandardMessage("paymentInfoId", paymentInfoId);

		if (checkIfCurrentUserIsTheCartUser() && StringUtils.isNotBlank(paymentInfoId))
		{
			final CustomerModel currentUserForCheckout = getCurrentUserForCheckout();
			final Optional<PaymentInfoModel> paymentInfo = getCustomerAccountService().getPaymentInfoForCode(currentUserForCheckout,
					paymentInfoId);

			final CartModel cartModel = getCart();
			if (paymentInfo.isPresent())
			{
				final CommerceCheckoutParameter parameter = createCommerceCheckoutParameter(cartModel, true);
				parameter.setPaymentInfo(paymentInfo.get());
				return getCommerceCheckoutService().setPaymentInfo(parameter);
			}
			LOG.warn(String.format(
					"Did not find paymentInfoModel for user: %s, cart: %s &  paymentInfoId: %s. PaymentInfo Will not get set.",
					currentUserForCheckout, cartModel, paymentInfoId));
		}
		return false;
	}

	protected void setExpressOrder(final CartModel cartModel, final OrderModel orderModel)
	{
		// NOTE(Husam): This is not necessary since the default value is "false", but just to make sure!!.
		orderModel.setExpress(false);
		try
		{



			if (this.isExpressOrder() && isExpressDeliveryModeType(getCart()))
			{
				orderModel.setExpress(true);
				getCart().setExpress(true);
				orderModel.setTimeSlotInfo(null);
				orderModel.setTimeslotEndDate(null);
				orderModel.setTimeslotStartDate(null);
			}

		}
		catch (final ExpressOrderException e1)
		{
		}

		getModelService().save(orderModel);
		getModelService().refresh(orderModel);
	}



	/**
	 * @param cart
	 * @return
	 */
	private boolean isExpressDeliveryModeType(final CartModel cart)
	{
		return cart != null && DeliveryModeType.EXPRESS.equals(cart.getDeliveryType());
	}

	@Override
	protected void afterPlaceOrder(final CartModel cartModel, final OrderModel orderModel)
	{
		redeemStoreCreditAmount();
		setExpressOrder(cartModel, orderModel);
		super.afterPlaceOrder(cartModel, orderModel);
	}

	/**
	 * Gets the payment mode converter.
	 *
	 * @return the paymentModeConverter
	 */
	public Converter<PaymentModeModel, PaymentModeData> getPaymentModeConverter()
	{
		return paymentModeConverter;
	}

	/**
	 * Gets the no card payment info converter.
	 *
	 * @return the noCardPaymentInfoConverter
	 */
	public Converter<NoCardPaymentInfoModel, NoCardPaymentInfoData> getNoCardPaymentInfoConverter()
	{
		return noCardPaymentInfoConverter;
	}



	/**
	 * Gets the customer account service.
	 *
	 * @return the customer account service
	 */
	@Override
	protected CustomCustomerAccountService getCustomerAccountService()
	{
		return customerAccountService;
	}


	/**
	 * Sets the payment mode.
	 *
	 * @param paymentMode
	 *           the new payment mode
	 */
	@Override
	public void setPaymentMode(final String paymentMode)
	{
		if (getCart() == null)
		{
			return;
		}
		validateParameterNotNullStandardMessage("paymentMode", paymentMode);
		final PaymentModeModel paymentModeModel = getPaymentModeService().getPaymentModeForCode(paymentMode);
		getCart().setPaymentMode(paymentModeModel);
		if (CONTINUE.equalsIgnoreCase(paymentModeModel.getCode()))
		{
			getCart().setPaymentStatus(PaymentStatus.PAID);
		}
		else
		{
			getCart().setPaymentStatus(PaymentStatus.NOTPAID);
		}
		getCart().setCalculated(Boolean.FALSE);
		getCommerceCheckoutService().calculateCart(getCart());
		getModelService().save(getCart());
	}

	@Override
	public void setPaymentStatus(final PaymentStatus paymentStatus)
	{
		if (getCart() == null)
		{
			return;
		}
		getCart().setPaymentStatus(paymentStatus);
		getModelService().save(getCart());
	}

	/**
	 * Gets the supported payment data.
	 *
	 * @return the supported payment data
	 */
	@Override
	public Optional<PaymentRequestData> getSupportedPaymentData()
	{
		return getPaymentContext().getPaymentDataByCurrentStore(getCart());
	}

	/**
	 * Save billing address.
	 *
	 * @param addressData
	 *           the address data
	 */
	@Override
	public void saveBillingAddress(final AddressData addressData)
	{
		final CartModel sessionCart = getCart();

		if (sessionCart == null)
		{
			return;
		}
		final AddressModel addressModel = getCartAddressModel(sessionCart);
		if (addressData.getId() != null && addressModel != null && addressData.getId().equals(addressModel.getPk().toString()))
		{
			addressModel.setBillingAddress(true);
			sessionCart.setPaymentAddress(addressModel);
			getModelService().save(addressModel);
		}
		else
		{
			final AddressModel convertReverse = addressReverseConverter.convert(addressData);
			convertReverse.setOwner(sessionCart.getUser());
			sessionCart.setPaymentAddress(convertReverse);
			getModelService().save(convertReverse);
			getModelService().save(sessionCart);
		}



	}

	/**
	 * Gets the supported payment provider.
	 *
	 * @return the supported payment provider
	 */
	@Override
	public Optional<PaymentProviderModel> getSupportedPaymentProvider()
	{
		return getPaymentProviderContext().getProviderByCurrentStore();
	}

	/**
	 * Checks if is successful paid order.
	 *
	 * @param data
	 *           the data
	 * @return true, if is successful paid order
	 */
	@Override
	public boolean isSuccessfulPaidOrder(final Object data)
	{
		return getPaymentContext().isSuccessfulPaidOrderByCurrentStore(getCart(), data);
	}

	/**
	 * Gets the payment response data.
	 *
	 * @param data
	 *           the data
	 * @return the payment response data
	 */
	@Override
	public Optional<PaymentResponseData> getPaymentResponseData(final Object data)
	{
		return getPaymentContext().getResponseDataByCurrentStore(getCart(), data);
	}

	/**
	 * Complete payment create subscription.
	 *
	 * @param orderInfoMap
	 *           the order info map
	 * @param saveInAccount
	 *           the save in account
	 * @return the optional
	 */
	@Override
	public Optional<PaymentSubscriptionResultData> completePaymentCreateSubscription(final Map<String, Object> orderInfoMap,
			final boolean saveInAccount)
	{

		final Optional<CreateSubscriptionResult> response = getPaymentContext().interpretResponseByCurrentStore(orderInfoMap);

		Preconditions.checkArgument(response.isPresent(), CREATE_SUBSCRIPTION_RESULT_CANNOT_BE_NULL_MSG);
		ServicesUtil.validateParameterNotNull(response, CREATE_SUBSCRIPTION_RESULT_CANNOT_BE_NULL_MSG);
		Assert.notNull(response.get().getDecision(), DECISION_CANNOT_BE_NULL_MSG);
		Assert.notNull(response.get().getAuthReplyData(), AUTH_REPLY_DATA_CANNOT_BE_NULL_MSG);
		Assert.notNull(response.get().getCustomerInfoData(), CUSTOMER_INFO_DATA_CANNOT_BE_NULL_MSG);
		Assert.notNull(response.get().getOrderInfoData(), ORDER_INFO_DATA_CANNOT_BE_NULL_MSG);
		Assert.notNull(response.get().getPaymentInfoData(), PAYMENT_INFO_DATA_CANNOT_BE_NULL_MSG);
		Assert.notNull(response.get().getSignatureData(), SIGNATURE_DATA_CANNOT_BE_NULL_MSG);
		Assert.notNull(response.get().getSubscriptionInfoData(), SUBSCRIPTION_INFO_DATA_CANNOT_BE_NULL_MSG);

		final PaymentSubscriptionResultItem paymentSubscriptionResult = new PaymentSubscriptionResultItem();
		paymentSubscriptionResult.setSuccess(DecisionsEnum.ACCEPT.name().equalsIgnoreCase(response.get().getDecision()));
		paymentSubscriptionResult.setDecision(String.valueOf(response.get().getDecision()));
		paymentSubscriptionResult.setResultCode(String.valueOf(response.get().getReasonCode()));

		if (DecisionsEnum.ACCEPT.name().equalsIgnoreCase(response.get().getDecision()))
		{
			final CustomerModel customerModel = (CustomerModel) getCart().getUser();
			final PaymentTransactionEntryModel savePaymentTransactionEntry = getPaymentTransactionStrategy()
					.savePaymentTransactionEntry(customerModel, response.get().getRequestId(), response.get().getOrderInfoData());
			final CreditCardPaymentInfoModel cardPaymentInfoModel = getCreditCardPaymentInfoCreateStrategy().saveSubscription(
					customerModel, response.get().getCustomerInfoData(), response.get().getSubscriptionInfoData(),
					response.get().getPaymentInfoData(), saveInAccount);
			paymentSubscriptionResult.setStoredCard(cardPaymentInfoModel);

			// Check if the subscription has already been validated
			final CCPaySubValidationModel subscriptionValidation = getCreditCardPaymentSubscriptionDao()
					.findSubscriptionValidationBySubscription(cardPaymentInfoModel.getSubscriptionId());
			if (subscriptionValidation != null)
			{
				cardPaymentInfoModel.setSubscriptionValidated(true);
				getModelService().save(cardPaymentInfoModel);
				getModelService().remove(subscriptionValidation);
				getModelService().refresh(cardPaymentInfoModel);
			}

			if (savePaymentTransactionEntry != null && savePaymentTransactionEntry.getPaymentTransaction() != null)
			{
				final PaymentTransactionModel paymentTransaction = savePaymentTransactionEntry.getPaymentTransaction();
				paymentTransaction.setInfo(cardPaymentInfoModel);
				savePaymentTransactionEntry.setPaymentTransaction(paymentTransaction);
				final CartModel sessionCart = getCart();
				sessionCart.setPaymentTransactions(Arrays.asList(paymentTransaction));
				getModelService().save(sessionCart);
			}
		}
		else
		{
			final String logData = String.format("Cannot create subscription. Decision: %s - Reason Code: %s",
					response.get().getDecision(), response.get().getReasonCode());
			LOG.error(logData);
		}



		return Optional.ofNullable(getPaymentSubscriptionResultDataConverter().convert(paymentSubscriptionResult));
	}

	@Override
	public void setTimeSlot(final TimeSlotInfoData timeSlotInfoData)
	{
		validateParameterNotNullStandardMessage("timeSlotInfoData", timeSlotInfoData);
		final TimeSlotInfoModel infoModel = timeSlotInfoReverseConverter.convert(timeSlotInfoData);
		if (getCart() != null)
		{
			final CartModel sessionCart = getCart();
			timeSlotService.saveTimeSlotInfo(infoModel, sessionCart, timeSlotInfoData.getDate(), timeSlotInfoData.getStart());
		}
	}

	@Override
	public void setOrderNote(final OrderNoteData orderNote)
	{
		validateParameterNotNullStandardMessage("orderNote", orderNote);
		final OrderNoteEntryModel noteModel = orderNoteReverseConverter.convert(orderNote);
		if (getCart() != null)
		{
			final CartModel sessionCart = getCart();
			orderNoteService.saveOrderNoteEntry(noteModel, sessionCart);
		}
	}

	/**
	 * Gets the payment subscription result data converter.
	 *
	 * @return the payment subscription result data converter
	 */
	protected Converter<PaymentSubscriptionResultItem, PaymentSubscriptionResultData> getPaymentSubscriptionResultDataConverter()
	{
		return paymentSubscriptionResultDataConverter;
	}

	/**
	 * Gets the credit card payment subscription dao.
	 *
	 * @return the credit card payment subscription dao
	 */
	protected CreditCardPaymentSubscriptionDao getCreditCardPaymentSubscriptionDao()
	{
		return creditCardPaymentSubscriptionDao;
	}

	/**
	 * Gets the credit card payment info create strategy.
	 *
	 * @return the credit card payment info create strategy
	 */
	protected CreditCardPaymentInfoCreateStrategy getCreditCardPaymentInfoCreateStrategy()
	{
		return creditCardPaymentInfoCreateStrategy;
	}

	/**
	 * Gets the payment transaction strategy.
	 *
	 * @return the payment transaction strategy
	 */
	protected PaymentTransactionStrategy getPaymentTransactionStrategy()
	{
		return paymentTransactionStrategy;
	}

	/**
	 * Gets the payment provider context.
	 *
	 * @return the payment provider context
	 */
	protected PaymentProviderContext getPaymentProviderContext()
	{
		return paymentProviderContext;
	}

	/**
	 * Gets the payment context.
	 *
	 * @return the payment context
	 */
	protected PaymentContext getPaymentContext()
	{
		return paymentContext;
	}

	@Override
	public Optional<List<StoreCreditModeData>> getSupportedStoreCreditModes()
	{
		return storeCreditModeFacade.getSupportedStoreCreditModesCurrentBaseStore();
	}

	@Override
	public List<DeliveryModeTypeData> getDeliveryModeTypes()
	{
		return deliveryModeTypeFacade.getSupportedDeliveryModeTypes();
	}

	@Override
	public void setStoreCreditMode(final String StoreCreditTypeCode, final Double storeCreditAmaountSelected)
	{
		if (getCart() != null)
		{
			getCart().setStoreCreditMode(storeCreditModeService.getStoreCreditMode(StoreCreditTypeCode));
			getCart().setStoreCreditAmountSelected(storeCreditAmaountSelected);
			getCart().setCalculated(Boolean.FALSE);
			getCommerceCheckoutService().calculateCart(getCart());
			getModelService().save(getCart());
		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.aldahra.facades.facade.CustomAcceleratorCheckoutFacade#redeemStoreCreditAmount(de.hybris.platform.core.model.
	 * order.AbstractOrderModel)
	 */
	@Override
	public void redeemStoreCreditAmount()
	{
		if (getCart() != null && getCart().getStoreCreditAmount() != null && getCart().getStoreCreditAmount().doubleValue() > 0)
		{
			storeCreditService.redeemStoreCreditAmount(getCart());
		}
	}


	@Override
	public boolean isStoreCreditModeSupported(final String StoreCreditTypeCode)
	{
		return storeCreditModeFacade.isStoreCreditModeSupportedByCurrentBaseStore(StoreCreditTypeCode);
	}

	@Override
	public Optional<PriceData> getAvailableBalanceStoreCreditAmount()
	{
		if (getCart() == null)
		{
			return Optional.empty();
		}
		return storeCreditFacade.getStoreCreditAmountByCurrentUserAndCurrentBaseStore();
	}


	@Override
	public Optional<PriceData> getStoreCreditAmountFullRedeem()
	{
		if (getCart() == null)
		{
			return Optional.empty();
		}
		return storeCreditFacade.getStoreCreditAmountFullRedeem(getCart());
	}

	@Override
	public Optional<PaymentResponseData> getPaymentOrderStatusResponseData(final Object data) throws PaymentException
	{
		return getPaymentContext().getPaymentOrderStatusResponseDataByCurrentStore(data, getCart());
	}

	@Override
	public List<OrderNoteData> getSupportedOrderNotes()
	{
		return orderNoteFacade.getOrderNotesByCurrentSite();
	}

	@Override
	public Optional<TimeSlotData> getSupportedTimeSlot()
	{
		final CartModel cartModel = getCart();
		Optional<TimeSlotData> timeSlot = Optional.empty();
		final AddressModel cartAddressModel = getCartAddressModel(cartModel);
		if (cartAddressModel == null)
		{
			LOG.warn("No address was found neither on cart or on point of service");
			return Optional.empty();
		}
		try
		{
			switch (cmsSiteService.getCurrentSite().getTimeSlotConfigType())
			{
				case BY_AREA:
					if (cartAddressModel.getArea() == null || StringUtils.isBlank(cartAddressModel.getArea().getCode()))
					{
						return Optional.empty();
					}
					timeSlot = timeSlotFacade.getTimeSlotDataByArea(cartAddressModel.getArea().getCode());

					break;
				case BY_DELIVERYMODE:
					if (cartModel.getDeliveryMode() == null || cartModel.getDeliveryMode().getCode() == null)
					{
						return Optional.empty();
					}
					timeSlot = timeSlotFacade.getTimeSlotData(cartModel.getDeliveryMode().getCode());

					break;

				default:
					timeSlot = timeSlotFacade.getTimeSlotData(cartModel.getDeliveryMode().getCode());

					break;
			}
		}
		catch (final TimeSlotException e)
		{
			LOG.error(e.getMessage(), e);
			return Optional.empty();
		}
		return timeSlot;
	}

	@Override
	public OrderData placeOrder(final SalesApplication salesApplication) throws InvalidCartException
	{
		final CartModel cartModel = getCart();
		if (cartModel != null
				&& (cartModel.getUser().equals(getCurrentUserForCheckout()) || getCheckoutCustomerStrategy().isAnonymousCheckout()))
		{
			beforePlaceOrder(cartModel);
			final OrderModel orderModel = placeOrder(cartModel);
			orderModel.setSalesApplication(salesApplication);
			getModelService().save(orderModel);
			getModelService().refresh(orderModel);
			afterPlaceOrder(cartModel, orderModel);
			return getOrderConverter().convert(orderModel);
		}
		return null;
	}

	@Override
	public void resetPaymentInfo()
	{
		final CartModel cartModel = getCart();
		if (cartModel != null)
		{
			cartModel.setPaymentInfo(null);
			getModelService().save(cartModel);
			getModelService().refresh(cartModel);
		}
	}

	@Override
	public void setCCAvenueId(final String ccAvenueId)
	{
		if (getCart() != null)
		{
			final CartModel sessionCart = getCart();
			sessionCart.setCcAvenueId(ccAvenueId);
			getModelService().save(sessionCart);
			getModelService().refresh(sessionCart);

		}
	}

	@Override
	public void setDeliveryInstructions(final String instructions)
	{
		if (getCart() != null)
		{
			final CartModel sessionCart = getCart();
			sessionCart.setDeliveryInstructions(instructions);
			getModelService().save(sessionCart);
			getModelService().refresh(sessionCart);
		}
	}

	@Override
	public void setDeliveryModeType(final DeliveryModeType deliveryMode)
	{
		if (getCart() != null)
		{
			final CartModel sessionCart = getCart();
			sessionCart.setDeliveryType(deliveryMode);
			getModelService().save(sessionCart);
			getModelService().refresh(sessionCart);
		}
	}

	@Override
	public DeliveryModeType getDeliveryModeType()
	{
		DeliveryModeType type = DeliveryModeType.NORMAL;
		if (getCart() != null)
		{
			final CartModel sessionCart = getCart();
			type = sessionCart.getDeliveryType();
			getModelService().save(sessionCart);
			getModelService().refresh(sessionCart);
		}
		return DeliveryModeType.EXPRESS.equals(type) ? DeliveryModeType.EXPRESS : DeliveryModeType.NORMAL;
	}


	@Override
	public void resetCartForGuestUser()
	{
		if (getCart() != null)
		{
			final CartModel sessionCart = getCart();
			sessionCart.setTimeSlotInfo(null);
			sessionCart.setPaymentMode(null);
			getModelService().save(sessionCart);
			getModelService().refresh(sessionCart);
		}

	}

	private boolean isTimeEnclosed(final TimeRangeModel range, final LocalTime time)
	{
		try
		{
			final LocalTime start = LocalTime.parse(range.getStart());
			final LocalTime end = LocalTime.parse(range.getEnd());
			return (start.isBefore(time) && end.isAfter(time));
		}
		catch (final DateTimeParseException e)
		{
			return false;
		}
	}

	@Override
	public boolean isExpressOrder() throws ExpressOrderException
	{

		final CartModel cart = getCart();
		if (cart == null || !hasShippingItems() || cart.getDeliveryAddress() == null || cart.getDeliveryAddress().getCity() == null
				|| cart.getDeliveryAddress().getArea() == null || cart.getStore() == null
				|| CollectionUtils.isEmpty(cart.getStore().getExpressDeliveryTimes()) || CollectionUtils.isEmpty(cart.getEntries()))
		{
			return false;
		}
		boolean anyProductIsExpress = false;
		boolean anyProductIsNotExpress = false;
		for (final AbstractOrderEntryModel entry : cart.getEntries())
		{
			if (entry.getProduct().isExpress())
			{
				anyProductIsExpress = true;
			}
			else
			{
				anyProductIsNotExpress = true;

			}


		}

		if (anyProductIsExpress && anyProductIsNotExpress)
		{
			throw new ExpressOrderException(ExpressOrderExceptionType.PRODUCTS_MIXED_EXPRESS);
		}
		else if (anyProductIsNotExpress)
		{
			throw new ExpressOrderException(ExpressOrderExceptionType.PRODUCTS_NOT_EXPRESS);
		}


		final CityModel city = cart.getDeliveryAddress().getCity();
		if (!city.isExpress())
		{
			throw new ExpressOrderException(ExpressOrderExceptionType.CITY_NOT_EXPRESS);
		}

		final AreaModel area = cart.getDeliveryAddress().getArea();
		if (!area.isExpressArea())
		{
			throw new ExpressOrderException(ExpressOrderExceptionType.AREA_NOT_EXPRESS);
		}

		final Set<TimeRangeModel> timesRanges = cart.getStore().getExpressDeliveryTimes();
		final LocalTime time = LocalTime.now();
		if (timesRanges.stream().noneMatch(range -> isTimeEnclosed(range, time)))
		{
			throw new ExpressOrderException(ExpressOrderExceptionType.TIME_NOT_EXPRESS);
		}


		final double maximumWeight = cart.getStore().getExpressDeliveryWeight() < 1.0 ? 10
				: cart.getStore().getExpressDeliveryWeight();
		final double weight = cart.getEntries().stream().mapToDouble(p -> p.getProduct().getWeight() * p.getQuantity()).sum();

		if (weight > maximumWeight)
		{
			throw new ExpressOrderException(ExpressOrderExceptionType.WEIGHT_EXCEEDED);
		}

		//		final String type = getDeliveryModeType().getCode();
		//		if (type != null && DeliveryModeType.NORMAL.getCode().equals(type))
		//		{
		//			return false;
		//		}
		return true;
	}

	@Override
	public boolean isExpressOrderWithoutValidationOnTime()
	{
		final CartModel cart = getCart();
		if (cart == null || cart.getDeliveryAddress() == null || cart.getDeliveryAddress().getCity() == null
				|| cart.getDeliveryAddress().getArea() == null || cart.getStore() == null
				|| CollectionUtils.isEmpty(cart.getStore().getExpressDeliveryTimes()) || !hasShippingItems())
		{
			return false;
		}
		final CityModel city = cart.getDeliveryAddress().getCity();
		final AreaModel area = cart.getDeliveryAddress().getArea();

		final boolean isThereANonExpressProduct = cart.getEntries().stream().anyMatch(e -> !e.getProduct().isExpress());
		final boolean isTheCityExpress = city.isExpress();
		final boolean isTheAreaExpress = area.isExpressArea();
		return !isThereANonExpressProduct && isTheCityExpress && isTheAreaExpress;
	}

	@Override
	public Optional<AddressData> getCartAddress()
	{
		final CartModel cart = getCart();
		if (cart == null || cart.getShipmentType() == null)
		{
			return Optional.empty();
		}
		if (ShipmentType.PICKUP_IN_STORE.equals(cart.getShipmentType()))
		{

			return Optional.ofNullable(getDefaultDeliveryPointOfServiceAddress(cart));

		}
		else
		{
			return Optional.ofNullable(getDeliveryAddress());
		}

	}

	private AddressModel getCartAddressModel(final CartModel cart)
	{
		if (cart == null || cart.getShipmentType() == null)
		{
			return null;
		}
		if (ShipmentType.PICKUP_IN_STORE.equals(cart.getShipmentType()))
		{

			return getDefaultDeliveryPointOfServiceAddressModel(cart);

		}
		else
		{
			return getDeliveryAddressModel(cart);
		}

	}

	private AddressModel getDeliveryAddressModel(final CartModel cart)
	{
		if (cart == null)
		{
			return null;
		}
		final AddressModel deliveryAddress = cart.getDeliveryAddress();
		if (deliveryAddress == null)
		{
			return null;
		}
		// Ensure that the delivery address is in the set of supported addresses
		return getDeliveryAddressModelForCode(deliveryAddress.getPk().toString());

	}

	private AddressModel getDefaultDeliveryPointOfServiceAddressModel(final CartModel cart)
	{
		return cart.getSite().getDefaultDeliveryPointOfService() == null
				|| cart.getSite().getDefaultDeliveryPointOfService().getAddress() == null ? null
						: cart.getSite().getDefaultDeliveryPointOfService().getAddress();

	}

	private AddressData getDefaultDeliveryPointOfServiceAddress(final CartModel cart)
	{
		final AddressModel addressModel = cart.getSite().getDefaultDeliveryPointOfService() == null
				|| cart.getSite().getDefaultDeliveryPointOfService().getAddress() == null ? null
						: cart.getSite().getDefaultDeliveryPointOfService().getAddress();
		return addressModel == null ? null : getAddressConverter().convert(addressModel);

	}

	@Override
	public List<ShipmentTypeData> getSupportedShipmentTypes()
	{
		return getCustomCartFacade().getSupportedShipmentTypes();
	}

	@Override
	public ShipmentTypeData getCurrentShipmentType()
	{
		return getCustomCartFacade().getCurrentShipmentType();
	}







}
