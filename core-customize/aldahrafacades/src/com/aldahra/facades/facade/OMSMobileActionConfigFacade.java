/**
 *
 */
package com.aldahra.facades.facade;

import com.aldahra.aldahrafacades.dto.config.OMSActionConfigData;


/**
 * @author monzer
 *
 */
public interface OMSMobileActionConfigFacade
{
	OMSActionConfigData getOMSActionConfigForOrder(String orderCode);
}
