/**
 *
 */
package com.aldahra.facades.facade;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;

import com.aldahra.core.loyaltypoint.data.LoyaltyPointHistoryData;


/**
 * @author mnasro
 *
 */
public interface LoyaltyPointFacade
{

	public List<LoyaltyPointHistoryData> getLoyaltyPointHistory(CustomerModel customerModel);

	public List<LoyaltyPointHistoryData> getLoyaltyPointHistoryByCurrentCustomer();

	public double getLoyaltyPoints(CustomerModel customerModel);

	public double getLoyaltyPointsByCurrentCustomer();

	public boolean isLoyaltyPointsEnabled(BaseStoreModel baseStoreModel);

	public boolean isLoyaltyPointsEnabledByCurrentStore();


}

