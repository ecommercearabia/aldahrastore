package com.aldahra.facades.facade;

import de.hybris.platform.commercefacades.user.data.DeliveryModeTypeData;

import java.util.List;
import java.util.Optional;


/**
 * @author mnasro
 *
 */
public interface DeliveryModeTypeFacade
{
	public Optional<DeliveryModeTypeData> getDeliveryModeType(String deliveryModeTypeTypeCode);

	public List<DeliveryModeTypeData> getSupportedDeliveryModeTypes();
}
