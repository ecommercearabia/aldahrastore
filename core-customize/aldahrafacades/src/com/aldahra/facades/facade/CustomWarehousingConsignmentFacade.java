/**
 *
 */
package com.aldahra.facades.facade;

import de.hybris.platform.warehousingfacades.order.WarehousingConsignmentFacade;

import java.util.Optional;

import com.aldahra.aldahrafulfillment.exception.FulfillentException;


/**
 * @author monzer
 *
 */
public interface CustomWarehousingConsignmentFacade extends WarehousingConsignmentFacade
{
	Optional<String> createShipment(String consignmentCode) throws FulfillentException;

	Optional<byte[]> printConsignmentAWB(String consignmentCode) throws FulfillentException;

	void updateConsignmentTrackingId(String consignmentCode, String trackingId);

}
