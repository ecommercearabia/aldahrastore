/**
 *
 */
package com.aldahra.facades.facade;

import com.aldahra.aldahrafacades.dto.config.WarehousingActionConfigData;


/**
 * @author monzer
 *
 */
public interface WarehousingMobileActionConfigFacade
{

	WarehousingActionConfigData getWarehousingActionsConfigForConsignment(String consignmentCode);

}
