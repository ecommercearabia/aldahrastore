package com.aldahra.facades.facade;

import com.aldahra.aldahratimeslotfacades.exception.TimeSlotException;

// TODO: Auto-generated Javadoc


/**
 * The Interface CustomCheckoutFlowFacade.
 *
 * @author mnasro The Interface CheckoutFlowFacade.
 */
public interface CustomCheckoutFlowFacade
{

	/**
	 * Checks if there is no payment info.
	 *
	 * @return true if there is no payment info
	 */
	boolean hasNoPaymentInfo();

	/**
	 * Checks for no payment mode.
	 *
	 * @return true, if successful
	 */
	boolean hasNoPaymentMode();

	/**
	 * Checks for payment provider.
	 *
	 * @return true, if successful
	 */
	boolean hasPaymentProvider();

	boolean hasValidCart();

	/**
	 * Checks for no payment mode.
	 *
	 * @return true, if successful
	 */
	boolean hasNoTimeSlot() throws TimeSlotException;

	boolean isTimeSlotEnabledByCurrentSite() throws TimeSlotException;
}
