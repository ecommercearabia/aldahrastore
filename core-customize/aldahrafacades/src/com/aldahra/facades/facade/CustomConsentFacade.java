/**
 *
 */
package com.aldahra.facades.facade;

import de.hybris.platform.commercefacades.consent.ConsentFacade;
import de.hybris.platform.core.model.user.CustomerModel;


/**
 * @author mnasro
 *
 */
public interface CustomConsentFacade extends ConsentFacade
{

	public void giveConsent(final String consentTemplateId, final Integer consentTemplateVersion, CustomerModel customerModel);

}
