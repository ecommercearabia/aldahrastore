/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.facades.facade;

import de.hybris.platform.acceleratorfacades.order.AcceleratorCheckoutFacade;
import de.hybris.platform.acceleratorfacades.payment.data.PaymentSubscriptionResultData;
import de.hybris.platform.commercefacades.order.data.NoCardPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.PaymentModeData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.DeliveryModeTypeData;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.enums.PaymentStatus;
import de.hybris.platform.order.InvalidCartException;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.aldahra.aldahraorderfacades.data.OrderNoteData;
import com.aldahra.aldahrapayment.ccavenue.exception.PaymentException;
import com.aldahra.aldahrapayment.entry.PaymentRequestData;
import com.aldahra.aldahrapayment.entry.PaymentResponseData;
import com.aldahra.aldahrapayment.model.PaymentProviderModel;
import com.aldahra.aldahrastorecreditfacades.data.StoreCreditModeData;
import com.aldahra.aldahratimeslotfacades.TimeSlotData;
import com.aldahra.aldahratimeslotfacades.TimeSlotInfoData;
import com.aldahra.core.enums.DeliveryModeType;
import com.aldahra.facades.exception.ExpressOrderException;
import com.aldahra.facades.order.data.ShipmentTypeData;


/**
 * The Interface CustomAcceleratorCheckoutFacade.
 *
 * @author mnasro
 */
public interface CustomAcceleratorCheckoutFacade extends AcceleratorCheckoutFacade
{
	public void resetPaymentInfo();

	public OrderData placeOrder(SalesApplication salesApplication) throws InvalidCartException;

	public Optional<TimeSlotData> getSupportedTimeSlot();

	/**
	 * Gets the supported order notes.
	 *
	 * @return the supported order notes
	 */
	public List<OrderNoteData> getSupportedOrderNotes();

	/**
	 * Gets the supported payment modes.
	 *
	 * @return the supported payment modes
	 */
	public Optional<List<PaymentModeData>> getSupportedPaymentModes();

	public Optional<List<PaymentModeData>> getSupportedPaymentModesForStoreFront();

	public Optional<List<PaymentModeData>> getSupportedPaymentModesForOCC();

	public Optional<List<PaymentModeData>> getSupportedPaymentModesForOCCForDevice(SalesApplication deviceType);

	/**
	 * Sets the payment mode.
	 *
	 * @param paymentMode
	 *           the new payment mode
	 */
	public void setPaymentMode(final String paymentMode);

	/**
	 * Sets the payment mode.
	 *
	 * @param paymentMode
	 *           the new payment mode
	 */
	public void setPaymentStatus(final PaymentStatus paymentStatus);

	/**
	 * Gets the supported payment provider.
	 *
	 * @return the supported payment provider
	 */
	public Optional<PaymentRequestData> getSupportedPaymentData();

	/**
	 * Gets the payment response data.
	 *
	 * @param data
	 *           the data
	 * @return the payment response data
	 */
	public Optional<PaymentResponseData> getPaymentResponseData(Object data);

	/**
	 * Gets the supported payment provider.
	 *
	 * @return the supported payment provider
	 */
	public Optional<PaymentProviderModel> getSupportedPaymentProvider();

	/**
	 * Checks if is successful paid order.
	 *
	 * @param data
	 *           the data
	 * @return true, if is successful paid order
	 */
	public boolean isSuccessfulPaidOrder(Object data);

	/**
	 * Creates the payment subscription.
	 *
	 * @param paymentInfoData
	 *           the payment info data
	 * @return the no card payment info data
	 */
	public Optional<NoCardPaymentInfoData> createPaymentSubscription(final NoCardPaymentInfoData paymentInfoData);


	/**
	 * Removes the payment subscription.
	 *
	 * @return nothing
	 */
	public void removePaymentSubscription();

	/**
	 * Set Payment Details on the cart.
	 *
	 * @param paymentInfoId
	 *           the ID of the payment info to set as the default payment
	 * @return true if operation succeeded
	 */
	boolean setGeneralPaymentDetails(String paymentInfoId);


	/**
	 * Save billing address.
	 *
	 * @param addressData
	 *           the address data
	 */
	void saveBillingAddress(AddressData addressData);


	/**
	 * Complete payment create subscription.
	 *
	 * @param orderInfoMap
	 *           the order info map
	 * @param saveInAccount
	 *           the save in account
	 * @return the optional
	 */
	public Optional<PaymentSubscriptionResultData> completePaymentCreateSubscription(final Map<String, Object> orderInfoMap,
			final boolean saveInAccount);


	void setTimeSlot(TimeSlotInfoData timeSlotInfoData);

	public void setOrderNote(final OrderNoteData orderNote);

	public void setDeliveryInstructions(final String instructions);

	public void setDeliveryModeType(final DeliveryModeType deliveryMode);

	public DeliveryModeType getDeliveryModeType();

	public Optional<List<StoreCreditModeData>> getSupportedStoreCreditModes();

	public List<DeliveryModeTypeData> getDeliveryModeTypes();

	public void setStoreCreditMode(final String StoreCreditTypeCode, final Double storeCreditAmaountSelected);

	public void redeemStoreCreditAmount();

	public boolean isStoreCreditModeSupported(final String storeCreditTypeCode);


	public Optional<PriceData> getStoreCreditAmountFullRedeem();

	public Optional<PriceData> getAvailableBalanceStoreCreditAmount();

	/**
	 * Gets the payment response data.
	 *
	 * @param data
	 *           the data
	 * @return the payment response data
	 * @throws PaymentException
	 */
	public Optional<PaymentResponseData> getPaymentOrderStatusResponseData(Object data) throws PaymentException;

	public void setCCAvenueId(final String ccAvenueId);

	public void resetCartForGuestUser();

	public boolean isExpressOrder() throws ExpressOrderException;

	public boolean isExpressOrderWithoutValidationOnTime();



	/**
	 *
	 */
	public Optional<AddressData> getCartAddress();

	public List<ShipmentTypeData> getSupportedShipmentTypes();

	public ShipmentTypeData getCurrentShipmentType();

}
