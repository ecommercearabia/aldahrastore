/**
 *
 */
package com.aldahra.facades.facade;

import java.util.List;

import com.aldahra.aldahrafacades.authority.AuthorityGroupData;


/**
 * @author monzer
 *
 */
public interface WarehousingAndOMSAuthorityFacade
{

	AuthorityGroupData getAllAuthorityGroupsForCurrentUser();

	AuthorityGroupData getAllAuthorityGroupsForUser(String userUid);

	List<String> getAllViewsAvailabelForUser(String userUid);

}
