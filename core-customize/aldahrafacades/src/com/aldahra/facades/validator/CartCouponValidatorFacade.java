/**
 *
 */
package com.aldahra.facades.validator;

import de.hybris.platform.core.model.order.CartModel;

import com.aldahra.facades.exception.CartCouponException;


/**
 * @author Tuqa
 *
 */
public interface CartCouponValidatorFacade
{

	public void validateCartCoupon(CartModel cartModel) throws CartCouponException;

	public void validateCartCouponByCurrentCart() throws CartCouponException;
}
