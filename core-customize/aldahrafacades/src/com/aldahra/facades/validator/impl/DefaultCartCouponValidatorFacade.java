/**
 *
 */
package com.aldahra.facades.validator.impl;

import de.hybris.platform.commercefacades.voucher.exceptions.VoucherOperationException;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.voucher.VoucherService;

import java.util.Collection;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;

import com.aldahra.core.enums.ShipmentType;
import com.aldahra.facades.exception.CartCouponException;
import com.aldahra.facades.exception.enums.CartCouponExceptionType;
import com.aldahra.facades.facade.CustomVoucherFacade;
import com.aldahra.facades.validator.CartCouponValidatorFacade;


/**
 * @author Tuqa
 *
 */
public class DefaultCartCouponValidatorFacade implements CartCouponValidatorFacade
{
	protected static final Logger LOG = Logger.getLogger(DefaultCartCouponValidatorFacade.class);

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "voucherService")
	private VoucherService voucherService;

	@Resource(name = "voucherFacade")
	private CustomVoucherFacade voucherFacade;

	/**
	 * @return the voucherService
	 */
	protected VoucherService getVoucherService()
	{
		return voucherService;
	}

	/**
	 * @return the voucherFacade
	 */
	protected CustomVoucherFacade getVoucherFacade()
	{
		return voucherFacade;
	}

	/**
	 * @return the cartService
	 */
	protected CartService getCartService()
	{
		return cartService;
	}

	@Override
	public void validateCartCouponByCurrentCart() throws CartCouponException
	{
		if (!getCartService().hasSessionCart())
		{
			return;
		}
		validateCartCoupon(getCartService().getSessionCart());
	}


	@Override
	public void validateCartCoupon(final CartModel cartModel) throws CartCouponException
	{
		if (cartModel == null || !ShipmentType.PICKUP_IN_STORE.equals(cartModel.getShipmentType())
				|| cartModel.getSite() == null || !cartModel.getSite().isDisableCouponForPickUpInStore())
		{
			return;
		}

		final Collection<String> vouchers = getVoucherService().getAppliedVoucherCodes(cartModel);

		if (CollectionUtils.isEmpty(vouchers))
		{
			return;
		}
		final String[] parm = new String[vouchers.size()];

		int index = 0;
		for (final String code : vouchers)
		{
			parm[index] = code;
			++index;
		}
		try
		{
			getVoucherFacade().removeAllCoupons(cartModel);
		}
		catch (final VoucherOperationException e)
		{
			throw new CartCouponException(CartCouponExceptionType.REMOVE_COUPON_OPERATION, e.getMessage());
		}

		throw new CartCouponException(CartCouponExceptionType.COUPON_FOR_PICKUP, parm);

	}

}
