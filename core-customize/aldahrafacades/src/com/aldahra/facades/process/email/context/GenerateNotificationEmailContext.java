/**
 *
 */
package com.aldahra.facades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import javax.annotation.Resource;

import org.springframework.util.StringUtils;

import com.aldahra.core.model.GenerateNotificationEmailProcessModel;
import com.google.common.base.Preconditions;


/**
 * @author Tuqa
 *
 */
public class GenerateNotificationEmailContext extends AbstractEmailContext<GenerateNotificationEmailProcessModel>
{
	private static final String MESSAGE = "msg";
	private static final String ORDER_CODE = "orderCode";
	private static final String OPERATION = "operation";

	private static final String INTEGRATION_PROVIDER = "provider";
	private String msg;
	private String orderCode;
	private String provider;
	private String operation;

	/**
	 * @return the operation
	 */
	protected String getOperation()
	{
		return operation;
	}

	/**
	 * @param operation
	 *           the operation to set
	 */
	protected void setOperation(final String operation)
	{
		this.operation = operation;
	}

	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	/**
	 * @return the commonI18NService
	 */
	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Override
	public void init(final GenerateNotificationEmailProcessModel businessProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(businessProcessModel, emailPageModel);
		Preconditions.checkArgument(!StringUtils.isEmpty(businessProcessModel.getNotificationMsg()),
				"Notification message must not be null.");
		Preconditions.checkArgument(businessProcessModel.getOrder() != null, "Order must not be null.");
		Preconditions.checkArgument(businessProcessModel.getOrder().getSite() != null, "Site must not be null.");
		Preconditions.checkArgument(businessProcessModel.getIntegrationProvider() != null,
				"Integration provider must not be null.");
		final CMSSiteModel cmsSite = (CMSSiteModel) businessProcessModel.getOrder().getSite();
		Preconditions.checkArgument(!StringUtils.isEmpty(cmsSite.getIntegrationNotificationEmailsReceiver()),
				"Integration Error Emails Receiver must not be null.");

		msg = businessProcessModel.getNotificationMsg();
		orderCode = businessProcessModel.getOrder().getCode();
		provider = businessProcessModel.getIntegrationProvider().getCode();
		operation = businessProcessModel.getOperation();

		put(EMAIL, cmsSite.getIntegrationNotificationEmailsReceiver());
		put(MESSAGE, msg);
		put(ORDER_CODE, orderCode);
		put(OPERATION, operation);
		put(INTEGRATION_PROVIDER, provider);
	}

	@Override
	protected BaseSiteModel getSite(final GenerateNotificationEmailProcessModel businessProcessModel)
	{
		return businessProcessModel.getOrder().getSite();
	}

	@Override
	protected CustomerModel getCustomer(final GenerateNotificationEmailProcessModel businessProcessModel)
	{
		return (CustomerModel) businessProcessModel.getOrder().getUser();

	}

	@Override
	protected LanguageModel getEmailLanguage(final GenerateNotificationEmailProcessModel businessProcessModel)
	{
		if (businessProcessModel.getOrder() instanceof OrderModel)
		{
			return ((OrderModel) businessProcessModel.getOrder()).getLanguage();
		}
		return getCommonI18NService().getAllLanguages().stream().filter(l -> "en".equalsIgnoreCase(l.getIsocode())).findAny().get();
	}

}
