/**
 *
 */
package com.aldahra.facades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.beans.factory.annotation.Required;

import com.aldahra.Ordermanagement.model.ReferralNewAppliedEmailProcessModel;


/**
 * @author mohammad
 *
 */
public class ReferralNewAppliedEmailContext extends AbstractEmailContext<ReferralNewAppliedEmailProcessModel>
{
	private Converter<UserModel, CustomerData> customerConverter;
	private CustomerData customerData;
	private double newAppliedRewardAmount;
	private NumberTool number;

	@Override
	public void init(final ReferralNewAppliedEmailProcessModel referralNewAppliedEmailProcess, final EmailPageModel emailPageModel)
	{
		super.init(referralNewAppliedEmailProcess, emailPageModel);

		this.number = new NumberTool();

		customerData = getCustomerConverter().convert(getCustomer(referralNewAppliedEmailProcess));
		final BaseStoreModel store = referralNewAppliedEmailProcess.getStore();
		if (!Objects.isNull(store))
		{
			this.newAppliedRewardAmount = store.getReferralCodeNewAppliedRewardAmount();
		}


	}

	@Override
	protected BaseSiteModel getSite(final ReferralNewAppliedEmailProcessModel storeFrontCustomerProcessModel)
	{
		return storeFrontCustomerProcessModel.getSite();
	}

	@Override
	protected CustomerModel getCustomer(final ReferralNewAppliedEmailProcessModel storeFrontCustomerProcessModel)
	{
		return storeFrontCustomerProcessModel.getCustomer();
	}

	protected Converter<UserModel, CustomerData> getCustomerConverter()
	{
		return customerConverter;
	}

	@Required
	public void setCustomerConverter(final Converter<UserModel, CustomerData> customerConverter)
	{
		this.customerConverter = customerConverter;
	}

	public CustomerData getCustomer()
	{
		return customerData;
	}

	@Override
	protected LanguageModel getEmailLanguage(final ReferralNewAppliedEmailProcessModel businessProcessModel)
	{
		return businessProcessModel.getLanguage();
	}

	public String getNumberWithoutZeros(final String format, final double num)
	{
		if (StringUtils.isBlank(format))
		{
			return String.valueOf(num);
		}

		return num % 1 == 0 ? number.format("#", num) : number.format(format, num);

	}

	/**
	 * @return the newAppliedRewardAmount
	 */
	public double getNewAppliedRewardAmount()
	{
		return newAppliedRewardAmount;
	}


	public NumberTool getNumber()
	{
		return this.number;
	}

	public static void main(final String[] arg)
	{
		final NumberTool number = new NumberTool();
	}
}
