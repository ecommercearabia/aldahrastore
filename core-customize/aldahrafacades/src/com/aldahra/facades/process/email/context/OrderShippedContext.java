/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.facades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.beans.factory.annotation.Required;

import com.aldahra.Ordermanagement.model.OrderShippingProcessModel;
import com.aldahra.aldahrareferralcode.model.AppliedReferralHistoryModel;
import com.aldahra.aldahrareferralcode.model.ReferralCodeModel;
import com.aldahra.aldahrareferralcode.service.ReferralCodeCustomerService;
import com.aldahra.aldahrareferralcodefacades.ReferralCodeData;


/**
 * Velocity context for email notification about order consignment.
 *
 * @author mohammad-abumuhasien
 */
public class OrderShippedContext extends AbstractEmailContext<OrderShippingProcessModel>
{
	private Converter<OrderModel, OrderData> orderConverter;
	@Resource(name = "referralCodeCustomerService")
	private ReferralCodeCustomerService referralCodeCustomerService;

	@Resource(name = "referralCodeConverter")
	private Converter<ReferralCodeModel, ReferralCodeData> referralCodeConverter;

	private CustomerData appliedCustomer;

	private double orderAmount;

	private ReferralCodeData referralData;

	private Converter<CustomerModel, CustomerData> customerConverter;

	private NumberTool number;


	@Override
	public void init(final OrderShippingProcessModel orderShippingProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(orderShippingProcessModel, emailPageModel);
		this.number = new NumberTool();
		final OrderModel order = orderShippingProcessModel.getOrder();
		final CustomerModel user = (CustomerModel) order.getUser();
		final Optional<CustomerModel> customerByReferralCode = referralCodeCustomerService
				.getCustomerByReferralCode(user.getAppliedReferralCode());
		if (customerByReferralCode.isPresent())
		{
			put(EMAIL, customerByReferralCode.get().getContactEmail());
			put(TITLE, customerByReferralCode.get().getTitle().getCode());
			put(DISPLAY_NAME, customerByReferralCode.get().getName());
			put(EMAIL_LANGUAGE, customerByReferralCode.get().getSessionLanguage());


		}

		put("order", order);

		appliedCustomer = getAppliedCustomer(order);
		if (!Objects.isNull(user.getAppliedReferralCode()))

		{
			referralData = getReferralData(order);
			orderAmount = getOrderAmount(user);
		}


	}



	@Override
	protected BaseSiteModel getSite(final OrderShippingProcessModel orderShippingProcessModel)
	{
		return orderShippingProcessModel.getOrder().getSite();
	}

	@Override
	protected CustomerModel getCustomer(final OrderShippingProcessModel orderShippingProcessModel)
	{
		final CustomerModel appliedCustomer = (CustomerModel) orderShippingProcessModel.getOrder().getUser();

		if (Objects.isNull(appliedCustomer.getAppliedReferralCode()))
		{
			return null;
		}

		final Optional<CustomerModel> customerByReferralCode = referralCodeCustomerService
				.getCustomerByReferralCode(appliedCustomer.getAppliedReferralCode());
		if (customerByReferralCode.isPresent())
		{
			return customerByReferralCode.get();
		}
		return null;
	}

	protected Converter<OrderModel, OrderData> getOrderConverter()
	{
		return orderConverter;
	}

	@Required
	public void setConsignmentConverter(final Converter<OrderModel, OrderData> orderConverter)
	{
		this.orderConverter = orderConverter;
	}




	/**
	 * @return the appliedCustomer
	 */
	public CustomerData getAppliedCustomer()
	{
		return appliedCustomer;
	}



	/**
	 * @return the orderAmount
	 */
	public double getOrderAmount()
	{
		return orderAmount;
	}



	/**
	 * @return the referralData
	 */
	public ReferralCodeData getReferralData()
	{
		return referralData;
	}



	@Override
	protected LanguageModel getEmailLanguage(final OrderShippingProcessModel orderShippingProcessModel)
	{
		if (Objects.isNull(orderShippingProcessModel.getOrder()))
		{
			return null;
		}
		if (Objects.isNull(orderShippingProcessModel.getOrder().getUser())
				|| !(orderShippingProcessModel.getOrder().getUser() instanceof CustomerModel))
		{
			return null;
		}


		final CustomerModel appliedCustomer = (CustomerModel) orderShippingProcessModel.getOrder().getUser();

		if (Objects.isNull(appliedCustomer.getAppliedReferralCode()))
		{
			return null;
		}

		final Optional<CustomerModel> customerByReferralCode = referralCodeCustomerService
				.getCustomerByReferralCode(appliedCustomer.getAppliedReferralCode());

		if (customerByReferralCode.isPresent())
		{
			return customerByReferralCode.get().getSessionLanguage();
		}
		return orderShippingProcessModel.getOrder().getSite().getDefaultLanguage();
	}

	@Required
	public void setCustomerConverter(final Converter<CustomerModel, CustomerData> customerConverter)
	{
		this.customerConverter = customerConverter;
	}

	protected CustomerData getAppliedCustomer(final AbstractOrderModel order)
	{
		if (Objects.isNull(order.getUser()) || !(order.getUser() instanceof CustomerModel))
		{
			return null;
		}
		return customerConverter.convert((CustomerModel) order.getUser());
	}

	protected ReferralCodeData getReferralData(final AbstractOrderModel order)
	{
		if (Objects.isNull(order.getUser()) || !(order.getUser() instanceof CustomerModel))
		{
			return null;
		}
		return referralCodeConverter.convert(((CustomerModel) order.getUser()).getAppliedReferralCode());

	}

	protected double getOrderAmount(final CustomerModel user)
	{
		if (Objects.isNull(user) || !(user instanceof CustomerModel))
		{
			return 0;
		}


		final List<AppliedReferralHistoryModel> appliedReferralHistory = user.getAppliedReferralCode().getAppliedReferralHistory();
		if (CollectionUtils.isEmpty(appliedReferralHistory))
		{
			return 0;
		}
		final AppliedReferralHistoryModel appliedReferralHistoryModel = appliedReferralHistory
				.get(appliedReferralHistory.size() - 1);
		return appliedReferralHistoryModel.getAmount();
	}

	public String getNumberWithoutZeros(final String format, final double num)
	{
		if (StringUtils.isBlank(format))
		{
			return String.valueOf(num);
		}

		return num % 1 == 0 ? number.format("#", num) : number.format(format, num);

	}

	/**
	 * @return the number
	 */
	public NumberTool getNumber()
	{
		return number;
	}


}
