/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.facades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import com.aldahra.Ordermanagement.model.CustomerMissingAttributesProcessModel;


/**
 * Velocity context for a customer email.
 */
public class CustomerMissingAttributesContext extends AbstractEmailContext<CustomerMissingAttributesProcessModel>
{
	private Converter<UserModel, CustomerData> customerConverter;
	private CustomerData customerData;
	private List<String> missingAttributes;

	@Override
	public void init(final CustomerMissingAttributesProcessModel storeFrontCustomerProcessModel,
			final EmailPageModel emailPageModel)
	{
		super.init(storeFrontCustomerProcessModel, emailPageModel);
		customerData = getCustomerConverter().convert(getCustomer(storeFrontCustomerProcessModel));
	}

	@Override
	protected BaseSiteModel getSite(final CustomerMissingAttributesProcessModel storeFrontCustomerProcessModel)
	{
		return storeFrontCustomerProcessModel.getSite();
	}

	@Override
	protected CustomerModel getCustomer(final CustomerMissingAttributesProcessModel storeFrontCustomerProcessModel)
	{
		return storeFrontCustomerProcessModel.getCustomer();
	}

	protected Converter<UserModel, CustomerData> getCustomerConverter()
	{
		return customerConverter;
	}

	@Required
	public void setCustomerConverter(final Converter<UserModel, CustomerData> customerConverter)
	{
		this.customerConverter = customerConverter;
	}

	public CustomerData getCustomer()
	{
		return customerData;
	}

	@Override
	protected LanguageModel getEmailLanguage(final CustomerMissingAttributesProcessModel businessProcessModel)
	{
		return businessProcessModel.getLanguage();
	}

	/**
	 * @return the missingAttributes
	 */
	public List<String> getMissingAttributes()
	{
		return missingAttributes;
	}

	/**
	 * @param missingAttributes
	 *           the missingAttributes to set
	 */
	public void setMissingAttributes(final List<String> missingAttributes)
	{
		this.missingAttributes = missingAttributes;
	}

}
