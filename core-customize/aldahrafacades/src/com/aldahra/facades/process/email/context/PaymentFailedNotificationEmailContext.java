package com.aldahra.facades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;

import org.springframework.util.StringUtils;

import com.google.common.base.Preconditions;



/**
 * @author abu-muhasien
 */
public class PaymentFailedNotificationEmailContext extends AbstractEmailContext<ConsignmentProcessModel>
{
	private static final String PAYMENT_NOT_CAPTURED_RECEIVER = "paymentnotcaptured.email.receiver";
	private static final String ORDERCODE = "orderCode";
	private String orderCode;

	/**
	 * @return the orderCode
	 */
	protected String getOrderCode()
	{
		return orderCode;
	}

	/**
	 * @param orderCode
	 *           the orderCode to set
	 */
	protected void setOrderCode(final String orderCode)
	{
		this.orderCode = orderCode;
	}

	/**
	 * Inits the.
	 *
	 * @param emailFailedNotificationProcessModel
	 *           the email failed notification process model
	 * @param emailPageModel
	 *           the email page model
	 */
	@Override
	public void init(final ConsignmentProcessModel emailFailedNotificationProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(emailFailedNotificationProcessModel, emailPageModel);
		if (emailFailedNotificationProcessModel.getConsignment() != null
				&& emailFailedNotificationProcessModel.getConsignment().getOrder() != null)
		{
			orderCode = emailFailedNotificationProcessModel.getConsignment().getOrder().getCode();
		}
		final String emailAddress = getConfigurationService().getConfiguration().getString(PAYMENT_NOT_CAPTURED_RECEIVER);
		Preconditions.checkArgument(!StringUtils.isEmpty(emailAddress),
				"Could not send payment not captured email because receiver email address is null.");
		put(EMAIL, emailAddress);
		put(DISPLAY_NAME, "Food Crowd");
		put(ORDERCODE, orderCode);
	}

	/**
	 * Gets the site.
	 *
	 * @param consignmentProcessModel
	 *           the consignment process model
	 * @return the site
	 */
	@Override
	protected BaseSiteModel getSite(final ConsignmentProcessModel consignmentProcessModel)
	{
		return consignmentProcessModel.getConsignment().getOrder().getSite();
	}

	/**
	 * Gets the customer.
	 *
	 * @param consignmentProcessModel
	 *           the consignment process model
	 * @return the customer
	 */
	@Override
	protected CustomerModel getCustomer(final ConsignmentProcessModel consignmentProcessModel)
	{
		return (CustomerModel) consignmentProcessModel.getConsignment().getOrder().getUser();
	}

	/**
	 * Gets the email language.
	 *
	 * @param consignmentProcessModel
	 *           the consignment process model
	 * @return the email language
	 */
	@Override
	protected LanguageModel getEmailLanguage(final ConsignmentProcessModel consignmentProcessModel)
	{
		// may throw ClassCastException
		return ((OrderModel) consignmentProcessModel.getConsignment().getOrder()).getLanguage();
	}

}
