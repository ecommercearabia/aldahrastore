package com.aldahra.facades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.springframework.util.StringUtils;

import com.aldahra.core.model.GenerateErrorEmailProcessModel;
import com.google.common.base.Preconditions;



/**
 * @author abu-muhasien
 */
public class GenerateErrorEmailContext extends AbstractEmailContext<GenerateErrorEmailProcessModel>
{
	private static final String ERROR_MESSAGE = "errorMsg";
	private static final String ORDER_CODE = "orderCode";
	private static final String INTEGRATION_PROVIDER = "provider";
	private String errorMsg;
	private String orderCode;
	private String provider;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;


	@Override
	public void init(final GenerateErrorEmailProcessModel businessProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(businessProcessModel, emailPageModel);
		Preconditions.checkArgument(!StringUtils.isEmpty(businessProcessModel.getErrorMsg()), "Error message must not be null.");
		Preconditions.checkArgument(businessProcessModel.getOrder() != null, "Order must not be null.");
		Preconditions.checkArgument(businessProcessModel.getOrder().getSite() != null, "Site must not be null.");
		Preconditions.checkArgument(businessProcessModel.getIntegrationProvider() != null,
				"Integration provider must not be null.");
		final CMSSiteModel cmsSite = (CMSSiteModel) businessProcessModel.getOrder().getSite();
		Preconditions.checkArgument(!StringUtils.isEmpty(cmsSite.getIntegrationErrorEmailsReceiver()),
				"Integration Error Emails Receiver must not be null.");
		errorMsg = businessProcessModel.getErrorMsg();
		orderCode = businessProcessModel.getOrder().getCode();
		provider = businessProcessModel.getIntegrationProvider().getCode();
		put(EMAIL, cmsSite.getIntegrationErrorEmailsReceiver());
		put(ERROR_MESSAGE, errorMsg);
		put(ORDER_CODE, orderCode);
		put(INTEGRATION_PROVIDER, provider);
	}

	/**
	 * Gets the site.
	 *
	 * @param consignmentProcessModel
	 *           the consignment process model
	 * @return the site
	 */
	@Override
	protected BaseSiteModel getSite(final GenerateErrorEmailProcessModel businessProcessModel)
	{
		return businessProcessModel.getOrder().getSite();
	}

	/**
	 * Gets the customer.
	 *
	 * @param consignmentProcessModel
	 *           the consignment process model
	 * @return the customer
	 */
	@Override
	protected CustomerModel getCustomer(final GenerateErrorEmailProcessModel businessProcessModel)
	{
		return (CustomerModel) businessProcessModel.getOrder().getUser();
	}

	/**
	 * Gets the email language.
	 *
	 * @param consignmentProcessModel
	 *           the consignment process model
	 * @return the email language
	 */
	@Override
	protected LanguageModel getEmailLanguage(final GenerateErrorEmailProcessModel businessProcessModel)
	{
		if (businessProcessModel.getOrder() instanceof OrderModel)
		{
			return ((OrderModel) businessProcessModel.getOrder()).getLanguage();
		}
		return commonI18NService.getAllLanguages().stream().filter(l -> "en".equalsIgnoreCase(l.getIsocode())).findAny().get();
	}

	/**
	 * @return the errorMsg
	 */
	public String getErrorMsg()
	{
		return errorMsg;
	}

	/**
	 * @param errorMsg
	 *           the errorMsg to set
	 */
	public void setErrorMsg(final String errorMsg)
	{
		this.errorMsg = errorMsg;
	}

	/**
	 * @return the orderCode
	 */
	public String getOrderCode()
	{
		return orderCode;
	}

	/**
	 * @param orderCode
	 *           the orderCode to set
	 */
	public void setOrderCode(final String orderCode)
	{
		this.orderCode = orderCode;
	}

	/**
	 * @return the provider
	 */
	public String getProvider()
	{
		return provider;
	}

	/**
	 * @param provider
	 *           the provider to set
	 */
	public void setProvider(final String provider)
	{
		this.provider = provider;
	}
}
