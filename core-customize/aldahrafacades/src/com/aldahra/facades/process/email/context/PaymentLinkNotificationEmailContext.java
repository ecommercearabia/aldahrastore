/**
 *
 */
package com.aldahra.facades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;

import com.aldahra.Ordermanagement.model.PaymentLinkNotificationEmailProcessModel;


/**
 * @author mohammad
 *
 */
public class PaymentLinkNotificationEmailContext extends AbstractEmailContext<PaymentLinkNotificationEmailProcessModel>
{


	@Override
	public void init(final PaymentLinkNotificationEmailProcessModel process, final EmailPageModel emailPageModel)
	{
		super.init(process, emailPageModel);
		put("order", process.getOrder());
		put("paymentLink", process.getPaymentLink());
		put("invoiceId", process.getInvoiceId());
	}

	@Override
	protected BaseSiteModel getSite(final PaymentLinkNotificationEmailProcessModel process)
	{
		return process.getSite();
	}

	@Override
	protected CustomerModel getCustomer(final PaymentLinkNotificationEmailProcessModel process)
	{
		return process.getCustomer();
	}

	@Override
	protected LanguageModel getEmailLanguage(final PaymentLinkNotificationEmailProcessModel process)
	{

		return process.getLanguage();
	}

}
