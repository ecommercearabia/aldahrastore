/**
 *
 */
package com.aldahra.facades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.beans.factory.annotation.Required;

import com.aldahra.Ordermanagement.model.ReferralCodeShareEmailProcessModel;


/**
 * @author mohammad
 *
 */
public class ReferralCodeShareEmailContext extends AbstractEmailContext<ReferralCodeShareEmailProcessModel>
{
	private Converter<UserModel, CustomerData> customerConverter;
	private CustomerData customerData;
	private double newAppliedAmount;
	private NumberTool number;


	@Override
	public void init(final ReferralCodeShareEmailProcessModel referralCodeShareEmailProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(referralCodeShareEmailProcessModel, emailPageModel);
		this.number = new NumberTool();
		customerData = getCustomerConverter().convert(getCustomer(referralCodeShareEmailProcessModel));
		final BaseStoreModel store = referralCodeShareEmailProcessModel.getStore();
		if (!Objects.isNull(store))
		{
			this.newAppliedAmount = store.getReferralCodeNewAppliedRewardAmount();
		 }

	}

	@Override
	protected BaseSiteModel getSite(final ReferralCodeShareEmailProcessModel storeFrontCustomerProcessModel)
	{
		return storeFrontCustomerProcessModel.getSite();
	}

	@Override
	protected CustomerModel getCustomer(final ReferralCodeShareEmailProcessModel storeFrontCustomerProcessModel)
	{
		return storeFrontCustomerProcessModel.getCustomer();
	}

	protected Converter<UserModel, CustomerData> getCustomerConverter()
	{
		return customerConverter;
	}

	@Required
	public void setCustomerConverter(final Converter<UserModel, CustomerData> customerConverter)
	{
		this.customerConverter = customerConverter;
	}

	public CustomerData getCustomer()
	{
		return customerData;
	}

	public String getNumberWithoutZeros(final String format, final double num)
	{
		if (StringUtils.isBlank(format))
		{
			return String.valueOf(num);
		}

		return num % 1 == 0 ? number.format("#", num) : number.format(format, num);

	}
	@Override
	protected LanguageModel getEmailLanguage(final ReferralCodeShareEmailProcessModel businessProcessModel)
	{
		return businessProcessModel.getLanguage();
	}

	/**
	 * @return the newAppliedAmount
	 */
	public double getNewAppliedAmount()
	{
		return newAppliedAmount;
	}

	/**
	 * @return the number
	 */
	public NumberTool getNumber()
	{
		return number;
	}


}
