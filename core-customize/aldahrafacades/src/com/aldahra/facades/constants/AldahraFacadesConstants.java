/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.facades.constants;

/**
 * Global class for all AldahraFacades constants.
 */
public class AldahraFacadesConstants extends GeneratedAldahraFacadesConstants
{
	public static final String EXTENSIONNAME = "aldahrafacades";

	private AldahraFacadesConstants()
	{
		//empty
	}
}
