/**
 *
 */
package com.aldahra.facades.exception;

import com.aldahra.facades.exception.enums.ExpressOrderExceptionType;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class ExpressOrderException extends Exception
{
	private final ExpressOrderExceptionType type;

	private final String[] args;

	protected String[] getArgs()
	{
		return args;
	}

	public ExpressOrderException(final ExpressOrderExceptionType type)
	{
		super(type.getMessage());
		this.type = type;
		this.args = null;
	}

	public ExpressOrderException(final ExpressOrderExceptionType type, final String message)
	{
		super(message);
		this.type = type;
		this.args = null;
	}
	public ExpressOrderException(final ExpressOrderExceptionType type, final String[] args)
	{
		super(type.getMessage());
		this.type = type;
		this.args = args;
	}

	public ExpressOrderException(final ExpressOrderExceptionType type, final String message, final String[] args)
	{
		super(message);
		this.type = type;
		this.args = args;
	}


	public ExpressOrderExceptionType getType()
	{
		return type;
	}

}
