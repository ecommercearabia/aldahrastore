/**
 *
 */
package com.aldahra.facades.exception;

import com.aldahra.facades.exception.enums.CartCouponExceptionType;


/**
 * @author Tuqa
 *
 *
 */
public class CartCouponException extends Exception
{
	private final CartCouponExceptionType type;

	private final String[] args;

	public String[] getArgs()
	{
		return args;
	}

	public CartCouponException(final CartCouponExceptionType type)
	{
		super(type.getMessage());
		this.type = type;
		this.args = null;
	}

	public CartCouponException(final CartCouponExceptionType type, final String message)
	{
		super(message);
		this.type = type;
		this.args = null;
	}

	public CartCouponException(final CartCouponExceptionType type, final String[] args)
	{
		super(type.getMessage());
		this.type = type;
		this.args = args;
	}

	public CartCouponException(final CartCouponExceptionType type, final String message, final String[] args)
	{
		super(message);
		this.type = type;
		this.args = args;
	}


	public CartCouponExceptionType getType()
	{
		return type;
	}

}
