/**
 *
 */
package com.aldahra.facades.exception.enums;

/**
 * @author Tuqa
 *
 */
public enum CartCouponExceptionType
{

	COUPON_FOR_PICKUP("Coupons not available in PickUp in store."), REMOVE_COUPON_OPERATION(
			"Can not remove coupons");

	private String message;

	private CartCouponExceptionType(final String message)
	{
		this.message = message;
	}

	public String getMessage()
	{
		return message;
	}

}
