/**
 *
 */
package com.aldahra.facades.exception.enums;

/**
 * @author husam.dababneh@erabia.com
 *
 */
public enum ExpressOrderExceptionType
{
	PRODUCTS_NOT_EXPRESS("One or more Products are not express"),
	PRODUCTS_MIXED_EXPRESS(
			"Oh! It seems like your cart has mixed items of “express delivery” and others. Please select your preferred delivery time slot."),
	WEIGHT_EXCEEDED("Cart Weight exceeded the limmit"),
	CITY_NOT_EXPRESS("The Selected City is not Available for Express delivery"),
	AREA_NOT_EXPRESS("Selected Area is not Available for Express delivery"),
	TIME_NOT_EXPRESS("Current Time is not Available for Express delivery");

	private String message;

	private ExpressOrderExceptionType(final String message)
	{
		this.message = message;
	}

	public String getMessage()
	{
		return message;
	}

}
