package com.aldahra.facades.populators;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.math.BigDecimal;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.aldahra.core.enums.LoyaltyPointActionType;
import com.aldahra.core.loyaltypoint.data.LoyaltyPointActionTypeData;
import com.aldahra.core.loyaltypoint.data.LoyaltyPointHistoryData;
import com.aldahra.core.model.LoyaltyPointHistoryModel;


public class LoyaltyPointHistoryPopulator implements Populator<LoyaltyPointHistoryModel, LoyaltyPointHistoryData>
{

	@Resource(name = "commerceCommonI18NService")
	private CommerceCommonI18NService commerceCommonI18NService;

	@Resource(name = "priceDataFactory")
	private PriceDataFactory priceDataFactory;

	@Resource(name = "loyaltyPointActionTypeConverter")
	private Converter<LoyaltyPointActionType, LoyaltyPointActionTypeData> loyaltyPointActionTypeConverter;


	/**
	 * @return the loyaltyPointActionTypeConverter
	 */
	protected Converter<LoyaltyPointActionType, LoyaltyPointActionTypeData> getLoyaltyPointActionTypeConverter()
	{
		return loyaltyPointActionTypeConverter;
	}

	/**
	 * @return the commerceCommonI18NService
	 */
	protected CommerceCommonI18NService getCommerceCommonI18NService()
	{
		return commerceCommonI18NService;
	}

	/**
	 * @return the priceDataFactory
	 */
	protected PriceDataFactory getPriceDataFactory()
	{
		return priceDataFactory;
	}

	@Override
	public void populate(final LoyaltyPointHistoryModel source, final LoyaltyPointHistoryData target) throws ConversionException
	{


		Assert.notNull(source, "Parameter LoyaltyPointHistoryModel source cannot be null.");
		Assert.notNull(target, "Parameter LoyaltyPointHistoryData target cannot be null.");

		target.setConvertLimitLoyaltyPoints(source.getConvertLimitLoyaltyPoints());
		target.setProductTOLoyaltyPointsConversion(source.getProductTOLoyaltyPointsConversion());
		target.setLoyaltyPointToStoreCreditConversion(source.getLoyaltyPointToStoreCreditConversion());

		if (source.getActionType() != null)
		{
			target.setActionType(getLoyaltyPointActionTypeConverter().convert(source.getActionType()));
		}

		if (source.getAmount() != null)
		{
			target.setAmount(
					getPriceData(source.getAmount(), PriceDataType.BUY,
					getCommerceCommonI18NService().getDefaultCurrency()));
		}

		target.setDateOfPurchase(source.getDateOfPurchase());
		target.setPoints(source.getPoints());
		target.setBalancePoints(source.getBalancePoints());
		target.setOrderCode(source.getOrderCode());
		target.setReferenceCode(source.getReferenceCode());

	}

	private PriceData getPriceData(final BigDecimal value, final PriceDataType priceType, final CurrencyModel currency)
	{
		if (currency == null)
		{
			return null;
		}
		return priceDataFactory.create(priceType, value, currency);
	}

}
