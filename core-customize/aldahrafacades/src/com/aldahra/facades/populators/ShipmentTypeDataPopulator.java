/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.type.TypeService;

import javax.annotation.Resource;

import com.aldahra.core.enums.ShipmentType;
import com.aldahra.facades.order.data.ShipmentTypeData;
import com.aldahra.facades.product.data.GenderData;


/**
 * Populates {@link GenderData} with name and code.
 */
public class ShipmentTypeDataPopulator implements Populator<ShipmentType, ShipmentTypeData>
{
	@Resource(name = "typeService")
	private TypeService typeService;

	protected TypeService getTypeService()
	{
		return typeService;
	}

	@Override
	public void populate(final ShipmentType source, final ShipmentTypeData target)
	{
		if (source == null || target == null)
		{
			return;
		}
		target.setCode(source.getCode());
		target.setName(getTypeService().getEnumerationValue(source).getName());
	}
}
