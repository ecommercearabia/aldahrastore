package com.aldahra.facades.populators;

import de.hybris.platform.commercefacades.order.converters.populator.OrderPopulator;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;

import org.apache.commons.collections.CollectionUtils;


/**
 * @author mohammed-baker The Class CustomOrderPopulator.
 */
public class CustomOrderPopulator extends OrderPopulator
{

	/**
	 * Check for guest customer.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	@Override
	protected void checkForGuestCustomer(final OrderModel source, final OrderData target)
	{
		if (source.getUser() instanceof CustomerModel)
		{
			super.checkForGuestCustomer(source, target);
		}
	}

	@Override
	protected void addEntries(final AbstractOrderModel source, final AbstractOrderData prototype)
	{
		if (!CollectionUtils.isEmpty(source.getEntries()))
		{
			prototype.setEntries(getOrderEntryConverter().convertAll(source.getEntries()));
		}
	}
}
