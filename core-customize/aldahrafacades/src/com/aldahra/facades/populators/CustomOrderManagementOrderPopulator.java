/**
 *
 */
package com.aldahra.facades.populators;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commercefacades.order.data.DeliveryModeData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderHistoryEntryData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.orderhistory.model.OrderHistoryEntryModel;
import de.hybris.platform.ordermanagementfacades.order.converters.populator.OrdermanagementOrderPopulator;
import de.hybris.platform.ordermanagementfacades.payment.data.PaymentTransactionData;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;


/**
 * @author mohammed-baker
 *
 */
public class CustomOrderManagementOrderPopulator extends OrdermanagementOrderPopulator
{
	@Resource(name = "paymentTransactionConverter")
	private Converter<PaymentTransactionModel, PaymentTransactionData> paymentTransactionConverter;

	@Resource(name = "ordermanagementConsignmentConverter")
	private Converter<ConsignmentModel, ConsignmentData> consignmentConverter;

	@Resource(name = "orderHistoryEntryConverter")
	private Converter<OrderHistoryEntryModel, OrderHistoryEntryData> orderHistoriesConverter;

	@Override
	public void populate(final OrderModel source, final OrderData target) throws ConversionException
	{
		if (source != null && target != null)
		{
			super.populate(source, target);
			populatePaymentTransactions(source, target);
			populateConsignments(source, target);
			populateOrderHistories(source, target);
		}

	}

	@Override
	protected void addDeliveryMethod(final AbstractOrderModel source, final AbstractOrderData prototype)
	{
		final DeliveryModeModel deliveryMode = source.getDeliveryMode();
		if (deliveryMode != null)
		{
			DeliveryModeData deliveryModeData;
			if (deliveryMode instanceof ZoneDeliveryModeModel)
			{
				deliveryModeData = getZoneDeliveryModeConverter().convert((ZoneDeliveryModeModel) deliveryMode);
			}
			else
			{
				deliveryModeData = getDeliveryModeConverter().convert(deliveryMode);
			}
			prototype.setDeliveryMode(deliveryModeData);
		}
	}

	@Override
	protected void addDetails(final OrderModel source, final OrderData target)
	{
		super.addDetails(source, target);
		target.setCreated(source.getCreationtime());
	}

	@Override
	protected void addEntries(final AbstractOrderModel source, final AbstractOrderData prototype)
	{
		if (!CollectionUtils.isEmpty(source.getEntries()))
		{
			prototype.setEntries(getOrderEntryConverter().convertAll(source.getEntries()));
		}
	}

	/**
	 * @param source
	 * @param target
	 */
	private void populatePaymentTransactions(final OrderModel source, final OrderData target)
	{
		if (!CollectionUtils.isEmpty(source.getPaymentTransactions()))
		{
			target.setPaymentTransactions(paymentTransactionConverter.convertAll(source.getPaymentTransactions()));
		}

	}

	/**
	 * @param source
	 * @param target
	 */
	protected void populateConsignments(final OrderModel source, final OrderData target)
	{
		if (!CollectionUtils.isEmpty(source.getConsignments()))
		{
			target.setConsignments(consignmentConverter.convertAll(source.getConsignments()));
		}
	}

	/**
	 * @param source
	 * @param prototype
	 */
	protected void populateOrderHistories(final OrderModel source, final OrderData prototype)
	{
		if (!CollectionUtils.isEmpty(source.getHistoryEntries()))
		{
			prototype.setOrderHistories(orderHistoriesConverter.convertAll(source.getHistoryEntries()));
		}
	}

	/**
	 * Check for guest customer.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	@Override
	protected void checkForGuestCustomer(final OrderModel source, final OrderData target)
	{
		if (source.getUser() instanceof CustomerModel)
		{
			super.checkForGuestCustomer(source, target);
		}
	}

}
