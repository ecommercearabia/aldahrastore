package com.aldahra.facades.populators;

import de.hybris.platform.commercefacades.product.converters.populator.PromotionsPopulator;
import de.hybris.platform.commercefacades.product.data.PromotionData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.jalo.JaloObjectNoLongerValidException;
import de.hybris.platform.promotionengineservices.model.PromotionSourceRuleModel;
import de.hybris.platform.promotionengineservices.model.RuleBasedPromotionModel;
import de.hybris.platform.promotions.model.AbstractPromotionModel;
import de.hybris.platform.promotions.result.PromotionOrderResults;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;


/**
 * Converter implementation for {@link de.hybris.platform.promotions.model.AbstractPromotionModel} as source and
 * {@link de.hybris.platform.commercefacades.product.data.PromotionData} as target type.
 */
public class CustomPromotionsPopulator extends PromotionsPopulator
{
	private static final Logger LOG = LoggerFactory.getLogger(CustomPromotionsPopulator.class);

	@Resource(name = "modelService")
	private ModelService modelService;

	@Override
	public void populate(final AbstractPromotionModel source, final PromotionData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		target.setEnablePromotionCountDownTimer(source.isEnablePromotionCountdownTimer());


		if (source instanceof RuleBasedPromotionModel && ((RuleBasedPromotionModel) source).getRule() != null
				&& ((RuleBasedPromotionModel) source).getRule().getSourceRule() != null &&

				((RuleBasedPromotionModel) source).getRule().getSourceRule() instanceof PromotionSourceRuleModel)
		{
			target.setEnablePromotionCountDownTimer(
					((PromotionSourceRuleModel) ((RuleBasedPromotionModel) source).getRule().getSourceRule())
							.isEnablePromotionCountdownTimer());
		}
		target.setEndDate(source.getEndDate());
		target.setDescription(getPromotionService().getPromotionDescription(source));
		target.setStartDate(source.getStartDate());
		target.setCode(source.getCode());
		target.setPromotionType(source.getPromotionType());
	}

	@Override
	protected void processPromotionMessages(final AbstractPromotionModel source, final PromotionData prototype)
	{
		if (getCartService().hasSessionCart())
		{
			final CartModel cartModel = getCartService().getSessionCart();
			if (cartModel != null)
			{
				PromotionOrderResults promoOrderResults = null;
				try
				{
					promoOrderResults = getPromotionService().getPromotionResults(cartModel);
				}
				catch (final JaloObjectNoLongerValidException e)
				{
					LOG.error(e.getMessage());
					return;
				}

				if (promoOrderResults != null)
				{
					prototype.setCouldFireMessages(getCouldFirePromotionsMessages(promoOrderResults, source));
					prototype.setFiredMessages(getFiredPromotionsMessages(promoOrderResults, source));
				}
			}
		}
	}

}
