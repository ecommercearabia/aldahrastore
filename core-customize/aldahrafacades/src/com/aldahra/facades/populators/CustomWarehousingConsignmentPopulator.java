/**
 *
 */
package com.aldahra.facades.populators;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.commercefacades.order.converters.populator.ConsignmentPopulator;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commercefacades.order.data.DeliveryModeData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.warehousing.model.PackagingInfoModel;
import de.hybris.platform.warehousingfacades.order.data.PackagingInfoData;

import java.util.Objects;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;


/**
 * The Class CustomWarehousingConsignmentPopulator.
 *
 * @author mohammed-baker
 */
public class CustomWarehousingConsignmentPopulator extends ConsignmentPopulator
{

	/** The delivery mode converter. */
	@Resource(name = "deliveryModeConverter")
	private Converter<DeliveryModeModel, DeliveryModeData> deliveryModeConverter;

	/** The packaging info converter. */
	@Resource(name = "ordermanagementPackagingInfoConverter")
	private Converter<PackagingInfoModel, PackagingInfoData> packagingInfoConverter;

	/**
	 * Populate.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	@Override
	public void populate(final ConsignmentModel source, final ConsignmentData target)
	{
		if (source != null && target != null)
		{
			if (!Objects.isNull(source.getOrder()))
			{
				target.setOrderCode(source.getOrder().getCode());
			}
			target.setShippingDate(source.getShippingDate());
			if (!Objects.isNull(source.getWarehouse()))
			{
				target.setWarehouseCode(source.getWarehouse().getCode());
			}
			target.setDeliveryMode(
					source.getDeliveryMode() != null ? deliveryModeConverter.convert(source.getDeliveryMode()) : null);
			target.setPackagingInfo(
					source.getPackagingInfo() != null ? packagingInfoConverter.convert(source.getPackagingInfo()) : null);

			target.setCode(source.getCode());
			target.setTrackingID(source.getTrackingID());
			target.setStatus(source.getStatus());
			if (!CollectionUtils.isEmpty(source.getConsignmentEntries()))
			{
				target.setEntries(Converters.convertAll(source.getConsignmentEntries(), getConsignmentEntryConverter()));
			}
			if (ConsignmentStatus.SHIPPED.equals(source.getStatus())
					|| ConsignmentStatus.READY_FOR_PICKUP.equals(source.getStatus()))
			{
				target.setStatusDate(source.getShippingDate());
			}
			if (source.getDeliveryPointOfService() != null)
			{
				target.setDeliveryPointOfService(getPointOfServiceConverter().convert(source.getDeliveryPointOfService()));
			}
			if (source.getShippingAddress() != null)
			{
				target.setShippingAddress(getAddressConverter().convert(source.getShippingAddress()));
			}
		}
	}

}
