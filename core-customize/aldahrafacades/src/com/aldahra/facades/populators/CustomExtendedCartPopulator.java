/**
 *
 */
package com.aldahra.facades.populators;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
/**
 * @author monzer
 *
 */
public class CustomExtendedCartPopulator extends CustomCartPopulator
{
	@Override
	public void populate(final CartModel source, final CartData target)
	{
		super.populate(source, target);
		addDeliveryAddress(source, target);
		addPaymentInformation(source, target);
		addDeliveryMethod(source, target);
		addPrincipalInformation(source, target);
		addExpress(source, target);
	}

	protected void addExpress(final AbstractOrderModel source, final AbstractOrderData target)
	{
		target.setExpress(true);
	}
}