/**
 *
 */
package com.aldahra.facades.populators;

import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.ordermanagementfacades.payment.data.PaymentTransactionData;
import de.hybris.platform.ordermanagementfacades.payment.data.PaymentTransactionEntryData;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;


/**
 * @author mohammed-baker
 *
 */
public class PaymentTransactionPopulator implements Populator<PaymentTransactionModel, PaymentTransactionData>
{

	@Resource(name = "creditCardPaymentInfoConverter")
	private Converter<CreditCardPaymentInfoModel, CCPaymentInfoData> creditCardPaymentInfoConverter;

	@Resource(name = "ordermanagementPaymentTransactionEntryConverter")
	private Converter<PaymentTransactionEntryModel, PaymentTransactionEntryData> paymentTransactionEntryConverter;

	@Override
	public void populate(final PaymentTransactionModel source, final PaymentTransactionData target) throws ConversionException
	{
		if (source != null && target != null)
		{
			target.setCode(source.getCode());
			target.setRequestId(source.getRequestId());
			target.setRequestToken(source.getRequestToken());
			target.setPaymentProvider(source.getPaymentProvider());
			target.setPlannedAmount(source.getPlannedAmount());
			if (source.getCurrency() != null)
			{
				target.setCurrencyIsocode(source.getCurrency().getIsocode());

			}
			target.setVersionID(source.getVersionID());

			if (source.getInfo() != null && source.getInfo() instanceof CreditCardPaymentInfoModel)
			{
				target.setPaymentInfo(creditCardPaymentInfoConverter.convert((CreditCardPaymentInfoModel) source.getInfo()));
			}
			if (CollectionUtils.isEmpty(source.getEntries()))
			{
				target.setEntries(paymentTransactionEntryConverter.convertAll(source.getEntries()));
			}
		}
	}

}
