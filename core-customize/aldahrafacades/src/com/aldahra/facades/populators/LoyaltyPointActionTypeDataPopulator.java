/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.type.TypeService;

import javax.annotation.Resource;

import com.aldahra.core.enums.LoyaltyPointActionType;
import com.aldahra.core.loyaltypoint.data.LoyaltyPointActionTypeData;
import com.aldahra.facades.product.data.GenderData;


/**
 * Populates {@link GenderData} with name and code.
 */
public class LoyaltyPointActionTypeDataPopulator implements Populator<LoyaltyPointActionType, LoyaltyPointActionTypeData>
{
	@Resource(name = "typeService")
	private TypeService typeService;

	protected TypeService getTypeService()
	{
		return typeService;
	}

	@Override
	public void populate(final LoyaltyPointActionType source, final LoyaltyPointActionTypeData target)
	{
		if (source == null || target == null)
		{
			return;
		}
		target.setCode(source.getCode());
		target.setName(getTypeService().getEnumerationValue(source).getName());
	}
}
