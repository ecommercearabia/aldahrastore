/**
 *
 */
package com.aldahra.facades.populators;

import de.hybris.platform.commercefacades.order.data.ConsignmentEntryData;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.warehousingfacades.order.converters.populator.WarehousingConsignmentEntryPopulator;

import java.util.Objects;


/**
 * @author mohammed-baker
 *
 */
public class CustomWarehousingConsignmentEntryPopulator extends WarehousingConsignmentEntryPopulator
{
	@Override
	public void populate(final ConsignmentEntryModel source, final ConsignmentEntryData target)
	{
		if (source != null && target != null)
		{
			target.setQuantityDeclined(source.getQuantityDeclined());
			target.setQuantityPending(source.getQuantityPending());
			target.setQuantityShipped(source.getQuantityShipped());
			target.setQuantity(source.getQuantity());
			target.setShippedQuantity(source.getShippedQuantity());
			if (!Objects.isNull(source.getOrderEntry()))
			{
				target.setOrderEntry(getOrderEntryConverter().convert(source.getOrderEntry()));
			}

		}
	}

}
