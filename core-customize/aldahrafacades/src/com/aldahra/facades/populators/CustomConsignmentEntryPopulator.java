/**
 *
 */
package com.aldahra.facades.populators;

import de.hybris.platform.commercefacades.order.converters.populator.ConsignmentEntryPopulator;
import de.hybris.platform.commercefacades.order.data.ConsignmentEntryData;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;

import java.util.Objects;

import org.springframework.util.Assert;


/**
 * @author mohammed-baker
 *
 */
public class CustomConsignmentEntryPopulator extends ConsignmentEntryPopulator
{

	@Override
	public void populate(final ConsignmentEntryModel source, final ConsignmentEntryData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setQuantity(source.getQuantity());
		target.setShippedQuantity(source.getShippedQuantity());
		if (!Objects.isNull(source.getOrderEntry()))
		{
			target.setOrderEntry(getOrderEntryConverter().convert(source.getOrderEntry()));
		}

	}
}
