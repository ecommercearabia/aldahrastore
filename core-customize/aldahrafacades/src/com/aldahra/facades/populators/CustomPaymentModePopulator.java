/**
 *
 */
package com.aldahra.facades.populators;

import de.hybris.platform.commercefacades.order.converters.populator.PaymentModePopulator;
import de.hybris.platform.commercefacades.order.data.PaymentModeData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;

import org.springframework.util.Assert;


/**
 * @author monzer
 *
 */
public class CustomPaymentModePopulator extends PaymentModePopulator implements Populator<PaymentModeModel, PaymentModeData>
{

	@Override
	public void populate(final PaymentModeModel source, final PaymentModeData target) throws ConversionException
	{
		super.populate(source, target);

		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setActive(source.getActive());
		target.setActiveForOCC(source.isActiveForOCC());
		target.setActiveForStorefront(source.isActiveForStorefront());
		target.setDeviceType(new ArrayList<>(source.getSupporttedDevice()));
	}

}
