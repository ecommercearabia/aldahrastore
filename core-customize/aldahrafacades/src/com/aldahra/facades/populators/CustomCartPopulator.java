package com.aldahra.facades.populators;

import de.hybris.platform.commercefacades.order.converters.populator.CartPopulator;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.product.data.PromotionResultData;
import de.hybris.platform.promotions.jalo.PromotionResult;
import de.hybris.platform.promotions.model.PromotionResultModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;


/**
 * @author monzer
 *
 */
public class CustomCartPopulator extends CartPopulator<CartData>
{
	private static final Logger LOG = LoggerFactory.getLogger(CustomCartPopulator.class);

	@Override
	protected List<PromotionResultData> getPromotions(final List<PromotionResult> promotionsResults)
	{
		final List<PromotionResultModel> promotionResultModels = new ArrayList<>();
		for (final PromotionResult promotionResult : promotionsResults)
		{
			try
			{
				final PromotionResultModel object = (PromotionResultModel) getModelService().get(promotionResult);
				promotionResultModels.add(object);
			}
			catch (final Exception e)
			{
				LOG.error("Error during getting the promotion results : " + e.getMessage());
			}
		}
		if (CollectionUtils.isEmpty(promotionResultModels))
		{
			return Collections.emptyList();
		}
		return getPromotionResultConverter().convertAll(promotionResultModels);
	}
}