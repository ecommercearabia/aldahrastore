package com.aldahra.facades.populators;

import de.hybris.platform.commercefacades.user.data.DeliveryModeTypeData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.type.TypeService;

import javax.annotation.Resource;

import com.aldahra.core.enums.DeliveryModeType;



/**
 * @author mnasro
 *
 */
public class DeliveryModeTypePopulator implements Populator<DeliveryModeType, DeliveryModeTypeData>
{
	@Resource(name = "typeService")
	private TypeService typeService;

	@Override
	public void populate(final DeliveryModeType source, final DeliveryModeTypeData target)
	{
		target.setCode(source.getCode());
		target.setName(typeService.getEnumerationValue(source).getName());
	}
}