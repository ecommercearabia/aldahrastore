package com.aldahra.facades.populators;

import de.hybris.platform.commercefacades.order.converters.populator.CommerceCartParameterBasicPopulator;
import de.hybris.platform.commercefacades.order.data.AddToCartParams;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import org.apache.commons.lang3.StringUtils;

import com.aldahra.core.enums.ShipmentType;


public class CustomCommerceCartParameterBasicPopulator extends CommerceCartParameterBasicPopulator
{

	@Override
	public void populate(final AddToCartParams source, final CommerceCartParameter target) throws ConversionException
	{
		super.populate(source, target);
		populateShipmentType(source, target);
	}

	/**
	 * @param source
	 * @param target
	 */
	private void populateShipmentType(final AddToCartParams source, final CommerceCartParameter target)
	{
		if (source == null || target == null)
		{
			return;
		}

		if (!StringUtils.isBlank(source.getShipmentType()))
		{
			target.setShipmentType(getShipmentType(source.getShipmentType()));
			return;
		}

		if (target.getCart() == null || target.getCart().getSite() == null || !target.getCart().getSite().isPickUpEnabled()
				|| ShipmentType.DELIVERY.equals(target.getCart().getShipmentType())
				|| target.getCart().getSite().getDefaultShipmentType() == null
				|| target.getCart().getSite().getDefaultDeliveryPointOfService() == null)
		{
			target.setShipmentType(ShipmentType.DELIVERY);
			return;
		}

		if (target.getCart().getShipmentType() != null && ShipmentType.PICKUP_IN_STORE.equals(target.getCart().getShipmentType()))
		{
			target.setShipmentType(ShipmentType.PICKUP_IN_STORE);
		}
		else
		{
			target.setShipmentType(target.getCart().getSite().getDefaultShipmentType());
		}


		if (ShipmentType.PICKUP_IN_STORE.equals(target.getShipmentType()))
		{
			final PointOfServiceModel pointOfService = target.getPointOfService() != null ? target.getPointOfService()
					: target.getCart().getSite().getDefaultDeliveryPointOfService();
			target.setPointOfService(pointOfService);

		}

	}

	/**
	 * @param shipmentType
	 * @return
	 */
	private ShipmentType getShipmentType(final String shipmentType)
	{
		try
		{
			return ShipmentType.valueOf(shipmentType);
		}
		catch (final Exception e)
		{
			return null;
		}
	}

}
