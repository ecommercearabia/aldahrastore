/**
 *
 */
package com.aldahra.facades.populators;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Objects;


/**
 * @author mohammedbaker
 *
 */
public class ProductSeoDescriptionPopulator implements Populator<ProductModel, ProductData>
{

	@Override
	public void populate(final ProductModel source, final ProductData target) throws ConversionException
	{
		if (!Objects.isNull(source) && !Objects.isNull(target))
		{

			target.setSeoDescription(source.getSeoDescription());
		}

	}

}
