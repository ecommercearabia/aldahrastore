/**
 *
 */
package com.aldahra.facades.populators;

import de.hybris.platform.commercefacades.order.data.OrderHistoryEntryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.orderhistory.model.OrderHistoryEntryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Objects;


/**
 * @author mohammed-baker
 *
 */
public class OrderHistoryEntryPopulator implements Populator<OrderHistoryEntryModel, OrderHistoryEntryData>
{

	@Override
	public void populate(final OrderHistoryEntryModel source, final OrderHistoryEntryData target) throws ConversionException
	{
		if (!Objects.isNull(source) && !Objects.isNull(target))
		{
			target.setDescription(source.getDescription());
			target.setTimestamp(source.getTimestamp().toString());
		}

	}

}
