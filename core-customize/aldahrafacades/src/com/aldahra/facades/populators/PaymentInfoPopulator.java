/**
 *
 */
package com.aldahra.facades.populators;

import de.hybris.platform.acceleratorservices.payment.data.PaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.NoCardPaymentInfoData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.NoCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Resource;


/**
 * @author mohammed-baker
 *
 */
public class PaymentInfoPopulator implements Populator<PaymentInfoModel, PaymentInfoData>
{

	@Resource(name = "noCardPaymentInfoPopulator")
	private Populator<NoCardPaymentInfoModel, NoCardPaymentInfoData> noCardPaymentInfoPopulator;
	@Resource(name = "creditCardPaymentInfoPopulator")
	private Populator<CreditCardPaymentInfoModel, CCPaymentInfoData> creditCardPaymentInfoPopulator;

	@Override
	public void populate(final PaymentInfoModel source, final PaymentInfoData target) throws ConversionException
	{
		if (source != null && target != null)
		{
			if (source instanceof CreditCardPaymentInfoModel)
			{
				creditCardPaymentInfoPopulator.populate((CreditCardPaymentInfoModel) source, new CCPaymentInfoData());
			}
			else if (source instanceof NoCardPaymentInfoModel)
			{
				noCardPaymentInfoPopulator.populate((NoCardPaymentInfoModel) source, new NoCardPaymentInfoData());
			}
		}
	}

}
