/**
 *
 */
package com.aldahra.facades.populators;



import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import com.aldahra.core.loyaltypoint.data.LoyaltyPointHistoryData;
import com.aldahra.core.model.LoyaltyPointHistoryModel;



/**
 * @author Tuqa
 *
 */
public class GeneralCustomerPopulator implements Populator<CustomerModel, CustomerData>
{
	@Resource(name = "loyaltyPointHistoryConverter")
	private Converter<LoyaltyPointHistoryModel, LoyaltyPointHistoryData> loyaltyPointHistoryConverter;

	/**
	 * @return the loyaltyPointHistoryConverter
	 */
	protected Converter<LoyaltyPointHistoryModel, LoyaltyPointHistoryData> getLoyaltyPointHistoryConverter()
	{
		return loyaltyPointHistoryConverter;
	}

	@Override
	public void populate(final CustomerModel source, final CustomerData target) throws ConversionException
	{
		Assert.notNull(target, "Target can not be null");
		Assert.notNull(source, "Source can not be null");
		populateLoyaltyPoints(source, target);
		populateLoyaltyPointHistory(source, target);

	}

	/**
	 * @param source
	 * @param target
	 */
	private void populateLoyaltyPoints(final CustomerModel source, final CustomerData target)
	{
		target.setLoyaltyPoints(source.getLoyaltyPoints());
	}

	/**
	 * @param source
	 * @param target
	 */
	private void populateLoyaltyPointHistory(final CustomerModel source, final CustomerData target)
	{

		if (CollectionUtils.isEmpty(source.getLoyaltyPointHistory()))
		{
			return;
		}

		target.setLoyaltyPointHistory(getLoyaltyPointHistoryConverter().convertAll(source.getLoyaltyPointHistory()));
	}




}
