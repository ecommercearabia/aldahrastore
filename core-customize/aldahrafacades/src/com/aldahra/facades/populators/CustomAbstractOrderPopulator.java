package com.aldahra.facades.populators;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;

import com.aldahra.core.cart.data.CartTimeSlotData;
import com.aldahra.core.enums.ShipmentType;
import com.aldahra.facades.exception.ExpressOrderException;
import com.aldahra.facades.facade.CustomAcceleratorCheckoutFacade;
import com.aldahra.facades.facade.CustomCartFacade;
import com.aldahra.facades.order.data.ShipmentTypeData;


/**
 *
 * The Class CustomAbstractOrderPopulator.
 *
 * @author mnasro
 *
 * @param <S>
 *           SOURCE, the generic type extends AbstractOrderModel
 * @param <T>
 *           TARGET, the generic type extends AbstractOrderData
 */
public class CustomAbstractOrderPopulator<S extends AbstractOrderModel, T extends AbstractOrderData> implements Populator<S, T>
{

	@Resource(name = "shipmentTypeConverter")
	private Converter<ShipmentType, ShipmentTypeData> shipmentTypeConverter;

	@Resource(name = "pointOfServiceConverter")
	private Converter<PointOfServiceModel, PointOfServiceData> pointOfServiceConverter;

	@Resource(name = "customCartFacadeImpl")
	private CustomCartFacade customCartFacade;

	@Resource(name = "acceleratorCheckoutFacade")
	private CustomAcceleratorCheckoutFacade checkoutFacade;


	/**
	 * @return the checkoutFacade
	 */
	protected CustomAcceleratorCheckoutFacade getCheckoutFacade()
	{
		return checkoutFacade;
	}

	/**
	 * @return the customCartFacade
	 */
	protected CustomCartFacade getCustomCartFacade()
	{
		return customCartFacade;
	}

	/**
	 * @return the pointOfServiceConverter
	 */
	protected Converter<PointOfServiceModel, PointOfServiceData> getPointOfServiceConverter()
	{
		return pointOfServiceConverter;
	}

	/**
	 * @return the shipmentTypeConverter
	 */
	public Converter<ShipmentType, ShipmentTypeData> getShipmentTypeConverter()
	{
		return shipmentTypeConverter;
	}

	/**
	 * Populate.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	@Override
	public void populate(final AbstractOrderModel source, final AbstractOrderData target)
	{
		if (source == null || target == null)
		{
			return;
		}

		target.setDeliveryType(source.getDeliveryType());
		populateExpress(source, target);
		populateDeliveryPointOfService(source, target);
		populateShipmentType(source, target);
		populateCartTimeSlot(source, target);
	}

	/**
	 * @param source
	 * @param target
	 */
	private void populateExpress(final AbstractOrderModel source, final AbstractOrderData target)
	{
		if (source instanceof CartModel)
		{
			try
			{
				target.setExpress(getCheckoutFacade().isExpressOrder());
			}
			catch (final ExpressOrderException e)
			{
				target.setExpress(false);
			}
			return;
		}

		target.setExpress(source.isExpress());


	}

	/**
	 * @param source
	 * @param target
	 */
	private void populateCartTimeSlot(final AbstractOrderModel source, final AbstractOrderData target)
	{
		if (!(source instanceof CartModel))
		{
			return;
		}
		final CartTimeSlotData cartTimeSlotData = getCustomCartFacade().getCartTimeSlotData((CartModel) source);
		target.setCartTimeSlot(cartTimeSlotData);
	}

	/**
	 * @param source
	 * @param target
	 */
	private void populateShipmentType(final AbstractOrderModel source, final AbstractOrderData target)
	{
		target.setShipmentType(getShipmentTypeConverter().convert(source.getShipmentType()));
	}

	/**
	 * @param source
	 * @param target
	 */
	protected void populateDeliveryPointOfService(final AbstractOrderModel source, final AbstractOrderData target)
	{
		if (CollectionUtils.isEmpty(source.getEntries()) || !ShipmentType.PICKUP_IN_STORE.equals(source.getShipmentType()))
		{
			return;
		}

		final PointOfServiceModel deliveryPointOfService = getFirstDeliveryPointOfService(source.getEntries());

		if (deliveryPointOfService == null)
		{
			return;
		}

		target.setDeliveryPointOfService(getPointOfServiceConverter().convert(deliveryPointOfService));
	}

	/**
	 * @param entries
	 * @return
	 */
	private PointOfServiceModel getFirstDeliveryPointOfService(final List<AbstractOrderEntryModel> entries)
	{
		for (final AbstractOrderEntryModel abstractOrderEntryModel : entries)
		{
			if (abstractOrderEntryModel.getDeliveryPointOfService() != null)
			{
				return abstractOrderEntryModel.getDeliveryPointOfService();
			}
		}
		return null;
	}

}
