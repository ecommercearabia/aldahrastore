/**
 *
 */
package com.aldahra.facades;

import static org.junit.Assert.assertEquals;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.basecommerce.enums.PointOfServiceTypeEnum;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.dao.CommerceCartDao;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.aldahra.core.enums.ShipmentType;
import com.aldahra.facades.facade.CustomCartFacade;


/**
 * @author mohammad.jaradat@erabia.com
 *
 */
@IntegrationTest
public class DefaultCustomCartFacadeTest extends ServicelayerTransactionalTest
{

	private static final String TEST_BASESITE_UID = "testSite";
	private static final String USER = "john";

	private CartModel cartModel;

	private UserModel user;

	private CMSSiteModel site;

	private BaseSiteModel currentBaseSite;

	private PointOfServiceModel pointOfservice;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "cartFacade")
	private CustomCartFacade cartFacade;

	@Resource(name = "commerceCartDao")
	private CommerceCartDao commerceCartDao;

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource
	private BaseSiteService baseSiteService;

	@Before
	public void setUp() throws ImpExException
	{
		importCsv("/aldahrafacades/test/testCommerceCart.csv", "utf-8");

		cartModel = modelService.create(CartModel.class);
		user = modelService.create(UserModel.class);
		final CurrencyModel currency = modelService.create(CurrencyModel.class);
		currency.setActive(true);
		currency.setIsocode("AED");
		site = modelService.create(CMSSiteModel.class);
		site.setActive(true);
		user.setName("moe");
		user.setUid("moe");
		cartModel.setUser(user);
		cartModel.setCurrency(currency);
		cartModel.setDate(new Date());
		cartModel.setCode("testCart");

		baseSiteService.setCurrentBaseSite(baseSiteService.getBaseSiteForUID(TEST_BASESITE_UID), false);
		currentBaseSite = baseSiteService.getCurrentBaseSite();
		final UserModel userModel = userService.getUserForUID(USER);
		userService.setCurrentUser(userModel);
		pointOfservice = modelService.create(PointOfServiceModel.class);
		pointOfservice.setName("test point of service");
		pointOfservice.setType(PointOfServiceTypeEnum.POS);
		modelService.save(pointOfservice);
		currentBaseSite.setDefaultDeliveryPointOfService(pointOfservice);


	}


	@Test
	public void addNewCart()
	{
		modelService.save(cartModel);
		assertEquals(cartModel, commerceCartDao.getCartForCodeAndUser("testCart", user));

	}

	@Test
	public void addNewCartEntriesWhileShippmentTypeIsPickup()
	{

		final PointOfServiceModel pointOfservice = modelService.create(PointOfServiceModel.class);
		site.setPickUpEnabled(true);
		site.setDefaultDeliveryPointOfService(pointOfservice);
		site.setDefaultShipmentType(ShipmentType.PICKUP_IN_STORE);
		cartModel.setSite(site);

		final AbstractOrderEntryModel abstractOrderEntryModel = new AbstractOrderEntryModel();
		final ProductModel productModel = new ProductModel();
		final ProductModel productModel2 = new ProductModel();

		abstractOrderEntryModel.setProduct(productModel);
		abstractOrderEntryModel.setProduct(productModel2);
		final List<AbstractOrderEntryModel> list = new ArrayList<>();
		cartModel.setEntries(list);

		for (final AbstractOrderEntryModel abstractOrderEntryModel2 : cartModel.getEntries())
		{
			assertEquals(abstractOrderEntryModel2.getDeliveryPointOfService(), pointOfservice);
		}

	}

	@Test
	public void addNewCartEntriesWhileShippmentTypeIsPickupUsingFacade() throws CommerceCartModificationException
	{


		final CartModel userCart = cartService.getSessionCart();


		currentBaseSite.setDefaultShipmentType(ShipmentType.PICKUP_IN_STORE);
		currentBaseSite.setPickUpEnabled(true);

		modelService.save(userCart);
		final ShipmentType shipmentType = null;

		cartFacade.addToCart("HW1210-3422", 1, shipmentType);


		for (final AbstractOrderEntryModel abstractOrderEntryModel : userCart.getEntries())
		{
			assertEquals(abstractOrderEntryModel.getDeliveryPointOfService(), pointOfservice);
		}


	}

	@Test
	public void addNewCartEntriesWhileShippmentTypeIsDelivery()
	{

		final PointOfServiceModel pointOfservice = modelService.create(PointOfServiceModel.class);
		site.setPickUpEnabled(true);
		site.setDefaultDeliveryPointOfService(pointOfservice);
		site.setDefaultShipmentType(ShipmentType.DELIVERY);
		cartModel.setSite(site);

		final AbstractOrderEntryModel abstractOrderEntryModel = new AbstractOrderEntryModel();
		final ProductModel productModel = new ProductModel();
		final ProductModel productModel2 = new ProductModel();

		abstractOrderEntryModel.setProduct(productModel);
		abstractOrderEntryModel.setProduct(productModel2);
		final List<AbstractOrderEntryModel> list = new ArrayList<>();
		cartModel.setEntries(list);

		for (final AbstractOrderEntryModel abstractOrderEntryModel2 : cartModel.getEntries())
		{
			assertEquals(abstractOrderEntryModel2.getDeliveryPointOfService(), null);
		}

	}


	@Test
	public void addNewCartEntriesWhileShippmentTypeIsDeliveryUsingFacade() throws CommerceCartModificationException
	{

		final CartModel userCart = cartService.getSessionCart();

		currentBaseSite.setDefaultShipmentType(ShipmentType.DELIVERY);
		currentBaseSite.setPickUpEnabled(true);
		modelService.save(userCart);
		final ShipmentType shipmentType = null;

		cartFacade.addToCart("HW1210-3422", 1, shipmentType);
		cartFacade.addToCart("HW1210-3423", 2, shipmentType);
		cartFacade.addToCart("HW1210-3424", 5, shipmentType);

		for (final AbstractOrderEntryModel abstractOrderEntryModel : userCart.getEntries())
		{
			assertEquals(abstractOrderEntryModel.getDeliveryPointOfService(), null);
		}


	}

	@Test
	public void addNewCartEntriesWhilePickUpIsNotEnabled() throws CommerceCartModificationException
	{

		final CartModel userCart = cartService.getSessionCart();

		currentBaseSite.setDefaultShipmentType(ShipmentType.PICKUP_IN_STORE);
		currentBaseSite.setPickUpEnabled(false);
		modelService.save(userCart);
		final ShipmentType shipmentType = null;

		cartFacade.addToCart("HW1210-3422", 1, shipmentType);
		cartFacade.addToCart("HW1210-3423", 2, shipmentType);
		cartFacade.addToCart("HW1210-3424", 5, shipmentType);

		for (final AbstractOrderEntryModel abstractOrderEntryModel : userCart.getEntries())
		{
			assertEquals(abstractOrderEntryModel.getDeliveryPointOfService(), null);
		}


	}

	@Test
	public void updateShipmentTypeForAllCartEntryTest() throws CommerceCartModificationException
	{
		final CartModel userCart = cartService.getSessionCart();

		currentBaseSite.setDefaultShipmentType(ShipmentType.PICKUP_IN_STORE);
		currentBaseSite.setPickUpEnabled(true);
		modelService.save(userCart);
		final ShipmentType shipmentType = null;

		cartFacade.addToCart("HW1210-3422", 1, shipmentType);
		cartFacade.addToCart("HW1210-3423", 2, shipmentType);
		cartFacade.addToCart("HW1210-3424", 5, shipmentType);

		for (final AbstractOrderEntryModel abstractOrderEntryModel : userCart.getEntries())
		{
			assertEquals(abstractOrderEntryModel.getDeliveryPointOfService(), pointOfservice);
		}

		cartFacade.updateShipmentTypeForAllCartEntry(ShipmentType.DELIVERY);

		for (final AbstractOrderEntryModel abstractOrderEntryModel : userCart.getEntries())
		{
			assertEquals(abstractOrderEntryModel.getDeliveryPointOfService(), null);
		}

		cartFacade.updateShipmentTypeForAllCartEntry(ShipmentType.PICKUP_IN_STORE);

		for (final AbstractOrderEntryModel abstractOrderEntryModel : userCart.getEntries())
		{
			assertEquals(abstractOrderEntryModel.getDeliveryPointOfService(), pointOfservice);
		}
	}



	@Test
	public void getCurrentShipmentType() throws CommerceCartModificationException
	{
		final CartModel userCart = cartService.getSessionCart();

		currentBaseSite.setDefaultShipmentType(ShipmentType.DELIVERY);
		currentBaseSite.setPickUpEnabled(true);
		modelService.save(userCart);

		final ShipmentType shipmentType = null;

		cartFacade.addToCart("HW1210-3422", 1, shipmentType);
		cartFacade.addToCart("HW1210-3423", 2, shipmentType);
		cartFacade.addToCart("HW1210-3424", 5, shipmentType);
		assertEquals(cartFacade.getCurrentShipmentType().getCode(), ShipmentType.DELIVERY.getCode());


	}




}
