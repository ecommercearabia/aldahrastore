/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraconsignmenttrackingocccustomaddon.controllers;

/**
 */
public interface AldahraconsignmenttrackingocccustomaddonControllerConstants
{
	// implement here controller constants used by this extension
}
