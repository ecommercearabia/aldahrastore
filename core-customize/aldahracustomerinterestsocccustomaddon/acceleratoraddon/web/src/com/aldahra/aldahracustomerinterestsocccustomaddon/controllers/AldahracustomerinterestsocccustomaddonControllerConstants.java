/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahracustomerinterestsocccustomaddon.controllers;

/**
 */
public interface AldahracustomerinterestsocccustomaddonControllerConstants
{
	// implement here controller constants used by this extension
}
