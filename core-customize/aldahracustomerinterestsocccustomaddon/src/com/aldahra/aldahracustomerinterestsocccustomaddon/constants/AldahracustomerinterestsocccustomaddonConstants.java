/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahracustomerinterestsocccustomaddon.constants;

/**
 * Global class for all aldahracustomerinterestsocccustomaddon constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings(
{ "deprecation", "squid:CallToDeprecatedMethod" })
public final class AldahracustomerinterestsocccustomaddonConstants extends GeneratedAldahracustomerinterestsocccustomaddonConstants
{
	public static final String EXTENSIONNAME = "aldahracustomerinterestsocccustomaddon"; //NOSONAR

	private AldahracustomerinterestsocccustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
