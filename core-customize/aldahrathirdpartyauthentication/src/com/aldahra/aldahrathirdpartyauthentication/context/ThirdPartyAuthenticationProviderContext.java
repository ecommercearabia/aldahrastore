/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrathirdpartyauthentication.context;

import java.util.Optional;

import com.aldahra.aldahrathirdpartyauthentication.model.ThirdPartyAuthenticationProviderModel;


/**
 *
 */
public interface ThirdPartyAuthenticationProviderContext
{
	public Optional<ThirdPartyAuthenticationProviderModel> getThirdPartyAuthenticationProvider(Class<?> providerClass);
}
