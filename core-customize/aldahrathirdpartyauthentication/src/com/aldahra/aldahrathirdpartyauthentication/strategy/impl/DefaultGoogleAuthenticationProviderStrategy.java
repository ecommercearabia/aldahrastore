/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrathirdpartyauthentication.strategy.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;

import java.util.Optional;

import javax.annotation.Resource;

import com.aldahra.aldahrathirdpartyauthentication.model.GoogleAuthenticationProviderModel;
import com.aldahra.aldahrathirdpartyauthentication.model.ThirdPartyAuthenticationProviderModel;
import com.aldahra.aldahrathirdpartyauthentication.service.ThirdPartyAuthenticationProviderService;
import com.aldahra.aldahrathirdpartyauthentication.strategy.AuthenticationProviderStrategy;

/**
 *
 */
public class DefaultGoogleAuthenticationProviderStrategy implements AuthenticationProviderStrategy
{
	@Resource(name = "thirdPartyAuthenticationProviderService")
	private ThirdPartyAuthenticationProviderService thirdPartyAuthenticationProviderService;

	@Override
	public Optional<ThirdPartyAuthenticationProviderModel> getActiveProvider(final String cmsSiteUid)
	{
		return getThirdPartyAuthenticationProviderService().get(cmsSiteUid, GoogleAuthenticationProviderModel.class);
	}

	@Override
	public Optional<ThirdPartyAuthenticationProviderModel> getActiveProvider(final CMSSiteModel cmsSiteModel)
	{
		return getThirdPartyAuthenticationProviderService().getActive(cmsSiteModel, GoogleAuthenticationProviderModel.class);
	}

	@Override
	public Optional<ThirdPartyAuthenticationProviderModel> getActiveProviderByCurrentSite()
	{
		return getThirdPartyAuthenticationProviderService().getActiveByCurrentSite(GoogleAuthenticationProviderModel.class);
	}

	public ThirdPartyAuthenticationProviderService getThirdPartyAuthenticationProviderService()
	{
		return thirdPartyAuthenticationProviderService;
	}
}

