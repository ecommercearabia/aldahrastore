/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrathirdpartyauthentication.strategy;

import de.hybris.platform.cms2.model.site.CMSSiteModel;

import java.util.Optional;

import com.aldahra.aldahrathirdpartyauthentication.model.ThirdPartyAuthenticationProviderModel;

/**
 *
 */
public interface AuthenticationProviderStrategy
{
	/**
	 *
	 */
	public Optional<ThirdPartyAuthenticationProviderModel> getActiveProvider(String cmsSiteUid);

	/**
	 *
	 */
	public Optional<ThirdPartyAuthenticationProviderModel> getActiveProvider(CMSSiteModel cmsSiteModel);

	/**
	 *
	 */
	public Optional<ThirdPartyAuthenticationProviderModel> getActiveProviderByCurrentSite();
}
