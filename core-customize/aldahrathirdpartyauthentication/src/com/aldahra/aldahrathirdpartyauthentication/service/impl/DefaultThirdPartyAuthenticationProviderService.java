/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrathirdpartyauthentication.service.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;

import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.aldahra.aldahrathirdpartyauthentication.dao.ThirdPartyAuthenticationProviderDao;
import com.aldahra.aldahrathirdpartyauthentication.model.ThirdPartyAuthenticationProviderModel;
import com.aldahra.aldahrathirdpartyauthentication.service.ThirdPartyAuthenticationProviderService;
import com.google.common.base.Preconditions;
/**
 *
 */
public class DefaultThirdPartyAuthenticationProviderService implements ThirdPartyAuthenticationProviderService
{
	@Resource(name = "thirdPartyAuthenticationProviderDaoMap")
	private Map<Class<?>, ThirdPartyAuthenticationProviderDao> thirdPartyAuthenticationProviderDaoMap;
	private static final String CODE_MUSTN_T_BE_NULL = "code mustn't be null or empty";
	private static final String CMSSITE_UID_MUSTN_T_BE_NULL = "cmsSiteUid mustn't be null or empty";
	private static final String CMSSITE_MUSTN_T_BE_NULL = "cmsSiteUid mustn't be null or empty";
	private static final String PROVIDER_CLASS_MUSTN_T_BE_NULL = "providerClass mustn't be null";
	private static final String PROVIDER_DAO_NOT_FOUND = "dao not found";
	@Override
	public Optional<ThirdPartyAuthenticationProviderModel> get(final String code, final Class<?> providerClass)
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(code), CODE_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<ThirdPartyAuthenticationProviderDao> dao = getDao(providerClass);
		Preconditions.checkArgument(dao.isPresent(), PROVIDER_DAO_NOT_FOUND);

		return dao.get().get(code);
	}

	@Override
	public Optional<ThirdPartyAuthenticationProviderModel> getActive(final String cmsSiteUid, final Class<?> providerClass)
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(cmsSiteUid), CMSSITE_UID_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<ThirdPartyAuthenticationProviderDao> dao = getDao(providerClass);
		Preconditions.checkArgument(dao.isPresent(), PROVIDER_DAO_NOT_FOUND);

		return dao.get().getActive(cmsSiteUid);
	}

	@Override
	public Optional<ThirdPartyAuthenticationProviderModel> getActive(final CMSSiteModel cmsSiteModel, final Class<?> providerClass)
	{
		Preconditions.checkArgument(cmsSiteModel != null, CMSSITE_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<ThirdPartyAuthenticationProviderDao> dao = getDao(providerClass);
		Preconditions.checkArgument(dao.isPresent(), PROVIDER_DAO_NOT_FOUND);

		return dao.get().getActive(cmsSiteModel);
	}

	@Override
	public Optional<ThirdPartyAuthenticationProviderModel> getActiveByCurrentSite(final Class<?> providerClass)
	{
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<ThirdPartyAuthenticationProviderDao> dao = getDao(providerClass);
		Preconditions.checkArgument(dao.isPresent(), PROVIDER_DAO_NOT_FOUND);

		return dao.get().getActiveByCurrentSite();
	}

	protected Map<Class<?>, ThirdPartyAuthenticationProviderDao> getThirdPartyAuthenticationProviderDaoMap()
	{
		return thirdPartyAuthenticationProviderDaoMap;
	}

	protected Optional<ThirdPartyAuthenticationProviderDao> getDao(final Class<?> providerClass)
	{
		final ThirdPartyAuthenticationProviderDao dao = getThirdPartyAuthenticationProviderDaoMap().get(providerClass);

		return Optional.ofNullable(dao);
	}

}
