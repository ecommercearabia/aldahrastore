/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrathirdpartyauthentication.dao;

import de.hybris.platform.cms2.model.site.CMSSiteModel;

import java.util.Optional;

import com.aldahra.aldahrathirdpartyauthentication.model.ThirdPartyAuthenticationProviderModel;


/**
 *
 */
public interface ThirdPartyAuthenticationProviderDao
{


	public Optional<ThirdPartyAuthenticationProviderModel> get(String code);

	public Optional<ThirdPartyAuthenticationProviderModel> getActive(String cmsSiteUid);

	public Optional<ThirdPartyAuthenticationProviderModel> getActive(CMSSiteModel cmsSiteModel);

	public Optional<ThirdPartyAuthenticationProviderModel> getActiveByCurrentSite();
}
