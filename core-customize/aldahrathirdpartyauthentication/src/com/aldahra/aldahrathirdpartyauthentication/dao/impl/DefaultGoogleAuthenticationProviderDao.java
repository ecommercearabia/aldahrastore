/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrathirdpartyauthentication.dao.impl;

import com.aldahra.aldahrathirdpartyauthentication.dao.ThirdPartyAuthenticationProviderDao;
import com.aldahra.aldahrathirdpartyauthentication.model.GoogleAuthenticationProviderModel;

/**
 *
 */
public class DefaultGoogleAuthenticationProviderDao extends DefaultThirdPartyAuthenticationProviderDao
		implements ThirdPartyAuthenticationProviderDao
{


	/**
	 *
	 */
	public DefaultGoogleAuthenticationProviderDao()
	{
		super(GoogleAuthenticationProviderModel._TYPECODE);
	}

	@Override
	protected String getModelName()
	{
		return GoogleAuthenticationProviderModel._TYPECODE;
	}


}
