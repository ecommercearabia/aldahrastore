/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrathirdpartyauthentication.dao.impl;

import com.aldahra.aldahrathirdpartyauthentication.dao.ThirdPartyAuthenticationProviderDao;
import com.aldahra.aldahrathirdpartyauthentication.model.FacebookAuthenticationProviderModel;

/**
 *
 */
public class DefaultFacebookAuthenticationProviderDao extends DefaultThirdPartyAuthenticationProviderDao
		implements ThirdPartyAuthenticationProviderDao
{

	/**
	 *
	 */
	public DefaultFacebookAuthenticationProviderDao()
	{
		super(FacebookAuthenticationProviderModel._TYPECODE);
	}

	@Override
	protected String getModelName()
	{
		return FacebookAuthenticationProviderModel._TYPECODE;
	}

}
