/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrapaymentfacades.constants;

/**
 * Global class for all Aldahrapaymentfacades constants. You can add global constants for your extension into this class.
 */
public final class AldahrapaymentfacadesConstants extends GeneratedAldahrapaymentfacadesConstants
{
	public static final String EXTENSIONNAME = "aldahrapaymentfacades";

	private AldahrapaymentfacadesConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "aldahrapaymentfacadesPlatformLogo";
}
