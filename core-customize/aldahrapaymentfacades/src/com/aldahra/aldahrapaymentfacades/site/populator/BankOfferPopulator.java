/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrapaymentfacades.site.populator;

import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import com.aldahra.aldahrapayment.model.BankOfferModel;
import com.aldahra.aldahrapaymentfacades.site.BankOfferData;


/**
 * @author amjad.shati@erabia.com
 */
public class BankOfferPopulator implements Populator<BankOfferModel, BankOfferData>
{
	@Resource(name = "imageConverter")
	private Converter<MediaModel, ImageData> imageConverter;

	@Override
	public void populate(final BankOfferModel source, final BankOfferData target) throws ConversionException
	{
		target.setCode(source.getCode());
		target.setMessage(source.getMessage());
		if (source.getIcon() != null)
		{
			target.setIcon(imageConverter.convert(source.getIcon()));
		}
	}

}
