/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrapaymentfacades.site.facade;

import java.util.List;

import com.aldahra.aldahrapaymentfacades.site.BankOfferData;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface BankOfferFacade
{
	List<BankOfferData> getBankOffersByCurrentSite();
}
