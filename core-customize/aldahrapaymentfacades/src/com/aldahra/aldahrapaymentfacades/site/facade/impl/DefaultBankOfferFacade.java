/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrapaymentfacades.site.facade.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.site.BaseSiteService;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;

import com.aldahra.aldahrapayment.model.BankOfferModel;
import com.aldahra.aldahrapaymentfacades.site.BankOfferData;
import com.aldahra.aldahrapaymentfacades.site.facade.BankOfferFacade;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class DefaultBankOfferFacade implements BankOfferFacade
{
	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	@Resource(name = "bankOfferConverter")
	private Converter<BankOfferModel, BankOfferData> bankOfferConverter;



	@Override
	public List<BankOfferData> getBankOffersByCurrentSite()
	{
		final CMSSiteModel currentCMSSite = (CMSSiteModel) baseSiteService.getCurrentBaseSite();
		if (currentCMSSite == null || CollectionUtils.isEmpty(currentCMSSite.getBankOffers()))
		{
			Collections.emptyList();
		}
		return bankOfferConverter.convertAll(currentCMSSite.getBankOffers().stream().filter(Objects::nonNull)
				.filter(o -> o.isActive()).collect(Collectors.toList()));
	}

}
