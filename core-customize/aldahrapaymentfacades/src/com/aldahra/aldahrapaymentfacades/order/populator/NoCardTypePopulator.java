package com.aldahra.aldahrapaymentfacades.order.populator;

import de.hybris.platform.commercefacades.order.data.NoCardTypeData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.type.TypeService;

import org.springframework.beans.factory.annotation.Autowired;

import com.aldahra.aldahrapayment.enums.NoCardType;


/**
 * Converter implementation for {@link de.hybris.platform.core.enums.CreditCardType} as source and
 * {@link de.hybris.platform.commercefacades.order.data.CardTypeData} as target type.
 *
 * @author mnasro
 */
public class NoCardTypePopulator implements Populator<NoCardType, NoCardTypeData>
{

	/** The type service. */
	private TypeService typeService;

	/**
	 * Gets the type service.
	 *
	 * @return the type service
	 */
	protected TypeService getTypeService()
	{
		return typeService;
	}

	/**
	 * Sets the type service.
	 *
	 * @param typeService
	 *           the new type service
	 */
	@Autowired
	public void setTypeService(final TypeService typeService)
	{
		this.typeService = typeService;
	}

	/**
	 * Populate.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	@Override
	public void populate(final NoCardType source, final NoCardTypeData target)
	{
		target.setCode(source.getCode());
		target.setName(getTypeService().getEnumerationValue(source).getName());
	}
}
