/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrapaymentfacades.order.populator;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.NoCardPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.PaymentModeData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.NoCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.TaxValue;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;


/**
 *
 * The Class PaymentAbstractOrderPopulator.
 *
 * @author mnasro
 *
 * @param <S>
 *           SOURCE, the generic type extends AbstractOrderModel
 * @param <T>
 *           TARGET, the generic type extends AbstractOrderData
 */
public class PaymentAbstractOrderPopulator<S extends AbstractOrderModel, T extends AbstractOrderData> implements Populator<S, T>
{

	/** The no card payment info converter. */
	@Resource(name = "noCardPaymentInfoConverter")
	private Converter<NoCardPaymentInfoModel, NoCardPaymentInfoData> noCardPaymentInfoConverter;

	/** The payment mode converter. */
	@Resource(name = "paymentModeConverter")
	private Converter<PaymentModeModel, PaymentModeData> paymentModeConverter;

	@Resource(name = "priceDataFactory")
	private PriceDataFactory priceDataFactory;


	/**
	 * @return the priceDataFactory
	 */
	public PriceDataFactory getPriceDataFactory()
	{
		return priceDataFactory;
	}

	/**
	 * Gets the payment mode converter.
	 *
	 * @return the paymentModeConverter
	 */
	public Converter<PaymentModeModel, PaymentModeData> getPaymentModeConverter()
	{
		return paymentModeConverter;
	}

	/**
	 * Gets the no card payment info converter.
	 *
	 * @return the noCardPaymentInfoConverter
	 */
	public Converter<NoCardPaymentInfoModel, NoCardPaymentInfoData> getNoCardPaymentInfoConverter()
	{
		return noCardPaymentInfoConverter;
	}

	/**
	 * Populate.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 * @throws ConversionException
	 *            the conversion exception
	 */
	@Override
	public void populate(final AbstractOrderModel source, final AbstractOrderData target)
	{
		if (source != null && target != null)
		{
			populatePaymentInfo(source, target);
			populatePaymentMode(source, target);
			populatePaymentBody(source, target);
			populateSubTotalBeforeSaving(source, target);
			populateNetAmount(source, target);
		}


	}


	private void populateNetAmount(final AbstractOrderModel source, final AbstractOrderData target)
	{
		final double productsDiscountsAmount = getProductsDiscountsAmount(source);
		final double subtotal = source.getSubtotal() == null ? 00.0 : source.getSubtotal().doubleValue();

		final double customSubTotal = productsDiscountsAmount + subtotal;
		final double totalTax = source.getTotalTax() == null ? 00.0 : source.getTotalTax().doubleValue();

		final List<AbstractOrderEntryModel> entries = source.getEntries();

		double allAppliedValue = 0.0;
		if (entries != null && !entries.isEmpty())
		{
			for (final AbstractOrderEntryModel abstractOrderEntryModel : entries)
			{
				final Collection<TaxValue> taxValues = abstractOrderEntryModel.getTaxValues();
				for (final TaxValue taxValue : taxValues)
				{
					final double appliedValue = taxValue.getAppliedValue();
					allAppliedValue += appliedValue;
				}
			}
		}

		final double netAmount = customSubTotal - allAppliedValue;
		target.setNetAmount(getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(netAmount), source.getCurrency()));
	}

	protected double getProductsDiscountsAmount(final AbstractOrderModel source)
	{
		double discounts = 0.0d;

		final List<AbstractOrderEntryModel> entries = source.getEntries();
		if (entries != null)
		{
			for (final AbstractOrderEntryModel entry : entries)
			{
				final List<DiscountValue> discountValues = entry.getDiscountValues();
				if (discountValues != null)
				{
					for (final DiscountValue dValue : discountValues)
					{
						discounts += dValue.getAppliedValue();
					}
				}
			}
		}
		return discounts;
	}

	/**
	 *
	 */
	private void populatePaymentBody(final AbstractOrderModel source, final AbstractOrderData target)
	{
		target.setResponsePaymentBody(source.getResponsePaymentBody());
		target.setRequestPaymentBody(source.getRequestPaymentBody());

	}

	/**
	 * Populate payment mode.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	private void populatePaymentMode(final AbstractOrderModel source, final AbstractOrderData target)
	{
		if (source.getPaymentMode() != null)
		{
			target.setPaymentMode(getPaymentModeConverter().convert(source.getPaymentMode()));
		}

	}

	/**
	 * Populate payment info.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	private void populatePaymentInfo(final AbstractOrderModel source, final AbstractOrderData target)
	{

		final PaymentInfoModel paymentInfo = source.getPaymentInfo();
		if (paymentInfo instanceof NoCardPaymentInfoModel)
		{
			final NoCardPaymentInfoData paymentInfoData = getNoCardPaymentInfoConverter()
					.convert((NoCardPaymentInfoModel) paymentInfo);
			target.setNoCardPaymentInfo(paymentInfoData);
		}

	}


	/**
	 *
	 */
	private void populateSubTotalBeforeSaving(final AbstractOrderModel source, final AbstractOrderData target)
	{
		double subTotalBeforeSaving = 0;
		final List<AbstractOrderEntryModel> entryModels = source.getEntries();

		for (final AbstractOrderEntryModel orderEntry : entryModels)
		{
			final Double basePrice = orderEntry.getBasePrice();
			final Long quantity = orderEntry.getQuantity();
			if (basePrice != null && quantity != null)
			{
				final double beforeSavingTotlePrice = basePrice.doubleValue() * quantity.doubleValue();
				subTotalBeforeSaving += beforeSavingTotlePrice;
			}
		}

		final PriceData subTotalBeforeSavingPrice = getPriceDataFactory().create(PriceDataType.BUY,
				BigDecimal.valueOf(subTotalBeforeSaving), source.getCurrency());
		target.setSubTotalBeforeSavingPrice(subTotalBeforeSavingPrice);



	}
}
