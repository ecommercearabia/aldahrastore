package com.aldahra.aldahrapaymentfacades.order.populator;

import de.hybris.platform.commercefacades.order.data.NoCardPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.NoCardTypeData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.payment.NoCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import com.aldahra.aldahrapayment.enums.NoCardType;


/**
 * The Class NoCardPaymentInfoPopulator.
 *
 * @author mnasro
 */
public class NoCardPaymentInfoPopulator implements Populator<NoCardPaymentInfoModel, NoCardPaymentInfoData>
{

	/** The address converter. */
	@Resource(name = "addressConverter")
	private Converter<AddressModel, AddressData> addressConverter;

	/** The no card type converter. */
	@Resource(name = "noCardTypeConverter")
	private Converter<NoCardType, NoCardTypeData> noCardTypeConverter;


	/**
	 * Populate.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	@Override
	public void populate(final NoCardPaymentInfoModel source, final NoCardPaymentInfoData target)
	{
		target.setId(source.getPk().toString());
		target.setSaved(source.isSaved());
		if (source.getType() != null)
		{
			target.setNoCardTypeData(getNoCardTypeConverter().convert(source.getType()));
		}
		if (source.getBillingAddress() != null)
		{
			target.setBillingAddress(getAddressConverter().convert(source.getBillingAddress()));
		}
	}



	/**
	 * Gets the address converter.
	 *
	 * @return the addressConverter
	 */
	public Converter<AddressModel, AddressData> getAddressConverter()
	{
		return addressConverter;
	}


	/**
	 * Gets the no card type converter.
	 *
	 * @return the noCardTypeConverter
	 */
	public Converter<NoCardType, NoCardTypeData> getNoCardTypeConverter()
	{
		return noCardTypeConverter;
	}
}
