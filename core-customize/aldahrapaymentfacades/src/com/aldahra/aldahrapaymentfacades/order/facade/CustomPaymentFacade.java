package com.aldahra.aldahrapaymentfacades.order.facade;

import de.hybris.platform.acceleratorfacades.payment.PaymentFacade;
import de.hybris.platform.acceleratorfacades.payment.data.PaymentSubscriptionResultData;

import java.util.Map;


/**
 * @author mnasro
 */
public interface CustomPaymentFacade extends PaymentFacade
{

	public PaymentSubscriptionResultData completePaymentCreateSubscription(Map<String, Object> orderInfoMap,
			boolean saveInAccount);
}
