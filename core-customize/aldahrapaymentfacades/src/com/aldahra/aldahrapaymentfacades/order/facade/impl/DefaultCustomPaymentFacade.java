/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrapaymentfacades.order.facade.impl;

import de.hybris.platform.acceleratorfacades.payment.data.PaymentSubscriptionResultData;
import de.hybris.platform.acceleratorfacades.payment.impl.DefaultPaymentFacade;
import de.hybris.platform.acceleratorservices.payment.data.PaymentSubscriptionResultItem;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.Map;

import javax.annotation.Resource;

import com.aldahra.aldahrapayment.service.CustomAcceleratorPaymentService;
import com.aldahra.aldahrapaymentfacades.order.facade.CustomPaymentFacade;


/**
 * @author mnasro
 */
public class DefaultCustomPaymentFacade extends DefaultPaymentFacade
		implements CustomPaymentFacade
{
	@Resource(name = "customAcceleratorPaymentService")
	private CustomAcceleratorPaymentService customAcceleratorPaymentService;


	@Override
	public PaymentSubscriptionResultData completePaymentCreateSubscription(final Map<String, Object> orderInfoMap,
			final boolean saveInAccount)
	{
		final CustomerModel customerModel = getCurrentUserForCheckout();
		final PaymentSubscriptionResultItem paymentSubscriptionResultItem = getCustomAcceleratorPaymentService()
				.completeCreatePaymentSubscription(customerModel, saveInAccount, orderInfoMap);

		if (paymentSubscriptionResultItem != null)
		{
			return getPaymentSubscriptionResultDataConverter().convert(paymentSubscriptionResultItem);
		}

		return null;
	}

	/**
	 * @return the customAcceleratorPaymentService
	 */
	public CustomAcceleratorPaymentService getCustomAcceleratorPaymentService()
	{
		return customAcceleratorPaymentService;
	}


	/**
	 * @param customAcceleratorPaymentService
	 *           the customAcceleratorPaymentService to set
	 */
	public void setCustomAcceleratorPaymentService(final CustomAcceleratorPaymentService customAcceleratorPaymentService)
	{
		this.customAcceleratorPaymentService = customAcceleratorPaymentService;
	}
}
