/**
 *
 */
package com.aldahra.aldahrastorecredit.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import javax.annotation.Resource;

import com.aldahra.aldahrastorecredit.dao.StoreCreditModeDao;
import com.aldahra.aldahrastorecredit.enums.StoreCreditModeType;
import com.aldahra.aldahrastorecredit.model.StoreCreditModeModel;


/**
 * @author mnasro
 *
 */
public class DefaultStoreCreditModeDao extends AbstractItemDao implements StoreCreditModeDao
{

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aldahra.aldahrastorecredit.dao.StoreCreditModeDao#getStoreCreditMode(com.aldahra.aldahrastorecredit.enums.
	 * StoreCreditModeType)
	 */
	@Override
	public StoreCreditModeModel getStoreCreditMode(final StoreCreditModeType storeCreditModeType)
	{
		final String query = "SELECT {PK} FROM {StoreCreditMode} WHERE {storeCreditModeType}=?storeCreditModeType";

		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(query);
		fQuery.addQueryParameter("storeCreditModeType", storeCreditModeType);

		final SearchResult<StoreCreditModeModel> result = getFlexibleSearchService().search(fQuery);

		return result.getCount() > 0 ? result.getResult().get(0) : null;
	}


}
