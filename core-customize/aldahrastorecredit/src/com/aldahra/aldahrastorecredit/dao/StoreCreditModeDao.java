/**
 *
 */
package com.aldahra.aldahrastorecredit.dao;

import com.aldahra.aldahrastorecredit.enums.StoreCreditModeType;
import com.aldahra.aldahrastorecredit.model.StoreCreditModeModel;


/**
 * @author mnasro
 *
 */
public interface StoreCreditModeDao
{
	public StoreCreditModeModel getStoreCreditMode(StoreCreditModeType storeCreditModeType);
}
