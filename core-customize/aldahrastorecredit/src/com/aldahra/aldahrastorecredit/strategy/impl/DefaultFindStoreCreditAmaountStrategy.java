package com.aldahra.aldahrastorecredit.strategy.impl;

import de.hybris.platform.commerceservices.constants.CommerceServicesConstants;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.result.PromotionOrderResults;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.PriceValue;

import java.math.BigDecimal;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.aldahra.aldahrastorecredit.enums.StoreCreditModeType;
import com.aldahra.aldahrastorecredit.model.StoreCreditModeModel;
import com.aldahra.aldahrastorecredit.service.StoreCreditService;
import com.aldahra.aldahrastorecredit.strategy.FindStoreCreditAmaountStrategy;
import com.google.common.math.DoubleMath;


/**
 * @author mnasro
 *
 *         Default implementation of {@link FindStoreCreditAmaountStrategy}.
 */
public class DefaultFindStoreCreditAmaountStrategy extends AbstractBusinessService implements FindStoreCreditAmaountStrategy
{

	private static final Logger LOG = Logger.getLogger(DefaultFindStoreCreditAmaountStrategy.class);
	private static final double EPSILON = 0.01d;
	@Resource(name = "storeCreditService")
	private StoreCreditService storeCreditService;
	@Resource(name = "promotionsService")
	private PromotionsService promotionsService;

	protected double getQuoteDiscountsAmount(final AbstractOrderModel source)
	{
		double discounts = 0.0d;
		final List<DiscountValue> discountList = source.getGlobalDiscountValues(); // discounts on the cart itself
		if (discountList != null && !discountList.isEmpty())
		{
			for (final DiscountValue discount : discountList)
			{
				final double value = discount.getAppliedValue();
				if (DoubleMath.fuzzyCompare(value, 0, EPSILON) > 0
						&& CommerceServicesConstants.QUOTE_DISCOUNT_CODE.equals(discount.getCode()))
				{
					discounts += value;
				}
			}
		}
		return discounts;
	}

	protected double getAllDiscounts(final AbstractOrderModel source)
	{
		final PromotionOrderResults promoOrderResults = promotionsService.getPromotionResults(source);
		double value = 0.0;
		final double quoteDiscountsAmount = getQuoteDiscountsAmount(source);

		if (promoOrderResults != null)
		{
			final double productsDiscountsAmount = getProductsDiscountsAmount(source);
			final double orderDiscountsAmount = getOrderDiscountsAmount(source);

			value = productsDiscountsAmount + orderDiscountsAmount + quoteDiscountsAmount;
		}

		return value;
	}

	protected double getProductsDiscountsAmount(final AbstractOrderModel source)
	{
		double discounts = 0.0d;

		final List<AbstractOrderEntryModel> entries = source.getEntries();
		if (entries != null)
		{
			for (final AbstractOrderEntryModel entry : entries)
			{
				final List<DiscountValue> discountValues = entry.getDiscountValues();
				if (discountValues != null)
				{
					for (final DiscountValue dValue : discountValues)
					{
						discounts += dValue.getAppliedValue();
					}
				}
			}
		}
		return discounts;
	}

	protected double getOrderDiscountsAmount(final AbstractOrderModel source)
	{
		double discounts = 0.0d;
		final List<DiscountValue> discountList = source.getGlobalDiscountValues(); // discounts on the cart itself
		if (discountList != null && !discountList.isEmpty())
		{
			for (final DiscountValue discount : discountList)
			{
				final double value = discount.getAppliedValue();
				if (DoubleMath.fuzzyCompare(value, 0, EPSILON) > 0
						&& !CommerceServicesConstants.QUOTE_DISCOUNT_CODE.equals(discount.getCode()))
				{
					discounts += value;
				}
			}
		}
		return discounts;
	}

	public static double calculateStoreCreditAmount(final double storeCreditAmount, final double totalOrderAmount)
	{
		return storeCreditAmount >= totalOrderAmount ? totalOrderAmount : storeCreditAmount;
	}

	protected double calcTotalWithTax(final AbstractOrderModel source)
	{
		if (source == null)
		{
			throw new IllegalArgumentException("source order must not be null");
		}
		if (source.getTotalPrice() == null)
		{
			return 0.0d;
		}

		BigDecimal totalPrice = BigDecimal.valueOf(source.getTotalPrice().doubleValue());

		// Add the taxes to the total price if the cart is net; if the total was null taxes should be null as well
		if (Boolean.TRUE.equals(source.getNet()) && totalPrice.compareTo(BigDecimal.ZERO) != 0 && source.getTotalTax() != null)
		{
			totalPrice = totalPrice.add(BigDecimal.valueOf(source.getTotalTax().doubleValue()));
		}
		return (totalPrice != null ? totalPrice.doubleValue() : 0.0d);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.aldahra.aldahrastorecredit.strategy.FindStoreCreditAmaountStrategy#getStoreCreditAmaount(de.hybris.platform
	 * .core.model.order.AbstractOrderModel, double, double)
	 */
	@Override
	public PriceValue getStoreCreditAmaount(final AbstractOrderModel order, final double totalDiscounts)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("order", order);

		double storeCreditAmount = 0.0;

		try
		{
			final StoreCreditModeModel storeCreditMode = order.getStoreCreditMode();
			final double subtotal = order.getSubtotal().doubleValue();
			final double paymentCost = order.getPaymentCost() == null ? 0.0 : order.getPaymentCost().doubleValue();
			final double deliveryCost = order.getDeliveryCost() == null ? 0.0 : order.getDeliveryCost().doubleValue();
			final boolean cscaidc = order.getStore().getCalcStoreCreditAmountIncludeDeliveryCost() == null ? false : order
					.getStore().getCalcStoreCreditAmountIncludeDeliveryCost().booleanValue();
			final boolean cscaipc = order.getStore().getCalcStoreCreditAmountIncludePaymentCost() == null ? false : order.getStore()
					.getCalcStoreCreditAmountIncludePaymentCost().booleanValue();

			//			final double allDiscounts = getAllDiscounts(order);

			double totalCost = subtotal;
			if (cscaidc)
			{
				totalCost += deliveryCost;
			}
			if (cscaipc)
			{
				totalCost += paymentCost;
			}

			totalCost -= totalDiscounts;

			final boolean enableCheckTotalPriceWithStoreCreditLimit = order.getStore() == null ? false
					: order.getStore().isEnableCheckTotalPriceWithStoreCreditLimit();
			if (enableCheckTotalPriceWithStoreCreditLimit)
			{
				final double totalPricewithStoreCreditLimit = order.getStore() == null ? 0.0
						: order.getStore().getTotalPriceWithStoreCreditLimit();
				totalCost -= totalPricewithStoreCreditLimit;

			}

			final BigDecimal storeCreditAmountOS = storeCreditService.getStoreCreditAmount(order);
			if (order.getUser() != null && totalCost > 0 && storeCreditAmountOS != null && storeCreditAmountOS.doubleValue() > 0
					&& storeCreditMode != null && storeCreditMode.getStoreCreditModeType() != null
					&& !StoreCreditModeType.REDEEM_NONE.getCode().equals(storeCreditMode.getStoreCreditModeType().getCode()))
			{

				getModelService().save(order);
				if (StoreCreditModeType.REDEEM_FULL_AMOUNT.getCode().equals(storeCreditMode.getStoreCreditModeType().getCode()))
				{

					storeCreditAmount = calculateStoreCreditAmount(storeCreditAmountOS.doubleValue(), totalCost);
				}
				else if (StoreCreditModeType.REDEEM_SPECIFIC_AMOUNT.getCode().equals(
						storeCreditMode.getStoreCreditModeType().getCode())
						&& order.getStoreCreditAmountSelected() != null && order.getStoreCreditAmountSelected().doubleValue() > 0)
				{
					final double storeCreditAmaountSelected = storeCreditAmountOS.doubleValue() >= order
							.getStoreCreditAmountSelected().doubleValue() ? order.getStoreCreditAmountSelected().doubleValue()
							: storeCreditAmountOS.doubleValue();
					storeCreditAmount = calculateStoreCreditAmount(storeCreditAmaountSelected, totalCost);
				}
			}
		}
		catch (final Exception e)
		{
			LOG.warn("Could not find storeCreditAmaount for order [" + order.getCode() + "] due to : " + e + "... skipping!");
		}

		return new PriceValue(order.getCurrency().getIsocode(), storeCreditAmount, order.getNet().booleanValue());
	}

}
