package com.aldahra.aldahrastorecredit.strategy;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.util.PriceValue;


public interface FindStoreCreditAmaountStrategy
{

	/**
	 * Returns order's Store Credit Amaount of the given order.
	 *
	 * @param order
	 *           {@link AbstractOrderModel}
	 * @return {@link PriceValue} representing Store Credit Amaount introduced in the order.
	 */
	PriceValue getStoreCreditAmaount(AbstractOrderModel order, double totalDiscounts);
}
