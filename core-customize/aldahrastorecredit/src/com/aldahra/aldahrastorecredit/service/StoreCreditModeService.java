/**
 *
 */
package com.aldahra.aldahrastorecredit.service;

import de.hybris.platform.store.BaseStoreModel;

import java.util.List;

import com.aldahra.aldahrastorecredit.enums.StoreCreditModeType;
import com.aldahra.aldahrastorecredit.model.StoreCreditModeModel;


/**
 * @author mnasro
 *
 */
public interface StoreCreditModeService
{
	public StoreCreditModeModel getStoreCreditMode(String storeCreditModeTypeCode);

	public StoreCreditModeModel getStoreCreditMode(StoreCreditModeType storeCreditModeType);

	public List<StoreCreditModeModel> getSupportedStoreCreditModes(BaseStoreModel baseStoreModel);

	public List<StoreCreditModeModel> getSupportedStoreCreditModesCurrentBaseStore();
}
