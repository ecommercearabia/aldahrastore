/**
 *
 */
package com.aldahra.aldahrastorecredit.service.impl;

import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.List;

import javax.annotation.Resource;

import com.aldahra.aldahrastorecredit.dao.StoreCreditModeDao;
import com.aldahra.aldahrastorecredit.enums.StoreCreditModeType;
import com.aldahra.aldahrastorecredit.exception.StoreCreditException;
import com.aldahra.aldahrastorecredit.exception.enums.StoreCreditExceptionType;
import com.aldahra.aldahrastorecredit.model.StoreCreditModeModel;
import com.aldahra.aldahrastorecredit.service.StoreCreditModeService;


/**
 * @author mnasro
 *
 */
public class DefaultStoreCreditModeService implements StoreCreditModeService
{
	@Resource(name = "storeCreditModeDao")
	private StoreCreditModeDao storeCreditModeDao;
	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.aldahra.aldahrastorecredit.service.StoreCreditModeService#getStoreCreditMode(com.aldahra.aldahrastorecredit.
	 * enums .StoreCreditModeType)
	 */
	@Override
	public StoreCreditModeModel getStoreCreditMode(final StoreCreditModeType storeCreditModeType)
	{
		if (storeCreditModeType == null)
		{
			throw new IllegalArgumentException("storeCreditModeType can't be null");
		}
		final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();

		if (!currentBaseStore.getStoreCreditEnabled())
		{
			throw new StoreCreditException("Store Credit is not available", StoreCreditExceptionType.NOT_AVAILABLE);
		}
		return storeCreditModeDao.getStoreCreditMode(storeCreditModeType);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.aldahra.aldahrastorecredit.service.StoreCreditModeService#getSupportedStoreCreditModes(de.hybris.platform.
	 * store .BaseStoreModel);
	 */
	@Override
	public List<StoreCreditModeModel> getSupportedStoreCreditModes(final BaseStoreModel baseStoreModel)
	{
		if (baseStoreModel == null)
		{
			throw new IllegalArgumentException("baseStoreModel can't be null");
		}
		else if (baseStoreModel.getStoreCreditEnabled() == null || !baseStoreModel.getStoreCreditEnabled())
		{
			throw new StoreCreditException("Store Credit is not available", StoreCreditExceptionType.NOT_AVAILABLE);
		}

		return baseStoreModel.getSupportedStoreCreditModes();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.aldahra.aldahrastorecredit.service.StoreCreditModeService#getSupportedStoreCreditModesCurrentBaseStore()
	 */
	@Override
	public List<StoreCreditModeModel> getSupportedStoreCreditModesCurrentBaseStore()
	{
		return getSupportedStoreCreditModes(baseStoreService.getCurrentBaseStore());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.aldahra.aldahrastorecredit.service.StoreCreditModeService#getStoreCreditMode(java.lang.String)
	 */
	@Override
	public StoreCreditModeModel getStoreCreditMode(final String storeCreditModeTypeCode)
	{
		if (storeCreditModeTypeCode == null)
		{
			throw new IllegalArgumentException("storeCreditModeTypeCode can't be null");
		}

		final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();

		if (currentBaseStore.getStoreCreditEnabled() == null || !currentBaseStore.getStoreCreditEnabled())
		{
			throw new StoreCreditException("Store Credit is not available", StoreCreditExceptionType.NOT_AVAILABLE);
		}

		final StoreCreditModeType storeCreditModeType = StoreCreditModeType.valueOf(storeCreditModeTypeCode);
		if (storeCreditModeType == null)
		{
			return null;
		}
		return getStoreCreditMode(storeCreditModeType);
	}


}
