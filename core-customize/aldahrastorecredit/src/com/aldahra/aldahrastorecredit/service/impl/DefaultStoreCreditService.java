/**
 *
 */
package com.aldahra.aldahrastorecredit.service.impl;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;

import com.aldahra.aldahrastorecredit.exception.StoreCreditException;
import com.aldahra.aldahrastorecredit.exception.enums.StoreCreditExceptionType;
import com.aldahra.aldahrastorecredit.model.StoreCreditHistoryModel;
import com.aldahra.aldahrastorecredit.model.UserStoreCreditAmountModel;
import com.aldahra.aldahrastorecredit.service.StoreCreditService;
import com.google.common.base.Preconditions;


/**
 * @author mnasro
 *
 */
public class DefaultStoreCreditService implements StoreCreditService
{
	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;
	@Resource(name = "modelService")
	private ModelService modelService;
	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.aldahra.aldahrastorecredit.services.StoreCreditService#getStoreCreditHistory(de.hybris.platform.core.model.
	 * user .UserModel)
	 */
	@Override
	public List<StoreCreditHistoryModel> getStoreCreditHistory(final UserModel userModel)
	{
		if (userModel == null)
		{
			throw new IllegalArgumentException("userModel is null");
		}
		final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
		if (currentBaseStore.getStoreCreditEnabled() == null || !currentBaseStore.getStoreCreditEnabled())
		{
			throw new StoreCreditException("Store Credit is not available", StoreCreditExceptionType.NOT_AVAILABLE);
		}

		return userModel.getStoreCreditHistory();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.aldahra.aldahrastorecredit.services.StoreCreditService#getStoreCreditHistoryByCurrentUser()
	 */
	@Override
	public List<StoreCreditHistoryModel> getStoreCreditHistoryByCurrentUser()
	{
		return getStoreCreditHistory(userService.getCurrentUser());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.aldahra.aldahrastorecredit.services.StoreCreditService#getStoreCreditAmount(de.hybris.platform.core.model.user
	 * .UserModel)
	 */
	@Override
	public BigDecimal getStoreCreditAmount(final UserModel userModel, final BaseStoreModel baseStoreModel)
	{
		if (userModel == null)
		{
			throw new IllegalArgumentException("userModel is null");
		}
		if (baseStoreModel == null)
		{
			throw new IllegalArgumentException("baseStoreModel cannot must be null");
		}

		final CurrencyModel currentCurrency = baseStoreModel.getDefaultCurrency();
		final double conversion = (currentCurrency.getConversion() == null ? 1.0 : currentCurrency.getConversion().doubleValue());
		final CurrencyModel baseCurrency = commonI18NService.getBaseCurrency();
		final double baseConversion = (baseCurrency.getConversion() == null ? 1.0 : baseCurrency.getConversion().doubleValue());

		final List<UserStoreCreditAmountModel> storeCreditAmounts = userModel.getStoreCreditAmounts();
		if (storeCreditAmounts != null && !storeCreditAmounts.isEmpty())
		{
			for (final UserStoreCreditAmountModel userStoreCreditAmountModel : storeCreditAmounts)
			{
				if (userStoreCreditAmountModel.getStore() != null
						&& baseStoreModel.getUid().equals(userStoreCreditAmountModel.getStore().getUid()))
				{
					double storeCreditAmount = 0.0;
					if (userStoreCreditAmountModel.getStoreCreditAmount() != null
							&& userStoreCreditAmountModel.getStoreCreditAmount().doubleValue() > 0)
					{
						storeCreditAmount = commonI18NService.convertAndRoundCurrency(baseConversion, conversion,
								currentCurrency.getDigits().intValue(), userStoreCreditAmountModel.getStoreCreditAmount().doubleValue());
					}
					return BigDecimal.valueOf(storeCreditAmount);

				}
			}
		}

		return new BigDecimal(0);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.aldahra.aldahrastorecredit.services.StoreCreditService#getStoreCreditEnabled(de.hybris.platform.store.
	 * BaseStoreModel )
	 */
	@Override
	public boolean getStoreCreditEnabled(final BaseStoreModel baseStoreModel)
	{

		if (baseStoreModel == null)
		{
			throw new IllegalArgumentException("baseStoreModel cannot must be null");
		}

		return baseStoreModel.getStoreCreditEnabled() == null ? false : baseStoreModel.getStoreCreditEnabled().booleanValue();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.aldahra.aldahrastorecredit.services.StoreCreditService#getStoreCreditEnabledByCurrentBaseStore()
	 */
	@Override
	public boolean getStoreCreditEnabledByCurrentBaseStore()
	{
		return getStoreCreditEnabled(baseStoreService.getCurrentBaseStore());
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.aldahra.aldahrastorecredit.services.StoreCreditService#getStoreCreditAmountByCurrentUser(de.hybris.platform.
	 * store.BaseStoreModel)
	 */
	@Override
	public BigDecimal getStoreCreditAmountByCurrentUser(final BaseStoreModel baseStoreModel)
	{
		return getStoreCreditAmount(userService.getCurrentUser(), baseStoreModel);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.aldahra.aldahrastorecredit.services.StoreCreditService#getStoreCreditAmountByCurrentUserAndCurrentBaseStore()
	 */
	@Override
	public BigDecimal getStoreCreditAmountByCurrentUserAndCurrentBaseStore()
	{
		return getStoreCreditAmountByCurrentUser(baseStoreService.getCurrentBaseStore());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.aldahra.aldahrastorecredit.services.StoreCreditService#redeemStoreCreditAmount(de.hybris.platform.core.model
	 * .user.UserModel, de.hybris.platform.store.BaseStoreModel)
	 */
	@Override
	public void redeemStoreCreditAmount(final AbstractOrderModel abstractOrderModel)
	{
		if (abstractOrderModel == null)
		{
			throw new IllegalArgumentException("abstractOrderModel is null");
		}

		final Double amount = abstractOrderModel.getStoreCreditAmount();
		if (amount == null || amount.doubleValue() <= 0)
		{
			return;
		}
		final List<UserStoreCreditAmountModel> storeCreditAmounts = abstractOrderModel.getUser().getStoreCreditAmounts();
		if (storeCreditAmounts != null && !storeCreditAmounts.isEmpty())
		{
			for (final UserStoreCreditAmountModel userStoreCreditAmountModel : storeCreditAmounts)
			{
				if (userStoreCreditAmountModel.getStore() != null
						&& abstractOrderModel.getStore().getUid().equals(userStoreCreditAmountModel.getStore().getUid()))
				{
					final BigDecimal storeCreditAmount = userStoreCreditAmountModel.getStoreCreditAmount();
					if (storeCreditAmount != null && storeCreditAmount.doubleValue() > 0)
					{
						final BigDecimal newstoreCreditAmount = BigDecimal
								.valueOf((storeCreditAmount.doubleValue() - amount.doubleValue()));

						userStoreCreditAmountModel.setStoreCreditAmount(newstoreCreditAmount);

						final StoreCreditHistoryModel storeCreditHistoryModel = modelService.create(StoreCreditHistoryModel.class);
						storeCreditHistoryModel.setAmount(new BigDecimal(amount));
						storeCreditHistoryModel.setBalance(storeCreditAmount);
						storeCreditHistoryModel.setOrderCode(abstractOrderModel.getCode());
						storeCreditHistoryModel.setUser(abstractOrderModel.getUser());
						storeCreditHistoryModel.setDateOfPurchase(abstractOrderModel.getDate());
						modelService.save(userStoreCreditAmountModel);

						final UserModel userModel = abstractOrderModel.getUser();
						modelService.refresh(userModel);

						final List<StoreCreditHistoryModel> storeCreditHistory = new ArrayList<StoreCreditHistoryModel>();
						if (userModel.getStoreCreditHistory() != null && !userModel.getStoreCreditHistory().isEmpty())
						{
							storeCreditHistory.addAll(userModel.getStoreCreditHistory());
						}
						storeCreditHistory.add(storeCreditHistoryModel);
						userModel.setStoreCreditHistory(storeCreditHistory);

						modelService.save(userModel);
					}
					break;
				}
			}
		}

	}


	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.aldahra.aldahrastorecredit.services.StoreCreditService#getStoreCreditAmount(de.hybris.platform.core.model.
	 * order .AbstractOrderModel)
	 */
	@Override
	public BigDecimal getStoreCreditAmount(final AbstractOrderModel abstractOrderModel)
	{
		if (abstractOrderModel == null)
		{
			throw new IllegalArgumentException("abstractOrderModel is null");
		}

		final CurrencyModel currentCurrency = abstractOrderModel.getCurrency();
		final double conversion = (currentCurrency.getConversion() == null ? 1.0 : currentCurrency.getConversion().doubleValue());
		final CurrencyModel baseCurrency = commonI18NService.getBaseCurrency();
		final double baseConversion = (baseCurrency.getConversion() == null ? 1.0 : baseCurrency.getConversion().doubleValue());

		final List<UserStoreCreditAmountModel> storeCreditAmounts = abstractOrderModel.getUser().getStoreCreditAmounts();
		if (storeCreditAmounts != null && !storeCreditAmounts.isEmpty())
		{
			for (final UserStoreCreditAmountModel userStoreCreditAmountModel : storeCreditAmounts)
			{
				if (userStoreCreditAmountModel.getStore() != null
						&& abstractOrderModel.getStore().getUid().equals(userStoreCreditAmountModel.getStore().getUid()))
				{
					double storeCreditAmount = 0.0;
					if (userStoreCreditAmountModel.getStoreCreditAmount() != null
							&& userStoreCreditAmountModel.getStoreCreditAmount().doubleValue() > 0)
					{
						storeCreditAmount = commonI18NService.convertAndRoundCurrency(baseConversion, conversion,
								currentCurrency.getDigits().intValue(), userStoreCreditAmountModel.getStoreCreditAmount().doubleValue());
					}

					return BigDecimal.valueOf(storeCreditAmount);
				}
			}
		}

		return new BigDecimal(0);
	}

	@Override
	public void addStoreCredit(final UserModel userModel, final BaseStoreModel baseStoreModel, final BigDecimal amount)
	{
		Preconditions.checkArgument(userModel != null, "User must not be null.");
		Preconditions.checkArgument(baseStoreModel != null, "BaseStore must not be null.");
		final List<UserStoreCreditAmountModel> collect = userModel.getStoreCreditAmounts().stream()
				.filter(a -> a.getStore().equals(baseStoreModel) && a.getUser().equals(userModel))
				.collect(Collectors.toList());
		if (CollectionUtils.isEmpty(collect))
		{
			createNewStoreCreditAmount(userModel, baseStoreModel, amount);
		}
		else
		{
			updateStoreCreditAmount(collect.get(0), amount);
		}
	}

	private void updateStoreCreditAmount(final UserStoreCreditAmountModel storeCreditAmount, final BigDecimal amount)
	{
		modelService.refresh(storeCreditAmount);
		storeCreditAmount.setStoreCreditAmount(
				BigDecimal.valueOf(storeCreditAmount.getStoreCreditAmount().doubleValue() + amount.doubleValue()));
		modelService.save(storeCreditAmount);
	}

	private void createNewStoreCreditAmount(final UserModel userModel, final BaseStoreModel baseStoreModel,
			final BigDecimal amount)
	{
		final UserStoreCreditAmountModel storeCreditAmount = modelService.create(UserStoreCreditAmountModel.class);
		storeCreditAmount.setStore(baseStoreModel);
		storeCreditAmount.setUser(userModel);
		storeCreditAmount.setStoreCreditAmount(amount);
		modelService.save(storeCreditAmount);
		modelService.refresh(storeCreditAmount);
		modelService.refresh(userModel);
		final List<UserStoreCreditAmountModel> amounts = new ArrayList<>();
		if (!CollectionUtils.isEmpty(userModel.getStoreCreditAmounts()))
		{
			amounts.addAll(userModel.getStoreCreditAmounts());
		}
		amounts.add(storeCreditAmount);
		userModel.setStoreCreditAmounts(amounts);
		modelService.save(userModel);
	}
}
