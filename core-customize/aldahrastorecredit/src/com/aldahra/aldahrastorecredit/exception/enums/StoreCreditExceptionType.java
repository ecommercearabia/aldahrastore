/**
 *
 */
package com.aldahra.aldahrastorecredit.exception.enums;

/**
 * @author yhammad
 *
 */
public enum StoreCreditExceptionType
{
	NOT_AVAILABLE, EMPTY;
}
