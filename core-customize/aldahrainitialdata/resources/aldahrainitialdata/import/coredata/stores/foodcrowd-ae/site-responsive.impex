# -----------------------------------------------------------------------
# Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
# -----------------------------------------------------------------------
#
# Import the CMS Site configuration for the Foodcrowd store
#
$productCatalog=foodcrowdProductCatalog
$contentCatalog=foodcrowd-aeContentCatalog
$contentCV=catalogVersion(CatalogVersion.catalog(Catalog.id[default=$contentCatalog]),CatalogVersion.version[default=Staged])[default=$contentCatalog:Staged]
$defaultLanguage=en
$storeUid=foodcrowd-ae
$siteUid=foodcrowd-ae
$webServiceSiteUid=$siteUidWS
$siteMapUrlLimitPerFile=50000
$siteMapLangCur=enAed,arAed
$siteMapPage=Homepage,Product,CategoryLanding,Category,Store,Content
$productCatalogVersion=productCatalogVersion(catalog(id[default='foodcrowdProductCatalog']),version[default='Staged'])[unique=true,default='foodcrowdProductCatalog:Staged']
 
# Remove black and blue themes for responsive
REMOVE SiteTheme ; code[unique=true] 
                 ; black             
                 ; blue              
                 
# Import config properties into impex macros
UPDATE GenericItem[processor=de.hybris.platform.commerceservices.impex.impl.ConfigPropertyImportProcessor] ; pk[unique=true] 
                                                                                                           
# Import modulegen config properties into impex macros                                                     
$jarResource=$config-jarResource                                                                           
                                                                                                           
# Load the storefront context root config param                                                            
$storefrontContextRoot=$config-storefrontContextRoot                                                       
                                                                                                           
# SiteMap Configuration                                                                                    
INSERT_UPDATE SiteMapLanguageCurrency ; &siteMapLanguageCurrency ; language(isoCode)[unique=true] ; currency(isocode)[unique=true] ;  
                                      ; enAed                    ; en                             ; AED                            
                                      ; arAed                    ; ar                             ; AED                            
                                      
                                      
INSERT_UPDATE CatalogUnawareMedia ; &siteMapMediaId       ; code[unique=true]     ; realfilename       ; @media[translator=de.hybris.platform.impex.jalo.media.MediaDataTranslator] ; mime[default='text/plain'] 
                                  ; $siteUid-siteMapMedia ; $siteUid-siteMapMedia ; siteMapTemplate.vm ; $jarResource/site-siteMapTemplate.vm                                       ;                            
                                  
INSERT_UPDATE RendererTemplate ; &siteMapRenderer         ; code[unique=true]        ; content(&siteMapMediaId) ; contextClass                                                           ; rendererType(code)[default='velocity'] ;  
                               ; $siteUid-siteMapTemplate ; $siteUid-siteMapTemplate ; $siteUid-siteMapMedia    ; de.hybris.platform.acceleratorservices.sitemap.renderer.SiteMapContext ;                                        
                               
INSERT_UPDATE SiteMapPage ; &siteMapPage    ; code(code)[unique=true] ; frequency(code)[unique=true] ; priority[unique=true] ; active[default=true] 
                          ; Homepage        ; Homepage                ; daily                        ; 1.0                   ;                      ;  
                          ; Product         ; Product                 ; weekly                       ; 0.6                   ;                      ;  
                          ; CategoryLanding ; CategoryLanding         ; daily                        ; 0.9                   ;                      ;  
                          ; Category        ; Category                ; daily                        ; 0.8                   ;                      ;  
                          ; Store           ; Store                   ; weekly                       ; 0.6                   ;                      ;  
                          ; Content         ; Content                 ; monthly                      ; 0.4                   ;                      ;  
                          
INSERT_UPDATE SiteMapConfig ; &siteMapConfigId      ; configId[unique=true] ; siteMapLanguageCurrencies(&siteMapLanguageCurrency) ; siteMapPages(&siteMapPage) ; siteMapTemplate(&siteMapRenderer)[unique=true] ;  
                            ; $siteUidSiteMapConfig ; $siteUidSiteMapConfig ; $siteMapLangCur                                     ; $siteMapPage               ; $siteUid-siteMapTemplate                       ;  
                            
                            
# CMS Site                  
INSERT_UPDATE CMSSite ; uid[unique=true] ; theme(code) ; channel(code) ; stores(uid) ; contentCatalogs(id) ; defaultCatalog(id) ; defaultLanguage(isoCode) ; siteMapConfig(&siteMapConfigId) ; urlPatterns                                                                                                                                                                                                                                                                                                                                                                                  ; active ; previewURL                            ; startingPage(uid,$contentCV) ; urlEncodingAttributes ; defaultPromotionGroup(Identifier)[default=foodcrowd-aePromoGrp] 
                      ; $siteUid         ; foodcrowd   ; B2C           ; $storeUid   ; $contentCatalog     ; $productCatalog    ; $defaultLanguage         ; $siteUidSiteMapConfig           ; (?i)^https?://[^/]+(/[^?]*)?\?(.*\&)?(site=$siteUid)(|\&.*)$,(?i)^https?://$siteUid\.[^/]+(|/.*|\?.*)$,^(.*)$ https://foodcrowd.com/$1,(?i)^https?://foodcrowd.com.*$,^(.*)$ https://www.foodcrowd.com/$1,(?i)^https?://www.foodcrowd.com.*$,^(.*)$ https://prod.foodcrowd.com/$1,(?i)^https?://prod.foodcrowd.com.*$,^(.*)$ https://stg.foodcrowd.com/$1,(?i)^https?://stg.foodcrowd.com.*$ ,^(.*)$ https://foodcrowd.local:9002/$1,(?i)^https?://foodcrowd.local:9002*$  ; true   ; $storefrontContextRoot/?site=$siteUid ; homepage                     ; storefront,language   
                      
# Cart Cleanup CronJobs
INSERT_UPDATE CartRemovalCronJob ; code[unique=true]       ; job(code)[default=cartRemovalJob] ; sites(uid)[default=$siteUid] ; sessionLanguage(isoCode)[default=en] 
                                 ; $siteUid-CartRemovalJob 
                                 
# Uncollected orders cronjob     
INSERT_UPDATE UncollectedOrdersCronJob ; code[unique=true]             ; job(code)[default=uncollectedOrdersJob] ; sites(uid)[default=$siteUid] ; sessionLanguage(isoCode)[default=en] 
                                       ; $siteUid-UncollectedOrdersJob 
                                       
# Sitemap Generation CronJobs          
INSERT_UPDATE SiteMapMediaCronJob ; code[unique=true]        ; job(code)[default=siteMapMediaJob] ; contentSite(uid)[default=$siteUid] ; sessionLanguage(isoCode)[default=en] ; siteMapUrlLimitPerFile  ; active ;  
                                  ; $siteUid-SiteMapMediaJob ;                                    ;                                    ;                                      ; $siteMapUrlLimitPerFile ; false  ;  
                                  
INSERT_UPDATE Trigger ; cronJob(code)[unique=true]    ; second ; minute ; hour ; day ; month ; year ; relative ; active ; maxAcceptableDelay 
                      ; $siteUid-CartRemovalJob       ; 0      ; 5      ; 4    ; -1  ; -1    ; -1   ; false    ; true   ; -1                 
                      ; $siteUid-UncollectedOrdersJob ; 0      ; 0      ; 6    ; -1  ; -1    ; -1   ; true     ; false  ; -1                 
                      ; $siteUid-SiteMapMediaJob      ; 0      ; 0      ; 6    ; -1  ; -1    ; -1   ; true     ; false  ; -1                 
                      
                      
# CMS Site MobileCountries
INSERT_UPDATE CMSSite ; uid[unique=true] ; mobileCountries(isocode) 
                      ; $siteUid         ; AE,JO                    
                      
# CMS Site Twilio     
UPDATE CMSSite ; uid[unique=true] ; enableOrderConfirmationSmsMessage[default=true] 
               ; $siteUid         ;                                                 
               
# CMS Site MobileCountries
INSERT_UPDATE CMSSite ; uid[unique=true] ; defaultCountryAddress(isocode) 
                      ; $siteUid         ; AE                             
# CMS Site MobileCountries
INSERT_UPDATE CMSSite ; uid[unique=true] ; defaultMobileCountry(isocode) 
                      ; $siteUid         ; AE                            
                      
$gtmContainerId=GTM-NPRSGPL
# CMS Site googletagmanager
INSERT_UPDATE CMSSite ; uid[unique=true] ; gtmContainerId  
                      ; $siteUid         ; $gtmContainerId 
                      
UPDATE CMSSite ; uid[unique=true] ; enableManageSavedCards 
               ; $siteUid         ; true 
 
UPDATE CMSSite ; uid[unique=true] ; disableCouponForPickUpInStore 
               ; $siteUid         ; true 
             
        
                      
###TwilioOTPProvider  
                      
$authToken=bb5ef870fd7937e6db6e3701b6b4be88
$apiKey=wdB5mTCHewufnSvcfI4t0haMCJ86vpUc
$accountSid=AC03c5cd473e40f99d7dcc4af43048e4fa
$messagingServiceSid=MGb0a25d28511ffc52161bb1801c5440a4
                      
# CMS Site TwilioOTPProvider
INSERT_UPDATE TwilioOTPProvider ; code[unique=true]             ; name                             ; apiKey  ; authToken  ; accountSid  ; messagingServiceSid  ; active 
                                ; foodcrowd-aeTwilioOTPProvider ; Foodcrowd AE Twilio OTP Provider ; $apiKey ; $authToken ; $accountSid ; $messagingServiceSid ; true   
                                
# CMS Site MobileCountries      
INSERT_UPDATE CMSSite ; uid[unique=true] ; registrationOTPProvider 
                      ; $siteUid         ; TwilioOTPProvider       
                      
                      
###Etisalat OTPProvider
                      
$username=Sangeeth    
$password=Dahra@009   
$senderAddress=Food Crowd
                      
                      
# CMS Site EtisalatOTPProvider
INSERT_UPDATE EtisalatOTPProvider ; code[unique=true]               ; name                               ; username  ; password  ; senderAddress  ; active 
                                  ; foodcrowd-aeEtisalatOTPProvider ; Foodcrowd AE Etisalat OTP Provider ; $username ; $password ; $senderAddress ; true   
                                  
$siteUid=foodcrowd-ae             
                                  
INSERT_UPDATE BotSocietyProvider ; code[unique=true]              ; name                             ; callbackUrl                                               ; orderConfirmationCampaignId          ; orderDeliveredCampaignId             ; active ;  
                                 ; foodcrowd-aeBotSocietyProvider ; Foodcrowd AE BotSociety Provider ; https://webhook.site/5c0768d2-2398-493f-aacf-de2eb622f96f ; d81bf611-06b7-41fd-959c-3920807c2c38 ; 001c7f52-3cb6-40f2-846f-854ab0e6b0d5 ; true   ;  
                                 
UPDATE CMSSite ; uid[unique=true] ; otpProviders(code)                                                                           ;  
               ; $siteUid         ; foodcrowd-aeBotSocietyProvider,foodcrowd-aeTwilioOTPProvider,foodcrowd-aeEtisalatOTPProvider 
               
UPDATE CMSSite ; uid[unique=true] ; sendOrderConfirmationWhatsappMessage ; sendOrderDeliveredWhatsappMessage ;  
               ; $siteUid         ; BotSocietyProvider                   ; BotSocietyProvider                ;  
               
UPDATE CMSSite ; uid[unique=true] ; enableOrderCancellationSmsMessage ; enableOrderConfirmationSmsMessage ; enableOrderConfirmationWhatsappMessage ; enableOrderDeliveredSmsMessage ; enableOrderDeliveredWhatsappMessage ; enableShippingConfirmationSmsMessage ;  
               ; $siteUid         ; true                              ; true                              ; true                                   ; true                           ; true                                ; true                                 ;  
               
# CMS Site MobileCountries
INSERT_UPDATE CMSSite ; uid[unique=true] ; sendMessagesOTPProvider 
                      ; $siteUid         ; EtisalatOTPProvider     
                      
                      
                      
###Wishlist settings  
UPDATE CMSSite ; uid[unique=true] ; numberOfWishlistItems ; numberOfWishlistItemsPerPage 
               ; $siteUid         ; 50                    ; 5                            
               
               
UPDATE CMSSite ; uid[unique=true] ; communityPageUrl           
               ; $siteUid         ; https://blog.foodcrowd.com 
               
UPDATE CMSSite ; uid[unique=true] ; timeSlotConfigType(code) 
               ; $siteUid         ; BY_AREA                  


 
#############################################################################
################### Foodcrowd Shipment config       #########################
#############################################################################                     
UPDATE CMSSite ; uid[unique=true] ; pickUpEnabled;defaultShipmentType(code) 
               ; $siteUid         ; true 		 ;DELIVERY
                                        
#############################################################################
################### Foodcrowd ERP Service Providers #########################
#############################################################################
               
############ Update Product Stock Levek Cronjob
               
INSERT_UPDATE UpdateProductStockLevelCronJob ; $productCatalogVersion ; code[unique=true]                                            ; job(code)                     ; sessionLanguage(isoCode)[default=en] ; sessionCurrency(isocode)[default=SAR] ; autoCreate[default=true] ; warehouses(code)[default=foodcrowd-ws] ; fixedStock[default=0] ; updateFixedStock[default=false] ; removeInventoryEvents[default=true] ; store(uid) ; productBulkSize[default=5] ;  
                                             ;                        ; foodcrowd-ae-UpdateProductStockLevelQuantityCronJob          ; updateProductStockLevelJob    ; en                                   ; AED                                   ; true                     ; foodcrowd-ws                           ; 0                     ; false                           ; true                                ; $storeUid  ;                            ;  
                                             ;                        ; foodcrowd-ae-UpdateProductStockLevelWithAllAttributesCronJob ; updateProductERPStockLevelJob ; en                                   ; AED                                   ; true                     ; foodcrowd-ws                           ; 0                     ; false                           ; true                                ; $storeUid  ;                            ;  
                                             
UPDATE UpdateProductStockLevelCronJob ; $productCatalogVersion ; code[unique=true]                                            ; job(code) ; enableSaveStockResponse[default=false] ; active[default=true] ;  
                                      ;                        ; foodcrowd-ae-UpdateProductStockLevelQuantityCronJob          ;           ;                                        ;                      ;  
                                      ;                        ; foodcrowd-ae-UpdateProductStockLevelWithAllAttributesCronJob ;           ;                                        ;                      ;  
                                      
############ Update Product Stock Levek Cronjob
                                      
$query=SELECT {o.pk},{o.creationTime} FROM {order as o JOIN consignment as con ON {con.order}={o.pk} } WHERE {o.originalVersion} IS NULL AND ({o.salesOrderCreatedSuccessfully} IS NULL OR {o.salesOrderCreatedSuccessfully} = 0) AND {o.creationTime} >= '2021-06-01' ORDER BY {o.pk}
INSERT_UPDATE SendSalesOrderCronJob ; code[unique=true]                  ; job(code)         ; salesOrderQuery ; sessionLanguage(isoCode)[default=en] ; sessionCurrency(isocode)[default=AED] ; active[default=true] ; logToFile[default=true] 
                                    ; foodcrowd-ae-SendSalesOrderCronJob ; sendSalesOrderJob ; $query          ;                                      ;                                       ;                      ;                         
                                    
#INSERT_UPDATE Trigger              ; cronJob(code)[unique = true]       ; second[default = 0]; minute          ; hour                                 ; day[default = -1]                     ; month[default = -1]  ; year[default = -1]      ; relative; active[default = true]; maxAcceptableDelay[default = -1]; 
#                                   ; foodcrowd-ae-SendSalesOrderCronJob ;                   ; 05              ; 11                                   ;                                       ;                      ;                         ; false; ; ; 
                                    
                                    
##################################################
############ Aldahra ERP Web Service Provider
INSERT_UPDATE UnitOfMesureMapping ; uomKey[unique=true] ; uomValue ;  
                                  ; 500g                ; GM       ;  
                                  ; Each                ; EA       ;  
                                  ; Jar                 ; EA       ;  
                                  ; Tray                ; PEC      ;  
                                  ; Unit                ; UN       ;  
                                  ; Box                 ; BOX      ;  
                                  ; Bag                 ; BG       ;  
                                  ; Piece               ; EA       ;  
                                  ; Bunch               ; EA       ;  
                                  ; Bottle              ; EA       ;  
                                  ; Kg                  ; KG       ;  
                                  ; Pack                ; PEC      ;
                                  ; Basket              ; BOX      ;  
                                  
INSERT_UPDATE  AldahraERPSalesOrderServiceProvider ; code[unique=true]    ; active[default=true] ; username ; password ; servicePoolName ; salesOrderPublicName ; stores(uid) ; shipSiteCode ; salesOrderSiteCode ; codCustomerCode ; cardCustomerCode ; salesOrderType ; unitsMapping(uomKey)                                              ; defaultUnitOfMeasure ; codPaymentType ; cardPaymentType ;  storeCreditSKU
                                                   ; sales-order-creation ;                      ; TEMA     ; QJJG6    ; PSGRND          ; YECCRESOH            ; $storeUid   ; U5Z80        ; U5H5               ; U10345          ; U10344           ; ORD            ; 500g,Each,Jar,Tray,Unit,Box,Bag,Piece,Bunch,Bottle,Kg,Pack,Basket ; EA                   ; UCAD           ; UDD             ;  E900436AE
                                                   
INSERT_UPDATE AldahraERPProductStockServiceProvider ; code[unique=true]          ; active[default=true] ; username ; password ; servicePoolName ; stockPublicName ; stockSiteCode ; stores(uid) ; enableSaveRecord ; enableSaveOldRecords ;  
                                                    ; product-stock-service      ;                      ; TEMA     ; QJJG6    ; PSGRND          ; YECITMSTK       ; U5Z80         ; $storeUid   ; true             ; false                ;  
                                                    ; product-stock-service-full ;                      ; TEMA     ; QJJG6    ; PSGRND          ; YECGETSTK       ; U5Z80         ; $storeUid   ; true             ; false                ;  
                                                    
INSERT_UPDATE SendPurchaseRequestForProductMinimumAmount ; code[unique=true]                ; $productCatalogVersion ; job(code)                 ; sessionLanguage(isoCode)[default=en] ; sessionCurrency(isocode)[default=AED] ; warehouses(code)[default=foodcrowd-ws] ; store(uid) ;  
                                                         ; foodcrowd-ae-SendPurchaseRequest ;                        ; productPurchaseRequestJob ;                                      ;                                       ;                                        ; $storeUid  ;  
                                                         
                                                         
INSERT_UPDATE AldahraERPProductPurchaseRequestServiceProvider ; code[unique=true]                    ; username ; password ; requestUserCode ; receivingSiteCode ; siteCode ; supplierCode ; defaultPurchaseQuantity ; active[default=true] ; servicePoolName ; schemaName ; unitsMapping(uomKey)                                              ; stores(uid) ;  
                                                              ; foodcrowd-ae-PurchaseRequestProvider ; TEMA     ; QJJG6    ; TEMA            ; U5Z90             ; U5Z90    ; U10059       ; 10                      ;                      ; PSGRND          ; YCREPSH    ; 500g,Each,Jar,Tray,Unit,Box,Bag,Piece,Bunch,Bottle,Kg,Pack,Basket ; $storeUid   ;  
 
 
 ################### Link nationalities to CMSSite ###################
               
INSERT_UPDATE Nationality ; code[unique=true] ; name[lang=en]                ; cmsSites(uid)
                          ; AE                ; "Emarati"                    ; $siteUid     
                          ; IN                ; "Indian"                     ; $siteUid     
                          ; RU                ; "Russian"                    ; $siteUid     
                          ; CN                ; "Chinese"                    ; $siteUid     
                          ; AU                ; "Australian"                 ; $siteUid     
                          ; BD                ; "Bangladeshi"                ; $siteUid     
                          ; BH                ; "Bahraini"                   ; $siteUid     
                          ; CH                ; "Swiss"                      ; $siteUid     
                          ; DK                ; "Danish"                     ; $siteUid     
                          ; DZ                ; "Algerian"                   ; $siteUid     
                          ; EG                ; "Egyptian"                   ; $siteUid     
                          ; ES                ; "Spanish"                    ; $siteUid     
                          ; ET                ; "Ethiopian"                  ; $siteUid     
                          ; FI                ; "Finnish"                    ; $siteUid     
                          ; FR                ; "French"                     ; $siteUid     
                          ; HK                ; "Hong Konger"                ; $siteUid     
                          ; HU                ; "Hungarian"                  ; $siteUid     
                          ; ID                ; "Indonesian"                 ; $siteUid     
                          ; IQ                ; "Iraqi"                      ; $siteUid     
                          ; DE                ; "German"                     ; $siteUid     
                          ; IR                ; "Iranian"                    ; $siteUid     
                          ; IT                ; "Italian"                    ; $siteUid     
                          ; JO                ; "Jordanian"                  ; $siteUid     
                          ; JP                ; "Japanese"                   ; $siteUid     
                          ; KE                ; "Kenyan"                     ; $siteUid     
                          ; KG                ; "Kyrgyz"                     ; $siteUid     
                          ; KW                ; "Kuwaiti"                    ; $siteUid     
                          ; LB                ; "Lebanese"                   ; $siteUid     
                          ; LK                ; "Sri Lankan"                 ; $siteUid     
                          ; MA                ; "Moroccan"                   ; $siteUid     
                          ; MC                ; "Monegasque"                 ; $siteUid     
                          ; MX                ; "Mexician"                   ; $siteUid     
                          ; RU                ; "Moscow"                     ; $siteUid     
                          ; NG                ; "Nigerian"                   ; $siteUid     
                          ; NL                ; "Dutch"                      ; $siteUid     
                          ; NO                ; "Norwegian"                  ; $siteUid     
                          ; NP                ; "Nepalese"                   ; $siteUid     
                          ; NZ                ; "New Zealander"              ; $siteUid     
                          ; OM                ; "Omani"                      ; $siteUid     
                          ; PH                ; "Filipino"                   ; $siteUid     
                          ; PK                ; "Pakistani"                  ; $siteUid     
                          ; PL                ; "Polish"                     ; $siteUid     
                          ; QA                ; "Qatari"                     ; $siteUid     
                          ; SA                ; "Saudi "                     ; $siteUid     
                          ; SD                ; "Sudanese"                   ; $siteUid     
                          ; SE                ; "Swedish"                    ; $siteUid     
                          ; SG                ; "Singaporean"                ; $siteUid     
                          ; SO                ; "Somali"                     ; $siteUid     
                          ; SY                ; "Syrian"                     ; $siteUid     
                          ; TH                ; "Thai"                       ; $siteUid     
                          ; TN                ; "Tunisian"                   ; $siteUid     
                          ; TR                ; "Turkish"                    ; $siteUid     
                          ; TW                ; "Taiwanese"                  ; $siteUid     
                          ; TZ                ; "Tanzanian"                  ; $siteUid     
                          ; UA                ; "Ukrainian"                  ; $siteUid     
                          ; GB                ; "British"                    ; $siteUid     
                          ; US                ; "American"                   ; $siteUid     
                          ; UZ                ; "Uzbek"                      ; $siteUid     
                          ; YE                ; "Yemeni"                     ; $siteUid     
                          ; ZA                ; "South African"              ; $siteUid     
                          ; 061               ; "Westerners"                 ; $siteUid     
                          ; 062               ; "Gcc Nationals"              ; $siteUid     
                          ; 063               ; "Other Asians"               ; $siteUid     
                          ; 064               ; "Other African"              ; $siteUid     
                          ; 065               ; "Other Arab Expat"           ; $siteUid     
                          ; 066               ; "Other South American"       ; $siteUid     
                          ; 067               ; "Other Indian Sub Continent" ; $siteUid     
                          ; CA                ; "Canadian"                   ; $siteUid     
                          ; MD                ; "Moldovan"                   ; $siteUid     
                          ; BT                ; "Bhutanese"                  ; $siteUid     
                          ; TJ                ; "Tajik"                      ; $siteUid     
                          ; HR                ; "Croatian"                   ; $siteUid     
                          ; BY                ; "Belarusian"                 ; $siteUid     
                          ; RO                ; "Romanian"                   ; $siteUid     
                          ; CY                ; "Cypriot"                    ; $siteUid     
                          ; BA                ; "Bosnian"                    ; $siteUid     
                          ; KZ                ; "Kazakh"                     ; $siteUid     
                          ; IE                ; "Irish"                      ; $siteUid     
                          ; CU                ; "Cuban"                      ; $siteUid     
                          ; BG                ; "Bulgarian"                  ; $siteUid     
                          ; AL                ; "Albanian"                   ; $siteUid     
                          ; LV                ; "Latvian"                    ; $siteUid     
                          ; MM                ; "Myanmar"                    ; $siteUid     
                          ; SD                ; "Sudanese"                   ; $siteUid     
                          ; TM                ; "Turkmen"                    ; $siteUid     
                          ; RS                ; "Serbian"                    ; $siteUid     
                          ; AM                ; "Armenian"                   ; $siteUid     
                          ; MK                ; "Macedonian"                 ; $siteUid     
                          ; LT                ; "Lithuanian"                 ; $siteUid     
                          ; BR                ; "Brazillian"                 ; $siteUid     
                          ; PS                ; "Palestinian"                ; $siteUid     
                          ; UG                ; "Ugandan"                    ; $siteUid     
                          ; PT                ; "Portuguese"                 ; $siteUid     
                          ; MY                ; "Malaysian"                  ; $siteUid     
                          ; VN                ; "Vietnamese"                 ; $siteUid     
                          ; LU                ; "Luxembourgian"              ; $siteUid     
                          ; AZ                ; "Azerbaijani"                ; $siteUid     
                          
                          
                          
#####################################################################
########## Al-Ain registration form configurations ##################
#####################################################################
                          
UPDATE CMSSite ; uid[unique=true] ; nationalityCustomerEnabled ; nationalityCustomerRequired ; nationalityCustomerHidden ;  
               ; $siteUid         ; true                       ; true                        ; false                     ;  
               
UPDATE CMSSite ; uid[unique=true] ; nationalityIdCustomerEnabled ; nationalityIdCustomerRequired ; nationalityIdCustomerHidden
               ; $siteUid         ; false                        ; false                         ; true                      ;  
               
UPDATE CMSSite ; uid[unique=true] ; birthOfDateCustomerEnabled ; birthOfDateCustomerRequired ; birthOfDateCustomerHidden
               ; $siteUid         ; true                       ; true                        ; false                    ;  
               
UPDATE CMSSite ; uid[unique=true] ; customerMaritalStatusEnabled ; customerMaritalStatusRequired ; customerMaritalStatusHidden ;  
               ; $siteUid         ; false                         ; false                          ; true                       ;  
               
#####################################################################
########## Al-Ain Add address form configurations ##################
#####################################################################
UPDATE CMSSite ; uid[unique=true] ; cityAddressEnabled ; cityAddressRequired ; cityAddressHidden ;  
               ; $siteUid         ; true               ; true                ; false             ;  
               
UPDATE CMSSite ; uid[unique=true] ; areaAddressEnabled ; areaAddressRequired ; areaAddressHidden ;  
               ; $siteUid         ; true               ; true                ; false             ;  
               
UPDATE CMSSite ; uid[unique=true] ; nearestLandmarkAddressEnabled ; nearestLandmarkAddressRequired ; nearestLandmarkAddressHidden ;  
               ; $siteUid         ; true                          ; false                           ; false                        ;  
               
UPDATE CMSSite ; uid[unique=true] ; buildingNameAddressEnabled ; buildingNameAddressRequired ; buildingNameAddressHidden ;  
               ; $siteUid         ; true                       ; true                        ; false                     ;  
               
UPDATE CMSSite ; uid[unique=true] ; floorNumberAddressEnabled ; floorNumberAddressRequired ; floorNumberAddressHidden ;  
               ; $siteUid         ; true                      ; false                       ; false                    ;  
               
UPDATE CMSSite ; uid[unique=true] ; apartmentNumberAddressEnabled ; apartmentNumberAddressRequired ; apartmentNumberAddressHidden ;  
               ; $siteUid         ; true                          ; false                           ; false                        ;  
               
UPDATE CMSSite ; uid[unique=true] ; moreInstructionsAddressEnabled ; moreInstructionsAddressRequired ; moreInstructionsAddressHidden ;  
               ; $siteUid         ; true                           ; false                            ; false                         ;  
               
#####################################################################
########## Natio ##################
#####################################################################
               
               
UPDATE CMSSite ; uid[unique=true] ; defaultCountryAddress(isocode) ; defaultDeliveryLocationCity(code) ; defaultDeliveryLocationArea(code) ;  
               ; $siteUid         ; AE                             ; ABU_DHABI                            ; ABU_DHABI_GATE_CITY                            ;  
               
UPDATE CMSSite ; uid[unique=true] ; showReOrderButton
               ; $siteUid         ; true                                                                          
