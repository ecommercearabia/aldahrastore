# -----------------------------------------------------------------------
# Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
# -----------------------------------------------------------------------
# Import essential data for the Accelerator
#
# Includes:
# * Languages
# * Currencies
# * Titles
# * Vendors
# * Warehouses
# * Supported Credit/Debit cards
# * User Groups
# * DistanceUnits for Storelocator
# * MediaFolders
# * MediaFormats
# * Tax & Tax Groups
# * Jobs

# Languages
INSERT_UPDATE Language;isocode[unique=true];fallbackLanguages(isocode);active[default=true]
;en;;;
;ar;en;;

# Currencies
INSERT_UPDATE Currency;isocode[unique=true];conversion;digits;symbol;base;active
;AED;1;2;AED;true;true
;USD;0.27;2;USD;false;false

# SiteMap Language Currencies
INSERT_UPDATE SiteMapLanguageCurrency;&siteMapLanguageCurrency;language(isoCode)[unique=true];currency(isocode)[unique=true];
;enAed;en;AED
;arAed;ar;AED

# Vendor
INSERT_UPDATE Vendor;code[unique=true];name
;foodcrowd;Food Crowd

INSERT Warehouse;code[unique=true];name;vendor(code)[default=foodcrowd, forceWrite=true];default[default=true, forceWrite=true]
"#% beforeEach:
import de.hybris.platform.core.Registry;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
String warehouseCode = line.get(Integer.valueOf(1));
WarehouseModel warehouse;
try
{
    warehouse = Registry.getApplicationContext().getBean(""warehouseService"").getWarehouseForCode(warehouseCode);
}
catch (Exception e)
{
    warehouse = null;
}
if (warehouse != null)
{
    line.clear();
}"
;foodcrowd-ws;Foodcrowd Warehouse ;

# Disable preview for email pages
UPDATE CMSPageType;code[unique=true];previewDisabled
;EmailPage;true

# Titles
INSERT_UPDATE Title;code[unique=true]
;mr
;mrs
;miss
;ms

# Media Folders
INSERT_UPDATE MediaFolder;qualifier[unique=true];path[unique=true]
;images;images
;email-body;email-body
;email-attachments;email-attachments
;documents;documents

# Media formats
INSERT_UPDATE MediaFormat;qualifier[unique=true]
;1200Wx1200H
;515Wx515H
;365Wx246H
;300Wx300H
;96Wx96H
;65Wx65H
;30Wx30H
;mobile
;tablet
;desktop
;widescreen

# Tax & Tax Groups
INSERT_UPDATE UserTaxGroup;code[unique=true]
;fc-ae-taxes

INSERT_UPDATE ProductTaxGroup;code[unique=true]
;fc-ae-vat-full


INSERT_UPDATE Tax;code[unique=true];value;currency(isocode)
;fc-ae-vat-full;5

INSERT_UPDATE ServicelayerJob;code[unique=true];springId[unique=true]
;cartRemovalJob;cartRemovalJob
;siteMapMediaJob;siteMapMediaJob



# Create oAuth2 clients
INSERT_UPDATE OAuthClientDetails;clientId[unique=true]	;resourceIds	;scope		;authorizedGrantTypes											;authorities			;clientSecret	;registeredRedirectUri
								;client-side			;hybris			;basic		;implicit,client_credentials									;ROLE_CLIENT			;secret			;http://localhost:9001/authorizationserver/oauth2_implicit_callback;
								;mobile_android			;hybris			;basic		;authorization_code,refresh_token,password,client_credentials,otp;ROLE_CLIENT			;secret			;http://localhost:9001/authorizationserver/oauth2_callback;
								;trusted_client			;hybris			;extended	;authorization_code,refresh_token,password,client_credentials	;ROLE_TRUSTED_CLIENT	;secret;		;
								;customer_group			;hybris			;basic		;authorization_code,refresh_token,password,client_credentials	;ROLE_CUSTOMERGROUP		;secret			;http://localhost:9001/authorizationserver/oauth2_callback;
								
# Titles           
INSERT_UPDATE Title ; code[unique=true] 
                    ; mr                
                    ; mrs               
                    ; miss              
                    ; ms  
