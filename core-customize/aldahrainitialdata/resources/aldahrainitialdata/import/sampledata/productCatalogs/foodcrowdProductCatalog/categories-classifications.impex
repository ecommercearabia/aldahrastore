# ImpEx for Importing Category Classifications
# -----------------------------------------------------------------------
# Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
# -----------------------------------------------------------------------
# ImpEx for Importing Category Classifications into Electronics Store

# Macros / Replacement Parameter definitions
$productCatalog=foodcrowdProductCatalog
$productCatalogName=Food Crowd Product Catalog
$catalogVersion=catalogversion(catalog(id[default=$productCatalog]),version[default='Staged'])[unique=true,default=$productCatalog:Staged]
$classCatalogVersion=catalogversion(catalog(id[default='FoodcrowdClassification']),version[default='1.0'])[unique=true,default='FoodcrowdClassification:1.0']
$classSystemVersion=systemVersion(catalog(id[default='FoodcrowdClassification']),version[default='1.0'])[unique=true]
$class=classificationClass(ClassificationClass.code,$classCatalogVersion)[unique=true]
$supercategories=source(code, $classCatalogVersion)[unique=true]
$categories=target(code, $catalogVersion)[unique=true]
$attribute=classificationAttribute(code,$classSystemVersion)[unique=true]
$unit=unit(code,$classSystemVersion)


$classCatalogVersionAE=catalogversion(catalog(id[default='FoodcrowdClassificationAE']),version[default='1.0'])[unique=true,default='FoodcrowdClassificationAE:1.0']
$classSystemVersionAE=systemVersion(catalog(id[default='FoodcrowdClassificationAE']),version[default='1.0'])[unique=true]
$classAE=classificationClass(ClassificationClass.code,$classCatalogVersionAE)[unique=true]
$supercategoriesAE=source(code, $classCatalogVersionAE)[unique=true]
$attributeAE=classificationAttribute(code,$classSystemVersionAE)[unique=true]
$unitAE=unit(code,$classSystemVersionAE)

INSERT_UPDATE ClassificationClass;$classCatalogVersion;code[unique=true];allowedPrincipals(uid)[default='customergroup']
;;FoodCrowdClassifications;

# Links ClassificationClasses to Categories
INSERT_UPDATE CategoryCategoryRelation;$categories;$supercategories
;1;FoodCrowdClassifications

# Insert Classification Attributes
INSERT_UPDATE ClassificationAttribute;$classSystemVersion;code[unique=true]
;;CountryOfOrigin
;;CountryOfOriginIsocode
;;StorageInstructions

INSERT_UPDATE ClassAttributeAssignment;$class;$attribute;position;attributeType(code[default=string]);multiValued[default=false];range[default=false];localized[default=true]
 ;FoodCrowdClassifications;CountryOfOrigin;1;;;;
 ;FoodCrowdClassifications;CountryOfOriginIsocode;;;;;false
 ;FoodCrowdClassifications;StorageInstructions;;;;;false
 
# Insert Classifications
INSERT_UPDATE ClassificationClass;$classCatalogVersionAE;code[unique=true];allowedPrincipals(uid)[default='customergroup']
;;FoodcrowdClassificationAE

 #	Links	ClassificationClasses	to	Categories
INSERT_UPDATE	CategoryCategoryRelation;$categories;$supercategoriesAE;
;1;FoodcrowdClassificationAE;

# Insert Classification Attributes
INSERT_UPDATE ClassificationAttribute;$classSystemVersionAE;code[unique=true]
;;ProductLabel

INSERT_UPDATE ClassAttributeAssignment;$classAE;$attributeAE                 ;position;$unitAE;attributeType(code[default=string]);multiValued[default=false];range[default=false];localized[default=true]
;FoodcrowdClassificationAE;ProductLabel;;;string;;;
 
## Organic
INSERT_UPDATE ClassificationAttribute;$classSystemVersion;code[unique=true]
;;Organic

INSERT_UPDATE ClassAttributeAssignment;$class;$attribute;position;attributeType(code[default=boolean]);multiValued[default=false];range[default=false];localized[default=true]
 ;FoodCrowdClassifications;Organic;2;;;;false
 
## Vegan
INSERT_UPDATE ClassificationAttribute;$classSystemVersion;code[unique=true]
;;Vegan
;;Frozen
;;Dry
;;Chilled

INSERT_UPDATE ClassAttributeAssignment;$class;$attribute;position;attributeType(code[default=boolean]);multiValued[default=false];range[default=false];localized[default=true]
 ;FoodCrowdClassifications;Vegan;3;;;;false
 ;FoodCrowdClassifications;Frozen;5;;;;false
 ;FoodCrowdClassifications;Dry;6;;;;false
 ;FoodCrowdClassifications;Chilled;7;;;;false
 
## Gluten Free
INSERT_UPDATE ClassificationAttribute;$classSystemVersion;code[unique=true]
;;GlutenFree

INSERT_UPDATE ClassAttributeAssignment;$class;$attribute;position;attributeType(code[default=boolean]);multiValued[default=false];range[default=false];localized[default=true]
 ;FoodCrowdClassifications;GlutenFree;4;;;;false
 