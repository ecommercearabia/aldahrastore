# -----------------------------------------------------------------------
# Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
# -----------------------------------------------------------------------
# ImpEx for SOLR Configuration

# Macros / Replacement Parameter definitions
$classSystemVersion=systemVersion(catalog(id[default='FoodcrowdClassification']),version[default='1.0'])
$classCatalogVersion=catalogVersion(catalog(id[default='FoodcrowdClassification']),version[default='1.0'])
$classAttribute=classificationAttribute(code,$classSystemVersion)
$classClass=classificationClass(code,$classCatalogVersion)
$classAttributeAssignment=classAttributeAssignment($classClass,$classAttribute,$classSystemVersion)
$contentCatalogName=foodcrowd-aeContentCatalog
$solrIndexedType=foodcrowd-aeProductType
$contentCatalogVersion=catalogVersion(CatalogVersion.catalog(Catalog.id[default=$contentCatalogName]),CatalogVersion.version[default=Staged])[default=$contentCatalogName:Staged]
$productCatalog=foodcrowdProductCatalog
$productCatalogVersion=catalogversion(catalog(id[default=$productCatalog]),version[default='Staged'])[unique=true,default=$productCatalog:Staged]


$classSystemVersionAE=systemVersion(catalog(id[default='FoodcrowdClassificationAE']),version[default='1.0'])
$classCatalogVersionAE=catalogVersion(catalog(id[default='FoodcrowdClassificationAE']),version[default='1.0'])
$classAttributeAE=classificationAttribute(code,$classSystemVersionAE)
$classClassAE=classificationClass(code,$classCatalogVersionAE)
$classAttributeAssignmentAE=classAttributeAssignment($classClassAE,$classAttributeAE,$classSystemVersionAE)


INSERT_UPDATE SolrIndexedProperty;solrIndexedType(identifier)[unique=true];name[unique=true];type(code);sortableType(code);currency[default=false];localized[default=false];multiValue[default=false];facet[default=false];facetType(code);facetSort(code);priority;visible;fieldValueProvider;customFacetSortProvider;rangeSets(name);$classAttributeAssignment
;$solrIndexedType;CountryOfOrigin;string;;;true;;true;MultiSelectOr;Alpha;1000;true;commerceClassificationPropertyValueProvider;;;FoodCrowdClassifications:::CountryOfOrigin::::
;$solrIndexedType;CountryOfOriginIsocode;string;;;false;;false;MultiSelectOr;Alpha;1000;true;commerceClassificationPropertyValueProvider;;;FoodCrowdClassifications:::CountryOfOriginIsocode::::
;$solrIndexedType;Organic;boolean;;;false;;false;MultiSelectOr;Alpha;1000;true;commerceClassificationPropertyValueProvider;;;FoodCrowdClassifications:::Organic::::
;$solrIndexedType;Vegan;boolean;;;false;;false;MultiSelectOr;Alpha;1000;true;commerceClassificationPropertyValueProvider;;;FoodCrowdClassifications:::Vegan::::
;$solrIndexedType;GlutenFree;boolean;;;false;;false;MultiSelectOr;Alpha;1000;true;commerceClassificationPropertyValueProvider;;;FoodCrowdClassifications:::GlutenFree::::
;$solrIndexedType;Frozen;boolean;;;false;;false;MultiSelectOr;Alpha;1000;true;commerceClassificationPropertyValueProvider;;;FoodCrowdClassifications:::Frozen::::
;$solrIndexedType;Dry;boolean;;;false;;false;MultiSelectOr;Alpha;1000;true;commerceClassificationPropertyValueProvider;;;FoodCrowdClassifications:::Dry::::
;$solrIndexedType;Chilled;boolean;;;false;;false;MultiSelectOr;Alpha;1000;true;commerceClassificationPropertyValueProvider;;;FoodCrowdClassifications:::Chilled::::


INSERT_UPDATE SolrSearchQueryProperty;indexedProperty(name, solrIndexedType(identifier))[unique=true];facet[default=true];facetType(code);priority;facetDisplayNameProvider;facetSortProvider;facetTopValuesProvider;includeInResponse[default=true];searchQueryTemplate(name, indexedType(identifier))[unique=true][default=DEFAULT:$solrIndexedType]
									 ;CountryOfOrigin:$solrIndexedType					;	;MultiSelectOr	;1000;	;								;;;
									 ;CountryOfOriginIsocode:$solrIndexedType					;false;MultiSelectOr	;1000;	;								;;;
									 ;Organic:$solrIndexedType							;	;MultiSelectOr	;1000;	;								;;;
									 ;Vegan:$solrIndexedType							;	;MultiSelectOr	;1000;	;								;;;
									 ;GlutenFree:$solrIndexedType							;	;MultiSelectOr	;1000;	;								;;;
									 ;Frozen:$solrIndexedType							;	;MultiSelectOr	;1000;	;								;;;
									 ;Dry:$solrIndexedType							;	;MultiSelectOr	;1000;	;								;;;
									 ;Chilled:$solrIndexedType							;	;MultiSelectOr	;1000;	;								;;;

INSERT_UPDATE SolrIndexedProperty;solrIndexedType(identifier)[unique=true];name[unique=true];type(code);sortableType(code);currency[default=false];localized[default=false];multiValue[default=false];facet[default=false];facetType(code);facetSort(code);priority;visible;fieldValueProvider;customFacetSortProvider;rangeSets(name);$classAttributeAssignmentAE
;$solrIndexedType;ProductLabel;string;; ;true; ;true;MultiSelectOr;Alpha;1000;true;commerceClassificationPropertyValueProvider;;;FoodcrowdClassificationAE:::ProductLabel::::

INSERT_UPDATE SolrSearchQueryProperty;indexedProperty(name, solrIndexedType(identifier))[unique=true];facet[default=true];facetType(code);priority;facetDisplayNameProvider;facetSortProvider;facetTopValuesProvider;includeInResponse[default=true];searchQueryTemplate(name, indexedType(identifier))[unique=true][default=DEFAULT:$solrIndexedType]
									 ;ProductLabel:$solrIndexedType				;	;MultiSelectOr	;1000;	;								;;;