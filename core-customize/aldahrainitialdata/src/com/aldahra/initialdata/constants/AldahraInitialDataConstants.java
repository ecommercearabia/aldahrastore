/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.initialdata.constants;

/**
 * Global class for all AldahraInitialData constants.
 */
public final class AldahraInitialDataConstants extends GeneratedAldahraInitialDataConstants
{
	public static final String EXTENSIONNAME = "aldahrainitialdata";

	private AldahraInitialDataConstants()
	{
		//empty
	}
}
