/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.aldahra.aldahrafulfillmentbackoffice.services;

/**
 * Hello World AldahrafulfillmentbackofficeService
 */
public class AldahrafulfillmentbackofficeService
{
	public String getHello()
	{
		return "Hello";
	}
}
