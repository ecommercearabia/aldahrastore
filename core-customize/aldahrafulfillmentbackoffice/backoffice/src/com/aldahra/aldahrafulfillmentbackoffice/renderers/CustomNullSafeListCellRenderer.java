/**
 *
 */
package com.aldahra.aldahrafulfillmentbackoffice.renderers;

import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.CollectionUtils;
import org.zkoss.zul.Listcell;

import com.hybris.cockpitng.core.config.impl.jaxb.listview.ListColumn;
import com.hybris.cockpitng.dataaccess.facades.type.DataType;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.widgets.common.WidgetComponentRenderer;


/**
 * @author mohammad
 *
 */
public class CustomNullSafeListCellRenderer implements WidgetComponentRenderer<Listcell, ListColumn, Object>
{
	private static final Logger LOG;
	private WidgetComponentRenderer<Listcell, ListColumn, Object> defaultListCellRenderer;
	private CustomNestedAttributeUtils nestedAttributeUtils;

	static
	{
		LOG = LoggerFactory.getLogger(CustomNullSafeListCellRenderer.class);
	}

	public void render(final Listcell parent, final ListColumn columnConfiguration, final Object object, final DataType dataType,
			final WidgetInstanceManager widgetInstanceManager)
	{
		final String qualifier = columnConfiguration.getQualifier();
		Object nestedObject = object;
		Object targetField = object;
		try
		{
			final List<String> tokenMap = this.getNestedAttributeUtils().splitQualifier(qualifier);
			for (int i = 0; i < tokenMap.size() - 1; ++i)
			{
				nestedObject = this.getNestedAttributeUtils().getNestedObject(nestedObject, tokenMap.get(i));
			}
			for (final String aTokenMap : tokenMap)
			{
				targetField = this.getNestedAttributeUtils().getNestedObject(targetField, aTokenMap);
			}
			if (nestedObject == null || targetField == null || this.checkIfObjectIsEmptyCollection(targetField))
			{
				LOG.info("Either Property {} is null or the field {} is null, skipping render of {}", new Object[]
				{ nestedObject, qualifier, qualifier });
			}
			else
			{
				this.getDefaultListCellRenderer().render(parent, columnConfiguration, object, dataType, widgetInstanceManager);
			}
		}
		catch (final Exception ex2)
		{
			LOG.error(ex2.getMessage(), ex2);
		}
	}

	protected WidgetComponentRenderer<Listcell, ListColumn, Object> getDefaultListCellRenderer()
	{
		return this.defaultListCellRenderer;
	}

	@Required
	public void setDefaultListCellRenderer(final WidgetComponentRenderer<Listcell, ListColumn, Object> defaultListCellRenderer)
	{
		this.defaultListCellRenderer = defaultListCellRenderer;
	}

	protected boolean checkIfObjectIsEmptyCollection(final Object object)
	{
		return object instanceof Collection && CollectionUtils.isEmpty((Collection) object);
	}

	protected CustomNestedAttributeUtils getNestedAttributeUtils()
	{
		return this.nestedAttributeUtils;
	}

	@Required
	public void setNestedAttributeUtils(final CustomNestedAttributeUtils nestedAttributeUtils)
	{
		this.nestedAttributeUtils = nestedAttributeUtils;
	}
}
