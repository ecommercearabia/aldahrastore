//
// Decompiled by Procyon v0.5.36
//

package com.aldahra.aldahrafulfillmentbackoffice.renderers;

public class InvalidNestedAttributeException extends Exception
{
	private static final long serialVersionUID = 9219802332908133366L;

	public InvalidNestedAttributeException(final String message)
	{
		super(message);
	}
}
