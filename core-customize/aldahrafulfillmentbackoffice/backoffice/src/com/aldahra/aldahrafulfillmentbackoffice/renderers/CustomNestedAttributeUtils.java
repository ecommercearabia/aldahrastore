/**
 *
 */
package com.aldahra.aldahrafulfillmentbackoffice.renderers;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.text.WordUtils;


/**
 * @author mohammad
 *
 */
public class CustomNestedAttributeUtils
{
	public static final String GETTER = "get";
	public static final String FAIL_TO_FIND_COLLECTION_ITEM = "Failed to find collection item for property ";
	private static final Pattern COLLECTION_PATTERN;
	private static final String END_BRACKET = "]";

	static
	{
		COLLECTION_PATTERN = Pattern.compile("^([^\\[]+)\\[(\\d+)\\]$");
	}

	public String propertyNameToGetter(final String propertyName)
	{
		final StringBuilder builder = new StringBuilder();
		builder.append("get");
		builder.append(WordUtils.capitalize(propertyName));
		return builder.toString();
	}

	public List<String> splitQualifier(final String qualifier)
	{
		final String[] tokenMap = qualifier.split("\\.");
		return Arrays.asList(tokenMap);
	}

	public Object getNestedObject(final Object object, final String propertyName)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, InvalidNestedAttributeException
	{
		if (propertyName.endsWith("]"))
		{
			return this.getArrayItem(object, propertyName);
		}
		return this.invokePropertyAsMethod(object, propertyName);
	}

	public String getNameOfClassWithoutModel(final Object object)
	{
		final String objectClass = object.getClass().getSimpleName();
		final String classWithoutModel = objectClass.substring(0, objectClass.length() - 5);
		return classWithoutModel;
	}

	public Object getArrayItem(final Object object, final String propertyName)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, InvalidNestedAttributeException
	{
		final Matcher matcher = CustomNestedAttributeUtils.COLLECTION_PATTERN.matcher(propertyName);
		if (!matcher.matches() || matcher.groupCount() != 2)
		{
			throw new InvalidNestedAttributeException("Failed to find collection item for property " + propertyName);
		}
		final String collectionProperty = matcher.group(1);
		final int index = Integer.parseInt(matcher.group(2));
		final Object collection = this.invokePropertyAsMethod(object, collectionProperty);
		if (collection == null || !(collection instanceof Collection))
		{
			throw new InvalidNestedAttributeException("Failed to find collection item for property " + propertyName);
		}
		int counter = 0;
		final Iterator iterator = ((Collection) collection).iterator();
		while (iterator.hasNext())
		{
			if (counter == index)
			{
				return iterator.next();
			}
			iterator.next();
			++counter;
		}
		throw new InvalidNestedAttributeException("Failed to find collection item for property " + propertyName);
	}

	public Object invokePropertyAsMethod(final Object object, final String propertyName)
			throws NoSuchMethodException, InvocationTargetException, IllegalAccessException
	{
		if (object == null)
		{
			return null;
		}
		final Class objectClass = object.getClass();
		final Method getter = objectClass.getMethod(this.propertyNameToGetter(propertyName), new Class[0]);
		return getter.invoke(object, (Object[]) null);
	}
}
