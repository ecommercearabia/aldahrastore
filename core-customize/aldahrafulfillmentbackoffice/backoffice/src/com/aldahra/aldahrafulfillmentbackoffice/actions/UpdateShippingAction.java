package com.aldahra.aldahrafulfillmentbackoffice.actions;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.EnumSet;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zul.Messagebox;

import com.aldahra.aldahrafulfillment.context.FulfillmentContext;
import com.aldahra.aldahrafulfillment.context.FulfillmentProviderContext;
import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.ActionResult.StatusFlag;
import com.hybris.cockpitng.actions.CockpitAction;
import com.hybris.cockpitng.engine.impl.AbstractComponentWidgetAdapterAware;


/**
 *
 * @author amjad.shati@erabia.com
 *
 */
public class UpdateShippingAction extends AbstractComponentWidgetAdapterAware
		implements CockpitAction<ConsignmentModel, ConsignmentModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(UpdateShippingAction.class);
	private static final String TRACKING_ID_NOT_FOUND = "Tracking id not found";


	@Resource(name = "fulfillmentContext")
	private FulfillmentContext fulfillmentContext;

	/**
	 * @return the fulfillmentProviderContext
	 */
	protected FulfillmentProviderContext getFulfillmentProviderContext()
	{
		return fulfillmentProviderContext;
	}

	@Resource(name = "fulfillmentProviderContext")
	private FulfillmentProviderContext fulfillmentProviderContext;

	/**
	 * @return the fulfillmentContext
	 */
	protected FulfillmentContext getFulfillmentContext()
	{
		return fulfillmentContext;
	}


	@Override
	public ActionResult<ConsignmentModel> perform(final ActionContext<ConsignmentModel> ctx)
	{
		final ActionResult<ConsignmentModel> actionResult = new ActionResult(ActionResult.SUCCESS);
		final ConsignmentModel consignment = ctx.getData();
		try
		{
			final Optional<String> trackingId = getFulfillmentContext().updateShipment(consignment);

			final EnumSet<StatusFlag> statusFlags = actionResult.getStatusFlags();
			statusFlags.add(StatusFlag.OBJECT_MODIFIED);
			actionResult.setStatusFlags(statusFlags);
			if (trackingId.isPresent())
			{
				Messagebox.show(String.format("Tracking ID : %s", trackingId.get()));
			}
			else
			{
				Messagebox.show(TRACKING_ID_NOT_FOUND);
			}


		}
		catch (final com.aldahra.aldahrafulfillment.exception.FulfillentException ex)
		{
			LOG.error(ex.getMessage());
			actionResult.setResultCode(ActionResult.ERROR);
			Messagebox.show(ex.getMessage());
		}

		return actionResult;
	}

	@Override
	public boolean canPerform(final ActionContext<ConsignmentModel> ctx)
	{
		final ConsignmentModel consignment = ctx.getData();
		return consignment != null && StringUtils.isNotEmpty(consignment.getTrackingID())
				&& consignment.getCarrierDetails() != null;
	}

	@Override
	public boolean needsConfirmation(final ActionContext<ConsignmentModel> ctx)
	{
		return true;
	}

	@Override
	public String getConfirmationMessage(final ActionContext<ConsignmentModel> ctx)
	{
		return "Do you want to update shipment details for this consignment?";
	}
}
