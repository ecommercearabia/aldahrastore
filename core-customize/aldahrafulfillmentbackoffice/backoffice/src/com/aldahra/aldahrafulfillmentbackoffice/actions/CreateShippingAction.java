package com.aldahra.aldahrafulfillmentbackoffice.actions;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.EnumSet;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zul.Messagebox;

import com.aldahra.aldahrafulfillment.context.FulfillmentContext;
import com.aldahra.aldahrafulfillment.context.FulfillmentProviderContext;
import com.aldahra.aldahrafulfillment.lyve.exception.LyveException;
import com.aldahra.aldahrafulfillment.lyve.exception.enums.LyveExceptionType;
import com.aldahra.aldahrafulfillment.lyve.service.LyveService;
import com.aldahra.core.enums.ShipmentType;
import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.ActionResult.StatusFlag;
import com.hybris.cockpitng.actions.CockpitAction;
import com.hybris.cockpitng.engine.impl.AbstractComponentWidgetAdapterAware;


/**
 *
 * @author Baha Almtoor
 *
 */
public class CreateShippingAction extends AbstractComponentWidgetAdapterAware
		implements CockpitAction<ConsignmentModel, ConsignmentModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(CreateShippingAction.class);
	private static final String TRACKING_ID_NOT_FOUND = "Tracking id not found";


	@Resource(name = "fulfillmentContext")
	private FulfillmentContext fulfillmentContext;

	/**
	 * @return the fulfillmentProviderContext
	 */
	protected FulfillmentProviderContext getFulfillmentProviderContext()
	{
		return fulfillmentProviderContext;
	}

	@Resource(name = "fulfillmentProviderContext")
	private FulfillmentProviderContext fulfillmentProviderContext;

	/**
	 * @return the fulfillmentContext
	 */
	protected FulfillmentContext getFulfillmentContext()
	{
		return fulfillmentContext;
	}

	@Resource(name = "lyveService")
	private LyveService lyveService;

	@Override
	public ActionResult<ConsignmentModel> perform(final ActionContext<ConsignmentModel> ctx)
	{
		final ActionResult<ConsignmentModel> actionResult = new ActionResult(ActionResult.SUCCESS);
		final ConsignmentModel consignment = ctx.getData();

		if (!isEnabledCreateShipment(consignment))
		{
			actionResult.setResultCode(ActionResult.ERROR);
			Messagebox.show("The shipment has been already created for this assignment");
			return actionResult;
		}
		try
		{
			final Optional<String> trackingId = getFulfillmentContext().createShipment(consignment);

			final EnumSet<StatusFlag> statusFlags = actionResult.getStatusFlags();
			statusFlags.add(StatusFlag.OBJECT_MODIFIED);
			actionResult.setStatusFlags(statusFlags);
			if (trackingId.isPresent())
			{
				Messagebox.show(String.format("Tracking ID : %s", trackingId.get()));
			}
			else
			{
				Messagebox.show(TRACKING_ID_NOT_FOUND);
			}


		}
		catch (final com.aldahra.aldahrafulfillment.exception.FulfillentException ex)
		{
			LOG.error(ex.getMessage());
			actionResult.setResultCode(ActionResult.ERROR);
			Messagebox.show(ex.getMessage());
		}

		return actionResult;
	}

	/**
	 * @param consignment
	 * @return
	 */
	private boolean isEnabledCreateShipment(final ConsignmentModel consignment)
	{
		if (consignment == null || consignment.getOrder() == null)
		{
			return false;
		}
		if (!consignment.getOrder().isExpress())
		{
			return true;
		}

		try
		{
			final Optional<String> status = fulfillmentContext.getStatus(consignment);
			return false;
		}
		catch (final Exception e)
		{
			if (e instanceof LyveException)
			{
				final LyveExceptionType type = ((LyveException) e).getType();
				if (type != null
						&& (LyveExceptionType.BAD_REQUEST.equals(type) || LyveExceptionType.SHIPMENT_NOT_CREATED.equals(type)))
				{
					return true;

				}

			}
		}
		return false;
	}


	@Override
	public boolean canPerform(final ActionContext<ConsignmentModel> ctx)
	{

		final ConsignmentModel consignment = ctx.getData();

		return consignment != null && consignment.getOrder() != null
				&& !ShipmentType.PICKUP_IN_STORE.equals(consignment.getOrder().getShipmentType());

	}

	@Override
	public boolean needsConfirmation(final ActionContext<ConsignmentModel> ctx)
	{
		return true;
	}

	@Override
	public String getConfirmationMessage(final ActionContext<ConsignmentModel> ctx)
	{
		if (StringUtils.isNotBlank(ctx.getData().getTrackingID()))
		{
			return "Do you want to create new shipment?";
		}
		else
		{
			return "Do you want to create shipment?";
		}
	}

	/**
	 * @return the lyveService
	 */
	protected LyveService getLyveService()
	{
		return lyveService;
	}


}
