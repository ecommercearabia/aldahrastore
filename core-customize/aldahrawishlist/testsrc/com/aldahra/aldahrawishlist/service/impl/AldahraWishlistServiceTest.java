/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrawishlist.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.data.PageableData;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.junit.Test;

import com.aldahra.aldahrawishlist.exception.WishlistException;


/**
 * @author mohammad-abu-muhasien
 */
@UnitTest
public class AldahraWishlistServiceTest
{

	private static final String WISHLIST_PK = "pk";
	private static final String newName = "newName";
	Optional<Wishlist2Model> wishlist2Model = Optional.of(new Wishlist2Model());
	@Resource(name = "aldahraWishlistService")
	private WishlistService aldahraWishlistService;


	/**
	 * Test method for
	 * {@link com.aldahra.aldahrawishlist.service.impl.DefaultWishlistService#removeAllWishlistEntries(java.lang.String)}.
	 *
	 * @throws WishlistException
	 */
	@Test(expected = WishlistException.class)
	public void testRemoveAllWishlistEntries() throws WishlistException
	{
		// call remove all wish list Entries
		aldahraWishlistService.removeAllWishlistEntries(WISHLIST_PK);
		assertTrue("WishList has been  Removed", true);
	}

	/**
	 * Test method for
	 * {@link com.aldahra.aldahrawishlist.service.impl.DefaultWishlistService#editWishlistName(java.lang.String, java.lang.String)}.
	 *
	 * @throws WishlistException
	 */
	@Test
	public void testEditWishlistName() throws WishlistException
	{
		when(aldahraWishlistService.editWishlistName(newName, WISHLIST_PK)).thenReturn(wishlist2Model);
		final Optional<Wishlist2Model> editWishlistName = aldahraWishlistService.editWishlistName(newName, WISHLIST_PK);
		assertEquals("two should be equaled", editWishlistName, wishlist2Model);
	}

	/**
	 * Test method for
	 * {@link com.aldahra.aldahrawishlist.service.impl.DefaultWishlistService#getWishlistEntries(java.lang.String, de.hybris.platform.cms2.data.PageableData)}.
	 *
	 * @throws WishlistException
	 */
	@Test
	public void testGetWishlistEntriesStringPageableData() throws WishlistException
	{
		final PageableData pageableData = new PageableData();
		when(aldahraWishlistService.getWishlistEntries(WISHLIST_PK, pageableData)).thenReturn(Optional.empty());
		final Optional<List<Wishlist2EntryModel>> wishlistEntries = aldahraWishlistService.getWishlistEntries(WISHLIST_PK,
				pageableData);
		assertEquals("The returend data should be empty", wishlistEntries, Optional.empty());
	}

	/**
	 * Test method for
	 * {@link com.aldahra.aldahrawishlist.service.impl.DefaultWishlistService#removeWishList(java.lang.String)}.
	 *
	 * @throws WishlistException
	 */
	@Test
	public void testRemoveWishList() throws WishlistException
	{
		aldahraWishlistService.removeWishList(WISHLIST_PK);
	}

	/**
	 * Test method for
	 * {@link com.aldahra.aldahrawishlist.service.impl.DefaultWishlistService#isProductInWishList(java.lang.String)}.
	 *
	 * @throws WishlistException
	 */
	@Test
	public void testIsProductInWishListString() throws WishlistException
	{
		when(aldahraWishlistService.isProductInWishList("Code")).thenReturn(true);
		final boolean productInWishList = aldahraWishlistService.isProductInWishList("Code");
		assertEquals("Two values should be equals", productInWishList, aldahraWishlistService.isProductInWishList("Code"));
	}



}
