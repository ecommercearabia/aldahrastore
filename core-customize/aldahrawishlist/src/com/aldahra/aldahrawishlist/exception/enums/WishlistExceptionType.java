/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrawishlist.exception.enums;

/**
 * @author mohammad-abu-muhasien
 */
public enum WishlistExceptionType
{

	WISHLIST_NOT_FOUND("wishlist_not_found"), LOGIN_REQUIRED("login_required"), MAX_CAPACITY("max_capacity"), PRODUCT_NOT_FOUND(
			"product_not_found"), PRODUCT_IS_EXIST("product_is_exist");

	private String type;

	private WishlistExceptionType(final String type)
	{
		this.type = type;
	}


	public String getMessage()
	{
		return type;
	}
}
