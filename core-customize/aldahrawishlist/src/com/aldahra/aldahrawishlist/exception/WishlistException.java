/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrawishlist.exception;

import com.aldahra.aldahrawishlist.exception.enums.WishlistExceptionType;




/**
 * The Class WishlistException.
 *
 * @author abu-muhasien
 *
 */
public class WishlistException extends Exception
{

	/** The type. */
	private final WishlistExceptionType type;

	/** The value. */
	private final String value;


	/**
	 * Instantiates a new wishlist exception.
	 *
	 * @param type
	 *           the type
	 * @param message
	 *           the message
	 * @param value
	 *           the value
	 */
	public WishlistException(final WishlistExceptionType type, final String message, final String value)
	{
		super(message);
		this.type = type;
		this.value = value;
	}



	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue()
	{
		return value;
	}

	/**
	 * Gets the wishlist exception type.
	 *
	 * @return the wishlist exception type
	 */
	public WishlistExceptionType getWishlistExceptionType()
	{
		return type;
	}



}
