/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrawishlist.service;

import de.hybris.platform.cms2.data.PageableData;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.wishlist2.Wishlist2Service;
import de.hybris.platform.wishlist2.enums.Wishlist2EntryPriority;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

import java.util.List;
import java.util.Optional;

import com.aldahra.aldahrawishlist.exception.WishlistException;


/**
 * @author mohammad-abu-muhasien
 */

public interface WishlistService extends Wishlist2Service
{

	public void addWishlistEntryWithCapacitySite(final String wishlistPK, final String productCode, final Integer desired,
			final Wishlist2EntryPriority entryPriority, final String comment, final CMSSiteModel cmsSiteModel)
			throws WishlistException;



	/**
	 * Returns wishlist for a given user
	 *
	 * @param wishlistPK
	 * @return
	 */
	public Optional<Wishlist2Model> getWishlistByPK(String wishlistPK) throws WishlistException;

	/**
	 * Removes all entries for a given wishlist model
	 *
	 * @param wishlistModel
	 */
	public void removeAllWishlistEntries(String wishlistPK) throws WishlistException;

	/**
	 * Changes the name of a given wishlist model
	 *
	 * @param newName
	 * @param wishlistPK
	 *
	 * @return Wishlist2Model
	 */
	public Optional<Wishlist2Model> editWishlistName(final String newName, final String wishlistPK) throws WishlistException;

	/**
	 * Gets all entries for a given wishlist and pagination settings
	 *
	 * @param wishlistPK
	 * @param pageableData
	 * @return
	 */
	public Optional<List<Wishlist2EntryModel>> getWishlistEntries(String wishlistPK, PageableData pageableData)
			throws WishlistException;


	/**
	 * Gets all entries for a given wishlist.
	 *
	 * @param wishlistPK
	 * @return
	 */
	public Optional<List<Wishlist2EntryModel>> getWishlistEntries(String wishlistPK) throws WishlistException;

	/**
	 * Removes wishlist for current user
	 *
	 * @param wishlist
	 */
	public void removeWishList(final String wishlistPK) throws WishlistException;

	/**
	 * Return if product is in wishlist.
	 *
	 * @param productCode
	 * @return boolean
	 */
	public boolean isProductInWishList(String productCode) throws WishlistException;

	/**
	 * Returns wishlists for the current user.
	 *
	 * @return Optional<List<Wishlist2Model>>
	 */
	public Optional<List<Wishlist2Model>> getWishListsByCurrentUser() throws WishlistException;

	/**
	 * Gets all entries for a given wishlist.
	 *
	 * @param productCode
	 * @param wishlistPK
	 *
	 * @return Optional<Wishlist2EntryModel>
	 */
	public Optional<Wishlist2EntryModel> getWishlistEntryByProductCode(String productCode, String wishlistPK)
			throws WishlistException;

	/**
	 * Removes wishlist entry by product code for given user
	 *
	 * @param productCode
	 */
	public void removeWishlistEntryForProduct(final String productCode) throws WishlistException;

	/**
	 * Removes wishlist entry by product code for given user
	 *
	 * @param productCode
	 * @param wishlistPk
	 */
	public void removeWishlistEntryForProduct(final String wishlistPk, final String productCode) throws WishlistException;

	/**
	 * Checks if is product in wish list.
	 *
	 * @param wishlistPK
	 *           the wishlist PK
	 * @param productCode
	 *           the product code
	 * @return true, if is product in wish list
	 */
	public boolean isProductInWishList(final String wishlistPK, final String productCode) throws WishlistException;



	/**
	 * Gets the wishlist entries count.
	 *
	 * @param wishlist
	 *           the wishlist
	 * @param pageableData
	 *           the pageable data
	 * @return the wishlist entries count
	 */
	public int getWishlistEntriesCount(final Wishlist2Model wishlist, final PageableData pageableData) throws WishlistException;
}