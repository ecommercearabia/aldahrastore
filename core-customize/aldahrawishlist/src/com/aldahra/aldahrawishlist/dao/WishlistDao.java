/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrawishlist.dao;

import de.hybris.platform.cms2.data.PageableData;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

import java.util.List;
import java.util.Optional;





/**
 * @author mohammad-abu-muhasien
 */
public interface WishlistDao
{

	/**
	 * Gets all the wishlist entries.
	 *
	 * @param wishlist
	 *           the wishlist
	 * @param pageableData
	 *           the pageable data
	 * @return the wishlist entries
	 */
	public Optional<List<Wishlist2EntryModel>> getWishlistEntries(Wishlist2Model wishlist, PageableData pageableData);

	/**
	 * Gets the wishlist entries.
	 *
	 * @param wishlist
	 *           the wishlist
	 * @return the wishlist entries
	 */
	public Optional<List<Wishlist2EntryModel>> getWishlistEntries(Wishlist2Model wishlist);



	/**
	 * Gets the wishlist entries count.
	 *
	 * @param wishlist
	 *           the wishlist
	 * @param pageableData
	 *           the pageable data
	 * @return the wishlist entries count
	 */
	public int getWishlistEntriesCount(Wishlist2Model wishlist, PageableData pageableData);

}
