/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrawishlist.dao.impl;

import de.hybris.platform.cms2.data.PageableData;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import com.aldahra.aldahrawishlist.dao.WishlistDao;





/**
 * @author mohammad-abu-muhasien
 */
public class DefaultWishlistDao implements WishlistDao
{

	/** The flexible search service. */
	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	/** The Constant WISHLIST_ENTRIES_QUERY. */
	protected static final String WISHLIST_ENTRIES_QUERY = "SELECT {pk} FROM {Wishlist2Entry} WHERE {wishlist} = ?wishlist";

	/** The Constant WISHLIST_QUERY_PARAMETER. */
	protected static final String WISHLIST_QUERY_PARAMETER = "wishlist";

	/**
	 * Gets the wishlist entries.
	 *
	 * @param wishlist
	 *           the wishlist
	 * @param pageableData
	 *           the pageable data
	 * @return the wishlist entries
	 */
	@Override
	public Optional<List<Wishlist2EntryModel>> getWishlistEntries(final Wishlist2Model wishlist, final PageableData pageableData)
	{
		final StringBuilder query = new StringBuilder(WISHLIST_ENTRIES_QUERY);
		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(query.toString());
		fQuery.addQueryParameter(WISHLIST_QUERY_PARAMETER, wishlist);
		fQuery.setNeedTotal(true);
		fQuery.setStart(pageableData.getCurrentPage() * pageableData.getPageSize());
		fQuery.setCount(pageableData.getPageSize());
		final SearchResult<Wishlist2EntryModel> result = getFlexibleSearchService().search(fQuery);
		return Optional.ofNullable(result.getResult());
	}

	/**
	 * Gets the wishlist entries.
	 *
	 * @param wishlist
	 *           the wishlist
	 * @return the wishlist entries
	 */
	@Override
	public Optional<List<Wishlist2EntryModel>> getWishlistEntries(final Wishlist2Model wishlist)
	{
		final StringBuilder query = new StringBuilder(WISHLIST_ENTRIES_QUERY);
		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(query.toString());
		fQuery.addQueryParameter(WISHLIST_QUERY_PARAMETER, wishlist);
		fQuery.setNeedTotal(true);
		final SearchResult<Wishlist2EntryModel> result = getFlexibleSearchService().search(fQuery);
		return Optional.ofNullable(result.getResult());
	}

	/**
	 * Gets the wishlist entries count.
	 *
	 * @param wishlist
	 *           the wishlist
	 * @param pageableData
	 *           the pageable data
	 * @return the wishlist entries count
	 */
	@Override
	public int getWishlistEntriesCount(final Wishlist2Model wishlist, final PageableData pageableData)
	{
		final StringBuilder query = new StringBuilder(WISHLIST_ENTRIES_QUERY);
		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(query.toString());
		fQuery.addQueryParameter(WISHLIST_QUERY_PARAMETER, wishlist);
		fQuery.setNeedTotal(true);
		fQuery.setStart(pageableData.getCurrentPage() * pageableData.getPageSize());
		fQuery.setCount(pageableData.getPageSize());
		final SearchResult<Wishlist2EntryModel> result = getFlexibleSearchService().search(fQuery);
		return result.getTotalCount();
	}


	/**
	 * Gets the flexible search service.
	 *
	 * @return the flexible search service
	 */
	protected FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

}
