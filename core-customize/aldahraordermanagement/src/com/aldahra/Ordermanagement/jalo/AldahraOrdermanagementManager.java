/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 */
package com.aldahra.Ordermanagement.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.aldahra.Ordermanagement.constants.AldahraOrdermanagementConstants;

@SuppressWarnings("PMD")
public class AldahraOrdermanagementManager extends GeneratedAldahraOrdermanagementManager
{
	public static final AldahraOrdermanagementManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (AldahraOrdermanagementManager) em.getExtension(AldahraOrdermanagementConstants.EXTENSIONNAME);
	}
	
}
