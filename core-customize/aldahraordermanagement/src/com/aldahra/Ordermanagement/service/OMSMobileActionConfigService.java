package com.aldahra.Ordermanagement.service;

import com.aldahra.aldahrafacades.dto.config.OMSActionConfigData;

import de.hybris.platform.core.model.order.OrderModel;

public interface OMSMobileActionConfigService
{

	boolean canPerformOMSMobileManualTaxVoidActionForOrder(OrderModel order);

	boolean canPerformOMSMobileManualTaxCommitActionForOrder(OrderModel order);

	boolean canPerformOMSMobileManualTaxRequoteActionForOrder(OrderModel order);

	boolean canPerformOMSMobileManualPaymentReauthactionActionForOrder(OrderModel order);

	boolean canPerformOMSMobileManualDeliveryCostCommitActionForOrder(OrderModel order);

	boolean canPerformOMSMobileCustomManualPaymentCaptureActionForOrder(OrderModel order);

	boolean canPerformOMSMobileCustomManualPaymentVoidActionForOrder(OrderModel order);

	boolean canPerformOMSMobileCancelOrderActionForOrder(OrderModel order);

	boolean canPerformOMSMobileCreateReturnRequestActionForOrder(OrderModel order);
	
}
