package com.aldahra.Ordermanagement.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.aldahra.core.service.CustomStockService;
import com.google.common.base.Preconditions;

import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.stock.StockService;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;
import de.hybris.platform.warehousing.data.allocation.DeclineEntry;
import de.hybris.platform.warehousing.inventoryevent.dao.InventoryEventDao;
import de.hybris.platform.warehousing.inventoryevent.service.InventoryEventService;
import de.hybris.platform.warehousing.model.AllocationEventModel;
import de.hybris.platform.warehousing.model.CancellationEventModel;
import de.hybris.platform.warehousing.model.IncreaseEventModel;
import de.hybris.platform.warehousing.model.InventoryEventModel;
import de.hybris.platform.warehousing.model.ShrinkageEventModel;
import de.hybris.platform.warehousing.model.WastageEventModel;
import de.hybris.platform.warehousing.stock.strategies.StockLevelSelectionStrategy;


public class DefaultCustomInventoryEventService implements InventoryEventService
{
	protected static final Logger LOGGER = LoggerFactory.getLogger(DefaultCustomInventoryEventService.class);
	private ModelService modelService;
	private InventoryEventDao inventoryEventDao;
	private StockLevelSelectionStrategy stockLevelSelectionStrategy;
	private CustomStockService stockService;
	private TimeService timeService;

	@Override
	public Collection<AllocationEventModel> getAllocationEventsForConsignmentEntry(ConsignmentEntryModel consignmentEntry)
	{
		ServicesUtil.validateParameterNotNullStandardMessage((String) "consignmentEntry", (Object) consignmentEntry);
		return this.getInventoryEventDao().getAllocationEventsForConsignmentEntry(consignmentEntry);
	}

	@Override
	public Collection<AllocationEventModel> getAllocationEventsForOrderEntry(OrderEntryModel orderEntry)
	{
		ServicesUtil.validateParameterNotNullStandardMessage((String) "orderEntry", (Object) orderEntry);
		return orderEntry.getConsignmentEntries().isEmpty() ? Collections.emptyList()
				: this.inventoryEventDao.getAllocationEventsForOrderEntry(orderEntry);
	}

	@Override
	public <T extends InventoryEventModel> Collection<T> getInventoryEventsForStockLevel(StockLevelModel stockLevel,
			Class<T> eventClassType)
	{
		ServicesUtil.validateParameterNotNullStandardMessage((String) "stocklevel", (Object) stockLevel);
		ServicesUtil.validateParameterNotNullStandardMessage((String) "eventClassType", eventClassType);
		return this.getInventoryEventDao().getInventoryEventsForStockLevel(stockLevel, eventClassType);
	}

	@Override
	public Collection<AllocationEventModel> createAllocationEvents(ConsignmentModel consignment)
	{
		ServicesUtil.validateParameterNotNullStandardMessage((String) "consignment", (Object) consignment);
		Preconditions.checkArgument((!consignment.getWarehouse().isExternal() ? 1 : 0) != 0,
				(Object) "External warehouses are not allowed to create AllocationEvent");
		List<AllocationEventModel> allocationEvents = consignment.getConsignmentEntries().stream()
				.map(consignmentEntryModel -> this
						.createAllocationEventsForConsignmentEntry((ConsignmentEntryModel) consignmentEntryModel))
				.flatMap(Collection::stream).collect(Collectors.toList());
		this.getModelService().saveAll(allocationEvents);
		return allocationEvents;
	}

	public List<AllocationEventModel> createAllocationEventsForConsignmentEntry(ConsignmentEntryModel consignmentEntry)
	{
		Preconditions.checkArgument((!consignmentEntry.getConsignment().getWarehouse().isExternal() ? 1 : 0) != 0,
				(Object) "External warehouses are not allowed to create AllocationEvent");
		LOGGER.debug("Creating allocation event for ConsignmentEntry :: Product [{}], at Warehouse [{}]: \tQuantity: '{}'",
				new Object[]
				{ consignmentEntry.getOrderEntry().getProduct().getCode(), consignmentEntry.getConsignment().getWarehouse().getCode(),
						consignmentEntry.getQuantity() });
		Collection stockLevels = this.getStockService().getStockLevels(consignmentEntry.getOrderEntry().getProduct(),
				Collections.singletonList(consignmentEntry.getConsignment().getWarehouse()));
		Map<StockLevelModel, Long> stockLevelForAllocation = this.getStockLevelSelectionStrategy()
				.getStockLevelsForAllocation(stockLevels, consignmentEntry.getQuantity());
		List<AllocationEventModel> allocationEvents = stockLevelForAllocation.entrySet().stream().map(stockMapEntry -> {
			AllocationEventModel allocationEvent = (AllocationEventModel) this.getModelService().create(AllocationEventModel.class);
			allocationEvent.setConsignmentEntry(consignmentEntry);
			StockLevelModel stockLevel = (StockLevelModel) stockMapEntry.getKey();
			allocationEvent.setStockLevel(stockLevel);
			allocationEvent.setStockLevelBinNumber(stockLevel.getBin());
			allocationEvent.setEventDate(this.getTimeService().getCurrentTime());
			allocationEvent.setQuantity(((Long) stockMapEntry.getValue()).longValue());
			this.getModelService().save((Object) allocationEvent);
			boolean isActive = consignmentEntry.getConsignment() != null && consignmentEntry.getConsignment().getOrder() != null
					&& consignmentEntry.getConsignment().getOrder().getStore() != null
					&& consignmentEntry.getConsignment().getOrder().getStore().isReservedAllocationEventForConsignmentEntry();
			if (isActive)
			{
				try
				{
					this.getStockService().reserve(stockMapEntry.getKey(), stockMapEntry.getValue().intValue());
				}
				catch (InsufficientStockLevelException e)
				{
					LOGGER.error(
							"DefaultCustomInventoryEventService ->  createAllocationEventsForConsignmentEntry -> InsufficientStockLevelException"
									+ e.getMessage(),
							e);
				}
			}
			return allocationEvent;
		}).collect(Collectors.toList());
		return allocationEvents;
	}

	@Override
	public ShrinkageEventModel createShrinkageEvent(ShrinkageEventModel shrinkageEventModel)
	{
		ServicesUtil.validateParameterNotNullStandardMessage((String) "stockLevel", (Object) shrinkageEventModel.getStockLevel());
		Preconditions.checkArgument((!shrinkageEventModel.getStockLevel().getWarehouse().isExternal() ? 1 : 0) != 0,
				(Object) "External warehouses are not allowed to create AllocationEvent");
		StockLevelModel stockLevel = shrinkageEventModel.getStockLevel();
		Long quantity = shrinkageEventModel.getQuantity();
		LOGGER.debug("Creating Shrinkage event for ConsignmentEntry :: Product [{}], at Warehouse [{}]: \tQuantity: '{}'",
				new Object[]
				{ stockLevel.getProductCode(), stockLevel.getWarehouse(), quantity });
		ShrinkageEventModel shrinkageEvent = (ShrinkageEventModel) this.getModelService().create(ShrinkageEventModel.class);
		shrinkageEvent.setStockLevel(stockLevel);
		shrinkageEvent.setEventDate(this.getTimeService().getCurrentTime());
		shrinkageEvent.setQuantity(quantity.longValue());
		shrinkageEvent.setComments(shrinkageEventModel.getComments());
		this.getModelService().save((Object) shrinkageEvent);
		return shrinkageEvent;
	}

	@Override
	public WastageEventModel createWastageEvent(WastageEventModel wastageEventModel)
	{
		ServicesUtil.validateParameterNotNullStandardMessage((String) "stockLevel", (Object) wastageEventModel.getStockLevel());
		Preconditions.checkArgument((!wastageEventModel.getStockLevel().getWarehouse().isExternal() ? 1 : 0) != 0,
				(Object) "External warehouses are not allowed to create AllocationEvent");
		StockLevelModel stockLevel = wastageEventModel.getStockLevel();
		Long quantity = wastageEventModel.getQuantity();
		LOGGER.debug("Creating Wastage event for ConsignmentEntry :: Product [{}], at Warehouse [{}]: \tQuantity: '{}'",
				new Object[]
				{ stockLevel.getProductCode(), stockLevel.getWarehouse(), quantity });
		WastageEventModel wastageEvent = (WastageEventModel) this.getModelService().create(WastageEventModel.class);
		wastageEvent.setStockLevel(stockLevel);
		wastageEvent.setEventDate(this.getTimeService().getCurrentTime());
		wastageEvent.setQuantity(quantity.longValue());
		this.getModelService().save((Object) wastageEvent);
		return wastageEvent;
	}

	@Override
	public List<CancellationEventModel> createCancellationEvents(CancellationEventModel cancellationEventModel)
	{
		ServicesUtil.validateParameterNotNullStandardMessage((String) "consignmentEntry",
				(Object) cancellationEventModel.getConsignmentEntry());
		Preconditions.checkArgument(
				(!cancellationEventModel.getConsignmentEntry().getConsignment().getWarehouse().isExternal() ? 1 : 0) != 0,
				(Object) "External warehouses are not allowed to create AllocationEvent");
		Collection<AllocationEventModel> allocationEvents = this
				.getAllocationEventsForConsignmentEntry(cancellationEventModel.getConsignmentEntry());
		Map<StockLevelModel, Long> stockLevelsForCancellation = this.getStockLevelSelectionStrategy()
				.getStockLevelsForCancellation(allocationEvents, cancellationEventModel.getQuantity());
		List<CancellationEventModel> cancellationEvents = stockLevelsForCancellation.entrySet().stream().map(stockMapEntry -> {
			CancellationEventModel cancellationEvent = (CancellationEventModel) this.getModelService()
					.create(CancellationEventModel.class);
			cancellationEvent.setConsignmentEntry(cancellationEventModel.getConsignmentEntry());
			cancellationEvent.setEventDate(this.getTimeService().getCurrentTime());
			cancellationEvent.setQuantity(((Long) stockMapEntry.getValue()).longValue());
			cancellationEvent.setStockLevel((StockLevelModel) stockMapEntry.getKey());
			cancellationEvent.setReason(cancellationEventModel.getReason());
			this.getModelService().save((Object) cancellationEvent);
			getStockService().release(stockMapEntry.getKey(), stockMapEntry.getValue().intValue());
			return cancellationEvent;
		}).collect(Collectors.toList());
		return cancellationEvents;
	}

	@Override
	public IncreaseEventModel createIncreaseEvent(IncreaseEventModel increaseEventModel)
	{
		ServicesUtil.validateParameterNotNullStandardMessage((String) "stockLevel", (Object) increaseEventModel.getStockLevel());
		Preconditions.checkArgument((!increaseEventModel.getStockLevel().getWarehouse().isExternal() ? 1 : 0) != 0,
				(Object) "External warehouses are not allowed to create AllocationEvent");
		IncreaseEventModel increaseEvent = (IncreaseEventModel) this.getModelService().create(IncreaseEventModel.class);
		increaseEvent.setEventDate(this.getTimeService().getCurrentTime());
		increaseEvent.setQuantity(increaseEventModel.getQuantity());
		increaseEvent.setStockLevel(increaseEventModel.getStockLevel());
		increaseEvent.setComments(increaseEventModel.getComments() != null ? increaseEventModel.getComments() : new ArrayList());
		this.getModelService().save((Object) increaseEvent);
		return increaseEvent;
	}

	@Override
	public void reallocateAllocationEvent(DeclineEntry declineEntry, Long quantityToDecline)
	{
		Preconditions.checkArgument((!declineEntry.getConsignmentEntry().getConsignment().getWarehouse().isExternal() ? 1 : 0) != 0,
				(Object) "External warehouses are not allowed to create AllocationEvent");
		Collection<AllocationEventModel> allocationEvents = this
				.getAllocationEventsForConsignmentEntry(declineEntry.getConsignmentEntry());
		if (declineEntry.getConsignmentEntry().getQuantity().equals(quantityToDecline))
		{
			this.getModelService().removeAll(allocationEvents);
		}
		else
		{
			Map<AllocationEventModel, Long> allocationEventsForReallocation = this
					.getAllocationEventsForReallocation(allocationEvents, quantityToDecline);
			for (Map.Entry<AllocationEventModel, Long> allocationMapEntry : allocationEventsForReallocation.entrySet())
			{
				AllocationEventModel allocationEvent = allocationMapEntry.getKey();
				if (allocationEvent.getQuantity() == allocationMapEntry.getValue().longValue())
				{
					this.getModelService().remove((Object) allocationEvent);
					continue;
				}
				allocationEvent.setQuantity(allocationEvent.getQuantity() - allocationMapEntry.getValue());
				this.getModelService().save((Object) allocationEvent);
			}
		}
	}

	@Override
	public Map<AllocationEventModel, Long> getAllocationEventsForReallocation(Collection<AllocationEventModel> allocationEvents,
			Long quantityToDecline)
	{
		LinkedHashMap<AllocationEventModel, Long> allocationMap = new LinkedHashMap<AllocationEventModel, Long>();
		long quantityLeftToDecline = quantityToDecline;
		List sortedAllocationEvents = allocationEvents.stream().sorted(Comparator
				.comparing(event -> event.getStockLevel().getReleaseDate(), Comparator.nullsLast(Comparator.reverseOrder())))
				.collect(Collectors.toList());
		Iterator it = sortedAllocationEvents.iterator();
		while (quantityLeftToDecline > 0L && it.hasNext())
		{
			AllocationEventModel allocationEvent = (AllocationEventModel) it.next();
			long declinableQty = allocationEvent.getQuantity();
			quantityLeftToDecline = this.addToAllocationMap(allocationMap, allocationEvent, quantityLeftToDecline, declinableQty);
		}
		this.finalizeStockMap(allocationMap, quantityLeftToDecline);
		return allocationMap;
	}

	protected long addToAllocationMap(Map<AllocationEventModel, Long> allocationMap, AllocationEventModel allocationEvent,
			long quantityLeft, Long quantityAvailable)
	{
		long quantityToFulfill = quantityLeft;
		if (quantityAvailable == null || quantityAvailable >= quantityToFulfill)
		{
			allocationMap.put(allocationEvent, quantityLeft);
			quantityToFulfill = 0L;
		}
		else if (quantityAvailable > 0L)
		{
			allocationMap.put(allocationEvent, quantityAvailable);
			quantityToFulfill -= quantityAvailable.longValue();
		}
		return quantityToFulfill;
	}

	protected void finalizeStockMap(Map<AllocationEventModel, Long> allocationMap, long quantityLeft)
	{
		if (quantityLeft > 0L && allocationMap.size() > 0)
		{
			Map.Entry<AllocationEventModel, Long> mapEntry = allocationMap.entrySet().iterator().next();
			allocationMap.put(mapEntry.getKey(), mapEntry.getValue() + quantityLeft);
		}
	}

	@Override
	public Collection<AllocationEventModel> getAllocationEventsForConsignment(ConsignmentModel consignment)
	{
		ServicesUtil.validateParameterNotNullStandardMessage((String) "consignment", (Object) consignment);
		return this.getInventoryEventDao().getAllocationEventsForConsignment(consignment);
	}

	protected ModelService getModelService()
	{
		return this.modelService;
	}

	@Required
	public void setModelService(ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected InventoryEventDao getInventoryEventDao()
	{
		return this.inventoryEventDao;
	}

	@Required
	public void setInventoryEventDao(InventoryEventDao inventoryEventDao)
	{
		this.inventoryEventDao = inventoryEventDao;
	}

	protected StockLevelSelectionStrategy getStockLevelSelectionStrategy()
	{
		return this.stockLevelSelectionStrategy;
	}

	@Required
	public void setStockLevelSelectionStrategy(StockLevelSelectionStrategy stockLevelSelectionStrategy)
	{
		this.stockLevelSelectionStrategy = stockLevelSelectionStrategy;
	}

	protected CustomStockService getStockService()
	{
		return this.stockService;
	}

	@Required
	public void setStockService(CustomStockService stockService)
	{
		this.stockService = stockService;
	}

	protected TimeService getTimeService()
	{
		return this.timeService;
	}

	@Required
	public void setTimeService(TimeService timeService)
	{
		this.timeService = timeService;
	}
}
