package com.aldahra.Ordermanagement.service.impl;

import java.util.List;
import java.util.Objects;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.CollectionUtils;

import com.aldahra.Ordermanagement.service.OMSMobileActionConfigService;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.ordercancel.OrderCancelService;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.returns.ReturnService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;


public class DefaultOMSMobileActionConfigService implements OMSMobileActionConfigService
{

	@Resource(name = "orderCancelService")
	private OrderCancelService orderCancelService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "omsConfigNotCancellableOrderStatus")
	private List<OrderStatus> notCancellableOrderStatus;

	@Resource(name = "returnService")
	private ReturnService returnService;

	@Override
	public boolean canPerformOMSMobileManualTaxVoidActionForOrder(OrderModel order)
	{
		BaseStoreModel baseStore = getAndValidateBaseStore(order);
		return !Objects.isNull(baseStore) && baseStore.isOmsMobileManualTaxVoidActionEnabled() && OrderStatus.TAX_NOT_VOIDED.equals(order.getStatus());
	}

	@Override
	public boolean canPerformOMSMobileManualTaxCommitActionForOrder(OrderModel order)
	{
		BaseStoreModel baseStore = getAndValidateBaseStore(order);
		return !Objects.isNull(baseStore) && baseStore.isOmsMobileManualTaxCommitActionEnabled() && OrderStatus.TAX_NOT_COMMITTED.equals(order.getStatus());
	}

	@Override
	public boolean canPerformOMSMobileManualTaxRequoteActionForOrder(OrderModel order)
	{
		BaseStoreModel baseStore = getAndValidateBaseStore(order);
		return !Objects.isNull(baseStore) && baseStore.isOmsMobileManualTaxRequoteActionEnabled() && OrderStatus.TAX_NOT_REQUOTED.equals(order.getStatus());
	}

	@Override
	public boolean canPerformOMSMobileManualPaymentReauthactionActionForOrder(OrderModel order)
	{
		BaseStoreModel baseStore = getAndValidateBaseStore(order);
		return !Objects.isNull(baseStore) && baseStore.isOmsMobileManualPaymentReauthactionActionEnabled() && OrderStatus.PAYMENT_NOT_AUTHORIZED.equals(order.getStatus());
	}

	@Override
	public boolean canPerformOMSMobileManualDeliveryCostCommitActionForOrder(OrderModel order)
	{
		BaseStoreModel baseStore = getAndValidateBaseStore(order);
		return !Objects.isNull(baseStore) && baseStore.isOmsMobileManualDeliveryCostCommitactionActionEnabled() && OrderStatus.TAX_NOT_COMMITTED.equals(order.getStatus());
	}

	@Override
	public boolean canPerformOMSMobileCustomManualPaymentCaptureActionForOrder(OrderModel order)
	{
		BaseStoreModel baseStore = getAndValidateBaseStore(order);
		return !Objects.isNull(baseStore) && baseStore.isOmsMobileCustomManualPaymentCaptureActionEnabled() && order.getPaymentInfo() != null && order.getPaymentInfo() instanceof CreditCardPaymentInfoModel
				&& !OrderStatus.PAYMENT_CAPTURED.equals(order.getStatus()) && !OrderStatus.CANCELLED.equals(order.getStatus())
				&& StringUtils.isBlank(order.getVersionID());
	}

	@Override
	public boolean canPerformOMSMobileCustomManualPaymentVoidActionForOrder(OrderModel order)
	{
		BaseStoreModel baseStore = getAndValidateBaseStore(order);
		return !Objects.isNull(baseStore) && baseStore.isOmsMobileCustommanualPaymentVoidActionEnabled() && order.getPaymentInfo() != null && order.getPaymentInfo() instanceof CreditCardPaymentInfoModel
				&& !OrderStatus.PAYMENT_CAPTURED.equals(order.getStatus()) && !OrderStatus.CANCELLED.equals(order.getStatus())
				&& StringUtils.isBlank(order.getVersionID());
	}

	@Override
	public boolean canPerformOMSMobileCancelOrderActionForOrder(OrderModel order)
	{
		BaseStoreModel baseStore = getAndValidateBaseStore(order);
		return !Objects.isNull(baseStore) && baseStore.isOmsMobileCancelOrderActionEnabled()
				&& !CollectionUtils.isEmpty(order.getEntries()) && this.getOrderCancelService()
						.isCancelPossible(order, this.getUserService().getCurrentUser(), true, true).isAllowed()
				&& !this.getNotCancellableOrderStatus().contains(order.getStatus());
	}

	@Override
	public boolean canPerformOMSMobileCreateReturnRequestActionForOrder(OrderModel order)
	{
		BaseStoreModel baseStore = getAndValidateBaseStore(order);
		return !Objects.isNull(baseStore) && baseStore.isOmsMobileCreateReturnreQuestActionEnabled() && this.containsConsignments(order)
				&& !order.getConsignments().stream()
						.noneMatch(consignment -> ConsignmentStatus.SHIPPED.equals((Object) consignment.getStatus())
								|| ConsignmentStatus.PICKUP_COMPLETE.equals((Object) consignment.getStatus()))
				&& !this.getReturnService().getAllReturnableEntries(order).isEmpty();
	}

	private boolean containsConsignments(final OrderModel order)
	{
		return order != null && !CollectionUtils.isEmpty(order.getEntries()) && !CollectionUtils.isEmpty(order.getConsignments());
	}
	
	protected BaseStoreModel getAndValidateBaseStore(OrderModel orderModel) {
		boolean isOrderNull = Objects.isNull(orderModel);
		boolean isBaseStoreNull = Objects.isNull(orderModel.getStore());
		return !isOrderNull && !isBaseStoreNull ? orderModel.getStore() : null;
	}

	protected OrderCancelService getOrderCancelService()
	{
		return this.orderCancelService;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	protected List<OrderStatus> getNotCancellableOrderStatus()
	{
		return notCancellableOrderStatus;
	}

	protected ReturnService getReturnService()
	{
		return this.returnService;
	}

}
