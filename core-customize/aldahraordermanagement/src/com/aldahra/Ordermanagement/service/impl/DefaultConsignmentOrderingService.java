package com.aldahra.Ordermanagement.service.impl;


import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;

import com.aldahra.Ordermanagement.breadcrum.builder.CustomProductBreadcrumbBuilder;
import com.aldahra.Ordermanagement.enums.ConsignmentOrderingType;
import com.aldahra.Ordermanagement.service.ConsignmentOrderingService;
import com.aldahra.core.enums.ConsignmentSortingType;

import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.store.services.BaseStoreService;


/***
 * 
 * @author husam.dababneh@erabia.com
 *
 */
public class DefaultConsignmentOrderingService implements ConsignmentOrderingService
{
	private static final String UNORDERED_ENTIRES = "UNORDERED_ENTIRES";

	private static final Logger LOG = Logger.getLogger(DefaultConsignmentOrderingService.class);


	@Resource(name = "categoryService")
	private CategoryService categoryService;


	@Resource(name = "customProductBreadcrumbBuilder")
	private CustomProductBreadcrumbBuilder customProductBreadcrumbBuilder;
	
	@Resource(name = "consignmentEntryPriorityComparator")
	private Comparator<ConsignmentEntryModel> consignmentEntryPriorityComparator;
	

	protected Comparator<ConsignmentEntryModel> getConsignmentEntryPriorityComparator()
	{
		return consignmentEntryPriorityComparator;
	}

	public List<ConsignmentEntryModel> orderConsignmentEntriesByPickSlipPriority(final ConsignmentModel consignmentModel,
			ConsignmentOrderingType type)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("consignmentModel", consignmentModel);
		ServicesUtil.validateParameterNotNullStandardMessage("type", type);

		if (consignmentModel.getOrder() == null || consignmentModel.getOrder().getStore() == null
				|| CollectionUtils.isEmpty(consignmentModel.getConsignmentEntries())
				|| (type.equals(ConsignmentOrderingType.PACK)
						&& !consignmentModel.getOrder().getStore().isEnablePackSlipPriorityReorder())
				|| (type.equals(ConsignmentOrderingType.PICK)
						&& !consignmentModel.getOrder().getStore().isEnablePickSlipPriorityReorder()))
		{

			return CollectionUtils.isEmpty(consignmentModel.getConsignmentEntries())?Collections.emptyList():List.copyOf(consignmentModel.getConsignmentEntries());
		}


		List<ConsignmentEntryModel> consignmentEntries = new ArrayList<> (consignmentModel.getConsignmentEntries());
		Collections.sort(consignmentEntries,  getConsignmentEntryPriorityComparator());
		return consignmentEntries;


	}

	public List<ConsignmentEntryModel> orderConsignmentEntriesByCategory(final ConsignmentModel consignmentModel,
			ConsignmentOrderingType type)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("consignmentModel", consignmentModel);
		ServicesUtil.validateParameterNotNullStandardMessage("type", type);

		if (consignmentModel.getOrder() == null || consignmentModel.getOrder().getStore() == null
				|| CollectionUtils.isEmpty(consignmentModel.getOrder().getStore().getProductCategoryPrioritySet())
				|| CollectionUtils.isEmpty(consignmentModel.getConsignmentEntries())
				|| (type.equals(ConsignmentOrderingType.PACK)
						&& !consignmentModel.getOrder().getStore().isEnablePackSlipPriorityReorder())
				|| (type.equals(ConsignmentOrderingType.PICK)
						&& !consignmentModel.getOrder().getStore().isEnablePickSlipPriorityReorder()))
		{
			return CollectionUtils.isEmpty(consignmentModel.getConsignmentEntries())?Collections.emptyList():List.copyOf(consignmentModel.getConsignmentEntries());
		}

		Set<CategoryModel> categories = consignmentModel.getOrder().getStore().getProductCategoryPrioritySet();
		LOG.debug("the ordered categories in baseStore  are : ");

		Set<ConsignmentEntryModel> consignmentEntries = consignmentModel.getConsignmentEntries();
		Map<String, List<ConsignmentEntryModel>> categoriesMap = fillCategoriesMap(categories);


		for (ConsignmentEntryModel entry : consignmentEntries)
		{
			boolean added = false;
			ProductModel productCode = entry.getOrderEntry().getProduct();
			List<CategoryModel> productCategories = customProductBreadcrumbBuilder.getBreadcrumbsByProduct(productCode);

			if (CollectionUtils.isEmpty(productCategories))
			{
				categoriesMap.get(UNORDERED_ENTIRES).add(entry);
				continue;
			}

			Collections.reverse(productCategories);

			for (CategoryModel category : productCategories)
			{
				if (categories.contains(category))
				{
					categoriesMap.get(category.getCode()).add(entry);
					added = true;
					break;
				}
			}


			if (!added)
			{
				categoriesMap.get(UNORDERED_ENTIRES).add(entry);
			}
		}

		Set<ConsignmentEntryModel> result = new LinkedHashSet<>();
		categoriesMap.values().stream().forEach(x -> x.stream().forEach(e -> result.add(e)));

		return  List.copyOf(result);


	}

	private Map<String, List<ConsignmentEntryModel>> fillCategoriesMap(Set<CategoryModel> categories)
	{
		Map<String, List<ConsignmentEntryModel>> map = new LinkedHashMap<>();
		for (CategoryModel categoryModel : categories)
		{
			map.put(categoryModel.getCode(), new LinkedList<>());
		}

		map.put(UNORDERED_ENTIRES, new LinkedList<>());

		return map;

	}


	protected CategoryService getCategoryService()
	{
		return categoryService;
	}




	protected void setCategoryService(CategoryService categoryService)
	{
		this.categoryService = categoryService;
	}



	protected CustomProductBreadcrumbBuilder getCustomProductBreadcrumbBuilder()
	{
		return customProductBreadcrumbBuilder;
	}




	protected void setCustomProductBreadcrumbBuilder(CustomProductBreadcrumbBuilder customProductBreadcrumbBuilder)
	{
		this.customProductBreadcrumbBuilder = customProductBreadcrumbBuilder;
	}

	@Override
	public List<ConsignmentEntryModel> orderConsignmentEntries(ConsignmentModel consignmentModel, ConsignmentOrderingType type)
	{
      if(consignmentModel ==null || CollectionUtils.isEmpty(consignmentModel.getConsignmentEntries()) || type==null)
      {
      	return Collections.emptyList();
      }
      
      
      ConsignmentSortingType consignmentSortingType= getConsignmentSortingType(consignmentModel, type);
      
      switch (consignmentSortingType)
		{
			case CATEGORY:
				return orderConsignmentEntriesByCategory(consignmentModel, type);
			case PRIORITY:
				return orderConsignmentEntriesByPickSlipPriority(consignmentModel, type);
			default:
				return  List.copyOf(consignmentModel.getConsignmentEntries());

		}

	}

	private ConsignmentSortingType getConsignmentSortingType(ConsignmentModel consignmentModel,ConsignmentOrderingType type)
	{
		if(consignmentModel ==null|| consignmentModel.getOrder()==null||consignmentModel.getOrder().getStore()==null || type==null )
      {
			return ConsignmentSortingType.DEFAULT;
      }
      else if(ConsignmentOrderingType.PICK.equals(type))
      {
      	return consignmentModel.getOrder().getStore().getPickConsignmentSortingType()==null? ConsignmentSortingType.DEFAULT:consignmentModel.getOrder().getStore().getPickConsignmentSortingType();
      }
      else if(ConsignmentOrderingType.PACK.equals(type))
      {
      	return consignmentModel.getOrder().getStore().getPackConsignmentSortingType()==null? ConsignmentSortingType.DEFAULT:consignmentModel.getOrder().getStore().getPackConsignmentSortingType();
      }
		return ConsignmentSortingType.DEFAULT;
	}

}
