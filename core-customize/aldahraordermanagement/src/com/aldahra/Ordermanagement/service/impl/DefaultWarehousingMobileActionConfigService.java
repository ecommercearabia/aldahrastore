package com.aldahra.Ordermanagement.service.impl;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;

import javax.annotation.Resource;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.CollectionUtils;

import com.aldahra.Ordermanagement.service.WarehousingMobileActionConfigService;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.commerceservices.model.PickUpDeliveryModeModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.warehousing.shipping.service.WarehousingShippingService;


public class DefaultWarehousingMobileActionConfigService implements WarehousingMobileActionConfigService
{

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "warehousingShippingService")
	private WarehousingShippingService warehousingShippingService;
	
	@Resource(name="reallocableConsignmentStatuses")
	private List<ConsignmentStatus>  reallocableConsignmentStatuses;
	
	@Override
	public boolean canPerformWarehousingMobilePickSlipActionConfig(ConsignmentModel consignmentModel)
	{
		return !Objects.isNull(consignmentModel) && consignmentModel.getFulfillmentSystemConfig() == null;
	}

	@Override
	public boolean canPerformWarehousingMobilePackSlipActionConfig(ConsignmentModel consignmentModel)
	{
		BaseStoreModel baseStore = getAndValidateBaseStore(consignmentModel);
		return !Objects.isNull(baseStore) && baseStore.isWarehousingMobilePackSlipActionEnabled() && configurationService.getConfiguration().getBoolean("warehousing.capturepaymentonconsignment", false)
				&& !ConsignmentStatus.CANCELLED.equals(consignmentModel.getStatus())
				&& consignmentModel.getFulfillmentSystemConfig() == null;
	}

	@Override
	public boolean canPerformWarehousingMobileCreateShipmentActionConfig(ConsignmentModel consignmentModel)
	{
		BaseStoreModel baseStore = getAndValidateBaseStore(consignmentModel);
		return !Objects.isNull(baseStore) && baseStore.isWarehousingMobileCreateShipmentActionEnabled();
	}

	@Override
	public boolean canPerformWarehousingMobileConfirmShipmentActionConfig(ConsignmentModel consignmentModel)
	{
		BaseStoreModel baseStore = getAndValidateBaseStore(consignmentModel);
		return !Objects.isNull(baseStore) && baseStore.isWarehousingMobileConfirmShipmentActionEnabled() && !(consignmentModel.getDeliveryMode() instanceof PickUpDeliveryModeModel)
				&& consignmentModel.getFulfillmentSystemConfig() == null
				&& warehousingShippingService.isConsignmentConfirmable(consignmentModel);
	}

	@Override
	public boolean canPerformWarehousingMobilePrintShipmentAwbActionConfig(ConsignmentModel consignmentModel)
	{
		BaseStoreModel baseStore = getAndValidateBaseStore(consignmentModel);
		return !Objects.isNull(baseStore) && baseStore.isWarehousingMobilePrintShipmentAwbActionEnabled() && consignmentModel.getCarrierDetails() != null;
	}

	@Override
	public boolean canPerformWarehousingMobileReallocateActionConfig(ConsignmentModel consignmentModel)
	{
      boolean decision = false;
      boolean captureOnConsignmentReallocationAllowed = true;
       if (!Objects.isNull(consignmentModel) && !CollectionUtils.isEmpty(consignmentModel.getConsignmentEntries()) && !(consignmentModel.getDeliveryMode() instanceof PickUpDeliveryModeModel) && consignmentModel.getFulfillmentSystemConfig() == null) {
           decision = consignmentModel.getConsignmentEntries().stream().anyMatch(consignmentEntry -> consignmentEntry.getQuantityPending() > 0L);
       }
       if (configurationService.getConfiguration().getBoolean("warehousing.capturepaymentonconsignment", Boolean.FALSE)) {
           captureOnConsignmentReallocationAllowed = reallocableConsignmentStatuses.contains(consignmentModel.getStatus());
       }
       BaseStoreModel baseStore = getAndValidateBaseStore(consignmentModel);
 		return !Objects.isNull(baseStore) && baseStore.isWarehousingMobileReallocateActionEnabled() && decision && captureOnConsignmentReallocationAllowed;
	}

	@Override
	public boolean canPerformWarehousingMobileUpdateShipmentActionConfig(ConsignmentModel consignmentModel)
	{
		BaseStoreModel baseStore = getAndValidateBaseStore(consignmentModel);
		return !Objects.isNull(baseStore) && baseStore.isWarehousingMobileUpdateShipmentActionEnabled() && StringUtils.isNotEmpty(consignmentModel.getTrackingID())
				&& consignmentModel.getCarrierDetails() != null;
	}

	@Override
	public boolean canPerformWarehousingMobilePrintReturnShippingLabelActionConfig(ConsignmentModel consignmentModel)
	{
		boolean result = false;
		Configuration configuration = configurationService.getConfiguration();
      
      try {
         if (configuration != null) {
            result = configuration.getBoolean("warehousing.printreturnshippinglabel.active");
         }
      } catch (NoSuchElementException | ConversionException ex) {
         return false;
      }
      BaseStoreModel baseStore = getAndValidateBaseStore(consignmentModel);
		return !Objects.isNull(baseStore) && baseStore.isWarehousingMobilePrintReturnShippingLabelActionEnabled() && result && consignmentModel.getFulfillmentSystemConfig() == null;
	}

	@Override
	public boolean canPerformWarehousingMobilePrintReturnFormActionConfig(ConsignmentModel consignmentModel)
	{
		boolean result = false;
      Configuration configuration = configurationService.getConfiguration();

      try {
         if (configuration != null) {
            result = configuration.getBoolean("warehousing.printreturnform.active");
         }
      } catch (NoSuchElementException | ConversionException var4) {
      	return false;
      }
      BaseStoreModel baseStore = getAndValidateBaseStore(consignmentModel);
		return !Objects.isNull(baseStore) && baseStore.isWarehousingMobilePrintReturnFormActionEnabled() && result && consignmentModel.getFulfillmentSystemConfig() == null;
	}

	@Override
	public boolean canPerformWarehousingMobileExportFormsActionConfig(ConsignmentModel consignmentModel)
	{
		BaseStoreModel baseStore = getAndValidateBaseStore(consignmentModel);
		return !Objects.isNull(baseStore) && baseStore.isWarehousingMobileExportFormsActionEnabled() && consignmentModel.getFulfillmentSystemConfig() == null;
	}
	
	protected BaseStoreModel getAndValidateBaseStore(ConsignmentModel consignmentModel) {
		boolean isConsignmentNull = Objects.isNull(consignmentModel);
		boolean isOrderNull = Objects.isNull(consignmentModel.getOrder());
		boolean isBaseStoreNull = Objects.isNull(consignmentModel.getOrder().getStore());
		return !isConsignmentNull && !isOrderNull && !isBaseStoreNull ? consignmentModel.getOrder().getStore() : null;
	}
	
	

}
