package com.aldahra.Ordermanagement.service;

import java.util.List;
import java.util.Set;

import com.aldahra.Ordermanagement.enums.ConsignmentOrderingType;

import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;


public interface ConsignmentOrderingService
{
	public List<ConsignmentEntryModel> orderConsignmentEntriesByCategory(final ConsignmentModel consignmentModel,ConsignmentOrderingType type);
	
	public List<ConsignmentEntryModel> orderConsignmentEntriesByPickSlipPriority(final ConsignmentModel consignmentModel,ConsignmentOrderingType type);
	
	public List<ConsignmentEntryModel> orderConsignmentEntries(final ConsignmentModel consignmentModel,ConsignmentOrderingType type);


}
