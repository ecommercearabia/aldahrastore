package com.aldahra.Ordermanagement.service;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;

/**
 * @author monzer
 *
 */
public interface WarehousingMobileActionConfigService
{
	boolean canPerformWarehousingMobilePickSlipActionConfig(ConsignmentModel consignmentModel);
	
	boolean canPerformWarehousingMobilePackSlipActionConfig(ConsignmentModel consignmentModel);
	
	boolean canPerformWarehousingMobileCreateShipmentActionConfig(ConsignmentModel consignmentModel);
	
	boolean canPerformWarehousingMobileConfirmShipmentActionConfig(ConsignmentModel consignmentModel);
	
	boolean canPerformWarehousingMobilePrintShipmentAwbActionConfig(ConsignmentModel consignmentModel);
	
	boolean canPerformWarehousingMobileReallocateActionConfig(ConsignmentModel consignmentModel);
	
	boolean canPerformWarehousingMobileUpdateShipmentActionConfig(ConsignmentModel consignmentModel);
	
	boolean canPerformWarehousingMobilePrintReturnShippingLabelActionConfig(ConsignmentModel consignmentModel);
	
	boolean canPerformWarehousingMobilePrintReturnFormActionConfig(ConsignmentModel consignmentModel);
	
	boolean canPerformWarehousingMobileExportFormsActionConfig(ConsignmentModel consignmentModel);
	
}
