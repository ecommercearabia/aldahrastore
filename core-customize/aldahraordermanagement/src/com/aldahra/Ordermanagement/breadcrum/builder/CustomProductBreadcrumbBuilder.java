/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.Ordermanagement.breadcrum.builder;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Required;

import com.aldahra.Ordermanagement.breadcrum.history.CustomBrowseHistory;
import com.aldahra.Ordermanagement.breadcrum.model.Breadcrumb;
import com.google.common.base.Preconditions;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.helper.ProductAndCategoryHelper;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.variants.model.VariantProductModel;


/**
 * ProductBreadcrumbBuilder implementation for {@link ProductData}
 */
public class CustomProductBreadcrumbBuilder
{
	private static final String LAST_LINK_CLASS = "active";

	private UrlResolver<ProductModel> productModelUrlResolver;
	private UrlResolver<CategoryModel> categoryModelUrlResolver;
	@Resource(name="customBrowseHistory")
	private CustomBrowseHistory customBrowseHistory;
	private ProductService productService;
	private ProductAndCategoryHelper productAndCategoryHelper;

	/**
	 * Returns a list of breadcrumbs for the given product.
	 *
	 * @param productCode
	 * @return breadcrumbs for the given product
	 */
	public List<CategoryModel> getBreadcrumbs(final String productCode)
	{
		final ProductModel productModel = getProductService().getProductForCode(productCode);
		Preconditions.checkArgument(productModel != null, "Product cannot be empty!");
		return getBreadcrumbsByProduct(productModel);
	}
	
	public List<CategoryModel> getBreadcrumbsByProduct(final ProductModel productModel)
	{
		Preconditions.checkArgument(productModel != null, "Product cannot be empty!");
		final List<CategoryModel> breadcrumbs = new LinkedList<>();

		final Collection<CategoryModel> categoryModels = new LinkedList<>();
		final ProductModel baseProductModel = getProductAndCategoryHelper().getBaseProduct(productModel);
		categoryModels.addAll(baseProductModel.getSupercategories());


		while (!categoryModels.isEmpty())
		{
			CategoryModel toDisplay = null;
			toDisplay = processCategoryModels(categoryModels, toDisplay);
			categoryModels.clear();
			if (toDisplay != null)
			{
				breadcrumbs.add(toDisplay);
				categoryModels.addAll(toDisplay.getSupercategories());
			}
		}
		Collections.reverse(breadcrumbs);
		breadcrumbs.remove(0);
		return new LinkedList<CategoryModel>(breadcrumbs);
	}

	protected CategoryModel processCategoryModels(final Collection<CategoryModel> categoryModels, final CategoryModel toDisplay)
	{
		CategoryModel categoryToDisplay = toDisplay;

		for (final CategoryModel categoryModel : categoryModels)
		{
			if (getProductAndCategoryHelper().isValidProductCategory(categoryModel))
			{
				if (categoryToDisplay == null)
				{
					categoryToDisplay = categoryModel;
				}
				if (getCustomBrowseHistory().findEntryMatchUrlEndsWith(categoryModel.getCode()) != null)
				{
					break;
				}
			}
		}
		return categoryToDisplay;
	}

	/**
	 * @deprecated As of 1905 - use {@link ProductAndCategoryHelper#getBaseProduct(ProductModel)} instead
	 */
	@Deprecated
	protected ProductModel getBaseProduct(final ProductModel product)
	{
		if (product instanceof VariantProductModel)
		{
			return getBaseProduct(((VariantProductModel) product).getBaseProduct());
		}
		return product;
	}

	protected Breadcrumb getProductBreadcrumb(final ProductModel product)
	{
		final String productUrl = getProductModelUrlResolver().resolve(product);
		return new Breadcrumb(productUrl, product.getName(), null);
	}

	protected String getCategoryBreadcrumb(final CategoryModel category)
	{
		final String categoryUrl = getCategoryModelUrlResolver().resolve(category);
		return category.getName();
	}

	protected UrlResolver<ProductModel> getProductModelUrlResolver()
	{
		return productModelUrlResolver;
	}

	@Required
	public void setProductModelUrlResolver(final UrlResolver<ProductModel> productModelUrlResolver)
	{
		this.productModelUrlResolver = productModelUrlResolver;
	}

	protected UrlResolver<CategoryModel> getCategoryModelUrlResolver()
	{
		return categoryModelUrlResolver;
	}

	@Required
	public void setCategoryModelUrlResolver(final UrlResolver<CategoryModel> categoryModelUrlResolver)
	{
		this.categoryModelUrlResolver = categoryModelUrlResolver;
	}



	protected CustomBrowseHistory getCustomBrowseHistory()
	{
		return customBrowseHistory;
	}
	@Required
	protected void setCustomBrowseHistory(CustomBrowseHistory customBrowseHistory)
	{
		this.customBrowseHistory = customBrowseHistory;
	}

	protected ProductService getProductService()
	{
		return productService;
	}

	@Required
	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}

	protected ProductAndCategoryHelper getProductAndCategoryHelper()
	{
		return productAndCategoryHelper;
	}

	@Required
	public void setProductAndCategoryHelper(final ProductAndCategoryHelper productAndCategoryHelper)
	{
		this.productAndCategoryHelper = productAndCategoryHelper;
	}



}
