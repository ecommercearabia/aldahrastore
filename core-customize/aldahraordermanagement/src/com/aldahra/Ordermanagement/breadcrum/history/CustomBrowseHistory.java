/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.Ordermanagement.breadcrum.history;

/**
 */
public interface CustomBrowseHistory
{
	/**
	 * Adds the url to the browsing history.
	 *
	 * @param browseHistoryEntry the {@link BrowseHistoryEntry} that will be added to the history
	 */
	void addBrowseHistoryEntry(CustomBrowseHistoryEntry browseHistoryEntry);
	
	CustomBrowseHistoryEntry findEntryMatchUrlEndsWith(String match);
}
