package com.aldahra.Ordermanagement.context;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.apache.logging.log4j.util.Strings;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import com.aldahra.Ordermanagement.enums.ConsignmentOrderingType;
import com.aldahra.Ordermanagement.service.ConsignmentOrderingService;
import com.aldahra.aldahraproduct.model.GroceryVariantProductModel;

import de.hybris.platform.acceleratorservices.model.cms2.pages.DocumentPageModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.servicelayer.model.ItemModelInternalContext;
import de.hybris.platform.solrfacetsearch.search.FacetValue;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.warehousing.model.AllocationEventModel;



/**
 * @author mohammad-abumuhasien
 */
public class CustomPickContext extends de.hybris.platform.warehousing.labels.context.PickSlipContext
{
	private static final Logger LOG = Logger.getLogger(CustomPickContext.class);

	@Resource(name = "orderFacade")
	private OrderFacade orderFacade;


	@Resource(name = "defaultConsignmentOrderingService")
	private ConsignmentOrderingService consignmentOrderingService;


	private Set<ConsignmentEntryModel> consignmentEntries;
	private OrderData orderData;
	private static final String LOGO = "/_ui/responsive/theme-foodcrowd/images/express.png";

	@Override
	public void init(final ConsignmentProcessModel businessProcessModel, final DocumentPageModel documentPageModel)
	{
		super.init(businessProcessModel, documentPageModel);
		orderData = orderFacade.getOrderDetailsForCode(getOrder().getCode());
		updateConsigmentEntriesOrdering(businessProcessModel.getConsignment());
	}

	public String getProductName(final ProductModel productModel)
	{
		return productModel.getName(Locale.ENGLISH);
	}
	
	public String getCityName(ConsignmentModel consignmentModel)
	{
		return consignmentModel.getShippingAddress().getCity().getName(Locale.ENGLISH);
	}
	public String getAreaName(ConsignmentModel consignmentModel)
	{
		return consignmentModel.getShippingAddress().getArea().getName(Locale.ENGLISH);
	}
	public String getUOMCode(final ProductModel productModel)
	{
		
		
		if (productModel instanceof GroceryVariantProductModel)
		{
			final GroceryVariantProductModel variant = (GroceryVariantProductModel) productModel;
			return variant.getUnitOfMeasure(Locale.ENGLISH);
		}
		return null;

	}

	private void updateConsigmentEntriesOrdering(final ConsignmentModel consignmentModel)
	{
		List<ConsignmentEntryModel> entries = getConsignmentOrderingService().orderConsignmentEntries(consignmentModel,
				ConsignmentOrderingType.PICK);
		
		for (int i = 0; i < entries.size(); i++)
		{
			System.out.println(entries.get(i).getOrderEntry().getProduct().getCode());
		}
		setConsignmentEntries( new LinkedHashSet<>(entries));
	}

	public String extractLot(final ConsignmentEntryModel consignmentEntryModel)
	{
		String lotNumber = "";
		final StringBuilder lots = new StringBuilder();
		final Collection<AllocationEventModel> events = getInventoryEventService()
				.getAllocationEventsForConsignmentEntry(consignmentEntryModel);
		events.stream().filter(e -> e.getStockLevel() != null && e.getStockLevel().getLotNumber() != null).forEach(e -> {
			lots.append(e.getStockLevel().getLotNumber()).append(",");
		});
		if (lots.length() > 0)
		{
			lotNumber = lots.substring(0, lots.length() - 1);
		}
		return lotNumber;
	}

	public String extractBin(ConsignmentEntryModel consignmentEntryModel)
	{
		String binLocation = "";

		Collection<AllocationEventModel> events = this.getInventoryEventService()
				.getAllocationEventsForConsignmentEntry(consignmentEntryModel);

		Set<String> collect = events.stream().filter(e -> e.getStockLevel() != null).map(e -> e.getStockLevel())
				.filter(e -> Strings.isNotBlank(e.getBin())).map(e -> e.getBin()).collect(Collectors.toSet());


		//bin from stock level
		if (!collect.isEmpty())
		{
			binLocation = String.join(", ", collect);
			return binLocation;
		}

		// else get bin from allocation event
		collect = events.stream().map(e -> e.getStockLevelBinNumber()).filter(Strings::isNotBlank).collect(Collectors.toSet());
		if (collect.isEmpty())
		{
			LOG.error("Could not find any bin number");
			return Strings.EMPTY;
		}

		binLocation = String.join(", ", collect);
		return binLocation;
	}

	public OrderData getOrderData()
	{

		return orderData;
	}

	public void setOrderData(OrderData orderData)
	{
		this.orderData = orderData;

	}

	protected ConsignmentOrderingService getConsignmentOrderingService()
	{
		return consignmentOrderingService;
	}

	protected void setConsignmentOrderingService(ConsignmentOrderingService consignmentOrderingService)
	{
		this.consignmentOrderingService = consignmentOrderingService;
	}

	/**
	 * @return the consignmentEntries
	 */
	public Set<ConsignmentEntryModel> getConsignmentEntries()
	{
		return consignmentEntries;
	}

	/**
	 * @param consignmentEntries
	 *                              the consignmentEntries to set
	 */
	public void setConsignmentEntries(Set<ConsignmentEntryModel> consignmentEntries)
	{
		this.consignmentEntries = consignmentEntries;
	}

	public String getExpressLogo()
	{
		return LOGO;
	}


}



