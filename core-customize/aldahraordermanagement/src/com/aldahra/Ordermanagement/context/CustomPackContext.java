package com.aldahra.Ordermanagement.context;

import java.io.IOException;
import java.lang.String;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang.LocaleUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.velocity.VelocityContext;

import com.aldahra.Ordermanagement.enums.ConsignmentOrderingType;
import com.aldahra.Ordermanagement.service.ConsignmentOrderingService;
import com.aldahra.aldahraproduct.model.GroceryVariantProductModel;
import com.aldahra.core.service.BarcodeGenaratorService;

import de.hybris.platform.acceleratorservices.model.cms2.pages.DocumentPageModel;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.servicelayer.model.ItemModelContext;
import de.hybris.platform.store.services.BaseStoreService;


/**
 * @author mnasro
 * @author mohammad-abumuhasien
 */
public class CustomPackContext extends de.hybris.platform.warehousing.labels.context.PackContext
{
	private static final Logger LOG = Logger.getLogger(CustomPackContext.class);

	@Resource(name = "orderFacade")
	private OrderFacade orderFacade;

	@Resource(name = "defaultConsignmentOrderingService")
	private ConsignmentOrderingService consignmentOrderingService;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	private Set<ConsignmentEntryModel> consignmentEntries;

	private OrderData orderData;

	@Resource(name = "barcodeGenaratorService")
	private BarcodeGenaratorService barcodeGenaratorService;

	private MediaModel barcode;


	private static final String LOGO = "/_ui/responsive/theme-foodcrowd/images/express.png";

	@Override
	public void init(final ConsignmentProcessModel businessProcessModel, final DocumentPageModel documentPageModel)
	{
		super.init(businessProcessModel, documentPageModel);
		orderData = orderFacade.getOrderDetailsForCode(getOrder().getCode());

		if (getConsignment().getBarcode() == null && !StringUtils.isBlank(getConsignment().getTrackingID()))
		{
			try
			{
				barcode = barcodeGenaratorService.generateBarcodeAsMedia(businessProcessModel.getConsignment());

			}
			catch (final IOException e)
			{
				LOG.error(e.getMessage());
			}
		}
		barcode = getConsignment().getBarcode();

		updateConsigmentEntriesOrdering(businessProcessModel.getConsignment());


	}

	private void updateConsigmentEntriesOrdering(final ConsignmentModel consignmentModel)
	{
		List<ConsignmentEntryModel> entries = getConsignmentOrderingService().orderConsignmentEntries(consignmentModel,
				ConsignmentOrderingType.PACK);

		this.setConsignmentEntries(new LinkedHashSet<>(entries));

	}

	public String getProductName(final ProductModel productModel)
	{
		return productModel.getName(Locale.ENGLISH);
	}

	public String getUnitOfMeasureDescription(final ProductModel productModel)
	{
		if (productModel instanceof GroceryVariantProductModel)
		{
			final GroceryVariantProductModel variant = (GroceryVariantProductModel) productModel;
			return variant.getUnitOfMeasureDescription(Locale.ENGLISH);
		}
		return null;

	}

	public String getPaymentMode(OrderModel order)
	{
		return order.getPaymentMode().getName(Locale.ENGLISH);

	}

	public String getDeliveryMode(OrderModel order)
	{
		return order.getDeliveryMode().getName(Locale.ENGLISH);
	}

	public String getAddressCountry(ConsignmentModel consignment)
	{
		return consignment.getShippingAddress().getCountry().getName(Locale.ENGLISH);
	}

	public String getAddressArea(ConsignmentModel consignment)
	{
		return consignment.getShippingAddress().getArea().getName(Locale.ENGLISH);
	}

	public String getcustomStreetName(ConsignmentModel consignment) {
		AddressModel address = consignment.getShippingAddress();
		
		if (address.getCustomStreetName()==null ||address.getCustomStreetName().isBlank() ) {
			return address.getLine1().toUpperCase(Locale.ENGLISH);
		}
		return address.getCustomStreetName().toUpperCase(Locale.ENGLISH);
		
	}

	public String getAddressCity(ConsignmentModel consignment)
	{
		return consignment.getShippingAddress().getCity().getName(Locale.ENGLISH);
	}

	public String getUOMCode(final ProductModel productModel)
	{
		if (productModel instanceof GroceryVariantProductModel)
		{
			final GroceryVariantProductModel variant = (GroceryVariantProductModel) productModel;
			return variant.getUnitOfMeasure(Locale.ENGLISH);
		}
		return null;

	}

	protected OrderFacade getOrderFacade()
	{
		return orderFacade;
	}

	protected void setOrderFacade(OrderFacade orderFacade)
	{
		this.orderFacade = orderFacade;
	}

	public OrderData getOrderData()
	{

		return orderData;
	}

	public void setOrderData(OrderData orderData)
	{
		this.orderData = orderData;

	}

	protected ConsignmentOrderingService getConsignmentOrderingService()
	{
		return consignmentOrderingService;
	}

	protected void setConsignmentOrderingService(ConsignmentOrderingService consignmentOrderingService)
	{
		this.consignmentOrderingService = consignmentOrderingService;
	}

	/**
	 * @return the consignmentEntries
	 */
	public Set<ConsignmentEntryModel> getConsignmentEntries()
	{
		return consignmentEntries;
	}

	/**
	 * @param consignmentEntries
	 *                              the consignmentEntries to set
	 */
	public void setConsignmentEntries(Set<ConsignmentEntryModel> consignmentEntries)
	{
		this.consignmentEntries = consignmentEntries;
	}

	public String getExpressLogo()
	{
		return LOGO;
	}

	/**
	 * @return the barcode
	 */
	public MediaModel getBarcode()
	{
		return barcode;
	}

	/**
	 * @param barcode
	 *                   the barcode to set
	 */
	public void setBarcode(final MediaModel barcode)
	{
		this.barcode = barcode;
	}


}


