/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 */
package com.aldahra.Ordermanagement.actions.consignment;

import javax.annotation.Resource;

import com.aldahra.aldahraotp.context.OTPContext;
import com.aldahra.aldahraotp.exception.OTPException;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;


/**
 * Update the {@link ConsignmentModel} status to {@link ConsignmentStatus#SHIPPED}.
 */
public class ConfirmShipConsignmentAction extends AbstractConsignmentAction
{
	@Resource(name = "otpContext")
	private OTPContext otpContext;
	@Override
	public void executeAction(final ConsignmentProcessModel process)
	{
		LOG.info("Process: {} in step {}", process.getCode(), getClass().getSimpleName());
		final ConsignmentModel consignment = process.getConsignment();
		consignment.setStatus(ConsignmentStatus.SHIPPED);
		consignment.setShipped(true);
		save(consignment);
		
		LOG.info("Starting to Send SMS Shipping Confirmation for consignment: " + consignment.getCode() + ", "
				+ consignment.getStatus() + " ...");
		
		try {
			otpContext.sendShippingConfirmationSMSMessage(consignment);
			otpContext.sendOrderShipmentWhatsappMessage(consignment);
			LOG.info("ConfirmShipConsignmentAction : Order Shipment WhatsApp message sent successfully");
		}catch(Exception e) {
			LOG.error("ConfirmShipConsignmentAction : " + e.getMessage(), e);
		}

		getEventService().publishEvent(getEvent(process));
	}

}
