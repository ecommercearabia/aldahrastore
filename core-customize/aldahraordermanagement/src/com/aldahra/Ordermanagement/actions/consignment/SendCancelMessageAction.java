package com.aldahra.Ordermanagement.actions.consignment;

import javax.annotation.Resource;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.orderprocessing.events.OrderPlacedEvent;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.servicelayer.event.EventService;
import org.apache.log4j.Logger;

import com.aldahra.Ordermanagement.events.SendOrderCancelEmailEvent;
import com.aldahra.aldahraotp.context.OTPContext;
import com.aldahra.aldahraotp.exception.OTPException;
import com.google.common.base.Preconditions;


/**
 * @author amjad.shati@erabia.com
 * @author abu-muhasien
 *	@author monzer
 *
 */
public class SendCancelMessageAction extends AbstractProceduralAction<ConsignmentProcessModel>
{
	private static final Logger LOG = Logger.getLogger(SendCancelMessageAction.class);
	
	@Resource(name = "otpContext")
	private OTPContext otpContext;
	@Resource(name = "eventService")
	private EventService eventService;

	/**
	 * @return the eventService
	 */
	public EventService getEventService()
	{
		return eventService;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.processengine.action.AbstractProceduralAction#executeAction(de.hybris.platform.processengine.model.
	 * BusinessProcessModel)
	 */
	@Override
	public void executeAction(final ConsignmentProcessModel process)
	{
		AbstractOrderModel order = process.getConsignment().getOrder();
		AddressModel deliveryAddress = order.getDeliveryAddress();
		Preconditions.checkArgument(deliveryAddress != null, "Delivery Address can not be null ... ");
		try
		{
			LOG.info(String.format("Process: %s in step %s", process.getCode(), getClass().getSimpleName()));
			 otpContext.sendOrderCancelationSMSMessage(order);
			LOG.info("SendCancelMessageAction : Cancelation SMS Message sent successfully");
			try {
				otpContext.sendOrderCancellationWhatsappMessage(process.getConsignment());
				LOG.info("SendCancelMessageAction : Cancelation Whatsapp Message sent successfully");
			}catch(OTPException e) {
				LOG.error("SendCancelMessageAction : " + e.getMessage(), e);
			}
		}
		catch (Exception e)
		{
			LOG.info(String.format("SendCancelMessageAction : Cancelation Order Sms could not be sent due to %s", e.getMessage()));

		}

		getEventService().publishEvent(new SendOrderCancelEmailEvent(process));
	}


}
