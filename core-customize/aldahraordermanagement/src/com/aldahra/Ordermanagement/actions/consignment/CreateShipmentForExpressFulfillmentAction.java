/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 */
package com.aldahra.Ordermanagement.actions.consignment;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aldahra.aldahrafulfillment.context.FulfillmentContext;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.commerceservices.model.PickUpDeliveryModeModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.processengine.action.AbstractAction;


/**
 * Redirects to the proper wait node depending on whether a consignment is for ship or pickup.
 */
public class CreateShipmentForExpressFulfillmentAction extends AbstractAction<ConsignmentProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(CreateShipmentForExpressFulfillmentAction.class);

	@Resource(name = "fulfillmentContext")
	private FulfillmentContext fulfillmentContext;
	
	
	/**
	 * @return the fulfillmentContext
	 */
	protected FulfillmentContext getFulfillmentContext()
	{
		return fulfillmentContext;
	}

	@Override
	public String execute(final ConsignmentProcessModel process)
	{
		LOG.info("Process: {} in step {}", process.getCode(), getClass().getSimpleName());
		final ConsignmentModel consignment = process.getConsignment();
		
		String transition = Transition.ERROR.toString();

		if ((consignment.getDeliveryMode() instanceof PickUpDeliveryModeModel) || consignment.getOrder() ==null || !consignment.getOrder().isExpress())
		{
			transition = Transition.OK.toString();
			LOG.warn("The order {} con not be express : transitions to {}",consignment.getOrder(), transition);
			LOG.debug("Process: {} transitions to {}", process.getCode(), transition);
			return transition;
		}

		
		consignment.setStatus(ConsignmentStatus.READY_FOR_SHIPPING);
		save(consignment);
		try
		{
			Optional<String> createShipment = getFulfillmentContext().createShipment(consignment);
			transition = Transition.OK.toString();

		}
		catch (Exception e) {
			LOG.error(" Error for create shipment for {} order : Error {}",consignment.getOrder(), e.getMessage());
			transition = Transition.ERROR.toString();
		}
		LOG.debug("Process: {} transitions to {}", process.getCode(), transition);

		return transition;
	}

	@Override
	public Set<String> getTransitions()
	{
		return Transition.getStringValues();
	}

	protected enum Transition
	{
		OK, ERROR;
		public static Set<String> getStringValues()
		{
			final Set<String> res = new HashSet<>();

			for (final Transition transition : Transition.values())
			{
				res.add(transition.toString());
			}
			return res;
		}
	}
}
