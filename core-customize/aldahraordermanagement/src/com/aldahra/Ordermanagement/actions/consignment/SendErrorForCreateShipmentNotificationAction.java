/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 */
package com.aldahra.Ordermanagement.actions.consignment;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aldahra.core.enums.IntegrationProvider;
import com.aldahra.core.event.GenerateErrorEmailEvent;
import com.google.common.base.Preconditions;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.store.BaseStoreModel;


/**
 * Redirects to the proper wait node depending on whether a consignment is for ship or pickup.
 */
public class SendErrorForCreateShipmentNotificationAction  extends AbstractProceduralAction<ConsignmentProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(UpdateConsignmentAction.class);
	
	private static final String BASESTORE_MODEL_MUSTN_T_BE_NULL = "baseStoreModel mustn't be null";

	
	
	@Resource(name = "eventService")
	private EventService eventService;
	/**
	 * @return the eventService
	 */
	protected EventService getEventService()
	{
		return eventService;
	}
	@Override
	public void executeAction(final ConsignmentProcessModel process)
	{
		LOG.info("Process: {} in step {}", process.getCode(), getClass().getSimpleName());
		final ConsignmentModel consignment = process.getConsignment();
		
		getEventService().publishEvent(new GenerateErrorEmailEvent(getIntegrationProviderType(consignment.getOrder().getStore()),
				consignment.getOrder(), consignment.getCreateShipmentResponseBody()));

	}
	private IntegrationProvider getIntegrationProviderType(final BaseStoreModel baseStoreModel)
	{
		Preconditions.checkArgument(baseStoreModel != null, BASESTORE_MODEL_MUSTN_T_BE_NULL);
		if (StringUtils.isEmpty(baseStoreModel.getFulfillmentProvider()))
		{
			return null;
		}
		switch (baseStoreModel.getExpressFulfillmentProvidorType())
		{
			case LYVE:
				return IntegrationProvider.LYVE;
			default:
				return null;
		}
	}
}