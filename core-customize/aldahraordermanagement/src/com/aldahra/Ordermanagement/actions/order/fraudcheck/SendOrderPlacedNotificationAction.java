/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 */
package com.aldahra.Ordermanagement.actions.order.fraudcheck;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.aldahra.aldahraotp.context.OTPContext;
import com.aldahra.core.enums.ShipmentType;
import com.google.common.base.Preconditions;

import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.orderprocessing.events.OrderPlacedEvent;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.servicelayer.event.EventService;


/**
 * Send a notification that an order is being placed.
 */
public class SendOrderPlacedNotificationAction extends AbstractProceduralAction<OrderProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(SendOrderPlacedNotificationAction.class);

	private EventService eventService;

	@Resource(name = "otpContext")
	private OTPContext otpContext;

	@Override
	public void executeAction(final OrderProcessModel process)
	{
		OrderModel order = process.getOrder();

		LOG.info("Process: {} in step {}", process.getCode(), getClass().getSimpleName());

		try
		{
			LOG.info("Process: {} in step {}", process.getCode(), getClass().getSimpleName());

			otpContext.sendOrderConfirmationSMSMessage(order);
			LOG.info("SMS Message sent successfully");
		}
		catch (Exception e)
		{
			LOG.error("Order Sms could not be sent due to ", e.getMessage());
		}
		try
		{
			LOG.info("Process: {} in step {}", process.getCode(), getClass().getSimpleName());
			otpContext.sendOrderConfirmationWhatsappMessage(order);
			LOG.info("Whatsapp Message sent successfully");
		}
		catch (Exception e)
		{
			LOG.error("Order Whatsapp could not be sent due to ", e.getMessage());
		}
			
			getEventService().publishEvent(new OrderPlacedEvent(process));
		
	}

	protected EventService getEventService()
	{
		return eventService;
	}

	@Required
	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}
}
