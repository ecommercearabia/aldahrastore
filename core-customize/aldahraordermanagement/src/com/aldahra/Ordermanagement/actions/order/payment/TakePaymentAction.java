/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 */
package com.aldahra.Ordermanagement.actions.order.payment;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.aldahra.aldahrapayment.ccavenue.enums.PaymentExceptionType;
import com.aldahra.aldahrapayment.ccavenue.exception.PaymentException;
import com.aldahra.aldahrapayment.context.PaymentContext;
import com.aldahra.aldahrapayment.entry.PaymentResponseData;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.model.ModelService;


/**
 * The TakePayment step captures the payment transaction.
 */
public class TakePaymentAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(TakePaymentAction.class);

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "paymentContext")
	private PaymentContext paymentContext;

	@Override
	public Transition executeAction(final OrderProcessModel process)
	{
		LOG.info("Process: {} in step {}", process.getCode(), getClass().getSimpleName());

		final OrderModel order = process.getOrder();
		boolean paymentFailed = false;
		if (order.getPaymentMode() == null || "cod".equalsIgnoreCase(order.getPaymentMode().getCode()))
		{
			LOG.info("PaymentMode is {} , Action status is {}",order.getPaymentMode() ,Transition.OK);
			setOrderStatus(order, OrderStatus.PAYMENT_NOT_CAPTURED);
			modelService.refresh(order);
			order.setPaymentStatus(de.hybris.platform.core.enums.PaymentStatus.NOTPAID);
			modelService.save(order);
			return Transition.OK;
		}
		LOG.info("The number of PaypmentTransactions is :  ",order.getPaymentTransactions().size());
		if (CollectionUtils.isEmpty(order.getPaymentTransactions()))
		{

			LOG.error("PaymentTransactions are empty,order stauts is : {}", OrderStatus.PAYMENT_NOT_CAPTURED);
			setOrderStatus(order, OrderStatus.PAYMENT_NOT_CAPTURED);
			modelService.refresh(order);
			order.setPaymentStatus(de.hybris.platform.core.enums.PaymentStatus.NOTPAID);
			modelService.save(order);
			return Transition.NOK;
		}
		PaymentTransactionEntryModel entry =null;
		for (final PaymentTransactionModel txn : order.getPaymentTransactions())
		{
			modelService.refresh(txn);

			entry = getPaymentTransactionEntry(txn,PaymentTransactionType.CAPTURE);
			if (entry != null)
			{
				LOG.info("captureEntry is exist ,captureEntry status is : {}", entry.getTransactionStatus());
				continue;
			}
			entry = getPaymentTransactionEntry(txn,PaymentTransactionType.AUTHORIZATION);

			if (entry == null)
			{

				LOG.error("AUTHORIZATION entry not found! ,paymentFailed is set to true.");
				paymentFailed = true;
				continue;
			}
			try
			{
				Optional<PaymentResponseData> paymentOrderConfirmedResponseData = paymentContext
						.getPaymentOrderConfirmedResponseData(order.getStore(), order);
			}
			catch (PaymentException ex)
			{
				if (!PaymentExceptionType.THE_ORDER_IS_ALLREADY_CAPTUARED.equals(ex.getType()))
				{
					LOG.error("Error in : {} , paymentFailed is set to true.", ex.getMessage());
					paymentFailed = true;
				}
				else
				{
					LOG.error("Action failed , order culd not be captuared, {} ", ex.getMessage());

				}
			}

		}
		if (paymentFailed)
		{
			LOG.error("paymentFailed is :{} ,order status is : {}", true, OrderStatus.PAYMENT_NOT_CAPTURED);

			setOrderStatus(order, OrderStatus.PAYMENT_NOT_CAPTURED);
			modelService.refresh(order);
			order.setPaymentStatus(de.hybris.platform.core.enums.PaymentStatus.NOTPAID);
			modelService.save(order);
			return Transition.NOK;
		}
		else
		{
			LOG.error("paymentFailed is :{} ,order status is : {}", false, OrderStatus.PAYMENT_CAPTURED);
			setOrderStatus(order, OrderStatus.PAYMENT_CAPTURED);
			modelService.refresh(order);
			order.setPaymentStatus(de.hybris.platform.core.enums.PaymentStatus.PAID);
			modelService.save(order);
			return Transition.OK;
		}

	}

	private PaymentTransactionEntryModel getPaymentTransactionEntry(final PaymentTransactionModel ptModel , PaymentTransactionType paymentTransactionType )
	{
		if (CollectionUtils.isEmpty(ptModel.getEntries())||paymentTransactionType==null)
		{
			return null;
		}
		for (PaymentTransactionEntryModel pteModel : ptModel.getEntries())
		{
			if(paymentTransactionType.equals(pteModel.getType())	&& de.hybris.platform.payment.dto.TransactionStatus.ACCEPTED.name().equalsIgnoreCase(pteModel.getTransactionStatus()))
			{
				return pteModel;
			}
		} 

		return null;
	}

}
