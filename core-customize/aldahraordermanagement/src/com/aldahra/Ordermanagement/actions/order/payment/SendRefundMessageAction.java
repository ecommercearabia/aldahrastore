package com.aldahra.Ordermanagement.actions.order.payment;

import javax.annotation.Resource;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.orderprocessing.events.OrderPlacedEvent;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.servicelayer.event.EventService;
import org.apache.log4j.Logger;

import com.aldahra.Ordermanagement.events.SendOrderCancelEmailEvent;
import com.aldahra.Ordermanagement.events.SendOrderRefundEmailEvent;
import com.aldahra.aldahraotp.context.OTPContext;
import com.google.common.base.Preconditions;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class SendRefundMessageAction extends AbstractProceduralAction<OrderProcessModel>
{
	private static final Logger LOG = Logger.getLogger(SendRefundMessageAction.class);
	
	@Resource(name = "eventService")
	private EventService eventService;

	/**
	 * @return the eventService
	 */
	public EventService getEventService()
	{
		return eventService;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.processengine.action.AbstractProceduralAction#executeAction(de.hybris.platform.processengine.model.
	 * BusinessProcessModel)
	 */
	@Override
	public void executeAction(final OrderProcessModel process)
	{
		LOG.info("Publishing SendOrderRefundEmailEvent");
		getEventService().publishEvent(new SendOrderRefundEmailEvent(process));
	}


}
