/**
 * 
 */
package com.aldahra.Ordermanagement.events;

import de.hybris.platform.orderprocessing.events.ConsignmentProcessingEvent;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;

/**
 * @author amjad.shati@erabia.com
 *
 */
public class SendOrderCancelEmailEvent extends ConsignmentProcessingEvent
{
	private static final long serialVersionUID = 1L;
	
	public SendOrderCancelEmailEvent(ConsignmentProcessModel process)
	{
		super(process);
	}
}
