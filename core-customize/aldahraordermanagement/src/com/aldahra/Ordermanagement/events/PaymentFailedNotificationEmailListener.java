package com.aldahra.Ordermanagement.events;

import org.springframework.beans.factory.annotation.Required;

import com.aldahra.Ordermanagement.model.PaymentFailedNotificationProcessModel;

import de.hybris.platform.acceleratorservices.site.AbstractAcceleratorSiteEventListener;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.commerceservices.event.AbstractSiteEventListener;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;



/**
 * The listener interface for receiving paymentFailedNotificationEmail events.
 * The class that is interested in processing a paymentFailedNotificationEmail
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addPaymentFailedNotificationEmailListener<code> method. When
 * the paymentFailedNotificationEmail event occurs, that object's appropriate
 * method is invoked.
 *
 * @see PaymentFailedNotificationEmailEvent
 * @author abu-muhasien
 */
public class PaymentFailedNotificationEmailListener extends AbstractAcceleratorSiteEventListener<PaymentFailedNotificationEmailEvent>
{

	/** The model service. */
	private ModelService modelService;
	
	/** The business process service. */
	private BusinessProcessService businessProcessService;
	
	/** The process code generator. */
	private KeyGenerator processCodeGenerator;


	/**
	 * Gets the business process service.
	 *
	 * @return the business process service
	 */
	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	/**
	 * Sets the business process service.
	 *
	 * @param businessProcessService the new business process service
	 */
	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	/**
	 * Gets the model service.
	 *
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * Sets the model service.
	 *
	 * @param modelService                        the modelService to set
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * On site event.
	 *
	 * @param event the event
	 */
	@Override
	protected void onSiteEvent(final PaymentFailedNotificationEmailEvent event)
	{
		ConsignmentProcessModel process = event.getProcess();
		final PaymentFailedNotificationProcessModel emailFailedNotificationProcessModel = (PaymentFailedNotificationProcessModel) getBusinessProcessService()
				.createProcess(
						"paymentFailedNotificationEmail-process-" + process.getCode() + "-" + processCodeGenerator.generate().toString(),
						"paymentFailedNotificationEmail-process");
		emailFailedNotificationProcessModel.setConsignment(process.getConsignment());
		getModelService().save(emailFailedNotificationProcessModel);
		getBusinessProcessService().startProcess(emailFailedNotificationProcessModel);
	}

	/**
	 * Should handle event.
	 *
	 * @param event the event
	 * @return true, if successful
	 */
	@Override
	protected boolean shouldHandleEvent(final PaymentFailedNotificationEmailEvent event)
	{
		return true;
	
	}

	/**
	 * Gets the process code generator.
	 *
	 * @return the process code generator
	 */
	public KeyGenerator getProcessCodeGenerator()
	{
		return processCodeGenerator;
	}

	/**
	 * Sets the process code generator.
	 *
	 * @param processCodeGenerator the new process code generator
	 */
	@Required
	public void setProcessCodeGenerator(KeyGenerator processCodeGenerator)
	{
		this.processCodeGenerator = processCodeGenerator;
	}

	/**
	 * Gets the site channel for event.
	 *
	 * @param event the event
	 * @return the site channel for event
	 */
	@Override
	protected SiteChannel getSiteChannelForEvent(PaymentFailedNotificationEmailEvent event)
	{
		final AbstractOrderModel order = event.getProcess().getConsignment().getOrder();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order", order);
		final BaseSiteModel site = order.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order.site", site);
		return site.getChannel();
	}

}
