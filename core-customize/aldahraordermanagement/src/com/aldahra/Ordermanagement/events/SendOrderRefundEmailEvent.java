/**
 * 
 */
package com.aldahra.Ordermanagement.events;

import de.hybris.platform.orderprocessing.events.ConsignmentProcessingEvent;
import de.hybris.platform.orderprocessing.events.OrderProcessingEvent;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;

/**
 * @author amjad.shati@erabia.com
 *
 */
public class SendOrderRefundEmailEvent extends OrderProcessingEvent
{
	private static final long serialVersionUID = 1L;
	
	public SendOrderRefundEmailEvent(OrderProcessModel process)
	{
		super(process);
	}
}
