package com.aldahra.Ordermanagement.events;

import de.hybris.platform.orderprocessing.events.ConsignmentProcessingEvent;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;



/**
 *@author abu-muhasien
 */
public class PaymentFailedNotificationEmailEvent extends ConsignmentProcessingEvent
{

	/**
	 * Instantiates a new payment failed notification email event.
	 *
	 * @param process the process
	 */
	public PaymentFailedNotificationEmailEvent(ConsignmentProcessModel process)
	{
		super(process);
	}
	

	
}