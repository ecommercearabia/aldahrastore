/**
 *
 */
package com.aldahra.aldahracommercewebservices.core.service.impl;

import de.hybris.platform.core.Registry;
import de.hybris.platform.core.Tenant;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.persistence.security.EJBPasswordEncoderNotFoundException;
import de.hybris.platform.persistence.security.PBKDF2WithHmacSHA1SaltedPasswordEncoder;
import de.hybris.platform.persistence.security.PasswordEncoder;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import com.aldahra.aldahracommercewebservices.core.service.UserSignatureAuthenticationService;
import com.google.common.base.Preconditions;


/**
 * @author monzer
 *
 */
public class DefaultUserSignatureAuthenticationService implements UserSignatureAuthenticationService
{

	@Resource(name = "userService")
	private UserService userService;

	@Override
	public boolean canCustomerLoginWithSignature(final String customerUid, final String customerSignature)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(customerUid), "Custoemr Uid cannot be empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(customerSignature), "Custoemr Signature cannot be empty");

		UserModel user = null;
		
		try
		{
			user = userService.getUserForUID(customerUid);
		}
		catch (final UnknownIdentifierException e)
		{
			return false;
		}

		if (user == null || !(user instanceof CustomerModel))
		{
			return false;
		}
		final CustomerModel customer = (CustomerModel)user;
		try
		{
			return checkSignature(customer, customerSignature);
		}
		catch (final EJBPasswordEncoderNotFoundException e)
		{
			return false;
		}
	}

	private boolean checkSignature(final CustomerModel customer, final String signature) throws EJBPasswordEncoderNotFoundException
	{
		final PasswordEncoder enc = Registry.<Tenant> getCurrentTenant().getJaloConnection()
				.getPasswordEncoder(customer.getPasswordEncoding());

		if (enc instanceof PBKDF2WithHmacSHA1SaltedPasswordEncoder)
		{
			return enc.check(customer.getUid(), customer.getSignatureId(), signature);
		}
		else
		{
			final String hashedPassword = enc.encode(signature, customer.getPasswordEncoding());
			return customer.getSignatureId().equals(hashedPassword);
		}
	}

}
