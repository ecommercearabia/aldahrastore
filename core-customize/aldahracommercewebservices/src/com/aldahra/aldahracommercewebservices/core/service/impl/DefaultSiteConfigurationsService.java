/**
 *
 */
package com.aldahra.aldahracommercewebservices.core.service.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Locale;
import java.util.Optional;

import javax.annotation.Resource;

import org.springframework.context.MessageSource;

import com.aldahra.aldahracommercewebservices.core.configuration.FieldConfigurationData;
import com.aldahra.aldahracommercewebservices.core.configuration.ReferralCodeConfigurationData;
import com.aldahra.aldahracommercewebservices.core.configuration.StoreCreditConfigurationData;
import com.aldahra.aldahracommercewebservices.core.configuration.site.AddressConfigurationData;
import com.aldahra.aldahracommercewebservices.core.configuration.site.RegistrationConfigurationData;
import com.aldahra.aldahracommercewebservices.core.configuration.site.SiteConfigurationData;
import com.aldahra.aldahracommercewebservices.core.service.SiteConfigurationsService;


/**
 * @author tuqa-pc
 *
 */
public class DefaultSiteConfigurationsService implements SiteConfigurationsService
{
	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "i18NService")
	private I18NService i18NService;

	@Resource(name = "messageSource")
	private MessageSource messageSource;

	private static final String STORECREDIT_SCAMOUNT_CHECK_LIMIT_MSG_KEY = "storeCredit.scAmount.check.limit.msg";

	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}
	@Override
	public Optional<SiteConfigurationData> getSiteConfigurationForCurrentSite()
	{

		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		if (currentSite == null)
		{
			return Optional.empty();
		}

		final SiteConfigurationData siteConfigurationData = new SiteConfigurationData();
		siteConfigurationData.setRegistrationConfiguration(getRegistrationConfiguration(currentSite));
		siteConfigurationData.setAddressConfiguration(getAddressConfig(currentSite));
		siteConfigurationData.setStoreCreditConfiguration(getStoreCreditConfig());
		siteConfigurationData.setPickupInStoreEnabled(currentSite.isPickUpEnabled());
		siteConfigurationData.setDisableCouponForPickUpInStore(currentSite.isDisableCouponForPickUpInStore());
		siteConfigurationData.setEnableLoyaltyPoint(baseStoreService.getCurrentBaseStore() == null ? false
				: baseStoreService.getCurrentBaseStore().isEnableLoyaltyPoint());


		final Locale currentLocale = getI18NService().getCurrentLocale();
		final BaseStoreModel store = getBaseStoreService().getCurrentBaseStore();
		siteConfigurationData.setDeliveryOptionMSG(store.getDeliveryModeMessage(currentLocale));
		return Optional.ofNullable(siteConfigurationData);
	}



	private RegistrationConfigurationData getRegistrationConfiguration(final CMSSiteModel currentSite)
	{
		final RegistrationConfigurationData registrationConfigurationData = new RegistrationConfigurationData();
		final FieldConfigurationData nationalityConfig = getNationalityConfig(currentSite);
		final ReferralCodeConfigurationData referralCodeConfig = getReferralCodeConfig(currentSite);
		final FieldConfigurationData nationalIdConfig = getNationalIdConfig(currentSite);

		registrationConfigurationData.setNationalityConfigurations(nationalityConfig);
		registrationConfigurationData.setNationalityIdConfigurations(nationalIdConfig);
		registrationConfigurationData.setReferralCodeConfigurations(referralCodeConfig);
		return registrationConfigurationData;
	}

	/**
	 * @param currentSite
	 * @return
	 */
	private StoreCreditConfigurationData getStoreCreditConfig()
	{
		final BaseStoreModel store = baseStoreService.getCurrentBaseStore();
		final StoreCreditConfigurationData storeCreditConfigurationData = new StoreCreditConfigurationData();

		final double totalPriceWithStoreCreditLimit = store.getTotalPriceWithStoreCreditLimit();
		storeCreditConfigurationData
				.setEnableCheckTotalPriceWithStoreCreditLimit(store.isEnableCheckTotalPriceWithStoreCreditLimit());
		storeCreditConfigurationData.setTotalPriceWithStoreCreditLimit(totalPriceWithStoreCreditLimit);


		return storeCreditConfigurationData;
	}



	private FieldConfigurationData getNationalIdConfig(final CMSSiteModel currentSite)
	{
		final FieldConfigurationData nationalityIdConfig = new FieldConfigurationData();
		nationalityIdConfig.setEnabled(currentSite.isNationalityIdCustomerEnabled());
		nationalityIdConfig.setHidden(currentSite.isNationalityIdCustomerHidden());
		nationalityIdConfig.setRequired(currentSite.isNationalityIdCustomerRequired());
		return nationalityIdConfig;
	}

	private FieldConfigurationData getNationalityConfig(final CMSSiteModel currentSite)
	{
		final FieldConfigurationData nationalityConfig = new FieldConfigurationData();
		nationalityConfig.setEnabled(currentSite.isNationalityCustomerEnabled());
		nationalityConfig.setHidden(currentSite.isNationalityCustomerHidden());
		nationalityConfig.setRequired(currentSite.isNationalityCustomerRequired());
		return nationalityConfig;
	}


	private ReferralCodeConfigurationData getReferralCodeConfig(final CMSSiteModel currentSite)
	{
		final BaseStoreModel store = currentSite.getStores().get(0);
		final ReferralCodeConfigurationData referralCodeConfigurationData = new ReferralCodeConfigurationData();
		referralCodeConfigurationData.setSenderRewardAmount(store.getReferralCodeSenderRewardAmount());
		referralCodeConfigurationData.setReceiverRewardAmount(store.getReferralCodeNewAppliedRewardAmount());
		referralCodeConfigurationData.setEnabled(store.isReferralCodeEnable());
		return referralCodeConfigurationData;
	}

	@Override
	public Optional<RegistrationConfigurationData> getRegistrationConfigurationForCurrentSite()
	{
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		if (currentSite == null)
		{
			return Optional.empty();
		}
		return Optional.ofNullable(getRegistrationConfiguration(currentSite));
	}

	@Override
	public Optional<AddressConfigurationData> getAddressConfigurationForCurrentSite()
	{
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		if (currentSite == null)
		{
			return Optional.empty();
		}
		return Optional.ofNullable(getAddressConfig(currentSite));
	}

	private AddressConfigurationData getAddressConfig(final CMSSiteModel currentSite)
	{
		final AddressConfigurationData addressConfig = new AddressConfigurationData();
		addressConfig.setEnableMap(currentSite.isEnableMapOnSite());
		addressConfig.setStreetNameConfigurations(getStreetNameConfig(currentSite));
		addressConfig.setBuildingNameConfigurations(getBuildingNameConfig(currentSite));
		addressConfig.setApartmentNumberConfigurations(getApartmentNumberConfig(currentSite));
		return addressConfig;
	}

	private FieldConfigurationData getStreetNameConfig(final CMSSiteModel currentSite)
	{
		final FieldConfigurationData streetConfig = new FieldConfigurationData();
		streetConfig.setEnabled(currentSite.isCustomStreetNameAddressEnabled());
		streetConfig.setHidden(currentSite.isCustomStreetNameAddressHidden());
		streetConfig.setRequired(currentSite.isCustomStreetNameAddressRequired());
		return streetConfig;
	}

	private FieldConfigurationData getBuildingNameConfig(final CMSSiteModel currentSite)
	{
		final FieldConfigurationData config = new FieldConfigurationData();
		config.setEnabled(currentSite.isBuildingNameAddressEnabled());
		config.setHidden(currentSite.isBuildingNameAddressHidden());
		config.setRequired(currentSite.isBuildingNameAddressRequired());
		return config;
	}

	private FieldConfigurationData getApartmentNumberConfig(final CMSSiteModel currentSite)
	{
		final FieldConfigurationData config = new FieldConfigurationData();
		config.setEnabled(currentSite.isApartmentNumberAddressEnabled());
		config.setHidden(currentSite.isApartmentNumberAddressHidden());
		config.setRequired(currentSite.isApartmentNumberAddressRequired());
		return config;
	}

	protected MessageSource getMessageSource()
	{
		return messageSource;
	}

	protected I18NService getI18NService()
	{
		return i18NService;
	}



	@Override
	public Optional<StoreCreditConfigurationData> getStoreCreditConfigurationForCurrentSite()
	{
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		if (currentSite == null)
		{
			return Optional.empty();
		}
		return Optional.ofNullable(getStoreCreditConfig());
	}
}
