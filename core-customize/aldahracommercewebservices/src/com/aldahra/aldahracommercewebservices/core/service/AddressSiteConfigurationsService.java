/**
 *
 */
package com.aldahra.aldahracommercewebservices.core.service;

import com.aldahra.aldahracommercewebservices.core.configuration.site.SiteConfigurationData;


/**
 * @author monzer
 *
 */
public interface AddressSiteConfigurationsService
{
	public SiteConfigurationData getCurrentSiteConfigurationForAddress(String baseSite);

}
