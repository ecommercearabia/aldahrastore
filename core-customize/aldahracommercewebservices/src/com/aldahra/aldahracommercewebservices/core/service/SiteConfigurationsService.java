/**
 *
 */
package com.aldahra.aldahracommercewebservices.core.service;

import java.util.Optional;

import com.aldahra.aldahracommercewebservices.core.configuration.StoreCreditConfigurationData;
import com.aldahra.aldahracommercewebservices.core.configuration.site.AddressConfigurationData;
import com.aldahra.aldahracommercewebservices.core.configuration.site.RegistrationConfigurationData;
import com.aldahra.aldahracommercewebservices.core.configuration.site.SiteConfigurationData;


/**
 * @author mbaker
 *
 */
public interface SiteConfigurationsService
{
	public Optional<RegistrationConfigurationData> getRegistrationConfigurationForCurrentSite();

	public Optional<AddressConfigurationData> getAddressConfigurationForCurrentSite();

	public Optional<SiteConfigurationData> getSiteConfigurationForCurrentSite();

	public Optional<StoreCreditConfigurationData> getStoreCreditConfigurationForCurrentSite();

}
