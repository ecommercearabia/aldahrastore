/**
 *
 */
package com.aldahra.aldahracommercewebservices.core.service;

/**
 * @author monzer
 *
 */
public interface UserSignatureAuthenticationService
{

	boolean canCustomerLoginWithSignature(String customerUid, String customerSignature);

}
