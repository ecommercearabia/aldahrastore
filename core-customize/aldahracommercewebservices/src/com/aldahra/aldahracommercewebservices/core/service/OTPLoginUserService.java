/**
 *
 */
package com.aldahra.aldahracommercewebservices.core.service;

import de.hybris.platform.commercefacades.user.data.CustomerData;

import com.aldahra.aldahraotp.exception.OTPException;


/**
 * @author monzer
 *
 */
public interface OTPLoginUserService
{

	CustomerData getUserByEmail(String email) throws OTPException;

	boolean sendOtpCodeToCustomer(String identifier) throws OTPException;

	boolean verifyOtpCode(String identifier, String otpCode) throws OTPException;

	/**
	 * @param email
	 * @return
	 * @throws OTPException
	 */
	CustomerData getUserByIdentifier(String identifier) throws OTPException;

}
