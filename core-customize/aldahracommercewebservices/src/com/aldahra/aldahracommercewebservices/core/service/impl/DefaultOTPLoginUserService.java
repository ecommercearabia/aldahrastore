/**
 *
 */
package com.aldahra.aldahracommercewebservices.core.service.impl;

import de.hybris.platform.commercefacades.user.data.CustomerData;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.aldahra.aldahracommercewebservices.core.service.OTPLoginUserService;
import com.aldahra.aldahraotp.context.OTPContext;
import com.aldahra.aldahraotp.exception.OTPException;
import com.aldahra.aldahraotp.exception.enums.OTPExceptionType;
import com.aldahra.aldahrauserfacades.customer.facade.CustomCustomerFacade;
import com.aldahra.core.service.CustomerMatchingService;


/**
 * @author monzer
 *
 */
public class DefaultOTPLoginUserService implements OTPLoginUserService
{

	@Resource(name = "otpContext")
	private OTPContext otpContext;

	@Resource(name = "customCustomerFacade")
	private CustomCustomerFacade customerFacade;

	@Resource(name = "customerMatchingService")
	private CustomerMatchingService customerMatchingService;

	@Override
	public CustomerData getUserByEmail(final String email) throws OTPException
	{
		final CustomerData user = customerFacade.getUserForUID(email);
		if (user == null || user.getMobileCountry() == null || StringUtils.isBlank(user.getMobileCountry().getIsocode())
				|| StringUtils.isBlank(user.getMobileNumber()))
		{
			throw new OTPException(OTPExceptionType.MESSAGE_CAN_NOT_BE_SENT,
					"user " + email + " does not have a valid mobile number");
		}
		return user;
	}

	@Override
	public CustomerData getUserByIdentifier(final String identifier) throws OTPException
	{
		final Optional<CustomerData> userOptional = customerFacade.getCustomerByIdentifier(identifier);
		if (userOptional.isEmpty())
		{
			throw new OTPException(OTPExceptionType.MESSAGE_CAN_NOT_BE_SENT, "user for " + identifier + " does not exist!");
		}
		final CustomerData user = userOptional.get();
		if (user == null || user.getMobileCountry() == null || StringUtils.isBlank(user.getMobileCountry().getIsocode())
				|| StringUtils.isBlank(user.getMobileNumber()))
		{
			throw new OTPException(OTPExceptionType.MESSAGE_CAN_NOT_BE_SENT, "user for " + identifier + " does not exist!");
		}
		return user;
	}

	@Override
	public boolean sendOtpCodeToCustomer(final String identifier) throws OTPException
	{
		final CustomerData customer = getUserByIdentifier(identifier);
		otpContext.sendOTPCodeByCurrentSite(customer.getMobileCountry().getIsocode(), customer.getMobileNumber());
		return true;
	}

	@Override
	public boolean verifyOtpCode(final String identifier, final String otpCode) throws OTPException
	{
		final CustomerData customer = getUserByIdentifier(identifier);
		return otpContext.verifyCodeByCurrentSite(customer.getMobileCountry().getIsocode(), customer.getMobileNumber(), otpCode);
	}

}
