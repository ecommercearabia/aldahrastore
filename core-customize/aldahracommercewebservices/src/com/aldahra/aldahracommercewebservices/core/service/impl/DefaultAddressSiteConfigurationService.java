/**
 *
 */
package com.aldahra.aldahracommercewebservices.core.service.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;

import javax.annotation.Resource;

import com.aldahra.aldahracommercewebservices.core.configuration.FieldConfigurationData;
import com.aldahra.aldahracommercewebservices.core.configuration.site.AddressConfigurationData;
import com.aldahra.aldahracommercewebservices.core.configuration.site.SiteConfigurationData;
import com.aldahra.aldahracommercewebservices.core.service.AddressSiteConfigurationsService;


/**
 * @author monzer
 *
 */
public class DefaultAddressSiteConfigurationService implements AddressSiteConfigurationsService
{

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Override
	public SiteConfigurationData getCurrentSiteConfigurationForAddress(final String baseSiteId)
	{
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		if (currentSite == null)
		{
			return null;
		}

		return fillToAddressConfigurationData(currentSite);
	}

	/**
	 * @param currentSite
	 * @return
	 */
	private SiteConfigurationData fillToAddressConfigurationData(final CMSSiteModel currentSite)
	{
		final SiteConfigurationData configurations = new SiteConfigurationData();
		final AddressConfigurationData addressConfigurations = buildAddressConfigurationsData(currentSite);
		configurations.setAddressConfiguration(addressConfigurations);
		return configurations;
	}

	/**
	 * @param currentSite
	 * @return
	 */
	private AddressConfigurationData buildAddressConfigurationsData(final CMSSiteModel currentSite)
	{
		final AddressConfigurationData addressConfigurations = new AddressConfigurationData();
		final FieldConfigurationData steerNameFieldConfiguration = buildStreetNameFieldConfigurationData(currentSite);
		final FieldConfigurationData buildingNameFieldConfiguration = buildBuildingNameFieldConfigurationData(currentSite);
		final FieldConfigurationData apartmentNumberFieldConfiguration = buildApartmentNumberFieldConfigurationData(
				currentSite);
		addressConfigurations.setStreetNameConfigurations(steerNameFieldConfiguration);
		addressConfigurations.setBuildingNameConfigurations(buildingNameFieldConfiguration);
		addressConfigurations.setApartmentNumberConfigurations(apartmentNumberFieldConfiguration);
		addressConfigurations.setEnableMap(currentSite.isEnableMapOnSite());
		return addressConfigurations;
	}

	/**
	 * @param currentSite
	 * @return
	 */
	private FieldConfigurationData buildApartmentNumberFieldConfigurationData(final CMSSiteModel currentSite)
	{
		final FieldConfigurationData apartmentNumber = new FieldConfigurationData();
		apartmentNumber.setEnabled(currentSite.isApartmentNumberAddressEnabled());
		apartmentNumber.setRequired(currentSite.isApartmentNumberAddressRequired());
		apartmentNumber.setHidden(currentSite.isApartmentNumberAddressHidden());
		return apartmentNumber;
	}

	/**
	 * @param currentSite
	 * @return
	 */
	private FieldConfigurationData buildBuildingNameFieldConfigurationData(final CMSSiteModel currentSite)
	{
		final FieldConfigurationData buildingName = new FieldConfigurationData();
		buildingName.setEnabled(currentSite.isBuildingNameAddressEnabled());
		buildingName.setRequired(currentSite.isBuildingNameAddressRequired());
		buildingName.setHidden(currentSite.isBuildingNameAddressHidden());
		return buildingName;
	}

	/**
	 * @param currentSite
	 * @return
	 */
	private FieldConfigurationData buildStreetNameFieldConfigurationData(final CMSSiteModel currentSite)
	{
		final FieldConfigurationData streetName = new FieldConfigurationData();
		streetName.setEnabled(currentSite.isCustomStreetNameAddressEnabled());
		streetName.setRequired(currentSite.isCustomStreetNameAddressRequired());
		streetName.setHidden(currentSite.isCustomStreetNameAddressHidden());
		return streetName;
	}

}
