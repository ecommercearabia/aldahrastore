/**
 *
 */
package com.aldahra.aldahracommercewebservices.core.oauth2;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.AccountStatusException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.OAuth2RequestFactory;
import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.security.oauth2.provider.token.AbstractTokenGranter;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;

import com.aldahra.aldahracommercewebservices.core.oauth2.token.SignatureIdAuthenticationOCCToken;


/**
 * @author monzer
 *
 */
public class SignatureIdTokenGranter extends AbstractTokenGranter
{

	private static final String GRANT_TYPE = "signature";

	private final AuthenticationManager authenticationManager;

	public SignatureIdTokenGranter(final AuthenticationManager authenticationManager,
			final AuthorizationServerTokenServices tokenServices, final ClientDetailsService clientDetailsService,
			final OAuth2RequestFactory requestFactory)
	{
		this(authenticationManager, tokenServices, clientDetailsService, requestFactory, GRANT_TYPE);
	}

	/**
	 * @param tokenServices
	 * @param clientDetailsService
	 * @param requestFactory
	 * @param grantType
	 */
	public SignatureIdTokenGranter(final AuthenticationManager authenticationManager, final AuthorizationServerTokenServices tokenServices,
			final ClientDetailsService clientDetailsService, final OAuth2RequestFactory requestFactory, final String grantType)
	{
		super(tokenServices, clientDetailsService, requestFactory, grantType);
		this.authenticationManager = authenticationManager;
	}

	@Override
	protected OAuth2Authentication getOAuth2Authentication(final ClientDetails client, final TokenRequest tokenRequest)
	{
		final Map<String, String> parameters = new LinkedHashMap<String, String>(tokenRequest.getRequestParameters());
		final String username = parameters.get("email");
		final String signature = parameters.get("signatureId");

		Authentication authToken = new SignatureIdAuthenticationOCCToken(username, signature);
		((AbstractAuthenticationToken) authToken).setDetails(parameters);
		try
		{
			authToken = authenticationManager.authenticate(authToken);
		}
		catch (final AccountStatusException ase)
		{
			//covers expired, locked, disabled cases (mentioned in section 5.2, draft 31)
			throw new InvalidGrantException(ase.getMessage());
		}
		catch (final BadCredentialsException e)
		{
			// If the username/password are wrong the spec says we should send 400/invalid grant
			throw new InvalidGrantException(e.getMessage());
		}
		if (authToken == null || !authToken.isAuthenticated())
		{
			throw new InvalidGrantException("Could not authenticate user: " + username);
		}

		
		final OAuth2Request storedOAuth2Request = getRequestFactory().createOAuth2Request(client, tokenRequest);
		return new OAuth2Authentication(storedOAuth2Request, authToken);
	}



}
