package com.aldahra.aldahracommercewebservices.core.validator;

import de.hybris.platform.commercefacades.user.data.AreaData;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.aldahra.aldahrauserfacades.area.facade.AreaFacade;


public class AreaValidator implements Validator
{
	private static final String INVALID_AREACODE_ID = " field.invalidAreaCode";

	@Resource(name = "areaFacade")
	private AreaFacade areaFacade;

	private String cityCode;
	private String areaCode;

	/**
	 * @return the areaCode
	 */
	public String getAreaCode()
	{
		return areaCode;
	}

	/**
	 * @param areaCode
	 *           the areaCode to set
	 */
	@Required
	public void setAreaCode(final String areaCode)
	{
		this.areaCode = areaCode;
	}

	/**
	 * @return the cityCode
	 */
	public String getCityCode()
	{
		return cityCode;
	}

	/**
	 * @param cityCode
	 *           the cityCode to set
	 */
	@Required
	public void setCityCode(final String cityCode)
	{
		this.cityCode = cityCode;
	}

	public boolean supports(final Class<?> aClass)
	{
		return true;
	}

	public void validate(final Object o, final Errors errors)
	{
		Assert.notNull(errors, "Errors object must not be null");
		final String areaCode = (String) errors.getFieldValue(this.areaCode);
		final String cityCode = (String) errors.getFieldValue(this.cityCode);

		if (!findArea(cityCode, areaCode))
		{
			errors.rejectValue(this.areaCode, INVALID_AREACODE_ID, new String[]
			{ this.areaCode }, "This field is not a valid area code.");
		}

	}

	private boolean findArea(final String cityCode, final String areaCode)
	{
		if (StringUtils.isBlank(cityCode) || StringUtils.isBlank(areaCode))
		{
			return false;
		}
		final Optional<List<AreaData>> areas = areaFacade.getByCityCode(cityCode);
		if (!areas.isPresent() || areas.get().isEmpty())
		{
			return false;
		}
		for (final AreaData areaData : areas.get())
		{
			if (areaCode.equals(areaData.getCode()))
			{
				return true;
			}
		}
		return false;

	}


}

