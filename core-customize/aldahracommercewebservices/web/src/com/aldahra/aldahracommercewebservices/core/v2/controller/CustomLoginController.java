/**
 *
 */
package com.aldahra.aldahracommercewebservices.core.v2.controller;

import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.ConfigurationException;
import de.hybris.platform.webservicescommons.cache.CacheControl;
import de.hybris.platform.webservicescommons.cache.CacheControlDirective;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdParam;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aldahra.aldahrawebserviceapi.util.WebServiceApiUtil;
import com.aldahra.core.service.CustomerMatchingService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * @author monzer
 *
 */
@Controller
@RequestMapping(value = "/{baseSiteId}/login/token")
@CacheControl(directive = CacheControlDirective.PUBLIC, maxAge = 360)
@Api(tags = "Custom Identifier Auth")
public class CustomLoginController extends BaseController
{

	private static final String AUTHORIZATION_PATH = "/authorizationserver/oauth/token";
	private static final String BASIC_AUTH = "Basic ";
	private static final String AUTHORIZATION = "Authorization";
	private static final String API_URL_PROPERTY = "api.foodcrowd-ae.https";

	@Resource(name = "customerMatchingService")
	private CustomerMatchingService customerMatchingService;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody
	@ApiOperation(nickname = "Identifier Login", value = "Identifier Login", notes = "Login by Customer Identifiers (email or mobile number)")
	@ApiBaseSiteIdParam
	public ResponseEntity<Map<String, Object>> loginByIdentifier(@RequestHeader("clientId")
	final String clientId, @RequestHeader("clientSecret")
	final String clientSecret, @RequestParam(required = true)
	final String identifier, @RequestParam(required = true)
	final String password)
	{
		final String authParams = buildAuthRequestBody(identifier, password);

		final LinkedMultiValueMap<String, String> headers = buildRequestAuthHeader(clientId, clientSecret);

		final String apiUrl = configurationService.getConfiguration().getString(API_URL_PROPERTY);

		if (StringUtils.isBlank(apiUrl))
		{
			throw new ConfigurationException("Could not find the API URL, configure the api.foodcrowd-ae.https property");
		}

		final String fullAuthUrl = apiUrl + AUTHORIZATION_PATH;

		final ResponseEntity<?> responseEntity = WebServiceApiUtil.httPOST(fullAuthUrl + authParams, null, headers,
				OAuth2AccessToken.class);

		if (responseEntity == null)
		{
			throw new InsufficientAuthenticationException("Authorization could not be successful due to empty response");
		}

		if (!responseEntity.getStatusCode().is2xxSuccessful())
		{
			throw new InsufficientAuthenticationException(
					"Authorization could not be successful due to " + responseEntity.getStatusCode() + " status code");
		}

		if (!(responseEntity.getBody() instanceof OAuth2AccessToken))
		{
			throw new InsufficientAuthenticationException("Could not extract the authorization data!");
		}

		return getResponse((OAuth2AccessToken) responseEntity.getBody());
	}

	private String buildAuthRequestBody(final String identifier, final String password)
	{
		final Optional<String> optionalUid = customerMatchingService.getCustomerUidAfterMatching(identifier);
		if (optionalUid.isEmpty())
		{
			throw new InsufficientAuthenticationException("No customers were found by identifier " + identifier);
		}
		final StringBuilder builder = new StringBuilder();
		builder.append("?username=").append(optionalUid.get()).append("&password=").append(password).append("&grant_type=password");
		return builder.toString();
	}

	private LinkedMultiValueMap<String, String> buildRequestAuthHeader(final String clientId, final String clientSecret)
	{
		final String plainCredentials = clientId + ":" + clientSecret;
		final String base64Credentials = new String(Base64.getEncoder().encode(plainCredentials.getBytes()));
		final LinkedMultiValueMap<String, String> headers = new LinkedMultiValueMap();
		headers.add(AUTHORIZATION, BASIC_AUTH + base64Credentials);
		return headers;
	}

	private ResponseEntity<Map<String, Object>> getResponse(final OAuth2AccessToken accessToken)
	{
		final HttpHeaders headers = new HttpHeaders();
		headers.set("Cache-Control", "no-store");
		headers.set("Pragma", "no-cache");
		headers.set("Content-Type", "application/json;charset=UTF-8");

		final Map<String, Object> tokenBody = new HashMap<String, Object>();
		tokenBody.put("access_token", accessToken.getValue());
		tokenBody.put("token_type", accessToken.getTokenType());
		tokenBody.put("refresh_token", accessToken.getRefreshToken().getValue());
		tokenBody.put("expires_in", accessToken.getExpiresIn());
		tokenBody.put("scope", accessToken.getScope().stream().collect(Collectors.joining(" ")));
		return new ResponseEntity(tokenBody, headers, HttpStatus.OK);
	}

}
