package com.aldahra.aldahracommercewebservices.core.validator;

import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercewebservicescommons.dto.order.StoreCreditDetailsWsDTO;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Locale;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.MessageSource;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.aldahra.aldahrastorecredit.enums.StoreCreditModeType;
import com.aldahra.facades.facade.CustomAcceleratorCheckoutFacade;



public class StoreCreditWsDTOValidator implements Validator
{
	@Resource(name = "acceleratorCheckoutFacade")
	private CustomAcceleratorCheckoutFacade checkoutFacade;


	@Resource(name = "cartFacade")
	private CartFacade cartFacade;

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "messageSource")
	private MessageSource messageSource;

	@Resource(name = "i18NService")
	private I18NService i18NService;

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.validation.Validator#supports(java.lang.Class)
	 */
	@Override
	public boolean supports(final Class<?> arg0)
	{
		// YTODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
	 */
	@Override
	public void validate(final Object storeCreditDetails, final Errors errors)
	{
		final String sctCode = ((StoreCreditDetailsWsDTO) storeCreditDetails).getStoreCreditModeTypeCode();
		final Double scAmount = ((StoreCreditDetailsWsDTO) storeCreditDetails).getAmount() == null ? Double.valueOf(0)
				: ((StoreCreditDetailsWsDTO) storeCreditDetails).getAmount();
		if (StringUtils.isEmpty(sctCode))
		{
			errors.rejectValue("storeCreditModeTypeCode", "storeCredit.sctCode.invalid");
		}
		StoreCreditModeType storeCreditModeType = null;
		try
		{
			storeCreditModeType = StoreCreditModeType.valueOf(sctCode);
		}
		catch (final Exception e)
		{
			errors.rejectValue("storeCreditModeTypeCode", "storeCredit.sctCode.invalid");
		}

		if (StoreCreditModeType.REDEEM_SPECIFIC_AMOUNT.getCode().equals(sctCode))
		{
			if (scAmount <= 0)
			{
				errors.rejectValue("amount", "storeCredit.scAmount.invalid");
			}

			final CartData cart = getCart();

			final CartModel cartModel = getCartModel();

			if (cart == null || cartModel == null || cartModel.getStore() == null
					|| !cartModel.getStore().isEnableCheckTotalPriceWithStoreCreditLimit())
			{
				return;
			}

			double totalPriceWithTaxVal = cart.getTotalPriceWithTax() == null || cart.getTotalPriceWithTax().getValue() == null
					? 0.0d
					: cart.getTotalPriceWithTax().getValue().doubleValue();


			final double storeCreditAmount = cart.getStoreCreditAmount() == null || cart.getStoreCreditAmount().getValue() == null
					? 0.0d
					: cart.getStoreCreditAmount().getValue().doubleValue();

			totalPriceWithTaxVal += storeCreditAmount;

			final double totalPricewithStoreCreditLimit = cartModel.getStore() == null ? 0.0d
					: cartModel.getStore().getTotalPriceWithStoreCreditLimit();

			totalPriceWithTaxVal -= totalPricewithStoreCreditLimit;

			if (totalPriceWithTaxVal < scAmount)
			{

				final Locale currentLocale = getI18NService().getCurrentLocale();
				final double param = getBaseStoreService().getCurrentBaseStore().getTotalPriceWithStoreCreditLimit();
				final Double[] params = new Double[1];
				params[0] = param;
				final String msg = getMessageSource().getMessage("storeCredit.scAmount.check.limit.invalid", params,
						"storeCredit.scAmount.check.limit.invalid", currentLocale);
				errors.rejectValue("amount", msg != null ? msg : "");
			}
		}
	}

	/**
	 * @return the i18NService
	 */



	protected I18NService getI18NService()
	{
		return i18NService;
	}

	/**
	 * @return the baseStoreService
	 */
	protected BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	public MessageSource getMessageSource()
	{
		return messageSource;
	}

	protected CartData getCart()
	{
		return cartFacade.hasSessionCart() ? cartFacade.getSessionCart() : null;
	}

	protected CartModel getCartModel()
	{
		return cartService.hasSessionCart() ? cartService.getSessionCart() : null;
	}
}
