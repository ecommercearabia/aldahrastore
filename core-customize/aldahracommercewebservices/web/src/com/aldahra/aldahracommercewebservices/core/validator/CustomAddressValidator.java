/**
 *
 */
package com.aldahra.aldahracommercewebservices.core.validator;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercewebservicescommons.dto.user.AddressWsDTO;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * @author monzer
 *
 *         Validates the configurable attributes by the CMSSite
 */
public class CustomAddressValidator implements Validator
{

	private static final String INVALID_STREET_NAME = "invalid.field.streetName";

	private static final String INVALID_BUILDING_NAME = "invalid.field.buildingName";

	private static final String INVALID_APARTMENT_NUMBER = "invalid.field.apartmentNumber";

	@Resource(name="cmsSiteService")
	private CMSSiteService cmsSiteService;

	private String streetName;
	private String buildingName;
	private String apartmentNumber;

	@Override
	public boolean supports(final Class<?> arg0)
	{
		return AddressWsDTO.class.isAssignableFrom(arg0);
	}

	@Override
	public void validate(final Object obj, final Errors errors)
	{
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		if(currentSite != null && currentSite.isCustomStreetNameAddressEnabled() && currentSite.isCustomStreetNameAddressRequired()) {
			final String streetName = (String) errors.getFieldValue(this.streetName);
			if(StringUtils.isBlank(streetName)) {
				 errors.rejectValue(this.streetName, INVALID_STREET_NAME, new String[]
						  { this.streetName }, "This field is not a valid street name.");
			}
		}
		if(currentSite != null && currentSite.isBuildingNameAddressEnabled() && currentSite.isBuildingNameAddressRequired()) {
			final String buildingName = (String) errors.getFieldValue(this.buildingName);
			if(StringUtils.isBlank(buildingName)) {
				errors.rejectValue(this.buildingName, INVALID_BUILDING_NAME, new String[]
						  { this.buildingName }, "This field is not a valid building name.");
			}
		}
		if (currentSite != null && currentSite.isApartmentNumberAddressEnabled() && currentSite.isApartmentNumberAddressRequired())
		{
			final String apartmentNumber = (String) errors.getFieldValue(this.apartmentNumber);
			if(StringUtils.isBlank(apartmentNumber)) {
				errors.rejectValue(this.apartmentNumber, INVALID_APARTMENT_NUMBER, new String[]
						  { this.apartmentNumber }, "This field is not a valid apartment number.");
			}
		}
	}

	/**
	 * @return the streetName
	 */
	public String getStreetName()
	{
		return streetName;
	}

	/**
	 * @param streetName
	 *           the streetName to set
	 */
	public void setStreetName(final String streetName)
	{
		this.streetName = streetName;
	}

	/**
	 * @return the buildingName
	 */
	public String getBuildingName()
	{
		return buildingName;
	}

	/**
	 * @param buildingName
	 *           the buildingName to set
	 */
	public void setBuildingName(final String buildingName)
	{
		this.buildingName = buildingName;
	}

	/**
	 * @return the apartmentNumber
	 */
	public String getApartmentNumber()
	{
		return apartmentNumber;
	}

	/**
	 * @param apartmentNumber
	 *           the apartmentNumber to set
	 */
	public void setApartmentNumber(final String apartmentNumber)
	{
		this.apartmentNumber = apartmentNumber;
	}

}
