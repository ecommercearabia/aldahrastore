/**
 *
 */
package com.aldahra.aldahracommercewebservices.core.v2.filter;

import java.io.IOException;
import java.util.Base64;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.NullRememberMeServices;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 * @author monzer
 *
 */
public class ClientCredentialsFilter extends OncePerRequestFilter
{

	private final boolean allowOnlyPost = false;
	private final AuthenticationDetailsSource<HttpServletRequest, ?> authenticationDetailsSource = new WebAuthenticationDetailsSource();
	private AuthenticationManager authenticationManager;
	private final RememberMeServices rememberMeServices = new NullRememberMeServices();
	private final String credentialsCharset = "UTF-8";

	@Override
	public void destroy()
	{

	}

	@Override
	protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
			final FilterChain filterChain) throws ServletException, IOException
	{
		final boolean endsWith = request.getRequestURI().endsWith("/otp/verify/login")
				|| request.getRequestURI().endsWith("/signature/auth");
		if (!endsWith)
		{
			filterChain.doFilter(request, response);
			return;
		}

		final String header = request.getHeader("Authorization");

		if (header == null || !header.toLowerCase().startsWith("basic "))
		{
			filterChain.doFilter(request, response);
			return;
		}

		final String[] tokens = extractAndDecodeHeader(header, request);
		assert tokens.length == 2;

		final String clientId = tokens[0];
		final String clientSecret = tokens[1];

		try
		{
			if (authenticationIsRequired(clientId))
			{
				final UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(clientId,
						clientSecret);
				authRequest.setDetails(this.authenticationDetailsSource.buildDetails(request));
				final Authentication authResult = this.authenticationManager.authenticate(authRequest);

				SecurityContextHolder.getContext().setAuthentication(authResult);

				this.rememberMeServices.loginSuccess(request, response, authResult);

				onSuccessfulAuthentication(request, response, authResult);
			}

		}
		catch (final AuthenticationException failed)
		{
			SecurityContextHolder.clearContext();
			this.rememberMeServices.loginFail(request, response);
			throw failed;
		}

		filterChain.doFilter(request, response);
	}

	private boolean authenticationIsRequired(final String username)
	{
		final Authentication existingAuth = SecurityContextHolder.getContext().getAuthentication();

		if (existingAuth == null || !existingAuth.isAuthenticated())
		{
			return true;
		}

		if (existingAuth instanceof UsernamePasswordAuthenticationToken && !existingAuth.getName().equals(username))
		{
			return true;
		}

		if (existingAuth instanceof AnonymousAuthenticationToken)
		{
			return true;
		}

		return false;
	}

	protected void onSuccessfulAuthentication(final HttpServletRequest request, final HttpServletResponse response,
			final Authentication authResult) throws IOException
	{
	}

	protected void onUnsuccessfulAuthentication(final HttpServletRequest request, final HttpServletResponse response,
			final AuthenticationException failed) throws IOException
	{
	}

	/**
	 * @return the authenticationManager
	 */
	public AuthenticationManager getAuthenticationManager()
	{
		return authenticationManager;
	}

	/**
	 * @param authenticationManager
	 *           the authenticationManager to set
	 */
	public void setAuthenticationManager(final AuthenticationManager authenticationManager)
	{
		this.authenticationManager = authenticationManager;
	}

	private String[] extractAndDecodeHeader(final String header, final HttpServletRequest request) throws IOException
	{

		final byte[] base64Token = header.substring(6).getBytes("UTF-8");
		byte[] decoded;
		try
		{
			decoded = Base64.getDecoder().decode(base64Token);
		}
		catch (final IllegalArgumentException e)
		{
			throw new BadCredentialsException("Failed to decode basic authentication token");
		}

		final String token = new String(decoded, credentialsCharset);

		final int delim = token.indexOf(":");

		if (delim == -1)
		{
			throw new BadCredentialsException("Invalid basic authentication token");
		}
		return new String[]
		{ token.substring(0, delim), token.substring(delim + 1) };
	}

}
