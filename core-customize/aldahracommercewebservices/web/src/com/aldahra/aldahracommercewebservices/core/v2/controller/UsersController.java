/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.aldahra.aldahracommercewebservices.core.v2.controller;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cmswebservices.dto.UserGroupListWsDTO;
import de.hybris.platform.commercefacades.customergroups.CustomerGroupFacade;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.MobileTokenData;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commercefacades.user.data.UserGroupDataList;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commercewebservicescommons.dto.product.PriceWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.user.ReferralCodeWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.user.UserSignUpWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.user.UserValidationWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.user.UserWsDTO;
import de.hybris.platform.commercewebservicescommons.errors.exceptions.RequestParameterException;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.webservicescommons.cache.CacheControl;
import de.hybris.platform.webservicescommons.cache.CacheControlDirective;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdAndUserIdParam;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdParam;
import de.hybris.platform.webservicescommons.swagger.ApiFieldsParam;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.util.UriUtils;

import com.aldahra.aldahracommercewebservices.core.constants.YcommercewebservicesConstants;
import com.aldahra.aldahracommercewebservices.core.populator.HttpRequestCustomerDataPopulator;
import com.aldahra.aldahracommercewebservices.core.user.data.LoyaltyPointHistoriesData;
import com.aldahra.aldahracommercewebservices.core.user.data.StoreCreditHistoriesData;
import com.aldahra.aldahracommercewebservices.core.validation.data.UserValidationData;
import com.aldahra.aldahracommercewebservices.dto.user.data.LoyaltyPointHistoriesWsDTO;
import com.aldahra.aldahracommercewebservices.dto.user.data.StoreCreditHistoriesWsDTO;
import com.aldahra.aldahrapayment.ccavenue.enums.PaymentExceptionType;
import com.aldahra.aldahrapayment.ccavenue.exception.PaymentException;
import com.aldahra.aldahrapayment.context.PaymentContext;
import com.aldahra.aldahrareferralcode.exception.ReferralCodeException;
import com.aldahra.aldahrareferralcode.model.ReferralCodeModel;
import com.aldahra.aldahrareferralcode.service.ReferralCodeService;
import com.aldahra.aldahrareferralcodefacades.AppliedReferralHistoryData;
import com.aldahra.aldahrareferralcodefacades.ReferralCodeData;
import com.aldahra.aldahrareferralcodefacades.facade.ReferralCodeFacade;
import com.aldahra.aldahrastorecreditfacades.StoreCreditHistoryData;
import com.aldahra.aldahrastorecreditfacades.facade.StoreCreditFacade;
import com.aldahra.aldahrauserfacades.customer.facade.CustomCustomerFacade;
import com.aldahra.core.loyaltypoint.data.LoyaltyPointHistoryData;
import com.aldahra.facades.facade.LoyaltyPointFacade;
import com.aldahracommercewebservices.ccavenue.entry.CustomerPaymentOptionList;
import com.aldahracommercewebservices.ccavenue.entry.CustomerPaymentOptionListWsDTO;
import com.aldahracommercewebservices.core.user.dto.MobileTokenDataList;
import com.aldahracommercewebservices.core.user.dto.MobileTokenListWsDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;


@Controller
@RequestMapping(value = "/{baseSiteId}/users")
@CacheControl(directive = CacheControlDirective.PRIVATE)
@Api(tags = "Users")
public class UsersController extends BaseCommerceController
{
	private static final Logger LOG = LoggerFactory.getLogger(UsersController.class);
	public static final String UPDATE_USER_MAPPER_CONFIG = "titleCode,firstName,lastName, mobileNumber,mobileCountry(isocode), nationality(code),nationalityID";
	private static final String ERROR_MSG = "Error while trying to get information from server, please contact customer support";

	@Resource(name = "paymentContext")
	private PaymentContext paymentContext;


	@Resource(name = "wsCustomerFacade")
	private CustomCustomerFacade customerFacade;
	@Resource(name = "wsCustomerGroupFacade")
	private CustomerGroupFacade customerGroupFacade;
	@Resource(name = "httpRequestCustomerDataPopulator")
	private HttpRequestCustomerDataPopulator httpRequestCustomerDataPopulator;
	@Resource(name = "HttpRequestUserSignUpDTOPopulator")
	private Populator<HttpServletRequest, UserSignUpWsDTO> httpRequestUserSignUpDTOPopulator;
	@Resource(name = "putUserDTOValidator")
	private Validator putUserDTOValidator;
	@Resource(name = "userSignUpDTOValidator")
	private Validator userSignUpDTOValidator;
	@Resource(name = "guestConvertingDTOValidator")
	private Validator guestConvertingDTOValidator;
	@Resource(name = "passwordStrengthValidator")
	private Validator passwordStrengthValidator;

	@Resource(name = "referralCodeFacade")
	private ReferralCodeFacade referralCodeFacade;

	@Resource(name = "referralCodeService")
	private ReferralCodeService referralCodeService;

	@Resource(name = "storeCreditFacade")
	private StoreCreditFacade storeCreditFacade;

	@Resource(name = "loyaltyPointFacade")
	private LoyaltyPointFacade loyaltyPointFacade;

	@Secured(
	{ "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.CREATED)
	@ResponseBody
	@ApiOperation(hidden = true, value = " Registers a customer", notes = "Registers a customer. There are two options for registering a customer. The first option requires "
			+ "the following parameters: login, password, firstName, lastName, titleCode. The second option converts a guest to a customer. In this case, the required parameters are: guid, password.")
	@ApiBaseSiteIdParam
	public UserWsDTO createUser(@ApiParam(value = "Customer's login. Customer login is case insensitive.")
	@RequestParam(required = false)
	final String login, @ApiParam(value = "Customer's password.", required = true)
	@RequestParam
	final String password, @ApiParam(value = "Customer's title code. For a list of codes, see /{baseSiteId}/titles resource")
	@RequestParam(required = false)
	final String titleCode, @ApiParam(value = "Customer's first name.")
	@RequestParam(required = false)
	final String firstName, @ApiParam(value = "Customer's last name.")
	@RequestParam(required = false)
	final String lastName, @ApiParam(value = "Customer's mobile number.")
	@RequestParam(required = false)
	final String mobileNumber, @ApiParam(value = "Customer's mobile country code.")
	@RequestParam(required = false)
	final String mobileCountryCode, @ApiParam(value = "Guest order's guid.")
	@RequestParam(required = false)
	final String nationality, @ApiParam(value = "Customer's nationality.")
	@RequestParam(required = false)
	final String nationalityId, @ApiParam(value = "Customer has national id.")
	@RequestParam(required = false)
	final boolean hasNationalId, @ApiParam(value = "Customer's nationality id.")
	@RequestParam(required = false)
	final String appliedReferralCode, @ApiParam(value = "Customer's applied referral code.")
	@RequestParam(required = false)
	final String guid, @ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields, final HttpServletRequest httpRequest, final HttpServletResponse httpResponse) throws DuplicateUidException
	{
		final UserSignUpWsDTO user = new UserSignUpWsDTO();
		httpRequestUserSignUpDTOPopulator.populate(httpRequest, user);
		CustomerData customer = null;
		final String userId;
		if (guid != null)
		{
			validate(user, "user", guestConvertingDTOValidator);
			convertToCustomer(password, guid);
			customer = customerFacade.getCurrentCustomer();
			userId = customer.getUid();
		}
		else
		{
			validate(user, "user", userSignUpDTOValidator);
			registerNewUser(login, password, titleCode, firstName, lastName, mobileNumber, mobileCountryCode, nationality,
					hasNationalId, nationalityId, appliedReferralCode);
			userId = login.toLowerCase(Locale.ENGLISH);
			customer = customerFacade.getUserForUID(userId);
		}
		httpResponse.setHeader(YcommercewebservicesConstants.LOCATION, getAbsoluteLocationURL(httpRequest, userId));
		return getDataMapper().map(customer, UserWsDTO.class, fields);
	}

	protected void convertToCustomer(final String password, final String guid) throws DuplicateUidException
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("convertToCustomer: guid=" + sanitize(guid));
		}

		try
		{
			customerFacade.changeGuestToCustomer(password, guid);
		}
		catch (final UnknownIdentifierException | IllegalArgumentException ex)
		{
			/*
			 * IllegalArgumentException - occurs when order does not belong to guest user. For security reasons it's better
			 * to treat it as "unknown identifier" error
			 */

			throw new RequestParameterException("Order with guid " + sanitize(guid) + " not found in current BaseStore",
					RequestParameterException.UNKNOWN_IDENTIFIER, "guid", ex);
		}
	}

	protected void registerNewUser(final String login, final String password, final String titleCode, final String firstName,
			final String lastName, final String mobileNumber, final String mobileCountryCode, final String nationality,
			final boolean hasNationalId, final String nationalityId, final String appliedRefrralCode) throws DuplicateUidException
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("registerUser: login=" + sanitize(login));
		}

		if (!EmailValidator.getInstance().isValid(login))
		{
			throw new RequestParameterException("Login [" + sanitize(login) + "] is not a valid e-mail address!",
					RequestParameterException.INVALID, "login");
		}

		final RegisterData registerData = createRegisterData(login, password, titleCode, firstName, lastName, mobileNumber,
				mobileCountryCode, nationality, hasNationalId, nationalityId, appliedRefrralCode);
		customerFacade.register(registerData);
	}

	private RegisterData createRegisterData(final String login, final String password, final String titleCode,
			final String firstName, final String lastName, final String mobileNumber, final String mobileCountryCode,
			final String nationality, final boolean hasNationalId, final String nationalityId, final String appliedRefrralCode)
	{
		final RegisterData registerData = new RegisterData();
		registerData.setFirstName(firstName);
		registerData.setLastName(lastName);
		registerData.setLogin(login);
		registerData.setPassword(password);
		registerData.setTitleCode(titleCode);
		registerData.setMobileNumber(mobileNumber);
		registerData.setMobileCountry(mobileCountryCode);
		registerData.setReferralCode(appliedRefrralCode);
		registerData.setNationality(nationality);
		registerData.setNationalityId(nationalityId);
		return registerData;
	}

	@Secured(
	{ "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/verification", method = RequestMethod.POST, consumes =
	{ MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	@ResponseBody
	@ApiOperation(nickname = "validateUser", value = " validate customer", notes = "validate a customer. Requires the following "
			+ "parameters: login, password, firstName, lastName, titleCode, mobileNumber, mobileCountry.")
	@ApiBaseSiteIdParam
	public UserValidationWsDTO validateUser(@ApiParam(value = "User's object.", required = true)
	@RequestBody
	final UserSignUpWsDTO user, @ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields, final HttpServletRequest httpRequest, final HttpServletResponse httpResponse)
	{
		// validation is a bit different here
		validate(user, "user", userSignUpDTOValidator);

		final UserValidationData validationData = new UserValidationData();
		validationData.setDecision("ACCEPT");

		return getDataMapper().map(validationData, UserValidationWsDTO.class, fields);
	}

	@Secured(
	{ "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(method = RequestMethod.POST, consumes =
	{ MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	@ResponseStatus(value = HttpStatus.CREATED)
	@ResponseBody
	@ApiOperation(nickname = "createUser", value = " Registers a customer", notes = "Registers a customer. Requires the following "
			+ "parameters: login, password, firstName, lastName, titleCode.")
	@ApiBaseSiteIdParam
	public UserWsDTO createUser(@ApiParam(value = "User's object.", required = true)
	@RequestBody
	final UserSignUpWsDTO user, @ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields, final HttpServletRequest httpRequest, final HttpServletResponse httpResponse)
	{
		validate(user, "user", userSignUpDTOValidator);
		final RegisterData registerData = getDataMapper().map(user, RegisterData.class,
				"login,password,titleCode,firstName,lastName,mobileNumber");

		registerData.setMobileNumber(user.getMobileNumber());
		registerData.setMobileCountry(user.getMobileCountryCode());
		registerData.setReferralCode(user.getReferralCode());
		registerData.setNationality(user.getNationality());
		registerData.setNationalityId(user.getNationalityId());
		registerData.setHasNationalityId(user.isHasNationalId());
		boolean userExists = false;
		try
		{
			customerFacade.register(registerData);
		}
		catch (final DuplicateUidException ex)
		{
			userExists = true;
			LOG.debug("Duplicated UID", ex);
		}
		final String userId = user.getUid().toLowerCase(Locale.ENGLISH);
		httpResponse.setHeader(YcommercewebservicesConstants.LOCATION, getAbsoluteLocationURL(httpRequest, userId)); //NOSONAR
		final CustomerData customerData = getCustomerData(registerData, userExists, userId);
		return getDataMapper().map(customerData, UserWsDTO.class, fields);
	}

	protected CustomerData getCustomerData(final RegisterData registerData, final boolean userExists, final String userId)
	{
		final CustomerData customerData;
		if (userExists)
		{
			customerData = customerFacade.nextDummyCustomerData(registerData);
		}
		else
		{
			customerData = customerFacade.getUserForUID(userId);
		}
		return customerData;
	}

	protected String getAbsoluteLocationURL(final HttpServletRequest httpRequest, final String uid)
	{
		final String requestURL = httpRequest.getRequestURL().toString();
		final StringBuilder absoluteURLSb = new StringBuilder(requestURL);
		if (!requestURL.endsWith(YcommercewebservicesConstants.SLASH))
		{
			absoluteURLSb.append(YcommercewebservicesConstants.SLASH);
		}
		absoluteURLSb.append(UriUtils.encodePathSegment(uid, StandardCharsets.UTF_8.name()));
		return absoluteURLSb.toString();
	}


	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{userId}", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getUser", value = "Get customer profile", notes = "Returns customer profile.")
	@ApiBaseSiteIdAndUserIdParam
	public UserWsDTO getUser(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{

		final CustomerData customerData = customerFacade.getCurrentCustomer();
		return getDataMapper().map(customerData, UserWsDTO.class, fields);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{userId}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(hidden = true, value = "Updates customer profile", notes = "Updates customer profile. Attributes not provided in the request body will be defined again (set to null or default).")
	@ApiImplicitParams(
	{ @ApiImplicitParam(name = "baseSiteId", value = "Base site identifier", required = true, dataType = "String", paramType = "path"),
			@ApiImplicitParam(name = "userId", value = "User identifier.", required = true, dataType = "String", paramType = "path"),
			@ApiImplicitParam(name = "language", value = "Customer's language.", required = false, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "currency", value = "Customer's currency.", required = false, dataType = "String", paramType = "query") })
	public void replaceUser(@ApiParam(value = "Customer's first name.", required = true)
	@RequestParam
	final String firstName, @ApiParam(value = "Customer's last name.", required = true)
	@RequestParam
	final String lastName,
			@ApiParam(value = "Customer's title code. For a list of codes, see /{baseSiteId}/titles resource", required = false)
			@RequestParam
			final String titleCode, final HttpServletRequest request) throws DuplicateUidException
	{
		final CustomerData customer = customerFacade.getCurrentCustomer();
		if (LOG.isDebugEnabled())
		{
			LOG.debug("putCustomer: userId={}", customer.getUid());
		}
		customer.setFirstName(firstName);
		customer.setLastName(lastName);
		customer.setTitleCode(titleCode);
		customer.setLanguage(null);
		customer.setCurrency(null);
		httpRequestCustomerDataPopulator.populate(request, customer);

		customerFacade.updateFullProfile(customer);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{userId}", method = RequestMethod.PUT, consumes =
	{ MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(nickname = "replaceUser", value = "Updates customer profile", notes = "Updates customer profile. Attributes not provided in the request body will be defined again (set to null or default).")
	@ApiBaseSiteIdAndUserIdParam
	public void replaceUser(@ApiParam(value = "User's object", required = true)
	@RequestBody
	final UserWsDTO user) throws DuplicateUidException
	{
		validate(user, "user", putUserDTOValidator);

		final CustomerData customer = customerFacade.getCurrentCustomer();
		if (LOG.isDebugEnabled())
		{
			LOG.debug("replaceUser: userId={}", customer.getUid());
		}

		getDataMapper().map(user, customer, UPDATE_USER_MAPPER_CONFIG, true);
		customerFacade.updateFullProfile(customer);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{userId}", method = RequestMethod.PATCH)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(hidden = true, value = "Updates customer profile", notes = "Updates customer profile. Only attributes provided in the request body will be changed.")
	@ApiImplicitParams(
	{ @ApiImplicitParam(name = "baseSiteId", value = "Base site identifier", required = true, dataType = "String", paramType = "path"),
			@ApiImplicitParam(name = "userId", value = "User identifier", required = true, dataType = "String", paramType = "path"),
			@ApiImplicitParam(name = "firstName", value = "Customer's first name", required = false, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "lastName", value = "Customer's last name", required = false, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "titleCode", value = "Customer's title code. Customer's title code. For a list of codes, see /{baseSiteId}/titles resource", required = false, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "language", value = "Customer's language", required = false, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "currency", value = "Customer's currency", required = false, dataType = "String", paramType = "query") })
	public void updateUser(final HttpServletRequest request) throws DuplicateUidException
	{
		final CustomerData customer = customerFacade.getCurrentCustomer();
		if (LOG.isDebugEnabled())
		{
			LOG.debug("updateUser: userId={}", customer.getUid());
		}
		httpRequestCustomerDataPopulator.populate(request, customer);
		customerFacade.updateFullProfile(customer);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{userId}", method = RequestMethod.PATCH, consumes =
	{ MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(nickname = "updateUser", value = "Updates customer profile", notes = "Updates customer profile. Only attributes provided in the request body will be changed.")
	@ApiBaseSiteIdAndUserIdParam
	public void updateUser(@ApiParam(value = "User's object.", required = true)
	@RequestBody
	final UserWsDTO user) throws DuplicateUidException
	{
		final CustomerData customer = customerFacade.getCurrentCustomer();
		if (LOG.isDebugEnabled())
		{
			LOG.debug("updateUser: userId={}", customer.getUid());
		}

		getDataMapper().map(user, customer,
				"firstName,lastName,titleCode,currency(isocode),language(isocode), mobileNumber, mobileCountry", false);
		customerFacade.updateFullProfile(customer);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{userId}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(nickname = "removeUser", value = "Delete customer profile.", notes = "Removes customer profile.")
	@ApiBaseSiteIdAndUserIdParam
	public void removeUser()
	{
		final CustomerData customer = customerFacade.closeAccount();
		if (LOG.isDebugEnabled())
		{
			LOG.debug("removeUser: userId={}", customer.getUid());
		}
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{userId}/login", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(nickname = "replaceUserLogin", value = "Changes customer's login name.", notes = "Changes a customer's login name. Requires the customer's current password.")
	@ApiBaseSiteIdAndUserIdParam
	public void replaceUserLogin(
			@ApiParam(value = "Customer's new login name. Customer login is case insensitive.", required = true)
			@RequestParam
			final String newLogin, @ApiParam(value = "Customer's current password.", required = true)
			@RequestParam
			final String password) throws DuplicateUidException
	{
		if (!EmailValidator.getInstance().isValid(newLogin))
		{
			throw new RequestParameterException("Login [" + newLogin + "] is not a valid e-mail address!",
					RequestParameterException.INVALID, "newLogin");
		}
		customerFacade.changeUid(newLogin, password);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{userId}/password", method = RequestMethod.PUT)
	@ResponseStatus(value = HttpStatus.ACCEPTED)
	@ApiOperation(nickname = "replaceUserPassword", value = "Changes customer's password", notes = "Changes customer's password.")
	@ApiBaseSiteIdAndUserIdParam
	public void replaceUserPassword(@ApiParam(value = "User identifier.", required = true)
	@PathVariable
	final String userId, @ApiParam("Old password. Required only for ROLE_CUSTOMERGROUP")
	@RequestParam(required = false)
	final String old, @ApiParam(value = "New password.", required = true)
	@RequestParam(value = "new")
	final String newPassword)
	{
		final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		final UserSignUpWsDTO customer = new UserSignUpWsDTO();
		customer.setPassword(newPassword);
		validate(customer, "password", passwordStrengthValidator);
		if (containsRole(auth, "ROLE_TRUSTED_CLIENT") || containsRole(auth, "ROLE_CUSTOMERMANAGERGROUP"))
		{
			customerFacade.setPassword(userId, newPassword);
		}
		else
		{
			if (StringUtils.isEmpty(old))
			{
				throw new RequestParameterException("Request parameter 'old' is missing.", RequestParameterException.MISSING, "old");
			}
			customerFacade.changePassword(old, newPassword);
		}
	}

	protected boolean containsRole(final Authentication auth, final String role)
	{
		for (final GrantedAuthority ga : auth.getAuthorities())
		{
			if (ga.getAuthority().equals(role))
			{
				return true;
			}
		}
		return false;
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{userId}/customergroups", method = RequestMethod.GET)
	@ApiOperation(nickname = "getUserCustomerGroups", value = "Get all customer groups of a customer.", notes = "Returns all customer groups of a customer.")
	@ApiBaseSiteIdAndUserIdParam
	@ResponseBody
	public UserGroupListWsDTO getUserCustomerGroups(@ApiParam(value = "User identifier.", required = true)
	@PathVariable
	final String userId, @ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		final UserGroupDataList userGroupDataList = new UserGroupDataList();
		userGroupDataList.setUserGroups(customerGroupFacade.getCustomerGroupsForUser(userId));
		return getDataMapper().map(userGroupDataList, UserGroupListWsDTO.class, fields);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{userId}/storecredit/history", method = RequestMethod.GET)
	@ApiOperation(nickname = "getStoreCreditHistory", value = "Get store credit history  of a customer.", notes = "Returns all customer store credit history.")
	@ApiBaseSiteIdAndUserIdParam
	@ResponseBody
	public StoreCreditHistoriesWsDTO getStoreCreditHistory(@ApiParam(value = "User identifier.", required = true)
	@PathVariable
	final String userId,
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields)
	{
		final Optional<List<StoreCreditHistoryData>> storeCreditHistoryByCurrentUser = storeCreditFacade
				.getStoreCreditHistoryByCurrentUser();
		final StoreCreditHistoriesData creditHistoriesData = new StoreCreditHistoriesData();
		if (storeCreditHistoryByCurrentUser.isPresent())
		{
			creditHistoriesData.setStoreCreditHistroy(storeCreditHistoryByCurrentUser.get());
		}

		return getDataMapper().map(creditHistoriesData, StoreCreditHistoriesWsDTO.class, fields);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{userId}/storecredit/amount", method = RequestMethod.GET)
	@ApiOperation(nickname = "getStoreCreditHistory", value = "Get the store credit available amount  of a customer.", notes = "Returns the available store credit.")
	@ApiBaseSiteIdAndUserIdParam
	@ResponseBody
	public PriceWsDTO getStoreCreditAvailableAmount(@ApiParam(value = "User identifier.", required = true)
	@PathVariable
	final String userId,
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields)
	{

		final Optional<PriceData> storeCreditAmountByCurrentUserAndCurrentBaseStore = storeCreditFacade
				.getStoreCreditAmountByCurrentUserAndCurrentBaseStore();
		final PriceData amount = storeCreditAmountByCurrentUserAndCurrentBaseStore.isPresent()
				? storeCreditAmountByCurrentUserAndCurrentBaseStore.get()
				: new PriceData();

		return getDataMapper().map(amount, PriceWsDTO.class, fields);
	}

	@Secured(
	{ "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_CUSTOMERGROUP" })
	@RequestMapping(value = "/{userId}/signature", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	@ResponseBody
	@ApiOperation(value = "Update the current  customer  signature")
	@ApiBaseSiteIdAndUserIdParam
	public void putSignatureIdForUser(@ApiParam(value = "User identifier.", required = true)
	@PathVariable
	final String userId, @ApiParam(value = "Customer signature.")
	@RequestParam(required = false)
	final String signatureId)
	{
		customerFacade.updateSignatureIdCurrentCustomer(signatureId);
	}

	@Secured(
	{ "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_CUSTOMERGROUP" })
	@RequestMapping(value = "/{userId}/referral-code/generate", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	@ResponseBody
	@ApiOperation(value = "Generate Referral Code")
	@ApiBaseSiteIdAndUserIdParam
	public ReferralCodeWsDTO generateReferralCode(@ApiParam(value = "User identifier.", required = true)
	@PathVariable
	final String userId,
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields)
	{
		try
		{
			final Optional<ReferralCodeModel> referralCode = referralCodeFacade.generateByCurrentBaseStoreAndCurrentCustomer();
			return referralCode.isEmpty() ? null : getDataMapper().map(referralCode.get(), ReferralCodeWsDTO.class, fields);
		}
		catch (final ReferralCodeException e)
		{

			return null;
		}
	}

	@Secured(
	{ "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_CUSTOMERGROUP" })
	@RequestMapping(value = "/{userId}/referral-code", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	@ResponseBody
	@ApiOperation(value = "Get Referral Code Data")
	@ApiBaseSiteIdAndUserIdParam
	public ReferralCodeWsDTO getReferralCodeData(@ApiParam(value = "User identifier.", required = true)
	@PathVariable
	final String userId, @RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		final Optional<ReferralCodeData> referralCode = referralCodeFacade.getByCurrentCustomer();
		if (referralCode.isEmpty())
		{
			return null;
		}
		final ReferralCodeData referralCodeData = referralCode.get();
		final List<AppliedReferralHistoryData> histories = referralCodeData.getHistories();

		final ReferralCodeWsDTO referralCodeWsDTO = getDataMapper().map(referralCode.get(), ReferralCodeWsDTO.class, fields);

		return referralCodeWsDTO;

	}

	@Secured(
	{ "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_CUSTOMERGROUP" })
	@RequestMapping(value = "/{userId}/subscribe", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	@ApiOperation(hidden = false, value = "Send referral subscription emails", notes = "Send referral subscription emails. Note that the emails must be comma seperated, also the inSeperateEmails represents how the emails will sent , if true each email will send separate from others, otherwise all emails will sent in one email . ")
	@ApiBaseSiteIdAndUserIdParam
	@ApiImplicitParams(
	{ @ApiImplicitParam(name = "baseSiteId", value = "Base site identifier", required = true, dataType = "String", paramType = "path"),
			@ApiImplicitParam(name = "userId", value = "User identifier", required = true, dataType = "String", paramType = "path"),
			@ApiImplicitParam(name = "emails", value = "emails", required = true, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "inSeperateEmails", value = "inSeperateEmails", required = true, dataType = "boolean", paramType = "query") })
	public void subscribeReferralByEmails(@ApiParam(value = "User identifier.", required = true)
	@PathVariable
	final String userId, final String emails, final boolean inSeperateEmails) throws ReferralCodeException
	{

		referralCodeService.shareByEmailsByCurrentSiteAndStore(Arrays.asList(emails.split(",")), inSeperateEmails);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{userId}/mobile-token", method = RequestMethod.PATCH)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(nickname = "replaceUserLogin", value = "Changes customer's login name.", notes = "Changes a customer's login name. Requires the customer's current password.")
	@ApiBaseSiteIdAndUserIdParam
	public void updateMobileToken(@RequestParam(name = "mobileToken", required = true)
	final String mobileToken)
	{
		customerFacade.updateCustomerMobileToken(mobileToken);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{userId}/mobile-tokens", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getMobileTokens", value = "Return List of Mobile tokens for customer", notes = "Return List of Mobile tokens for customer. Requires the customer's current password.")
	@ApiBaseSiteIdAndUserIdParam
	public MobileTokenListWsDTO getMobileTokens(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{

		final List<MobileTokenData> customerMobileTokens = customerFacade.getCustomerMobileTokens();

		if (customerMobileTokens != null && !CollectionUtils.isEmpty(customerMobileTokens))
		{
			final MobileTokenDataList dto = new MobileTokenDataList();
			dto.setTokens(customerMobileTokens);

			return getDataMapper().map(dto, MobileTokenListWsDTO.class, fields);
		}

		return new MobileTokenListWsDTO();
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{userId}/customer-payment-options", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getCustomerPaymentOptions", value = "Get Customer Payment Options", notes = "Returns Customer  Payment Options.")
	@ApiBaseSiteIdAndUserIdParam
	public CustomerPaymentOptionListWsDTO getCustomerPaymentOptions(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields) throws PaymentException
	{
		if (!isEnableManageSavedCards())
		{
			throw new PaymentException(
					getMessageSource().getMessage("manage.saved.cards.is.not.enabled", null, getI18NService().getCurrentLocale()),
					PaymentExceptionType.BAD_REQUEST);
		}
		try
		{
			final CustomerPaymentOptionList customerPaymentOptionList = new CustomerPaymentOptionList();
			customerPaymentOptionList.setCustomerPaymentOptions(paymentContext.getCustomerPaymentOptionsByCurrentStoreAndCustomer());
			return getDataMapper().map(customerPaymentOptionList, CustomerPaymentOptionListWsDTO.class, fields);

		}
		catch (final PaymentException e)
		{
			LOG.error("UsersController -> getCustomerPaymentOptions:PaymentException Type[" + e.getType() + "] , Message["
					+ e.getMessage() + "]");
			String message = e.getMessage();
			switch (e.getType())
			{
				case FAILURE_RESULT:
					message = getMessageSource().getMessage("manage.saved.cards.failure.result.error", null,
							"No matching results for this customer", getI18NService().getCurrentLocale());
					break;
				case INVALID_RESPONSE_SYNTAX_EXPECTED_JSON:
					message = getMessageSource().getMessage("manage.saved.cards.invalid.response.error", null, ERROR_MSG,
							getI18NService().getCurrentLocale());
					break;
				case BAD_REQUEST:
					message = getMessageSource().getMessage("manage.saved.cards.bad.request.error", null, ERROR_MSG,
							getI18NService().getCurrentLocale());
					break;
				default:
					message = getMessageSource().getMessage("manage.saved.cards.bad.request.error", null, ERROR_MSG,
							getI18NService().getCurrentLocale());
					break;
			}
			throw new PaymentException(message, e.getType());
		}
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{userId}/customer-payment-options/{customerCardId}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(nickname = "deleteCustomerPaymentOption", value = "Delete Customer Payment Option.", notes = "Delete Customer Payment Option.")
	@ApiBaseSiteIdAndUserIdParam
	public void deleteCustomerPaymentOption(@ApiParam(value = "Customer Card Id.", required = true)
	@PathVariable
	final String customerCardId) throws NoSuchMessageException, PaymentException
	{
		if (!isEnableManageSavedCards())
		{
			throw new PaymentException(
					getMessageSource().getMessage("manage.saved.cards.is.not.enabled", null, getI18NService().getCurrentLocale()),
					PaymentExceptionType.BAD_REQUEST);
		}
		try
		{
			paymentContext.deleteCustomerPaymentOptionByCurrentStoreAndCustomer(customerCardId);
		}
		catch (final PaymentException e)
		{
			LOG.error("UsersController -> deleteCustomerPaymentOption:PaymentException Type[" + e.getType() + "] , Message["
					+ e.getMessage() + "]");
			String message = e.getMessage();

			switch (e.getType())
			{
				case FAILURE_RESULT:
					message = getMessageSource().getMessage("manage.saved.cards.failure.result.delete.error", new Object[]
					{ customerCardId }, getI18NService().getCurrentLocale());
					break;

				case INVALID_RESPONSE_SYNTAX_EXPECTED_JSON:
					message = getMessageSource().getMessage("manage.saved.cards.invalid.response.delete.error", new Object[]
					{ customerCardId }, getI18NService().getCurrentLocale());
					break;

				case BAD_REQUEST:
					message = getMessageSource().getMessage("manage.saved.cards.bad.request.delete.error", new Object[]
					{ customerCardId }, getI18NService().getCurrentLocale());
					break;

				default:
					message = getMessageSource().getMessage("manage.saved.cards.bad.request.delete.error", new Object[]
					{ customerCardId }, getI18NService().getCurrentLocale());
					break;
			}
			throw new PaymentException(message, e.getType());
		}
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{userId}/loyaltypoint", method = RequestMethod.GET)
	@ApiOperation(nickname = "getLoyaltyPoint", value = "Get Loyalty Point of a customer.", notes = "Returns all customer Loyalty Points.")
	@ApiBaseSiteIdAndUserIdParam
	@ResponseBody
	public double getLoyaltyPoint(@ApiParam(value = "User identifier.", required = true)
	@PathVariable
	final String userId,
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields)
	{

		final double loyaltyPointByCurrentUser = getLoyaltyPointFacade().getLoyaltyPointsByCurrentCustomer();

		return getDataMapper().map(loyaltyPointByCurrentUser, Double.class, fields);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{userId}/loyaltypoint/history", method = RequestMethod.GET)
	@ApiOperation(nickname = "getLoyaltyPointHistory", value = "Get Loyalty Point history  of a customer.", notes = "Returns all customer Loyalty Point history.")
	@ApiBaseSiteIdAndUserIdParam
	@ResponseBody
	public LoyaltyPointHistoriesWsDTO getLoyaltyPointHistory(@ApiParam(value = "User identifier.", required = true)
	@PathVariable
	final String userId,
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields)
	{

		final List<LoyaltyPointHistoryData> loyaltyPointHistoryByCurrentUser = getLoyaltyPointFacade()
				.getLoyaltyPointHistoryByCurrentCustomer();

		final LoyaltyPointHistoriesData loyaltyPointHistoriesData = new LoyaltyPointHistoriesData();

		if (!loyaltyPointHistoryByCurrentUser.isEmpty())
		{
			loyaltyPointHistoriesData.setLoyaltyPointHistroy(loyaltyPointHistoryByCurrentUser);

		}

		return getDataMapper().map(loyaltyPointHistoriesData, LoyaltyPointHistoriesWsDTO.class, fields);
	}

	protected boolean isEnableManageSavedCards()
	{
		final CMSSiteModel currentSite = getCmsSiteService().getCurrentSite();

		return currentSite == null ? false : currentSite.isEnableManageSavedCards();
	}


	/**
	 * @return the loyaltyPointFacade
	 */
	protected LoyaltyPointFacade getLoyaltyPointFacade()
	{
		return loyaltyPointFacade;
	}
}
