/**
 *
 */
package com.aldahra.aldahracommercewebservices.core.v2.filter;

import java.io.IOException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.filter.OncePerRequestFilter;


/**
 * @author monzer
 *
 */
public class RequestDescriptorFilter extends OncePerRequestFilter
{

	private static final String LINE_SEPERATOR = "-----\n";
	private static final String QUERY_PARAMETERS = "Query Parameters:";
	private static final String REQUEST_METHOD = "Request Method:";
	private static final String REQUEST_PARAMETERS = "Request Parameters";
	private static final String HEADERS = "Headers";
	private static final String REQUEST_URL = "requestURL:";
	private static final String COLON = ":";
	private static final String NEW_LINE = "\n";
	private static final Logger LOG = LoggerFactory.getLogger(RequestDescriptorFilter.class);

	@Override
	protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
			final FilterChain filterChain) throws ServletException, IOException
	{
		final StringBuilder builder = new StringBuilder();
		builder.append(REQUEST_URL + request.getRequestURL().toString()).append(NEW_LINE).append(LINE_SEPERATOR);

		builder.append(REQUEST_METHOD).append(request.getMethod()).append(NEW_LINE).append(LINE_SEPERATOR);

		getHeaderAttributes(request, builder);

		builder.append(QUERY_PARAMETERS).append(request.getQueryString()).append(NEW_LINE).append(LINE_SEPERATOR);

		getRequestParameters(request, builder);

		LOG.info("RequestDescriptorFilter: {}", builder);
		filterChain.doFilter(request, response);
	}

	private void getRequestParameters(final HttpServletRequest request, final StringBuilder builder)
	{
		builder.append(REQUEST_PARAMETERS).append(COLON).append(NEW_LINE);
		final Map<String, String[]> parameterMap = request.getParameterMap();
		parameterMap.keySet().stream().forEach(key -> {
			final String param = Arrays.asList(parameterMap.get(key)).stream().collect(Collectors.joining(" "));
			builder.append(key).append(COLON).append(param);
		});
		builder.append(NEW_LINE).append(LINE_SEPERATOR);
	}

	private void getHeaderAttributes(final HttpServletRequest request, final StringBuilder builder)
	{
		builder.append(HEADERS).append(COLON).append(NEW_LINE);
		final Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements())
		{
			final String headerName = headerNames.nextElement();
			builder.append(headerName);
			builder.append(COLON);

			final Enumeration<String> headers = request.getHeaders(headerName);
			while (headers.hasMoreElements())
			{
				final String headerValue = headers.nextElement();
				builder.append(headerValue);
			}
			builder.append(NEW_LINE);
		}
		builder.append(LINE_SEPERATOR);
	}

}
