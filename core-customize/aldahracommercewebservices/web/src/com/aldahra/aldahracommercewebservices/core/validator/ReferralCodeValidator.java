/**
 *
 */
package com.aldahra.aldahracommercewebservices.core.validator;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.aldahra.aldahrareferralcode.model.ReferralCodeModel;
import com.aldahra.aldahrareferralcode.service.ReferralCodeService;


/**
 * @author mohammedbaker
 *
 */
public class ReferralCodeValidator implements Validator
{
	private static final String INVALID_REFERRAL_CODE_MESSAGE_ID = "field.invalidReferralCode";

	private String referralCode;

	@Resource(name = "referralCodeService")
	private ReferralCodeService referralCodeService;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return true;
	}

	@Override
	public void validate(final Object o, final Errors errors)
	{
		Assert.notNull(errors, "Errors object must not be null");
		final String referralCode = (String) errors.getFieldValue(this.referralCode);

		if (!StringUtils.isEmpty(referralCode))
		{

			final Optional<ReferralCodeModel> code = referralCodeService.getByCode(referralCode);
			if (code.isEmpty())
			{
				errors.rejectValue(this.referralCode, INVALID_REFERRAL_CODE_MESSAGE_ID, new String[]
				{ this.referralCode }, "This field is not a valid referral code.");
			}
		}

	}

	/**
	 * @return the referralCode
	 */
	public String getReferralCode()
	{
		return referralCode;
	}

	/**
	 * @param referralCode
	 *           the referralCode to set
	 */
	@Required
	public void setReferralCode(final String referralCode)
	{
		this.referralCode = referralCode;
	}

}
