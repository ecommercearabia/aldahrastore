/**
 *
 */
package com.aldahra.aldahracommercewebservices.core.oauth2.token;

import java.util.Collection;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

/**
 * @author monzer
 *
 */
public class SignatureIdAuthenticationOCCToken extends AbstractAuthenticationToken
{

	Object email;
	Object customerSigature;

	public SignatureIdAuthenticationOCCToken(final Object email, final Object customerSignature)
	{
		super(null);
		this.email = email;
		this.customerSigature = customerSignature;
	}

	public SignatureIdAuthenticationOCCToken(final Object email, final Object customerSigature,
			final Collection<? extends GrantedAuthority> authorities)
	{
		
		super(authorities);
		this.email = email;
		this.customerSigature = customerSigature;
		super.setAuthenticated(true);
	}

	@Override
	public Object getCredentials()
	{
		return customerSigature;
	}

	@Override
	public Object getPrincipal()
	{
		return email;
	}

}
