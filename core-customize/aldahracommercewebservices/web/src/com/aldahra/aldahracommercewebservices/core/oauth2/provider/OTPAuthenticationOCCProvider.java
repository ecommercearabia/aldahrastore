/**
 *
 */
package com.aldahra.aldahracommercewebservices.core.oauth2.provider;

import de.hybris.platform.core.Registry;
import de.hybris.platform.jalo.JaloConnection;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.jalo.user.UserManager;
import de.hybris.platform.spring.security.CoreAuthenticationProvider;
import de.hybris.platform.spring.security.CoreUserDetails;

import java.util.Collections;
import java.util.Optional;

import javax.annotation.Resource;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsChecker;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.aldahra.aldahracommercewebservices.core.oauth2.token.OTPAuthenticationOCCToken;
import com.aldahra.core.service.CustomerMatchingService;


/**
 * @author monzer
 *
 */
public class OTPAuthenticationOCCProvider extends CoreAuthenticationProvider
{
	private final UserDetailsChecker postAuthenticationChecks = new CoreAuthenticationProvider().getPreAuthenticationChecks();

	@Resource(name = "customerMatchingService")
	private CustomerMatchingService customerMatchingService;

	@Override
	public Authentication authenticate(final Authentication authentication) throws AuthenticationException
	{
		if (Registry.hasCurrentTenant() && JaloConnection.getInstance().isSystemInitialized())
		{

			final String username = authentication.getPrincipal() == null ? "NONE_PROVIDED" : authentication.getName();

			final Optional<String> identifier = customerMatchingService.getCustomerUidAfterMatching(username);

			if (identifier.isEmpty())
			{
				throw new BadCredentialsException(
						messages.getMessage("CoreAuthenticationProvider.badCredentials", "Bad credentials"));
			}

			UserDetails userDetails = null;
			try
			{
				userDetails = this.retrieveUser(identifier.get());
			}
			catch (final UsernameNotFoundException ex)
			{
				throw new BadCredentialsException(
						this.messages.getMessage("CoreAuthenticationProvider.badCredentials", "Bad credentials"), ex);
			}

			this.getPreAuthenticationChecks().check(userDetails);
			final User user = UserManager.getInstance().getUserByLogin(userDetails.getUsername());

			this.additionalAuthenticationChecks(userDetails, (AbstractAuthenticationToken) authentication);
			this.postAuthenticationChecks.check(userDetails);

			JaloSession.getCurrentSession().setUser(user);
			return this.createSuccessAuthentication(authentication, userDetails);
		}
		else
		{
			return super.createSuccessAuthentication(authentication, new CoreUserDetails("systemNotInitialized",
					"systemNotInitialized", true, false, true, true, Collections.EMPTY_LIST, (String) null));
		}
	}

	@Override
	public boolean supports(final Class authentication)
	{
		return OTPAuthenticationOCCToken.class.isAssignableFrom(authentication);
	}


}
