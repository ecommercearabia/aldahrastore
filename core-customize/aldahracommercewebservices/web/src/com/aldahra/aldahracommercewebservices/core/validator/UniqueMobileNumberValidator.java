package com.aldahra.aldahracommercewebservices.core.validator;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.aldahra.aldahrauser.service.MobilePhoneService;
import com.aldahra.aldahrauserfacades.customer.facade.CustomCustomerFacade;


/**
 * @author monzer
 *
 */
public class UniqueMobileNumberValidator implements Validator
{
	private static final String INVALID_EMAIL_MESSAGE_ID = "field.invalidEmail";
	private static final String INVALID_MOBILENUMBER_MESSAGE_ID = "field.invalidMobileNumber";
	private static final String INVALID_MOBILECOUNTRYCODE_ID = " field.invalidMobileCountryCode";
	private static final String INVALID_UNIQUE_MOBILE_NUMBER = "field.invalidUniqueMobileNumber";

	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;

	@Resource(name = "wsCustomerFacade")
	private CustomCustomerFacade customerFacade;

	private String mobileCountryCode;
	private String mobileNumber;

	public boolean supports(final Class<?> aClass)
	{
		return true;
	}

	public void validate(final Object o, final Errors errors)
	{
		Assert.notNull(errors, "Errors object must not be null");
		final String mobileNumber = (String) errors.getFieldValue(this.mobileNumber);
		final String mobileCountryCode = (String) errors.getFieldValue(this.mobileCountryCode);

		if (StringUtils.isEmpty(mobileCountryCode))
		{
			errors.rejectValue(this.mobileCountryCode, INVALID_MOBILECOUNTRYCODE_ID, new String[]
			{ this.mobileCountryCode }, "This field is not a valid mobile country code.");

		}
		if (StringUtils.isEmpty(mobileNumber))
		{
			errors.rejectValue(this.mobileNumber, INVALID_MOBILENUMBER_MESSAGE_ID, new String[]
			{ this.mobileNumber }, "This field is not a valid mobile number.");
		}
		else if (!StringUtils.isEmpty(mobileCountryCode))
		{
			final Optional<String> normalizedPhoneNumber = mobilePhoneService
					.validateAndNormalizePhoneNumberByIsoCode(mobileCountryCode, mobileNumber);

			if (normalizedPhoneNumber.isEmpty())
			{
				errors.rejectValue(this.mobileCountryCode, INVALID_MOBILECOUNTRYCODE_ID, new String[]
				{ this.mobileCountryCode }, "This field is not a valid mobile country code.");
				errors.rejectValue(this.mobileNumber, INVALID_MOBILENUMBER_MESSAGE_ID, new String[]
				{ this.mobileNumber }, "This field is not a valid mobile number.");
			}
			if (normalizedPhoneNumber.isPresent())
			{
				if (!customerFacade.isValidMobileNumber(normalizedPhoneNumber.get()))
				{
					errors.rejectValue(this.mobileNumber, INVALID_UNIQUE_MOBILE_NUMBER, new String[]
					{ this.mobileNumber }, "This monile number is used by  another customer..");
				}
			}
		}

	}

	/**
	 * @return the mobileCountryCode
	 */
	public String getMobileCountryCode()
	{
		return mobileCountryCode;
	}

	/**
	 * @param mobileCountryCode
	 *           the mobileCountryCode to set
	 */
	@Required
	public void setMobileCountryCode(final String mobileCountryCode)
	{
		this.mobileCountryCode = mobileCountryCode;
	}

	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber()
	{
		return mobileNumber;
	}

	/**
	 * @param mobileNumber
	 *           the mobileNumber to set
	 */
	@Required
	public void setMobileNumber(final String mobileNumber)
	{
		this.mobileNumber = mobileNumber;
	}

}
