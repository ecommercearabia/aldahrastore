/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.aldahra.aldahracommercewebservices.core.v2.filter;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commercewebservicescommons.strategies.CartLoaderStrategy;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Required;

import com.aldahra.aldahracommercewebservices.core.context.ContextInformationLoader;
import com.aldahra.core.service.CartHistoryService;


/**
 * Filter that puts cart from the requested url into the session.
 */
public class CartMatchingFilter extends AbstractUrlMatchingFilter
{
	public static final String REFRESH_CART_PARAM = "refreshCart";
	private static final String SALES_APPLICATION_ATTRIBUTE = "salesApp";

	private String regexp;
	private CartLoaderStrategy cartLoaderStrategy;
	private boolean cartRefreshedByDefault = true;

	@Resource(name = "wsContextInformationLoaderV2")
	private ContextInformationLoader contextInformationLoader;

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "cartHistoryService")
	private CartHistoryService cartHistoryService;

	@Override
	protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
			final FilterChain filterChain) throws ServletException, IOException
	{
		if (matchesUrl(request, regexp))
		{
			final String cartId = getValue(request, regexp);
			cartLoaderStrategy.loadCart(cartId, shouldCartBeRefreshed(request));
			if (cartService.hasSessionCart())
			{
				final SalesApplication sourceApplication = contextInformationLoader.setSalesApplicationFromRequest(request);
				final CartModel cartModel = cartService.getSessionCart();
				cartHistoryService.updateCartHistory(cartModel, sourceApplication, getPath(request));
			}
		}

		filterChain.doFilter(request, response);
	}

	protected boolean shouldCartBeRefreshed(final HttpServletRequest request)
	{
		final String refreshParam = request.getParameter(REFRESH_CART_PARAM);
		return refreshParam == null ? isCartRefreshedByDefault() : Boolean.parseBoolean(refreshParam);
	}

	protected String getRegexp()
	{
		return regexp;
	}

	@Required
	public void setRegexp(final String regexp)
	{
		this.regexp = regexp;
	}

	public CartLoaderStrategy getCartLoaderStrategy()
	{
		return cartLoaderStrategy;
	}

	@Required
	public void setCartLoaderStrategy(final CartLoaderStrategy cartLoaderStrategy)
	{
		this.cartLoaderStrategy = cartLoaderStrategy;
	}

	public boolean isCartRefreshedByDefault()
	{
		return cartRefreshedByDefault;
	}

	public void setCartRefreshedByDefault(final boolean cartRefreshedByDefault)
	{
		this.cartRefreshedByDefault = cartRefreshedByDefault;
	}

}