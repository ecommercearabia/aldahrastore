/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.aldahra.aldahracommercewebservices.core.validator;

import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * Default commerce web services point of service validator. Checks if point of service with given name exist.
 */
public class ForgottenPasswordValidator implements Validator
{
	private static final Logger LOG = Logger.getLogger(ForgottenPasswordValidator.class);

	@Resource(name = "userService")
	private UserService userService;

	@Override
	public boolean supports(final Class<?> clazz)
	{
		return String.class.equals(clazz);
	}

	@Override
	public void validate(final Object target, final Errors errors)
	{
		final String userId = (String) target;

		try
		{
			userService.getUserForUID(userId);
		}
		catch (final UnknownIdentifierException unknownIdentifierException)
		{
			LOG.warn("userId: " + userId + " does not exist in the database.");
			errors.reject("forgotten.password.email.does.not.exist.invalid");
		}

	}



}
