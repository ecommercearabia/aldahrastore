package com.aldahra.aldahracommercewebservices.core.validator;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.aldahra.aldahrauser.model.NationalityModel;
import com.aldahra.aldahrauser.service.NationalityService;


public class NationalityValidator implements Validator
{

	private static final String INVALID_NATIONALITY_MESSAGE_ID = "field.register.nationality.invalid";
	private static final String INVALID_NATIONALITY_ID_MESSAGE_ID = "field.register.nationalityId.invalid";

	private static final String EMPTY_CURRENT_SITE_ERROR_MSG = "current site is null";

	private static final String NATIONALITY_KEY = "nationality";
	private static final String NATIONALITY_ID_KEY = "nationalityId";


	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "nationalityService")
	private NationalityService nationalityService;

	private String nationality;

	private String nationalityId;

	private String hasNationalId;


	@Override
	public boolean supports(final Class<?> aClass)
	{
		return true;
	}

	@Override
	public void validate(final Object o, final Errors errors)
	{
		Assert.notNull(errors, "Errors object must not be null");
		final String id = (String) errors.getFieldValue(this.nationalityId);
		final String national = (String) errors.getFieldValue(this.nationality);
		final boolean hasId = Boolean.valueOf(String.valueOf(errors.getFieldValue(this.hasNationalId)));


		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();

		validateNationality(errors, currentSite, national);
		validateNationalityID(errors, currentSite, id, hasId);

	}

	private void validateNationality(final Errors errors, final CMSSiteModel currentSite, final String nationality)
	{
		if (currentSite == null)
		{
			throw new IllegalArgumentException(EMPTY_CURRENT_SITE_ERROR_MSG);
		}

		if (!currentSite.isNationalityCustomerEnabled() || !currentSite.isNationalityCustomerRequired())
		{
			return;
		}

		if (StringUtils.isEmpty(nationality))
		{
			errors.rejectValue(NATIONALITY_KEY, INVALID_NATIONALITY_MESSAGE_ID);
			return;
		}

		final Optional<NationalityModel> national = nationalityService.get(nationality);
		if (!national.isPresent())
		{
			errors.rejectValue(NATIONALITY_KEY, INVALID_NATIONALITY_MESSAGE_ID);
		}

	}

	private void validateNationalityID(final Errors errors, final CMSSiteModel currentSite, final String nationalityId,
			final boolean hasId)
	{
		if (currentSite == null)
		{
			throw new IllegalArgumentException(EMPTY_CURRENT_SITE_ERROR_MSG);
		}

		if (!currentSite.isNationalityIdCustomerEnabled() || !currentSite.isNationalityIdCustomerRequired())
		{
			return;
		}

		if (hasId && StringUtils.isEmpty(nationalityId))
		{
			errors.rejectValue(NATIONALITY_ID_KEY, INVALID_NATIONALITY_ID_MESSAGE_ID);
		}

	}

	/**
	 * @return the nationality
	 */
	public String getNationality()
	{
		return nationality;
	}

	/**
	 * @param nationality
	 *           the nationality to set
	 */
	public void setNationality(final String nationality)
	{
		this.nationality = nationality;
	}

	/**
	 * @return the nationalityId
	 */
	public String getNationalityId()
	{
		return nationalityId;
	}

	/**
	 * @param nationalityId
	 *           the nationalityId to set
	 */
	public void setNationalityId(final String nationalityId)
	{
		this.nationalityId = nationalityId;
	}

	/**
	 * @return the hasNationalId
	 */
	public String getHasNationalId()
	{
		return hasNationalId;
	}

	/**
	 * @param hasNationalId
	 *           the hasNationalId to set
	 */
	public void setHasNationalId(final String hasNationalId)
	{
		this.hasNationalId = hasNationalId;
	}


}

