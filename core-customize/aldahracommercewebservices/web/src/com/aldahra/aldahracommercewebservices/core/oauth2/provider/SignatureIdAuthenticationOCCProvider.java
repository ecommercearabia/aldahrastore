/**
 *
 */
package com.aldahra.aldahracommercewebservices.core.oauth2.provider;

import de.hybris.platform.core.Registry;
import de.hybris.platform.core.Tenant;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.JaloConnection;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.jalo.user.UserManager;
import de.hybris.platform.persistence.security.EJBPasswordEncoderNotFoundException;
import de.hybris.platform.persistence.security.PBKDF2WithHmacSHA1SaltedPasswordEncoder;
import de.hybris.platform.persistence.security.PasswordEncoder;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.spring.security.CoreAuthenticationProvider;
import de.hybris.platform.spring.security.CoreUserDetails;

import java.util.Collections;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsChecker;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.aldahra.aldahracommercewebservices.core.oauth2.token.SignatureIdAuthenticationOCCToken;


/**
 * @author monzer
 *
 */
public class SignatureIdAuthenticationOCCProvider extends CoreAuthenticationProvider
{
	private final UserDetailsChecker postAuthenticationChecks = new CoreAuthenticationProvider().getPreAuthenticationChecks();

	@Resource(name = "userService")
	private UserService userService;

	@Override
	public Authentication authenticate(final Authentication authentication) throws AuthenticationException
	{
		if (!Registry.hasCurrentTenant() || !JaloConnection.getInstance().isSystemInitialized())
		{
			return super.createSuccessAuthentication(authentication, new CoreUserDetails("systemNotInitialized",
					"systemNotInitialized", true, false, true, true, Collections.EMPTY_LIST, (String) null));
		}

		final String username = authentication.getPrincipal() == null ? "NONE_PROVIDED" : authentication.getName();

		UserDetails userDetails = null;
		try
		{
			userDetails = this.retrieveUser(username);
		}
		catch (final UsernameNotFoundException ex)
		{
			throw new BadCredentialsException(
					this.messages.getMessage("CoreAuthenticationProvider.badCredentials", "Bad credentials"), ex);
		}

		this.getPreAuthenticationChecks().check(userDetails);
		final User user = UserManager.getInstance().getUserByLogin(userDetails.getUsername());

		this.additionalAuthenticationChecks(userDetails, (AbstractAuthenticationToken) authentication);
		this.postAuthenticationChecks.check(userDetails);

		JaloSession.getCurrentSession().setUser(user);
		return this.createSuccessAuthentication(authentication, userDetails);

	}



	@Override
	protected void additionalAuthenticationChecks(final UserDetails details, final AbstractAuthenticationToken authentication)
			throws AuthenticationException
	{
		if (details == null || StringUtils.isBlank(details.getUsername()))
		{
			throw new UsernameNotFoundException("Username not found for additional authentication checks");
		}

		UserModel user = null;
		try
		{
			user = userService.getUserForUID(details.getUsername());
		}
		catch (final UnknownIdentifierException e)
		{
			throw new UsernameNotFoundException(e.getMessage());
		}

		if (user == null)
		{
			throw new UsernameNotFoundException("Username does not exist for additional authentication checks");
		}

		if (!(user instanceof CustomerModel))
		{
			throw new BadCredentialsException(
					this.messages.getMessage("CoreAuthenticationProvider.badCredentials", "Bad credentials"));
		}

		final CustomerModel customer = (CustomerModel) user;
		boolean checkSignature;
		try
		{
			checkSignature = checkSignature(customer, String.valueOf(authentication.getCredentials()));
		}
		catch (final EJBPasswordEncoderNotFoundException e)
		{
			throw new BadCredentialsException(
					this.messages.getMessage("CoreAuthenticationProvider.badCredentials", "Bad credentials"));
		}
		if (StringUtils.isBlank(customer.getSignatureId()) || !checkSignature)
		{
			throw new BadCredentialsException(
					this.messages.getMessage("CoreAuthenticationProvider.badCredentials", "Bad credentials"));
		}
	}

	private boolean checkSignature(final CustomerModel customer, final String signature) throws EJBPasswordEncoderNotFoundException
	{
		final PasswordEncoder enc = Registry.<Tenant> getCurrentTenant().getJaloConnection()
				.getPasswordEncoder(customer.getPasswordEncoding());

		
		if (enc instanceof PBKDF2WithHmacSHA1SaltedPasswordEncoder)
		{
			return enc.check(customer.getUid(), customer.getSignatureId(), signature);
		}
		else
		{
			final String hashedPassword = enc.encode(signature, customer.getPasswordEncoding());
			return customer.getSignatureId().equals(hashedPassword);
		}
	}

	@Override
	public boolean supports(final Class authentication)
	{
		return SignatureIdAuthenticationOCCToken.class.isAssignableFrom(authentication);
	}

}
