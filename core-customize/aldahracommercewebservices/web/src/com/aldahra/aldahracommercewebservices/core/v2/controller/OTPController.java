package com.aldahra.aldahracommercewebservices.core.v2.controller;

import de.hybris.platform.commercewebservicescommons.dto.otp.OTPResponseWsDTO;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.webservicescommons.cache.CacheControl;
import de.hybris.platform.webservicescommons.cache.CacheControlDirective;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdParam;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aldahra.aldahraotp.context.OTPContext;
import com.aldahra.aldahraotp.exception.OTPException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;


/**
 * @author abu-muhasien
 *
 */
@Controller
@RequestMapping(value = "/{baseSiteId}/otp")
@CacheControl(directive = CacheControlDirective.PUBLIC, maxAge = 360)
@Api(tags = "OTP")

public class OTPController extends BaseController
{
	@Resource(name = "otpContext")
	private OTPContext otpContext;
	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	@RequestMapping(value = "/send", method = RequestMethod.POST)
	@ResponseBody
	@ApiOperation(nickname = "sendOTPCode", value = "send OTP Code", notes = "Send a Otp code to specific mobile number and return true if the message has been sent successfully and return false if the message did not send.")
	@ApiBaseSiteIdParam
	public OTPResponseWsDTO sendOTPCode(@ApiParam
	@RequestParam(required = true)
	final String countryisoCode, @ApiParam
	@RequestParam(required = true)
	final String mobileNumber)

	{
		OTPResponseWsDTO otpResponseWsDTO = null;
		try
		{
			otpResponseWsDTO = new OTPResponseWsDTO();
			otpContext.sendOTPCodeByCurrentSite(countryisoCode, mobileNumber);
			otpResponseWsDTO.setStatus(Boolean.TRUE);
			otpResponseWsDTO.setDescription("The message has been sent successfully");
			return otpResponseWsDTO;
		}
		catch (final OTPException e)
		{
			otpResponseWsDTO.setStatus(Boolean.FALSE);
			otpResponseWsDTO.setDescription(e.getMessage());
			return otpResponseWsDTO;
		}

	}

	@RequestMapping(value = "/verify", method = RequestMethod.POST)
	@ResponseBody
	@ApiOperation(nickname = "verifyCode", value = "verify Code.", notes = "specifying Otp Code ,return true if the message has been sent successfully and return false if the message did not send.")
	@ApiBaseSiteIdParam
	public OTPResponseWsDTO verifyCode(@ApiParam
	@RequestParam(required = true)
	final String countryisoCode, @ApiParam
	@RequestParam(required = true)
	final String mobileNumber, @ApiParam
	@RequestParam(required = true)
	final String code)
	{
		OTPResponseWsDTO otpResponseWsDTO = null;
		try
		{
			otpResponseWsDTO = new OTPResponseWsDTO();
			otpContext.verifyCodeByCurrentSite(countryisoCode, mobileNumber, code);
			otpResponseWsDTO.setStatus(Boolean.TRUE);
			otpResponseWsDTO.setDescription("the number has been verified successfully");
			return otpResponseWsDTO;
		}
		catch (final OTPException e)
		{
			otpResponseWsDTO.setStatus(Boolean.FALSE);
			otpResponseWsDTO.setDescription(e.getMessage());
			return otpResponseWsDTO;
		}

	}
}
