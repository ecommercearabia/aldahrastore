/**
 *
 */
package com.aldahra.aldahracommercewebservices.core.v2.controller;

import de.hybris.platform.webservicescommons.cache.CacheControl;
import de.hybris.platform.webservicescommons.cache.CacheControlDirective;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdParam;

import java.security.Principal;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.InvalidClientException;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.common.exceptions.InvalidRequestException;
import org.springframework.security.oauth2.common.exceptions.UnsupportedGrantTypeException;
import org.springframework.security.oauth2.common.util.OAuth2Utils;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2RequestFactory;
import org.springframework.security.oauth2.provider.OAuth2RequestValidator;
import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestValidator;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aldahra.aldahracommercewebservices.core.exceptions.CustomerSignatureException;
import com.aldahra.aldahracommercewebservices.core.oauth2.SignatureIdTokenGranter;
import com.aldahra.aldahracommercewebservices.core.service.UserSignatureAuthenticationService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;


/**
 * @author monzer
 *
 */
@Controller
@RequestMapping(value = "/{baseSiteId}/signature")
@CacheControl(directive = CacheControlDirective.PUBLIC, maxAge = 360)
@Api(tags = "Customer Signature Authentication")
public class SignatureIdAuthenticationController extends BaseController
{
	private static final Logger LOG = LoggerFactory.getLogger(SignatureIdAuthenticationController.class);

	@Resource(name = "userSignatureAuthenticationService")
	private UserSignatureAuthenticationService userSignatureAuthenticationService;

	@Resource(name = "signatureTokenGranter")
	private SignatureIdTokenGranter signatureTokenGranter;

	@Autowired
	@Qualifier(value = "openIDClientDetails")
	private ClientDetailsService clientDetailsService;

	@Resource(name = "authenticationManager")
	private AuthenticationManager authenticationManager;

	@Autowired
	private OAuth2RequestFactory oAuth2RequestFactory;

	private final OAuth2RequestValidator oAuth2RequestValidator = new DefaultOAuth2RequestValidator();

	@RequestMapping(value = "/auth", method = RequestMethod.POST)
	@ResponseBody
	@ApiOperation(nickname = "authenticateWithCustomerSignature", value = "authenticate With Customer Signature.", notes = "Authenticate a customer using his device signature")
	@ApiBaseSiteIdParam
	public ResponseEntity<Map<String, Object>> authenticateByCustomerSignature(@ApiParam
	@RequestParam(required = true)
	final String email, @ApiParam
	@RequestParam(required = true)
	final String signatureId, final Principal principal) throws CustomerSignatureException
	{
		final boolean canCustomerLoginWithSignature = userSignatureAuthenticationService.canCustomerLoginWithSignature(email,
				signatureId);
		if (!canCustomerLoginWithSignature)
		{
			throw new CustomerSignatureException(
					"Signature mismatch or user with uid of " + email + " is not found");
		}
		final Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("email", email);
		parameters.put("signatureId", signatureId);
		parameters.put("grant_type", "signature");

		if (!(principal instanceof Authentication))
		{
			throw new InsufficientAuthenticationException(
					"There is no client authentication. Try adding an appropriate authentication filter.");
		}

		final String clientId = getClientId(principal);
		final ClientDetails authenticatedClient = clientDetailsService.loadClientByClientId(clientId);

		final TokenRequest tokenRequest = oAuth2RequestFactory.createTokenRequest(parameters, authenticatedClient);

		if (clientId != null && !clientId.equals(""))
		{
			// Only validate the client details if a client authenticated during this
			// request.
			if (!clientId.equals(tokenRequest.getClientId()))
			{
				// double check to make sure that the client ID in the token request is the same as that in the
				// authenticated client
				throw new InvalidClientException("Given client ID does not match authenticated client");
			}
		}
		if (authenticatedClient != null)
		{
			oAuth2RequestValidator.validateScope(tokenRequest, authenticatedClient);
		}
		if (!StringUtils.hasText(tokenRequest.getGrantType()))
		{
			throw new InvalidRequestException("Missing grant type");
		}
		if (tokenRequest.getGrantType().equals("implicit"))
		{
			throw new InvalidGrantException("Implicit grant type not supported from token endpoint");
		}

		if (isAuthCodeRequest(parameters))
		{
			// The scope was requested or determined during the authorization step
			if (!tokenRequest.getScope().isEmpty())
			{
				LOG.debug("Clearing scope of incoming token request");
				tokenRequest.setScope(Collections.<String> emptySet());
			}
		}

		if (isRefreshTokenRequest(parameters))
		{
			// A refresh token has its own default scopes, so we should ignore any added by the factory here.
			tokenRequest.setScope(OAuth2Utils.parseParameterList(parameters.get(OAuth2Utils.SCOPE)));
		}

		final OAuth2AccessToken token = signatureTokenGranter.grant(tokenRequest.getGrantType(), tokenRequest);
		if (token == null)
		{
			throw new UnsupportedGrantTypeException("Unsupported grant type: " + tokenRequest.getGrantType());
		}

		return getResponse(token);

	}

	protected String getClientId(final Principal principal)
	{
		final Authentication client = (Authentication) principal;
		if (!client.isAuthenticated())
		{
			throw new InsufficientAuthenticationException("The client is not authenticated.");
		}
		String clientId = client.getName();
		if (client instanceof OAuth2Authentication)
		{
			// Might be a client and user combined authentication
			clientId = ((OAuth2Authentication) client).getOAuth2Request().getClientId();
		}
		return clientId;
	}

	private ResponseEntity<Map<String, Object>> getResponse(final OAuth2AccessToken accessToken)
	{
		final HttpHeaders headers = new HttpHeaders();
		headers.set("Cache-Control", "no-store");
		headers.set("Pragma", "no-cache");
		headers.set("Content-Type", "application/json;charset=UTF-8");
		headers.set("signature-authentication", "The user has been authenticated successfully using signature code");
		final Map<String, Object> tokenBody = new HashMap<String, Object>();
		tokenBody.put("access_token", accessToken.getValue());
		tokenBody.put("token_type", accessToken.getTokenType());
		tokenBody.put("refresh_token", accessToken.getRefreshToken().getValue());
		tokenBody.put("expires_in", accessToken.getExpiresIn());
		tokenBody.put("scope", accessToken.getScope().stream().collect(Collectors.joining(", ")));

		
		return new ResponseEntity<Map<String, Object>>(tokenBody, headers, HttpStatus.OK);
	}

	private boolean isRefreshTokenRequest(final Map<String, String> parameters)
	{
		return "refresh_token".equals(parameters.get("grant_type")) && parameters.get("refresh_token") != null;
	}

	private boolean isAuthCodeRequest(final Map<String, String> parameters)
	{
		return "authorization_code".equals(parameters.get("grant_type")) && parameters.get("code") != null;
	}
}
