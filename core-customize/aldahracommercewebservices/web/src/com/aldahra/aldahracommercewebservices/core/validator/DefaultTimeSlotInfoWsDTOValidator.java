package com.aldahra.aldahracommercewebservices.core.validator;

import de.hybris.platform.commercewebservicescommons.dto.order.PaymentDetailsWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.TimeSlotInfoWsDTO;

import java.time.LocalTime;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * Validates instances of {@link PaymentDetailsWsDTO}.
 *
 */
public class DefaultTimeSlotInfoWsDTOValidator implements Validator
{
	@Override
	public boolean supports(final Class clazz)
	{
		return TimeSlotInfoWsDTO.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(final Object target, final Errors errors)
	{
		final TimeSlotInfoWsDTO timeSlotInfoWsDTO = (TimeSlotInfoWsDTO) target;

		final String periodCode = timeSlotInfoWsDTO.getPeriodCode();
		final LocalTime start = timeSlotInfoWsDTO.getStart();
		final LocalTime end = timeSlotInfoWsDTO.getEnd();
		final String day = timeSlotInfoWsDTO.getDay();
		final String date = timeSlotInfoWsDTO.getDate();

		if (StringUtils.isBlank(periodCode))
		{
			errors.rejectValue("periodCode", "timeslot.period.value.error");
		}
		if (start == null)
		{
			errors.rejectValue("start", "timeslot.start.value.error");
		}
		if (end == null)
		{
			errors.rejectValue("end", "timeslot.end.value.error");
		}
		if (StringUtils.isBlank(day))
		{
			errors.rejectValue("day", "timeslot.day.value.error");
		}
		if (StringUtils.isBlank(date))
		{
			errors.rejectValue("date", "timeslot.date.value.error");
		}
	}
}
