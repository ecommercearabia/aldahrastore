/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.aldahra.aldahracommercewebservices.core.v2.controller;

import de.hybris.platform.acceleratorfacades.payment.data.PaymentSubscriptionResultData;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.order.SaveCartFacade;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.CartModificationDataList;
import de.hybris.platform.commercefacades.order.data.DeliveryModesData;
import de.hybris.platform.commercefacades.order.data.NoCardPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.NoCardTypeData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.order.data.PaymentModeData;
import de.hybris.platform.commercefacades.product.data.PromotionResultData;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.commercefacades.promotion.CommercePromotionRestrictionFacade;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.voucher.VoucherFacade;
import de.hybris.platform.commercefacades.voucher.exceptions.VoucherOperationException;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.order.CommerceCartMergingException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.commerceservices.promotion.CommercePromotionRestrictionException;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commercewebservicescommons.dto.order.CartCheckOutValidatorWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.CartListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.CartModificationListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.CartModificationWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.CartWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.DeliveryModeListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.DeliveryModeTypeListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.DeliveryModeWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.OrderEntryListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.OrderEntryWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.PaymentDetailsWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.PaymentModeListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.PaymentModeWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.PaymentRequestWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.StoreCreditDetailsWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.StoreCreditModeListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.TimeSlotInfoWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.TimeSlotWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.product.PromotionResultListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.user.AddressWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.voucher.VoucherListWsDTO;
import de.hybris.platform.commercewebservicescommons.errors.exceptions.CartEntryException;
import de.hybris.platform.commercewebservicescommons.errors.exceptions.CartException;
import de.hybris.platform.commercewebservicescommons.errors.exceptions.LowStockException;
import de.hybris.platform.commercewebservicescommons.errors.exceptions.ProductLowStockException;
import de.hybris.platform.commercewebservicescommons.errors.exceptions.RequestParameterException;
import de.hybris.platform.commercewebservicescommons.errors.exceptions.StockSystemException;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.webservicescommons.cache.CacheControl;
import de.hybris.platform.webservicescommons.cache.CacheControlDirective;
import de.hybris.platform.webservicescommons.errors.exceptions.WebserviceValidationException;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdAndUserIdParam;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdUserIdAndCartIdParam;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.aldahra.aldahracommercewebservices.core.basesite.data.CartCheckOutValidatorData;
import com.aldahra.aldahracommercewebservices.core.basesite.data.PaymentModesData;
import com.aldahra.aldahracommercewebservices.core.basesite.data.StoreCreditModesData;
import com.aldahra.aldahracommercewebservices.core.cart.impl.CommerceWebServicesCartFacade;
import com.aldahra.aldahracommercewebservices.core.exceptions.InvalidPaymentInfoException;
import com.aldahra.aldahracommercewebservices.core.exceptions.NoCheckoutCartException;
import com.aldahra.aldahracommercewebservices.core.exceptions.UnsupportedDeliveryModeException;
import com.aldahra.aldahracommercewebservices.core.exceptions.UnsupportedRequestException;
import com.aldahra.aldahracommercewebservices.core.order.data.CartDataList;
import com.aldahra.aldahracommercewebservices.core.order.data.DeliveryModeTypeList;
import com.aldahra.aldahracommercewebservices.core.order.data.OrderEntryDataList;
import com.aldahra.aldahracommercewebservices.core.payment.dto.PaymentLinkWsDTO;
import com.aldahra.aldahracommercewebservices.core.product.data.PromotionResultDataList;
import com.aldahra.aldahracommercewebservices.core.request.support.impl.PaymentProviderRequestSupportedStrategy;
import com.aldahra.aldahracommercewebservices.core.stock.CommerceStockFacade;
import com.aldahra.aldahracommercewebservices.core.voucher.data.VoucherDataList;
import com.aldahra.aldahraorder.cart.exception.CartValidationException;
import com.aldahra.aldahraorder.cart.exception.enums.CartExceptionType;
import com.aldahra.aldahraorder.cart.service.CartValidationService;
import com.aldahra.aldahraotp.exception.OTPException;
import com.aldahra.aldahrapayment.ccavenue.entry.ApplePayPaymentResponseData;
import com.aldahra.aldahrapayment.ccavenue.entry.ApplePayRequest;
import com.aldahra.aldahrapayment.ccavenue.entry.PaymentLinkResponseData;
import com.aldahra.aldahrapayment.ccavenue.entry.RequestData;
import com.aldahra.aldahrapayment.ccavenue.enums.PaymentExceptionType;
import com.aldahra.aldahrapayment.ccavenue.exception.PaymentException;
import com.aldahra.aldahrapayment.context.PaymentContext;
import com.aldahra.aldahrapayment.entry.PaymentRequestData;
import com.aldahra.aldahrapayment.entry.PaymentResponseData;
import com.aldahra.aldahrastorecreditfacades.data.StoreCreditModeData;
import com.aldahra.aldahratimeslotfacades.TimeSlotData;
import com.aldahra.aldahratimeslotfacades.TimeSlotInfoData;
import com.aldahra.aldahratimeslotfacades.exception.TimeSlotException;
import com.aldahra.aldahratimeslotfacades.exception.type.TimeSlotExceptionType;
import com.aldahra.aldahrauserfacades.user.facade.CustomUserFacade;
import com.aldahra.aldahrawishlistfacade.facade.WishlistFacade;
import com.aldahra.core.cart.data.CartTimeSlotData;
import com.aldahra.core.enums.DeliveryModeType;
import com.aldahra.core.enums.ShipmentType;
import com.aldahra.facades.exception.CartCouponException;
import com.aldahra.facades.exception.enums.CartCouponExceptionType;
import com.aldahra.facades.facade.CustomAcceleratorCheckoutFacade;
import com.aldahra.facades.facade.CustomCartFacade;
import com.aldahra.facades.order.data.ShipmentTypeDataList;
import com.aldahra.facades.validator.CartCouponValidatorFacade;
import com.aldahracommercewebservicees.cart.shipment.ShipmentTypeListWsDTO;
import com.aldahracommercewebservicees.cart.shipment.ShipmentTypeWsDTO;
import com.aldahracommercewebservicees.dto.order.CartTimeSlotWsDTO;
import com.aldahracommercewebservices.core.payment.dto.apple.ApplePayPaymentRequestWsDTO;
import com.aldahracommercewebservices.core.payment.dto.apple.ApplePayPaymentResponseWsDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;


@Controller
@RequestMapping(value = "/{baseSiteId}/users/{userId}/carts")
@CacheControl(directive = CacheControlDirective.NO_CACHE)
@Api(tags = "Carts")
public class CartsController extends BaseCommerceController
{
	public static final String PICKUP_STORE_OBJECT_NAME = "pickupStore";
	private static final Logger LOG = LoggerFactory.getLogger(CartsController.class);
	private static final String ADDRESS_MAPPING = "firstName,lastName,titleCode,phone,cellphone,line1,line2,town,postalCode,region(isocode),district,country(isocode),defaultAddress,mobileCountry(isocode),mobileNumber,nearestLandmark,addressName,city(code),area(code)";
	private static final String OBJECT_NAME_ADDRESS = "address";
	private static final long DEFAULT_PRODUCT_QUANTITY = 1;
	private static final String APPLIED_VOUCHER_WITH_STORECREDIT = "basket.validation.couponNotApplicapleWithStoreCredit";
	private static final String STORE_CREDIT_NOT_ALLOWED_WITH_VOUCHER = "basket.validation.storeCreditNotApplicapleWithCoupon";
	private static final String APPLIED_VOUCHER_EXCEEDS_LIMIT = "text.limit.voucher.apply.invalid.error";

	@Resource(name = "paymentModeWsDTOValidator")
	private Validator paymentModeValidator;

	@Resource(name = "timeSlotInfoWsDTOValidator")
	private Validator timeSlotInfoValidator;
	@Resource(name = "storeCreditWsDTOValidator")
	private Validator storeCreditValidator;
	@Resource(name = "applePayRequestValidator")
	private Validator applePayRequestValidator;
	@Resource(name = "cartValidationService")
	private CartValidationService cartValidationService;

	@Resource(name = "cartCouponValidatorFacade")
	private CartCouponValidatorFacade cartCouponValidatorFacade;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "messageSource")
	private MessageSource messageSource;

	@Resource(name = "paymentContext")
	private PaymentContext paymentContext;

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "wishlistFacade")
	private WishlistFacade wishlistFacade;

	@Resource(name = "userFacade")
	private CustomUserFacade customUserFacade;

	@Resource(name = "calculationService")
	private CalculationService calculationService;

	@Resource(name = "i18NService")
	private I18NService i18NService;

	@Resource(name = "defaultCartFacade")
	private CustomCartFacade customCartFacade;

	@Resource(name = "defaultAcceleratorCheckoutFacade")
	private CustomAcceleratorCheckoutFacade acceleratorCheckoutFacade;


	/**
	 * @return the acceleratorCheckoutFacade
	 */
	protected CustomAcceleratorCheckoutFacade getAcceleratorCheckoutFacade()
	{
		return acceleratorCheckoutFacade;
	}


	/**
	 * @return the customUserFacade
	 */
	public CustomUserFacade getCustomUserFacade()
	{
		return customUserFacade;
	}


	/**
	 * @return the messageSource
	 */
	@Override
	public MessageSource getMessageSource()
	{
		return messageSource;
	}


	/**
	 * @return the timeSlotInfoValidator
	 */
	protected Validator getTimeSlotInfoValidator()
	{
		return timeSlotInfoValidator;
	}


	/**
	 * @return the storeCreditValidator
	 */
	protected Validator getStoreCreditValidator()
	{
		return storeCreditValidator;
	}

	/**
	 * @return the baseStoreService
	 */
	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	@Resource(name = "commercePromotionRestrictionFacade")
	private CommercePromotionRestrictionFacade commercePromotionRestrictionFacade;
	@Resource(name = "commerceStockFacade")
	private CommerceStockFacade commerceStockFacade;
	@Resource(name = "customerFacade")
	private CustomerFacade customerFacade;
	@Resource(name = "pointOfServiceValidator")
	private Validator pointOfServiceValidator;
	@Resource(name = "orderEntryCreateValidator")
	private Validator orderEntryCreateValidator;
	@Resource(name = "orderEntryUpdateValidator")
	private Validator orderEntryUpdateValidator;
	@Resource(name = "orderEntryReplaceValidator")
	private Validator orderEntryReplaceValidator;
	@Resource(name = "greaterThanZeroValidator")
	private Validator greaterThanZeroValidator;
	@Resource(name = "paymentProviderRequestSupportedStrategy")
	private PaymentProviderRequestSupportedStrategy paymentProviderRequestSupportedStrategy;
	@Resource(name = "saveCartFacade")
	private SaveCartFacade saveCartFacade;
	@Resource(name = "voucherFacade")
	private VoucherFacade voucherFacade;
	@Resource(name = "modelService")
	private ModelService modelService;

	protected static CartModificationData mergeCartModificationData(final CartModificationData cmd1,
			final CartModificationData cmd2)
	{
		if ((cmd1 == null) && (cmd2 == null))
		{
			return new CartModificationData();
		}
		if (cmd1 == null)
		{
			return cmd2;
		}
		if (cmd2 == null)
		{
			return cmd1;
		}
		final CartModificationData cmd = new CartModificationData();
		cmd.setDeliveryModeChanged(Boolean
				.valueOf(Boolean.TRUE.equals(cmd1.getDeliveryModeChanged()) || Boolean.TRUE.equals(cmd2.getDeliveryModeChanged())));
		cmd.setEntry(cmd2.getEntry());
		cmd.setQuantity(cmd2.getQuantity());
		cmd.setQuantityAdded(cmd1.getQuantityAdded() + cmd2.getQuantityAdded());
		cmd.setStatusCode(cmd2.getStatusCode());
		return cmd;
	}

	protected static OrderEntryData getCartEntryForNumber(final CartData cart, final long number)
	{
		final List<OrderEntryData> entries = cart.getEntries();
		if (entries != null && !entries.isEmpty())
		{
			final Integer requestedEntryNumber = Integer.valueOf((int) number);
			for (final OrderEntryData entry : entries)
			{
				if (entry != null && requestedEntryNumber.equals(entry.getEntryNumber()))
				{
					return entry;
				}
			}
		}
		throw new CartEntryException("Entry not found", CartEntryException.NOT_FOUND, String.valueOf(number));
	}

	protected static OrderEntryData getCartEntry(final CartData cart, final String productCode, final String pickupStore)
	{
		for (final OrderEntryData oed : cart.getEntries())
		{
			if (oed.getProduct().getCode().equals(productCode))
			{
				if (pickupStore == null && oed.getDeliveryPointOfService() == null)
				{
					return oed;
				}
				else if (pickupStore != null && oed.getDeliveryPointOfService() != null
						&& pickupStore.equals(oed.getDeliveryPointOfService().getName()))
				{
					return oed;
				}
			}
		}
		return null;
	}

	protected static void validateForAmbiguousPositions(final CartData currentCart, final OrderEntryData currentEntry,
			final String newPickupStore) throws CommerceCartModificationException
	{
		final OrderEntryData entryToBeModified = getCartEntry(currentCart, currentEntry.getProduct().getCode(), newPickupStore);
		if (entryToBeModified != null && !entryToBeModified.getEntryNumber().equals(currentEntry.getEntryNumber()))
		{
			throw new CartEntryException(
					"Ambiguous cart entries! Entry number " + currentEntry.getEntryNumber()
							+ " after change would be the same as entry " + entryToBeModified.getEntryNumber(),
					CartEntryException.AMBIGIOUS_ENTRY, entryToBeModified.getEntryNumber().toString());
		}
	}

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getCarts", value = "Get all customer carts.", notes = "Lists all customer carts.")
	@ApiBaseSiteIdAndUserIdParam
	public CartListWsDTO getCarts(
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields,
			@ApiParam(value = "Optional parameter. If the parameter is provided and its value is true, only saved carts are returned.")
			@RequestParam(defaultValue = "false")
			final boolean savedCartsOnly,
			@ApiParam(value = "Optional pagination parameter in case of savedCartsOnly == true. Default value 0.")
			@RequestParam(defaultValue = DEFAULT_CURRENT_PAGE)
			final int currentPage,
			@ApiParam(value = "Optional {@link PaginationData} parameter in case of savedCartsOnly == true. Default value 20.")
			@RequestParam(defaultValue = DEFAULT_PAGE_SIZE)
			final int pageSize, @ApiParam(value = "Optional sort criterion in case of savedCartsOnly == true. No default value.")
			@RequestParam(required = false)
			final String sort)
	{
		if (getUserFacade().isAnonymousUser())
		{
			throw new AccessDeniedException("Access is denied");
		}

		final CartDataList cartDataList = new CartDataList();

		final PageableData pageableData = new PageableData();
		pageableData.setCurrentPage(currentPage);
		pageableData.setPageSize(pageSize);
		pageableData.setSort(sort);
		final List<CartData> allCarts = new ArrayList<>(
				saveCartFacade.getSavedCartsForCurrentUser(pageableData, null).getResults());
		if (!savedCartsOnly)
		{
			allCarts.addAll(getCartFacade().getCartsForCurrentUser());
		}
		cartDataList.setCarts(allCarts);

		return getDataMapper().map(cartDataList, CartListWsDTO.class, fields);
	}

	@RequestMapping(value = "/{cartId}", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getCart", value = "Get a cart with a given identifier.", notes = "Returns the cart with a given identifier.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public CartWsDTO getCart(
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields)
	{
		// CartMatchingFilter sets current cart based on cartId, so we can return cart from the session
		return getDataMapper().map(getSessionCart(), CartWsDTO.class, fields);
	}

	@RequestMapping(value = "/{cartId}/deliverytypes", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getDeliveryType", value = "Get a cart delivery type with a given identifier.", notes = "Returns cart delivery type with a given identifier.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public DeliveryModeTypeListWsDTO getDeliveryTypes(
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields)
	{

		final DeliveryModeTypeList dataList = new DeliveryModeTypeList();
		dataList.setDeliveryModeTypes(getAcceleratorCheckoutFacade().getDeliveryModeTypes());

		return getDataMapper().map(dataList, DeliveryModeTypeListWsDTO.class, fields);

	}

	@RequestMapping(value = "/{cartId}/deliverytype", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getDeliveryType", value = "Get a cart delivery type with a given identifier.", notes = "Returns cart delivery type with a given identifier.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public String getDeliveryType(
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields)
	{
		return getAcceleratorCheckoutFacade().getDeliveryModeType().getCode();
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/deliverytype", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	@ApiOperation(nickname = "setDeliveryType", value = "Defines and assigns details of a new Delivery Type to the cart.", notes = "Defines the details of a new Delivery Type, and assigns this payment option to the cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public CartWsDTO setDeliveryType(final HttpServletRequest request, //NOSONAR
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields, @RequestParam(required = true)
			final DeliveryModeType deliveryTypeCode)
			throws InvalidPaymentInfoException, NoCheckoutCartException, UnsupportedRequestException
	{
		//validate(paymentModeCode, "paymentModeCode", getPaymentModeValidator());

		getAcceleratorCheckoutFacade().setDeliveryModeType(deliveryTypeCode);

		final CartData cart = getSessionCart();

		return getDataMapper().map(cart, CartWsDTO.class, fields);
	}



	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	@ApiOperation(nickname = "createCart", value = "Creates or restore a cart for a user.", notes = "Creates a new cart or restores an anonymous cart as a user's cart (if an old Cart Id is given in the request).")
	@ApiBaseSiteIdAndUserIdParam
	public CartWsDTO createCart(@ApiParam(value = "Anonymous cart GUID.")
	@RequestParam(required = false)
	final String oldCartId, @ApiParam(value = "The GUID of the user's cart that will be merged with the anonymous cart.")
	@RequestParam(required = false)
	final String toMergeCartGuid,
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields)
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("createCart");
		}

		String evaluatedToMergeCartGuid = toMergeCartGuid;

		if (StringUtils.isNotEmpty(oldCartId))
		{
			if (getUserFacade().isAnonymousUser())
			{
				throw new CartException("Anonymous user is not allowed to copy cart!");
			}

			if (!isCartAnonymous(oldCartId))
			{
				throw new CartException("Cart is not anonymous", CartException.CANNOT_RESTORE, oldCartId);
			}

			if (StringUtils.isEmpty(evaluatedToMergeCartGuid))
			{
				evaluatedToMergeCartGuid = getSessionCart().getGuid();
			}
			else
			{
				if (!isUserCart(evaluatedToMergeCartGuid))
				{
					throw new CartException("Cart is not current user's cart", CartException.CANNOT_RESTORE, evaluatedToMergeCartGuid);
				}
			}

			try
			{
				getCartFacade().restoreAnonymousCartAndMerge(oldCartId, evaluatedToMergeCartGuid);
				return getDataMapper().map(getSessionCart(), CartWsDTO.class, fields);
			}
			catch (final CommerceCartMergingException e)
			{
				throw new CartException("Couldn't merge carts", CartException.CANNOT_MERGE, e);
			}
			catch (final CommerceCartRestorationException e)
			{
				throw new CartException("Couldn't restore cart", CartException.CANNOT_RESTORE, e);
			}
		}
		else
		{
			if (StringUtils.isNotEmpty(evaluatedToMergeCartGuid))
			{
				if (!isUserCart(evaluatedToMergeCartGuid))
				{
					throw new CartException("Cart is not current user's cart", CartException.CANNOT_RESTORE, evaluatedToMergeCartGuid);
				}

				try
				{
					getCartFacade().restoreSavedCart(evaluatedToMergeCartGuid);
					return getDataMapper().map(getSessionCart(), CartWsDTO.class, fields);
				}
				catch (final CommerceCartRestorationException e)
				{
					throw new CartException("Couldn't restore cart", CartException.CANNOT_RESTORE, oldCartId, e);
				}

			}
			return getDataMapper().map(getSessionCart(), CartWsDTO.class, fields);
		}
	}

	protected boolean isUserCart(final String toMergeCartGuid)
	{
		if (getCartFacade() instanceof CommerceWebServicesCartFacade)
		{
			final CommerceWebServicesCartFacade commerceWebServicesCartFacade = (CommerceWebServicesCartFacade) getCartFacade();
			return commerceWebServicesCartFacade.isCurrentUserCart(toMergeCartGuid);
		}
		return true;
	}

	protected boolean isCartAnonymous(final String cartGuid)
	{
		if (getCartFacade() instanceof CommerceWebServicesCartFacade)
		{
			final CommerceWebServicesCartFacade commerceWebServicesCartFacade = (CommerceWebServicesCartFacade) getCartFacade();
			return commerceWebServicesCartFacade.isAnonymousUserCart(cartGuid);
		}
		return true;
	}

	@RequestMapping(value = "/{cartId}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(nickname = "removeCart", value = "Deletes a cart with a given cart id.", notes = "Deletes a cart with a given cart id.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public void removeCart()
	{
		getCartFacade().removeSessionCart();
	}

	@Secured(
	{ "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/email", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(nickname = "replaceCartGuestUser", value = "Assigns an email to the cart.", notes = "Assigns an email to the cart. This step is required to make a guest checkout.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public void replaceCartGuestUser(
			@ApiParam(value = "Email of the guest user. It will be used during the checkout process.", required = true)
			@RequestParam
			final String email) throws DuplicateUidException
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("replaceCartGuestUser: email={}", sanitize(email));
		}

		if (!EmailValidator.getInstance().isValid(email))
		{
			throw new RequestParameterException("Email [" + sanitize(email) + "] is not a valid e-mail address!",
					RequestParameterException.INVALID, "login");
		}

		customerFacade.createGuestUserForAnonymousCheckout(email, "guest");
	}

	@RequestMapping(value = "/{cartId}/entries", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getCartEntries", value = "Get cart entries.", notes = "Returns cart entries.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public OrderEntryListWsDTO getCartEntries(
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields)
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("getCartEntries");
		}
		final OrderEntryDataList dataList = new OrderEntryDataList();
		dataList.setOrderEntries(getSessionCart().getEntries());
		return getDataMapper().map(dataList, OrderEntryListWsDTO.class, fields);
	}

	@RequestMapping(value = "/{cartId}/entries", method = RequestMethod.POST)
	@ResponseBody
	@ApiOperation(hidden = true, value = "Adds a product to the cart.", notes = "Adds a product to the cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public CartModificationWsDTO createCartEntry(@ApiParam(value = "Base site identifier.", required = true)
	@PathVariable
	final String baseSiteId,
			@ApiParam(value = "Code of the product to be added to cart. Product look-up is performed for the current product catalog version.", required = true)
			@RequestParam
			final String code, @ApiParam(value = "Quantity of product.")
			@RequestParam(defaultValue = "1")
			final long qty,
			@ApiParam(value = "Name of the store where product will be picked. Set only if want to pick up from a store.")
			@RequestParam(required = false)
			final String pickupStore,
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields) throws CommerceCartModificationException
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("createCartEntry: {}, {}, {}", logParam("code", code), logParam("qty", qty),
					logParam(PICKUP_STORE_OBJECT_NAME, pickupStore));
		}

		if (StringUtils.isNotEmpty(pickupStore))
		{
			validate(pickupStore, PICKUP_STORE_OBJECT_NAME, pointOfServiceValidator);
		}

		return addCartEntryInternal(baseSiteId, code, qty, pickupStore, fields);
	}

	protected CartModificationWsDTO addCartEntryInternal(final String baseSiteId, final String code, final long qty,
			final String pickupStore, final String fields) throws CommerceCartModificationException
	{
		final CartModificationData cartModificationData;
		if (StringUtils.isNotEmpty(pickupStore))
		{
			validateIfProductIsInStockInPOS(baseSiteId, code, pickupStore, null);
			cartModificationData = getCartFacade().addToCart(code, qty, pickupStore);
		}
		else
		{
			validateIfProductIsInStockOnline(baseSiteId, code, null);
			cartModificationData = getCartFacade().addToCart(code, qty);
		}
		return getDataMapper().map(cartModificationData, CartModificationWsDTO.class, fields);
	}

	@RequestMapping(value = "/{cartId}/entries", method = RequestMethod.POST, consumes =
	{ MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	@ResponseBody
	@ApiOperation(nickname = "createCartEntry", value = "Adds a product to the cart.", notes = "Adds a product to the cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public CartModificationWsDTO createCartEntry(@ApiParam(value = "Base site identifier", required = true)
	@PathVariable
	final String baseSiteId,
			@ApiParam(value = "Request body parameter that contains details such as the product code (product.code), the quantity of product (quantity), and the pickup store name (deliveryPointOfService.name).\n\nThe DTO is in XML or .json format.", required = true)
			@RequestBody
			final OrderEntryWsDTO entry,
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields) throws CommerceCartModificationException
	{
		if (entry.getQuantity() == null)
		{
			entry.setQuantity(Long.valueOf(DEFAULT_PRODUCT_QUANTITY));
		}

		validate(entry, "entry", orderEntryCreateValidator);

		final String pickupStore = entry.getDeliveryPointOfService() == null ? null : entry.getDeliveryPointOfService().getName();
		return addCartEntryInternal(baseSiteId, entry.getProduct().getCode(), entry.getQuantity().longValue(), pickupStore, fields);
	}

	@RequestMapping(value = "/{cartId}/entries/{entryNumber}", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getCartEntry", value = "Get the details of the cart entries.", notes = "Returns the details of the cart entries.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public OrderEntryWsDTO getCartEntry(
			@ApiParam(value = "The entry number. Each entry in a cart has an entry number. Cart entries are numbered in ascending order, starting with zero (0).", required = true)
			@PathVariable
			final long entryNumber,
			@ApiParam(value = "Response configerrors.turation. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields)
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("getCartEntry: {}", logParam("entryNumber", entryNumber));
		}
		final OrderEntryData orderEntry = getCartEntryForNumber(getSessionCart(), entryNumber);
		return getDataMapper().map(orderEntry, OrderEntryWsDTO.class, fields);
	}

	@RequestMapping(value = "/{cartId}/entries/{entryNumber}", method = RequestMethod.PUT)
	@ResponseBody
	@ApiOperation(hidden = true, value = "Set quantity and store details of a cart entry.", notes = "Updates the quantity of a single cart entry and the details of the store where the cart entry will be picked up. Attributes not provided in request will be defined again (set to null or default)")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public CartModificationWsDTO replaceCartEntry(@ApiParam(value = "Base site identifier.", required = true)
	@PathVariable
	final String baseSiteId,
			@ApiParam(value = "The entry number. Each entry in a cart has an entry number. Cart entries are numbered in ascending order, starting with zero (0).", required = true)
			@PathVariable
			final long entryNumber, @ApiParam(value = "Quantity of product.", required = true)
			@RequestParam
			final Long qty,
			@ApiParam(value = "Name of the store where product will be picked. Set only if want to pick up from a store.")
			@RequestParam(required = false)
			final String pickupStore,
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields) throws CommerceCartModificationException
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("replaceCartEntry: {}, {}, {}", logParam("entryNumber", entryNumber), logParam("qty", qty),
					logParam(PICKUP_STORE_OBJECT_NAME, pickupStore));
		}
		final CartData cart = getSessionCart();
		final OrderEntryData orderEntry = getCartEntryForNumber(cart, entryNumber);
		if (!StringUtils.isEmpty(pickupStore))
		{
			validate(pickupStore, PICKUP_STORE_OBJECT_NAME, pointOfServiceValidator);
		}

		return updateCartEntryInternal(baseSiteId, cart, orderEntry, qty, pickupStore, fields, true);
	}

	protected CartModificationWsDTO updateCartEntryInternal(final String baseSiteId, final CartData cart,
			final OrderEntryData orderEntry, final Long qty, final String pickupStore, final String fields, final boolean putMode)
			throws CommerceCartModificationException
	{
		final long entryNumber = orderEntry.getEntryNumber().longValue();
		final String productCode = orderEntry.getProduct().getCode();
		final PointOfServiceData currentPointOfService = orderEntry.getDeliveryPointOfService();

		CartModificationData cartModificationData1 = null;
		CartModificationData cartModificationData2 = null;

		if (!StringUtils.isEmpty(pickupStore))
		{
			if (currentPointOfService == null || !currentPointOfService.getName().equals(pickupStore))
			{
				//was 'shipping mode' or store is changed
				validateForAmbiguousPositions(cart, orderEntry, pickupStore);
				validateIfProductIsInStockInPOS(baseSiteId, productCode, pickupStore, Long.valueOf(entryNumber));
				cartModificationData1 = getCartFacade().updateCartEntry(entryNumber, pickupStore);
			}
		}
		else if (putMode && currentPointOfService != null)
		{
			//was 'pickup in store', now switch to 'shipping mode'
			validateForAmbiguousPositions(cart, orderEntry, pickupStore);
			validateIfProductIsInStockOnline(baseSiteId, productCode, Long.valueOf(entryNumber));
			cartModificationData1 = getCartFacade().updateCartEntry(entryNumber, pickupStore);
		}

		if (qty != null)
		{
			cartModificationData2 = getCartFacade().updateCartEntry(entryNumber, qty.longValue());
		}

		return getDataMapper().map(mergeCartModificationData(cartModificationData1, cartModificationData2),
				CartModificationWsDTO.class, fields);
	}

	@RequestMapping(value = "/{cartId}/entries/{entryNumber}", method = RequestMethod.PUT, consumes =
	{ MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	@ResponseBody
	@ApiOperation(nickname = "replaceCartEntry", value = "Set quantity and store details of a cart entry.", notes = "Updates the quantity of a single cart entry and the details of the store where the cart entry will be picked up. "
			+ "Attributes not provided in request will be defined again (set to null or default)")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public CartModificationWsDTO replaceCartEntry(@ApiParam(value = "Base site identifier.", required = true)
	@PathVariable
	final String baseSiteId,
			@ApiParam(value = "The entry number. Each entry in a cart has an entry number. Cart entries are numbered in ascending order, starting with zero (0).", required = true)
			@PathVariable
			final long entryNumber,
			@ApiParam(value = "Request body parameter that contains details such as the quantity of product (quantity), and the pickup store name (deliveryPointOfService.name)\n\nThe DTO is in XML or .json format.", required = true)
			@RequestBody
			final OrderEntryWsDTO entry,
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields) throws CommerceCartModificationException
	{
		final CartData cart = getSessionCart();
		final OrderEntryData orderEntry = getCartEntryForNumber(cart, entryNumber);
		final String pickupStore = entry.getDeliveryPointOfService() == null ? null : entry.getDeliveryPointOfService().getName();

		validateCartEntryForReplace(orderEntry, entry);

		return updateCartEntryInternal(baseSiteId, cart, orderEntry, entry.getQuantity(), pickupStore, fields, true);
	}

	protected void validateCartEntryForReplace(final OrderEntryData oryginalEntry, final OrderEntryWsDTO entry)
	{
		final String productCode = oryginalEntry.getProduct().getCode();
		final Errors errors = new BeanPropertyBindingResult(entry, "entry");
		if (entry.getProduct() != null && entry.getProduct().getCode() != null && !entry.getProduct().getCode().equals(productCode))
		{
			errors.reject("cartEntry.productCodeNotMatch");

			throw new WebserviceValidationException(errors);
		}

		validate(entry, "entry", orderEntryReplaceValidator);
	}

	@RequestMapping(value = "/{cartId}/entries/{entryNumber}", method = RequestMethod.PATCH)
	@ResponseBody
	@ApiOperation(hidden = true, value = "Update quantity and store details of a cart entry.", notes = "Updates the quantity of a single cart entry and the details of the store where the cart entry will be picked up.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public CartModificationWsDTO updateCartEntry(@ApiParam(value = "Base site identifier.", required = true)
	@PathVariable
	final String baseSiteId,
			@ApiParam(value = "The entry number. Each entry in a cart has an entry number. Cart entries are numbered in ascending order, starting with zero (0).", required = true)
			@PathVariable
			final long entryNumber, @ApiParam(value = "Quantity of product.")
			@RequestParam(required = false)
			final Long qty,
			@ApiParam(value = "Name of the store where product will be picked. Set only if want to pick up from a store.")
			@RequestParam(required = false)
			final String pickupStore,
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields) throws CommerceCartModificationException
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("updateCartEntry: {}, {}, {}", logParam("entryNumber", entryNumber), logParam("qty", qty),
					logParam(PICKUP_STORE_OBJECT_NAME, pickupStore));
		}

		final CartData cart = getSessionCart();
		final OrderEntryData orderEntry = getCartEntryForNumber(cart, entryNumber);

		if (qty == null && StringUtils.isEmpty(pickupStore))
		{
			throw new RequestParameterException("At least one parameter (qty,pickupStore) should be set!",
					RequestParameterException.MISSING);
		}

		if (qty != null)
		{
			validate(qty, "quantity", greaterThanZeroValidator);
		}

		if (pickupStore != null)
		{
			validate(pickupStore, PICKUP_STORE_OBJECT_NAME, pointOfServiceValidator);
		}

		return updateCartEntryInternal(baseSiteId, cart, orderEntry, qty, pickupStore, fields, false);
	}

	@RequestMapping(value = "/{cartId}/entries/{entryNumber}", method = RequestMethod.PATCH, consumes =
	{ MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	@ResponseBody
	@ApiOperation(nickname = "updateCartEntry", value = "Update quantity and store details of a cart entry.", notes = "Updates the quantity of a single cart entry and the details of the store where the cart entry will be picked up.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public CartModificationWsDTO updateCartEntry(@ApiParam(value = "Base site identifier.", required = true)
	@PathVariable
	final String baseSiteId,
			@ApiParam(value = "The entry number. Each entry in a cart has an entry number. Cart entries are numbered in ascending order, starting with zero (0).", required = true)
			@PathVariable
			final long entryNumber,
			@ApiParam(value = "Request body parameter that contains details such as the quantity of product (quantity), and the pickup store name (deliveryPointOfService.name)\n\nThe DTO is in XML or .json format.", required = true)
			@RequestBody
			final OrderEntryWsDTO entry,
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields) throws CommerceCartModificationException
	{
		final CartData cart = getSessionCart();
		final OrderEntryData orderEntry = getCartEntryForNumber(cart, entryNumber);

		final String productCode = orderEntry.getProduct().getCode();
		final Errors errors = new BeanPropertyBindingResult(entry, "entry");
		if (entry.getProduct() != null && entry.getProduct().getCode() != null && !entry.getProduct().getCode().equals(productCode))
		{
			errors.reject("cartEntry.productCodeNotMatch");
			throw new WebserviceValidationException(errors);
		}

		if (entry.getQuantity() == null)
		{
			entry.setQuantity(orderEntry.getQuantity());
		}

		validate(entry, "entry", orderEntryUpdateValidator);

		final String pickupStore = entry.getDeliveryPointOfService() == null ? null : entry.getDeliveryPointOfService().getName();
		return updateCartEntryInternal(baseSiteId, cart, orderEntry, entry.getQuantity(), pickupStore, fields, false);
	}

	@RequestMapping(value = "/{cartId}/entries/{entryNumber}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(nickname = "removeCartEntry", value = "Deletes cart entry.", notes = "Deletes cart entry.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public void removeCartEntry(
			@ApiParam(value = "The entry number. Each entry in a cart has an entry number. Cart entries are numbered in ascending order, starting with zero (0).", required = true)
			@PathVariable
			final long entryNumber) throws CommerceCartModificationException
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("removeCartEntry: {}", logParam("entryNumber", entryNumber));
		}

		final CartData cart = getSessionCart();
		getCartEntryForNumber(cart, entryNumber);
		getCartFacade().updateCartEntry(entryNumber, 0);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_GUEST", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/addresses/delivery", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	@ApiOperation(hidden = true, value = "Creates a delivery address for the cart.", notes = "Creates an address and assigns it to the cart as the delivery address.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public AddressWsDTO createCartDeliveryAddress(final HttpServletRequest request,
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields) throws NoCheckoutCartException
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("createCartDeliveryAddress");
		}
		final AddressData addressData = super.createAddressInternal(request);
		final String addressId = addressData.getId();
		super.setCartDeliveryAddressInternal(addressId);
		return getDataMapper().map(addressData, AddressWsDTO.class, fields);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_GUEST", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/addresses/delivery", method = RequestMethod.POST, consumes =
	{ MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	@ApiOperation(nickname = "createCartDeliveryAddress", value = "Creates a delivery address for the cart.", notes = "Creates an address and assigns it to the cart as the delivery address.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public AddressWsDTO createCartDeliveryAddress(
			@ApiParam(value = "Request body parameter that contains details such as the customer's first name (firstName), the customer's last name (lastName), the customer's title (titleCode), the customer's phone (phone), "
					+ "the country (country.isocode), the first part of the address (line1), the second part of the address (line2), the town (town), the postal code (postalCode), and the region (region.isocode).\n\nThe DTO is in XML or .json format.", required = true)
			@RequestBody
			final AddressWsDTO address,
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields) throws NoCheckoutCartException
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("createCartDeliveryAddress");
		}
		validate(address, OBJECT_NAME_ADDRESS, getAddressDTOValidator());
		AddressData addressData = getDataMapper().map(address, AddressData.class, ADDRESS_MAPPING);
		addressData = createAddressInternal(addressData);
		setCartDeliveryAddressInternal(addressData.getId());
		return getDataMapper().map(addressData, AddressWsDTO.class, fields);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/addresses/delivery", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(nickname = "replaceCartDeliveryAddress", value = "Sets a delivery address for the cart.", notes = "Sets a delivery address for the cart. The address country must be placed among the delivery countries of the current base store.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public void replaceCartDeliveryAddress(@ApiParam(value = "Address identifier", required = true)
	@RequestParam
	final String addressId) throws NoCheckoutCartException
	{
		super.setCartDeliveryAddressInternal(addressId);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/addresses/delivery", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(nickname = "removeCartDeliveryAddress", value = "Deletes the delivery address from the cart.", notes = "Deletes the delivery address from the cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public void removeCartDeliveryAddress()
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("removeCartDeliveryAddress");
		}
		if (!getCheckoutFacade().removeDeliveryAddress())
		{
			throw new CartException("Cannot reset address!", CartException.CANNOT_RESET_ADDRESS);
		}
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/deliverymode", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getCartDeliveryMode", value = "Get the delivery mode selected for the cart.", notes = "Returns the delivery mode selected for the cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public DeliveryModeWsDTO getCartDeliveryMode(
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields)
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("getCartDeliveryMode");
		}
		return getDataMapper().map(getSessionCart().getDeliveryMode(), DeliveryModeWsDTO.class, fields);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/deliverymode", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(nickname = "replaceCartDeliveryMode", value = "Sets the delivery mode for a cart.", notes = "Sets the delivery mode with a given identifier for the cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public void replaceCartDeliveryMode(@ApiParam(value = "Delivery mode identifier (code)", required = true)
	@RequestParam(required = true)
	final String deliveryModeId) throws UnsupportedDeliveryModeException
	{
		super.setCartDeliveryModeInternal(deliveryModeId);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/deliverymode", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(nickname = "removeCartDeliveryMode", value = "Deletes the delivery mode from the cart.", notes = "Deletes the delivery mode from the cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public void removeCartDeliveryMode()
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("removeCartDeliveryMode");
		}
		if (!getCheckoutFacade().removeDeliveryMode())
		{
			throw new CartException("Cannot reset delivery mode!", CartException.CANNOT_RESET_DELIVERYMODE);
		}
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/deliverymodes", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getCartDeliveryModes", value = "Get all delivery modes for the current store and delivery address.", notes = "Returns all delivery modes supported for the "
			+ "current base store and cart delivery address. A delivery address must be set for the cart, otherwise an empty list will be returned.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public DeliveryModeListWsDTO getCartDeliveryModes(
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields)
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("getCartDeliveryModes");
		}
		final DeliveryModesData deliveryModesData = new DeliveryModesData();
		deliveryModesData.setDeliveryModes(getCheckoutFacade().getSupportedDeliveryModes());

		return getDataMapper().map(deliveryModesData, DeliveryModeListWsDTO.class, fields);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/payment/info", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getPaymentInfo", value = "Get getPaymentInfo for the current store and delivery address.", notes = "Returns payment info")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public PaymentRequestWsDTO getPaymentInfo(
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields) throws PaymentException
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("getPaymentInfo");
		}
		if (!cartService.hasSessionCart())
		{
			throw new CartException("No cart found for the current user");
		}

		final Optional<PaymentRequestData> paymentDataByCurrentStore = paymentContext
				.getPaymentDataByCurrentStore(cartService.getSessionCart());

		if (paymentDataByCurrentStore.isEmpty())
		{
			throw new PaymentException("Could not fetch the payment data!", PaymentExceptionType.BAD_REQUEST);
		}

		final RequestData data = (RequestData) paymentDataByCurrentStore.get().getData();

		return getDataMapper().map(data, PaymentRequestWsDTO.class, fields);
	}


	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/payment/transaction", method = RequestMethod.POST)
	@ResponseBody
	@ApiOperation(nickname = "setPaymentTransaction", value = "Set getPaymentTransaction for the current store and delivery address.", notes = "Returns payment transaction")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public PaymentDetailsWsDTO setPaymentTransaction(
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields, @RequestParam("data")
			final String data, @ApiParam(value = "ccAvenue ID")
			@RequestParam(required = false)
			final String ccAvenueId) throws PaymentException, InvalidPaymentInfoException, NoCheckoutCartException
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("setPaymentTransaction");
		}
		if (!getCheckoutFacade().hasCheckoutCart())
		{
			throw new NoCheckoutCartException("Cannot add Payment Transaction. There was no checkout cart created yet!");
		}


		getCheckoutFacade().setCCAvenueId(ccAvenueId);

		getCheckoutFacade().resetPaymentInfo();

		Optional<PaymentResponseData> paymentOrderStatusResponseData = null;

		paymentOrderStatusResponseData = getCheckoutFacade().getPaymentOrderStatusResponseData(data);

		final Map<String, Object> orderInfoMap = paymentOrderStatusResponseData.get().getResponseData();

		if (orderInfoMap.get("status") == null || ((Integer) orderInfoMap.get("status")).equals(Integer.valueOf(1)))
		{
			LOG.error("invalid status code");
			throw new PaymentException(PaymentExceptionType.INVALID_ORDER_STATUS.getMessage(),
					PaymentExceptionType.INVALID_ORDER_STATUS);
		}
		saveDiscountIfReturned(orderInfoMap);
		final Optional<PaymentSubscriptionResultData> completePaymentCreateSubscription = getCheckoutFacade()
				.completePaymentCreateSubscription(orderInfoMap, true);
		if (!completePaymentCreateSubscription.isPresent() || completePaymentCreateSubscription.get().getStoredCard() == null)
		{
			LOG.error("Complete pyament create subscription is failed");
			throw new PaymentException(PaymentExceptionType.INVALID_ORDER_STATUS.getMessage(),
					PaymentExceptionType.INVALID_ORDER_STATUS);
		}
		else
		{
			final CCPaymentInfoData newPaymentSubscription = completePaymentCreateSubscription.get().getStoredCard();

			if (getUserFacade().getCCPaymentInfos(true).size() <= 1)
			{
				getUserFacade().setDefaultPaymentInfo(newPaymentSubscription);
			}
			getCheckoutFacade().setPaymentDetails(newPaymentSubscription.getId());
		}

		final CCPaymentInfoData paymentInfoData = completePaymentCreateSubscription.get().getStoredCard();
		//		paymentInfoData = addPaymentDetailsInternal(paymentInfoData).getPaymentInfo();
		return getDataMapper().map(paymentInfoData, PaymentDetailsWsDTO.class, fields);
	}

	private void saveDiscountIfReturned(final Map<String, Object> responseData)
	{
		if (responseData.get("order_discount") == null)
		{
			return;
		}
		Double discountValue = null;
		try
		{
			discountValue = (Double) responseData.get("order_discount");
		}
		catch (final Exception e)
		{
			LOG.error("Error casting discount_value", e);

		}

		LOG.info("discount_value from ccavenue order status: " + discountValue);

		final double discount = discountValue == null ? 0 : discountValue.doubleValue();
		LOG.info("discount to be added on order: " + discount);
		if (discount > 0)
		{
			appendDiscountForSessionCart("bankOffer", discount);
		}
	}

	private void appendDiscountForSessionCart(final String offerCode, final double value)
	{
		final CartModel sessionCart = cartService.getSessionCart();
		final List<DiscountValue> allDiscounts = new ArrayList<DiscountValue>();
		allDiscounts.add(new DiscountValue(offerCode, value, true, sessionCart.getCurrency().getIsocode()));
		if (CollectionUtils.isEmpty(sessionCart.getGlobalDiscountValues()))
		{
			LOG.info("Setting discount on Session cart");
			sessionCart.setGlobalDiscountValues(allDiscounts);
		}
		else
		{
			LOG.info("Appending discount on Session cart");
			allDiscounts.addAll(sessionCart.getGlobalDiscountValues());
			sessionCart.setGlobalDiscountValues(allDiscounts);
		}
		LOG.info("Saving discounts on cart: " + DiscountValue.toString(allDiscounts));
		modelService.save(sessionCart);
		try
		{
			calculationService.calculateTotals(sessionCart, true);
		}
		catch (final CalculationException e)
		{
			LOG.error("Exception invoking recalculate", e);
		}
	}

	private double getDoubleValue(final String input)
	{
		try
		{
			return Double.parseDouble(input);
		}
		catch (final Exception e)
		{
			LOG.error("Error casting String to Double", e);
			return 0;
		}
	}

	@RequestMapping(value = "/{cartId}/verification", method = RequestMethod.POST)
	@ResponseBody
	@ApiOperation(nickname = "getCartVerification", value = "Get CartVerification for the current store and delivery address.", notes = "Returns valid flag and errors if not valid")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public CartCheckOutValidatorWsDTO getCartVerification(
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields) throws InvalidCartException, WebserviceValidationException, NoCheckoutCartException
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("getCartVerification");
		}

		validateCart();

		final CartCheckOutValidatorData cartCheckOutValidatorData = new CartCheckOutValidatorData();
		cartCheckOutValidatorData.setValid(true);
		final List<String> errors = new LinkedList<>();

		try
		{

			final boolean validateProfileAttributes = cartValidationService.validateProfileAttributesByCurrentCart();

		}
		catch (final CartValidationException ex)
		{
			if (CartExceptionType.MISSING_VALUE.equals(ex.getCartExceptionType()))
			{
				final String[] errorParm = ex.getErrorParm();
				cartCheckOutValidatorData.setValid(false);
				final Locale currentLocale = getI18NService().getCurrentLocale();

				errors.add(getMessageSource().getMessage("cart.missing.attributes.error", errorParm, "cart.missing.attributes.error",
						currentLocale));
			}
		}
		//******* Max and Min Validation **********//
		try
		{

			final boolean validateCartMaxAmount = cartValidationService.validateCartMaxAmountByCurrentCart();

		}
		catch (final CartValidationException ex)
		{
			if (CartExceptionType.MAX_AMOUNT.equals(ex.getCartExceptionType()))
			{
				final String[] errorParm = ex.getErrorParm();
				cartCheckOutValidatorData.setValid(false);

				errors.add(getMessageSource().getMessage("max2.cart.amount.value.error", errorParm,
						"Please note, The maximum order amount is {0} {1}", null));




			}
		}
		try
		{

			final boolean validateCartMaxAmount = cartValidationService.validateCartMinAmountByCurrentCart();

		}
		catch (final CartValidationException ex)
		{
			if (CartExceptionType.MIN_AMOUNT.equals(ex.getCartExceptionType()))
			{
				final String[] errorParm = ex.getErrorParm();
				cartCheckOutValidatorData.setValid(false);

				errors.add(getMessageSource().getMessage("min2.cart.amount.value.error", errorParm,
						"Min order amount is {0} {1}, please add {4} {1} more to continue with checkout.", null));

			}
		}
		//******* End Max and Min Validation **********//

		//******* Start Coupon Validation **********//
		try
		{

			cartCouponValidatorFacade.validateCartCouponByCurrentCart();

		}
		catch (final CartCouponException ex)
		{
			if (CartCouponExceptionType.COUPON_FOR_PICKUP.equals(ex.getType()))
			{
				final String[] errorParm = new String[]
				{ Arrays.toString(ex.getArgs()) };

				errors.add(getMessageSource().getMessage("cart.coupon.for.pickup.error", errorParm,
						"Your coupons have been removed as you change to pickup in store delivery {0}", null));

			}
		}
		//******* End Coupon Validation **********//

		if (!cartCheckOutValidatorData.getValid())
		{


			cartCheckOutValidatorData.setErrors(errors);
		}

		return getDataMapper().map(cartCheckOutValidatorData, CartCheckOutValidatorWsDTO.class, fields);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/timeSlot", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getCartTimeSlot", value = "Get timeSlot for the current store.", notes = "Returns timeSlot "
			+ "current base store and cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public TimeSlotWsDTO getCartTimeSlot(
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields) throws TimeSlotException
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("getCartTimeSlot");
		}

		final Optional<TimeSlotData> supportedTimeSlot = getCheckoutFacade().getSupportedTimeSlot();

		TimeSlotData timeSlotData = null;
		if (supportedTimeSlot.isPresent())
		{
			timeSlotData = supportedTimeSlot.get();
		}
		else
		{
			throw new TimeSlotException(TimeSlotExceptionType.NO_TIMESLOTS_FOUND,
					"Timeslot requires a missing attribute to be selected.");
		}

		return getDataMapper().map(timeSlotData, TimeSlotWsDTO.class, fields);
	}




	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/timeSlot", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	@ApiOperation(nickname = "setTimeSlot", value = "Defines and assigns timeSlot to the cart.", notes = "Defines and assigns timeSlot to the cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public void setTimeSlot(final HttpServletRequest request, //NOSONAR
			@RequestBody
			final TimeSlotInfoWsDTO timeSlotInfos, //NOSONAR
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields) throws InvalidPaymentInfoException, NoCheckoutCartException, UnsupportedRequestException
	{

		validateTimeSlot(timeSlotInfos);

		final TimeSlotInfoData data = new TimeSlotInfoData();
		data.setDate(timeSlotInfos.getDate());
		data.setDay(timeSlotInfos.getDay());
		data.setEnd(timeSlotInfos.getEnd());
		data.setStart(timeSlotInfos.getStart());
		data.setPeriodCode(timeSlotInfos.getPeriodCode());

		//		data.set
		getCheckoutFacade().setTimeSlot(data);

	}

	protected void validateTimeSlot(final TimeSlotInfoWsDTO timeSlotInfos) throws NoCheckoutCartException
	{
		if (!getCheckoutFacade().hasCheckoutCart())
		{
			throw new NoCheckoutCartException("Cannot add timeSlotInfos. There was no checkout cart created yet!");
		}
		validate(timeSlotInfos, "timeSlotInfos", getTimeSlotInfoValidator());
	}


	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/paymentmodes", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getCartPaymentModes", value = "Get all payment modes for the current store.", notes = "Returns all payment modes supported for the "
			+ "current base store and cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public PaymentModeListWsDTO getCartPaymentModes(
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields, @ApiParam(value = "Optional parameter. If Apple payment mode would be returned or not")
			@RequestParam(name = "includeApplePay", required = false, defaultValue = "false")
			final boolean includeApplePay)
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("getCartPaymentModes");
		}
		final Optional<List<PaymentModeData>> supportedPaymentModes = getCheckoutFacade().getSupportedPaymentModesForOCC();
		final PaymentModesData paymentModesData = new PaymentModesData();
		if (supportedPaymentModes.isPresent() && includeApplePay)
		{
			paymentModesData.setPaymentModes(supportedPaymentModes.get());
		}
		else if (supportedPaymentModes.isPresent())
		{
			paymentModesData.setPaymentModes(
					supportedPaymentModes.get().stream().filter(mode -> !"apple".equals(mode.getCode())).collect(Collectors.toList()));
		}
		return getDataMapper().map(paymentModesData, PaymentModeListWsDTO.class, fields);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/paymentmodes/device", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getCartPaymentModes", value = "Get all payment modes for the current store.", notes = "Returns all payment modes supported for the "
			+ "current base store and cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public PaymentModeListWsDTO getCartPaymentModesForDevice(
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields, @ApiParam(value = "Optional parameter. payment mode based on the selected device")
			@RequestParam(name = "device", required = false)
			final SalesApplication device)
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("getCartPaymentModes");
		}
		final Optional<List<PaymentModeData>> supportedPaymentModes = getCheckoutFacade()
				.getSupportedPaymentModesForOCCForDevice(device);
		final PaymentModesData paymentModesData = new PaymentModesData();
		if (supportedPaymentModes.isPresent())
		{
			paymentModesData.setPaymentModes(supportedPaymentModes.get());
		}
		return getDataMapper().map(paymentModesData, PaymentModeListWsDTO.class, fields);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/storecreditmodes", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	@ApiOperation(nickname = "setStoreCreditMode", value = "Defines and assigns details of a new credit card payment to the cart.", notes = "Defines the details of a new credit card, and assigns this payment option to the cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public void setStoreCreditMode(final HttpServletRequest request, //NOSONAR
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields, @RequestParam(required = true)
			final String storeCreditModeTypeCode, @RequestParam(required = false)
			final Double amount) throws InvalidPaymentInfoException, NoCheckoutCartException, UnsupportedRequestException
	{
		final StoreCreditDetailsWsDTO storeCreditDetailsWsDTO = new StoreCreditDetailsWsDTO();
		storeCreditDetailsWsDTO.setAmount(amount);
		storeCreditDetailsWsDTO.setStoreCreditModeTypeCode(storeCreditModeTypeCode);
		validate(storeCreditDetailsWsDTO, "storeCreditModeTypeCode", getStoreCreditValidator());

		//		if (isCartContainsAppliedCoupon()
		//				&& !StoreCreditModeType.REDEEM_NONE.getCode().equals(storeCreditDetailsWsDTO.getStoreCreditModeTypeCode()))
		//		{
		//			throw new IllegalArgumentException(getMessageSource().getMessage(STORE_CREDIT_NOT_ALLOWED_WITH_VOUCHER, null,
		//					STORE_CREDIT_NOT_ALLOWED_WITH_VOUCHER, i18NService.getCurrentLocale()));
		//		}
		getCheckoutFacade().setStoreCreditMode(storeCreditModeTypeCode, amount);
	}

	private boolean isCartContainsAppliedCoupon()
	{
		return CollectionUtils.isEmpty(cartService.getSessionCart().getAppliedCouponCodes()) ? false : true;
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/paymentmodes", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	@ApiOperation(nickname = "setPaymentMode", value = "Defines and assigns details of a new credit card payment to the cart.", notes = "Defines the details of a new credit card, and assigns this payment option to the cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public PaymentModeWsDTO setPaymentMode(final HttpServletRequest request, //NOSONAR
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields, @RequestParam(required = true)
			final String paymentModeCode) throws InvalidPaymentInfoException, NoCheckoutCartException, UnsupportedRequestException
	{
		validate(paymentModeCode, "paymentModeCode", getPaymentModeValidator());
		getCheckoutFacade().setPaymentMode(paymentModeCode);
		final CartData cart = getSessionCart();

		getCheckoutFacade().resetPaymentInfo();

		final Optional<AddressData> address = getCheckoutFacade().getCartAddress();
		final AddressData addressData = address.isPresent() ? address.get() : null;

		if (addressData != null)
		{
			addressData.setShippingAddress(Boolean.TRUE);
			addressData.setBillingAddress(Boolean.TRUE);
		}

		switch (paymentModeCode.toLowerCase())
		{
			case "pis":

			case "cod":

			case "ccod":

			case "continue":


				final NoCardPaymentInfoData noCardPaymentInfoData = new NoCardPaymentInfoData();
				final NoCardTypeData noCardTypeData = new NoCardTypeData();
				noCardTypeData.setCode(paymentModeCode.toUpperCase());
				noCardPaymentInfoData.setNoCardTypeData(noCardTypeData);
				noCardPaymentInfoData.setDefaultPaymentInfo(true);
				noCardPaymentInfoData.setBillingAddress(addressData);
				noCardPaymentInfoData.setSaved(true);

				final Optional<NoCardPaymentInfoData> createPaymentSubscription = getCheckoutFacade()
						.createPaymentSubscription(noCardPaymentInfoData);
				//				getCheckoutFacade().saveBillingAddress(cart.getDeliveryAddress());
				setPaymentSubscription(createPaymentSubscription.get());
				break;


		}
		getCheckoutFacade().saveBillingAddress(addressData);

		final Optional<List<PaymentModeData>> supportedPaymentModes = getCheckoutFacade().getSupportedPaymentModes();
		PaymentModeData paymentModeData = null;
		if (supportedPaymentModes.isPresent())
		{
			final List<PaymentModeData> supportedPaymentModesList = supportedPaymentModes.get();
			paymentModeData = supportedPaymentModesList.stream().filter(pm -> paymentModeCode.equals(pm.getCode())).findFirst()
					.orElseGet(null);
		}
		return getDataMapper().map(paymentModeData, PaymentModeWsDTO.class, fields);
	}

	protected boolean setPaymentSubscription(final NoCardPaymentInfoData newPaymentSubscription)
	{
		if (newPaymentSubscription != null && StringUtils.isNotBlank(newPaymentSubscription.getId()))
		{
			if (getCustomUserFacade().getNoCardPaymentInfos(true).isPresent()
					&& getCustomUserFacade().getNoCardPaymentInfos(true).get().size() <= 1)
			{
				getCustomUserFacade().setDefaultPaymentInfo(newPaymentSubscription);
			}
			getCheckoutFacade().setGeneralPaymentDetails(newPaymentSubscription.getId());
		}
		return true;
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/storecreditmodes", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getStoreCreditModes", value = "Get all payment modes for the current store.", notes = "Returns all payment modes supported for the "
			+ "current base store and cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public StoreCreditModeListWsDTO getStoreCreditModes(
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields)
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("getStoreCreditModes");
		}
		final Optional<List<StoreCreditModeData>> supportedStoreCreditModes = getCheckoutFacade().getSupportedStoreCreditModes();
		final StoreCreditModesData storeCreditModesData = new StoreCreditModesData();
		if (supportedStoreCreditModes.isPresent())
		{
			storeCreditModesData.setStoreCreditModes(supportedStoreCreditModes.get());
		}
		return getDataMapper().map(storeCreditModesData, StoreCreditModeListWsDTO.class, fields);
	}


	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/paymentdetails", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	@ApiOperation(hidden = true, value = "Defines and assigns details of a new credit card payment to the cart.", notes = "Defines the details of a new credit card, and assigns this payment option to the cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public PaymentDetailsWsDTO createCartPaymentDetails(final HttpServletRequest request, //NOSONAR
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields) throws InvalidPaymentInfoException, NoCheckoutCartException, UnsupportedRequestException
	{
		paymentProviderRequestSupportedStrategy.checkIfRequestSupported("addPaymentDetails");
		final CCPaymentInfoData paymentInfoData = super.addPaymentDetailsInternal(request).getPaymentInfo();
		return getDataMapper().map(paymentInfoData, PaymentDetailsWsDTO.class, fields);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/paymentdetails", method = RequestMethod.POST, consumes =
	{ MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	@ApiOperation(nickname = "createCartPaymentDetails", value = "Defines and assigns details of a new credit card payment to the cart.", notes = "Defines the details of a new credit card, and assigns this payment option to the cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public PaymentDetailsWsDTO createCartPaymentDetails(
			@ApiParam(value = "Request body parameter that contains details such as the name on the card (accountHolderName), the card number (cardNumber), the card type (cardType.code), "
					+ "the month of the expiry date (expiryMonth), the year of the expiry date (expiryYear), whether the payment details should be saved (saved), whether the payment details "
					+ "should be set as default (defaultPaymentInfo), and the billing address (billingAddress.firstName, billingAddress.lastName, billingAddress.titleCode, billingAddress.country.isocode, "
					+ "billingAddress.line1, billingAddress.line2, billingAddress.town, billingAddress.postalCode, billingAddress.region.isocode)\n\nThe DTO is in XML or .json format.", required = true)
			@RequestBody
			final PaymentDetailsWsDTO paymentDetails, //NOSONAR
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields) throws InvalidPaymentInfoException, NoCheckoutCartException, UnsupportedRequestException
	{
		paymentProviderRequestSupportedStrategy.checkIfRequestSupported("addPaymentDetails");
		validatePayment(paymentDetails);
		final String copiedfields = "accountHolderName,cardNumber,cardType,cardTypeData(code),expiryMonth,expiryYear,issueNumber,startMonth,startYear,subscriptionId,defaultPaymentInfo,saved,billingAddress(titleCode,firstName,lastName,line1,line2,town,postalCode,country(isocode),region(isocode),defaultAddress)";
		CCPaymentInfoData paymentInfoData = getDataMapper().map(paymentDetails, CCPaymentInfoData.class, copiedfields);
		paymentInfoData = addPaymentDetailsInternal(paymentInfoData).getPaymentInfo();
		return getDataMapper().map(paymentInfoData, PaymentDetailsWsDTO.class, fields);
	}

	protected void validatePayment(final PaymentDetailsWsDTO paymentDetails) throws NoCheckoutCartException
	{
		if (!getCheckoutFacade().hasCheckoutCart())
		{
			throw new NoCheckoutCartException("Cannot add PaymentInfo. There was no checkout cart created yet!");
		}
		validate(paymentDetails, "paymentDetails", getPaymentDetailsDTOValidator());
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/paymentdetails", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(nickname = "replaceCartPaymentDetails", value = "Sets credit card payment details for the cart.", notes = "Sets credit card payment details for the cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public void replaceCartPaymentDetails(@ApiParam(value = "Payment details identifier.", required = true)
	@RequestParam
	final String paymentDetailsId) throws InvalidPaymentInfoException
	{
		super.setPaymentDetailsInternal(paymentDetailsId);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_CLIENT", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/promotions", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getCartPromotions", value = "Get information about promotions applied on cart.", notes = "Returns information about the promotions applied on the cart. "
			+ "Requests pertaining to promotions have been developed for the previous version of promotions and vouchers, and as a result, some of them "
			+ "are currently not compatible with the new promotions engine.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public PromotionResultListWsDTO getCartPromotions(
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(required = false, defaultValue = DEFAULT_FIELD_SET)
			final String fields)
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("getCartPromotions");
		}
		final List<PromotionResultData> appliedPromotions = new ArrayList<>();
		final List<PromotionResultData> orderPromotions = getSessionCart().getAppliedOrderPromotions();
		final List<PromotionResultData> productPromotions = getSessionCart().getAppliedProductPromotions();
		appliedPromotions.addAll(orderPromotions);
		appliedPromotions.addAll(productPromotions);

		final PromotionResultDataList dataList = new PromotionResultDataList();
		dataList.setPromotions(appliedPromotions);
		return getDataMapper().map(dataList, PromotionResultListWsDTO.class, fields);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_CLIENT", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/promotions/{promotionId}", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getCartPromotion", value = "Get information about promotion applied on cart.", notes = "Returns information about a promotion (with a specific promotionId), that has "
			+ "been applied on the cart. Requests pertaining to promotions have been developed for the previous version of promotions and vouchers, and as a result, some "
			+ "of them are currently not compatible with the new promotions engine.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public PromotionResultListWsDTO getCartPromotion(@ApiParam(value = "Promotion identifier (code)", required = true)
	@PathVariable
	final String promotionId,
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields)
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("getCartPromotion: promotionId = {}", sanitize(promotionId));
		}
		final List<PromotionResultData> appliedPromotions = new ArrayList<>();
		final List<PromotionResultData> orderPromotions = getSessionCart().getAppliedOrderPromotions();
		final List<PromotionResultData> productPromotions = getSessionCart().getAppliedProductPromotions();
		for (final PromotionResultData prd : orderPromotions)
		{
			if (prd.getPromotionData().getCode().equals(promotionId))
			{
				appliedPromotions.add(prd);
			}
		}
		for (final PromotionResultData prd : productPromotions)
		{
			if (prd.getPromotionData().getCode().equals(promotionId))
			{
				appliedPromotions.add(prd);
			}
		}

		final PromotionResultDataList dataList = new PromotionResultDataList();
		dataList.setPromotions(appliedPromotions);
		return getDataMapper().map(dataList, PromotionResultListWsDTO.class, fields);
	}

	@Secured(
	{ "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/promotions", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(nickname = "doApplyCartPromotion", value = "Enables promotions based on the promotionsId of the cart.", notes = "Enables a promotion for the order based on the promotionId defined for the cart. "
			+ "Requests pertaining to promotions have been developed for the previous version of promotions and vouchers, and as a result, some of them are currently not compatible "
			+ "with the new promotions engine.", authorizations =
	{ @Authorization(value = "oauth2_client_credentials") })
	@ApiBaseSiteIdUserIdAndCartIdParam
	public void doApplyCartPromotion(@ApiParam(value = "Promotion identifier (code)", required = true)
	@RequestParam(required = true)
	final String promotionId) throws CommercePromotionRestrictionException
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("doApplyCartPromotion: promotionId = {}", sanitize(promotionId));
		}
		commercePromotionRestrictionFacade.enablePromotionForCurrentCart(promotionId);
	}

	@Secured(
	{ "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/promotions/{promotionId}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(nickname = "removeCartPromotion", value = "Disables the promotion based on the promotionsId of the cart.", notes = "Disables the promotion for the order based on the promotionId defined for the cart. "
			+ "Requests pertaining to promotions have been developed for the previous version of promotions and vouchers, and as a result, some of them are currently not compatible with "
			+ "the new promotions engine.", authorizations =
	{ @Authorization(value = "oauth2_client_credentials") })
	@ApiBaseSiteIdUserIdAndCartIdParam
	@SuppressWarnings("squid:S1160")
	public void removeCartPromotion(@ApiParam(value = "Promotion identifier (code)", required = true)
	@PathVariable
	final String promotionId) //NOSONAR
			throws CommercePromotionRestrictionException
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("removeCartPromotion: promotionId = {}", sanitize(promotionId));
		}
		commercePromotionRestrictionFacade.disablePromotionForCurrentCart(promotionId);
	}

	@Secured(
	{ "ROLE_CLIENT", "ROLE_CUSTOMERGROUP", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_GUEST" })
	@RequestMapping(value = "/{cartId}/vouchers", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getCartVouchers", value = "Get a list of vouchers applied to the cart.", notes = "Returns a list of vouchers applied to the cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public VoucherListWsDTO getCartVouchers(
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields)
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("getVouchers");
		}
		final VoucherDataList dataList = new VoucherDataList();
		dataList.setVouchers(voucherFacade.getVouchersForCart());
		return getDataMapper().map(dataList, VoucherListWsDTO.class, fields);
	}

	private boolean isApplyCouponAllowed(final CartModel cartModel)
	{
		//		if (cartModel.getStoreCreditAmount() != null && cartModel.getStoreCreditAmount().doubleValue() > 0)
		//		{
		//			return false;
		//		}
		return true;
	}

	private boolean isValidCouponLimit(final CartModel sessionCart)
	{
		if (sessionCart.getStore() == null || !sessionCart.getStore().isEnableCouponLimit()
				|| sessionCart.getAppliedCouponCodes() == null
				|| sessionCart.getAppliedCouponCodes().size() < sessionCart.getStore().getAllowedCouponApplied())
		{
			return true;
		}
		return false;
	}

	@Secured(
	{ "ROLE_CLIENT", "ROLE_CUSTOMERGROUP", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_GUEST" })
	@RequestMapping(value = "/{cartId}/vouchers", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(nickname = "doApplyCartVoucher", value = "Applies a voucher based on the voucherId defined for the cart.", notes = "Applies a voucher based on the voucherId defined for the cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	@SuppressWarnings("squid:S1160")
	public void doApplyCartVoucher(@ApiParam(value = "Voucher identifier (code)", required = true)
	@RequestParam
	final String voucherId) throws NoCheckoutCartException, VoucherOperationException
	{
		final CartModel cartModel = cartService.hasSessionCart() ? cartService.getSessionCart() : null;
		if (cartModel == null)
		{
			throw new NoCheckoutCartException("No carts created !");
		}
		if (!isValidCouponLimit(cartModel))
		{
			throw new VoucherOperationException(getMessageSource().getMessage(APPLIED_VOUCHER_EXCEEDS_LIMIT, null,
					APPLIED_VOUCHER_EXCEEDS_LIMIT, i18NService.getCurrentLocale()));
		}
		//		if (!isApplyCouponAllowed(cartModel))
		//		{
		//			throw new VoucherOperationException(getMessageSource().getMessage(APPLIED_VOUCHER_WITH_STORECREDIT, null,
		//					APPLIED_VOUCHER_WITH_STORECREDIT, i18NService.getCurrentLocale()));
		//		}
		super.applyVoucherForCartInternal(voucherId);
	}

	@Secured(
	{ "ROLE_CLIENT", "ROLE_CUSTOMERGROUP", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_GUEST" })
	@RequestMapping(value = "/{cartId}/vouchers/{voucherId}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(nickname = "removeCartVoucher", value = "Deletes a voucher defined for the current cart.", notes = "Deletes a voucher based on the voucherId defined for the current cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	@SuppressWarnings("squid:S1160")
	public void removeCartVoucher(@ApiParam(value = "Voucher identifier (code)", required = true)
	@PathVariable
	final String voucherId) //NOSONAR
			throws NoCheckoutCartException, VoucherOperationException //NOSONAR
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("release voucher : voucherCode = {}", sanitize(voucherId));
		}
		if (!getCheckoutFacade().hasCheckoutCart())
		{
			throw new NoCheckoutCartException("Cannot realese voucher. There was no checkout cart created yet!");
		}
		getVoucherFacade().releaseVoucher(voucherId);
	}

	protected void validateIfProductIsInStockInPOS(final String baseSiteId, final String productCode, final String storeName,
			final Long entryNumber)
	{
		if (!commerceStockFacade.isStockSystemEnabled(baseSiteId))
		{
			throw new StockSystemException("Stock system is not enabled on this site", StockSystemException.NOT_ENABLED, baseSiteId);
		}
		final StockData stock = commerceStockFacade.getStockDataForProductAndPointOfService(productCode, storeName);
		if (stock != null && stock.getStockLevelStatus().equals(StockLevelStatus.OUTOFSTOCK))
		{
			if (entryNumber != null)
			{
				throw new LowStockException("Product [" + sanitize(productCode) + "] is currently out of stock", //NOSONAR
						LowStockException.NO_STOCK, String.valueOf(entryNumber));
			}
			else
			{
				throw new ProductLowStockException("Product [" + sanitize(productCode) + "] is currently out of stock",
						LowStockException.NO_STOCK, productCode);
			}
		}
		else if (stock != null && stock.getStockLevelStatus().equals(StockLevelStatus.LOWSTOCK))
		{
			if (entryNumber != null)
			{
				throw new LowStockException("Not enough product in stock", LowStockException.LOW_STOCK, String.valueOf(entryNumber));
			}
			else
			{
				throw new ProductLowStockException("Not enough product in stock", LowStockException.LOW_STOCK, productCode);
			}
		}
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/payment-link/generate", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getPaymentInfo", value = "Get getPaymentInfo for the current store and delivery address.", notes = "Returns payment info", response = PaymentLinkWsDTO.class)
	@ApiBaseSiteIdUserIdAndCartIdParam
	public PaymentLinkWsDTO generatePaymentLink(
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields) throws PaymentException, OTPException
	{
		final Optional<PaymentLinkResponseData> generatedPaymentLink = paymentContext
				.getGeneratedPaymentLinkByCurrentStore(cartService.getSessionCart());

		return generatedPaymentLink.isPresent() ? getDataMapper().map(generatedPaymentLink.get(), PaymentLinkWsDTO.class) : null;
	}

	protected void validateIfProductIsInStockOnline(final String baseSiteId, final String productCode, final Long entryNumber)
	{
		if (!commerceStockFacade.isStockSystemEnabled(baseSiteId))
		{
			throw new StockSystemException("Stock system is not enabled on this site", StockSystemException.NOT_ENABLED, baseSiteId);
		}
		final StockData stock = commerceStockFacade.getStockDataForProductAndBaseSite(productCode, baseSiteId);
		if (stock != null && stock.getStockLevelStatus().equals(StockLevelStatus.OUTOFSTOCK))
		{
			if (entryNumber != null)
			{
				throw new LowStockException("Product [" + sanitize(productCode) + "] cannot be shipped - out of stock online",
						LowStockException.NO_STOCK, String.valueOf(entryNumber));
			}
			else
			{
				throw new ProductLowStockException("Product [" + sanitize(productCode) + "] cannot be shipped - out of stock online",
						LowStockException.NO_STOCK, productCode);
			}
		}
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/payment/transaction/applepay", method = RequestMethod.POST)
	@ResponseBody
	@ApiOperation(nickname = "setPaymentTransactionWithApplePay", value = "Set setPayment Transaction With ApplePay for the current store and delivery address.", notes = "Returns payment transaction")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public ApplePayPaymentResponseWsDTO setPaymentTransactionWithApplePay(
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields,
			@ApiParam(value = "Request body parameter that contains card details for apple pay", required = true)
			@RequestBody
			final ApplePayPaymentRequestWsDTO paymentDataWsDTO)
			throws PaymentException, InvalidPaymentInfoException, NoCheckoutCartException
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("setPaymentTransactionWithApplePay");
		}
		if (!getCheckoutFacade().hasCheckoutCart())
		{
			throw new NoCheckoutCartException("Cannot add Payment Transaction. There was no checkout cart created yet!");
		}
		validate(paymentDataWsDTO, "paymentDataWsDTO", applePayRequestValidator);

		getCheckoutFacade().setCCAvenueId(paymentDataWsDTO.getOrderId());

		getCheckoutFacade().resetPaymentInfo();

		final ApplePayRequest applePayRequest = createApplePayRequestDate(paymentDataWsDTO);
		final Optional<ApplePayPaymentResponseData> applePayResponseDataByCurrentBaseStore = paymentContext
				.getApplePayResponseDataByCurrentBaseStore(cartService.getSessionCart(), applePayRequest);

		if (applePayResponseDataByCurrentBaseStore.isEmpty())
		{
			throw new PaymentException("Could not get web source for Apple Pay Payment",
					PaymentExceptionType.COULD_NOT_FETCH_WEB_SOURCE);
		}

		return getDataMapper().map(applePayResponseDataByCurrentBaseStore.get(), ApplePayPaymentResponseWsDTO.class, fields);
	}

	/**
	 * @param orderId
	 * @param amount
	 * @param currency
	 * @param cardName
	 * @param paymentData
	 * @param appleCardType
	 * @param paymentOption
	 * @return
	 */
	private ApplePayRequest createApplePayRequestDate(final ApplePayPaymentRequestWsDTO applePayPaymentData)
	{
		final ApplePayRequest applePayRequest = new ApplePayRequest();

		applePayRequest.setOrderId(applePayPaymentData.getOrderId());
		applePayRequest.setCardName(applePayPaymentData.getCardName());
		String encode = null;
		try
		{
			encode = URLEncoder.encode(applePayPaymentData.getPaymentData(), "UTF-8");
		}
		catch (final UnsupportedEncodingException e)
		{
			LOG.error("Error during encoding the paymentData to URLEncoder: {}", e.getStackTrace());
		}
		applePayRequest.setPaymentData(StringUtils.isBlank(encode) ? applePayPaymentData.getPaymentData() : encode);
		applePayRequest.setAppleCardType(applePayPaymentData.getAppleCardType());
		applePayRequest.setPaymentOption(applePayPaymentData.getPaymentOption());
		applePayRequest.setCardType(applePayPaymentData.getCardType());
		return applePayRequest;
	}

	@RequestMapping(value = "/{cartId}/shipment-types", method = RequestMethod.PUT)
	@ResponseBody
	@ApiOperation(nickname = "selectShipmentType", value = "Select Shipment Type", notes = "Returns Cart modifications")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public CartModificationListWsDTO selectShipmentType(
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields, @ApiParam(value = "Shipment Type Code", allowableValues = "DELIVERY, PICKUP_IN_STORE")
			@RequestParam(required = true)
			final String code)
	{
		if (StringUtils.isBlank(code))
		{
			return new CartModificationListWsDTO();
		}
		List<CartModificationData> updateShipmentTypeForAllCartEntry = Collections.emptyList();
		try
		{
			updateShipmentTypeForAllCartEntry = getCustomCartFacade().updateShipmentTypeForAllCartEntry(ShipmentType.valueOf(code));
		}
		catch (final Exception e)
		{
			return new CartModificationListWsDTO();
		}
		if (CollectionUtils.isEmpty(updateShipmentTypeForAllCartEntry))
		{
			return new CartModificationListWsDTO();
		}
		final CartModificationDataList list = new CartModificationDataList();
		list.setCartModificationList(updateShipmentTypeForAllCartEntry);
		return getDataMapper().map(list, CartModificationListWsDTO.class);

	}

	@RequestMapping(value = "/{cartId}/shipment-types/current", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getCurrentShipmentType", value = "Current Shipment Type", notes = "Returns ShipmentTypeWsDTO")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public ShipmentTypeWsDTO getCurrentShipmentType()
	{
		return getDataMapper().map(getCustomCartFacade().getCurrentShipmentType(), ShipmentTypeWsDTO.class);
	}

	@RequestMapping(value = "/{cartId}/shipment-types", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getSupportedShipmentTypes", value = "Supported Shipment Type", notes = "Returns Cart modifications")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public ShipmentTypeListWsDTO getSupportedShipmentTypes()
	{
		final ShipmentTypeDataList list = new ShipmentTypeDataList();
		list.setSupportedShipmentTypes(getCustomCartFacade().getSupportedShipmentTypes());
		return getDataMapper().map(list, ShipmentTypeListWsDTO.class);
	}

	/**
	 * @return the paymentModeValidator
	 */
	protected Validator getPaymentModeValidator()
	{
		return paymentModeValidator;
	}



	@RequestMapping(value = "/{cartId}/addwishlistproducttocart/{wishlistPK}", method = RequestMethod.PUT)
	@ResponseBody
	@ApiOperation(nickname = "AddWishListEntriestoCart", value = "Add wishlist entries to cart.", notes = "Add wishlist entries to cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	@ResponseStatus(HttpStatus.OK)
	public CartModificationListWsDTO addAllWishlistEntriesToCart(
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields, @ApiParam(value = "Wishlist PK.", required = true)
			@PathVariable
			final String wishlistPK) throws WebserviceValidationException, CommerceCartModificationException
	{
		List<CartModificationData> cartModificationList;
		try
		{
			cartModificationList = wishlistFacade.addAllEntriesToCartWithCartModification(wishlistPK);
		}
		catch (final Exception e)
		{
			return new CartModificationListWsDTO();
		}

		if (CollectionUtils.isEmpty(cartModificationList))
		{
			return new CartModificationListWsDTO();
		}
		final CartModificationDataList list = new CartModificationDataList();
		list.setCartModificationList(cartModificationList);
		return getDataMapper().map(list, CartModificationListWsDTO.class);

	}

	@RequestMapping(value = "/{cartId}/timeslotinfo", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getCartTimeSlotInfo", value = "Cart  Time Slot Info", notes = "Cart  Time Slot Info")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public CartTimeSlotWsDTO getCartTimeSlotInfo()
	{
		final CartTimeSlotData cartTimeSlotData = customCartFacade.getCartTimeSlotDataForCurrentCart();
		return getDataMapper().map(cartTimeSlotData, CartTimeSlotWsDTO.class);
	}

}
