/**
 *
 */
package com.aldahra.aldahracommercewebservices.core.validator;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercewebservicescommons.dto.user.NationalityWsDTO;

import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.aldahra.aldahrauser.model.NationalityModel;
import com.aldahra.aldahrauser.service.MobilePhoneService;
import com.aldahra.aldahrauser.service.NationalityService;


/**
 * @author mbaker
 *
 */
public class UpdateUserValidator implements Validator
{

	private static final String INVALID_BIRTH_DATE_MESSAGE_ID = "field.register.birthDate.invalid";
	private static final String EMPTY_MARITAL_STATUS_MESSAGE_ID = "field.register.maritalStatus.empty";
	private static final String INVALID_MARITAL_STATUS_MESSAGE_ID = "field.register.maritalStatus.invalid";
	private static final String INVALID_NATIONALITY_MESSAGE_ID = "field.register.nationality.invalid";
	private static final String INVALID_NATIONALITY_ID_MESSAGE_ID = "field.register.nationalityId.invalid";
	private static final String INVALID_MOBILENUMBER_MESSAGE_ID = ".register.mobileNumber.invalid";
	private static final String INVALID_MOBILECOUNTRYCODE_ID = " field.register.mobileCountryCode.invalid";

	private static final String EMPTY_CURRENT_SITE_ERROR_MSG = "current site is null";

	private static final String NATIONALITY_KEY = "nationality";
	private static final String NATIONALITY_ID_KEY = "nationalityID";
	private static final String MARITAL_STATUS_KEY = "maritalStatus";
	private static final String BIRTH_DATE_KEY = "birthDate";


	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "nationalityService")
	private NationalityService nationalityService;



	private String nationality;

	private String nationalityId;

	private String hasNationalId;


	@Override
	public boolean supports(final Class<?> clazz)
	{
		return true;
	}

	@Override
	public void validate(final Object o, final Errors errors)
	{
		Assert.notNull(errors, "Errors object must not be null");
		final String nationalId = (String) errors.getFieldValue(this.nationalityId);
		final NationalityWsDTO nationalityWsDTO = (NationalityWsDTO) errors.getFieldValue(this.nationality);
		final boolean hasId = (boolean) errors.getFieldValue(this.hasNationalId);
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();

		validateNationality(errors, currentSite, nationalityWsDTO);
		validateNationalID(errors, currentSite, nationalId, hasId);
	}



	private void validateNationality(final Errors errors, final CMSSiteModel currentSite, final NationalityWsDTO nationalityWsDTO)
	{

		if (currentSite == null)
		{
			throw new IllegalArgumentException(EMPTY_CURRENT_SITE_ERROR_MSG);
		}

		if (Objects.isNull(nationalityWsDTO))
		{
			throw new IllegalArgumentException("nationalityWsDTO is null");

		}

		if (!currentSite.isNationalityCustomerEnabled() || !currentSite.isNationalityCustomerRequired())
		{
			return;
		}
		final String code = nationalityWsDTO.getCode();
		if (StringUtils.isEmpty(code))
		{
			errors.rejectValue(NATIONALITY_KEY, INVALID_NATIONALITY_MESSAGE_ID);
			return;
		}

		final Optional<NationalityModel> national = nationalityService.get(code);
		if (!national.isPresent())
		{
			errors.rejectValue(NATIONALITY_KEY, INVALID_NATIONALITY_MESSAGE_ID);
		}

	}

	private void validateNationalID(final Errors errors, final CMSSiteModel currentSite, final String nationalityId,
			final boolean hasId)
	{

		if (currentSite == null)
		{
			throw new IllegalArgumentException(EMPTY_CURRENT_SITE_ERROR_MSG);
		}

		if (!currentSite.isNationalityIdCustomerEnabled() || !currentSite.isNationalityIdCustomerRequired())
		{
			return;
		}

		if (hasId && StringUtils.isEmpty(nationalityId))
		{
			errors.rejectValue(NATIONALITY_ID_KEY, INVALID_NATIONALITY_ID_MESSAGE_ID);
		}

	}





	/**
	 * @return the nationality
	 */
	public String getNationality()
	{
		return nationality;
	}

	/**
	 * @param nationality
	 *           the nationality to set
	 */
	public void setNationality(final String nationality)
	{
		this.nationality = nationality;
	}

	/**
	 * @return the nationalityId
	 */
	public String getNationalityId()
	{
		return nationalityId;
	}

	/**
	 * @param nationalityId
	 *           the nationalityId to set
	 */
	public void setNationalityId(final String nationalityId)
	{
		this.nationalityId = nationalityId;
	}

	/**
	 * @return the hasNationalId
	 */
	public String getHasNationalId()
	{
		return hasNationalId;
	}

	/**
	 * @param hasNationalId
	 *           the hasNationalId to set
	 */
	public void setHasNationalId(final String hasNationalId)
	{
		this.hasNationalId = hasNationalId;
	}



}
