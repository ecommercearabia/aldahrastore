/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.aldahra.aldahracommercewebservices.core.validator;

import de.hybris.platform.commercefacades.order.data.CartData;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.aldahra.aldahraorder.cart.exception.CartValidationException;
import com.aldahra.aldahraorder.cart.exception.enums.CartExceptionType;
import com.aldahra.aldahraorder.cart.service.CartValidationService;
import com.aldahra.aldahratimeslotfacades.exception.TimeSlotException;
import com.aldahra.facades.facade.CustomCheckoutFlowFacade;


/**
 * Default commerce web services cart validator. Checks if cart is calculated and if needed values are filled.
 */
public class PlaceOrderCartValidator implements Validator
{
	@Resource(name = "cartValidationService")
	private CartValidationService cartValidationService;

	@Resource(name = "customCheckoutFlowFacade")
	private CustomCheckoutFlowFacade customCheckoutFlowFacade;

	private static final Logger LOG = Logger.getLogger(PlaceOrderCartValidator.class);


	public CustomCheckoutFlowFacade getCustomCheckoutFlowFacade()
	{
		return customCheckoutFlowFacade;
	}

	@Override
	public boolean supports(final Class<?> clazz)
	{
		return CartData.class.equals(clazz);
	}

	@Override
	public void validate(final Object target, final Errors errors)
	{
		final CartData cart = (CartData) target;

		if (!cart.isCalculated())
		{
			errors.reject("cart.notCalculated");

		}

		try
		{

			final boolean validateProfileAttributes = cartValidationService.validateProfileAttributesByCurrentCart();

		}
		catch (final CartValidationException ex)
		{
			if (CartExceptionType.MAX_AMOUNT.equals(ex.getCartExceptionType()))
			{
				final String[] errorParm = ex.getErrorParm();

				errors.reject("cart.missing.attributes.error", errorParm, "Please note, there are a missing profile info");

			}
		}

		//******* Max and Min Validation **********//
		try
		{

			final boolean validateCartMaxAmount = cartValidationService.validateCartMaxAmountByCurrentCart();

		}
		catch (final CartValidationException ex)
		{
			if (CartExceptionType.MAX_AMOUNT.equals(ex.getCartExceptionType()))
			{
				final String[] errorParm = ex.getErrorParm();


				errors.reject("max.cart.amount.value.error", errorParm, "Please note, The maximum order amount is {0} {1}");

			}
		}
		try
		{

			final boolean validateCartMaxAmount = cartValidationService.validateCartMinAmountByCurrentCart();

		}
		catch (final CartValidationException ex)
		{
			if (CartExceptionType.MIN_AMOUNT.equals(ex.getCartExceptionType()))
			{
				final String[] errorParm = ex.getErrorParm();
				errors.reject("min.cart.amount.value.error", new Object[]
				{ errorParm }, "Min order amount is {0} {1}, please add {4} {1} more to continue with checkout.");

			}
		}
		//******* End Max and Min Validation **********//

		try
		{
			if (cart != null && cart.getCartTimeSlot() != null && cart.getCartTimeSlot().isShowTimeSlot()
					&& getCustomCheckoutFlowFacade().hasNoTimeSlot())
			{
				LOG.error("PlaceOrderCartValidator - isShowTimeSlot : type [" + cart.getCartTimeSlot().isShowTimeSlot() + "]");
				LOG.error("PlaceOrderCartValidator - isShowTimeSlot : type [" + cart.getCartTimeSlot().isShowTimeSlot() + "]");

				LOG.error("Cart not Express....");
				LOG.error("Cart has No TimeSlot....");
				errors.reject("cart.timeSlotNotSet");
			}
		}
		catch (final TimeSlotException e)
		{
			LOG.error("PlaceOrderCartValidator - TimeSlotException : type [" + e.getTimeSlotExceptionType() + "] , Message ["
					+ e.getMessage() + "]");

			errors.reject("cart.timeSlotNotSet");
		}

		if (cart.getDeliveryMode() == null)
		{
			errors.reject("cart.deliveryModeNotSet");
		}
		if (cart.getPaymentMode() != null && cart.getPaymentMode().getCode().equalsIgnoreCase("card"))
		{
			if (cart.getPaymentInfo() == null)
			{
				errors.reject("cart.paymentInfoNotSet");
			}
		}
	}
}
