package com.aldahra.aldahracommercewebservices.core.validator;

import de.hybris.platform.commercefacades.user.data.CityData;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.aldahra.aldahrauserfacades.city.facade.CityFacade;


public class CityValidator implements Validator
{
	private static final String INVALID_CITYCODE_ID = " field.invalidCityCode";

	@Resource(name = "cityFacade")
	private CityFacade cityFacade;
	private String cityCode;

	private String countryIsdcode;

	/**
	 * @return the countryIsdcode
	 */
	public String getCountryIsdcode()
	{
		return countryIsdcode;
	}

	/**
	 * @param countryIsdcode
	 *           the countryIsdcode to set
	 */
	@Required
	public void setCountryIsdcode(final String countryIsdcode)
	{
		this.countryIsdcode = countryIsdcode;
	}

	/**
	 * @return the cityCode
	 */
	public String getCityCode()
	{
		return cityCode;
	}

	/**
	 * @param cityCode
	 *           the cityCode to set
	 */
	@Required
	public void setCityCode(final String cityCode)
	{
		this.cityCode = cityCode;
	}

	public boolean supports(final Class<?> aClass)
	{
		return true;
	}

	public void validate(final Object o, final Errors errors)
	{
		Assert.notNull(errors, "Errors object must not be null");
		final String cityCode = (String) errors.getFieldValue(this.cityCode);
		final String countryIsdcode = (String) errors.getFieldValue(this.countryIsdcode);

		if (!findCity(countryIsdcode, cityCode))
		{
			errors.rejectValue(this.cityCode, INVALID_CITYCODE_ID, new String[]
			{ this.cityCode }, "This field is not a valid city code.");
		}

	}

	/**
	 * @param countryIsdcode2
	 * @param cityCode2
	 * @return
	 */
	private boolean findCity(final String countryIsdcode, final String cityCode)
	{
		if (StringUtils.isBlank(countryIsdcode) || StringUtils.isBlank(cityCode))
		{
			return false;
		}
		final Optional<List<CityData>> byCountryIsocode = cityFacade.getByCountryIsocode(countryIsdcode);
		if (!byCountryIsocode.isPresent() || byCountryIsocode.get().isEmpty())
		{
			return false;
		}
		for (final CityData cityData : byCountryIsocode.get())
		{
			if (cityCode.equals(cityData.getCode()))
			{
				return true;
			}
		}
		return false;

	}


}

