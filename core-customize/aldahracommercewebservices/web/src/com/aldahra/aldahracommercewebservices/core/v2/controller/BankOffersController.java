/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahracommercewebservices.core.v2.controller;

import de.hybris.platform.commercefacades.order.data.PaymentModeData;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.order.PaymentModeService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.webservicescommons.mapping.DataMapper;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdParam;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aldahra.aldahracommercewebservices.core.order.data.BankOfferDataList;
import com.aldahra.aldahracommercewebservices.dto.site.BankOfferListWsDTO;
import com.aldahra.aldahrapaymentfacades.site.facade.BankOfferFacade;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;


@Controller
@RequestMapping(value = "/{baseSiteId}/bankoffers")
@Api(tags = "Bank Offers")
public class BankOffersController extends BaseController
{
	@Resource(name = "dataMapper")
	private DataMapper dataMapper;

	@Resource(name = "paymentModeService")
	private PaymentModeService paymentModeService;

	@Resource(name = "paymentModeConverter")
	private Converter<PaymentModeModel, PaymentModeData> paymentModeConverter;

	@Resource(name = "bankOfferFacade")
	private BankOfferFacade bankOfferFacade;

	@ResponseBody
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(nickname = "getBankOffers", value = "Get all available bank offers.", notes = "Gets all bank offers defined for the base site.")
	@ApiBaseSiteIdParam
	public BankOfferListWsDTO getBankOffers(
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields)
	{
		final BankOfferDataList bankOfferList = new BankOfferDataList();
		bankOfferList.setBankOffers(bankOfferFacade.getBankOffersByCurrentSite());
		return dataMapper.map(bankOfferList, BankOfferListWsDTO.class);
	}
}
