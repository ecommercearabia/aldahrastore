/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.aldahra.aldahracommercewebservices.core.v2.controller;

import de.hybris.platform.commercewebservicescommons.dto.wishlist.WishlistListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.wishlist.WishlistWsDTO;
import de.hybris.platform.webservicescommons.cache.CacheControl;
import de.hybris.platform.webservicescommons.cache.CacheControlDirective;
import de.hybris.platform.webservicescommons.errors.exceptions.WebserviceValidationException;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdAndUserIdParam;
import de.hybris.platform.webservicescommons.swagger.ApiFieldsParam;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.aldahra.aldahrawishlist.exception.WishlistException;
import com.aldahra.aldahrawishlistfacade.data.WishlistData;
import com.aldahra.aldahrawishlistfacade.data.WishlistDataList;
import com.aldahra.aldahrawishlistfacade.facade.WishlistFacade;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;


@Controller
@RequestMapping(value = "/{baseSiteId}/users/{userId}/wishlists")
@CacheControl(directive = CacheControlDirective.PRIVATE)
@Api(tags = "Wishlist")
public class WishlistController extends BaseCommerceController
{
	private static final Logger LOG = LoggerFactory.getLogger(WishlistController.class);

	@Resource(name = "wishlistFacade")
	private WishlistFacade wishlistFacade;

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getWishlists", value = "Get customer's wishlists", notes = "Returns customer's wishlists.")
	@ApiBaseSiteIdAndUserIdParam
	@ApiResponse(code = 200, message = "List of customer's wishlists")
	public WishlistListWsDTO getWishlists(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields) throws WishlistException
	{
		final Optional<List<WishlistData>> lists = wishlistFacade.getWishLists();
		if (lists.isPresent() && !CollectionUtils.isEmpty(lists.get()))
		{
			final WishlistDataList dto = new WishlistDataList();
			dto.setWishlists(lists.get());
			return getDataMapper().map(dto, WishlistListWsDTO.class, fields);
		}
		return new WishlistListWsDTO();
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/default", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getDefaultWishlist", value = "Get customer's default wishlist", notes = "Returns customer's default wishlist.")
	@ApiBaseSiteIdAndUserIdParam
	@ApiResponse(code = 200, message = "Customer's default wishlist")
	public WishlistWsDTO getDefaultWishlist(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields) throws WishlistException
	{
		final Optional<WishlistData> wishList = wishlistFacade.getDefaultWishList();
		if (wishList.isPresent())
		{
			return getDataMapper().map(wishList.get(), WishlistWsDTO.class, fields);
		}
		return null;
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{wishlistPK}/{productCode}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(nickname = "addWishlistEntry", value = "Add wishlist entry", notes = "Updates the wishlist. Adds a new entry.")
	@ApiBaseSiteIdAndUserIdParam
	public void addWishlistEntry(@ApiParam(value = "Wishlist PK.", required = true)
	@PathVariable
	final String wishlistPK, @ApiParam(value = "Product code.", required = true)
	@PathVariable
	final String productCode, final HttpServletRequest request) throws WebserviceValidationException //NOSONAR
			, WishlistException
	{
		wishlistFacade.addWishlistEntry(wishlistPK, productCode, null, null, null);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{wishlistPK}/{productCode}", method = RequestMethod.DELETE)
	@ApiOperation(nickname = "removeAddress", value = "Remove wishlist entry.", notes = "Removes wishlist entry.")
	@ApiBaseSiteIdAndUserIdParam
	@ResponseStatus(HttpStatus.OK)
	public void removeWishlistEntry(@ApiParam(value = "Wishlist PK.", required = true)
	@PathVariable
	final String wishlistPK, @ApiParam(value = "Product code.", required = true)
	@PathVariable
	final String productCode) throws WishlistException
	{
		wishlistFacade.removeWishlistEntryForProduct(wishlistPK, productCode);
	}


	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{wishlistPK}", method = RequestMethod.DELETE)
	@ApiOperation(nickname = "removeAllWishlistEntries", value = "Remove wishlist entries.", notes = "Removes wishlist entries.")
	@ApiBaseSiteIdAndUserIdParam
	@ResponseStatus(HttpStatus.OK)
	public void removeWishlistEntries(@ApiParam(value = "Wishlist PK.", required = true)
	@PathVariable
	final String wishlistPK) throws WishlistException
	{
		wishlistFacade.removeAllWishlistEntries(wishlistPK);
	}



}
