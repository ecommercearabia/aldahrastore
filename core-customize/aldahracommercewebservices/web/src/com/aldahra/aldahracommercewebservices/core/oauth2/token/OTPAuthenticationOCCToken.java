/**
 *
 */
package com.aldahra.aldahracommercewebservices.core.oauth2.token;

import java.util.Collection;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

/**
 * @author monzer
 *
 */
public class OTPAuthenticationOCCToken extends AbstractAuthenticationToken
{

	Object email;
	Object otpCode;

	public OTPAuthenticationOCCToken(final Object email, final Object otpCode)
	{
		super(null);
		this.email = email;
		this.otpCode = otpCode;
	}

	public OTPAuthenticationOCCToken(final Object email, final Object otpCode,
			final Collection<? extends GrantedAuthority> authorities)
	{
		super(authorities);
		this.email = email;
		this.otpCode = otpCode;
		super.setAuthenticated(true);
	}

	@Override
	public Object getCredentials()
	{
		return otpCode;
	}

	@Override
	public Object getPrincipal()
	{
		return email;
	}

}
