package com.aldahra.aldahraotp.service.impl;

import java.util.Optional;

import javax.annotation.Resource;

import com.aldahra.aldahraotp.context.OTPProviderContext;
import com.aldahra.aldahraotp.etisalat.service.EtisalatService;
import com.aldahra.aldahraotp.exception.OTPException;
import com.aldahra.aldahraotp.exception.enums.OTPExceptionType;
import com.aldahra.aldahraotp.model.EtisalatOTPProviderModel;
import com.aldahra.aldahraotp.model.OTPProviderModel;
import com.aldahra.aldahraotp.model.TwilioOTPProviderModel;
import com.aldahra.aldahraotp.service.OTPService;


/**
 * @author mnasro
 * @author abu-muhasien
 * @author monzer
 *
 *         The Class DefaultEtisalatOTPService.
 */
public class DefaultEtisalatOTPService implements OTPService
{

	/** The Etisalat service. */
	@Resource(name = "etisalatService")
	private EtisalatService etisalatService;

	/**
	 * @return the etisalatService
	 */
	public EtisalatService getEtisalatService()
	{
		return etisalatService;
	}

	/** The otp provider context. */
	@Resource(name = "otpProviderContext")
	private OTPProviderContext otpProviderContext;

	/**
	 * Send OTP code.
	 *
	 * @param countryCode
	 *           the country code
	 * @param mobileNumber
	 *           the mobile number
	 * @return true, if successful
	 * @throws OTPException
	 *            the OTP exception
	 */
	@Override
	public boolean sendOTPCode(final String countryCode, final String mobileNumber) throws OTPException
	{

		return false;
	}

	/**
	 * Verify code.
	 *
	 * @param countryCode
	 *           the country code
	 * @param mobileNumber
	 *           the mobile number
	 * @param code
	 *           the code
	 * @return true, if successful
	 * @throws OTPException
	 *            the OTP exception
	 */
	@Override
	public boolean verifyCode(final String countryCode, final String mobileNumber, final String code) throws OTPException
	{
		return false;
	}


	/**
	 * Gets the otp provider context.
	 *
	 * @return the otp provider context
	 */
	protected OTPProviderContext getOtpProviderContext()
	{
		return otpProviderContext;
	}

	@Override
	public boolean sendSMSMessage(final String to, final String message) throws OTPException
	{
		final Optional<OTPProviderModel> provider = getOtpProviderContext().getProvider(TwilioOTPProviderModel.class);
		if (!provider.isPresent())
		{
			throw new OTPException(OTPExceptionType.OTP_CONFIG_UNAVAILABLE, "provider not found");
		}
		final EtisalatOTPProviderModel etisalatOTPProviderModel = (EtisalatOTPProviderModel) provider.get();

		final String authorizationToken = getEtisalatService().getAuthorizationToken(etisalatOTPProviderModel.getUsername(),
				etisalatOTPProviderModel.getPassword());
		return getEtisalatService().sendSMSMessage(to, etisalatOTPProviderModel.getSenderAddress(), message, authorizationToken);

	}

	@Override
	public String sendOrderConfirmationWhatsappMessage(final String orderId, final String mobileNumber) throws OTPException
	{
		throw new OTPException(OTPExceptionType.SERVICE_UNAVAILABLE, "Whatsapp Message through Etisalat is not supported");
	}

	@Override
	public String sendOrderDeliveredWhatsappMessage(final String orderId, final String mobileNumber) throws OTPException
	{
		throw new OTPException(OTPExceptionType.SERVICE_UNAVAILABLE, "Whatsapp Message through Etisalat is not supported");
	}

	@Override
	public String sendOrderShipmentWhatsappMessage(final String orderId, final String mobileNumber) throws OTPException
	{
		throw new OTPException(OTPExceptionType.SERVICE_UNAVAILABLE, "Whatsapp Message through Etisalat is not supported");
	}

	@Override
	public String sendOrderCancellationWhatsappMessage(final String orderId, final String mobileNumber) throws OTPException
	{
		throw new OTPException(OTPExceptionType.SERVICE_UNAVAILABLE, "Whatsapp Message through Etisalat is not supported");
	}
}


