/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraotp.service.impl;

import de.hybris.platform.commerceservices.customer.TokenInvalidatedException;
import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.commerceservices.security.SecureTokenService;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;

import com.aldahra.aldahraotp.dao.OTPVerificationTokenDao;
import com.aldahra.aldahraotp.enums.OTPVerificationTokenType;
import com.aldahra.aldahraotp.model.OTPVerificationTokenModel;
import com.aldahra.aldahraotp.service.OTPVerificationTokenService;
import com.google.common.base.Preconditions;


/**
 *
 */
public class DefaultOTPVerificationTokenService implements OTPVerificationTokenService
{
	private static final String TYPE_MUSTN_T_BE_NULL = "type mustn't be null";

	private static final String DATA_MUSTN_T_BE_NULL = "data mustn't be null";

	private static final String CUSTOMER_MUSTN_T_BE_NULL = "Customer mustn't be null";

	private static final String TOKEN_MUSTN_T_BE_NULL = "Token mustn't be null";

	private static final String COUNTRY_ISOCODE_MUSTN_T_BE_NULL = "countryisoCode mustn't be null";
	private static final String MOBILENUMBER_MUSTN_T_BE_NULL = "mobileNumber mustn't be null";





	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "secureTokenService")
	private SecureTokenService secureTokenService;

	@Resource(name = "otpVerificationTokenDao")
	private OTPVerificationTokenDao otpVerificationTokenDao;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	private long tokenValiditySeconds;



	private static final Logger LOG = Logger.getLogger(DefaultOTPVerificationTokenService.class);

	@Override
	public Optional<OTPVerificationTokenModel> generateToken(final OTPVerificationTokenType type, final Object data,
			final String countryisoCode, final String mobileNumber,
			final CustomerModel customerModel)
	{
		Preconditions.checkArgument(type != null, TYPE_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(data != null, DATA_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(customerModel != null, CUSTOMER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNotBlank(countryisoCode), COUNTRY_ISOCODE_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNotBlank(mobileNumber), MOBILENUMBER_MUSTN_T_BE_NULL);


		final OTPVerificationTokenModel otpVerificationTokenModel = getModelService().create(OTPVerificationTokenModel.class);
		final String tokenCreated = createToken(data, customerModel);
		otpVerificationTokenModel.setToken(tokenCreated);
		otpVerificationTokenModel.setData(data);
		otpVerificationTokenModel.setMobileCountry(countryisoCode);
		otpVerificationTokenModel.setMobileNumber(mobileNumber);
		otpVerificationTokenModel.setType(type);

		if (checkIsCustomer(customerModel))
		{
			getModelService().refresh(customerModel);
			Map<OTPVerificationTokenType, String> otpVerificationToken = customerModel.getOtpVerificationToken();
			if (otpVerificationToken == null)
			{
				otpVerificationToken = new HashMap<>();
			}
			otpVerificationToken.put(type, tokenCreated);
			customerModel.setOtpVerificationToken(otpVerificationToken);
			getModelService().save(customerModel);
		}

		getModelService().save(otpVerificationTokenModel);
		return Optional.ofNullable(otpVerificationTokenModel);
	}

	/**
	 *
	 */
	private String createToken(final Object data, final CustomerModel customerModel)
	{
		final String secureTokenData = checkIsCustomer(customerModel) ? customerModel.getUid() : data.toString();
		final long timeStamp = getTokenValiditySeconds() > 0L ? new Date().getTime() : 0L;
		final SecureToken secureToken = new SecureToken(secureTokenData, timeStamp);
		return getSecureTokenService().encryptData(secureToken);
	}

	/**
	 *
	 */
	private boolean checkIsCustomer(final CustomerModel customerModel)
	{
		final String sessionAlive = (String) sessionService.getAttribute("SESSION_ALIVE");

		return !userService.isAnonymousUser(customerModel) && !StringUtils.isEmpty(sessionAlive);

	}

	@Override
	public Optional<OTPVerificationTokenModel> getToken(final String token)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(token), TOKEN_MUSTN_T_BE_NULL);
		return getOtpVerificationTokenDao().getToken(token);
	}

	@Override
	public boolean verifyToken(final String token, final CustomerModel customerModel) throws TokenInvalidatedException
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(token), TOKEN_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(customerModel != null, CUSTOMER_MUSTN_T_BE_NULL);

		final OTPVerificationTokenModel otpVerificationTokenModel = validateOTPVerificationToken(token, customerModel);

		return otpVerificationTokenModel != null;
	}

	/**
	 *
	 */
	private String getCustomerToken(final OTPVerificationTokenType type,
			final Map<OTPVerificationTokenType, String> customerOTPVerificationToken)
	{

		return (CollectionUtils.isEmpty(customerOTPVerificationToken) || type == null) ? null
				: customerOTPVerificationToken.get(customerOTPVerificationToken);
	}

	/**
	 *
	 */
	protected OTPVerificationTokenModel validateOTPVerificationToken(final String token, final CustomerModel customerModel)
			throws TokenInvalidatedException
	{
		final Optional<OTPVerificationTokenModel> otpVerificationTokenOptional = getToken(token);

		if (otpVerificationTokenOptional.isEmpty() || otpVerificationTokenOptional.get().getToken() == null)
		{
			throw new TokenInvalidatedException("token[" + token + "] is not found!");
		}

		final SecureToken data = getSecureTokenService().decryptData(token);
		if (getTokenValiditySeconds() > 0L)
		{
			final long delta = new Date().getTime() - data.getTimeStamp();
			if (delta / 1000 > getTokenValiditySeconds())
			{
				removeToken(token);
				throw new TokenInvalidatedException("token[" + token + "] is  expired");
			}
		}

		if (checkIsCustomer(customerModel))
		{
			final CustomerModel customer = getUserService().getUserForUID(data.getData(), CustomerModel.class);
			if (customer == null || CollectionUtils.isEmpty(customer.getOtpVerificationToken()))
			{
				throw new TokenInvalidatedException("user for token[" + token + "] is not found");
			}

			final String customerToken = getCustomerToken(otpVerificationTokenOptional.get().getType(),
					customer.getOtpVerificationToken());

			if (customerToken == null || !token.equals(customerToken))
			{
				throw new TokenInvalidatedException();
			}
		}


		return otpVerificationTokenOptional.get();
	}

	@Override
	public void removeToken(final String token)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(token), TOKEN_MUSTN_T_BE_NULL);

		final Optional<OTPVerificationTokenModel> otpVerificationTokenOptional = getToken(token);

		if (otpVerificationTokenOptional.isEmpty() || otpVerificationTokenOptional.get().getToken() == null)
		{
			return;
		}

		getModelService().remove(otpVerificationTokenOptional.get());
	}

	@Override
	public void removeToken(final String token, final CustomerModel customerModel)
	{
		if (checkIsCustomer(customerModel) && customerModel != null
				&& !CollectionUtils.isEmpty(customerModel.getOtpVerificationToken()))
		{
			final Map<OTPVerificationTokenType, String> otpVerificationToken = customerModel.getOtpVerificationToken();

			otpVerificationToken.entrySet().removeIf(e -> token.equalsIgnoreCase(e.getValue()));

			customerModel.setOtpVerificationToken(otpVerificationToken);

			getModelService().save(customerModel);

		}
		removeToken(token);
	}
	/**
	 * @param tokenValiditySeconds
	 *           the tokenValiditySeconds to set
	 */
	public void setTokenValiditySeconds(final long tokenValiditySeconds)
	{
		if (tokenValiditySeconds < 0)
		{
			throw new IllegalArgumentException("tokenValiditySeconds has to be >= 0");
		}
		this.tokenValiditySeconds = tokenValiditySeconds;
	}

	/**
	 * @return the tokenValiditySeconds
	 */
	public long getTokenValiditySeconds()
	{
		return tokenValiditySeconds;
	}

	/**
	 * @return the secureTokenService
	 */
	public SecureTokenService getSecureTokenService()
	{
		return secureTokenService;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @return the otpVerificationTokenDao
	 */
	public OTPVerificationTokenDao getOtpVerificationTokenDao()
	{
		return otpVerificationTokenDao;
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	@Override
	public Optional<OTPVerificationTokenModel> generateToken(final OTPVerificationTokenType type, final Object data,
			final String countryisoCode, final CustomerModel customerModel)
	{
		Preconditions.checkArgument(type != null, TYPE_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(data != null, DATA_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(customerModel != null, CUSTOMER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNotBlank(countryisoCode), COUNTRY_ISOCODE_MUSTN_T_BE_NULL);


		final OTPVerificationTokenModel otpVerificationTokenModel = getModelService().create(OTPVerificationTokenModel.class);
		final String tokenCreated = createToken(data, customerModel);
		otpVerificationTokenModel.setToken(tokenCreated);
		otpVerificationTokenModel.setData(data);
		otpVerificationTokenModel.setMobileCountry(countryisoCode);
		otpVerificationTokenModel.setMobileNumber("");
		otpVerificationTokenModel.setType(type);

		if (checkIsCustomer(customerModel))
		{
			getModelService().refresh(customerModel);
			Map<OTPVerificationTokenType, String> otpVerificationToken = customerModel.getOtpVerificationToken();
			if (otpVerificationToken == null)
			{
				otpVerificationToken = new HashMap<>();
			}
			otpVerificationToken.put(type, tokenCreated);
			customerModel.setOtpVerificationToken(otpVerificationToken);
			getModelService().save(customerModel);
		}

		getModelService().save(otpVerificationTokenModel);
		return Optional.ofNullable(otpVerificationTokenModel);
	}



}

