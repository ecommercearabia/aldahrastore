package com.aldahra.aldahraotp.service.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;

import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.aldahra.aldahraotp.dao.OTPProviderDao;
import com.aldahra.aldahraotp.model.OTPProviderModel;
import com.aldahra.aldahraotp.service.OTPProviderService;
import com.google.common.base.Preconditions;


/**
 * @author mnasro
 *
 *         The Class DefaultOTPProviderService.
 */
public class DefaultOTPProviderService implements OTPProviderService
{

	/** The otp provider dao map. */
	@Resource(name = "otpProviderDaoMap")
	private Map<Class<?>, OTPProviderDao> otpProviderDaoMap;

	/**
	 * Gets the OTP provider dao map.
	 *
	 * @return the OTP provider dao map
	 */
	protected Map<Class<?>, OTPProviderDao> getOTPProviderDaoMap()
	{
		return otpProviderDaoMap;
	}

	/** The Constant CODE_MUSTN_T_BE_NULL. */
	private static final String CODE_MUSTN_T_BE_NULL = "code mustn't be null or empty";

	/** The Constant CMSSITE_UID_MUSTN_T_BE_NULL. */
	private static final String CMSSITE_UID_MUSTN_T_BE_NULL = "cmsSiteUid mustn't be null or empty";

	/** The Constant CMSSITE_MUSTN_T_BE_NULL. */
	private static final String CMSSITE_MUSTN_T_BE_NULL = "cmsSiteModel mustn't be null or empty";

	/** The Constant PROVIDER_CLASS_MUSTN_T_BE_NULL. */
	private static final String PROVIDER_CLASS_MUSTN_T_BE_NULL = "providerClass mustn't be null";

	/** The Constant PROVIDER_DAO_NOT_FOUND. */
	private static final String PROVIDER_DAO_NOT_FOUND = "dao not found";

	/**
	 * Gets the.
	 *
	 * @param code
	 *           the code
	 * @param providerClass
	 *           the provider class
	 * @return the optional
	 */
	@Override
	public Optional<OTPProviderModel> get(final String code, final Class<?> providerClass)
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(code), CODE_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<OTPProviderDao> dao = getDao(providerClass);
		Preconditions.checkArgument(dao.isPresent(), PROVIDER_DAO_NOT_FOUND);

		return dao.get().get(code);
	}

	/**
	 * Gets the active.
	 *
	 * @param cmsSiteUid
	 *           the cms site uid
	 * @param providerClass
	 *           the provider class
	 * @return the active
	 */
	@Override
	public Optional<OTPProviderModel> getActive(final String cmsSiteUid, final Class<?> providerClass)
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(cmsSiteUid), CMSSITE_UID_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<OTPProviderDao> dao = getDao(providerClass);
		Preconditions.checkArgument(dao.isPresent(), PROVIDER_DAO_NOT_FOUND);

		return dao.get().getActive(cmsSiteUid);
	}

	/**
	 * Gets the active.
	 *
	 * @param cmsSiteModel
	 *           the cms site model
	 * @param providerClass
	 *           the provider class
	 * @return the active
	 */
	@Override
	public Optional<OTPProviderModel> getActive(final CMSSiteModel cmsSiteModel, final Class<?> providerClass)
	{
		Preconditions.checkArgument(cmsSiteModel != null, CMSSITE_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<OTPProviderDao> dao = getDao(providerClass);
		Preconditions.checkArgument(dao.isPresent(), PROVIDER_DAO_NOT_FOUND);

		return dao.get().getActive(cmsSiteModel);
	}

	/**
	 * Gets the active provider by current site.
	 *
	 * @param providerClass
	 *           the provider class
	 * @return the active provider by current site
	 */
	@Override
	public Optional<OTPProviderModel> getActiveProviderByCurrentSite(final Class<?> providerClass)
	{
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<OTPProviderDao> dao = getDao(providerClass);
		Preconditions.checkArgument(dao.isPresent(), PROVIDER_DAO_NOT_FOUND);

		return dao.get().getActiveProviderByCurrentSite();
	}

	/**
	 * Gets the dao.
	 *
	 * @param providerClass
	 *           the provider class
	 * @return the dao
	 */
	protected Optional<OTPProviderDao> getDao(final Class<?> providerClass)
	{
		final OTPProviderDao dao = getOTPProviderDaoMap().get(providerClass);

		return Optional.ofNullable(dao);
	}

}
