/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraotp.service.impl;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aldahra.aldahraotp.botsociety.service.BotSocietyService;
import com.aldahra.aldahraotp.context.OTPProviderContext;
import com.aldahra.aldahraotp.exception.OTPException;
import com.aldahra.aldahraotp.exception.enums.OTPExceptionType;
import com.aldahra.aldahraotp.model.BotSocietyProviderModel;
import com.aldahra.aldahraotp.model.OTPProviderModel;
import com.aldahra.aldahraotp.service.OTPService;
import com.google.common.base.Preconditions;


/**
 * @author monzer
 */
public class DefaultBotSocietyMessageService implements OTPService
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultBotSocietyMessageService.class);

	private static final String CUSTOMER_NAME_MUST_NOT_BE_NULL_OR_EMPTY = "Customer name must not be nul";
	private static final String TO_NUMBER_MUST_NOT_BE_NULL_OR_EMPTY = "To number must not be nul";
	private static final String CAMPIGN_ID_MUST_NOT_BE_NULL_OR_EMPTY = "CampaignId must not be nul";
	private static final String ORDER_ID_MUST_NOT_BE_NULL_OR_EMPTY = "Order Id must not be nul";
	private static final String DATE_MUST_NOT_BE_NULL_OR_EMPTY = "Date must not be nul";
	private static final String TIME_MUST_NOT_BE_NULL_OR_EMPTY = "Time must not be nul";

	/** The otp provider context. */
	@Resource(name = "otpProviderContext")
	private OTPProviderContext otpProviderContext;

	@Resource(name = "botSocietyService")
	private BotSocietyService botSocietyService;

	@Override
	public boolean sendOTPCode(final String countryCode, final String mobileNumber) throws OTPException
	{
		throw new OTPException(OTPExceptionType.SERVICE_UNAVAILABLE, "Sending OTP Code through Whatsapp is not supported");
	}

	@Override
	public boolean verifyCode(final String countryCode, final String mobileNumber, final String code) throws OTPException
	{
		throw new OTPException(OTPExceptionType.SERVICE_UNAVAILABLE, "Verifying OTP Code through Whatsapp is not supported");
	}

	@Override
	public boolean sendSMSMessage(final String to, final String message) throws OTPException
	{
		throw new OTPException(OTPExceptionType.SERVICE_UNAVAILABLE, "Sending SMS Message through Whatsapp is not supported");
	}

	@Override
	public String sendOrderConfirmationWhatsappMessage(final String orderId, final String mobileNumber) throws OTPException
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(mobileNumber), TO_NUMBER_MUST_NOT_BE_NULL_OR_EMPTY);
		Preconditions.checkArgument(StringUtils.isNoneEmpty(orderId), ORDER_ID_MUST_NOT_BE_NULL_OR_EMPTY);

		final Optional<OTPProviderModel> provider = otpProviderContext.getProvider(BotSocietyProviderModel.class);
		if (provider.isEmpty())
		{
			LOG.error("DefaultBotSocietyMessageService : BotSociety Provider is not supported!");
			throw new OTPException(OTPExceptionType.OTP_CONFIG_UNAVAILABLE, "provider not found");
		}
		final BotSocietyProviderModel whatsappProvider = (BotSocietyProviderModel) provider.get();
		final String campignId = whatsappProvider.getOrderConfirmationCampaignId();
		return botSocietyService.sendOrderConfirmationMessage(campignId, orderId, mobileNumber);
	}

	@Override
	public String sendOrderDeliveredWhatsappMessage(final String orderId, final String mobileNumber) throws OTPException
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(mobileNumber), TO_NUMBER_MUST_NOT_BE_NULL_OR_EMPTY);
		Preconditions.checkArgument(StringUtils.isNoneEmpty(orderId), ORDER_ID_MUST_NOT_BE_NULL_OR_EMPTY);

		final Optional<OTPProviderModel> provider = otpProviderContext.getProvider(BotSocietyProviderModel.class);
		if (provider.isEmpty())
		{
			LOG.error("DefaultBotSocietyMessageService : BotSociety Provider is not supported!");
			throw new OTPException(OTPExceptionType.OTP_CONFIG_UNAVAILABLE, "provider not found");
		}
		final BotSocietyProviderModel whatsappProvider = (BotSocietyProviderModel) provider.get();
		final String campignId = whatsappProvider.getOrderDeliveredCampaignId();
		return botSocietyService.sendOrderDeliveredMessage(campignId, orderId, mobileNumber);
	}

	@Override
	public String sendOrderShipmentWhatsappMessage(final String orderId, final String mobileNumber) throws OTPException
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(mobileNumber), TO_NUMBER_MUST_NOT_BE_NULL_OR_EMPTY);
		Preconditions.checkArgument(StringUtils.isNoneEmpty(orderId), ORDER_ID_MUST_NOT_BE_NULL_OR_EMPTY);

		final Optional<OTPProviderModel> provider = otpProviderContext.getProvider(BotSocietyProviderModel.class);
		if (provider.isEmpty())
		{
			LOG.error("DefaultBotSocietyMessageService : BotSociety Provider is not supported!");
			throw new OTPException(OTPExceptionType.OTP_CONFIG_UNAVAILABLE, "provider not found");
		}
		final BotSocietyProviderModel whatsappProvider = (BotSocietyProviderModel) provider.get();
		final String campignId = whatsappProvider.getOrderDeliveredCampaignId();
		return botSocietyService.sendOrderShipmentMessage(campignId, orderId, mobileNumber);
	}

	@Override
	public String sendOrderCancellationWhatsappMessage(final String orderId, final String mobileNumber) throws OTPException
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(mobileNumber), TO_NUMBER_MUST_NOT_BE_NULL_OR_EMPTY);
		Preconditions.checkArgument(StringUtils.isNoneEmpty(orderId), ORDER_ID_MUST_NOT_BE_NULL_OR_EMPTY);

		final Optional<OTPProviderModel> provider = otpProviderContext.getProvider(BotSocietyProviderModel.class);
		if (provider.isEmpty())
		{
			LOG.error("DefaultBotSocietyMessageService : BotSociety Provider is not supported!");
			throw new OTPException(OTPExceptionType.OTP_CONFIG_UNAVAILABLE, "provider not found");
		}
		final BotSocietyProviderModel whatsappProvider = (BotSocietyProviderModel) provider.get();
		final String campignId = whatsappProvider.getOrderDeliveredCampaignId();
		return botSocietyService.sendOrderCancellationMessage(campignId, orderId, mobileNumber);
	}

}
