/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraotp.service;

import de.hybris.platform.cms2.model.site.CMSSiteModel;

import java.util.Optional;

import com.aldahra.aldahraotp.model.OTPProviderModel;


/**
 * @author mnasro
 *
 *         The Interface OTPProviderService.
 */
public interface OTPProviderService
{

	/**
	 * Gets the.
	 *
	 * @param code
	 *           the code
	 * @param providerClass
	 *           the provider class
	 * @return the optional
	 */
	public Optional<OTPProviderModel> get(String code, final Class<?> providerClass);

	/**
	 * Gets the active.
	 *
	 * @param cmsSiteUid
	 *           the cms site uid
	 * @param providerClass
	 *           the provider class
	 * @return the active
	 */
	public Optional<OTPProviderModel> getActive(String cmsSiteUid, final Class<?> providerClass);

	/**
	 * Gets the active.
	 *
	 * @param cmsSiteModel
	 *           the cms site model
	 * @param providerClass
	 *           the provider class
	 * @return the active
	 */
	public Optional<OTPProviderModel> getActive(CMSSiteModel cmsSiteModel, final Class<?> providerClass);

	/**
	 * Gets the active provider by current site.
	 *
	 * @param providerClass
	 *           the provider class
	 * @return the active provider by current site
	 */
	public Optional<OTPProviderModel> getActiveProviderByCurrentSite(final Class<?> providerClass);
}
