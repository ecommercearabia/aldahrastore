/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraotp.botsociety.service.impl;

import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.aldahra.aldahraotp.botsociety.service.BotSocietyService;
import com.aldahra.aldahrawebserviceapi.util.WebServiceApiUtil;
import com.google.common.base.Preconditions;

/**
 *
 */
public class DefaultBotSocietyService implements BotSocietyService
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultBotSocietyService.class);

	private static final String TO = "To";

	private static final String PARAMETERS = "Parameters";

	private static final String ORDER_ID = "order_id";

	private static final String PHONE_NUMBER = "PhoneNumber";

	private static final String CAMPAIGN_ID = "CampaignID";
	private static final String URL = "https://foodcrowd.verloop.io/api/v1/Campaign/SendMessage";
	private static final String CUSTOMER_NAME_MUST_NOT_BE_NULL_OR_EMPTY = "Customer name must not be nul";
	private static final String TO_NUMBER_MUST_NOT_BE_NULL_OR_EMPTY = "To number must not be nul";
	private static final String ORDER_ID_MUST_NOT_BE_NULL_OR_EMPTY = "Order Id must not be nul";
	private static final String CAMPIGN_ID_MUST_NOT_BE_NULL_OR_EMPTY = "CampaignId must not be nul";
	private static final String DATE_MUST_NOT_BE_NULL_OR_EMPTY = "Date must not be nul";
	private static final String TIME_MUST_NOT_BE_NULL_OR_EMPTY = "Time must not be nul";

	@Override
	public String sendOrderConfirmationMessage(final String campignId, final String orderId, final String mobileNumber)
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(campignId), CAMPIGN_ID_MUST_NOT_BE_NULL_OR_EMPTY);
		Preconditions.checkArgument(StringUtils.isNoneEmpty(mobileNumber), TO_NUMBER_MUST_NOT_BE_NULL_OR_EMPTY);
		Preconditions.checkArgument(StringUtils.isNoneEmpty(orderId), ORDER_ID_MUST_NOT_BE_NULL_OR_EMPTY);

		final Map<String, Object> orderConfirmationRequestBody = buildWhatsappMessageRequestBody(campignId, orderId,
				mobileNumber);
		final ResponseEntity<?> response = WebServiceApiUtil.httPOST(URL, orderConfirmationRequestBody, null, Map.class, 0);
		LOG.info("DefaultBotSocietyService : Order Confirmation Whatsapp message request submitted, response code is : {}",
				response.getStatusCode());
		if (response.getStatusCode().equals(HttpStatus.OK))
		{
			return "Sent Order Confirmation Whatsapp messsage successfully";
		}
		return "Failed";
	}

	@Override
	public String sendOrderDeliveredMessage(final String campignId, final String orderId, final String mobileNumber)
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(campignId), CAMPIGN_ID_MUST_NOT_BE_NULL_OR_EMPTY);
		Preconditions.checkArgument(StringUtils.isNoneEmpty(mobileNumber), TO_NUMBER_MUST_NOT_BE_NULL_OR_EMPTY);
		Preconditions.checkArgument(StringUtils.isNoneEmpty(orderId), ORDER_ID_MUST_NOT_BE_NULL_OR_EMPTY);

		final Map<String, Object> orderConfirmationRequestBody = buildWhatsappMessageRequestBody(campignId, orderId, mobileNumber);
		final ResponseEntity<?> response = WebServiceApiUtil.httPOST(URL, orderConfirmationRequestBody, null, Map.class, 0);
		LOG.info("DefaultBotSocietyService : Order Delivered Whatsapp message request submitted, response code is : {}",
				response.getStatusCode());
		if (response.getStatusCode().equals(HttpStatus.OK))
		{
			return "Sent Order Delivered Whatsapp messsage successfully";
		}
		return "Failed";
	}

	@Override
	public String sendOrderShipmentMessage(final String campignId, final String orderId, final String mobileNumber)
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(campignId), CAMPIGN_ID_MUST_NOT_BE_NULL_OR_EMPTY);
		Preconditions.checkArgument(StringUtils.isNoneEmpty(mobileNumber), TO_NUMBER_MUST_NOT_BE_NULL_OR_EMPTY);
		Preconditions.checkArgument(StringUtils.isNoneEmpty(orderId), ORDER_ID_MUST_NOT_BE_NULL_OR_EMPTY);

		final Map<String, Object> orderConfirmationRequestBody = buildWhatsappMessageRequestBody(campignId, orderId, mobileNumber);
		final ResponseEntity<?> response = WebServiceApiUtil.httPOST(URL, orderConfirmationRequestBody, null, Map.class, 0);
		LOG.info("DefaultBotSocietyService : Order Shipment Whatsapp message request submitted, response code is : {}",
				response.getStatusCode());
		if (response.getStatusCode().equals(HttpStatus.OK))
		{
			return "Sent Order Shipment Whatsapp messsage successfully";
		}
		return "Failed";
	}

	@Override
	public String sendOrderCancellationMessage(final String campignId, final String orderId, final String mobileNumber)
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(campignId), CAMPIGN_ID_MUST_NOT_BE_NULL_OR_EMPTY);
		Preconditions.checkArgument(StringUtils.isNoneEmpty(mobileNumber), TO_NUMBER_MUST_NOT_BE_NULL_OR_EMPTY);
		Preconditions.checkArgument(StringUtils.isNoneEmpty(orderId), ORDER_ID_MUST_NOT_BE_NULL_OR_EMPTY);

		final Map<String, Object> orderConfirmationRequestBody = buildWhatsappMessageRequestBody(campignId, orderId, mobileNumber);
		final ResponseEntity<?> response = WebServiceApiUtil.httPOST(URL, orderConfirmationRequestBody, null, Map.class, 0);
		LOG.info("DefaultBotSocietyService : Order Cancellation Whatsapp message request submitted, response code is : {}",
				response.getStatusCode());
		if (response.getStatusCode().equals(HttpStatus.OK))
		{
			return "Sent Order Cancellation Whatsapp messsage successfully";
		}
		return "Failed";
	}

	private Map<String, Object> buildWhatsappMessageRequestBody(final String campignId, final String orderId,
			final String mobileNumber)
	{
		final Map<String, Object> requestBody = new HashedMap();
		requestBody.put(CAMPAIGN_ID, campignId);

		final Map<String, Object> to = new HashedMap();
		to.put(PHONE_NUMBER, mobileNumber);
		requestBody.put(TO, to);

		final Map<String, Object> parameters = new HashedMap();
		parameters.put(ORDER_ID, orderId);
		requestBody.put(PARAMETERS, parameters);

		return requestBody;
	}

}
