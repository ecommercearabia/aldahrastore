package com.aldahra.aldahraotp.exception;

import com.aldahra.aldahraotp.exception.enums.OTPExceptionType;


/**
 * @author mnasro
 *
 *         The Class OTPException.
 */
public class OTPException extends Exception
{

	/** The otp exception type. */
	private final OTPExceptionType type;

	/**
	 * Instantiates a new OTP exception.
	 *
	 * @param type
	 *           the otp exception type
	 * @param message
	 *           the message
	 */
	public OTPException(final OTPExceptionType type, final String message)
	{
		super(message);
		this.type = type;
	}

	/**
	 * Gets the OTP exception type.
	 *
	 * @return the OTPExceptionType
	 */
	public OTPExceptionType geType()
	{
		return type;
	}

}
