/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraotp.context.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.aldahra.aldahraotp.context.OTPContext;
import com.aldahra.aldahraotp.context.OTPProviderContext;
import com.aldahra.aldahraotp.entity.SessionData;
import com.aldahra.aldahraotp.entity.SmsForm;
import com.aldahra.aldahraotp.exception.OTPException;
import com.aldahra.aldahraotp.model.BotSocietyProviderModel;
import com.aldahra.aldahraotp.model.OTPProviderModel;
import com.aldahra.aldahraotp.strategy.OTPStrategy;
import com.aldahra.aldahrauser.service.MobilePhoneService;
import com.aldahra.core.enums.ShipmentType;
import com.aldahra.core.event.PaymentLinkNotificationEvent;
import com.google.common.base.Preconditions;


/**
 * The Class DefaultOTPContext.
 *
 * @author mnasro
 * @author monzer
 */
public class DefaultOTPContext implements OTPContext
{
	private static final Logger LOG = Logger.getLogger(DefaultOTPContext.class);

	/** The Constant OTP_STRATEGY_NOT_FOUND. */
	private static final String OTP_STRATEGY_NOT_FOUND = "strategy not found";
	private static final String DEFAULT_ORDER_DELIVERY_CONFIRMATION_SMS_MESSAGE = "Dear {0}, Your order number #{1} has been delivered.";
	private static final String DEFAULT_ORDER_SHIPPING_CONFIRMATION_SMS_MESSAGE = "Dear {0}, Your order number #{1} is out for delivery.For inquiries, please contact our customer support team at 80036632.";
	private static final String DEFAULT_ORDER_CANCELLATION_SMS_MESSAGE = "Your order has been canceled, Your order number is {0}";
	private static final String DEFAULT_ORDER_CONFIRMATION_SMS_MESSAGE = "Dear {0}, Your order #{1} is received.\n"
			+ "For more info, please contact our customer support team at 80036632.";


	/** The otp provider context. */
	@Resource(name = "otpProviderContext")
	private OTPProviderContext otpProviderContext;
	@Resource(name = "sessionService")
	private SessionService sessionService;
	@Resource(name = "modelService")
	private ModelService modelService;
	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;

	@Resource(name = "eventService")
	private EventService eventService;

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	/**
	 * Gets the otp provider context.
	 *
	 * @return the otp provider context
	 */
	protected OTPProviderContext getOtpProviderContext()
	{
		return otpProviderContext;
	}

	/** The payment strategy map. */
	@Resource(name = "otpStrategyMap")
	private Map<Class<?>, OTPStrategy> otpStrategyMap;


	protected Map<Class<?>, OTPStrategy> getOtpStrategyMap()
	{
		return otpStrategyMap;
	}

	/**
	 * Gets the strategy.
	 *
	 * @param providerClass
	 *           the provider class
	 * @return the strategy
	 */
	protected Optional<OTPStrategy> getStrategy(final Class<?> providerClass)
	{
		final OTPStrategy strategy = getOtpStrategyMap().get(providerClass);
		Preconditions.checkArgument(strategy != null, OTP_STRATEGY_NOT_FOUND);

		return Optional.ofNullable(strategy);
	}


	/**
	 * Send OTP code.
	 *
	 * @param countryisoCode
	 *           the countryiso code
	 * @param mobileNumber
	 *           the mobile number
	 * @param otpProviderModel
	 *           the otp provider model
	 * @throws OTPException
	 *            the OTP exception
	 */
	@Override
	public void sendOTPCode(final String countryisoCode, final String mobileNumber, final OTPProviderModel otpProviderModel)
			throws OTPException
	{
		getStrategy(otpProviderModel.getClass()).get().sendOTPCode(countryisoCode, mobileNumber, otpProviderModel);
	}

	/**
	 * Send OTP code.
	 *
	 * @param countryisoCode
	 *           the countryiso code
	 * @param mobileNumber
	 *           the mobile number
	 * @param cmsSiteModel
	 *           the cms site model
	 * @throws OTPException
	 *            the OTP exception
	 */
	@Override
	public void sendOTPCode(final String countryisoCode, final String mobileNumber, final CMSSiteModel cmsSiteModel)
			throws OTPException
	{
		final Optional<OTPProviderModel> OTPProviderModel = getOtpProviderContext().getProvider(cmsSiteModel);
		Preconditions.checkArgument(OTPProviderModel.isPresent(), "OTPProviderModel is null");
		sendOTPCode(countryisoCode, mobileNumber, OTPProviderModel.get());
	}

	/**
	 * Send OTP code by current site.
	 *
	 * @param countryisoCode
	 *           the countryiso code
	 * @param mobileNumber
	 *           the mobile number
	 * @throws OTPException
	 *            the OTP exception
	 */
	@Override
	public void sendOTPCodeByCurrentSite(final String countryisoCode, final String mobileNumber) throws OTPException
	{
		final Optional<OTPProviderModel> OTPProviderModel = getOtpProviderContext().getProviderByCurrentSite();
		Preconditions.checkArgument(OTPProviderModel.isPresent(), "OTPProviderModel is null");
		sendOTPCode(countryisoCode, mobileNumber, OTPProviderModel.get());
	}

	/**
	 * Verify code.
	 *
	 * @param countryisoCode
	 *           the countryiso code
	 * @param mobileNumber
	 *           the mobile number
	 * @param code
	 *           the code
	 * @param otpProviderModel
	 *           the otp provider model
	 * @return true, if successful
	 * @throws OTPException
	 *            the OTP exception
	 */
	@Override
	public boolean verifyCode(final String countryisoCode, final String mobileNumber, final String code,
			final OTPProviderModel otpProviderModel) throws OTPException
	{
		return getStrategy(otpProviderModel.getClass()).get().verifyCode(countryisoCode, mobileNumber, code, otpProviderModel);

	}

	/**
	 * Verify code.
	 *
	 * @param countryisoCode
	 *           the countryiso code
	 * @param mobileNumber
	 *           the mobile number
	 * @param code
	 *           the code
	 * @param cmsSiteModel
	 *           the cms site model
	 * @return true, if successful
	 * @throws OTPException
	 *            the OTP exception
	 */
	@Override
	public boolean verifyCode(final String countryisoCode, final String mobileNumber, final String code,
			final CMSSiteModel cmsSiteModel) throws OTPException
	{
		final Optional<OTPProviderModel> OTPProviderModel = getOtpProviderContext().getProvider(cmsSiteModel);
		Preconditions.checkArgument(OTPProviderModel.isPresent(), "OTPProviderModel is null");
		return verifyCode(countryisoCode, mobileNumber, code, OTPProviderModel.get());
	}

	/**
	 * Verify code by current site.
	 *
	 * @param countryisoCode
	 *           the countryiso code
	 * @param mobileNumber
	 *           the mobile number
	 * @param code
	 *           the code
	 * @return true, if successful
	 * @throws OTPException
	 *            the OTP exception
	 */
	@Override
	public boolean verifyCodeByCurrentSite(final String countryisoCode, final String mobileNumber, final String code)
			throws OTPException
	{
		final Optional<OTPProviderModel> OTPProviderModel = getOtpProviderContext().getProviderByCurrentSite();
		Preconditions.checkArgument(OTPProviderModel.isPresent(), "OTPProviderModel is null");
		return verifyCode(countryisoCode, mobileNumber, code, OTPProviderModel.get());
	}

	@Override
	public boolean isEnabled(final CMSSiteModel cmsSiteModel)
	{
		if (cmsSiteModel == null || cmsSiteModel.getRegistrationOTPProvider() == null
				|| StringUtils.isBlank(cmsSiteModel.getRegistrationOTPProvider()))
		{
			return false;
		}
		final Optional<OTPProviderModel> otpProviderModel = getOtpProviderContext().getProvider(cmsSiteModel);

		if (otpProviderModel.isPresent())
		{
			return true;
		}
		return false;
	}

	@Override
	public boolean isEnabledByCurrentSite()
	{
		Optional<OTPProviderModel> otpProviderModel = Optional.empty();
		try
		{
			otpProviderModel = getOtpProviderContext().getProviderByCurrentSite();
		}
		catch (final Exception e)
		{
			e.printStackTrace();
			return false;
		}
		if (otpProviderModel.isPresent())
		{
			return true;
		}
		return false;
	}

	@Override
	public void sendOTPCodeByCurrentSiteAndSessionData(final String countryisoCode, final String mobileNumber,
			final SessionData data) throws OTPException
	{
		Preconditions.checkArgument(data != null, "data is null");
		Preconditions.checkArgument(data.getData() != null, "data object is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(data.getSessionKey()), "data SessionKey is null or empty");

		sendOTPCodeByCurrentSite(countryisoCode, mobileNumber);
		getSessionService().setAttribute(data.getSessionKey(), data);
		getSessionService().setAttribute("isSend", Boolean.TRUE);

	}

	@Override
	public Optional<SessionData> getSessionData(final String sessionKey)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(sessionKey), "data SessionKey is null or empty");

		final Object data = getSessionService().getAttribute(sessionKey);
		if (data == null || !(data instanceof SessionData))
		{
			return Optional.empty();
		}
		return Optional.ofNullable((SessionData) data);
	}

	@Override
	public void removeSessionData(final String sessionKey)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(sessionKey), "data SessionKey is null or empty");

		getSessionService().removeAttribute(sessionKey);

	}

	@Override
	public boolean sendOrderConfirmationSMSMessage(final AbstractOrderModel abstractOrderModel) throws OTPException
	{
		Preconditions.checkArgument(abstractOrderModel != null, "abstractOrderModel is null");
		final CMSSiteModel cmsSiteModel = (CMSSiteModel) abstractOrderModel.getSite();
		if (cmsSiteModel == null || !cmsSiteModel.getEnableOrderConfirmationSmsMessage())
		{
			LOG.info("DefaultOTPContext : Order Confirmation SMS Message is disabled");
			return false;
		}
		final Optional<OTPProviderModel> otpProviderModel = getOtpProviderContext().getSendSMSProvider(cmsSiteModel);

		Preconditions.checkArgument(otpProviderModel.isPresent(), "otpProviderModel is null");
		try
		{
			final Map<String, String> map = new HashMap<>();
			final CustomerModel customerModel = (CustomerModel) abstractOrderModel.getUser();
			map.put("{0}", customerModel.getDisplayName());
			map.put("{1}", abstractOrderModel.getCode());

			final String orderConfirmationSmsMessage = getOrderConfirmationSmsMessage(cmsSiteModel, abstractOrderModel.isExpress());

			final String message = replace(orderConfirmationSmsMessage, map);
			final String mobileNumber = getMobileNumber(customerModel, abstractOrderModel);

			String sendSMSMessageWithDescription = null;

			abstractOrderModel.setSmsOrderConfermationRequest(new SmsForm(mobileNumber, "Food Crowd", message).toString());
			sendSMSMessageWithDescription = getStrategy(otpProviderModel.get().getClass()).get()
					.sendSMSMessageWithDescription(mobileNumber, message, otpProviderModel.get());

			abstractOrderModel.setSmsOrderConfermationResponse(sendSMSMessageWithDescription);
			modelService.save(abstractOrderModel);
			modelService.refresh(abstractOrderModel);
			return true;
		}
		catch (final Exception e)
		{
			LOG.error("Exception occurred sending SMS DefaultOTPContext::324", e);
			abstractOrderModel.setSmsOrderConfermationResponse(e.getMessage());
			modelService.save(abstractOrderModel);
			modelService.refresh(abstractOrderModel);
			return false;
		}
	}

	/**
	 *
	 */
	private String getOrderConfirmationSmsMessage(final CMSSiteModel cmsSiteModel, final boolean isExpress)
	{

		if (cmsSiteModel == null)
		{
			return DEFAULT_ORDER_CONFIRMATION_SMS_MESSAGE;
		}
		if (isExpress)
		{
			return StringUtils.isBlank(cmsSiteModel.getOrderConfirmationExpressSmsMessage()) ? DEFAULT_ORDER_CONFIRMATION_SMS_MESSAGE
					: cmsSiteModel.getOrderConfirmationExpressSmsMessage();
		}
		else
		{
			return StringUtils.isBlank(cmsSiteModel.getOrderConfirmationSmsMessage()) ? DEFAULT_ORDER_CONFIRMATION_SMS_MESSAGE
					: cmsSiteModel.getOrderConfirmationSmsMessage();
		}

	}

	private String replace(final String text, final String newString)
	{

		return text + " " + newString;

	}

	private String replace(final String message, final Map<String, String> replacements)
	{
		if (message == null || message.trim().isBlank() || replacements == null || replacements.isEmpty())
		{
			return message;
		}

		final Pattern pattern = Pattern.compile("\\{\\d*\\}");
		final Matcher matcher = pattern.matcher(message);
		final StringBuilder builder = new StringBuilder();
		int i = 0;
		while (matcher.find())
		{
			final String replacement = replacements.get(matcher.group());
			builder.append(message.substring(i, matcher.start()));
			if (replacement == null)
			{
				builder.append(matcher.group(0));
			}
			else
			{
				builder.append(replacement);
			}
			i = matcher.end();
		}
		if (builder.toString().length() - 1 != i)
		{
			builder.append(message.substring(i));
		}
		return builder.toString();
	}

	@Override
	public boolean sendOrderCancelationSMSMessage(final AbstractOrderModel abstractOrderModel) throws OTPException
	{
		Preconditions.checkArgument(abstractOrderModel != null, "abstractOrderModel is null");
		final CMSSiteModel cmsSiteModel = (CMSSiteModel) abstractOrderModel.getSite();
		if (cmsSiteModel == null || !cmsSiteModel.getEnableOrderCancellationSmsMessage())
		{
			LOG.info("DefaultOTPContext : Order Cancellation SMS Message is disabled");
			return false;
		}
		final Optional<OTPProviderModel> otpProviderModel = getOtpProviderContext().getSendSMSProvider(cmsSiteModel);

		Preconditions.checkArgument(otpProviderModel.isPresent(), "otpProviderModel is null");
		try
		{

			final String orderCancelationSmsMessage = getOrderCancelationSmsMessage(cmsSiteModel, abstractOrderModel.isExpress());


			final String message = replace(orderCancelationSmsMessage, abstractOrderModel.getCode());

			final CustomerModel customerModel = (CustomerModel) abstractOrderModel.getUser();

			final String mobileNumber = getMobileNumber(customerModel, abstractOrderModel);



			abstractOrderModel.setSmsOrderCancelationRequest(new SmsForm(mobileNumber, "Food Crowd", message).toString());
			final String sendSMSMessageWithDescription = getStrategy(otpProviderModel.get().getClass()).get()
					.sendSMSMessageWithDescription(mobileNumber, message, otpProviderModel.get());


			abstractOrderModel.setSmsOrderCancelationResponse(sendSMSMessageWithDescription);
			modelService.save(abstractOrderModel);
			modelService.refresh(abstractOrderModel);
			return true;
		}
		catch (final Exception e)
		{
			LOG.error("Exception occurred sending SMS DefaultOTPContext::392", e);
			abstractOrderModel.setSmsOrderCancelationResponse(e.getMessage());
			modelService.save(abstractOrderModel);
			modelService.refresh(abstractOrderModel);
			return false;
		}
	}

	/**
	 *
	 */
	private String getOrderCancelationSmsMessage(final CMSSiteModel cmsSiteModel, final boolean isExpress)
	{

		if (cmsSiteModel == null)
		{
			return DEFAULT_ORDER_CANCELLATION_SMS_MESSAGE;
		}
		if (isExpress)
		{
			return StringUtils.isBlank(cmsSiteModel.getOrderCancelationExpressSmsMessage()) ? DEFAULT_ORDER_CANCELLATION_SMS_MESSAGE
					: cmsSiteModel.getOrderCancelationExpressSmsMessage();
		}
		else
		{
			return StringUtils.isBlank(cmsSiteModel.getOrderCancelationSmsMessage()) ? DEFAULT_ORDER_CANCELLATION_SMS_MESSAGE
					: cmsSiteModel.getOrderCancelationSmsMessage();
		}

	}

	protected String getNormalizedMobileNumber(final String countryisocode, final String mobileNumber)
	{

		Optional<String> normalizedPhoneNumber = Optional.empty();

		if (!StringUtils.isEmpty(countryisocode) && !StringUtils.isEmpty(mobileNumber))
		{
			normalizedPhoneNumber = mobilePhoneService.validateAndNormalizePhoneNumberByIsoCode(countryisocode, mobileNumber);
		}
		return normalizedPhoneNumber.isPresent() ? normalizedPhoneNumber.get() : mobileNumber;
	}

	@Override
	public boolean sendDeliveryConfirmationSMSMessage(final ConsignmentModel consignmentModel)
	{
		Preconditions.checkArgument(consignmentModel != null, "consignmentModel is null");
		Preconditions.checkArgument(consignmentModel.getOrder() != null, "order in the consignmentModel is null");
		final CMSSiteModel cmsSiteModel = (CMSSiteModel) consignmentModel.getOrder().getSite();
		if (cmsSiteModel == null || !cmsSiteModel.getEnableOrderDeliveredSmsMessage())
		{
			LOG.info("DefaultOTPContext : Order Delivery SMS Message is disabled");
			return false;
		}
		final Optional<OTPProviderModel> otpProviderModel = getOtpProviderContext().getSendSMSProvider(cmsSiteModel);

		Preconditions.checkArgument(otpProviderModel.isPresent(), "otpProviderModel is null");
		try
		{
			String message = getOrderDeliveryConfirmationSmsMessage(cmsSiteModel, consignmentModel);

			final Map<String, String> map = new HashMap<>();
			final CustomerModel customerModel = (CustomerModel) consignmentModel.getOrder().getUser();
			map.put("{0}", customerModel.getDisplayName());
			map.put("{1}", consignmentModel.getOrder().getCode());
			message = replace(message, map);

			final String mobileNumber = getMobileNumber(customerModel, consignmentModel.getOrder());

			String sendSMSMessageWithDescription = null;
			consignmentModel.setSmsDeliveryConfirmationRequest(new SmsForm(mobileNumber, "Food Crowd", message).toString());
			sendSMSMessageWithDescription = getStrategy(otpProviderModel.get().getClass()).get()
					.sendSMSMessageWithDescription(mobileNumber, message, otpProviderModel.get());

			consignmentModel.setSmsDeliveryConfirmationResponse(sendSMSMessageWithDescription);
			modelService.save(consignmentModel);
			modelService.refresh(consignmentModel);
			return true;
		}
		catch (final Exception e)
		{
			LOG.error("Exception occurred sending SMS DefaultOTPContext", e);
			consignmentModel.setSmsDeliveryConfirmationResponse(e.getMessage());
			modelService.save(consignmentModel);
			modelService.refresh(consignmentModel);
			return false;
		}
	}

	/**
	 *
	 */
	private String getOrderDeliveryConfirmationSmsMessage(final CMSSiteModel cmsSiteModel, final ConsignmentModel consignmentModel)
	{

		final StringBuilder sb = new StringBuilder();
		if (cmsSiteModel == null)
		{
			return DEFAULT_ORDER_DELIVERY_CONFIRMATION_SMS_MESSAGE;
		}
		if (consignmentModel.getOrder() != null && consignmentModel.getOrder().isExpress())
		{
			return StringUtils.isBlank(cmsSiteModel.getOrderDeliveryConfirmationExpressSmsMessage())
					? DEFAULT_ORDER_DELIVERY_CONFIRMATION_SMS_MESSAGE
					: cmsSiteModel.getOrderDeliveryConfirmationExpressSmsMessage();
		}

		if (StringUtils.isBlank(cmsSiteModel.getOrderDeliveryConfirmationSmsMessage()))
		{
			return DEFAULT_ORDER_DELIVERY_CONFIRMATION_SMS_MESSAGE;
		}


		if (cmsSiteModel.isEnableDeliveryConfirmationUrl() && StringUtils.isNotBlank(cmsSiteModel.getDeliveryConfirmationUrl()))
		{
			final CustomerModel customerModel = (CustomerModel) consignmentModel.getOrder().getUser();
			sb.append(cmsSiteModel.getOrderDeliveryConfirmationSmsMessage());
			sb.append("\n");
			LOG.info(sb.toString());
			final String messageToAdjust = cmsSiteModel.getDeliveryConfirmationUrl().replace("{0}", customerModel.getContactEmail());
			return sb.append(messageToAdjust.replace("{1}", consignmentModel.getOrder().getCode())).toString();

		}

		return DEFAULT_ORDER_DELIVERY_CONFIRMATION_SMS_MESSAGE;


	}

	@Override
	public boolean sendShippingConfirmationSMSMessage(final ConsignmentModel consignmentModel)
	{
		Preconditions.checkArgument(consignmentModel != null, "consignmentModel is null");
		Preconditions.checkArgument(consignmentModel.getOrder() != null, "order in the consignmentModel is null");
		final CMSSiteModel cmsSiteModel = (CMSSiteModel) consignmentModel.getOrder().getSite();
		if (cmsSiteModel == null || !cmsSiteModel.getEnableShippingConfirmationSmsMessage())
		{
			LOG.info("DefaultOTPContext : Order Shipment SMS Message is disabled");
			return false;
		}
		final Optional<OTPProviderModel> otpProviderModel = getOtpProviderContext().getSendSMSProvider(cmsSiteModel);

		Preconditions.checkArgument(otpProviderModel.isPresent(), "otpProviderModel is null");
		try
		{
			String message = getOrderShippingConfirmationSmsMessage(cmsSiteModel, consignmentModel);
			/*
			 * String message = StringUtils.isBlank(cmsSiteModel.getOrderShippingConfirmationSmsMessage()) ?
			 * "Your order number {0} has been shipped" : cmsSiteModel.getOrderShippingConfirmationSmsMessage();
			 */

			final Map<String, String> map = new HashMap<>();
			final CustomerModel customerModel = (CustomerModel) consignmentModel.getOrder().getUser();
			map.put("{0}", customerModel.getDisplayName());
			map.put("{1}", consignmentModel.getOrder().getCode());
			message = replace(message, map);

			final String mobileNumber = getMobileNumber(customerModel, consignmentModel.getOrder());

			String sendSMSMessageWithDescription = null;
			consignmentModel.setSmsShippingConfirmationRequest(new SmsForm(mobileNumber, "Food Crowd", message).toString());
			sendSMSMessageWithDescription = getStrategy(otpProviderModel.get().getClass()).get()
					.sendSMSMessageWithDescription(mobileNumber, message, otpProviderModel.get());

			consignmentModel.setSmsShippingConfirmationResponse(sendSMSMessageWithDescription);
			modelService.save(consignmentModel);
			modelService.refresh(consignmentModel);
			return true;
		}
		catch (final Exception e)
		{
			LOG.error("Exception occurred sending SMS DefaultOTPContext::324", e);
			consignmentModel.setSmsShippingConfirmationResponse(e.getMessage());
			modelService.save(consignmentModel);
			modelService.refresh(consignmentModel);
			return false;
		}
	}


	/**
	 *
	 */
	private String getOrderShippingConfirmationSmsMessage(final CMSSiteModel cmsSiteModel, final ConsignmentModel consignmentModel)
	{

		if (cmsSiteModel == null)
		{
			return DEFAULT_ORDER_SHIPPING_CONFIRMATION_SMS_MESSAGE;
		}
		if (consignmentModel.getOrder() != null && consignmentModel.getOrder().isExpress())
		{
			return StringUtils.isBlank(cmsSiteModel.getOrderShippingConfirmationExpressSmsMessage())
					? DEFAULT_ORDER_SHIPPING_CONFIRMATION_SMS_MESSAGE
					: cmsSiteModel.getOrderShippingConfirmationExpressSmsMessage();
		}
		else
		{
			return StringUtils.isBlank(cmsSiteModel.getOrderShippingConfirmationSmsMessage())
					? DEFAULT_ORDER_SHIPPING_CONFIRMATION_SMS_MESSAGE
					: cmsSiteModel.getOrderShippingConfirmationSmsMessage();
		}

	}

	@Override
	public boolean sendOrderConfirmationWhatsappMessage(final AbstractOrderModel abstractOrderModel) throws OTPException
	{
		Preconditions.checkArgument(abstractOrderModel != null, "abstractOrderModel is null");
		final CMSSiteModel cmsSiteModel = (CMSSiteModel) abstractOrderModel.getSite();
		if (cmsSiteModel == null || !cmsSiteModel.getEnableOrderConfirmationWhatsappMessage())
		{
			LOG.info("DefaultOTPContext : Order Confirmation WhatsApp Message is disabled");
			return false;
		}
		final Optional<OTPProviderModel> otpProviderModel = getOtpProviderContext()
				.getSendOrderConfirmationWhatsappProvider(cmsSiteModel);
		Preconditions.checkArgument(otpProviderModel.isPresent(), "otpProviderModel is null");
		final CustomerModel customerModel = (CustomerModel) abstractOrderModel.getUser();
		final String mobileNumber = getMobileNumber(customerModel, abstractOrderModel);
		final String orderId = abstractOrderModel.getCode();
		final String whatsappMessageResponse = getStrategy(BotSocietyProviderModel.class).get()
				.sendWhatsappOrderConfirmationMessage(orderId, mobileNumber, otpProviderModel.get());
		abstractOrderModel.setOrderConfirmedWhatsappMessageResponse(whatsappMessageResponse);
		modelService.save(abstractOrderModel);
		modelService.refresh(abstractOrderModel);
		return true;
	}

	@Override
	public boolean sendOrderDeliveredWhatsappMessage(final ConsignmentModel consignmentModel) throws OTPException
	{
		Preconditions.checkArgument(consignmentModel != null, "abstractOrderModel is null");
		final CMSSiteModel cmsSiteModel = (CMSSiteModel) consignmentModel.getOrder().getSite();
		if (cmsSiteModel == null || !cmsSiteModel.getEnableOrderDeliveredWhatsappMessage())
		{
			LOG.info("DefaultOTPContext : Order Delivered WhatsApp Message is disabled");
			return false;
		}
		final Optional<OTPProviderModel> otpProviderModel = getOtpProviderContext()
				.getSendOrderConfirmationWhatsappProvider(cmsSiteModel);
		Preconditions.checkArgument(otpProviderModel.isPresent(), "otpProviderModel is null");

		final CustomerModel customerModel = (CustomerModel) consignmentModel.getOrder().getUser();
		final String mobileNumber = getMobileNumber(customerModel, consignmentModel.getOrder());
		final String orderId = consignmentModel.getOrder().getCode();
		final String whatsappMessageResponse = getStrategy(BotSocietyProviderModel.class).get()
				.sendWhatsappOrderDeliveredMessage(orderId, mobileNumber, otpProviderModel.get());
		consignmentModel.setDeliveryCompletedWhatsapMessageResponse(whatsappMessageResponse);
		modelService.save(consignmentModel);
		modelService.refresh(consignmentModel);
		return true;
	}

	@Override
	public boolean sendOrderShipmentWhatsappMessage(final ConsignmentModel consignmentModel) throws OTPException
	{
		Preconditions.checkArgument(consignmentModel != null, "abstractOrderModel is null");
		final CMSSiteModel cmsSiteModel = (CMSSiteModel) consignmentModel.getOrder().getSite();
		if (cmsSiteModel == null || !Boolean.TRUE.equals(cmsSiteModel.getEnableOrderShipmentWhatsappMessage()))
		{
			LOG.info("DefaultOTPContext : Order Shipment WhatsApp Message is disabled");
			return false;
		}
		final Optional<OTPProviderModel> otpProviderModel = getOtpProviderContext()
				.getSendOrderConfirmationWhatsappProvider(cmsSiteModel);
		Preconditions.checkArgument(otpProviderModel.isPresent(), "otpProviderModel is null");

		final CustomerModel customerModel = (CustomerModel) consignmentModel.getOrder().getUser();
		final String mobileNumber = getMobileNumber(customerModel, consignmentModel.getOrder());
		final String orderId = consignmentModel.getOrder().getCode();
		final String whatsappMessageResponse = getStrategy(BotSocietyProviderModel.class).get()
				.sendOrderShipmentWhatsappMessage(orderId, mobileNumber, otpProviderModel.get());
		consignmentModel.setShipmentWhatsapMessageResponse(whatsappMessageResponse);
		modelService.save(consignmentModel);
		modelService.refresh(consignmentModel);
		return true;
	}

	@Override
	public boolean sendOrderCancellationWhatsappMessage(final ConsignmentModel consignmentModel) throws OTPException
	{
		Preconditions.checkArgument(consignmentModel != null, "abstractOrderModel is null");
		final CMSSiteModel cmsSiteModel = (CMSSiteModel) consignmentModel.getOrder().getSite();
		if (cmsSiteModel == null || !cmsSiteModel.getEnableOrderCancellationWhatsappMessage())
		{
			LOG.info("DefaultOTPContext : Order Cancellation WhatsApp Message is disabled");
			return false;
		}
		final Optional<OTPProviderModel> otpProviderModel = getOtpProviderContext()
				.getSendOrderConfirmationWhatsappProvider(cmsSiteModel);
		Preconditions.checkArgument(otpProviderModel.isPresent(), "otpProviderModel is null");

		final CustomerModel customerModel = (CustomerModel) consignmentModel.getOrder().getUser();
		final String mobileNumber = getMobileNumber(customerModel, consignmentModel.getOrder());
		final String orderId = consignmentModel.getOrder().getCode();
		final String whatsappMessageResponse = getStrategy(BotSocietyProviderModel.class).get()
				.sendOrderCancellationWhatsappMessage(orderId, mobileNumber, otpProviderModel.get());
		consignmentModel.setCancellationWhatsapMessageResponse(whatsappMessageResponse);
		modelService.save(consignmentModel);
		modelService.refresh(consignmentModel);
		return true;
	}

	@Override
	public boolean sendPaymentLinkNotificationMessage(final AbstractOrderModel abstractOrderModel, final String invoiceId,
			final String paymentLink) throws OTPException
	{
		Preconditions.checkArgument(abstractOrderModel != null, "abstractOrderModel is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(paymentLink), "Payment Link is Empty!");
		Preconditions.checkArgument(StringUtils.isNotBlank(invoiceId), "Invoice Id is Empty");

		final CMSSiteModel cmsSiteModel = (CMSSiteModel) abstractOrderModel.getSite();
		if (cmsSiteModel == null)
		{
			LOG.info("DefaultOTPContext : Order Confirmation WhatsApp Message is disabled");
			return false;
		}
		if (cmsSiteModel.getEnablePaymentLinkSMSNotification())
		{
			sendPaymentLinkSMSMessage(cmsSiteModel, abstractOrderModel, invoiceId, paymentLink);
			LOG.info("SMS payment link notification message has been sent successfully");
		}
		if (cmsSiteModel.getEnablePaymentLinkEmailNotification())
		{
			sendPaymentLinkEmailMessage(abstractOrderModel, invoiceId, paymentLink);
			LOG.info("Email payment link notification message has been sent successfully");
		}

		return true;
	}

	/**
	 *
	 */
	private void sendPaymentLinkEmailMessage(final AbstractOrderModel abstractOrderModel, final String invoiceId,
			final String paymentLink)
	{
		LOG.info("Preparing to send payment link Email message");
		final PaymentLinkNotificationEvent event = new PaymentLinkNotificationEvent();
		event.setSite(abstractOrderModel.getSite());
		event.setLanguage(abstractOrderModel.getUser().getSessionLanguage());
		event.setBaseStore(abstractOrderModel.getStore());
		event.setCustomer((CustomerModel) abstractOrderModel.getUser());
		event.setOrder(abstractOrderModel);
		event.setPaymentLink(paymentLink);
		event.setInvoiceId(invoiceId);
		eventService.publishEvent(event);
	}

	/**
	 * @throws OTPException
	 *
	 */
	protected void sendPaymentLinkSMSMessage(final CMSSiteModel cmsSiteModel, final AbstractOrderModel abstractOrderModel,
			final String invoiceId, final String paymentLink) throws OTPException
	{
		LOG.info("Preparing to send payment link SMS message");
		final Optional<OTPProviderModel> otpProviderModel = getOtpProviderContext()
				.getPaymentLinkNotificationProvider(cmsSiteModel);
		if (otpProviderModel.isEmpty())
		{
			LOG.error("OTP Provider is empty for SMS payment link notification");
			return;
		}
		final CustomerModel customerModel = (CustomerModel) abstractOrderModel.getUser();
		final List<String> mobileNumber = getMobileNumber(cmsSiteModel, abstractOrderModel, customerModel);
		final String message = getNotificationMessage(cmsSiteModel, abstractOrderModel, invoiceId, paymentLink);
		for (final String number : mobileNumber)
		{
			getStrategy(otpProviderModel.get().getClass()).get().sendSMSMessageWithDescription(number, message,
					otpProviderModel.get());
		}
	}

	/**
	 *
	 */
	private List<String> getMobileNumber(final CMSSiteModel cmsSiteModel, final AbstractOrderModel abstractOrderModel,
			final CustomerModel customerModel)
	{
		if (cmsSiteModel == null && cmsSiteModel.getPaymentLinkSMSNotificationFor() == null)
		{
			return Arrays.asList(getMobileNumber(customerModel));
		}
		switch (cmsSiteModel.getPaymentLinkSMSNotificationFor())
		{
			case CUSTOMER:
				return Arrays.asList(getMobileNumber(customerModel));
			case DELIVERY_ADDRESS:
				return Arrays.asList(getMobileNumber(customerModel, abstractOrderModel));
			case BOTH:
				return getBothMobileNumbers(customerModel, abstractOrderModel);
		}
		return Arrays.asList(getMobileNumber(customerModel));
	}


	protected String getMobileNumber(final CustomerModel customerModel, final AbstractOrderModel abstractOrderModel)
	{
		if (ShipmentType.PICKUP_IN_STORE.equals(abstractOrderModel.getShipmentType()))
		{
			return getNormalizedMobileNumber(customerModel.getMobileCountry().getIsocode(), customerModel.getMobileNumber());
		}

		final String deliveryAddressMobileNumber = abstractOrderModel.getDeliveryAddress() != null
				&& StringUtils.isNotBlank(abstractOrderModel.getDeliveryAddress().getMobile())
						? abstractOrderModel.getDeliveryAddress().getMobile()
						: null;

		if (StringUtils.isNotBlank(deliveryAddressMobileNumber))
		{
			return getNormalizedMobileNumber(abstractOrderModel.getDeliveryAddress().getMobileCountry().getIsocode(),
					abstractOrderModel.getDeliveryAddress().getMobile());
		}
		else
		{
			return getNormalizedMobileNumber(customerModel.getMobileCountry().getIsocode(), customerModel.getMobileNumber());
		}
	}

	protected String getMobileNumber(final CustomerModel customerModel)
	{
		return getNormalizedMobileNumber(customerModel.getMobileCountry().getIsocode(), customerModel.getMobileNumber());
	}

	protected List<String> getBothMobileNumbers(final CustomerModel customerModel, final AbstractOrderModel abstractOrderModel)
	{
		final List<String> mobileNumbers = new ArrayList<>();
		final String customerMobileNumber = getMobileNumber(customerModel);
		final String deliveryAddressMobileNumber = getMobileNumber(customerModel, abstractOrderModel);
		if (StringUtils.equals(customerMobileNumber, deliveryAddressMobileNumber))
		{
			return Arrays.asList(customerMobileNumber); // Both mobile numbers are the same, so it does not matter which mobile number is the target
		}
		if (StringUtils.isNotBlank(deliveryAddressMobileNumber))
		{
			mobileNumbers.add(deliveryAddressMobileNumber);
		}
		if (StringUtils.isNotBlank(customerMobileNumber))
		{
			mobileNumbers.add(customerMobileNumber);
		}
		return mobileNumbers;
	}

	/**
	 *
	 */
	private String getNotificationMessage(final CMSSiteModel cmsSiteModel, final AbstractOrderModel abstractOrderModel,
			final String invoiceId, final String paymentLink)
	{
		Preconditions.checkArgument(
				cmsSiteModel != null && StringUtils.isNotBlank(cmsSiteModel.getPaymentLinkNotificationContent()),
				"Empty notification content for payment link notification");
		Preconditions.checkArgument(StringUtils.isNotBlank(paymentLink), "Empty payment link to be sent with the notification!");
		String message = cmsSiteModel.getPaymentLinkNotificationContent();
		final Map<String, String> map = new HashMap<String, String>();
		map.put("{0}", invoiceId);
		map.put("{1}", abstractOrderModel.getCurrency().getIsocode());
		map.put("{2}", String.valueOf(abstractOrderModel.getTotalPrice().doubleValue()));
		map.put("{3}", paymentLink);
		message = replace(message, map);

		return message;
	}
}
