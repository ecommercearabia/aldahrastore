package com.aldahra.aldahraotp.context.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;

import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;
import javax.ws.rs.NotSupportedException;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aldahra.aldahraotp.context.OTPProviderContext;
import com.aldahra.aldahraotp.model.BotSocietyProviderModel;
import com.aldahra.aldahraotp.model.EtisalatOTPProviderModel;
import com.aldahra.aldahraotp.model.OTPProviderModel;
import com.aldahra.aldahraotp.model.TwilioOTPProviderModel;
import com.aldahra.aldahraotp.strategy.OTPProviderStrategy;
import com.google.common.base.Preconditions;


/**
 * The Class DefaultOTPProviderContext.
 *
 * @author mnasro
 *
 */
public class DefaultOTPProviderContext implements OTPProviderContext
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultOTPProviderContext.class);

	/** The otp provider map. */
	@Resource(name = "otpProviderMap")
	private Map<Class<?>, OTPProviderStrategy> otpProviderMap;

	/** The Constant PROVIDER_CLASS_MUSTN_T_BE_NULL. */
	private static final String PROVIDER_CLASS_MUSTN_T_BE_NULL = "strategy mustn't be null";

	/** The Constant PROVIDER_STRATEGY_NOT_FOUND. */
	private static final String PROVIDER_STRATEGY_NOT_FOUND = "strategy not found";

	/** The Constant CMSSITE_MUSTN_T_BE_NULL. */
	private static final String CMSSITE_MUSTN_T_BE_NULL = "baseStoreModel mustn't be null";

	private static final String PROVIDER_TYPE_MUSTN_T_BE_NULL = "providerType mustn't be null";


	/** The Constant OTP_PROVIDER__MUSTN_T_BE_NULL. */
	private static final String OTP_PROVIDER__MUSTN_T_BE_NULL = "otpProvider mustn't be null";

	/** The cms site service. */
	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	/**
	 * Gets the cms site service.
	 *
	 * @return the cms site service
	 */
	protected CMSSiteService getCmsSiteService()
	{
		return cmsSiteService;
	}

	/**
	 * Gets the provider.
	 *
	 * @param providerClass
	 *           the provider class
	 * @return the provider
	 */
	@Override
	public Optional<OTPProviderModel> getProvider(final Class<?> providerClass, final CMSSiteModel cmsSiteModel)
	{
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<OTPProviderStrategy> strategy = getStrategy(providerClass);
		Preconditions.checkArgument(strategy.isPresent(), PROVIDER_STRATEGY_NOT_FOUND);

		if (!strategy.isPresent())
		{
			return Optional.empty();
		}
		return strategy.get().getActiveProvider(cmsSiteModel);
	}

	@Override
	public Optional<OTPProviderModel> getProvider(final Class<?> providerClass)
	{
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<OTPProviderStrategy> strategy = getStrategy(providerClass);
		Preconditions.checkArgument(strategy.isPresent(), PROVIDER_STRATEGY_NOT_FOUND);

		return strategy.get().getActiveProviderByCurrentSite();
	}

	/**
	 * Gets the strategy.
	 *
	 * @param providerClass
	 *           the provider class
	 * @return the strategy
	 */
	protected Optional<OTPProviderStrategy> getStrategy(final Class<?> providerClass)
	{
		final OTPProviderStrategy strategy = getOTPProviderMap().get(providerClass);

		return Optional.ofNullable(strategy);
	}

	/**
	 * Gets the OTP provider map.
	 *
	 * @return the OTP provider map
	 */
	protected Map<Class<?>, OTPProviderStrategy> getOTPProviderMap()
	{
		return otpProviderMap;
	}

	/**
	 * Gets the provider.
	 *
	 * @param cmsSiteModel
	 *           the cms site model
	 * @return the provider
	 */
	@Override
	public Optional<OTPProviderModel> getProvider(final CMSSiteModel cmsSiteModel)
	{
		Preconditions.checkArgument(cmsSiteModel != null, CMSSITE_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNotBlank(cmsSiteModel.getRegistrationOTPProvider()),
				OTP_PROVIDER__MUSTN_T_BE_NULL);
		if (StringUtils.isBlank(cmsSiteModel.getRegistrationOTPProvider()))
		{
			return Optional.empty();
		}
		switch (cmsSiteModel.getRegistrationOTPProvider().toUpperCase())
		{
			case "ETISALATOTPPROVIDER":
				return getProvider(EtisalatOTPProviderModel.class, cmsSiteModel);
			case "TWILIOOTPPROVIDER":
				return getProvider(TwilioOTPProviderModel.class, cmsSiteModel);
			default:
				return Optional.empty();
		}

	}

	@Override
	public Optional<OTPProviderModel> getProvider(final CMSSiteModel cmsSiteModel, final String providerType)
	{
		Preconditions.checkArgument(cmsSiteModel != null, CMSSITE_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNotBlank(providerType), PROVIDER_TYPE_MUSTN_T_BE_NULL);

		switch (providerType.toUpperCase())
		{
			case "ETISALATOTPPROVIDER":
				return getProvider(EtisalatOTPProviderModel.class, cmsSiteModel);
			case "TWILIOOTPPROVIDER":
				return getProvider(TwilioOTPProviderModel.class, cmsSiteModel);
			default:
				throw new NotSupportedException("providerType[" + providerType + "] is not supported ");
		}
	}

	/**
	 * Gets the provider by current site.
	 *
	 * @return the provider by current site
	 */
	@Override
	public Optional<OTPProviderModel> getProviderByCurrentSite()
	{
		return getProvider(getCmsSiteService().getCurrentSite());

	}

	@Override
	public Optional<OTPProviderModel> getSendSMSProvider(final CMSSiteModel cmsSiteModel)
	{
		Preconditions.checkArgument(cmsSiteModel != null, CMSSITE_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNotBlank(cmsSiteModel.getSendMessagesOTPProvider()),
				OTP_PROVIDER__MUSTN_T_BE_NULL);
		if (StringUtils.isBlank(cmsSiteModel.getSendMessagesOTPProvider()))
		{
			return Optional.empty();
		}
		switch (cmsSiteModel.getSendMessagesOTPProvider().toUpperCase())
		{
			case "ETISALATOTPPROVIDER":
				return getProvider(EtisalatOTPProviderModel.class, cmsSiteModel);
			case "TWILIOOTPPROVIDER":
				return getProvider(TwilioOTPProviderModel.class, cmsSiteModel);
			default:
				return Optional.empty();
		}
	}

	@Override
	public Optional<OTPProviderModel> getSendOrderConfirmationWhatsappProvider(final CMSSiteModel cmsSiteModel)
	{
		Preconditions.checkArgument(cmsSiteModel != null, CMSSITE_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNotBlank(cmsSiteModel.getSendOrderConfirmationWhatsappMessage()),
				OTP_PROVIDER__MUSTN_T_BE_NULL);
		if (StringUtils.isBlank(cmsSiteModel.getSendOrderConfirmationWhatsappMessage()))
		{
			return Optional.empty();
		}

		switch (cmsSiteModel.getSendOrderConfirmationWhatsappMessage().toUpperCase())
		{
			case "BOTSOCIETYPROVIDER":
				return getProvider(BotSocietyProviderModel.class, cmsSiteModel);
			default:
				return Optional.empty();
		}
	}

	@Override
	public Optional<OTPProviderModel> getSendOrderDeliveredWhatsappProvider(final CMSSiteModel cmsSiteModel)
	{
		Preconditions.checkArgument(cmsSiteModel != null, CMSSITE_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNotBlank(cmsSiteModel.getSendOrderDeliveredWhatsappMessage()),
				OTP_PROVIDER__MUSTN_T_BE_NULL);
		if (StringUtils.isBlank(cmsSiteModel.getSendOrderDeliveredWhatsappMessage()))
		{
			return Optional.empty();
		}
		switch (cmsSiteModel.getSendOrderDeliveredWhatsappMessage().toUpperCase())
		{
			case "BOTSOCIETYPROVIDER":
				return getProvider(BotSocietyProviderModel.class, cmsSiteModel);
			default:
				return Optional.empty();
		}
	}

	@Override
	public Optional<OTPProviderModel> getPaymentLinkNotificationProvider(final CMSSiteModel cmsSiteModel)
	{
		Preconditions.checkArgument(cmsSiteModel != null, CMSSITE_MUSTN_T_BE_NULL);
		if (StringUtils.isBlank(cmsSiteModel.getPaymentLinkNotificationProviderName()))
		{
			LOG.error("Payment link notification provider is empty");
			return Optional.empty();
		}
		switch (cmsSiteModel.getPaymentLinkNotificationProviderName().toUpperCase())
		{
			case "ETISALATOTPPROVIDER":
				return getProvider(EtisalatOTPProviderModel.class, cmsSiteModel);
			case "TWILIOOTPPROVIDER":
				return getProvider(TwilioOTPProviderModel.class, cmsSiteModel);
			default:
				return Optional.empty();
		}
	}

	@Override
	public Optional<OTPProviderModel> getProviderByCurrentSite(final String providerType)
	{
		return getProvider(getCmsSiteService().getCurrentSite(), providerType);
	}


}
