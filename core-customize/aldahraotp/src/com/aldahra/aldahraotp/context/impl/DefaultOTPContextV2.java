/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraotp.context.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commerceservices.customer.TokenInvalidatedException;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;
import javax.ws.rs.NotSupportedException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.aldahra.aldahraotp.context.OTPContextV2;
import com.aldahra.aldahraotp.context.OTPProviderContext;
import com.aldahra.aldahraotp.enums.OTPVerificationTokenType;
import com.aldahra.aldahraotp.exception.OTPException;
import com.aldahra.aldahraotp.exception.enums.OTPExceptionType;
import com.aldahra.aldahraotp.model.OTPProviderModel;
import com.aldahra.aldahraotp.model.OTPVerificationTokenModel;
import com.aldahra.aldahraotp.service.OTPVerificationTokenService;
import com.aldahra.aldahraotp.strategy.OTPStrategy;
import com.google.common.base.Preconditions;


/**
 * The Class DefaultOTPContext.
 *
 * @author mnasro
 * @author monzer
 */
public class DefaultOTPContextV2 implements OTPContextV2
{
	private static final Logger LOG = Logger.getLogger(DefaultOTPContextV2.class);
	private static final String OTP_STRATEGY_NOT_FOUND = "strategy not found";

	@Resource(name = "otpProviderContext")
	private OTPProviderContext otpProviderContext;

	@Resource(name = "otpVerificationTokenService")
	private OTPVerificationTokenService otpVerificationTokenService;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "otpStrategyMap")
	private Map<Class<?>, OTPStrategy> otpStrategyMap;


	protected Optional<OTPStrategy> getStrategy(final Class<?> providerClass)
	{
		final OTPStrategy strategy = getOtpStrategyMap().get(providerClass);
		Preconditions.checkArgument(strategy != null, OTP_STRATEGY_NOT_FOUND);

		return Optional.ofNullable(strategy);
	}

	@Override
	public boolean isEnabledByCurrentSite(final OTPVerificationTokenType type)
	{
		return isEnabled(type, getCmsSiteService().getCurrentSite());
	}

	@Override
	public boolean isEnabled(final OTPVerificationTokenType type, final CMSSiteModel cmsSiteModel)
	{
		if (cmsSiteModel == null || type == null)
		{
			return false;
		}
		final String otpProviderType = getOTPProviderType(type, cmsSiteModel);
		try
		{
			final Optional<OTPProviderModel> otpProviderModel = getOtpProviderContext().getProvider(cmsSiteModel, otpProviderType);
			return otpProviderModel.isPresent();
		}
		catch (final NotSupportedException e)
		{
			return false;
		}
	}

	private Optional<OTPProviderModel> getOTPProvider(final OTPVerificationTokenType type, final CMSSiteModel cmsSiteModel)
	{
		if (type == null)
		{
			return Optional.empty();
		}

		final String otpProviderType = getOTPProviderType(type, cmsSiteModel);

		if (otpProviderType == null)
		{
			return Optional.empty();
		}

		return getOtpProviderContext().getProvider(cmsSiteModel, otpProviderType);
	}

	/**
	 *
	 */
	private String getOTPProviderType(final OTPVerificationTokenType type, final CMSSiteModel cmsSiteModel)
	{
		if (type == null)
		{
			return null;
		}
		switch (type)
		{
			case LOGIN:
			case CHECKOUT_LOGIN:
			case REGISTRATION:
			case CHECKOUT_REGISTRATION:
			case UPDATE_PROFILE:
				return cmsSiteModel.getRegistrationOTPProvider();
			default:
				return null;

		}

	}

	@Override
	public Optional<OTPVerificationTokenModel> sendOTPCode(final OTPVerificationTokenType type, final String countryisoCode,
			final String mobileNumber, final Object data, final CustomerModel customerModel, final CMSSiteModel cmsSiteModel)
			throws OTPException
	{
		Preconditions.checkArgument(Objects.nonNull(type), "type mustn't be null");
		Preconditions.checkArgument(StringUtils.isNotBlank(countryisoCode), "countryisoCode mustn't be null");
		Preconditions.checkArgument(StringUtils.isNotBlank(mobileNumber), "mobileNumber mustn't be null");
		Preconditions.checkArgument(Objects.nonNull(data), "data mustn't be null");
		Preconditions.checkArgument(Objects.nonNull(customerModel), "customerModel mustn't be null");
		Preconditions.checkArgument(Objects.nonNull(cmsSiteModel), "cmsSiteModel mustn't be null");

		if (!isEnabled(type, cmsSiteModel))
		{
			throw new OTPException(OTPExceptionType.DISABLED, "OTP provider is disabled");
		}

		final Optional<OTPProviderModel> otpProvider = getOTPProvider(type, cmsSiteModel);

		if (otpProvider.isEmpty())
		{
			throw new OTPException(OTPExceptionType.OTP_CONFIG_UNAVAILABLE, "OTP provider config is unavailable");
		}

		final boolean sendOTPCode = getStrategy(otpProvider.get().getClass()).get().sendOTPCode(countryisoCode, mobileNumber,
				otpProvider.get());
		if (sendOTPCode)
		{
			return getOtpVerificationTokenService().generateToken(type, data, countryisoCode, mobileNumber, customerModel);
		}
		else
		{
			throw new OTPException(OTPExceptionType.SERVICE_UNAVAILABLE, "send OTP is failed - service unavailable");
		}
	}

	@Override
	public Optional<OTPVerificationTokenModel> sendOTPCodeByCurrentSite(final OTPVerificationTokenType type,
			final String countryisoCode, final String mobileNumber, final Object data, final CustomerModel customerModel)
			throws OTPException
	{
		return sendOTPCode(type, countryisoCode, mobileNumber, data, customerModel, getCmsSiteService().getCurrentSite());
	}

	@Override
	public Optional<OTPVerificationTokenModel> sendOTPCodeByCurrentSiteAndCustomer(final OTPVerificationTokenType type,
			final String countryisoCode, final String mobileNumber, final Object data) throws OTPException
	{
		return sendOTPCodeByCurrentSite(type, countryisoCode, mobileNumber, data, getCurrentCustomer());
	}

	@Override
	public boolean verifyCode(final String token, final String countryisoCode, final String mobileNumber, final String code,
			final CustomerModel customerModel, final CMSSiteModel cmsSiteModel) throws OTPException, TokenInvalidatedException
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(token), "token mustn't be null");
		Preconditions.checkArgument(StringUtils.isNotBlank(countryisoCode), "countryisoCode mustn't be null");
		Preconditions.checkArgument(StringUtils.isNotBlank(mobileNumber), "mobileNumber mustn't be null");
		Preconditions.checkArgument(StringUtils.isNotBlank(code), "code mustn't be null");
		Preconditions.checkArgument(Objects.nonNull(customerModel), "customerModel mustn't be null");
		Preconditions.checkArgument(Objects.nonNull(cmsSiteModel), "cmsSiteModel mustn't be null");

		final Optional<OTPVerificationTokenModel> otpVerificationTokenModel = getOtpVerificationTokenService().getToken(token);

		if (otpVerificationTokenModel.isEmpty())
		{
			throw new TokenInvalidatedException("token[" + token + "] is not found!");
		}

		final OTPVerificationTokenType type = otpVerificationTokenModel.get().getType();
		if (!isEnabled(type, cmsSiteModel))
		{
			throw new OTPException(OTPExceptionType.DISABLED, "OTP provider is disabled");
		}

		final Optional<OTPProviderModel> otpProvider = getOTPProvider(type, cmsSiteModel);

		if (!otpProvider.isEmpty())
		{
			throw new OTPException(OTPExceptionType.OTP_CONFIG_UNAVAILABLE, "OTP provider config is unavailable");
		}

		getOtpVerificationTokenService().verifyToken(token, customerModel);
		final boolean verifyCode = getStrategy(otpProvider.get().getClass()).get().verifyCode(countryisoCode, mobileNumber, code,
				otpProvider.get());
		if (verifyCode)
		{
			getOtpVerificationTokenService().removeToken(token);
		}
		return verifyCode;
	}

	@Override
	public boolean verifyCodeByCurrentSite(final String token, final String countryisoCode, final String mobileNumber,
			final String code, final CustomerModel customerModel) throws OTPException, TokenInvalidatedException
	{
		return verifyCode(token, countryisoCode, mobileNumber, code, customerModel, getCmsSiteService().getCurrentSite());
	}

	@Override
	public boolean verifyCodeByCurrentSiteAndCustomer(final String token, final String countryisoCode, final String mobileNumber,
			final String code) throws OTPException, TokenInvalidatedException
	{
		return verifyCodeByCurrentSite(token, countryisoCode, mobileNumber, code, getCurrentCustomer());
	}


	protected CustomerModel getCurrentCustomer()
	{
		final UserModel currentUser = getCurrentUser();

		return (currentUser == null || !(currentUser instanceof CustomerModel)) ? null : (CustomerModel) currentUser;
	}

	private UserModel getCurrentUser()
	{
		return getUserService().getCurrentUser();
	}

	protected Map<Class<?>, OTPStrategy> getOtpStrategyMap()
	{
		return otpStrategyMap;
	}

	protected OTPProviderContext getOtpProviderContext()
	{
		return otpProviderContext;
	}

	protected CMSSiteService getCmsSiteService()
	{
		return cmsSiteService;
	}

	public OTPVerificationTokenService getOtpVerificationTokenService()
	{
		return otpVerificationTokenService;
	}

	public UserService getUserService()
	{
		return userService;
	}

	@Override
	public boolean verifyToken(final String token, final CustomerModel customerModel) throws TokenInvalidatedException
	{
		return getOtpVerificationTokenService().verifyToken(token, customerModel);
	}

	@Override
	public boolean verifyTokenByCurrentCustomer(final String token) throws TokenInvalidatedException
	{
		return verifyToken(token, getCurrentCustomer());
	}

	@Override
	public Optional<OTPVerificationTokenModel> getToken(final String token, final CustomerModel customerModel)
			throws TokenInvalidatedException
	{
		return verifyToken(token, customerModel) ? getOtpVerificationTokenService().getToken(token) : Optional.empty();
	}

	@Override
	public Optional<OTPVerificationTokenModel> getTokenByCurrentCustomer(final String token) throws TokenInvalidatedException
	{
		return getToken(token, getCurrentCustomer());
	}

	@Override
	public void removeToken(final String token, final CustomerModel customerModel)
	{
		getOtpVerificationTokenService().removeToken(token);
	}

	@Override
	public void removeTokenByCurrentCustomer(final String token)
	{
		removeToken(token, getCurrentCustomer());

	}

	@Override
	public Optional<OTPVerificationTokenModel> generateToken(final OTPVerificationTokenType type, final Object data,
			final String countryisoCode, final CustomerModel customerModel)
	{
		return getOtpVerificationTokenService().generateToken(type, data, countryisoCode, customerModel);
	}

	@Override
	public Optional<OTPVerificationTokenModel> generateTokenCurrentCustomer(final OTPVerificationTokenType type, final Object data,
			final String countryisoCode)
	{
		return getOtpVerificationTokenService().generateToken(type, data, countryisoCode, getCurrentCustomer());

	}



}
