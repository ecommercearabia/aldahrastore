package com.aldahra.aldahraotp.context;

import de.hybris.platform.cms2.model.site.CMSSiteModel;

import java.util.Optional;

import com.aldahra.aldahraotp.model.OTPProviderModel;


/**
 * The Interface OTPProviderContext.
 *
 * @author mnasro
 *
 */
public interface OTPProviderContext
{

	/**
	 * Gets the provider.
	 *
	 * @param providerClass
	 *           the provider class
	 * @return the provider
	 */
	public Optional<OTPProviderModel> getProvider(Class<?> providerClass, CMSSiteModel baseStoreModel);

	public Optional<OTPProviderModel> getProvider(Class<?> providerClass);


	/**
	 * Gets the provider.
	 *
	 * @param cmsSiteModel
	 *           the cms site model
	 * @return the provider
	 */
	public Optional<OTPProviderModel> getProvider(CMSSiteModel cmsSiteModel);

	public Optional<OTPProviderModel> getProvider(final CMSSiteModel cmsSiteModel, final String providerType);

	public Optional<OTPProviderModel> getProviderByCurrentSite(final String providerType);





	/**
	 * Gets the provider.
	 *
	 * @param cmsSiteModel
	 *           the cms site model
	 * @return the provider
	 */
	public Optional<OTPProviderModel> getSendSMSProvider(CMSSiteModel cmsSiteModel);

	public Optional<OTPProviderModel> getSendOrderConfirmationWhatsappProvider(CMSSiteModel cmsSiteModel);

	public Optional<OTPProviderModel> getSendOrderDeliveredWhatsappProvider(CMSSiteModel cmsSiteModel);

	public Optional<OTPProviderModel> getPaymentLinkNotificationProvider(CMSSiteModel cmsSiteModel);

	/**
	 * Gets the provider by current site.
	 *
	 * @return the provider by current site
	 */
	public Optional<OTPProviderModel> getProviderByCurrentSite();

}
