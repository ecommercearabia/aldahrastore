/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraotp.etisalat.service.impl;

import java.io.IOException;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.aldahra.aldahraotp.entity.SmsForm;
import com.aldahra.aldahraotp.entity.UserAuth;
import com.aldahra.aldahraotp.etisalat.service.EtisalatService;
import com.aldahra.aldahraotp.exception.OTPException;
import com.aldahra.aldahraotp.exception.enums.OTPExceptionType;
import com.aldahra.aldahrawebserviceapi.util.WebServiceApiUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Preconditions;


/**
 *
 *
 * @author abu-muhasien
 */

public class DefaultEtisalatService implements EtisalatService
{

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(DefaultEtisalatService.class);

	/** The Constant AUTH_URL. */
	private final static String AUTH_URL = "https://smartmessaging.etisalat.ae:5676/login/user/";

	/** The Constant BASE_URL. */
	private final static String BASE_URL = "https://smartmessaging.etisalat.ae:5676/campaigns/submissions/sms/nb";

	/**
	 * Send SMS message.
	 *
	 * @param recipientMobileNumber
	 *           the recipient mobile number
	 * @param senderAddr
	 *           the sender addr
	 * @param message
	 *           the message
	 * @param token
	 *           the token
	 * @return the string
	 * @throws OTPException
	 *            the OTP exception
	 */
	public boolean sendSMSMessage(final String recipientMobileNumber, final String senderAddr, final String message,
			final String token) throws OTPException
	{
		validateParameters(recipientMobileNumber, senderAddr, message, token);
		ResponseEntity<?> httPOST = null;
		try
		{

			LOG.info("Sending Message in progress");
			final SmsForm form = new SmsForm();
			form.setRecipient(recipientMobileNumber);
			form.setSenderAddr(senderAddr);
			form.setMessage(message);
			form.setClientTxnId(new Random(1000000).nextLong() + "");
			final MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
			headers.add("Authorization", "Bearer " + token);
			headers.add("content-type", "application/json");
			headers.add("user-agent", "*");

			LOG.info(String.format("The Auth Token is : %s", headers.getFirst("Authorization")));
			LOG.info(String.format("Sending messing Request Body is : %s", form.toString()));
			httPOST = WebServiceApiUtil.httPOST(BASE_URL, form, headers, String.class);
			LOG.info(String.format("The Otp Response is : %s", httPOST.getBody().toString()));
			LOG.info("message sent successfully ...");
			return true;
		}
		catch (final Exception e)
		{

			LOG.error(String.format("Bad Request,Message can not be sent: %s", e.getMessage()));
			LOG.error("Bad Request,Message can not be sent");
			if (httPOST == null)
			{
				LOG.error("The HttpResponse is null");
			}
			else
			{
				LOG.error(String.format("The Response Error is ", e.getMessage()));

			}


			throw new OTPException(OTPExceptionType.MESSAGE_CAN_NOT_BE_SENT,
					String.format("Bad Request,Message can not be sent due to : ", e.getMessage()));
		}
	}

	/**
	 * Gets the authorization token.
	 *
	 * @param username
	 *           the username
	 * @param password
	 *           the password
	 * @return the authorization token
	 * @throws OTPException
	 *            the OTP exception
	 */
	public String getAuthorizationToken(final String username, final String password) throws OTPException
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(username), "username can not be null or empty.");
		Preconditions.checkArgument(StringUtils.isNoneEmpty(password), "password can not be null or empty.");

		final UserAuth auth = new UserAuth(username, password);
		LOG.info("getting Authorization Token in progress");
		final ResponseEntity<?> httPOST = WebServiceApiUtil.httPOST(AUTH_URL, auth, null, String.class);
		final ObjectMapper mapper = new ObjectMapper();
		try
		{
			final Map<String, Object> readValue = mapper.readValue(httPOST.getBody().toString(), Map.class);
			final String value = (String) readValue.get("token");
			return value == null ? "Bad Request,Invalid Authorization Token" : value;
		}
		catch (final JsonProcessingException e)
		{

			LOG.error("Invalid Authorization Token");
			throw new OTPException(OTPExceptionType.MESSAGE_CAN_NOT_BE_SENT, "Bad Request,Invalid Authorization Token");
		}
		catch (final IOException e)
		{
			LOG.error("Invalid Authorization Token");
			throw new OTPException(OTPExceptionType.MESSAGE_CAN_NOT_BE_SENT, "Bad Request,Message can not be sent");
		}

	}

	@Override
	public String sendSMSMessageWithDescription(final String recipientMobileNumber, final String senderAddr, final String message,
			final String token) throws OTPException
	{
		validateParameters(recipientMobileNumber, senderAddr, message, token);
		ResponseEntity<?> httPOST = null;
		try
		{

			LOG.info("Sending Message in progress");
			final SmsForm form = new SmsForm();
			form.setRecipient(recipientMobileNumber);
			form.setSenderAddr(senderAddr);
			form.setMessage(message);
			form.setClientTxnId(new Random(1000000).nextLong() + "");
			final MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
			headers.add("Authorization", "Bearer " + token);
			headers.add("content-type", "application/json");
			headers.add("user-agent", "*");

			LOG.info(String.format("The Auth Token is : %s", headers.getFirst("Authorization")));
			LOG.info(String.format("Sending messing Request Body is : %s", form.toString()));
			httPOST = WebServiceApiUtil.httPOST(BASE_URL, form, headers, String.class);
			LOG.info(String.format("The Otp Response is : %s", httPOST.getBody().toString()));
			LOG.info("message sent successfully ...");
			return httPOST.getBody().toString();
		}
		catch (final Exception e)
		{

			LOG.error(String.format("Bad Request,Message can not be sent: %s", e.getMessage()));
			LOG.error("Bad Request,Message can not be sent");
			if (httPOST == null)
			{
				LOG.error("The HttpResponse is null");

			}


			throw new OTPException(OTPExceptionType.MESSAGE_CAN_NOT_BE_SENT,
					String.format("Bad Request,Message can not be sent due to : ", e.getMessage()));
		}
	}

	private void validateParameters(final String recipientMobileNumber, final String senderAddr, final String message,
			final String token)
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(recipientMobileNumber),
				"recipientMobileNumber can not be null or empty.");
		Preconditions.checkArgument(StringUtils.isNoneEmpty(senderAddr), "Sender Address can not be null or empty.");
		Preconditions.checkArgument(StringUtils.isNoneEmpty(message), "Message can not be null or empty.");
		Preconditions.checkArgument(StringUtils.isNoneEmpty(token), "Token can not be null or empty.");
	}
}
