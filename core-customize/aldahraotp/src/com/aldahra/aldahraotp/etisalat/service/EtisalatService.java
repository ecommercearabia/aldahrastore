/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraotp.etisalat.service;

import com.aldahra.aldahraotp.exception.OTPException;


/**
 * @author abu-muhasien.
 */
public interface EtisalatService
{

	/**
	 * Send SMS message.
	 *
	 * @param recipientMobileNumber
	 *           the recipient mobile number
	 * @param senderAddr
	 *           the sender addr
	 * @param message
	 *           the message
	 * @param token
	 *           the token
	 * @return the string
	 * @throws OTPException
	 *            the OTP exception
	 */
	public boolean sendSMSMessage(final String recipientMobileNumber, final String senderAddr, final String message, String token)
			throws OTPException;

	public String sendSMSMessageWithDescription(final String recipientMobileNumber, final String senderAddr, final String message,
			String token) throws OTPException;




	/**
	 * Gets the authorization token.
	 *
	 * @param username
	 *           the username
	 * @param password
	 *           the password
	 * @return the authorization token
	 * @throws OTPException
	 *            the OTP exception
	 */
	public String getAuthorizationToken(final String username, final String password) throws OTPException;

}
