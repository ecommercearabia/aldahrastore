/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraotp.strategy;

import de.hybris.platform.cms2.model.site.CMSSiteModel;

import java.util.Optional;

import com.aldahra.aldahraotp.model.OTPProviderModel;


/**
 * @author mnasro
 *
 *         The Interface OTPProviderStrategy.
 */
public interface OTPProviderStrategy
{

	/**
	 * Gets the active provider.
	 *
	 * @param cmsSiteUid
	 *           the cms site uid
	 * @return the active provider
	 */
	public Optional<OTPProviderModel> getActiveProvider(String cmsSiteUid);

	/**
	 * Gets the active provider.
	 *
	 * @param cmsSiteModel
	 *           the cms site model
	 * @return the active provider
	 */
	public Optional<OTPProviderModel> getActiveProvider(CMSSiteModel cmsSiteModel);

	/**
	 * Gets the active provider by current site.
	 *
	 * @return the active provider by current site
	 */
	public Optional<OTPProviderModel> getActiveProviderByCurrentSite();

}
