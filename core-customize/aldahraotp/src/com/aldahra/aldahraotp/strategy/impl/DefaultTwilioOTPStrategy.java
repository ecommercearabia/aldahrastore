/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraotp.strategy.impl;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.aldahra.aldahraotp.exception.OTPException;
import com.aldahra.aldahraotp.exception.enums.OTPExceptionType;
import com.aldahra.aldahraotp.model.OTPProviderModel;
import com.aldahra.aldahraotp.model.TwilioOTPProviderModel;
import com.aldahra.aldahraotp.strategy.OTPStrategy;
import com.aldahra.aldahraotp.twilio.service.TwilioService;
import com.google.common.base.Preconditions;


/**
 *
 */
public class DefaultTwilioOTPStrategy implements OTPStrategy
{
	/** The Constant MESSAGE_CAN_NOT_BE_NULL_OR_EMPTY. */
	private static final String MESSAGE_CAN_NOT_BE_NULL_OR_EMPTY = "message can not be null or empty";

	/** The Constant MESSAGING_SERVICE_SID_CAN_NOT_BE_NULL_OR_EMPTY. */
	private static final String MESSAGING_SERVICE_SID_CAN_NOT_BE_NULL_OR_EMPTY = "messagingServiceSid can not be null or empty";

	/** The Constant TO_NUMBER_MUST_NOT_BE_NULL_OR_EMPTY. */
	private static final String TO_NUMBER_MUST_NOT_BE_NULL_OR_EMPTY = "To Number Must not be null or empty";
	/** The Constant APIKEY_MUSTN_T_BE_NULL. */
	private static final String APIKEY_MUSTN_T_BE_NULL = "apiKey mustn't be null or empty";
	/** The Constant ACCOUNT_SID_MUSTN_T_BE_NULL. */
	private static final String ACCOUNT_SID_MUSTN_T_BE_NULL = "accountSid mustn't be null or empty";

	@Resource(name = "twilioService")
	private TwilioService twilioService;

	@Override
	public boolean sendOTPCode(final String countryisoCode, final String mobileNumber, final OTPProviderModel otpProviderModel)
			throws OTPException
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(countryisoCode), "countryisoCode cannot be null or empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(mobileNumber), "mobileNumber cannot be null or empty");
		Preconditions.checkArgument(otpProviderModel != null, "otpProviderModel cannot be null");
		Preconditions.checkArgument(otpProviderModel instanceof TwilioOTPProviderModel,
				"otpProviderModel cannot cast to TwilioOTPProviderModel");
		final TwilioOTPProviderModel providerModel = (TwilioOTPProviderModel) otpProviderModel;
		return twilioService.sendOTPCode(providerModel.getAuthToken(), providerModel.getApiKey(), providerModel.getAccountSid(),
				countryisoCode, mobileNumber);
	}

	@Override
	public boolean verifyCode(final String countryisoCode, final String mobileNumber, final String code,
			final OTPProviderModel otpProviderModel) throws OTPException
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(countryisoCode), "countryisoCode cannot be null or empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(mobileNumber), "mobileNumber cannot be null or empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(code), "code cannot be null or empty");
		Preconditions.checkArgument(otpProviderModel != null, "otpProviderModel cannot be null");
		Preconditions.checkArgument(otpProviderModel instanceof TwilioOTPProviderModel,
				"otpProviderModel cannot cast to TwilioOTPProviderModel");
		final TwilioOTPProviderModel providerModel = (TwilioOTPProviderModel) otpProviderModel;

		return twilioService.verifyCode(providerModel.getAuthToken(), providerModel.getApiKey(), providerModel.getAccountSid(),
				countryisoCode, mobileNumber, code);
	}

	@Override
	public boolean sendSMSMessage(final String mobileNumber, final String message, final OTPProviderModel otpProviderModel)
			throws OTPException
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(mobileNumber), TO_NUMBER_MUST_NOT_BE_NULL_OR_EMPTY);
		Preconditions.checkArgument(StringUtils.isNoneEmpty(message), MESSAGE_CAN_NOT_BE_NULL_OR_EMPTY);
		Preconditions.checkArgument(otpProviderModel != null, "Provider con not be null");

		final TwilioOTPProviderModel providerModel = (TwilioOTPProviderModel) otpProviderModel;
		return twilioService.sendSMSMessage(providerModel.getAuthToken(), providerModel.getAccountSid(), mobileNumber,
				providerModel.getMessagingServiceSid(), message);
	}

	@Override
	public String sendSMSMessageWithDescription(final String mobileNumber, final String message,
			final OTPProviderModel otpProviderModel) throws OTPException
	{
		throw new UnsupportedOperationException("The method is not supported");
	}

	@Override
	public String sendWhatsappOrderConfirmationMessage(final String orderId, final String mobileNumber,
			final OTPProviderModel otpProviderModel) throws OTPException
	{
		throw new OTPException(OTPExceptionType.SERVICE_UNAVAILABLE, "Whatsapp Message through Twiliow is not supported");
	}

	@Override
	public String sendWhatsappOrderDeliveredMessage(final String orderId, final String mobileNumber,
			final OTPProviderModel otpProviderModel) throws OTPException
	{
		throw new OTPException(OTPExceptionType.SERVICE_UNAVAILABLE, "Whatsapp Message through Twiliow is not supported");
	}

	@Override
	public String sendOrderShipmentWhatsappMessage(final String orderId, final String mobileNumber,
			final OTPProviderModel provider) throws OTPException
	{
		throw new OTPException(OTPExceptionType.SERVICE_UNAVAILABLE, "Whatsapp Message through Twiliow is not supported");
	}

	@Override
	public String sendOrderCancellationWhatsappMessage(final String orderId, final String mobileNumber,
			final OTPProviderModel provider) throws OTPException
	{
		throw new OTPException(OTPExceptionType.SERVICE_UNAVAILABLE, "Whatsapp Message through Twiliow is not supported");
	}

}
