/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraotp.strategy.impl;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.aldahra.aldahraotp.botsociety.service.BotSocietyService;
import com.aldahra.aldahraotp.exception.OTPException;
import com.aldahra.aldahraotp.exception.enums.OTPExceptionType;
import com.aldahra.aldahraotp.model.BotSocietyProviderModel;
import com.aldahra.aldahraotp.model.OTPProviderModel;
import com.aldahra.aldahraotp.strategy.OTPStrategy;
import com.google.common.base.Preconditions;


/**
 *
 */
public class DefaultBotSocietyStrategy implements OTPStrategy
{

	/**
	 *
	 */
	private static final String PROVIDER_CON_NOT_BE_NULL = "Provider con not be null";
	private static final String CUSTOMER_NAME_MUST_NOT_BE_NULL_OR_EMPTY = "Customer name must not be nul";
	private static final String TO_NUMBER_MUST_NOT_BE_NULL_OR_EMPTY = "To number must not be nul";
	private static final String CAMPIGN_ID_MUST_NOT_BE_NULL_OR_EMPTY = "CampaignId must not be nul";
	private static final String ORDER_ID_MUST_NOT_BE_NULL_OR_EMPTY = "Order Id must not be nul";
	private static final String DATE_MUST_NOT_BE_NULL_OR_EMPTY = "Date must not be nul";;
	private static final String TIME_MUST_NOT_BE_NULL_OR_EMPTY = "Time must not be nul";;

	@Resource(name = "botSocietyService")
	private BotSocietyService botSocietyService;

	@Override
	public boolean sendOTPCode(final String countryisoCode, final String mobileNumber, final OTPProviderModel otpProviderModel)
			throws OTPException
	{
		throw new OTPException(OTPExceptionType.SERVICE_UNAVAILABLE, "Sending OTP Code through Whatsapp is not supported");
	}

	@Override
	public boolean verifyCode(final String countryisoCode, final String mobileNumber, final String code,
			final OTPProviderModel otpProviderModel) throws OTPException
	{
		throw new OTPException(OTPExceptionType.SERVICE_UNAVAILABLE, "Verifying OTP Code through Whatsapp is not supported");
	}

	@Override
	public boolean sendSMSMessage(final String mobileNumber, final String message, final OTPProviderModel otpProviderModel)
			throws OTPException
	{
		throw new OTPException(OTPExceptionType.SERVICE_UNAVAILABLE, "Sending SMS Message through Whatsapp is not supported");
	}

	@Override
	public String sendSMSMessageWithDescription(final String mobileNumber, final String message,
			final OTPProviderModel otpProviderModel) throws OTPException
	{
		throw new OTPException(OTPExceptionType.SERVICE_UNAVAILABLE, "Sending SMS Message through Whatsapp is not supported");
	}

	@Override
	public String sendWhatsappOrderConfirmationMessage(final String orderId, final String mobileNumber,
			final OTPProviderModel otpProviderModel) throws OTPException
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(orderId), ORDER_ID_MUST_NOT_BE_NULL_OR_EMPTY);
		Preconditions.checkArgument(StringUtils.isNoneEmpty(mobileNumber), TO_NUMBER_MUST_NOT_BE_NULL_OR_EMPTY);
		Preconditions.checkArgument(otpProviderModel != null, PROVIDER_CON_NOT_BE_NULL);
		final BotSocietyProviderModel whatsappProvider = (BotSocietyProviderModel) otpProviderModel;
		final String campignId = whatsappProvider.getOrderConfirmationCampaignId();
		return botSocietyService.sendOrderConfirmationMessage(campignId, orderId, mobileNumber);
	}

	@Override
	public String sendWhatsappOrderDeliveredMessage(final String orderId, final String mobileNumber,
			final OTPProviderModel otpProviderModel) throws OTPException
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(orderId), ORDER_ID_MUST_NOT_BE_NULL_OR_EMPTY);
		Preconditions.checkArgument(StringUtils.isNoneEmpty(mobileNumber), TO_NUMBER_MUST_NOT_BE_NULL_OR_EMPTY);
		Preconditions.checkArgument(otpProviderModel != null, PROVIDER_CON_NOT_BE_NULL);

		final BotSocietyProviderModel whatsappProvider = (BotSocietyProviderModel) otpProviderModel;

		final String campignId = whatsappProvider.getOrderDeliveredCampaignId();
		return botSocietyService.sendOrderDeliveredMessage(campignId, orderId, mobileNumber);
	}

	@Override
	public String sendOrderShipmentWhatsappMessage(final String orderId, final String mobileNumber,
			final OTPProviderModel provider) throws OTPException
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(orderId), ORDER_ID_MUST_NOT_BE_NULL_OR_EMPTY);
		Preconditions.checkArgument(StringUtils.isNoneEmpty(mobileNumber), TO_NUMBER_MUST_NOT_BE_NULL_OR_EMPTY);
		Preconditions.checkArgument(provider != null, PROVIDER_CON_NOT_BE_NULL);

		final BotSocietyProviderModel whatsappProvider = (BotSocietyProviderModel) provider;

		final String campignId = whatsappProvider.getOrderShipmentCampaignId();
		return botSocietyService.sendOrderShipmentMessage(campignId, orderId, mobileNumber);
	}

	@Override
	public String sendOrderCancellationWhatsappMessage(final String orderId, final String mobileNumber,
			final OTPProviderModel provider) throws OTPException
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(orderId), ORDER_ID_MUST_NOT_BE_NULL_OR_EMPTY);
		Preconditions.checkArgument(StringUtils.isNoneEmpty(mobileNumber), TO_NUMBER_MUST_NOT_BE_NULL_OR_EMPTY);
		Preconditions.checkArgument(provider != null, PROVIDER_CON_NOT_BE_NULL);

		final BotSocietyProviderModel whatsappProvider = (BotSocietyProviderModel) provider;

		final String campignId = whatsappProvider.getOrderCancellationCampaignId();
		return botSocietyService.sendOrderCancellationMessage(campignId, orderId, mobileNumber);
	}

}
