package com.aldahra.aldahraotp.strategy;

import com.aldahra.aldahraotp.exception.OTPException;
import com.aldahra.aldahraotp.model.OTPProviderModel;


/**
 * @author mnasro
 * @author abu-muhasien
 * @author monzer
 *
 *         The Interface PaymentStrategy.
 */
public interface OTPStrategy
{

	public boolean sendOTPCode(final String countryisoCode, final String mobileNumber, final OTPProviderModel otpProviderModel)
			throws OTPException;


	public boolean verifyCode(final String countryisoCode, final String mobileNumber, final String code,
			final OTPProviderModel otpProviderModel) throws OTPException;

	public boolean sendSMSMessage(String mobileNumber, String message, final OTPProviderModel otpProviderModel)
			throws OTPException;

	public String sendSMSMessageWithDescription(String mobileNumber, String message, final OTPProviderModel otpProviderModel)
			throws OTPException;

	public String sendWhatsappOrderConfirmationMessage(String orderId, String mobileNumber,
			final OTPProviderModel otpProviderModel)
			throws OTPException;

	public String sendWhatsappOrderDeliveredMessage(String orderId, String mobileNumber,
			final OTPProviderModel otpProviderModel)
			throws OTPException;

	public String sendOrderShipmentWhatsappMessage(String orderId, String mobileNumber, OTPProviderModel provider)
			throws OTPException;

	public String sendOrderCancellationWhatsappMessage(String orderId, String mobileNumber, OTPProviderModel provider)
			throws OTPException;

}
