/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraotp.twilio.service.impl;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aldahra.aldahraotp.exception.OTPException;
import com.aldahra.aldahraotp.exception.enums.OTPExceptionType;
import com.aldahra.aldahraotp.twilio.service.TwilioService;
import com.authy.AuthyApiClient;
import com.authy.AuthyException;
import com.authy.api.Params;
import com.authy.api.Verification;
import com.google.common.base.Preconditions;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;


// TODO: Auto-generated Javadoc
/**
 * The Class DefaultTwilioService.
 *
 * @author mnasro
 * @author abu-muhasien
 *
 *         The Class DefaultTwilioService.
 */
public class DefaultTwilioService implements TwilioService
{

	/** The Constant MESSAGE_CAN_NOT_BE_NULL_OR_EMPTY. */
	private static final String MESSAGE_CAN_NOT_BE_NULL_OR_EMPTY = "message can not be null or empty";

	/** The Constant MESSAGING_SERVICE_SID_CAN_NOT_BE_NULL_OR_EMPTY. */
	private static final String MESSAGING_SERVICE_SID_CAN_NOT_BE_NULL_OR_EMPTY = "messagingServiceSid can not be null or empty";

	/** The Constant TO_NUMBER_MUST_NOT_BE_NULL_OR_EMPTY. */
	private static final String TO_NUMBER_MUST_NOT_BE_NULL_OR_EMPTY = "To Number Must not be null or empty";
	/** The Constant APIKEY_MUSTN_T_BE_NULL. */
	private static final String APIKEY_MUSTN_T_BE_NULL = "apiKey mustn't be null or empty";

	/** The Constant ACCOUNT_SID_MUSTN_T_BE_NULL. */
	private static final String ACCOUNT_SID_MUSTN_T_BE_NULL = "accountSid mustn't be null or empty";

	/** The Constant COUNTRY_CODE_MUSTN_T_BE_NULL. */
	private static final String COUNTRY_CODE_MUSTN_T_BE_NULL = "countryCode mustn't be null or empty";

	/** The Constant MESSAGE_TO_MUSTN_T_BE_NULL. */
	private static final String MESSAGE_TO_MUSTN_T_BE_NULL = "messageTo mustn't be null or empty";

	/** The Constant COUNTRY_NOT_FOUND. */
	private static final String COUNTRY_NOT_FOUND = "country not found";

	/** The Constant TWILIO_SUSPENDED_ACCOUNT_RESPONSE. */
	private static final String TWILIO_SUSPENDED_ACCOUNT_RESPONSE = "Your account is suspended.";

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(DefaultTwilioService.class);

	/** The common I 18 N service. */
	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	/**
	 * Gets the common I 18 N service.
	 *
	 * @return the common I 18 N service
	 */
	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	/**
	 * Send OTP code.
	 *
	 * @param authToken
	 *           the auth token
	 * @param apiKey
	 *           the api key
	 * @param accountSid
	 *           the account sid
	 * @param countryCode
	 *           the country code
	 * @param messageTo
	 *           the message to
	 * @return true, if successful
	 * @throws OTPException
	 *            the OTP exception
	 */
	@Override
	public boolean sendOTPCode(final String authToken, final String apiKey, final String accountSid, final String countryCode,
			final String messageTo) throws OTPException
	{
		final CountryModel countryModel = validateArgs(apiKey, accountSid, countryCode, messageTo);
		final String locale = getCommonI18NService().getCurrentLanguage() == null ? "en"
				: getCommonI18NService().getCurrentLanguage().getIsocode();

		Twilio.init(accountSid, authToken);
		final AuthyApiClient authyApiClient = new AuthyApiClient(apiKey);
		final Params params = new Params();
		params.setAttribute("code_length", "4");
		params.setAttribute("locale", locale);
		Verification verification = null;
		try
		{
			verification = authyApiClient.getPhoneVerification().start(messageTo, countryModel.getIsdcode(), "sms", params);
		}
		catch (final AuthyException ex)
		{
			LOG.error("Error verifying token. {0}", ex.getMessage());
			throw new OTPException(OTPExceptionType.SERVICE_UNAVAILABLE,
					TWILIO_SUSPENDED_ACCOUNT_RESPONSE + " Please recharge your account.");
		}

		if (verification.isOk())
		{
			LOG.info(String.format("Verification code sent successfuly to: " + messageTo));
			return true;
		}
		else
		{
			LOG.error(String.format("Error verifying token. {0}", verification.getMessage()));
			if (TWILIO_SUSPENDED_ACCOUNT_RESPONSE.equalsIgnoreCase(verification.getMessage()))
			{
				throw new OTPException(OTPExceptionType.SERVICE_UNAVAILABLE,
						TWILIO_SUSPENDED_ACCOUNT_RESPONSE + " Please recharge your account.");
			}
			throw new OTPException(OTPExceptionType.SERVICE_UNAVAILABLE, verification.getMessage());
		}

	}

	/**
	 * Verify code.
	 *
	 * @param authToken
	 *           the auth token
	 * @param apiKey
	 *           the api key
	 * @param accountSid
	 *           the account sid
	 * @param countryCode
	 *           the country code
	 * @param messageTo
	 *           the message to
	 * @param code
	 *           the code
	 * @return true, if successful
	 * @throws OTPException
	 *            the OTP exception
	 */
	@Override
	public boolean verifyCode(final String authToken, final String apiKey, final String accountSid, final String countryCode,
			final String messageTo, final String code) throws OTPException
	{
		final CountryModel countryModel = validateArgs(apiKey, accountSid, countryCode, messageTo);

		Twilio.init(accountSid, authToken);
		final AuthyApiClient authyApiClient = new AuthyApiClient(apiKey);
		Verification verification;
		try
		{
			verification = authyApiClient.getPhoneVerification().check(messageTo, countryModel.getIsdcode(), code);


			if (!verification.isOk())
			{
				LOG.error("Error verifying token. {0}", verification.getMessage());
				if (TWILIO_SUSPENDED_ACCOUNT_RESPONSE.equalsIgnoreCase(verification.getMessage()))
				{
					throw new OTPException(OTPExceptionType.SERVICE_UNAVAILABLE,
							TWILIO_SUSPENDED_ACCOUNT_RESPONSE + " Please recharge your account.");
				}
				throw new OTPException(OTPExceptionType.SERVICE_UNAVAILABLE, verification.getMessage());
			}
		}
		catch (final AuthyException ex)
		{
			LOG.error("Error verifying token. {0}", ex.getMessage());
			throw new OTPException(OTPExceptionType.SERVICE_UNAVAILABLE,
					TWILIO_SUSPENDED_ACCOUNT_RESPONSE + " Please recharge your account.");
		}
		LOG.info("Verification is completed successfuly to: {0}", messageTo);
		return true;

	}

	/**
	 * Validate args.
	 *
	 * @param apiKey
	 *           the api key
	 * @param accountSid
	 *           the account sid
	 * @param countryCode
	 *           the country code
	 * @param messageTo
	 *           the message to
	 * @return the optional
	 */
	protected CountryModel validateArgs(final String apiKey, final String accountSid, final String countryCode,
			final String messageTo)
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(apiKey), APIKEY_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNoneEmpty(accountSid), ACCOUNT_SID_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNoneEmpty(countryCode), COUNTRY_CODE_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNoneEmpty(messageTo), MESSAGE_TO_MUSTN_T_BE_NULL);
		final CountryModel countryModel = getCommonI18NService().getCountry(countryCode);
		Preconditions.checkArgument(countryModel != null, COUNTRY_NOT_FOUND);
		return countryModel;
	}

	/**
	 * Send SMS message.
	 *
	 * @param authToken
	 *           the auth token
	 * @param accountSid
	 *           the account sid
	 * @param to
	 *           the to
	 * @param messagingServiceSid
	 *           the messaging service sid
	 * @param message
	 *           the message
	 * @return true, if successful
	 * @throws OTPException
	 *            the OTP exception
	 */
	@Override
	public boolean sendSMSMessage(final String authToken, final String accountSid, final String to,
			final String messagingServiceSid, final String message) throws OTPException
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(accountSid), ACCOUNT_SID_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNoneEmpty(to), TO_NUMBER_MUST_NOT_BE_NULL_OR_EMPTY);
		Preconditions.checkArgument(StringUtils.isNoneEmpty(messagingServiceSid), MESSAGING_SERVICE_SID_CAN_NOT_BE_NULL_OR_EMPTY);
		Preconditions.checkArgument(StringUtils.isNoneEmpty(message), MESSAGE_CAN_NOT_BE_NULL_OR_EMPTY);
		Message sentMessage = null;
		try
		{
			Twilio.init(accountSid, authToken);
			sentMessage = Message.creator(accountSid, new PhoneNumber(to), messagingServiceSid, message).create();
			LOG.info(String.format("Message  sent successfuly to: {0} ", sentMessage.getBody()));
			return true;
		}
		catch (final Exception e)
		{
			throw new OTPException(OTPExceptionType.MESSAGE_CAN_NOT_BE_SENT, TWILIO_SUSPENDED_ACCOUNT_RESPONSE
					+ " Please check your credentials and messagingServiceSid or your account balance.");
		}

	}

}
