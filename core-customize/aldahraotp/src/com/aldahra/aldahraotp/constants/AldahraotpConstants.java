/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraotp.constants;

/**
 * Global class for all Aldahraotp constants. You can add global constants for your extension into this class.
 */
public final class AldahraotpConstants extends GeneratedAldahraotpConstants
{
	public static final String EXTENSIONNAME = "aldahraotp";

	private AldahraotpConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "aldahraotpPlatformLogo";
}
