/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraotp.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SmsForm
{

	private final String desc;
	private final String campaignName;
	private final String msgCategory;
	private final String contentType;
	private final String dndCategory;
	private final  int priority;
	private final String dr;
	private String recipient;
	private String senderAddr;
	private String msg;
	private String clientTxnId;

	public SmsForm()
	{
		this.desc = " Rest API Test";
		this.campaignName = "Non Bulk";
		this.msgCategory = "4.5";
		this.contentType = "3.2";
		this.dndCategory = "Campaign";
		this.priority = 1;
		this.dr = "1";
	}


	public SmsForm(final String mobileNumber, final String senderAddress, final String message)
	{
		this.desc = " Rest API Test";
		this.campaignName = "Non Bulk";
		this.msgCategory = "4.5";
		this.contentType = "3.2";
		this.dndCategory = "Campaign";
		this.priority = 1;
		this.dr = "1";
		this.recipient = mobileNumber;
		this.senderAddr = senderAddress;
		this.msg = message;

	}


	public String getMsg()
	{
		return msg;
	}

	public void setMsg(final String msg)
	{
		this.msg = msg;
	}

	/**
	 * @return the desc
	 */
	public String getDesc()
	{
		return desc;
	}

	/**
	 * @return the campaignName
	 */
	public String getCampaignName()
	{
		return campaignName;
	}

	/**
	 * @return the msgCategory
	 */
	public String getMsgCategory()
	{
		return msgCategory;
	}

	/**
	 * @return the contentType
	 */
	public String getContentType()
	{
		return contentType;
	}

	public String getClientTxnId()
	{
		return clientTxnId;
	}

	public void setClientTxnId(final String clientTxnId)
	{
		this.clientTxnId = clientTxnId;
	}

	/**
	 * @return the dndCategory
	 */
	public String getDndCategory()
	{
		return dndCategory;
	}

	/**
	 * @return the priority
	 */
	public int getPriority()
	{
		return priority;
	}

	/**
	 * @return the dr
	 */
	public String getDr()
	{
		return dr;
	}


	public String getRecipient()
	{
		return recipient;
	}

	public void setRecipient(final String recipient)
	{
		this.recipient = recipient;
	}

	/**
	 * @return the senderAddr
	 */
	public String getSenderAddr()
	{
		return senderAddr;
	}

	/**
	 * @param senderAddr
	 *           the senderAddr to set
	 */
	public void setSenderAddr(final String senderAddr)
	{
		this.senderAddr = senderAddr;
	}

	/**
	 * @return the message
	 */
	public String getMessage()
	{
		return msg;
	}

	/**
	 * @param message
	 *           the message to set
	 */
	public void setMessage(final String message)
	{
		this.msg = message;
	}

	@Override
	public String toString()
	{
		return " {desc :" + desc + ", campaignName :" + campaignName + ", msgCategory :" + msgCategory + ", contentType :"
				+ contentType + ", dndCategory :" + dndCategory + ", priority :" + priority + ", dr :" + dr + ", recipient :"
				+ recipient + ", senderAddr :" + senderAddr + ", msg :" + msg + "}";
	}



}
