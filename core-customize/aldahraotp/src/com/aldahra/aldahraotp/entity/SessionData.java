/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraotp.entity;

/**
 * The Class SessionData.
 *
 * @author mnasro
 */
public class SessionData
{

	/** The session key. */
	private String sessionKey;

	/** The data. */
	private Object data;



	/**
	 *
	 */
	public SessionData(final String sessionKey, final Object data)
	{
		super();
		this.sessionKey = sessionKey;
		this.data = data;
	}

	/**
	 * Gets the session key.
	 *
	 * @return the session key
	 */
	public String getSessionKey()
	{
		return sessionKey;
	}

	/**
	 * Sets the session key.
	 *
	 * @param sessionKey
	 *           the new session key
	 */
	public void setSessionKey(final String sessionKey)
	{
		this.sessionKey = sessionKey;
	}

	/**
	 * Gets the data.
	 *
	 * @return the data
	 */
	public Object getData()
	{
		return data;
	}

	/**
	 * Sets the data.
	 *
	 * @param data
	 *           the new data
	 */
	public void setData(final Object data)
	{
		this.data = data;
	}

}
