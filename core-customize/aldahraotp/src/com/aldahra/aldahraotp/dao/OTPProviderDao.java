/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraotp.dao;

import de.hybris.platform.cms2.model.site.CMSSiteModel;

import java.util.Optional;

import com.aldahra.aldahraotp.model.OTPProviderModel;


/**
 * @author mnasro
 *
 *         The Interface OTPProviderDao.
 */
public interface OTPProviderDao
{

	/**
	 * Gets the.
	 *
	 * @param code
	 *           the code
	 * @return the optional
	 */
	public Optional<OTPProviderModel> get(String code);

	/**
	 * Gets the active.
	 *
	 * @param cmsSiteUid
	 *           the cms site uid
	 * @return the active
	 */
	public Optional<OTPProviderModel> getActive(String cmsSiteUid);

	/**
	 * Gets the active.
	 *
	 * @param cmsSiteModel
	 *           the cms site model
	 * @return the active
	 */
	public Optional<OTPProviderModel> getActive(CMSSiteModel cmsSiteModel);

	/**
	 * Gets the active provider by current site.
	 *
	 * @return the active provider by current site
	 */
	public Optional<OTPProviderModel> getActiveProviderByCurrentSite();
}
