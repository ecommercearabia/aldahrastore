/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraotp.dao.impl;

import com.aldahra.aldahraotp.dao.OTPProviderDao;
import com.aldahra.aldahraotp.model.TwilioOTPProviderModel;


/**
 * @author mnasro
 *
 *         The Class DefaultTwilioOTPProviderDao.
 */
public class DefaultTwilioOTPProviderDao extends DefaultOTPProviderDao implements OTPProviderDao
{

	/**
	 * Instantiates a new default twilio OTP provider dao.
	 */
	public DefaultTwilioOTPProviderDao()
	{
		super(TwilioOTPProviderModel._TYPECODE);
	}

	/**
	 * Gets the model name.
	 *
	 * @return the model name
	 */
	@Override
	protected String getModelName()
	{
		return TwilioOTPProviderModel._TYPECODE;
	}

}
