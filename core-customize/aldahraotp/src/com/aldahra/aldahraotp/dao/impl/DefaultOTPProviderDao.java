/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraotp.dao.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.site.BaseSiteService;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.aldahra.aldahraotp.dao.OTPProviderDao;
import com.aldahra.aldahraotp.model.OTPProviderModel;
import com.google.common.base.Preconditions;


/**
 * @author mnasro
 *
 *         The Class DefaultOTPProviderDao.
 */
public abstract class DefaultOTPProviderDao extends DefaultGenericDao<OTPProviderModel>
		implements OTPProviderDao
{

	/** The cms site service. */
	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	/** The base site service. */
	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	/** The Constant CODE_MUSTN_T_BE_NULL. */
	private static final String CODE_MUSTN_T_BE_NULL = "code mustn't be null or empty";

	/** The Constant SITES. */
	private static final String SITES = "sites";

	/** The Constant CODE. */
	private static final String CODE = "code";

	/** The Constant ACTIVE. */
	private static final String ACTIVE = "active";

	/**
	 * Instantiates a new default OTP provider dao.
	 *
	 * @param typecode
	 *           the typecode
	 */
	public DefaultOTPProviderDao(final String typecode)
	{
		super(typecode);
	}

	/**
	 * Gets the model name.
	 *
	 * @return the model name
	 */
	protected abstract String getModelName();

	/**
	 * Gets the.
	 *
	 * @param code
	 *           the code
	 * @return the optional
	 */
	@Override
	public Optional<OTPProviderModel> get(final String code)
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(code), CODE_MUSTN_T_BE_NULL);
		final Map<String, Object> params = new HashMap<>();
		params.put(CODE, code);
		final List<OTPProviderModel> find = find(params);
		final Optional<OTPProviderModel> findFirst = find.stream().findFirst();

		return findFirst.isPresent() ? Optional.ofNullable(findFirst.get()) : Optional.ofNullable(null);
	}

	/**
	 * Gets the active.
	 *
	 * @param cmsSiteUid
	 *           the cms site uid
	 * @return the active
	 */
	@Override
	public Optional<OTPProviderModel> getActive(final String cmsSiteUid)
	{
		return getActive((CMSSiteModel) getBaseSiteService().getBaseSiteForUID(cmsSiteUid));
	}

	/**
	 * Gets the active.
	 *
	 * @param cmsSiteModel
	 *           the cms site model
	 * @return the active
	 */
	@Override
	public Optional<OTPProviderModel> getActive(final CMSSiteModel cmsSiteModel)
	{
		Preconditions.checkArgument(cmsSiteModel != null, "cmsSiteModel must not be null");
		final Optional<Collection<OTPProviderModel>> find = find(Arrays.asList(cmsSiteModel), Boolean.TRUE, 1);

		return find.isPresent() && !find.get().isEmpty() ? Optional.ofNullable(find.get().iterator().next())
				: Optional.ofNullable(null);
	}

	/**
	 * Gets the active provider by current site.
	 *
	 * @return the active provider by current site
	 */
	@Override
	public Optional<OTPProviderModel> getActiveProviderByCurrentSite()
	{
		return getActive(getCmsSiteService().getCurrentSite());
	}

	/**
	 * Find.
	 *
	 * @param sites
	 *           the sites
	 * @param active
	 *           the active
	 * @param countRecords
	 *           the count records
	 * @return the optional
	 */
	protected Optional<Collection<OTPProviderModel>> find(final List<CMSSiteModel> sites,
			final Boolean active,
			final Integer countRecords)
	{
		final Map<String, Object> params = new HashMap<>();
		final StringBuilder query = new StringBuilder();

		//		SELECT {otpp.PK} FROM
		//		{
		//			TwilioOTPProvider AS otpp JOIN OTPProviderForCMSSite AS otpofcs ON {otpofcs.target}={otpp.PK}
		//		 	JOIN CMSSite AS cs ON {otpofcs.source}={cs.PK} AND {cs.PK} IN (?sites)
		//
		//		} WHERE {otpp.active}=?active


		query.append("SELECT {otpp.PK} ");
		query.append("FROM ");
		query.append("{ ");
		query.append(getModelName()).append(" AS otpp ");
		if (CollectionUtils.isNotEmpty(sites))
		{
			query.append("JOIN OTPProviderForCMSSite AS otpofcs ON {otpofcs.target}={otpp.PK} ");
			query.append("JOIN CMSSite AS cs ON {otpofcs.source}={cs.PK} AND {cs.PK} IN (?sites) ");
			params.put(SITES, sites);
		}

		query.append("} ");

		int activeValue = 0;
		if (active == null || Boolean.TRUE.equals(active))
		{
			activeValue = 1;
		}
		query.append("WHERE {otpp.active}=?active ");
		params.put(ACTIVE, activeValue);

		query.append("ORDER BY {otpp.creationtime} DESC");

		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query.toString());
		flexibleSearchQuery.getQueryParameters().putAll(params);
		if (countRecords != null && countRecords > 0)
		{
			flexibleSearchQuery.setCount(countRecords);
		}

		final SearchResult<OTPProviderModel> result = getFlexibleSearchService().search(flexibleSearchQuery);

		return result != null ? Optional.ofNullable(result.getResult()) : Optional.ofNullable(Collections.emptyList());
	}

	/**
	 * Gets the cms site service.
	 *
	 * @return the cms site service
	 */
	protected CMSSiteService getCmsSiteService()
	{
		return cmsSiteService;
	}

	/**
	 * Gets the base site service.
	 *
	 * @return the base site service
	 */
	protected BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}
}
