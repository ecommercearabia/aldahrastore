/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraotp.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

import java.util.List;
import java.util.Optional;

import org.apache.commons.collections.CollectionUtils;

import com.aldahra.aldahraotp.dao.OTPVerificationTokenDao;
import com.aldahra.aldahraotp.model.OTPVerificationTokenModel;


/**
 * @author mnasro
 *
 *         The Class DefaultOTPVerificationTokenDao.
 */
public class DefaultOTPVerificationTokenDao extends DefaultGenericDao<OTPVerificationTokenModel>
		implements OTPVerificationTokenDao
{
	/** The base select query. */
	private static final String BASE_SELECT_QUERY = "SELECT {p:" + OTPVerificationTokenModel.PK + "} " + "FROM {"
			+ OTPVerificationTokenModel._TYPECODE + " AS p} ";

	/**
	 *
	 */
	public DefaultOTPVerificationTokenDao()
	{
		super(OTPVerificationTokenModel._TYPECODE);
	}

	@Override
	public Optional<OTPVerificationTokenModel> getToken(final String token)
	{
		final String stringQuery = BASE_SELECT_QUERY + "WHERE " + "{p:" + OTPVerificationTokenModel.TOKEN + "}=?"
				+ OTPVerificationTokenModel.TOKEN;
		final FlexibleSearchQuery query = new FlexibleSearchQuery(stringQuery);
		query.addQueryParameter(OTPVerificationTokenModel.TOKEN, token);
		final List<OTPVerificationTokenModel> result = getFlexibleSearchService().<OTPVerificationTokenModel> search(query)
				.getResult();

		return CollectionUtils.isEmpty(result) ? Optional.empty() : Optional.ofNullable(result.get(0));
	}


}
