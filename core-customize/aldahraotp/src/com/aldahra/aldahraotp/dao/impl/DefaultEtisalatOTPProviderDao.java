/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraotp.dao.impl;

import com.aldahra.aldahraotp.dao.OTPProviderDao;
import com.aldahra.aldahraotp.model.EtisalatOTPProviderModel;


/**
 * @author mnasro
 *
 *         The Class DefaultEtisalatOTPProviderDao.
 */
public class DefaultEtisalatOTPProviderDao extends DefaultOTPProviderDao implements OTPProviderDao
{

	/**
	 * Instantiates a new default Etisalat OTP provider dao.
	 */
	public DefaultEtisalatOTPProviderDao()
	{
		super(EtisalatOTPProviderModel._TYPECODE);
	}

	/**
	 * Gets the model name.
	 *
	 * @return the model name
	 */
	@Override
	protected String getModelName()
	{
		return EtisalatOTPProviderModel._TYPECODE;
	}

}
