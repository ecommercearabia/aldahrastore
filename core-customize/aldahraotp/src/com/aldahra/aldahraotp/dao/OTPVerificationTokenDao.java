/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraotp.dao;

import java.util.Optional;

import com.aldahra.aldahraotp.model.OTPVerificationTokenModel;


/**
 * @author mnasro
 *
 *         The Interface OTPVerificationTokenDao.
 */
public interface OTPVerificationTokenDao
{
	public Optional<OTPVerificationTokenModel> getToken(String token);
}
