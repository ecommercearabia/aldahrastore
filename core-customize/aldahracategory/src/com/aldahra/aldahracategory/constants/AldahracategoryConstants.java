/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahracategory.constants;

/**
 * Global class for all Aldahracategory constants. You can add global constants for your extension into this class.
 */
public final class AldahracategoryConstants extends GeneratedAldahracategoryConstants
{
	public static final String EXTENSIONNAME = "aldahracategory";

	private AldahracategoryConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "aldahracategoryPlatformLogo";
}
