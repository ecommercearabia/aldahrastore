/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrapayment.exception;

/**
 * @author amjad.shati@erabia.com
 *
 */
public class RefundException extends Exception
{
	/**
	 *
	 */
	public RefundException(final String message)
	{
		super(message);
	}
}
