/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrapayment.service;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.aldahra.aldahrapayment.model.PaymentProviderModel;


/**
 * @author mnasro
 *
 *         The Interface PaymentProviderService.
 */
public interface PaymentProviderService
{

	/**
	 * Gets the.
	 *
	 * @param code
	 *           the code
	 * @param providerClass
	 *           the provider class
	 * @return the optional
	 */
	public Optional<PaymentProviderModel> get(String code, final Class<?> providerClass);

	/**
	 * Gets the active.
	 *
	 * @param baseStoreUid
	 *           the base store uid
	 * @param providerClass
	 *           the provider class
	 * @return the active
	 */
	public Optional<PaymentProviderModel> getActive(String baseStoreUid, final Class<?> providerClass);

	/**
	 * Gets the active.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @param providerClass
	 *           the provider class
	 * @return the active
	 */
	public Optional<PaymentProviderModel> getActive(BaseStoreModel baseStoreModel, final Class<?> providerClass);

	/**
	 * Gets the active provider by current base store.
	 *
	 * @param providerClass
	 *           the provider class
	 * @return the active provider by current base store
	 */
	public Optional<PaymentProviderModel> getActiveProviderByCurrentBaseStore(final Class<?> providerClass);
}
