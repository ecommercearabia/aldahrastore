/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrapayment.service.impl;

import de.hybris.platform.core.model.order.OrderModel;

import java.math.BigDecimal;

import javax.annotation.Resource;

import com.aldahra.aldahrapayment.ccavenue.exception.PaymentException;
import com.aldahra.aldahrapayment.context.PaymentContext;
import com.aldahra.aldahrapayment.exception.RefundException;
import com.aldahra.aldahrapayment.service.CustomRefundService;
import com.aldahra.aldahrastorecredit.service.StoreCreditService;
import com.google.common.base.Preconditions;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class DefaultCustomRefundService implements CustomRefundService
{
	@Resource(name = "paymentContext")
	private PaymentContext paymentContext;

	@Resource(name = "storeCreditService")
	private StoreCreditService storeCreditService;

	@Override
	public void refundAmount(final OrderModel order, final BigDecimal amount) throws RefundException, PaymentException
	{
		validateParams(order, amount);
		if ("cod".equalsIgnoreCase(order.getPaymentMode().getCode()))
		{
			refundCODAmount(order, amount);
		}
		else if ("card".equalsIgnoreCase(order.getPaymentMode().getCode()))
		{
			refundCCAmount(order, amount);
		}
	}

	private void refundCCAmount(final OrderModel order, final BigDecimal amount) throws PaymentException
	{
		paymentContext.getPaymentOrderRefundResponseData(order.getStore(), order, amount);
	}

	private void refundCODAmount(final OrderModel order, final BigDecimal amount)
	{
		Preconditions.checkArgument(amount != null, "amount must not be null.");
		storeCreditService.addStoreCredit(order.getUser(), order.getStore(), amount);
	}

	private void validateParams(final OrderModel order, final BigDecimal amount) throws RefundException
	{
		Preconditions.checkArgument(order != null, "Order must not be null.");
		Preconditions.checkArgument(order.getPaymentMode() != null, "Order payment mode must not be null.");
		if (amount.doubleValue() > order.getTotalPrice().doubleValue())
		{
			throw new RefundException("Refund amount is more than the order total.");
		}

	}
}
