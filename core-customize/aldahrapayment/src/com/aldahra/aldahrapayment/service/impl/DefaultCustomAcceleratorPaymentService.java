package com.aldahra.aldahrapayment.service.impl;

import de.hybris.platform.acceleratorservices.model.payment.CCPaySubValidationModel;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.PaymentSubscriptionResultItem;
import de.hybris.platform.acceleratorservices.payment.enums.DecisionsEnum;
import de.hybris.platform.acceleratorservices.payment.impl.DefaultAcceleratorPaymentService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.aldahra.aldahrapayment.context.PaymentContext;
import com.aldahra.aldahrapayment.context.PaymentProviderContext;
import com.aldahra.aldahrapayment.service.CustomAcceleratorPaymentService;
import com.aldahra.aldahrapayment.service.PaymentProviderService;


/**
 * The Class DefaultCustomAcceleratorPaymentService.
 *
 * @author mnasro
 */
public class DefaultCustomAcceleratorPaymentService extends DefaultAcceleratorPaymentService
		implements CustomAcceleratorPaymentService
{

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(DefaultCustomAcceleratorPaymentService.class);

	/** The Constant SUBSCRIPTION_INFO_DATA_CANNOT_BE_NULL_MSG. */
	private static final String SUBSCRIPTION_INFO_DATA_CANNOT_BE_NULL_MSG = "SubscriptionInfoData cannot be null";

	/** The Constant SIGNATURE_DATA_CANNOT_BE_NULL_MSG. */
	private static final String SIGNATURE_DATA_CANNOT_BE_NULL_MSG = "SignatureData cannot be null";

	/** The Constant PAYMENT_INFO_DATA_CANNOT_BE_NULL_MSG. */
	private static final String PAYMENT_INFO_DATA_CANNOT_BE_NULL_MSG = "PaymentInfoData cannot be null";

	/** The Constant ORDER_INFO_DATA_CANNOT_BE_NULL_MSG. */
	private static final String ORDER_INFO_DATA_CANNOT_BE_NULL_MSG = "OrderInfoData cannot be null";

	/** The Constant CUSTOMER_INFO_DATA_CANNOT_BE_NULL_MSG. */
	private static final String CUSTOMER_INFO_DATA_CANNOT_BE_NULL_MSG = "CustomerInfoData cannot be null";

	/** The Constant AUTH_REPLY_DATA_CANNOT_BE_NULL_MSG. */
	private static final String AUTH_REPLY_DATA_CANNOT_BE_NULL_MSG = "AuthReplyData cannot be null";

	/** The Constant DECISION_CANNOT_BE_NULL_MSG. */
	private static final String DECISION_CANNOT_BE_NULL_MSG = "Decision cannot be null";

	/** The Constant CREATE_SUBSCRIPTION_RESULT_CANNOT_BE_NULL_MSG. */
	private static final String CREATE_SUBSCRIPTION_RESULT_CANNOT_BE_NULL_MSG = "CreateSubscriptionResult cannot be null";

	/** The payment context. */
	@Resource(name = "paymentContext")
	private PaymentContext paymentContext;

	/** The payment provider context. */
	@Resource(name = "paymentProviderContext")
	private PaymentProviderContext paymentProviderContext;

	/** The payment provider service. */
	@Resource(name = "paymentProviderService")
	private PaymentProviderService paymentProviderService;

	/** The cart service. */
	@Resource(name = "cartService")
	private CartService cartService;

	/**
	 * Complete create payment subscription.
	 *
	 * @param customerModel
	 *           the customer model
	 * @param saveInAccount
	 *           the save in account
	 * @param parameters
	 *           the parameters
	 * @return the payment subscription result item
	 */
	@Override
	public PaymentSubscriptionResultItem completeCreatePaymentSubscription(final CustomerModel customerModel,
			final boolean saveInAccount, final Map<String, Object> parameters)
	{


		final Optional<CreateSubscriptionResult> createSubscriptionResult = paymentContext
				.interpretResponseByCurrentStore(parameters);
		if (!createSubscriptionResult.isPresent())
		{
			return null;
		}

		final CreateSubscriptionResult response = createSubscriptionResult.get();
		ServicesUtil.validateParameterNotNull(response, CREATE_SUBSCRIPTION_RESULT_CANNOT_BE_NULL_MSG);
		Assert.notNull(response.getDecision(), DECISION_CANNOT_BE_NULL_MSG);
		Assert.notNull(response.getAuthReplyData(), AUTH_REPLY_DATA_CANNOT_BE_NULL_MSG);
		Assert.notNull(response.getCustomerInfoData(), CUSTOMER_INFO_DATA_CANNOT_BE_NULL_MSG);
		Assert.notNull(response.getOrderInfoData(), ORDER_INFO_DATA_CANNOT_BE_NULL_MSG);
		Assert.notNull(response.getPaymentInfoData(), PAYMENT_INFO_DATA_CANNOT_BE_NULL_MSG);
		Assert.notNull(response.getSignatureData(), SIGNATURE_DATA_CANNOT_BE_NULL_MSG);
		Assert.notNull(response.getSubscriptionInfoData(), SUBSCRIPTION_INFO_DATA_CANNOT_BE_NULL_MSG);

		final PaymentSubscriptionResultItem paymentSubscriptionResult = new PaymentSubscriptionResultItem();
		paymentSubscriptionResult.setSuccess(DecisionsEnum.ACCEPT.name().equalsIgnoreCase(response.getDecision()));
		paymentSubscriptionResult.setDecision(String.valueOf(response.getDecision()));
		paymentSubscriptionResult.setResultCode(String.valueOf(response.getReasonCode()));

		if (DecisionsEnum.ACCEPT.name().equalsIgnoreCase(response.getDecision()))
		{
			final PaymentTransactionEntryModel savePaymentTransactionEntry = getPaymentTransactionStrategy()
					.savePaymentTransactionEntry(customerModel, response.getRequestId(), response.getOrderInfoData());

			final CreditCardPaymentInfoModel cardPaymentInfoModel = getCreditCardPaymentInfoCreateStrategy().saveSubscription(
					customerModel, response.getCustomerInfoData(), response.getSubscriptionInfoData(), response.getPaymentInfoData(),
					saveInAccount);
			paymentSubscriptionResult.setStoredCard(cardPaymentInfoModel);

			// Check if the subscription has already been validated
			final CCPaySubValidationModel subscriptionValidation = getCreditCardPaymentSubscriptionDao()
					.findSubscriptionValidationBySubscription(cardPaymentInfoModel.getSubscriptionId());
			if (subscriptionValidation != null)
			{
				cardPaymentInfoModel.setSubscriptionValidated(true);
				getModelService().save(cardPaymentInfoModel);
				getModelService().remove(subscriptionValidation);
				getModelService().refresh(cardPaymentInfoModel);
			}

			if (savePaymentTransactionEntry != null && savePaymentTransactionEntry.getPaymentTransaction() != null
					&& cartService.hasSessionCart())
			{
				final PaymentTransactionModel paymentTransaction = savePaymentTransactionEntry.getPaymentTransaction();
				paymentTransaction.setInfo(cardPaymentInfoModel);
				final CartModel sessionCart = cartService.getSessionCart();
				sessionCart.setPaymentTransactions(Arrays.asList(paymentTransaction));
				getModelService().save(sessionCart);
			}
		}
		else
		{
			final String logData = String.format("Cannot create subscription. Decision: %s - Reason Code: %s",
					response.getDecision(), response.getReasonCode());
			LOG.error(logData);
		}
		return paymentSubscriptionResult;
	}

}
