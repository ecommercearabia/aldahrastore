package com.aldahra.aldahrapayment.service.impl;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.aldahra.aldahrapayment.dao.PaymentProviderDao;
import com.aldahra.aldahrapayment.model.PaymentProviderModel;
import com.aldahra.aldahrapayment.service.PaymentProviderService;
import com.google.common.base.Preconditions;


/**
 * @author mnasro
 *
 *         The Class DefaultPaymentProviderService.
 */
public class DefaultPaymentProviderService implements PaymentProviderService
{

	/** The payment provider dao map. */
	@Resource(name = "paymentProviderDaoMap")
	private Map<Class<?>, PaymentProviderDao> paymentProviderDaoMap;

	/**
	 * Gets the payment provider dao map.
	 *
	 * @return the payment provider dao map
	 */
	protected Map<Class<?>, PaymentProviderDao> getPaymentProviderDaoMap()
	{
		return paymentProviderDaoMap;
	}

	/** The Constant CODE_MUSTN_T_BE_NULL. */
	private static final String CODE_MUSTN_T_BE_NULL = "code mustn't be null or empty";

	/** The Constant BASESTORE_UID_MUSTN_T_BE_NULL. */
	private static final String BASESTORE_UID_MUSTN_T_BE_NULL = "baseStoreUid mustn't be null or empty";

	/** The Constant BASESTORE_MUSTN_T_BE_NULL. */
	private static final String BASESTORE_MUSTN_T_BE_NULL = "baseStoreModel mustn't be null or empty";

	/** The Constant PROVIDER_CLASS_MUSTN_T_BE_NULL. */
	private static final String PROVIDER_CLASS_MUSTN_T_BE_NULL = "providerClass mustn't be null";

	/** The Constant PROVIDER_DAO_NOT_FOUND. */
	private static final String PROVIDER_DAO_NOT_FOUND = "dao not found";

	/**
	 * Gets the.
	 *
	 * @param code
	 *           the code
	 * @param providerClass
	 *           the provider class
	 * @return the optional
	 */
	@Override
	public Optional<PaymentProviderModel> get(final String code, final Class<?> providerClass)
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(code), CODE_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<PaymentProviderDao> dao = getDao(providerClass);
		Preconditions.checkArgument(dao.isPresent(), PROVIDER_DAO_NOT_FOUND);

		return dao.get().get(code);
	}

	/**
	 * Gets the active.
	 *
	 * @param baseStoreUid
	 *           the base store uid
	 * @param providerClass
	 *           the provider class
	 * @return the active
	 */
	@Override
	public Optional<PaymentProviderModel> getActive(final String baseStoreUid, final Class<?> providerClass)
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(baseStoreUid), BASESTORE_UID_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<PaymentProviderDao> dao = getDao(providerClass);
		Preconditions.checkArgument(dao.isPresent(), PROVIDER_DAO_NOT_FOUND);

		return dao.get().getActive(baseStoreUid);
	}

	/**
	 * Gets the active.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @param providerClass
	 *           the provider class
	 * @return the active
	 */
	@Override
	public Optional<PaymentProviderModel> getActive(final BaseStoreModel baseStoreModel, final Class<?> providerClass)
	{
		Preconditions.checkArgument(baseStoreModel != null, BASESTORE_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<PaymentProviderDao> dao = getDao(providerClass);
		Preconditions.checkArgument(dao.isPresent(), PROVIDER_DAO_NOT_FOUND);

		return dao.get().getActive(baseStoreModel);
	}

	/**
	 * Gets the active provider by current base store.
	 *
	 * @param providerClass
	 *           the provider class
	 * @return the active provider by current base store
	 */
	@Override
	public Optional<PaymentProviderModel> getActiveProviderByCurrentBaseStore(final Class<?> providerClass)
	{
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<PaymentProviderDao> dao = getDao(providerClass);

		if (dao.isPresent())
		{
			return dao.get().getActiveByCurrentBaseStore();
		}
		return dao.get().getActiveByCurrentBaseStore();
	}

	/**
	 * Gets the dao.
	 *
	 * @param providerClass
	 *           the provider class
	 * @return the dao
	 */
	protected Optional<PaymentProviderDao> getDao(final Class<?> providerClass)
	{
		final PaymentProviderDao dao = getPaymentProviderDaoMap().get(providerClass);

		return Optional.ofNullable(dao);
	}

}
