/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrapayment.service;

import de.hybris.platform.core.model.order.OrderModel;

import java.math.BigDecimal;

import com.aldahra.aldahrapayment.ccavenue.exception.PaymentException;
import com.aldahra.aldahrapayment.exception.RefundException;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface CustomRefundService
{

	public void refundAmount(OrderModel order, BigDecimal amount) throws RefundException, PaymentException;

}
