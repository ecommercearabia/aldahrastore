/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrapayment.service;

import de.hybris.platform.acceleratorservices.payment.PaymentService;
import de.hybris.platform.acceleratorservices.payment.data.PaymentSubscriptionResultItem;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.Map;


/**
 * The Interface CustomAcceleratorPaymentService.
 *
 * @author mnasro
 */

public interface CustomAcceleratorPaymentService extends PaymentService
{

	/**
	 * Complete create payment subscription.
	 *
	 * @param customerModel
	 *           the customer model
	 * @param saveInAccount
	 *           the save in account
	 * @param parameters
	 *           the parameters
	 * @return the payment subscription result item
	 */
	public PaymentSubscriptionResultItem completeCreatePaymentSubscription(final CustomerModel customerModel,
			final boolean saveInAccount, final Map<String, Object> parameters);
}
