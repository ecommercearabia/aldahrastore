package com.aldahra.aldahrapayment.strategy.impl;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import javax.annotation.Resource;

import com.aldahra.aldahrapayment.model.CCAvenuePaymentProviderModel;
import com.aldahra.aldahrapayment.model.PaymentProviderModel;
import com.aldahra.aldahrapayment.service.PaymentProviderService;
import com.aldahra.aldahrapayment.strategy.PaymentProviderStrategy;


/**
 * @author mnasro
 * @author abu-muhasien
 *
 *         The Class DefaultCCAvenuePaymentProviderStrategy.
 */
public class DefaultCCAvenuePaymentProviderStrategy implements PaymentProviderStrategy
{

	/** The payment provider service. */
	@Resource(name = "paymentProviderService")
	private PaymentProviderService paymentProviderService;

	/**
	 * Gets the payment provider service.
	 *
	 * @return the payment provider service
	 */
	protected PaymentProviderService getPaymentProviderService()
	{
		return paymentProviderService;
	}

	/**
	 * Gets the active provider.
	 *
	 * @param baseStoreUid
	 *           the base store uid
	 * @return the active provider
	 */
	@Override
	public Optional<PaymentProviderModel> getActiveProvider(final String baseStoreUid)
	{
		return getPaymentProviderService().getActive(baseStoreUid, CCAvenuePaymentProviderModel.class);
	}

	/**
	 * Gets the active provider.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @return the active provider
	 */
	@Override
	public Optional<PaymentProviderModel> getActiveProvider(final BaseStoreModel baseStoreModel)
	{
		return getPaymentProviderService().getActive(baseStoreModel, CCAvenuePaymentProviderModel.class);
	}

	/**
	 * Gets the active provider by current base store.
	 *
	 * @return the active provider by current base store
	 */
	@Override
	public Optional<PaymentProviderModel> getActiveProviderByCurrentBaseStore()
	{
		return getPaymentProviderService().getActiveProviderByCurrentBaseStore(CCAvenuePaymentProviderModel.class);
	}
}

