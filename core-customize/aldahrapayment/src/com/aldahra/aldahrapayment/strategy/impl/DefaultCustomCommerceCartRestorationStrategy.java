/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrapayment.strategy.impl;

import de.hybris.platform.commerceservices.order.impl.DefaultCommerceCartRestorationStrategy;
import de.hybris.platform.core.model.order.CartModel;


/**
 *
 */
public class DefaultCustomCommerceCartRestorationStrategy extends DefaultCommerceCartRestorationStrategy
{
	@Override
	protected void clearPaymentTransactionsOnCart(final CartModel cartModel)
	{

	}

}
