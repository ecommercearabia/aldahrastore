package com.aldahra.aldahrapayment.strategy.impl;

import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.core.enums.PaymentStatus;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.couponservices.model.AbstractCouponModel;
import de.hybris.platform.couponservices.services.CouponService;
import de.hybris.platform.order.CartService;
import de.hybris.platform.payment.dto.TransactionStatusDetails;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;

import com.aldahra.aldahraotp.context.OTPContext;
import com.aldahra.aldahraotp.exception.OTPException;
import com.aldahra.aldahrapayment.ccavenue.entry.ApplePayPaymentResponseData;
import com.aldahra.aldahrapayment.ccavenue.entry.ApplePayRequest;
import com.aldahra.aldahrapayment.ccavenue.entry.CCAvenueApplePayRequest;
import com.aldahra.aldahrapayment.ccavenue.entry.CCAvenueCredentials;
import com.aldahra.aldahrapayment.ccavenue.entry.CustomerPaymentOptionData;
import com.aldahra.aldahrapayment.ccavenue.entry.CustomerPaymentOptionsData;
import com.aldahra.aldahrapayment.ccavenue.entry.PaymentLinkResponseData;
import com.aldahra.aldahrapayment.ccavenue.entry.RequestData;
import com.aldahra.aldahrapayment.ccavenue.enums.PaymentExceptionType;
import com.aldahra.aldahrapayment.ccavenue.enums.TransactionStatus;
import com.aldahra.aldahrapayment.ccavenue.exception.PaymentException;
import com.aldahra.aldahrapayment.ccavenue.service.CCAvenueService;
import com.aldahra.aldahrapayment.entry.PaymentRequestData;
import com.aldahra.aldahrapayment.entry.PaymentResponseData;
import com.aldahra.aldahrapayment.enums.PaymentResponseStatus;
import com.aldahra.aldahrapayment.enums.TransactionOperation;
import com.aldahra.aldahrapayment.model.CCAvenuePaymentProviderModel;
import com.aldahra.aldahrapayment.model.PaymentProviderModel;
import com.aldahra.aldahrapayment.strategy.CustomPaymentTransactionStrategy;
import com.aldahra.aldahrapayment.strategy.PaymentStrategy;
import com.aldahra.core.enums.ShipmentType;
import com.google.common.base.Preconditions;


/**
 * The Class DefaultCCAvenuePaymentStrategy.
 *
 * @author mnasro
 * @author abu-muhasien
 *
 *         The Class DefaultCCAvenuePaymentStrategy.
 */
public class DefaultCCAvenuePaymentStrategy implements PaymentStrategy
{

	/**
	 *
	 */
	private static final String APPLE = "apple";
	private static final Logger LOG = Logger.getLogger(DefaultCCAvenuePaymentStrategy.class);
	/** The Constant DELIVERY_ADDRESS_CAN_NOT_BE_EMPTY. */
	private static final String DELIVERY_ADDRESS_CAN_NOT_BE_EMPTY = "Delivery Address can not be empty";


	/** The Constant PAYMENT_ADDRESS_CAN_NOT_BE_NULL. */
	private static final String PAYMENT_ADDRESS_CAN_NOT_BE_NULL = "Payment address can not be null";


	/** The Constant THE_PROVIDER_MODER_IS_NOT_A_CC_AVENUE_PAYMENT_PROVIDER_MODEL. */
	private static final String THE_PROVIDER_MODER_IS_NOT_A_CC_AVENUE_PAYMENT_PROVIDER_MODEL = "the provider moder is not a CCAvenuePaymentProviderModel ";


	/** The Constant PAYMENT_INFO_DATA_CAN_NOT_BE_NULL. */
	private static final String PAYMENT_INFO_DATA_CAN_NOT_BE_NULL = "paymentInfoData can not be null";

	/** The common I 18 N service. */
	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;
	/** The payment provider service. */
	@Resource(name = "ccavenueService")
	private CCAvenueService ccavenueService;


	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "createSubscriptionResultConverter")
	private Converter<Map<String, Object>, CreateSubscriptionResult> createSubscriptionResultConverter;
	@Resource(name = "paymentTransactionStrategy")
	private CustomPaymentTransactionStrategy paymentTransactionStrategy;

	@Resource(name = "couponService")
	private CouponService couponService;

	@Resource(name = "otpContext")
	private OTPContext otpContext;

	/**
	 * @return the createSubscriptionResultConverter
	 */
	protected Converter<Map<String, Object>, CreateSubscriptionResult> getCreateSubscriptionResultConverter()
	{
		return createSubscriptionResultConverter;
	}



	/** The Constant PAYMENT_PROVIDER_MUSTN_T_BE_NULL. */
	private static final String PAYMENT_PROVIDER_MUSTN_T_BE_NULL = "paymentProviderModel mustn't be null";


	private static final String ABSTRACTORDER_MUSTN_T_BE_NULL = "abstractOrder mustn't be null";
	/** The Constant RESPONSE_PARAMS_MUSTN_T_BE_NULL. */
	private static final String RESPONSE_PARAMS_MUSTN_T_BE_NULL = "responseParams mustn't be null";

	/** The Constant DATA_CAN_NOT_BE_NULL. */
	private static final String DATA_CAN_NOT_BE_NULL = "data mustn't be null";
	@Resource(name = "orderCodeGenerator")
	private KeyGenerator keyGenerator;


	/**
	 * Builds the payment request data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrder
	 *           the abstract order
	 * @return the optional
	 */
	@Override
	public Optional<PaymentRequestData> buildPaymentRequestData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrder)
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(abstractOrder != null, ABSTRACTORDER_MUSTN_T_BE_NULL);

		if (StringUtils.isEmpty(abstractOrder.getOrderCode()))
		{
			abstractOrder.setOrderCode(generateOrderCode());
			modelService.save(abstractOrder);
		}
		final RequestData requestData = getRequestData(paymentProviderModel, abstractOrder);
		final Optional<String> scriptSrc = ccavenueService.getScriptSrc(requestData);
		if (scriptSrc.isPresent())
		{
			modelService.refresh(abstractOrder);
			abstractOrder.setRequestPaymentBody(scriptSrc.get());
			modelService.save(abstractOrder);
			final PaymentRequestData paymentRequestData = new PaymentRequestData(scriptSrc.get(),
					CCAvenuePaymentProviderModel._TYPECODE, requestData);
			return Optional.ofNullable(paymentRequestData);

		}

		return Optional.empty();
	}

	protected String generateOrderCode()
	{
		final Object generatedValue = keyGenerator.generate();
		if (generatedValue instanceof String)
		{
			return (String) generatedValue;
		}
		else
		{
			return String.valueOf(generatedValue);
		}
	}


	/**
	 * Interpret response.
	 *
	 * @param responseParams
	 *           the response params
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @return the optional
	 */
	@Override
	public Optional<CreateSubscriptionResult> interpretResponse(final Map<String, Object> responseParams,
			final PaymentProviderModel paymentProviderModel)
	{
		Preconditions.checkArgument(responseParams != null, RESPONSE_PARAMS_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		final CreateSubscriptionResult createSubscriptionResult = getCreateSubscriptionResultConverter().convert(responseParams);
		return createSubscriptionResult == null ? Optional.empty() : Optional.ofNullable(createSubscriptionResult);
	}

	/**
	 * Builds the payment response data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param data
	 *           the data
	 * @return the optional
	 */
	@Override
	public Optional<PaymentResponseData> buildPaymentResponseData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrder, final Object data)
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(abstractOrder != null, ABSTRACTORDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(data != null, DATA_CAN_NOT_BE_NULL);

		final Optional<Map<String, Object>> responseData = ccavenueService.getResponseData((String) data,
				getCCAvenueWorkingKeyByCart((CCAvenuePaymentProviderModel) paymentProviderModel, abstractOrder));
		if (responseData.isPresent())
		{
			modelService.refresh(abstractOrder);
			abstractOrder.setResponsePaymentBody(responseData.get().toString());
			final Object tracking_id = responseData.get().get("tracking_id");
			if (tracking_id != null && tracking_id instanceof String)
			{
				abstractOrder.setPaymentReferenceId((String) tracking_id);
			}
			modelService.save(abstractOrder);
			final PaymentResponseData paymentResponseData = new PaymentResponseData(responseData.get(),
					CCAvenuePaymentProviderModel._TYPECODE, null);
			return Optional.ofNullable(paymentResponseData);
		}
		return Optional.empty();
	}

	/**
	 * Gets the response data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the order
	 * @return the response data
	 */
	protected RequestData getRequestData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel)
	{
		Preconditions.checkArgument(paymentProviderModel instanceof CCAvenuePaymentProviderModel,
				THE_PROVIDER_MODER_IS_NOT_A_CC_AVENUE_PAYMENT_PROVIDER_MODEL);
		Preconditions.checkArgument(commonI18NService.getCurrentLanguage() != null, "CurrentLanguage is null");
		Preconditions.checkArgument(abstractOrderModel.getPaymentMode() != null,
				"No payment mode found for " + abstractOrderModel.getCode() + " cart!");
		final CCAvenuePaymentProviderModel providerModel = (CCAvenuePaymentProviderModel) paymentProviderModel;
		final AddressModel paymentAddress = getPaymentAddress(abstractOrderModel);
		final AddressModel deliveryAddress = getDeilveryAddress(abstractOrderModel);
		final RequestData requestData = new RequestData();
		final Double totalPrice = getTotalPrice(abstractOrderModel);

		final String contactEmail = ((de.hybris.platform.core.model.user.CustomerModel) abstractOrderModel.getUser())
				.getContactEmail();

		final String orderNo = StringUtils.isNotBlank(abstractOrderModel.getCcAvenueId()) ? abstractOrderModel.getCcAvenueId()
				: abstractOrderModel.getOrderCode();

		requestData.setAccessCode(getCCAvenueAccessCodeByCart(providerModel, abstractOrderModel));
		requestData.setWorkingKey(getCCAvenueWorkingKeyByCart(providerModel, abstractOrderModel));
		requestData.setMerchantId(getCCAvenueMerchantIdByCart(providerModel, abstractOrderModel));
		requestData.setIntegrationType("iframe_normal");
		requestData.setOrderId(orderNo);
		requestData.setAmount(totalPrice == null ? null : totalPrice.doubleValue() + "");
		requestData.setCustomerIdentifier(contactEmail);
		requestData.setCurrency(providerModel.getCurrency().getIsocode());
		requestData.setRedirectUrl(providerModel.getRedirectUrl());
		requestData.setCancelUrl(providerModel.getCancelUrl());
		requestData.setLanguage(commonI18NService.getCurrentLanguage() == null ? "EN"
				: commonI18NService.getCurrentLanguage().getIsocode().toUpperCase());
		if (paymentAddress != null)
		{
			requestData.setBillingName("Billing Name");
			requestData.setBillingAddress(paymentAddress.getCountry().getName(Locale.ENGLISH));
			requestData.setBillingCity(paymentAddress.getCity() == null ? "Billing City" : paymentAddress.getCity().getCode());
			requestData.setBillingState(getState(paymentAddress));
			requestData.setBillingZip("00000");
			requestData.setBillingCountry(paymentAddress.getCountry().getName(Locale.ENGLISH));
			requestData.setBillingTel(paymentAddress.getMobile());
			requestData.setBillingEmail(contactEmail);
		}
		if (deliveryAddress != null)
		{
			requestData.setDeliveryName("Delivery Name");
			requestData.setDeliveryAddress(deliveryAddress.getCountry().getName(Locale.ENGLISH));
			requestData.setDeliveryCity(
					deliveryAddress.getCity() != null ? deliveryAddress.getCity().getName(Locale.ENGLISH) : "Delivery City");
			requestData.setDeliveryState(getState(deliveryAddress));
			requestData.setDeliveryZip("00000");
			requestData.setDeliveryCountry(deliveryAddress.getCountry().getName(Locale.ENGLISH));
			requestData.setDeliveryTel(deliveryAddress.getMobile());
		}

		requestData.setMerchantParam1("additional Info.");
		requestData.setMerchantParam2("additional Info.");
		requestData.setMerchantParam3("additional Info.");
		requestData.setMerchantParam4("additional Info.");

		abstractOrderModel.getAppliedCouponCodes();
		requestData.setPromoCode(getCoupon(abstractOrderModel.getAppliedCouponCodes()));

		return requestData;
	}

	private String getCoupon(final Collection<String> appliedVouchers)
	{
		if (!CollectionUtils.isEmpty(appliedVouchers))
		{
			for (final String voucherCode : appliedVouchers)
			{
				final Optional<AbstractCouponModel> coupon = couponService.getCouponForCode(voucherCode);
				if (coupon.isPresent() && coupon.get().isBankOffer())
				{
					return voucherCode;
				}
			}
		}
		return "";
	}

	/**
	 *
	 */
	private String getState(final AddressModel addressModel)
	{
		if (addressModel == null)
		{
			return StringUtils.EMPTY;
		}
		return addressModel.getRegion() != null ? addressModel.getRegion().getName(Locale.ENGLISH)
				: (addressModel.getCity() == null ? "State" : addressModel.getCity().getName(Locale.ENGLISH));
	}

	/**
	 * Gets the total price.
	 *
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the total price
	 */
	protected Double getTotalPrice(final AbstractOrderModel abstractOrderModel, Double amount)
	{

		final Double totalPrice = getTotalPrice(abstractOrderModel);

		if (amount.doubleValue() > totalPrice)
		{
			return 00.0;
		}
		final CurrencyModel cartCurrency = abstractOrderModel.getCurrency();
		final CurrencyModel baseCurrency = commonI18NService.getBaseCurrency();
		CurrencyModel currency = null;
		if (baseCurrency.getIsocode().equalsIgnoreCase(cartCurrency.getIsocode()))
		{
			currency = cartCurrency;
			amount = abstractOrderModel.getTotalPrice();
		}
		else
		{
			currency = baseCurrency;
			amount = commonI18NService.convertAndRoundCurrency(cartCurrency.getConversion().doubleValue(),
					baseCurrency.getConversion().doubleValue(), cartCurrency.getDigits().intValue(),
					abstractOrderModel.getTotalPrice());
		}

		return amount;
	}

	protected Double getTotalPrice(final AbstractOrderModel abstractOrderModel)
	{
		final CurrencyModel cartCurrency = abstractOrderModel.getCurrency();
		final CurrencyModel baseCurrency = commonI18NService.getBaseCurrency();
		CurrencyModel currency = null;
		Double amount = null;
		if (baseCurrency.getIsocode().equalsIgnoreCase(cartCurrency.getIsocode()))
		{
			currency = cartCurrency;
			amount = abstractOrderModel.getTotalPrice();
		}
		else
		{
			currency = baseCurrency;
			amount = commonI18NService.convertAndRoundCurrency(cartCurrency.getConversion().doubleValue(),
					baseCurrency.getConversion().doubleValue(), cartCurrency.getDigits().intValue(),
					abstractOrderModel.getTotalPrice());
		}

		return amount;
	}


	/**
	 * Checks if is successful paid order.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @param data
	 *           the data
	 * @return true, if is successful paid order
	 */
	@Override
	public boolean isSuccessfulPaidOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel, final Object data)
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);

		final Optional<PaymentResponseData> buildPaymentResponseData = buildPaymentResponseData(paymentProviderModel,
				abstractOrderModel, data);
		if (!buildPaymentResponseData.isPresent())
		{
			return false;
		}
		final Map<String, Object> responseData = buildPaymentResponseData.get().getResponseData();

		return !(responseData == null || responseData.isEmpty() || !responseData.containsKey("order_status")
				|| !responseData.get("order_status").equals("Success"));
	}

	@Override
	public Optional<PaymentResponseData> getPaymentOrderStatusResponseData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrder, final Object data) throws PaymentException
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(abstractOrder != null, ABSTRACTORDER_MUSTN_T_BE_NULL);

		final CCAvenuePaymentProviderModel ccavenuePaymentProviderModel = (CCAvenuePaymentProviderModel) paymentProviderModel;
		final String referanceNo = (data == null ? abstractOrder.getPaymentReferenceId() : (String) data);
		abstractOrder.setPaymentReferenceId(referanceNo);
		modelService.save(abstractOrder);

		final String orderNo = StringUtils.isNotBlank(abstractOrder.getCcAvenueId()) ? abstractOrder.getCcAvenueId()
				: abstractOrder.getOrderCode();
		final String workingKey = getCCAvenueWorkingKeyByCart(ccavenuePaymentProviderModel, abstractOrder);
		final String accessCode = getCCAvenueAccessCodeByCart(ccavenuePaymentProviderModel, abstractOrder);
		final Optional<Map<String, Object>> responseData = ccavenueService.getOrderStatusData(workingKey, accessCode, referanceNo,
				orderNo);

		if (responseData.isPresent())
		{
			final PaymentResponseStatus paymentResponseStatus = getResponseStatus(responseData.get());

			final de.hybris.platform.payment.dto.TransactionStatus transactionStatus = paymentResponseStatus == null
					|| PaymentResponseStatus.FAILURE.equals(paymentResponseStatus)
							? de.hybris.platform.payment.dto.TransactionStatus.REJECTED
							: de.hybris.platform.payment.dto.TransactionStatus.ACCEPTED;

			final TransactionStatusDetails transactionStatusDetails = paymentResponseStatus == null
					|| PaymentResponseStatus.FAILURE.equals(paymentResponseStatus) ? TransactionStatusDetails.INVALID_REQUEST
							: TransactionStatusDetails.REVIEW_NEEDED;

			final String requestPaymentBody = "WorkingKey=[" + workingKey + "], AccessCode=[" + accessCode + "], referanceNo=["
					+ referanceNo + "], OrderCode=[" + orderNo + "]";
			final String responsePaymentBody = responseData.get().toString();
			paymentTransactionStrategy.savePaymentTransactionEntry(abstractOrder, abstractOrder.getPaymentReferenceId(),
					PaymentTransactionType.REVIEW_DECISION, transactionStatus, transactionStatusDetails, requestPaymentBody,
					responsePaymentBody, null);

			modelService.refresh(abstractOrder);
			abstractOrder.setResponsePaymentBody(responseData.get().toString());
			modelService.save(abstractOrder);
			final PaymentResponseData paymentResponseData = new PaymentResponseData(responseData.get(),
					CCAvenuePaymentProviderModel._TYPECODE, null);
			return Optional.ofNullable(paymentResponseData);
		}
		return Optional.empty();
	}

	@Override
	public Optional<PaymentResponseData> captureOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException
	{

		final Optional<PaymentResponseData> paymentOrderStatusResponseData = getPaymentOrderStatusResponseData(paymentProviderModel,
				abstractOrderModel, abstractOrderModel.getPaymentReferenceId());
		if (paymentOrderStatusResponseData.isPresent())
		{

			final Map<String, Object> responseData = paymentOrderStatusResponseData.get().getResponseData();
			validateTransactionStatus(responseData, abstractOrderModel, TransactionOperation.CAPTURE);

			final CCAvenuePaymentProviderModel ccavenuePaymentProviderModel = (CCAvenuePaymentProviderModel) paymentProviderModel;
			final Optional<Map<String, Object>> orderConfiemData = ccavenueService.getOrderConfiemData(
					getCCAvenueWorkingKeyByCart(ccavenuePaymentProviderModel, abstractOrderModel),
					getCCAvenueAccessCodeByCart(ccavenuePaymentProviderModel, abstractOrderModel),
					abstractOrderModel.getPaymentReferenceId(), getTotalPrice(abstractOrderModel) + "");
			final Integer successCount = (Integer) orderConfiemData.get().get("success_count");
			if (successCount == null || successCount <= 0)
			{
				LOG.error("capture Order is failed! THE_ORDER_CAN_NOT_BE_CAPTUARED!");
				final String requestPaymentBody = "PaymentReferenceId=[" + abstractOrderModel.getPaymentReferenceId()
						+ "], TotalPrice[" + getTotalPrice(abstractOrderModel) + "]";
				paymentTransactionStrategy.savePaymentTransactionEntry(abstractOrderModel, abstractOrderModel.getPaymentReferenceId(),
						PaymentTransactionType.CAPTURE, de.hybris.platform.payment.dto.TransactionStatus.REJECTED,
						TransactionStatusDetails.INVALID_REQUEST, requestPaymentBody, orderConfiemData.toString(), null);
				throw new PaymentException(PaymentExceptionType.THE_ORDER_CAN_NOT_BE_CAPTUARED.getMessage(),
						PaymentExceptionType.THE_ORDER_CAN_NOT_BE_CAPTUARED);
			}
			final PaymentResponseStatus paymentResponseStatus = getCaptureResponseStatus(orderConfiemData.get());
			final de.hybris.platform.payment.dto.TransactionStatus transactionStatus = paymentResponseStatus == null
					|| PaymentResponseStatus.FAILURE.equals(paymentResponseStatus)
							? de.hybris.platform.payment.dto.TransactionStatus.REJECTED
							: de.hybris.platform.payment.dto.TransactionStatus.ACCEPTED;


			final TransactionStatusDetails transactionStatusDetails = paymentResponseStatus == null
					|| PaymentResponseStatus.FAILURE.equals(paymentResponseStatus) ? TransactionStatusDetails.INVALID_REQUEST
							: TransactionStatusDetails.CREDIT_FOR_VOIDED_CAPTURE;
			final String requestPaymentBody = null;
			final String responsePaymentBody = responseData.toString();
			paymentTransactionStrategy.savePaymentTransactionEntry(abstractOrderModel, abstractOrderModel.getPaymentReferenceId(),
					PaymentTransactionType.CAPTURE, transactionStatus, transactionStatusDetails, requestPaymentBody,
					responsePaymentBody, null);


			modelService.refresh(abstractOrderModel);
			abstractOrderModel.setResponsePaymentBody(orderConfiemData.toString());
			if (PaymentResponseStatus.SUCCESS.equals(paymentResponseStatus))
			{
				abstractOrderModel.setPaymentStatus(PaymentStatus.PAID);
			}
			modelService.save(abstractOrderModel);

			final PaymentResponseData paymentResponseData = new PaymentResponseData(orderConfiemData.get(),
					CCAvenuePaymentProviderModel._TYPECODE, paymentResponseStatus);
			return Optional.ofNullable(paymentResponseData);

		}

		return Optional.empty();
	}

	@Override
	public Optional<PaymentResponseData> cancelOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException
	{
		final Optional<PaymentResponseData> paymentOrderStatusResponseData = getPaymentOrderStatusResponseData(paymentProviderModel,
				abstractOrderModel, abstractOrderModel.getPaymentReferenceId());
		if (paymentOrderStatusResponseData.isPresent())
		{

			final Map<String, Object> responseData = paymentOrderStatusResponseData.get().getResponseData();
			validateTransactionStatus(responseData, abstractOrderModel, TransactionOperation.CANCEL);
			final CCAvenuePaymentProviderModel ccavenuePaymentProviderModel = (CCAvenuePaymentProviderModel) paymentProviderModel;
			final Optional<Map<String, Object>> orderCancelData = ccavenueService.getOrderCancelData(
					getCCAvenueWorkingKeyByCart(ccavenuePaymentProviderModel, abstractOrderModel),
					getCCAvenueAccessCodeByCart(ccavenuePaymentProviderModel, abstractOrderModel),
					abstractOrderModel.getPaymentReferenceId(), getTotalPrice(abstractOrderModel) + "");
			final Integer successCount = (Integer) orderCancelData.get().get("success_count");
			if (successCount == null || successCount <= 0)
			{
				throw new PaymentException(PaymentExceptionType.THE_ORDER_CAN_NOT_BE_CANCELED.getMessage(),
						PaymentExceptionType.THE_ORDER_CAN_NOT_BE_CANCELED);
			}
			final PaymentResponseStatus paymentResponseStatus = getCancelResponseStatus(orderCancelData.get());
			final de.hybris.platform.payment.dto.TransactionStatus transactionStatus = paymentResponseStatus == null
					|| PaymentResponseStatus.FAILURE.equals(paymentResponseStatus)
							? de.hybris.platform.payment.dto.TransactionStatus.REJECTED
							: de.hybris.platform.payment.dto.TransactionStatus.ACCEPTED;

			final TransactionStatusDetails transactionStatusDetails = paymentResponseStatus == null
					|| PaymentResponseStatus.FAILURE.equals(paymentResponseStatus) ? TransactionStatusDetails.INVALID_REQUEST
							: TransactionStatusDetails.SUCCESFULL;
			final String requestPaymentBody = null;
			final String responsePaymentBody = responseData.toString();
			paymentTransactionStrategy.savePaymentTransactionEntry(abstractOrderModel, abstractOrderModel.getPaymentReferenceId(),
					PaymentTransactionType.CANCEL, transactionStatus, transactionStatusDetails, requestPaymentBody,
					responsePaymentBody, null);




			modelService.refresh(abstractOrderModel);
			abstractOrderModel.setResponsePaymentBody(orderCancelData.get().toString());
			if (PaymentResponseStatus.SUCCESS.equals(paymentResponseStatus))
			{
				abstractOrderModel.setPaymentStatus(PaymentStatus.NOTPAID);
			}

			modelService.save(abstractOrderModel);
			final PaymentResponseData paymentResponseData = new PaymentResponseData(orderCancelData.get(),
					CCAvenuePaymentProviderModel._TYPECODE, paymentResponseStatus);
			return Optional.ofNullable(paymentResponseData);

		}
		return Optional.empty();
	}

	/**
	 *
	 */
	private PaymentResponseStatus getResponseStatus(final Map<String, Object> map)
	{
		try
		{
			final boolean containsErrorCode = map.containsKey("error_code");
			String errorCode = null;
			if (containsErrorCode)
			{
				errorCode = (String) map.get("error_code");
				if (!StringUtils.isBlank(errorCode))
				{
					return PaymentResponseStatus.FAILURE;
				}
			}
			return PaymentResponseStatus.SUCCESS;
		}
		catch (final Exception e)
		{
			return PaymentResponseStatus.FAILURE;
		}

	}

	/**
	 *
	 */
	private PaymentResponseStatus getCaptureResponseStatus(final Map<String, Object> map)
	{
		try
		{
			final boolean containsErrorCode = map.containsKey("error_code");
			String errorCode = null;
			if (containsErrorCode)
			{
				errorCode = (String) map.get("error_code");
				if (!StringUtils.isBlank(errorCode))
				{
					return PaymentResponseStatus.FAILURE;
				}
			}
			return PaymentResponseStatus.SUCCESS;
		}
		catch (final Exception e)
		{
			return PaymentResponseStatus.FAILURE;
		}

	}





	/**
	 *
	 */
	private PaymentResponseStatus getCancelResponseStatus(final Map<String, Object> map)
	{
		try
		{
			final boolean containsErrorCode = map.containsKey("error_code");
			String errorCode = null;
			if (containsErrorCode)
			{
				errorCode = (String) map.get("error_code");
				if (!StringUtils.isBlank(errorCode))
				{
					return PaymentResponseStatus.FAILURE;
				}
			}
			return PaymentResponseStatus.SUCCESS;
		}
		catch (final Exception e)
		{
			return PaymentResponseStatus.FAILURE;
		}
	}

	@Override
	public Optional<PaymentResponseData> refundOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException
	{
		final Optional<PaymentResponseData> paymentOrderStatusResponseData = getPaymentOrderStatusResponseData(paymentProviderModel,
				abstractOrderModel, abstractOrderModel.getPaymentReferenceId());
		if (paymentOrderStatusResponseData.isPresent())
		{

			final Map<String, Object> responseData = paymentOrderStatusResponseData.get().getResponseData();
			validateTransactionStatus(responseData, abstractOrderModel, TransactionOperation.REFUND_STANDALONE);
			final CCAvenuePaymentProviderModel ccavenuePaymentProviderModel = (CCAvenuePaymentProviderModel) paymentProviderModel;
			final Optional<Map<String, Object>> orderRefundData = ccavenueService.getOrderRefundData(
					getCCAvenueWorkingKeyByCart(ccavenuePaymentProviderModel, abstractOrderModel),
					getCCAvenueAccessCodeByCart(ccavenuePaymentProviderModel, abstractOrderModel),
					abstractOrderModel.getPaymentReferenceId(), getTotalPrice(abstractOrderModel).toString(),
					((int) (Math.random() * 300000)) + "");

			final Integer refundStatus = (Integer) orderRefundData.get().get("refund_status");
			if (refundStatus == null || refundStatus != 0)
			{
				throw new PaymentException(PaymentExceptionType.THE_ORDER_CAN_NOT_BE_CANCELED.getMessage(),
						PaymentExceptionType.THE_ORDER_CAN_NOT_BE_REFUNDED);
			}
			final PaymentResponseStatus paymentResponseStatus = getCancelResponseStatus(orderRefundData.get());
			final de.hybris.platform.payment.dto.TransactionStatus transactionStatus = paymentResponseStatus == null
					|| PaymentResponseStatus.FAILURE.equals(paymentResponseStatus)
							? de.hybris.platform.payment.dto.TransactionStatus.REJECTED
							: de.hybris.platform.payment.dto.TransactionStatus.ACCEPTED;

			final TransactionStatusDetails transactionStatusDetails = paymentResponseStatus == null
					|| PaymentResponseStatus.FAILURE.equals(paymentResponseStatus) ? TransactionStatusDetails.INVALID_REQUEST
							: TransactionStatusDetails.SUCCESFULL;
			final String requestPaymentBody = null;
			final String responsePaymentBody = responseData.toString();
			paymentTransactionStrategy.savePaymentTransactionEntry(abstractOrderModel, abstractOrderModel.getPaymentReferenceId(),
					PaymentTransactionType.REFUND_STANDALONE, transactionStatus, transactionStatusDetails, requestPaymentBody,
					responsePaymentBody, null);

			modelService.refresh(abstractOrderModel);
			abstractOrderModel.setResponsePaymentBody(orderRefundData.get().toString());
			modelService.save(abstractOrderModel);



			final PaymentResponseData paymentResponseData = new PaymentResponseData(orderRefundData.get(),
					CCAvenuePaymentProviderModel._TYPECODE, paymentResponseStatus);
			return Optional.ofNullable(paymentResponseData);
		}
		return Optional.empty();
	}

	private void validateTransactionStatus(final Map<String, Object> data, final AbstractOrderModel abstractOrderModel,
			final TransactionOperation operation) throws PaymentException
	{
		LOG.info("Validating transaction status .");
		Preconditions.checkArgument(!data.isEmpty(), "Response data map in empty");
		Preconditions.checkArgument(operation != null, "operation is null");
		final String orderStatus = (String) data.get("order_status");
		final TransactionStatus transactionStatus = getTransactionStatus(orderStatus);

		switch (operation)
		{
			case CAPTURE:


			case CANCEL:

				if (TransactionStatus.CANCELED.equals(transactionStatus))
				{
					paymentTransactionStrategy.createCancelEntryIfNotExists(abstractOrderModel);
					LOG.error(String.format("Error when validating transaction status : %s",
							PaymentExceptionType.THE_ORDER_IS_ALLREADY_CANCELED.getMessage()));
					throw new PaymentException(PaymentExceptionType.THE_ORDER_IS_ALLREADY_CANCELED.getMessage(),
							PaymentExceptionType.THE_ORDER_IS_ALLREADY_CANCELED);
				}
				if (TransactionStatus.SHIPPED.equals(transactionStatus))
				{
					paymentTransactionStrategy.createCaptureEntryIfNotExists(abstractOrderModel);
					LOG.error(String.format("Error when validating transaction status : %s",
							PaymentExceptionType.THE_ORDER_IS_ALLREADY_CAPTUARED.getMessage()));
					throw new PaymentException(PaymentExceptionType.THE_ORDER_IS_ALLREADY_CAPTUARED.getMessage(),
							PaymentExceptionType.THE_ORDER_IS_ALLREADY_CAPTUARED);
				}
				if (TransactionStatus.UNSUCCESSFUL.equals(transactionStatus))
				{
					LOG.error(String.format("Error when validating transaction status : %s",
							PaymentExceptionType.THE_ORDER_IS_NOT_AUTHORIZED.getMessage()));
					throw new PaymentException(PaymentExceptionType.THE_ORDER_IS_NOT_AUTHORIZED.getMessage(),
							PaymentExceptionType.THE_ORDER_IS_NOT_AUTHORIZED);
				}
				if (TransactionStatus.REFUNDED.equals(transactionStatus))
				{
					LOG.error(String.format("Error when validating transaction status : %s",
							PaymentExceptionType.THE_ORDER_IS_ALLREADY_REFUNDED.getMessage()));
					throw new PaymentException(PaymentExceptionType.THE_ORDER_IS_ALLREADY_REFUNDED.getMessage(),
							PaymentExceptionType.THE_ORDER_IS_ALLREADY_REFUNDED);
				}

				break;
			case REFUND_STANDALONE:
				if (TransactionStatus.UNSUCCESSFUL.equals(transactionStatus))
				{
					LOG.error(String.format("Error when validating transaction status : %s",
							PaymentExceptionType.THE_ORDER_IS_NOT_AUTHORIZED.getMessage()));
					throw new PaymentException(PaymentExceptionType.THE_ORDER_IS_NOT_AUTHORIZED.getMessage(),
							PaymentExceptionType.THE_ORDER_IS_NOT_AUTHORIZED);
				}
				if (TransactionStatus.CANCELED.equals(transactionStatus))
				{
					LOG.error(String.format("Error when validating transaction status : %s",
							PaymentExceptionType.THE_ORDER_IS_ALLREADY_CANCELED.getMessage()));
					throw new PaymentException(PaymentExceptionType.THE_ORDER_IS_ALLREADY_CANCELED.getMessage(),
							PaymentExceptionType.THE_ORDER_IS_ALLREADY_CANCELED);
				}
				if (TransactionStatus.SUCCESSFUL.equals(transactionStatus))
				{
					LOG.error(String.format("Error when validating transaction status : %s",
							PaymentExceptionType.ORDER_IS_AUTHORIZED.getMessage()));
					throw new PaymentException(PaymentExceptionType.ORDER_IS_AUTHORIZED.getMessage(),
							PaymentExceptionType.ORDER_IS_AUTHORIZED);
				}

				break;
		}


	}

	/**
	 *
	 */
	private TransactionStatus getTransactionStatus(final String orderStatus)
	{
		if (StringUtils.isBlank(orderStatus))
		{
			return TransactionStatus.UNSUCCESSFUL;
		}
		try
		{
			return TransactionStatus.valueOf(orderStatus.toUpperCase());
		}
		catch (final IllegalArgumentException e)
		{
			LOG.warn(String.format("Order status [%s] is undefined.", orderStatus));
			return TransactionStatus.UNSUCCESSFUL;
		}

	}

	@Override
	public Optional<PaymentResponseData> partialRefundOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel, final BigDecimal amount) throws PaymentException
	{
		final Optional<PaymentResponseData> paymentOrderStatusResponseData = getPaymentOrderStatusResponseData(paymentProviderModel,
				abstractOrderModel, abstractOrderModel.getPaymentReferenceId());
		if (paymentOrderStatusResponseData.isPresent())
		{
			final Map<String, Object> responseData = paymentOrderStatusResponseData.get().getResponseData();
			validateTransactionStatus(responseData, abstractOrderModel, TransactionOperation.REFUND_STANDALONE);
			final CCAvenuePaymentProviderModel ccavenuePaymentProviderModel = (CCAvenuePaymentProviderModel) paymentProviderModel;
			final Optional<Map<String, Object>> orderRefundData = ccavenueService.getOrderRefundData(
					getCCAvenueWorkingKeyByCart(ccavenuePaymentProviderModel, abstractOrderModel),
					getCCAvenueAccessCodeByCart(ccavenuePaymentProviderModel, abstractOrderModel),
					abstractOrderModel.getPaymentReferenceId(), getTotalPrice(abstractOrderModel, amount.doubleValue()).toString(),
					((int) (Math.random() * 300000)) + "");
			final Integer refundStatus = (Integer) orderRefundData.get().get("refund_status");
			if (refundStatus == null || refundStatus != 0)
			{
				throw new PaymentException(PaymentExceptionType.THE_ORDER_CAN_NOT_BE_CANCELED.getMessage(),
						PaymentExceptionType.THE_ORDER_CAN_NOT_BE_REFUNDED);
			}
			final PaymentResponseStatus paymentResponseStatus = getCancelResponseStatus(orderRefundData.get());
			final de.hybris.platform.payment.dto.TransactionStatus transactionStatus = paymentResponseStatus == null
					|| PaymentResponseStatus.FAILURE.equals(paymentResponseStatus)
							? de.hybris.platform.payment.dto.TransactionStatus.REJECTED
							: de.hybris.platform.payment.dto.TransactionStatus.ACCEPTED;

			final TransactionStatusDetails transactionStatusDetails = paymentResponseStatus == null
					|| PaymentResponseStatus.FAILURE.equals(paymentResponseStatus) ? TransactionStatusDetails.INVALID_REQUEST
							: TransactionStatusDetails.SUCCESFULL;
			final String requestPaymentBody = null;
			final String responsePaymentBody = responseData.toString();
			paymentTransactionStrategy.savePaymentTransactionEntry(abstractOrderModel, abstractOrderModel.getPaymentReferenceId(),
					PaymentTransactionType.REFUND_STANDALONE, transactionStatus, transactionStatusDetails, requestPaymentBody,
					responsePaymentBody, null);

			modelService.refresh(abstractOrderModel);
			abstractOrderModel.setResponsePaymentBody(orderRefundData.get().toString());
			modelService.save(abstractOrderModel);



			final PaymentResponseData paymentResponseData = new PaymentResponseData(orderRefundData.get(),
					CCAvenuePaymentProviderModel._TYPECODE, paymentResponseStatus);
			return Optional.ofNullable(paymentResponseData);

		}
		return Optional.empty();
	}

	@Override
	public Optional<PaymentLinkResponseData> getGeneratedPaymentLink(final AbstractOrderModel abstractOrderModel,
			final PaymentProviderModel paymentProviderModel) throws PaymentException
	{
		Preconditions.checkArgument(abstractOrderModel != null, "Order is empty");
		Preconditions.checkArgument(abstractOrderModel.getUser() != null, "Order User is empty");
		Preconditions.checkArgument(abstractOrderModel.getUser() instanceof CustomerModel, "Order User is empty");
		Preconditions.checkArgument(paymentProviderModel != null, "Payment Provider is empty");
		Preconditions.checkArgument(paymentProviderModel instanceof CCAvenuePaymentProviderModel,
				"Invalid CCAvenue Payment Provider");

		final CCAvenuePaymentProviderModel provider = (CCAvenuePaymentProviderModel) paymentProviderModel;
		final CustomerModel user = (CustomerModel) abstractOrderModel.getUser();
		final Optional<Map<String, Object>> invoiceLink = ccavenueService.getPaymentGeneratedInvoiceLink(
				getCCAvenueWorkingKeyByCart(provider, abstractOrderModel), getCCAvenueAccessCodeByCart(provider, abstractOrderModel),
				user.getDisplayName(), user.getUid(), user.getMobileNumber(), abstractOrderModel.getCurrency().getIsocode(),
				String.valueOf(abstractOrderModel.getTotalPrice()));
		if (invoiceLink.isEmpty())
		{
			throw new PaymentException("Empty response for payment link",
					PaymentExceptionType.INVALID_RESPONSE_SYNTAX_EXPECTED_JSON);
		}

		modelService.refresh(abstractOrderModel);
		abstractOrderModel.setResponsePaymentBody(invoiceLink.get().toString());
		modelService.save(abstractOrderModel);

		final Optional<PaymentLinkResponseData> paymentLinkResponseData = buildPaymentLinkResponseData(invoiceLink.get());
		try
		{
			otpContext.sendPaymentLinkNotificationMessage(abstractOrderModel, paymentLinkResponseData.get().getInvoiceId(),
					paymentLinkResponseData.get().getPaymentUrl());
		}
		catch (final OTPException e)
		{
			// [Monzer] caught OTPException so the messages service does not affect bad on the payment link generation process
			LOG.error("DefaultCCAvenuePaymentStrategy: Notification messages hasnot been sent due to : " + e.getMessage());
		}

		return paymentLinkResponseData;
	}

	@Override
	public Optional<ApplePayPaymentResponseData> getApplePayTransactionResponse(final AbstractOrderModel abstractOrder,
			final PaymentProviderModel paymentProviderModel, final ApplePayRequest requestData) throws PaymentException
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(abstractOrder != null, ABSTRACTORDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(requestData != null, "apple order id is empty");
		Preconditions.checkArgument(requestData.getOrderId() != null, "apple order id is empty");
		Preconditions.checkArgument(requestData.getPaymentOption() != null, "payment Option is empty");
		Preconditions.checkArgument(requestData.getAppleCardType() != null, "card Type is empty");
		Preconditions.checkArgument(requestData.getCardName() != null, "card Name is empty");
		Preconditions.checkArgument(requestData.getPaymentData() != null, "payment Data is empty");

		final CCAvenuePaymentProviderModel ccavenuePaymentProviderModel = (CCAvenuePaymentProviderModel) paymentProviderModel;

		final CCAvenueApplePayRequest applePayRequest = getCCAvenueApplePayRequest(requestData, abstractOrder,
				ccavenuePaymentProviderModel);
		final Optional<Map<String, Object>> applePayResponse = ccavenueService
				.getApplePayPaymentWebView(getccAvenueCredentials(abstractOrder, ccavenuePaymentProviderModel), applePayRequest);

		if (applePayResponse.isEmpty() || !(boolean) applePayResponse.get().get("success")
				|| StringUtils.isBlank((String) applePayResponse.get().get("webSource")))
		{
			LOG.error("Could not fetch the CCAvenue Apply Pay web source");
			final String requestPaymentBody = "PaymentReferenceId=[" + abstractOrder.getCode() + "], TotalPrice["
					+ getTotalPrice(abstractOrder) + "]";
			paymentTransactionStrategy.savePaymentTransactionEntry(abstractOrder, abstractOrder.getCode(),
					PaymentTransactionType.CAPTURE, de.hybris.platform.payment.dto.TransactionStatus.REJECTED,
					TransactionStatusDetails.INVALID_REQUEST, requestPaymentBody, applePayResponse.toString(), null);
			throw new PaymentException(PaymentExceptionType.COULD_NOT_FETCH_WEB_SOURCE.getMessage(),
					PaymentExceptionType.COULD_NOT_FETCH_WEB_SOURCE);
		}

		final Map<String, Object> response = applePayResponse.get();

		modelService.refresh(abstractOrder);
		abstractOrder.setResponsePaymentBody(response.toString());
		modelService.save(abstractOrder);
		final ApplePayPaymentResponseData paymentResponseData = new ApplePayPaymentResponseData();
		paymentResponseData.setWebSource((String) response.get("webSource"));
		paymentResponseData.setSuccess((boolean) response.get("success"));
		return Optional.ofNullable(paymentResponseData);
	}

	private Optional<PaymentLinkResponseData> buildPaymentLinkResponseData(final Map<String, Object> map)
	{
		if (map.isEmpty())
		{
			return Optional.empty();
		}
		final LinkedHashMap<String, Object> resultMap = (LinkedHashMap<String, Object>) (map.get("Generate_Invoice_Result"));
		final PaymentLinkResponseData response = new PaymentLinkResponseData();
		response.setErrorCode(String.valueOf(resultMap.get("error_code") == null ? "" : resultMap.get("error_code")));
		response.setErrorDesc(String.valueOf(resultMap.get("error_desc") == null ? "" : resultMap.get("error_desc")));
		response.setInvoiceId(String.valueOf(resultMap.get("invoice_id")));
		response.setInvoiceStatus(String.valueOf(resultMap.get("invoice_status")));
		response.setPaymentUrl(String.valueOf(resultMap.get("tiny_url")));
		return Optional.of(response);
	}

	private CCAvenueCredentials getccAvenueCredentials(final AbstractOrderModel abstractOrder,
			final CCAvenuePaymentProviderModel provider)
	{
		final CCAvenueCredentials avenueCredentials = new CCAvenueCredentials();
		avenueCredentials.setWorkingKey(getCCAvenueWorkingKeyByCart(provider, abstractOrder));
		avenueCredentials.setAccessCode(getCCAvenueAccessCodeByCart(provider, abstractOrder));
		avenueCredentials.setRedirectUrl(provider.getRedirectUrl());
		avenueCredentials.setMerchantId(getCCAvenueMerchantIdByCart(provider, abstractOrder));

		return avenueCredentials;
	}

	private CCAvenueApplePayRequest getCCAvenueApplePayRequest(final ApplePayRequest requestData,
			final AbstractOrderModel abstractOrderModel, final CCAvenuePaymentProviderModel ccavenuePaymentProviderModel)
	{
		final CCAvenueApplePayRequest applePayRequest = new CCAvenueApplePayRequest();
		applePayRequest.setAmount(String.valueOf(abstractOrderModel.getTotalPrice().doubleValue()));
		applePayRequest.setAppleCardType(requestData.getAppleCardType());
		applePayRequest.setCardName(requestData.getCardName());
		applePayRequest.setCardType(requestData.getCardType());
		applePayRequest.setCurrency(abstractOrderModel.getCurrency().getIsocode());
		applePayRequest.setOrderId(requestData.getOrderId());
		applePayRequest.setPaymentData(requestData.getPaymentData());
		applePayRequest.setPaymentOption(requestData.getPaymentOption());

		final CustomerModel customer = (CustomerModel) abstractOrderModel.getUser();
		final AddressModel deliveryAddress = getCartAddressModel(abstractOrderModel);
		final AddressModel paymentAddress = deliveryAddress;

		// Billing parameters
		applePayRequest.setBillingName(customer.getDisplayName());
		applePayRequest.setBillingAddress(paymentAddress == null ? "" : paymentAddress.getAddressName());
		applePayRequest
				.setBillingCity(paymentAddress == null || paymentAddress.getCity() == null ? "" : paymentAddress.getCity().getName());
		applePayRequest.setBillingCountry(
				paymentAddress == null || paymentAddress.getCountry() == null ? "" : paymentAddress.getCountry().getName());
		applePayRequest.setBillingPhoneNumber(
				paymentAddress == null || StringUtils.isBlank(paymentAddress.getMobile()) ? customer.getMobileNumber()
						: paymentAddress.getMobile());
		applePayRequest.setBillingEmail(customer.getUid());

		// Delivery parameters
		applePayRequest.setDeliveryName(customer.getDisplayName());
		applePayRequest.setDeliveryAddress(deliveryAddress == null ? "" : deliveryAddress.getAddressName());
		applePayRequest.setDeliveryCity(
				deliveryAddress == null || deliveryAddress.getCity() == null ? "" : deliveryAddress.getCity().getName());
		applePayRequest.setDeliveryCountry(
				deliveryAddress == null || deliveryAddress.getCountry() == null ? "" : deliveryAddress.getCountry().getName());
		applePayRequest.setDeliveryPhoneNumber(
				deliveryAddress == null || StringUtils.isBlank(deliveryAddress.getMobile()) ? customer.getMobileNumber()
						: deliveryAddress.getMobile());
		applePayRequest.setDeliveryEmail(customer.getUid());

		applePayRequest.setRedirectUrl(ccavenuePaymentProviderModel.getApplePayRedirectUrl());

		applePayRequest.setUserAgentHeader(ccavenuePaymentProviderModel.getUserAgentHeader());
		applePayRequest.setUseUserAgentHeader(ccavenuePaymentProviderModel.isUseUserAgentHeader());
		return applePayRequest;
	}

	private AddressModel getCartAddressModel(final AbstractOrderModel cart)
	{
		if (cart == null || cart.getShipmentType() == null)
		{
			return null;
		}
		if (ShipmentType.PICKUP_IN_STORE.equals(cart.getShipmentType()))
		{

			return getDefaultDeliveryPointOfServiceAddressModel(cart);

		}
		else
		{
			return cart.getDeliveryAddress();
		}

	}

	private AddressModel getDefaultDeliveryPointOfServiceAddressModel(final AbstractOrderModel cart)
	{
		return cart.getSite().getDefaultDeliveryPointOfService() == null
				|| cart.getSite().getDefaultDeliveryPointOfService().getAddress() == null ? null
						: cart.getSite().getDefaultDeliveryPointOfService().getAddress();

	}

	private String getCCAvenueAccessCodeByCart(final CCAvenuePaymentProviderModel paymentProvider, final AbstractOrderModel order)
	{
		return APPLE.equalsIgnoreCase(order.getPaymentMode().getCode()) ? paymentProvider.getAppleAccessCode()
				: paymentProvider.getAccessCode();
	}

	private String getCCAvenueWorkingKeyByCart(final CCAvenuePaymentProviderModel paymentProvider, final AbstractOrderModel order)
	{
		return APPLE.equalsIgnoreCase(order.getPaymentMode().getCode()) ? paymentProvider.getAppleWorkingKey()
				: paymentProvider.getWorkingKey();
	}

	private String getCCAvenueMerchantIdByCart(final CCAvenuePaymentProviderModel paymentProvider, final AbstractOrderModel order)
	{
		return APPLE.equalsIgnoreCase(order.getPaymentMode().getCode()) ? paymentProvider.getAppleMerchantId()
				: paymentProvider.getMerchantId();
	}

	@Override
	public List<CustomerPaymentOptionData> getCustomerPaymentOptions(final CustomerModel customerModel,
			final PaymentProviderModel paymentProviderModel) throws PaymentException
	{
		Preconditions.checkArgument(customerModel != null, "customerModel is empty");
		Preconditions.checkArgument(paymentProviderModel != null, "Payment Provider is empty");
		Preconditions.checkArgument(paymentProviderModel instanceof CCAvenuePaymentProviderModel,
				"Invalid CCAvenue Payment Provider");

		final CCAvenuePaymentProviderModel provider = (CCAvenuePaymentProviderModel) paymentProviderModel;


		final String requestPaymentOptionsBody = "WorkingKey=[" + provider.getWorkingKey() + "], AccessCode=["
				+ provider.getAccessCode() + "], customerId=[" + customerModel.getContactEmail() + "]";
		modelService.refresh(customerModel);
		customerModel.setRequestGetPaymentOptionsBody(requestPaymentOptionsBody);
		modelService.save(customerModel);

		final Optional<Map<String, Object>> responseData = ccavenueService.getCustomerPaymentOptions(provider.getWorkingKey(),
				provider.getAccessCode(), customerModel.getContactEmail());

		modelService.refresh(customerModel);
		customerModel.setResponseGetPaymentOptionsBody(responseData.get().toString());
		modelService.save(customerModel);

		if (responseData.isEmpty())
		{
			throw new PaymentException("Empty response for get Customer Payment Options",
					PaymentExceptionType.INVALID_RESPONSE_SYNTAX_EXPECTED_JSON);
		}

		return buildCustomerPaymentOptionsResponseData(responseData.get());
	}



	private List<CustomerPaymentOptionData> buildCustomerPaymentOptionsResponseData(final Map<String, Object> resultMap)
			throws PaymentException
	{
		final CustomerPaymentOptionsData response = new CustomerPaymentOptionsData();
		response.setErrorCode(String.valueOf(resultMap.get("error_code") == null ? "" : resultMap.get("error_code")));
		response.setErrorDesc(String.valueOf(resultMap.get("error_desc") == null ? "" : resultMap.get("error_desc")));
		response.setCustomerId(String.valueOf(resultMap.get("customer_id") == null ? "" : resultMap.get("customer_id")));

		if (StringUtils.isNotBlank(response.getErrorCode()))
		{
			throw new PaymentException("Get Customer Payment Options is failed! ErrorCode[" + response.getErrorCode()
					+ "] , ErrorDesc[" + response.getErrorDesc() + "]", PaymentExceptionType.FAILURE_RESULT);
		}

		final List<LinkedHashMap> customerPaymentOptions = ((List<LinkedHashMap>) resultMap.get("pay_Opt_List"));

		CustomerPaymentOptionData object = null;
		final List<CustomerPaymentOptionData> options = new ArrayList<>();
		for (final LinkedHashMap linkedHashMap : customerPaymentOptions)
		{
			object = new CustomerPaymentOptionData();
			object.setCustomerCardName(
					String.valueOf(linkedHashMap.get("customer_card_name") == null ? "" : linkedHashMap.get("customer_card_name")));
			object.setCustomerCardType(
					String.valueOf(linkedHashMap.get("customer_card_type") == null ? "" : linkedHashMap.get("customer_card_type")));
			object.setCustomerCardId(
					String.valueOf(linkedHashMap.get("customer_card_id") == null ? "" : linkedHashMap.get("customer_card_id")));
			object.setCustomerEmail(
					String.valueOf(linkedHashMap.get("customer_email") == null ? "" : linkedHashMap.get("customer_email")));
			object.setCustomerPayoptType(String
					.valueOf(linkedHashMap.get("customer_payopt_type") == null ? "" : linkedHashMap.get("customer_payopt_type")));
			object.setCustomerCardLabel(
					String.valueOf(linkedHashMap.get("customer_card_label") == null ? "" : linkedHashMap.get("customer_card_label")));
			object.setCustomerCardNo(
					String.valueOf(linkedHashMap.get("customer_card_no") == null ? "" : linkedHashMap.get("customer_card_no")));
			options.add(object);
		}

		response.setOptions(options);

		return options;
	}

	@Override
	public void deleteCustomerPaymentOption(final CustomerModel customerModel, final String customerCardId,
			final PaymentProviderModel paymentProviderModel) throws PaymentException
	{
		Preconditions.checkArgument(customerModel != null, "customerModel is empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(customerCardId), "customerCardId is empty");
		Preconditions.checkArgument(paymentProviderModel != null, "Payment Provider is empty");
		Preconditions.checkArgument(paymentProviderModel instanceof CCAvenuePaymentProviderModel,
				"Invalid CCAvenue Payment Provider");

		final CCAvenuePaymentProviderModel provider = (CCAvenuePaymentProviderModel) paymentProviderModel;


		final String requestPaymentOptionsBody = "WorkingKey=[" + provider.getWorkingKey() + "], AccessCode=["
				+ provider.getAccessCode() + "], customerId=[" + customerModel.getContactEmail() + "], customerCardId["
				+ customerCardId + "]";
		modelService.refresh(customerModel);
		customerModel.setRequestDeletePaymentOptionsBody(requestPaymentOptionsBody);
		modelService.save(customerModel);

		final Optional<Map<String, Object>> responseData = ccavenueService.deleteCustomerPaymentOption(provider.getWorkingKey(),
				provider.getAccessCode(), customerModel.getContactEmail(), customerCardId);

		modelService.refresh(customerModel);
		customerModel.setResponseDeletePaymentOptionsBody(responseData.get().toString());
		modelService.save(customerModel);

		if (responseData.isEmpty())
		{
			throw new PaymentException("Empty response for get Customer Payment Options",
					PaymentExceptionType.INVALID_RESPONSE_SYNTAX_EXPECTED_JSON);
		}

		final String deletionStatus = String
				.valueOf(responseData.get().get("deletion_status") == null ? "" : responseData.get().get("deletion_status"));
		final String errorCode = String
				.valueOf(responseData.get().get("error_code") == null ? "" : responseData.get().get("error_code"));
		final String errorDesc = String
				.valueOf(responseData.get().get("error_desc") == null ? "" : responseData.get().get("error_desc"));

		if (!"0".equals(deletionStatus))
		{
			throw new PaymentException(
					"Delete Customer Payment Option is failed! ErrorCode[" + errorCode + "] , ErrorDesc[" + errorDesc + "]",
					PaymentExceptionType.FAILURE_RESULT);
		}

	}

	/**
	 * @param entries
	 * @return
	 */
	private AddressModel getDeilveryAddress(final AbstractOrderModel abstractOrderModel)
	{

		return ShipmentType.PICKUP_IN_STORE.equals(abstractOrderModel.getShipmentType())
				? getAddressFromPOS(abstractOrderModel.getEntries())
				: abstractOrderModel.getDeliveryAddress();
	}

	private AddressModel getAddressFromPOS(final List<AbstractOrderEntryModel> entries)
	{

		for (final AbstractOrderEntryModel abstractOrderEntryModel : entries)
		{
			if (abstractOrderEntryModel.getDeliveryPointOfService() != null)
			{
				return abstractOrderEntryModel.getDeliveryPointOfService().getAddress();
			}
		}

		return null;

	}

	private AddressModel getPaymentAddress(final AbstractOrderModel abstractOrderModel)
	{
		return ShipmentType.PICKUP_IN_STORE.equals(abstractOrderModel.getShipmentType())
				? getAddressFromPOS(abstractOrderModel.getEntries())
				: abstractOrderModel.getPaymentAddress();
	}
}
