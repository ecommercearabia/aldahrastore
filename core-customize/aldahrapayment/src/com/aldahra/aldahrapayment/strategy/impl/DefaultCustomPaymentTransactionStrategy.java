/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrapayment.strategy.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.strategies.impl.DefaultPaymentTransactionStrategy;
import de.hybris.platform.acceleratorservices.payment.data.OrderInfoData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.CollectionUtils;

import com.aldahra.aldahrapayment.strategy.CustomPaymentTransactionStrategy;


public class DefaultCustomPaymentTransactionStrategy extends DefaultPaymentTransactionStrategy
		implements CustomPaymentTransactionStrategy
{

	@Resource(name = "cartService")
	private CartService cartService;

	@Override
	public PaymentTransactionEntryModel savePaymentTransactionEntry(final CustomerModel customerModel, final String requestId,
			final OrderInfoData orderInfoData)
	{
		validateParameterNotNull(orderInfoData, "orderInfoData cannot be null");


		return createTransacionForNewRequest(cartService.getSessionCart(), customerModel, requestId, orderInfoData);

	}

	@Override
	public Optional<PaymentTransactionEntryModel> savePaymentTransactionEntry(final AbstractOrderModel orderModel,
			final String requestId, final PaymentTransactionType paymentTransactionType, final TransactionStatus transactionStatus,
			final TransactionStatusDetails transactionStatusDetails, final String requestPaymentBody,
			final String responsePaymentBody, final String entryCode)
	{

		validateParameterNotNull(orderModel, "orderModel cannot be null");
		validateParameterNotNull(requestId, "requestId cannot be null");
		validateParameterNotNull(paymentTransactionType, "paymentTransactionType cannot be null");
		validateParameterNotNull(transactionStatus, "transactionStatus cannot be null");
		validateParameterNotNull(transactionStatusDetails, "transactionStatusDetails cannot be null");

		Optional<PaymentTransactionModel> paymentTransactionToUpdate = getPaymentTransactionToUpdate(orderModel, requestId);
		PaymentTransactionModel paymentTransactionModel = null;
		if (paymentTransactionToUpdate.isEmpty())
		{
			paymentTransactionModel = getModelService().create(PaymentTransactionModel.class);
			paymentTransactionModel.setOrder(orderModel);
			paymentTransactionModel.setCode(orderModel.getCode() + "_" + UUID.randomUUID());
			paymentTransactionModel.setRequestId(orderModel.getCode());
			paymentTransactionModel.setRequestId(requestId);
			paymentTransactionModel.setPaymentProvider(getCommerceCheckoutService().getPaymentProvider());
			if (orderModel.getCurrency() != null)
			{
				paymentTransactionModel.setCurrency(orderModel.getCurrency());
			}
			paymentTransactionModel.setPaymentProvider(getCommerceCheckoutService().getPaymentProvider());

			getModelService().saveAll(paymentTransactionModel);
			getModelService().refresh(paymentTransactionModel);
			paymentTransactionToUpdate = getPaymentTransactionToUpdate(orderModel, requestId);

		}
		else
		{
			paymentTransactionModel = paymentTransactionToUpdate.get();
		}
		final PaymentTransactionEntryModel entry = getModelService().create(PaymentTransactionEntryModel.class);
		entry.setAmount(BigDecimal.valueOf(orderModel.getTotalPrice().doubleValue()));
		if (orderModel.getCurrency() != null)
		{
			entry.setCurrency(orderModel.getCurrency());
		}
		entry.setType(paymentTransactionType);
		entry.setTime(new Date());
		entry.setPaymentTransaction(paymentTransactionModel);
		entry.setRequestId(paymentTransactionModel.getRequestId());
		entry.setTransactionStatus(transactionStatus.name());
		entry.setTransactionStatusDetails(transactionStatusDetails.name());
		entry.setCode(StringUtils.isBlank(entryCode) ? String.valueOf(UUID.randomUUID()) : entryCode);
		entry.setTime(new Date());
		entry.setResponsePaymentBody(responsePaymentBody);
		entry.setRequestPaymentBody(requestPaymentBody);
		entry.setPaymentTransaction(paymentTransactionModel);
		getModelService().saveAll(entry);
		getModelService().saveAll(paymentTransactionModel);
		getModelService().refresh(paymentTransactionModel);
		getModelService().refresh(entry);
		return Optional.ofNullable(entry);

	}

	protected Optional<PaymentTransactionModel> getPaymentTransactionToUpdate(final AbstractOrderModel orderModel,
			final String requestId)
	{
		final List<PaymentTransactionModel> paymentTransactions = orderModel.getPaymentTransactions();
		if (CollectionUtils.isEmpty(paymentTransactions) || StringUtils.isBlank(requestId))
		{
			return Optional.empty();
		}

		for (final PaymentTransactionModel paymentTransactionModel : paymentTransactions)
		{
			if (requestId.equals(paymentTransactionModel.getRequestId()))
			{
				return Optional.ofNullable(paymentTransactionModel);
			}
		}
		return Optional.empty();
	}

	protected final PaymentTransactionEntryModel createTransacionForNewRequest(final AbstractOrderModel orderModel,
			final CustomerModel customerModel, final String requestId, final OrderInfoData orderInfoData)
	{
		final Optional<PaymentTransactionModel> paymentTransactionToUpdate = getPaymentTransactionToUpdate(orderModel, requestId);
		PaymentTransactionModel paymentTransactionModel = null;
		if (paymentTransactionToUpdate.isEmpty())
		{
			paymentTransactionModel = getModelService().create(PaymentTransactionModel.class);
			paymentTransactionModel.setOrder(orderModel);
			paymentTransactionModel.setCode(orderModel.getCode() + "_" + UUID.randomUUID());
			paymentTransactionModel.setRequestId(orderModel.getCode());
			paymentTransactionModel.setRequestId(requestId);
			paymentTransactionModel.setRequestToken(orderInfoData.getOrderPageRequestToken());
			paymentTransactionModel.setPaymentProvider(getCommerceCheckoutService().getPaymentProvider());
			if (orderModel.getCurrency() != null)
			{
				paymentTransactionModel.setCurrency(orderModel.getCurrency());
			}
			paymentTransactionModel.setPaymentProvider(getCommerceCheckoutService().getPaymentProvider());
			getModelService().saveAll(paymentTransactionModel);
			getModelService().refresh(paymentTransactionModel);
		}
		else
		{
			paymentTransactionModel = paymentTransactionToUpdate.get();
		}

		final PaymentTransactionEntryModel paymentTransactionEntryModel = createTransactionEntryForNewRequest(orderModel,
				customerModel, requestId, orderInfoData, paymentTransactionModel);
		final List<PaymentTransactionEntryModel> entries = paymentTransactionModel.getEntries();
		if (CollectionUtils.isEmpty(entries))
		{
			paymentTransactionModel.setEntries(Collections.singletonList((paymentTransactionEntryModel)));
		}
		else
		{
			final List<PaymentTransactionEntryModel> list = new ArrayList<>();
			list.addAll(entries);
			list.add(paymentTransactionEntryModel);
			paymentTransactionModel.setEntries(list);
		}

		getModelService().saveAll(paymentTransactionModel);
		getModelService().refresh(paymentTransactionModel);
		return paymentTransactionModel == null || paymentTransactionModel.getEntries() == null
				|| paymentTransactionModel.getEntries().isEmpty() ? null : paymentTransactionModel.getEntries().get(0);
	}

	protected PaymentTransactionEntryModel createTransactionEntryForNewRequest(final AbstractOrderModel orderModel,
			final CustomerModel customerModel, final String requestId, final OrderInfoData orderInfoData,
			final PaymentTransactionModel paymentTransactionModel)
	{
		final PaymentTransactionEntryModel entry = getModelService().create(PaymentTransactionEntryModel.class);
		entry.setAmount(BigDecimal.valueOf(orderModel.getTotalPrice().doubleValue()));
		if (orderModel.getCurrency() != null)
		{
			entry.setCurrency(orderModel.getCurrency());
		}
		entry.setType(PaymentTransactionType.AUTHORIZATION);
		entry.setTime(new Date());
		entry.setPaymentTransaction(paymentTransactionModel);
		entry.setRequestId(paymentTransactionModel.getRequestId());
		entry.setTransactionStatus(TransactionStatus.ACCEPTED.name());
		entry.setTransactionStatusDetails(TransactionStatusDetails.SUCCESFULL.name());
		entry.setCode(String.valueOf(UUID.randomUUID()));


		entry.setRequestToken(orderInfoData.getOrderPageRequestToken());
		entry.setTime(new Date());
		entry.setPaymentTransaction(paymentTransactionModel);
		entry.setTransactionStatus(TransactionStatus.ACCEPTED.name());
		entry.setTransactionStatusDetails(TransactionStatusDetails.SUCCESFULL.name());

		return entry;
	}

	@Override
	public Optional<PaymentTransactionEntryModel> savePaymentTransactionEntry(final AbstractOrderModel orderModel,
			final String requestId, final PaymentTransactionType paymentTransactionType, final TransactionStatus transactionStatus,
			final String transactionStatusDetails, final String requestPaymentBody, final String responsePaymentBody,
			final String entryCode)
	{
		validateParameterNotNull(orderModel, "orderModel cannot be null");
		validateParameterNotNull(requestId, "requestId cannot be null");
		validateParameterNotNull(paymentTransactionType, "paymentTransactionType cannot be null");
		validateParameterNotNull(transactionStatus, "transactionStatus cannot be null");
		validateParameterNotNull(transactionStatusDetails, "transactionStatusDetails cannot be null");

		final Optional<PaymentTransactionModel> paymentTransactionToUpdate = getPaymentTransactionToUpdate(orderModel, requestId);
		if (paymentTransactionToUpdate.isEmpty())
		{
			return Optional.empty();
		}
		final PaymentTransactionModel paymentTransactionModel = paymentTransactionToUpdate.get();
		final PaymentTransactionEntryModel entry = getModelService().create(PaymentTransactionEntryModel.class);
		entry.setAmount(BigDecimal.valueOf(orderModel.getTotalPrice().doubleValue()));
		if (orderModel.getCurrency() != null)
		{
			entry.setCurrency(orderModel.getCurrency());
		}
		entry.setType(paymentTransactionType);
		entry.setTime(new Date());
		entry.setPaymentTransaction(paymentTransactionModel);
		entry.setRequestId(paymentTransactionModel.getRequestId());
		entry.setTransactionStatus(transactionStatus.name());
		entry.setTransactionStatusDetails(transactionStatusDetails);
		entry.setCode(StringUtils.isBlank(entryCode) ? String.valueOf(UUID.randomUUID()) : entryCode);
		entry.setTime(new Date());
		entry.setResponsePaymentBody(responsePaymentBody);
		entry.setRequestPaymentBody(requestPaymentBody);
		entry.setPaymentTransaction(paymentTransactionModel);
		getModelService().saveAll(entry);
		getModelService().saveAll(paymentTransactionModel);
		getModelService().refresh(paymentTransactionModel);
		getModelService().refresh(entry);
		return Optional.ofNullable(entry);

	}

	@Override
	public void createCaptureEntryIfNotExists(final AbstractOrderModel orderModel)
	{
		final List<PaymentTransactionModel> paymentTransactions = orderModel.getPaymentTransactions();
		outer: for (final PaymentTransactionModel paymentTransactionModel : paymentTransactions)
		{
			final long count = paymentTransactionModel.getEntries().stream()
					.filter(e -> PaymentTransactionType.CAPTURE.equals(e.getType())).count();
			if (count == 0)
			{
				savePaymentTransactionEntry(orderModel, orderModel.getPaymentReferenceId(), PaymentTransactionType.CAPTURE,
						de.hybris.platform.payment.dto.TransactionStatus.ACCEPTED, TransactionStatusDetails.CREDIT_FOR_VOIDED_CAPTURE,
						null, orderModel.getResponsePaymentBody(), null);
				orderModel.setStatus(OrderStatus.PAYMENT_CAPTURED);
				break outer;

			}

		}



	}

	@Override
	public void createCancelEntryIfNotExists(final AbstractOrderModel orderModel)
	{
		final List<PaymentTransactionModel> paymentTransactions = orderModel.getPaymentTransactions();
		outer: for (final PaymentTransactionModel paymentTransactionModel : paymentTransactions)
		{

			final long count = paymentTransactionModel.getEntries().stream()
					.filter(e -> PaymentTransactionType.CANCEL.equals(e.getType())).count();

			if (count == 0)
			{
				savePaymentTransactionEntry(orderModel, orderModel.getPaymentReferenceId(), PaymentTransactionType.CANCEL,
						de.hybris.platform.payment.dto.TransactionStatus.ACCEPTED, TransactionStatusDetails.CREDIT_FOR_VOIDED_CAPTURE,
						null, orderModel.getResponsePaymentBody(), null);
				orderModel.setStatus(OrderStatus.CANCELLED);
				break outer;
			}

		}

	}
}
