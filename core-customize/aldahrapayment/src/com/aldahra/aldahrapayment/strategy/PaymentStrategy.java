/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrapayment.strategy;

import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.aldahra.aldahraotp.exception.OTPException;
import com.aldahra.aldahrapayment.ccavenue.entry.ApplePayPaymentResponseData;
import com.aldahra.aldahrapayment.ccavenue.entry.ApplePayRequest;
import com.aldahra.aldahrapayment.ccavenue.entry.CustomerPaymentOptionData;
import com.aldahra.aldahrapayment.ccavenue.entry.PaymentLinkResponseData;
import com.aldahra.aldahrapayment.ccavenue.exception.PaymentException;
import com.aldahra.aldahrapayment.entry.PaymentRequestData;
import com.aldahra.aldahrapayment.entry.PaymentResponseData;
import com.aldahra.aldahrapayment.model.PaymentProviderModel;


/**
 * The Interface PaymentStrategy.
 *
 * @author mnasro
 * @author abu-muhasien
 *
 *         The Interface PaymentStrategy.
 */
public interface PaymentStrategy
{

	/**
	 * Builds the payment request data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrder
	 *           the abstract order
	 * @return the optional
	 */
	public Optional<PaymentRequestData> buildPaymentRequestData(final PaymentProviderModel paymentProviderModel,
			AbstractOrderModel abstractOrder);

	/**
	 * Builds the payment response data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param data
	 *           the data
	 * @return the optional
	 */
	public Optional<PaymentResponseData> buildPaymentResponseData(final PaymentProviderModel paymentProviderModel,
			AbstractOrderModel abstractOrderModel, final Object data);

	/**
	 * Interpret response.
	 *
	 * @param responseParams
	 *           the response params
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @return the optional
	 */
	public Optional<CreateSubscriptionResult> interpretResponse(final Map<String, Object> responseParams,
			PaymentProviderModel paymentProviderModel);


	/**
	 * Checks if is successful paid order.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @param data
	 *           the data
	 * @return true, if is successful paid order
	 */
	public boolean isSuccessfulPaidOrder(PaymentProviderModel paymentProviderModel, AbstractOrderModel abstractOrderModel,
			Object data);


	public Optional<PaymentResponseData> getPaymentOrderStatusResponseData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel, final Object data) throws PaymentException;

	public Optional<PaymentResponseData> captureOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException;

	public Optional<PaymentResponseData> cancelOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException;

	public Optional<PaymentResponseData> refundOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException;

	public Optional<PaymentResponseData> partialRefundOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel, final BigDecimal amount) throws PaymentException;

	public Optional<ApplePayPaymentResponseData> getApplePayTransactionResponse(final AbstractOrderModel abstractOrderModel,
			final PaymentProviderModel paymentProviderModel, ApplePayRequest requestData) throws PaymentException;

	/**
	 * @throws PaymentException
	 * @throws OTPException
	 *
	 */
	public Optional<PaymentLinkResponseData> getGeneratedPaymentLink(AbstractOrderModel abstractOrderModel,
			PaymentProviderModel paymentProviderModel) throws PaymentException;


	public List<CustomerPaymentOptionData> getCustomerPaymentOptions(final CustomerModel customerModel,
			final PaymentProviderModel paymentProviderModel) throws PaymentException;

	public void deleteCustomerPaymentOption(final CustomerModel customerModel, final String customerCardId,
			final PaymentProviderModel paymentProviderModel) throws PaymentException;


}
