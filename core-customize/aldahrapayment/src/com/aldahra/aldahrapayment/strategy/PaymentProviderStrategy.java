/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrapayment.strategy;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.aldahra.aldahrapayment.model.PaymentProviderModel;


/**
 * @author mnasro
 *
 *         The Interface PaymentProviderStrategy.
 */
public interface PaymentProviderStrategy
{

	/**
	 * Gets the active provider.
	 *
	 * @param baseStoreUid
	 *           the base store uid
	 * @return the active provider
	 */
	public Optional<PaymentProviderModel> getActiveProvider(String baseStoreUid);

	/**
	 * Gets the active provider.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @return the active provider
	 */
	public Optional<PaymentProviderModel> getActiveProvider(BaseStoreModel baseStoreModel);

	/**
	 * Gets the active provider by current base store.
	 *
	 * @return the active provider by current base store
	 */
	public Optional<PaymentProviderModel> getActiveProviderByCurrentBaseStore();

}
