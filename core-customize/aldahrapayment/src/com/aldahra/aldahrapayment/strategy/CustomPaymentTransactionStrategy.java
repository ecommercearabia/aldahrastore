package com.aldahra.aldahrapayment.strategy;

import de.hybris.platform.acceleratorservices.payment.strategies.PaymentTransactionStrategy;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

import java.util.Optional;



public interface CustomPaymentTransactionStrategy extends PaymentTransactionStrategy
{
	public Optional<PaymentTransactionEntryModel> savePaymentTransactionEntry(final AbstractOrderModel orderModel,
			final String requestId, final PaymentTransactionType paymentTransactionType, final TransactionStatus transactionStatus,
			final TransactionStatusDetails transactionStatusDetails, final String requestPaymentBody,
			final String responsePaymentBody, final String entryCode);

	public Optional<PaymentTransactionEntryModel> savePaymentTransactionEntry(final AbstractOrderModel orderModel,
			final String requestId, final PaymentTransactionType paymentTransactionType, final TransactionStatus transactionStatus,
			final String transactionStatusDetails, final String requestPaymentBody, final String responsePaymentBody,
			final String entryCode);

	public void createCaptureEntryIfNotExists(final AbstractOrderModel orderModel);

	public void createCancelEntryIfNotExists(final AbstractOrderModel orderModel);
}
