package com.aldahra.aldahrapayment.entry;

import java.io.Serializable;
import java.util.Map;

import com.aldahra.aldahrapayment.enums.PaymentResponseStatus;



/**
 * The Class PaymentResponseData.
 *
 * @author abu-muhasien
 */
public class PaymentResponseData implements Serializable
{

	/** The response data. */
	private Map<String, Object> responseData;

	/** The payment provider. */
	private String paymentProvider;
	private final PaymentResponseStatus status;

	public PaymentResponseStatus getStatus()
	{
		return status;
	}

	/**
	 * Instantiates a new payment response data.
	 *
	 * @param responseData
	 *           the response data
	 * @param paymentProvider
	 *           the payment provider
	 */
	public PaymentResponseData(final Map<String, Object> responseData, final String paymentProvider,
			final PaymentResponseStatus status)
	{
		super();
		this.responseData = responseData;
		this.paymentProvider = paymentProvider;
		this.status = status;
	}

	/**
	 * Gets the response data.
	 *
	 * @return the response data
	 */
	public Map<String, Object> getResponseData()
	{
		return responseData;
	}

	/**
	 * Sets the response data.
	 *
	 * @param responseData
	 *           the response data
	 */
	public void setResponseData(final Map<String, Object> responseData)
	{
		this.responseData = responseData;
	}

	/**
	 * Gets the payment provider.
	 *
	 * @return the payment provider
	 */
	public String getPaymentProvider()
	{
		return paymentProvider;
	}

	/**
	 * Sets the payment provider.
	 *
	 * @param paymentProvider
	 *           the new payment provider
	 */
	public void setPaymentProvider(final String paymentProvider)
	{
		this.paymentProvider = paymentProvider;
	}


}
