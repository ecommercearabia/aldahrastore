package com.aldahra.aldahrapayment.entry;

import java.io.Serializable;


/**
 * The Class PaymentRequestData.
 *
 * @author abu-muhasien
 *
 */
public class PaymentRequestData implements Serializable
{

	/** The script src. */
	private String scriptSrc;

	/** The payment provider. */
	private String paymentProvider;

	private Object data;

	/**
	 *
	 */
	public PaymentRequestData(final String scriptSrc, final String paymentProvider, final Object data)
	{
		super();
		this.scriptSrc = scriptSrc;
		this.paymentProvider = paymentProvider;
		this.data = data;
	}

	/**
	 * @return the data
	 */
	public Object getData()
	{
		return data;
	}

	/**
	 * @param data
	 *           the data to set
	 */
	public void setData(final Object data)
	{
		this.data = data;
	}

	/**
	 * Instantiates a new payment request data.
	 *
	 * @param scriptSrc
	 *           the script src
	 * @param paymentProvider
	 *           the payment provider
	 */
	public PaymentRequestData(final String scriptSrc, final String paymentProvider)
	{
		super();
		this.scriptSrc = scriptSrc;
		this.paymentProvider = paymentProvider;
	}

	/**
	 * Sets the payment provider.
	 *
	 * @param paymentProvider
	 *           the new payment provider
	 */
	public void setPaymentProvider(final String paymentProvider)
	{
		this.paymentProvider = paymentProvider;
	}

	/**
	 * Gets the payment provider.
	 *
	 * @return the payment provider
	 */
	public String getPaymentProvider()
	{
		return paymentProvider;
	}

	/**
	 * Gets the script src.
	 *
	 * @return the scriptSrc
	 */
	public String getScriptSrc()
	{
		return scriptSrc;
	}

	/**
	 * Sets the script src.
	 *
	 * @param scriptSrc
	 *           the scriptSrc to set
	 */
	public void setScriptSrc(final String scriptSrc)
	{
		this.scriptSrc = scriptSrc;
	}

}
