/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrapayment.ccavenue.service;

import java.util.Map;
import java.util.Optional;

import com.aldahra.aldahrapayment.ccavenue.entry.CCAvenueApplePayRequest;
import com.aldahra.aldahrapayment.ccavenue.entry.CCAvenueCredentials;
import com.aldahra.aldahrapayment.ccavenue.entry.RequestData;
import com.aldahra.aldahrapayment.ccavenue.exception.CCAvenueException;
import com.aldahra.aldahrapayment.ccavenue.exception.PaymentException;


/**
 * The Interface CCAvenueService.
 *
 * @author mnasro
 * @author alshati
 * @author abu-muhasien
 */
public interface CCAvenueService
{


	/**
	 * Gets the script src.
	 *
	 * @param requestData
	 *           the request data
	 * @return the script src
	 */
	public Optional<String> getScriptSrc(final RequestData requestData);

	/**
	 * Gets the response data.
	 *
	 * @param source
	 *           the source
	 * @param workingKey
	 *           the working key
	 * @return the response data
	 */
	public Optional<Map<String, Object>> getResponseData(String source, final String workingKey);

	/**
	 * Gets the response data.
	 *
	 * @param source
	 *           the source
	 * @param workingKey
	 *           the working key
	 * @return the response data
	 */
	public Optional<Map<String, Object>> getOrderStatusData(final String workingKey, final String accessCode,
			final String referanceNo, final String orderNo) throws PaymentException;

	public Optional<Map<String, Object>> getOrderCancelData(String workingKey, String accessCode, String referanceNo,
			String amount) throws PaymentException;

	public Optional<Map<String, Object>> getOrderRefundData(String workingKey, String accessCode, String referanceNo,
			String refundAmount, String refundRefNo) throws PaymentException;

	public Optional<Map<String, Object>> getOrderConfiemData(String workingKey, String accessCode, String referanceNo,
			String amount) throws PaymentException;

	public Optional<Map<String, Object>> getPaymentGeneratedInvoiceLink(String workingKey, String accessCode, String customerName,
			String customerEmail, String customerMobileNumber, String currency, String amount) throws PaymentException;

	public Optional<Map<String, Object>> getApplePayPaymentWebView(CCAvenueCredentials ccAvenueCredentials,
			CCAvenueApplePayRequest ccAvenueApplePayRequest);

	public Optional<Map<String, Object>> getCustomerPaymentOptions(String workingKey, String accessCode, String customerId)
			throws CCAvenueException;

	public Optional<Map<String, Object>> deleteCustomerPaymentOption(String workingKey, String accessCode, String customerId,
			String customerCardId) throws CCAvenueException;




}