/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrapayment.ccavenue.service.impl;

import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.aldahra.aldahrapayment.ccavenue.entry.CCAvenueApplePayRequest;
import com.aldahra.aldahrapayment.ccavenue.entry.CCAvenueCredentials;
import com.aldahra.aldahrapayment.ccavenue.entry.RequestData;
import com.aldahra.aldahrapayment.ccavenue.enums.CCAvenueConstants;
import com.aldahra.aldahrapayment.ccavenue.enums.PaymentExceptionType;
import com.aldahra.aldahrapayment.ccavenue.exception.CCAvenueException;
import com.aldahra.aldahrapayment.ccavenue.exception.PaymentException;
import com.aldahra.aldahrapayment.ccavenue.service.CCAvenueService;
import com.aldahra.aldahrawebserviceapi.util.WebServiceApiUtil;
import com.ccavenue.security.AesCryptUtil;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Preconditions;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;


/**
 * The Class DefaultCCAvenueService.
 *
 * @author mnasr
 * @author alshati
 * @author abu-muhasien
 *
 */
public class DefaultCCAvenueService implements CCAvenueService
{
	private static final String COMMAND = "command";

	private static final String PIPESIGN = "|";

	private static final String DOLLARSIGN = "$";

	private static final String REQUEST_TYPE = "request_type";

	private static final String RESPONSE_TYPE = "response_type";

	private static final String ACCESS_CODE = "access_code";

	private static final String REFUND_AMOUNT_RESPONSE_CAN_NOT_BE_NULL = "refundAmount response can not be null";

	private static final String REFUND_REF_NO_RESPONSE_CAN_NOT_BE_NULL = "refundRefNo response can not be null";

	private static final String AMOUNT_RESPONSE_CAN_NOT_BE_NULL = "amount response can not be null";

	private static final String ORDER_NO_RESPONSE_CAN_NOT_BE_NULL = "orderNo response can not be null";

	private static final String CUSTOMER_ID_RESPONSE_CAN_NOT_BE_NULL = "customerId response can not be null";

	private static final String CUSTOMER_CARD_ID_RESPONSE_CAN_NOT_BE_NULL = "customerCardId response can not be null";


	private static final String REFERANCE_NO_RESPONSE_CAN_NOT_BE_NULL = "referanceNo response can not be null";

	private static final String SOURCE_RESPONSE_CAN_NOT_BE_NULL = "source response can not be null";

	private static final String ACCESS_CODE_RESPONSE_CAN_NOT_BE_NULL = "accessCode response can not be null";

	private static final String WORKING_KEY_RESPONSE_CAN_NOT_BE_NULL = "workingKey response can not be null";
	private final static String VERSION = "1.1";
	private final static String STRING_TYPE = "STRING";
	private final static String JSON_TYPE = "JSON";
	/** The Constant AMPERSAND. */
	private static final String AMPERSAND = "&";

	/** The Constant EQUAL. */
	private static final String EQUAL = "=";

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(DefaultCCAvenueService.class);

	/** The Constant IFRAME_SRC_URL. */
	private static final String IFRAME_SRC_URL = "https://secure.ccavenue.ae/transaction/transaction.do";

	@Resource(name = "commerceCommonI18NService")
	private CommerceCommonI18NService commerceCommonI18NService;

	/** The Constant ACCESSCODE. */
	private static final String ACCESSCODE = CCAvenueConstants.ACCESS_CODE;

	/** The Constant WORKINGKEY. */
	private static final String WORKINGKEY = CCAvenueConstants.WORKING_KEY;
	private static final String PRODUCTION_SRC_URL = "https://login.ccavenue.ae/apis/servlet/DoWebTrans";
	private static final String APPLE_PAY_URL = "https://secure.ccavenue.ae/transaction/transaction.do?command=initiatePayloadTransaction";

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	private String getWebsiteRoot()
	{

		return cmsSiteService.getCurrentSite() == null ? configurationService.getConfiguration().getString("website.https")
				: configurationService.getConfiguration().getString("website." + cmsSiteService.getCurrentSite().getUid() + ".https");
	}

	private String getFullUrl(final String urlPostfix)
	{
		final String uid = cmsSiteService.getCurrentSite().getUid();
		final StringBuilder builder = new StringBuilder(getWebsiteRoot() + "/");
		builder.append(getCurrentLanguage() + "/" + urlPostfix);
		LOG.info(builder.toString());
		return builder.toString();
	}

	private String getCurrentLanguage()
	{
		return commerceCommonI18NService.getCurrentLanguage() == null ? "en"
				: commerceCommonI18NService.getCurrentLanguage().getIsocode();
	}

	/**
	 * Gets the script src.
	 *
	 * @param requestData
	 *           the request data
	 * @return the script src
	 */
	@Override
	public Optional<String> getScriptSrc(final RequestData requestData)
	{
		final String merchant_id = requestData.getMerchantId();
		final String order_id = requestData.getOrderId();
		final String currency = requestData.getCurrency();
		final String redirect_url = getFullUrl(requestData.getRedirectUrl());
		final String cancel_url = getFullUrl(requestData.getCancelUrl());
		final String language = requestData.getLanguage();
		final String billing_name = requestData.getBillingName();
		final String billing_address = requestData.getBillingAddress();
		final String billing_city = requestData.getBillingCity();
		final String billing_state = requestData.getBillingState();
		final String billing_zip = requestData.getBillingZip();
		final String billing_country = requestData.getBillingCountry();
		final String billing_tel = requestData.getBillingTel();
		final String billing_email = requestData.getBillingEmail();
		final String delivery_name = requestData.getDeliveryName();
		final String delivery_address = requestData.getDeliveryAddress();
		final String delivery_city = requestData.getDeliveryCity();
		final String delivery_state = requestData.getDeliveryState();
		final String delivery_zip = requestData.getDeliveryZip();
		final String delivery_country = requestData.getDeliveryCountry();
		final String delivery_tel = requestData.getDeliveryTel();
		final String merchant_param1 = requestData.getMerchantParam1();
		final String merchant_param2 = requestData.getMerchantParam2();
		final String merchant_param3 = requestData.getMerchantParam3();
		final String merchant_param4 = requestData.getMerchantParam4();
		final String frame_normal = requestData.getIntegrationType();
		final String promo_code = requestData.getPromoCode();
		final String customer_identifier = requestData.getCustomerIdentifier();
		final String amount = requestData.getAmount();
		final String accessCode = requestData.getAccessCode();
		final String workingKey = requestData.getWorkingKey();

		final StringBuilder ccaRequest = new StringBuilder();
		LOG.info("Ccavenue Request redirect_url : " + redirect_url);

		appendToRequest(ccaRequest, "order_id", order_id);
		appendToRequest(ccaRequest, "merchant_id", merchant_id);
		appendToRequest(ccaRequest, "currency", currency);
		appendToRequest(ccaRequest, "amount", amount);
		appendToRequest(ccaRequest, "redirect_url", redirect_url);
		appendToRequest(ccaRequest, "integration_type", frame_normal);
		appendToRequest(ccaRequest, "cancel_url", cancel_url);
		appendToRequest(ccaRequest, "language", language);
		appendToRequest(ccaRequest, "billing_name", billing_name);
		appendToRequest(ccaRequest, "billing_address", billing_address);
		appendToRequest(ccaRequest, "billing_city", billing_city);
		appendToRequest(ccaRequest, "billing_state", billing_state);
		appendToRequest(ccaRequest, "billing_zip", billing_zip);
		appendToRequest(ccaRequest, "billing_country", billing_country);
		appendToRequest(ccaRequest, "billing_tel", billing_tel);
		appendToRequest(ccaRequest, "billing_email", billing_email);
		appendToRequest(ccaRequest, "delivery_name", delivery_name);
		appendToRequest(ccaRequest, "delivery_address", delivery_address);
		appendToRequest(ccaRequest, "delivery_city", delivery_city);
		appendToRequest(ccaRequest, "delivery_state", delivery_state);
		appendToRequest(ccaRequest, "delivery_zip", delivery_zip);
		appendToRequest(ccaRequest, "delivery_country", delivery_country);
		appendToRequest(ccaRequest, "delivery_tel", delivery_tel);
		appendToRequest(ccaRequest, "merchant_param1", merchant_param1);
		appendToRequest(ccaRequest, "merchant_param2", merchant_param2);
		appendToRequest(ccaRequest, "merchant_param3", merchant_param3);
		appendToRequest(ccaRequest, "merchant_param4", merchant_param4);
		appendToRequest(ccaRequest, "promo_code", promo_code);
		ccaRequest.append("customer_identifier").append(EQUAL).append(customer_identifier);
		final AesCryptUtil aesUtil = new AesCryptUtil(workingKey);

		final String encRequest = aesUtil.encrypt(ccaRequest.toString());

		final StringBuffer src = new StringBuffer(IFRAME_SRC_URL);
		src.append("?command=initiateTransaction&merchant_id=").append(merchant_id).append("&encRequest=").append(encRequest)
				.append("&access_code=").append(accessCode);

		return Optional.ofNullable(src.toString());
	}

	/**
	 * Gets the response data.
	 *
	 * @param source
	 *           the source
	 * @param workingKey
	 *           the working key
	 * @return the response data
	 */
	@Override
	public Optional<Map<String, Object>> getResponseData(final String source, final String workingKey)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(source), SOURCE_RESPONSE_CAN_NOT_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNotBlank(workingKey), WORKING_KEY_RESPONSE_CAN_NOT_BE_NULL);

		final AesCryptUtil aesUtil = new AesCryptUtil(workingKey);
		final String src = aesUtil.decrypt(source);
		final Map<String, Object> map = new HashMap<>();
		Arrays.stream(src.split(AMPERSAND)).map(e -> e.split(EQUAL)).filter(e -> e.length == 2)
				.forEach(element -> map.put(element[0], element[1]));
		LOG.info("ResponseData has been called successfullt");
		return Optional.ofNullable(map);

	}

	/**
	 * Append to request.
	 *
	 * @param ccaRequest
	 *           the cca request
	 * @param paramName
	 *           the param name
	 * @param paramValue
	 *           the param value
	 */
	protected void appendToRequest(final StringBuilder ccaRequest, final String paramName, final String paramValue)
	{
		if (StringUtils.isNotBlank(paramValue))
		{
			ccaRequest.append(paramName).append(EQUAL).append(paramValue).append(AMPERSAND);
		}

	}

	@Override
	public Optional<Map<String, Object>> getOrderStatusData(final String workingKey, final String accessCode,
			final String referanceNo, final String orderNo) throws PaymentException
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(workingKey), WORKING_KEY_RESPONSE_CAN_NOT_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNotBlank(accessCode), ACCESS_CODE_RESPONSE_CAN_NOT_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNotBlank(referanceNo), REFERANCE_NO_RESPONSE_CAN_NOT_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNotBlank(orderNo), ORDER_NO_RESPONSE_CAN_NOT_BE_NULL);

		final String command = "orderStatusTracker";
		final Map<String, Object> responseEnc = new HashMap<>();

		try
		{
			final String statusSource = getStatusSource(referanceNo, orderNo, workingKey, accessCode, VERSION, command, JSON_TYPE,
					STRING_TYPE);
			LOG.info("Order Status Data url has been encrypted successfully");
			final ResponseEntity<?> httPOST = WebServiceApiUtil.httPOST(statusSource, null, null, String.class);
			final String response = (String) httPOST.getBody();
			LOG.info("DefaultCCAvenueService Order Status Data Response has been returned successfully");
			LOG.info(String.format("DefaultCCAvenueService The returned response  is : %s", response));
			Arrays.stream(response.split("&")).map(e -> e.split("=")).forEach(ex -> responseEnc.put(ex[0].trim(), ex[1].trim()));
			LOG.info(String.format("DefaultCCAvenueService The response after mapping it  is : %s", responseEnc));
			LOG.info(String.format("DefaultCCAvenueService the enc_response is : %s", responseEnc.get("enc_response")));
			final AesCryptUtil aesUtil = new AesCryptUtil(workingKey);
			final String src = aesUtil.decrypt(((String) responseEnc.get("enc_response")));

			if (StringUtils.isBlank(src))
			{
				LOG.error(String.format("DefaultCCAvenueService Error!!  %s", ((String) responseEnc.get("enc_response"))));
				LOG.error(String.format("DefaultCCAvenueService Error!!  %s", responseEnc.toString()));
				throw new CCAvenueException(PaymentExceptionType.BAD_REQUEST.getMessage(), PaymentExceptionType.BAD_REQUEST);
			}
			final ObjectMapper objectMapper = new ObjectMapper();
			LOG.info("DefaultCCAvenueService decrypting Order Status Data Response ");
			final Map<String, Object> readValue = objectMapper.readValue(src, Map.class);
			LOG.info("DefaultCCAvenueService decrypting Order Status Data Response successfuly and returned as json ");
			return Optional.ofNullable(readValue);
		}
		catch (final JsonParseException e)
		{
			LOG.error(String.format("DefaultCCAvenueService Error when parsing Response %s", e.getMessage()));
			throw new CCAvenueException(PaymentExceptionType.INVALID_RESPONSE_SYNTAX_EXPECTED_JSON.getMessage(),
					PaymentExceptionType.INVALID_RESPONSE_SYNTAX_EXPECTED_JSON);
		}
		catch (final JsonMappingException e)
		{
			LOG.error(String.format("DefaultCCAvenueService Error when Mapping Response %s", e.getMessage()));
			throw new CCAvenueException(PaymentExceptionType.BAD_REQUEST.getMessage(), PaymentExceptionType.BAD_REQUEST);
		}
		catch (final IOException e)
		{
			LOG.error(String.format("DefaultCCAvenueService Error!!  %s", e.getMessage()));
			throw new CCAvenueException(PaymentExceptionType.BAD_REQUEST.getMessage(), PaymentExceptionType.BAD_REQUEST);
		}
	}


	private Optional<Map<String, Object>> validateProcess(final String workingKey, final String statusSource)
	{
		final Map<String, Object> responseEnc = new HashMap<>();
		try
		{
			final ObjectMapper objectMapper = new ObjectMapper();
			final ResponseEntity<?> httPOST = WebServiceApiUtil.httPOST(statusSource, null, null, String.class);
			final String response = (String) httPOST.getBody();
			LOG.info(String.format("The returned response  is : %s", response));
			Arrays.stream(response.split("&")).map(e -> e.split("=")).forEach(ex -> responseEnc.put(ex[0].trim(), ex[1].trim()));
			LOG.info(String.format("The response after mapping it  is : %s", responseEnc));
			LOG.info(String.format("the enc_response is : %s", responseEnc.get("enc_response")));
			final AesCryptUtil aesUtil = new AesCryptUtil(workingKey);
			final String src = aesUtil.decrypt(((String) responseEnc.get("enc_response")));
			LOG.info(String.format("The decrybted source is : %s", src));
			final Map<String, Object> readValue = objectMapper.readValue(src, Map.class);
			return Optional.ofNullable(readValue);
		}
		catch (final JsonProcessingException e)
		{
			LOG.error(e.getMessage());
			return Optional.ofNullable(responseEnc);
		}
		catch (final IOException e)
		{
			LOG.error(e.getMessage());
			return Optional.ofNullable(responseEnc);
		}
		catch (final Exception e)
		{
			LOG.error(e.getMessage());
			return Optional.ofNullable(responseEnc);
		}
	}

	private Optional<Map<String, Object>> validateProcessWithBody(final String workingKey, final String sourceLink,
			final MultiValueMap<String, String> body, final String userAgentHeader, final boolean useUserHeaderAgent)
	{
		final Map<String, Object> responseEnc = new HashMap<>();
		HttpResponse<String> responseRest;
		try
		{
			responseRest = Unirest.post(sourceLink).header("Host", "secure.ccavenue.ae").header("Accept", "*/*")
					.header("Content-Type", "application/x-www-form-urlencoded").field("access_code", body.get("access_code"))
					.field("encRequest", body.get("encRequest")).asString();
		}
		catch (final Exception e)
		{
			LOG.error(e.getMessage());
			responseEnc.put("webSource", StringUtils.EMPTY);
			responseEnc.put("success", false);
			responseEnc.put("message", e.getMessage());
			return Optional.ofNullable(responseEnc);
		}
		if (responseRest == null)
		{
			responseEnc.put("webSource", StringUtils.EMPTY);
			responseEnc.put("success", false);
			responseEnc.put("message", "No response !");
			return Optional.ofNullable(responseEnc);
		}
		final String response = responseRest.getBody();
		LOG.info(String.format("The returned response  is : %s", response));
		responseEnc.put("webSource", response);
		responseEnc.put("success", true);
		return Optional.ofNullable(responseEnc);
	}

	private String getDeleteCustomerPaymentOptionsSource(final String customerid, final String customerCardId,
			final String wrokingKey, final String accessCode, final String version, final String command, final String responseType,
			final String requestType)
	{
		LOG.info("Building DeleteCustomerPaymentOptionsSource url ....");
		final StringBuilder ccaRequest = new StringBuilder();
		ccaRequest.append(customerCardId).append(PIPESIGN).append(customerid).append(PIPESIGN);


		final AesCryptUtil aesUtil = new AesCryptUtil(wrokingKey);

		final String encRequest = aesUtil.encrypt(ccaRequest.toString());
		final StringBuffer src = new StringBuffer(PRODUCTION_SRC_URL);
		src.append("?enc_request=").append(encRequest).append(AMPERSAND).append(ACCESS_CODE).append(EQUAL).append(accessCode)
				.append(AMPERSAND).append(COMMAND).append(EQUAL).append(command).append(AMPERSAND).append(REQUEST_TYPE).append(EQUAL)
				.append(requestType).append(AMPERSAND).append(RESPONSE_TYPE).append(EQUAL).append(responseType).append(AMPERSAND)
				.append("version").append(EQUAL).append(version);
		LOG.info("Building  DeleteCustomerPaymentOptionsSource url Finshed");
		return src.toString();
	}

	private String getCustomerPaymentOptionsSource(final String customerid, final String wrokingKey, final String accessCode,
			final String version, final String command, final String responseType, final String requestType)
	{
		LOG.info("Building CustomerPaymentOptionsSource url ....");
		final StringBuilder ccaRequest = new StringBuilder();
		ccaRequest.append(customerid).append(PIPESIGN);


		final AesCryptUtil aesUtil = new AesCryptUtil(wrokingKey);

		final String encRequest = aesUtil.encrypt(ccaRequest.toString());
		final StringBuffer src = new StringBuffer(PRODUCTION_SRC_URL);
		src.append("?enc_request=").append(encRequest).append(AMPERSAND).append(ACCESS_CODE).append(EQUAL).append(accessCode)
				.append(AMPERSAND).append(COMMAND).append(EQUAL).append(command).append(AMPERSAND).append(REQUEST_TYPE).append(EQUAL)
				.append(requestType).append(AMPERSAND).append(RESPONSE_TYPE).append(EQUAL).append(responseType).append(AMPERSAND)
				.append("version").append(EQUAL).append(version);
		LOG.info("Building  CustomerPaymentOptionsSource url Finshed");
		return src.toString();
	}

	private String getStatusSource(final String getReferenceNo, final String orderNo, final String wrokingKey,
			final String accessCode, final String version, final String command, final String responseType, final String requestType)
	{
		LOG.info("Building StatusSource url ....");
		final StringBuilder ccaRequest = new StringBuilder();
		ccaRequest.append(getReferenceNo).append(PIPESIGN).append(orderNo).append(PIPESIGN);


		final AesCryptUtil aesUtil = new AesCryptUtil(wrokingKey);

		final String encRequest = aesUtil.encrypt(ccaRequest.toString());
		final StringBuffer src = new StringBuffer(PRODUCTION_SRC_URL);
		src.append("?enc_request=").append(encRequest).append(AMPERSAND).append(ACCESS_CODE).append(EQUAL).append(accessCode)
				.append(AMPERSAND).append(COMMAND).append(EQUAL).append(command).append(AMPERSAND).append(REQUEST_TYPE).append(EQUAL)
				.append(requestType).append(AMPERSAND).append(RESPONSE_TYPE).append(EQUAL).append(responseType).append(AMPERSAND)
				.append("version").append(EQUAL).append(version);
		LOG.info("Building  StatusSource url Finshed");
		return src.toString();
	}

	@Override
	public Optional<Map<String, Object>> getOrderCancelData(final String workingKey, final String accessCode,
			final String referanceNo, final String amount) throws PaymentException
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(workingKey), WORKING_KEY_RESPONSE_CAN_NOT_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNotBlank(accessCode), ACCESS_CODE_RESPONSE_CAN_NOT_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNotBlank(referanceNo), REFERANCE_NO_RESPONSE_CAN_NOT_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNotBlank(amount), AMOUNT_RESPONSE_CAN_NOT_BE_NULL);

		final String command = "cancelOrder";
		final String statusSource = getCancelSource(workingKey, accessCode, referanceNo, command, STRING_TYPE, JSON_TYPE, amount,
				VERSION);
		LOG.info("getCancelSource url has been encrypted successfully");
		final ResponseEntity<?> httPOST = WebServiceApiUtil.httPOST(statusSource, null, null, String.class);
		String response = (String) httPOST.getBody();
		LOG.info("cancelOrder Data Response has been returned successfully");
		response = response.substring(response.lastIndexOf("=") + 1).trim();
		final AesCryptUtil aesUtil = new AesCryptUtil(workingKey);
		LOG.info("decrypting Order Status Data Response ");
		final String src = aesUtil.decrypt(response.trim());

		final ObjectMapper objectMapper = new ObjectMapper();
		Map<String, Object> readValue;
		try
		{
			readValue = objectMapper.readValue(src, Map.class);
			LOG.info("decrypting cancelOrder Data Response successfuly and returned as json ");
			return Optional.ofNullable(readValue);
		}
		catch (final JsonParseException e)
		{
			LOG.error(String.format("Error when parsing Response %s", e.getMessage()));
			throw new CCAvenueException(PaymentExceptionType.INVALID_RESPONSE_SYNTAX_EXPECTED_JSON.getMessage(),
					PaymentExceptionType.INVALID_RESPONSE_SYNTAX_EXPECTED_JSON);
		}
		catch (final JsonMappingException e)
		{
			LOG.error(String.format("Error when Mapping Response %s", e.getMessage()));
			throw new CCAvenueException(PaymentExceptionType.BAD_REQUEST.getMessage(), PaymentExceptionType.BAD_REQUEST);
		}
		catch (final IOException e)
		{
			LOG.error(String.format("Error!!  %s", e.getMessage()));
			throw new CCAvenueException(PaymentExceptionType.BAD_REQUEST.getMessage(), PaymentExceptionType.BAD_REQUEST);
		}

	}

	@Override
	public Optional<Map<String, Object>> getOrderRefundData(final String workingKey, final String accessCode,
			final String referanceNo, final String refundAmount, final String refundRefNo) throws PaymentException
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(workingKey), WORKING_KEY_RESPONSE_CAN_NOT_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNotBlank(accessCode), ACCESS_CODE_RESPONSE_CAN_NOT_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNotBlank(referanceNo), REFERANCE_NO_RESPONSE_CAN_NOT_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNotBlank(refundRefNo), REFUND_REF_NO_RESPONSE_CAN_NOT_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNotBlank(refundAmount), REFUND_AMOUNT_RESPONSE_CAN_NOT_BE_NULL);

		final String command = "refundOrder";
		final String statusSource = getRefundSource(workingKey, accessCode, referanceNo, command, STRING_TYPE, JSON_TYPE,
				refundAmount, refundRefNo, VERSION);
		LOG.info("RefundSource url has been encrypted successfully");
		final ResponseEntity<?> httPOST = WebServiceApiUtil.httPOST(statusSource, null, null, String.class);
		String response = (String) httPOST.getBody();
		LOG.info("refundOrder Data Response has been returned successfully");
		response = response.substring(response.lastIndexOf(EQUAL) + 1).trim();
		final AesCryptUtil aesUtil = new AesCryptUtil(workingKey);
		LOG.info("decrypting refundOrder Data Response ");
		final String src = aesUtil.decrypt(response.trim());
		final ObjectMapper objectMapper = new ObjectMapper();

		try
		{
			final Map<String, Object> readValue = objectMapper.readValue(src, Map.class);
			LOG.info("decrypting OrderRefund  Data Response successfuly and returned as json ");
			return Optional.ofNullable(readValue);
		}
		catch (final JsonParseException e)
		{
			LOG.error(String.format("Error when parsing Response %s", e.getMessage()));
			throw new CCAvenueException(PaymentExceptionType.INVALID_RESPONSE_SYNTAX_EXPECTED_JSON.getMessage(),
					PaymentExceptionType.INVALID_RESPONSE_SYNTAX_EXPECTED_JSON);
		}
		catch (final JsonMappingException e)
		{
			LOG.error(String.format("Error when Mapping Response %s", e.getMessage()));
			throw new CCAvenueException(PaymentExceptionType.BAD_REQUEST.getMessage(), PaymentExceptionType.BAD_REQUEST);
		}
		catch (final IOException e)
		{
			LOG.error(String.format("Error!!  %s", e.getMessage()));
			throw new CCAvenueException(PaymentExceptionType.BAD_REQUEST.getMessage(), PaymentExceptionType.BAD_REQUEST);
		}
	}

	@Override
	public Optional<Map<String, Object>> getOrderConfiemData(final String workingKey, final String accessCode,
			final String referanceNo, final String amount) throws PaymentException
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(workingKey), WORKING_KEY_RESPONSE_CAN_NOT_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNotBlank(accessCode), ACCESS_CODE_RESPONSE_CAN_NOT_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNotBlank(referanceNo), REFERANCE_NO_RESPONSE_CAN_NOT_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNotBlank(amount), AMOUNT_RESPONSE_CAN_NOT_BE_NULL);

		final String command = "confirmOrder";
		final String statusSource = getConfiemSource(workingKey, accessCode, referanceNo, command, STRING_TYPE, JSON_TYPE, amount,
				VERSION);
		LOG.info("confirmOrder url has been encrypted successfully");
		final ResponseEntity<?> httPOST = WebServiceApiUtil.httPOST(statusSource, null, null, String.class);
		String response = (String) httPOST.getBody();
		LOG.info("confirmOrder Data Response has been returned successfully");
		response = response.substring(response.lastIndexOf("=") + 1).trim();
		final AesCryptUtil aesUtil = new AesCryptUtil(workingKey);
		LOG.info("decrypting confirmOrder Data Response ");
		final String src = aesUtil.decrypt(response.trim());
		final ObjectMapper objectMapper = new ObjectMapper();

		try
		{
			final Map<String, Object> readValue = objectMapper.readValue(src, Map.class);
			LOG.info("decrypting confirmOrder  Data Response successfuly and returned as json ");
			return Optional.ofNullable(readValue);
		}
		catch (final JsonParseException e)
		{
			LOG.error(String.format("Error when parsing Response %s", e.getMessage()));
			throw new CCAvenueException(PaymentExceptionType.INVALID_RESPONSE_SYNTAX_EXPECTED_JSON.getMessage(),
					PaymentExceptionType.INVALID_RESPONSE_SYNTAX_EXPECTED_JSON);
		}
		catch (final JsonMappingException e)
		{
			LOG.error(String.format("Error when Mapping Response %s", e.getMessage()));
			throw new CCAvenueException(PaymentExceptionType.BAD_REQUEST.getMessage(), PaymentExceptionType.BAD_REQUEST);
		}
		catch (final IOException e)
		{
			LOG.error(String.format("Error!!  %s", e.getMessage()));
			throw new CCAvenueException(PaymentExceptionType.BAD_REQUEST.getMessage(), PaymentExceptionType.BAD_REQUEST);
		}
	}

	@Override
	public Optional<Map<String, Object>> getPaymentGeneratedInvoiceLink(final String workingKey, final String accessCode,
			final String customerName, final String customerEmail, final String customerMobileNumber, final String currency,
			final String amount) throws PaymentException
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(workingKey), WORKING_KEY_RESPONSE_CAN_NOT_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNotBlank(accessCode), ACCESS_CODE_RESPONSE_CAN_NOT_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNotBlank(customerName), "Customer name is empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(customerEmail), "Customer email is empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(customerMobileNumber), "Customer mobile number is empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(currency), "Currency is empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(amount), "Amount is empty");

		final String fullRequest = buildRequest(workingKey, accessCode, customerName, customerEmail, customerMobileNumber, currency,
				amount, "generateQuickInvoice");
		if (StringUtils.isBlank(fullRequest))
		{
			LOG.error("Mapped full request string is blank");
			throw new CCAvenueException("Full request is blank", PaymentExceptionType.INVALID_RESPONSE_SYNTAX_EXPECTED_JSON);
		}

		final ResponseEntity<?> httPOST = WebServiceApiUtil.httPOST(fullRequest, null, null, String.class);
		if (httPOST == null || httPOST.getBody() == null)
		{
			throw new CCAvenueException("EMPTY RESPONSE OR EMPTY RESPONSE BODY FOR CCAVENUE GET GENERATE PAAYMENTY LINK",
					PaymentExceptionType.BAD_REQUEST);
		}

		final Map<String, Object> responseEnc = new HashMap<>();
		final ObjectMapper objectMapper = new ObjectMapper();
		final String response = (String) httPOST.getBody();
		Arrays.stream(response.split("&")).map(e -> e.split("=")).forEach(ex -> responseEnc.put(ex[0].trim(), ex[1].trim()));
		final AesCryptUtil aesUtil = new AesCryptUtil(workingKey);
		final String src = aesUtil.decrypt(((String) responseEnc.get("enc_response")));
		LOG.info(String.format("The decrybted source is : %s", src));
		try
		{
			final Map<String, Object> readValue = objectMapper.readValue(src, Map.class);
			return Optional.of(readValue);
		}
		catch (final JsonProcessingException e)
		{
			LOG.error(e.getMessage());
			return Optional.empty();
		}
		catch (final IOException e)
		{
			LOG.error(e.getMessage());
			return Optional.empty();
		}
		catch (final Exception e)
		{
			LOG.error(e.getMessage());
			return Optional.empty();
		}

	}

	private String buildRequest(final String workingKey, final String accessCode, final String customerName,
			final String customerEmail, final String customerMobileNumber, final String currency, final String amount,
			final String command) throws CCAvenueException
	{
		final String requestString = buildInvoiceLinkToJSON(customerName, customerEmail, customerMobileNumber, currency, amount);
		if (StringUtils.isBlank(requestString))
		{
			LOG.error("Mapped request parameters string is blank");
			throw new CCAvenueException("request is blank", PaymentExceptionType.INVALID_RESPONSE_SYNTAX_EXPECTED_JSON);
		}
		final AesCryptUtil util = new AesCryptUtil(workingKey);
		final String request = util.encrypt(requestString);
		final StringBuilder src = new StringBuilder(PRODUCTION_SRC_URL);
		src.append("?enc_request=").append(request).append(AMPERSAND).append(ACCESS_CODE).append(EQUAL).append(accessCode)
				.append(AMPERSAND).append(COMMAND).append(EQUAL).append(command).append(AMPERSAND).append(REQUEST_TYPE).append(EQUAL)
				.append(JSON_TYPE).append(AMPERSAND).append(RESPONSE_TYPE).append(EQUAL).append(JSON_TYPE).append(AMPERSAND)
				.append(VERSION).append(EQUAL).append("1.1");

		return src.toString();
	}

	private String buildInvoiceLinkToJSON(final String customerName, final String customerEmail, final String customerMobileNumber,
			final String currency, final String amount)
	{
		final Map<String, Object> data = new HashMap<>();
		data.put("customer_name", customerName);
		data.put("bill_delivery_type", "NONE"); // notification message channel
		data.put("customer_mobile_no", customerMobileNumber);
		data.put("customer_email_id", customerEmail);
		data.put("customer_email_subject", "CCAvenue Payment Message"); // Email notification subject, does not matter since the bill_delivery_type is NONE
		data.put("currency", currency);
		data.put("valid_for", 2); // the generated link validity, 2 as specified by the Verloop example request
		data.put("valid_type", "days");
		data.put("amount", amount);

		final ObjectMapper objectMapper = new ObjectMapper();
		String writeValueAsString = StringUtils.EMPTY;
		try
		{
			writeValueAsString = objectMapper.writeValueAsString(data);
		}
		catch (final JsonProcessingException e)
		{
			LOG.error("Error during mapping CCAvenue request to string: " + e.getMessage());
		}
		return writeValueAsString;
	}

	private String getRefundSource(final String workingKey, final String accessCode, final String referanceNo,
			final String command, final String requestType, final String responseType, final String refundAmount,
			final String refundRefNo, final String version)
	{
		LOG.info("Building RefundSource Url .... ");
		final StringBuilder ccaRequest = new StringBuilder();
		ccaRequest.append(referanceNo).append(PIPESIGN).append(refundAmount).append(PIPESIGN).append(refundRefNo).append(PIPESIGN);

		final AesCryptUtil aesUtil = new AesCryptUtil(workingKey);

		final String encRequest = aesUtil.encrypt(ccaRequest.toString());
		LOG.info(String.format("decrypt Refund statusSource is : %s", aesUtil.decrypt(encRequest.toString().trim())));
		final StringBuffer src = new StringBuffer(PRODUCTION_SRC_URL);
		src.append("?enc_request=").append(encRequest).append(AMPERSAND).append(ACCESS_CODE).append(EQUAL).append(accessCode)
				.append(AMPERSAND).append(COMMAND).append(EQUAL).append(command).append(AMPERSAND).append(REQUEST_TYPE).append(EQUAL)
				.append(requestType).append(AMPERSAND).append(RESPONSE_TYPE).append(EQUAL).append(responseType).append(AMPERSAND)
				.append("version").append(EQUAL).append(version);
		LOG.info("Building RefundSource Url  Finshed ");
		return src.toString();

	}

	private String getConfiemSource(final String workingKey, final String accessCode, final String referanceNo,
			final String command, final String requestType, final String responseType, final String amount, final String version)
	{
		LOG.info("Building ConfiemSource Url .... ");
		final StringBuilder ccaRequest = new StringBuilder();
		ccaRequest.append(referanceNo).append(DOLLARSIGN).append(amount).append(PIPESIGN);

		final AesCryptUtil aesUtil = new AesCryptUtil(workingKey);

		final String encRequest = aesUtil.encrypt(ccaRequest.toString());
		LOG.info(String.format("decrypt Confiem statusSource is : %s", aesUtil.decrypt(encRequest.toString().trim())));
		final StringBuffer src = new StringBuffer(PRODUCTION_SRC_URL);
		src.append("?enc_request=").append(encRequest).append(AMPERSAND).append(ACCESS_CODE).append(EQUAL).append(accessCode)
				.append(AMPERSAND).append(COMMAND).append(EQUAL).append(command).append(AMPERSAND).append(REQUEST_TYPE).append(EQUAL)
				.append(requestType).append(AMPERSAND).append(RESPONSE_TYPE).append(EQUAL).append(responseType).append(AMPERSAND)
				.append("version").append(EQUAL).append(version);
		LOG.info("Building ConfiemSource Url  Finshed ");
		return src.toString();

	}

	private String getCancelSource(final String workingKey, final String accessCode, final String referanceNo,
			final String command, final String requestType, final String responseType, final String amount, final String version)
	{

		LOG.info("Building CancelSource Url .... ");
		final StringBuilder ccaRequest = new StringBuilder();
		ccaRequest.append(referanceNo).append(DOLLARSIGN).append(amount).append(PIPESIGN);
		final AesCryptUtil aesUtil = new AesCryptUtil(workingKey);

		final String encRequest = aesUtil.encrypt(ccaRequest.toString());
		LOG.info(String.format("decrypt Cancel statusSource is : %s", aesUtil.decrypt(encRequest.toString().trim())));
		final StringBuffer src = new StringBuffer(PRODUCTION_SRC_URL);
		src.append("?enc_request=").append(encRequest).append(AMPERSAND).append(ACCESS_CODE).append(EQUAL).append(accessCode)
				.append(AMPERSAND).append(COMMAND).append(EQUAL).append(command).append(AMPERSAND).append(REQUEST_TYPE).append(EQUAL)
				.append(requestType).append(AMPERSAND).append(RESPONSE_TYPE).append(EQUAL).append(responseType).append(AMPERSAND)
				.append("version").append(EQUAL).append(version);
		LOG.info("Building CancelSource Url  Finshed ");
		return src.toString();

	}

	@Override
	public Optional<Map<String, Object>> getApplePayPaymentWebView(final CCAvenueCredentials ccAvenueCredentials,
			final CCAvenueApplePayRequest ccAvenueApplePayRequest)
	{
		Preconditions.checkArgument(ccAvenueApplePayRequest != null, "CCAvenueApplePayRequest is null");
		Preconditions.checkArgument(ccAvenueCredentials != null, "CCAvenueCredentials is null");

		final String statusSource = getApplePaySource(ccAvenueCredentials, ccAvenueApplePayRequest);
		final String requestBody = getApplePayRequestBody(ccAvenueCredentials, ccAvenueApplePayRequest);
		final MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
		map.add("access_code", ccAvenueCredentials.getAccessCode());
		map.add("encRequest", requestBody);
		return validateProcessWithBody(ccAvenueCredentials.getWorkingKey(), statusSource, map,
				ccAvenueApplePayRequest.getUserAgentHeader(), ccAvenueApplePayRequest.isUseUserAgentHeader());
	}

	private String getApplePaySource(final CCAvenueCredentials ccAvenueCredentials,
			final CCAvenueApplePayRequest ccAvenueApplePayRequest)
	{
		final StringBuilder src = new StringBuilder(APPLE_PAY_URL);

		LOG.info(String.format("encrypt ApplePaySource  cridintials statusSource is : %s", src.toString()));

		return src.toString();
	}

	private String getApplePayRequestBody(final CCAvenueCredentials ccAvenueCredentials,
			final CCAvenueApplePayRequest ccAvenueApplePayRequest)
	{
		final StringBuilder ccaRequest = new StringBuilder();

	// @formatter:off
		ccaRequest.append("merchant_id=").append(ccAvenueCredentials.getMerchantId()).append(AMPERSAND)
         		.append("order_id").append(EQUAL).append(ccAvenueApplePayRequest.getOrderId()).append(AMPERSAND)
         		.append("currency").append(EQUAL).append(ccAvenueApplePayRequest.getCurrency()).append(AMPERSAND)
         		.append("amount").append(EQUAL).append(ccAvenueApplePayRequest.getAmount()).append(AMPERSAND)
         		.append("redirect_url").append(EQUAL).append(ccAvenueApplePayRequest.getRedirectUrl()).append(AMPERSAND)
         		.append("payment_option").append(EQUAL).append(ccAvenueApplePayRequest.getPaymentOption()).append(AMPERSAND)
         		.append("card_type").append(EQUAL).append(ccAvenueApplePayRequest.getCardType()).append(AMPERSAND)
         		.append("card_name").append(EQUAL).append(ccAvenueApplePayRequest.getCardName()).append(AMPERSAND)
         		.append("paymentData").append(EQUAL).append(ccAvenueApplePayRequest.getPaymentData()).append(AMPERSAND)
         		.append("appleCardType").append(EQUAL).append(ccAvenueApplePayRequest.getAppleCardType()).append(AMPERSAND)
         		.append("billing_name").append(EQUAL).append(ccAvenueApplePayRequest.getBillingName()).append(AMPERSAND)
         		.append("billing_address").append(EQUAL).append(ccAvenueApplePayRequest.getBillingAddress()).append(AMPERSAND)
         		.append("billing_city").append(EQUAL).append(ccAvenueApplePayRequest.getBillingCity()).append(AMPERSAND)
         		.append("billing_country").append(EQUAL).append(ccAvenueApplePayRequest.getBillingCountry()).append(AMPERSAND)
         		.append("billing_tel").append(EQUAL).append(ccAvenueApplePayRequest.getBillingPhoneNumber()).append(AMPERSAND)
         		.append("billing_email").append(EQUAL).append(ccAvenueApplePayRequest.getBillingEmail()).append(AMPERSAND)
         		.append("delivery_name").append(EQUAL).append(ccAvenueApplePayRequest.getDeliveryName()).append(AMPERSAND)
         		.append("delivery_address").append(EQUAL).append(ccAvenueApplePayRequest.getDeliveryAddress()).append(AMPERSAND)
         		.append("delivery_city").append(EQUAL).append(ccAvenueApplePayRequest.getDeliveryCity()).append(AMPERSAND)
         		.append("delivery_country").append(EQUAL).append(ccAvenueApplePayRequest.getDeliveryCountry()).append(AMPERSAND)
         		.append("delivery_tel").append(EQUAL).append(ccAvenueApplePayRequest.getDeliveryPhoneNumber()).append(AMPERSAND)
         		.append("delivery_email").append(EQUAL).append(ccAvenueApplePayRequest.getDeliveryEmail());
	// @formatter:on

		final AesCryptUtil aesUtil = new AesCryptUtil(ccAvenueCredentials.getWorkingKey());
		final String request = aesUtil.encrypt(ccaRequest.toString());

		LOG.info(String.format("encrypted ApplePaySource params are: %s", request));

		return request;
	}

	@Override
	public Optional<Map<String, Object>> getCustomerPaymentOptions(final String workingKey, final String accessCode,
			final String customerId) throws CCAvenueException
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(workingKey), WORKING_KEY_RESPONSE_CAN_NOT_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNotBlank(accessCode), ACCESS_CODE_RESPONSE_CAN_NOT_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNotBlank(customerId), CUSTOMER_ID_RESPONSE_CAN_NOT_BE_NULL);

		final String command = "getCustomerPaymentOptions";
		final Map<String, Object> responseEnc = new HashMap<>();

		try
		{
			final String customerPaymentOptionsSource = getCustomerPaymentOptionsSource(customerId, workingKey, accessCode, VERSION,
					command, JSON_TYPE, STRING_TYPE);
			LOG.info("getCustomerPaymentOptions Data url has been encrypted successfully");
			final ResponseEntity<?> httPOST = WebServiceApiUtil.httPOST(customerPaymentOptionsSource, null, null, String.class);
			final String response = (String) httPOST.getBody();
			LOG.info("DefaultCCAvenueService getCustomerPaymentOptions Response has been returned successfully");
			LOG.info(String.format("DefaultCCAvenueService The returned response  is : %s", response));
			Arrays.stream(response.split("&")).map(e -> e.split("=")).forEach(ex -> responseEnc.put(ex[0].trim(), ex[1].trim()));
			LOG.info(String.format("DefaultCCAvenueService The response after mapping it  is : %s", responseEnc));
			LOG.info(String.format("DefaultCCAvenueService the enc_response is : %s", responseEnc.get("enc_response")));
			final AesCryptUtil aesUtil = new AesCryptUtil(workingKey);
			final String src = aesUtil.decrypt(((String) responseEnc.get("enc_response")));

			if (StringUtils.isBlank(src))
			{
				LOG.error(String.format("DefaultCCAvenueService Error!!  %s", ((String) responseEnc.get("enc_response"))));
				LOG.error(String.format("DefaultCCAvenueService Error!!  %s", responseEnc.toString()));
				throw new CCAvenueException(PaymentExceptionType.BAD_REQUEST.getMessage(), PaymentExceptionType.BAD_REQUEST);
			}
			final ObjectMapper objectMapper = new ObjectMapper();
			LOG.info("DefaultCCAvenueService decrypting getCustomerPaymentOptions Data Response ");
			final Map<String, Object> readValue = objectMapper.readValue(src, Map.class);
			LOG.info("DefaultCCAvenueService decrypting getCustomerPaymentOptions Data Response successfuly and returned as json ");
			return Optional.ofNullable(readValue);
		}
		catch (final JsonParseException e)
		{
			LOG.error(String.format("DefaultCCAvenueService Error when parsing Response %s", e.getMessage()));
			throw new CCAvenueException(PaymentExceptionType.INVALID_RESPONSE_SYNTAX_EXPECTED_JSON.getMessage(),
					PaymentExceptionType.INVALID_RESPONSE_SYNTAX_EXPECTED_JSON);
		}
		catch (final JsonMappingException e)
		{
			LOG.error(String.format("DefaultCCAvenueService Error when Mapping Response %s", e.getMessage()));
			throw new CCAvenueException(PaymentExceptionType.BAD_REQUEST.getMessage(), PaymentExceptionType.BAD_REQUEST);
		}
		catch (final IOException e)
		{
			LOG.error(String.format("DefaultCCAvenueService Error!!  %s", e.getMessage()));
			throw new CCAvenueException(PaymentExceptionType.BAD_REQUEST.getMessage(), PaymentExceptionType.BAD_REQUEST);
		}
	}

	@Override
	public Optional<Map<String, Object>> deleteCustomerPaymentOption(final String workingKey, final String accessCode,
			final String customerId, final String customerCardId) throws CCAvenueException
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(workingKey), WORKING_KEY_RESPONSE_CAN_NOT_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNotBlank(accessCode), ACCESS_CODE_RESPONSE_CAN_NOT_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNotBlank(customerId), CUSTOMER_ID_RESPONSE_CAN_NOT_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNotBlank(customerCardId), CUSTOMER_CARD_ID_RESPONSE_CAN_NOT_BE_NULL);


		final String command = "deleteCustomerPaymentOption";
		final Map<String, Object> responseEnc = new HashMap<>();

		try
		{
			final String customerPaymentOptionsSource = getDeleteCustomerPaymentOptionsSource(customerId, customerCardId, workingKey,
					accessCode, VERSION, command, JSON_TYPE, STRING_TYPE);
			LOG.info("“deleteCustomerPaymentOptio Data url has been encrypted successfully");
			final ResponseEntity<?> httPOST = WebServiceApiUtil.httPOST(customerPaymentOptionsSource, null, null, String.class);
			final String response = (String) httPOST.getBody();
			LOG.info("DefaultCCAvenueService deleteCustomerPaymentOption Response has been returned successfully");
			LOG.info(String.format("DefaultCCAvenueService The returned response  is : %s", response));
			Arrays.stream(response.split("&")).map(e -> e.split("=")).forEach(ex -> responseEnc.put(ex[0].trim(), ex[1].trim()));
			LOG.info(String.format("DefaultCCAvenueService The response after mapping it  is : %s", responseEnc));
			LOG.info(String.format("DefaultCCAvenueService the enc_response is : %s", responseEnc.get("enc_response")));
			final AesCryptUtil aesUtil = new AesCryptUtil(workingKey);
			final String src = aesUtil.decrypt(((String) responseEnc.get("enc_response")));

			if (StringUtils.isBlank(src))
			{
				LOG.error(String.format("DefaultCCAvenueService Error!!  %s", ((String) responseEnc.get("enc_response"))));
				LOG.error(String.format("DefaultCCAvenueService Error!!  %s", responseEnc.toString()));
				throw new CCAvenueException(PaymentExceptionType.BAD_REQUEST.getMessage(), PaymentExceptionType.BAD_REQUEST);
			}
			final ObjectMapper objectMapper = new ObjectMapper();
			LOG.info("DefaultCCAvenueService decrypting deleteCustomerPaymentOption Data Response ");
			final Map<String, Object> readValue = objectMapper.readValue(src, Map.class);
			LOG.info(
					"DefaultCCAvenueService decrypting deleteCustomerPaymentOption Data Response successfuly and returned as json ");
			return Optional.ofNullable(readValue);
		}
		catch (final JsonParseException e)
		{
			LOG.error(String.format("DefaultCCAvenueService Error when parsing Response %s", e.getMessage()));
			throw new CCAvenueException(PaymentExceptionType.INVALID_RESPONSE_SYNTAX_EXPECTED_JSON.getMessage(),
					PaymentExceptionType.INVALID_RESPONSE_SYNTAX_EXPECTED_JSON);
		}
		catch (final JsonMappingException e)
		{
			LOG.error(String.format("DefaultCCAvenueService Error when Mapping Response %s", e.getMessage()));
			throw new CCAvenueException(PaymentExceptionType.BAD_REQUEST.getMessage(), PaymentExceptionType.BAD_REQUEST);
		}
		catch (final IOException e)
		{
			LOG.error(String.format("DefaultCCAvenueService Error!!  %s", e.getMessage()));
			throw new CCAvenueException(PaymentExceptionType.BAD_REQUEST.getMessage(), PaymentExceptionType.BAD_REQUEST);
		}
	}
}
