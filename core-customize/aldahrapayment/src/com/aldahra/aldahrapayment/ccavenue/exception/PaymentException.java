/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrapayment.ccavenue.exception;

import com.aldahra.aldahrapayment.ccavenue.enums.PaymentExceptionType;


/**
 *
 */
public class PaymentException extends Exception
{
	private final PaymentExceptionType type;
	private String[] errorParm;

	public PaymentExceptionType getType()
	{
		return type;
	}

	public PaymentException(final String message, final PaymentExceptionType type, final String[] parm)
	{
		super(message);
		this.type = type;
		this.errorParm = parm;
	}

	public PaymentException(final String message, final PaymentExceptionType type)
	{
		super(message);
		this.type = type;
	}

	/**
	 * @return the errorParm
	 */
	public String[] getErrorParm()
	{
		return errorParm;
	}


}
