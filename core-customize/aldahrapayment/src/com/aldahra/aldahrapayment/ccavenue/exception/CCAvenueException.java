/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrapayment.ccavenue.exception;


import com.aldahra.aldahrapayment.ccavenue.enums.PaymentExceptionType;


/**
 *
 */
public class CCAvenueException extends PaymentException
{

	public CCAvenueException(final String message, final PaymentExceptionType type)
	{
		super(message, type);
	}

}
