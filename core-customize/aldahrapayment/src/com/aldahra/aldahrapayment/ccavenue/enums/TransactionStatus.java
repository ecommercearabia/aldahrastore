/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrapayment.ccavenue.enums;

/**
 *
 */
public enum TransactionStatus
{


	UNSUCCESSFUL("unsuccessful"), SUCCESSFUL("successful"), SHIPPED("shipped"), CANCELED("canceled"), REFUNDED("refunded");

	private String status;

	private TransactionStatus(final String status)
	{
		this.status = status;
	}

	public String getStatus()
	{
		return status;
	}

}
