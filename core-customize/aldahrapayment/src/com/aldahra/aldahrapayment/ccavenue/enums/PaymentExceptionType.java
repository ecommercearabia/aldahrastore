/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrapayment.ccavenue.enums;

public enum PaymentExceptionType
{
	BAD_REQUEST("Bad Request"), INVALID_RESPONSE_SYNTAX_EXPECTED_JSON(
			"Invalid response syntax,bad json syntax"), THE_ORDER_IS_ALLREADY_CANCELED(
					"The order is allready canceled"), ORDER_IS_AUTHORIZED(
							"The order is allready authorized"), TRACKING_NUMBER_DOES_NOT_EXIST(
									"Tracking number does not exist"), THE_ORDER_IS_ALLREADY_CAPTUARED(
											"The order is allready captuared"), CAN_NOT_CANCEL_CAPTUARED_ORDER(
													"Can not cancel captuared order please refund first"), THE_ORDER_IS_ALLREADY_REFUNDED(
															"The order is allready refunded or refund limit exceeds"), THE_ORDER_IS_NOT_AUTHORIZED(
																	"The order is not authorized"), THE_ORDER_IS_NOT_CAPTUARED(
																			"The order is not captuared yet."), UNKNOWN_ORDER_STATUS(
																					"The order status  not unknown"), INVALID_ORDER_STATUS(
																							"Invalid order status "), THE_ORDER_CAN_NOT_BE_CANCELED(
																									"The order can not be canceled"), THE_ORDER_CAN_NOT_BE_CAPTUARED(
																											"the order can not be captuared"), PAYMENT_WIYH_STORE_CREDIT_CART_LIMIT(
																													"The payment not reach the min limit"),

	THE_ORDER_CAN_NOT_BE_REFUNDED("the order can not be captuared"), COULD_NOT_FETCH_WEB_SOURCE(
			"Could not fetch payment web source"),

	FAILURE_RESULT("FAILURE RESULT");

	String message;


	private PaymentExceptionType(final String message)
	{
		this.message = message;

	}


	public String getMessage()
	{
		return message;
	}


}
