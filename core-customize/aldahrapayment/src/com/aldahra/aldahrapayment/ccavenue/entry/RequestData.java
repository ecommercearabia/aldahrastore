package com.aldahra.aldahrapayment.ccavenue.entry;

public class RequestData
{
	private String accessCode;
	private String workingKey;
	private String merchantId;
	private String orderId;
	private String currency;
	private String redirectUrl;
	private String cancelUrl;
	private String language;
	private String billingName;
	private String billingAddress;
	private String billingCity;
	private String billingState;
	private String billingZip;
	private String billingCountry;
	private String billingTel;
	private String billingEmail;
	private String deliveryName;
	private String deliveryAddress;
	private String deliveryCity;
	private String deliveryState;
	private String deliveryZip;
	private String deliveryCountry;
	private String deliveryTel;
	private String merchantParam1;
	private String merchantParam2;
	private String merchantParam3;
	private String merchantParam4;
	private String promoCode;
	private String customerIdentifier;
	private String integrationType;
	private String saveCard;
	private String amount;


	public String getAmount()
	{
		return amount;
	}


	public void setAmount(final String amount)
	{
		this.amount = amount;
	}


	public RequestData()
	{

	}


	public String getAccessCode()
	{
		return accessCode;
	}


	public void setAccessCode(final String accessCode)
	{
		this.accessCode = accessCode;
	}


	public String getWorkingKey()
	{
		return workingKey;
	}


	public void setWorkingKey(final String workingKey)
	{
		this.workingKey = workingKey;
	}


	public String getMerchantId()
	{
		return merchantId;
	}


	public void setMerchantId(final String merchantId)
	{
		this.merchantId = merchantId;
	}


	public String getOrderId()
	{
		return orderId;
	}


	public void setOrderId(final String orderId)
	{
		this.orderId = orderId;
	}


	public String getCurrency()
	{
		return currency;
	}


	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}


	public String getRedirectUrl()
	{
		return redirectUrl;
	}


	public void setRedirectUrl(final String redirectUrl)
	{
		this.redirectUrl = redirectUrl;
	}


	public String getCancelUrl()
	{
		return cancelUrl;
	}


	public void setCancelUrl(final String cancelUrl)
	{
		this.cancelUrl = cancelUrl;
	}


	public String getLanguage()
	{
		return language;
	}


	public void setLanguage(final String language)
	{
		this.language = language;
	}




	public String getBillingName()
	{
		return billingName;
	}


	public void setBillingName(final String billingName)
	{
		this.billingName = billingName;
	}


	public String getBillingAddress()
	{
		return billingAddress;
	}


	public void setBillingAddress(final String billingAddress)
	{
		this.billingAddress = billingAddress;
	}


	public String getBillingCity()
	{
		return billingCity;
	}


	public void setBillingCity(final String billingCity)
	{
		this.billingCity = billingCity;
	}


	public String getBillingState()
	{
		return billingState;
	}


	public void setBillingState(final String billingState)
	{
		this.billingState = billingState;
	}


	public String getBillingZip()
	{
		return billingZip;
	}


	public void setBillingZip(final String billingZip)
	{
		this.billingZip = billingZip;
	}


	public String getBillingCountry()
	{
		return billingCountry;
	}


	public void setBillingCountry(final String billingCountry)
	{
		this.billingCountry = billingCountry;
	}


	public String getBillingTel()
	{
		return billingTel;
	}


	public void setBillingTel(final String billingTel)
	{
		this.billingTel = billingTel;
	}


	public String getBillingEmail()
	{
		return billingEmail;
	}


	public void setBillingEmail(final String billingEmail)
	{
		this.billingEmail = billingEmail;
	}


	public String getDeliveryName()
	{
		return deliveryName;
	}


	public void setDeliveryName(final String deliveryName)
	{
		this.deliveryName = deliveryName;
	}


	public String getDeliveryAddress()
	{
		return deliveryAddress;
	}


	public void setDeliveryAddress(final String deliveryAddress)
	{
		this.deliveryAddress = deliveryAddress;
	}


	public String getDeliveryCity()
	{
		return deliveryCity;
	}


	public void setDeliveryCity(final String deliveryCity)
	{
		this.deliveryCity = deliveryCity;
	}


	public String getDeliveryState()
	{
		return deliveryState;
	}


	public void setDeliveryState(final String deliveryState)
	{
		this.deliveryState = deliveryState;
	}


	public String getDeliveryZip()
	{
		return deliveryZip;
	}


	public void setDeliveryZip(final String deliveryZip)
	{
		this.deliveryZip = deliveryZip;
	}


	public String getDeliveryCountry()
	{
		return deliveryCountry;
	}


	public void setDeliveryCountry(final String deliveryCountry)
	{
		this.deliveryCountry = deliveryCountry;
	}


	public String getDeliveryTel()
	{
		return deliveryTel;
	}


	public void setDeliveryTel(final String deliveryTel)
	{
		this.deliveryTel = deliveryTel;
	}


	public String getMerchantParam1()
	{
		return merchantParam1;
	}


	public void setMerchantParam1(final String merchantParam1)
	{
		this.merchantParam1 = merchantParam1;
	}


	public String getMerchantParam2()
	{
		return merchantParam2;
	}


	public void setMerchantParam2(final String merchantParam2)
	{
		this.merchantParam2 = merchantParam2;
	}


	public String getMerchantParam3()
	{
		return merchantParam3;
	}


	public void setMerchantParam3(final String merchantParam3)
	{
		this.merchantParam3 = merchantParam3;
	}


	public String getMerchantParam4()
	{
		return merchantParam4;
	}


	public void setMerchantParam4(final String merchantParam4)
	{
		this.merchantParam4 = merchantParam4;
	}


	public String getPromoCode()
	{
		return promoCode;
	}


	public void setPromoCode(final String promoCode)
	{
		this.promoCode = promoCode;
	}


	public String getCustomerIdentifier()
	{
		return customerIdentifier;
	}


	public void setCustomerIdentifier(final String customerIdentifier)
	{
		this.customerIdentifier = customerIdentifier;
	}


	public String getIntegrationType()
	{
		return integrationType;
	}


	public void setIntegrationType(final String integrationType)
	{
		this.integrationType = integrationType;
	}


	public String getSaveCard()
	{
		return saveCard;
	}


	public void setSaveCard(final String saveCard)
	{
		this.saveCard = saveCard;
	}


}