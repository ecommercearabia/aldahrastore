package com.aldahra.aldahrapayment.ccavenue.entry;

public class ApplePayRequest
{
	private String orderId;
	private String cardName;
	private String paymentData;
	private String appleCardType;
	private String paymentOption;
	private String cardType;

	/**
	 *
	 */
	public ApplePayRequest()
	{
	}

	public String getOrderId()
	{
		return orderId;
	}

	public void setOrderId(final String orderId)
	{
		this.orderId = orderId;
	}

	public String getCardName()
	{
		return cardName;
	}

	public void setCardName(final String cardName)
	{
		this.cardName = cardName;
	}

	public String getPaymentData()
	{
		return paymentData;
	}

	public void setPaymentData(final String paymentData)
	{
		this.paymentData = paymentData;
	}

	public String getAppleCardType()
	{
		return appleCardType;
	}

	public void setAppleCardType(final String appleCardType)
	{
		this.appleCardType = appleCardType;
	}

	public String getPaymentOption()
	{
		return paymentOption;
	}

	public void setPaymentOption(final String paymentOption)
	{
		this.paymentOption = paymentOption;
	}

	/**
	 * @return the cardType
	 */
	public String getCardType()
	{
		return cardType;
	}

	/**
	 * @param cardType
	 *           the cardType to set
	 */
	public void setCardType(final String cardType)
	{
		this.cardType = cardType;
	}

	@Override
	public String toString()
	{
		return "ApplePayRequest [orderId=" + orderId + ", cardName=" + cardName + ", paymentData=" + paymentData
				+ ", appleCardType=" + appleCardType + ", paymentOption=" + paymentOption + "]";
	}

}
