/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrapayment.ccavenue.entry;

/**
 *
 */
public class CCAvenueCredentials
{
	private String workingKey;
	private String accessCode;
	private String merchantId;
	private String redirectUrl;

	public CCAvenueCredentials()
	{

	}


	public String getWorkingKey()
	{
		return workingKey;
	}


	public void setWorkingKey(final String workingKey)
	{
		this.workingKey = workingKey;
	}


	public String getAccessCode()
	{
		return accessCode;
	}


	public void setAccessCode(final String accessCode)
	{
		this.accessCode = accessCode;
	}


	public String getMerchantId()
	{
		return merchantId;
	}


	public void setMerchantId(final String merchantId)
	{
		this.merchantId = merchantId;
	}


	public String getRedirectUrl()
	{
		return redirectUrl;
	}


	public void setRedirectUrl(final String redirectUrl)
	{
		this.redirectUrl = redirectUrl;
	}


	@Override
	public String toString()
	{
		return "CCAvenueCredentials [workingKey=" + workingKey + ", accessCode=" + accessCode + ", merchantId=" + merchantId
				+ ", redirectUrl=" + redirectUrl + "]";
	}



}
