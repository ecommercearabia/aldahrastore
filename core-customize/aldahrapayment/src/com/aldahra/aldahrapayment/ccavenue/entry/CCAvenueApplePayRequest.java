/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrapayment.ccavenue.entry;

/**
 *
 */
public class CCAvenueApplePayRequest
{
	private String orderId;
	private String amount;
	private String currency;
	private String paymentOption;
	private String cardType;
	private String cardName;
	private String paymentData;
	private String appleCardType;
	// Payment Billing parameters
	private String billingName;
	private String billingAddress;
	private String billingCity;
	private String billingCountry;
	private String billingPhoneNumber;
	private String billingEmail;
	//Payment Delivery Parameters
	private String deliveryName;
	private String deliveryAddress;
	private String deliveryCity;
	private String deliveryCountry;
	private String deliveryPhoneNumber;
	private String deliveryEmail;

	private String redirectUrl;

	private String userAgentHeader;
	private boolean useUserAgentHeader;

	public CCAvenueApplePayRequest()
	{
	}

	public String getOrderId()
	{
		return orderId;
	}

	public void setOrderId(final String orderId)
	{
		this.orderId = orderId;
	}

	public String getAmount()
	{
		return amount;
	}

	public void setAmount(final String amount)
	{
		this.amount = amount;
	}

	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	public String getPaymentOption()
	{
		return paymentOption;
	}

	public void setPaymentOption(final String paymentOption)
	{
		this.paymentOption = paymentOption;
	}

	public String getCardType()
	{
		return cardType;
	}

	public void setCardType(final String cardType)
	{
		this.cardType = cardType;
	}

	public String getCardName()
	{
		return cardName;
	}

	public void setCardName(final String cardName)
	{
		this.cardName = cardName;
	}

	public String getPaymentData()
	{
		return paymentData;
	}

	public void setPaymentData(final String paymentData)
	{
		this.paymentData = paymentData;
	}

	public String getAppleCardType()
	{
		return appleCardType;
	}

	public void setAppleCardType(final String appleCardType)
	{
		this.appleCardType = appleCardType;
	}

	@Override
	public String toString()
	{
		return "CCAvenueApplePayRequest [orderId=" + orderId + ", amount=" + amount + ", currency=" + currency + ", paymentOption="
				+ paymentOption + ", cardType=" + cardType + ", cardName=" + cardName + ", paymentData=" + paymentData
				+ ", appleCardType=" + appleCardType + "]";
	}

	/**
	 * @return the billingName
	 */
	public String getBillingName()
	{
		return billingName;
	}

	/**
	 * @param billingName
	 *           the billingName to set
	 */
	public void setBillingName(final String billingName)
	{
		this.billingName = billingName;
	}

	/**
	 * @return the billingAddress
	 */
	public String getBillingAddress()
	{
		return billingAddress;
	}

	/**
	 * @param billingAddress
	 *           the billingAddress to set
	 */
	public void setBillingAddress(final String billingAddress)
	{
		this.billingAddress = billingAddress;
	}

	/**
	 * @return the billingCity
	 */
	public String getBillingCity()
	{
		return billingCity;
	}

	/**
	 * @param billingCity
	 *           the billingCity to set
	 */
	public void setBillingCity(final String billingCity)
	{
		this.billingCity = billingCity;
	}

	/**
	 * @return the billingCountry
	 */
	public String getBillingCountry()
	{
		return billingCountry;
	}

	/**
	 * @param billingCountry
	 *           the billingCountry to set
	 */
	public void setBillingCountry(final String billingCountry)
	{
		this.billingCountry = billingCountry;
	}

	/**
	 * @return the billingPhoneNumber
	 */
	public String getBillingPhoneNumber()
	{
		return billingPhoneNumber;
	}

	/**
	 * @param billingPhoneNumber
	 *           the billingPhoneNumber to set
	 */
	public void setBillingPhoneNumber(final String billingPhoneNumber)
	{
		this.billingPhoneNumber = billingPhoneNumber;
	}

	/**
	 * @return the billingEmail
	 */
	public String getBillingEmail()
	{
		return billingEmail;
	}

	/**
	 * @param billingEmail
	 *           the billingEmail to set
	 */
	public void setBillingEmail(final String billingEmail)
	{
		this.billingEmail = billingEmail;
	}

	/**
	 * @return the deliveryName
	 */
	public String getDeliveryName()
	{
		return deliveryName;
	}

	/**
	 * @param deliveryName
	 *           the deliveryName to set
	 */
	public void setDeliveryName(final String deliveryName)
	{
		this.deliveryName = deliveryName;
	}

	/**
	 * @return the deliveryAddress
	 */
	public String getDeliveryAddress()
	{
		return deliveryAddress;
	}

	/**
	 * @param deliveryAddress
	 *           the deliveryAddress to set
	 */
	public void setDeliveryAddress(final String deliveryAddress)
	{
		this.deliveryAddress = deliveryAddress;
	}

	/**
	 * @return the deliveryCity
	 */
	public String getDeliveryCity()
	{
		return deliveryCity;
	}

	/**
	 * @param deliveryCity
	 *           the deliveryCity to set
	 */
	public void setDeliveryCity(final String deliveryCity)
	{
		this.deliveryCity = deliveryCity;
	}

	/**
	 * @return the deliveryCountry
	 */
	public String getDeliveryCountry()
	{
		return deliveryCountry;
	}

	/**
	 * @param deliveryCountry
	 *           the deliveryCountry to set
	 */
	public void setDeliveryCountry(final String deliveryCountry)
	{
		this.deliveryCountry = deliveryCountry;
	}

	/**
	 * @return the deliveryPhoneNumber
	 */
	public String getDeliveryPhoneNumber()
	{
		return deliveryPhoneNumber;
	}

	/**
	 * @param deliveryPhoneNumber
	 *           the deliveryPhoneNumber to set
	 */
	public void setDeliveryPhoneNumber(final String deliveryPhoneNumber)
	{
		this.deliveryPhoneNumber = deliveryPhoneNumber;
	}

	/**
	 * @return the deliveryEmail
	 */
	public String getDeliveryEmail()
	{
		return deliveryEmail;
	}

	/**
	 * @param deliveryEmail
	 *           the deliveryEmail to set
	 */
	public void setDeliveryEmail(final String deliveryEmail)
	{
		this.deliveryEmail = deliveryEmail;
	}

	/**
	 * @return the redirectUrl
	 */
	public String getRedirectUrl()
	{
		return redirectUrl;
	}

	/**
	 * @param redirectUrl
	 *           the redirectUrl to set
	 */
	public void setRedirectUrl(final String redirectUrl)
	{
		this.redirectUrl = redirectUrl;
	}

	/**
	 * @return the userAgentHeader
	 */
	public String getUserAgentHeader()
	{
		return userAgentHeader;
	}

	/**
	 * @param userAgentHeader
	 *           the userAgentHeader to set
	 */
	public void setUserAgentHeader(final String userAgentHeader)
	{
		this.userAgentHeader = userAgentHeader;
	}

	/**
	 * @return the useUserAgentHeader
	 */
	public boolean isUseUserAgentHeader()
	{
		return useUserAgentHeader;
	}

	/**
	 * @param useUserAgentHeader
	 *           the useUserAgentHeader to set
	 */
	public void setUseUserAgentHeader(final boolean useUserAgentHeader)
	{
		this.useUserAgentHeader = useUserAgentHeader;
	}

}
