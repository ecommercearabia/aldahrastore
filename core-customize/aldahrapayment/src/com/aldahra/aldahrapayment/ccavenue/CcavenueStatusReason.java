/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrapayment.ccavenue;

/**
 *
 */
public enum CcavenueStatusReason
{
	REFERENCE_NO("reference_no"),
	ORDER_NO("order_no"),
	ORDER_CURRNCY("order_currncy"),
	ORDER_AMT("order_amt"),
	ORDER_DATE_TIME("order_date_time"),
	ORDER_BILL_NAME("order_bill_name"),
	ORDER_BILL_ADDRESS("order_bill_address"),
	ORDER_BILL_ZIP("order_bill_zip"),
	ORDER_BILL_TEL("order_bill_tel"),
	ORDER_BILL_EMAIL("order_bill_email"),
	ORDER_BILL_COUNTRY("order_bill_country"),
	ORDER_SHIP_NAME("order_ship_name"),
	ORDER_SHIP_ADDRESS("order_ship_address"),
	ORDER_SHIP_COUNTRY("order_ship_country"),
	ORDER_SHIP_TEL("order_ship_tel"),
	ORDER_BILL_CITY("order_bill_city"),
	ORDER_BILL_STATE("order_bill_state"),
	ORDER_SHIP_CITY("order_ship_city"),
	ORDER_SHIP_STATE("order_ship_state"),
	ORDER_SHIP_ZIP("order_ship_zip"),
	ORDER_SHIP_EMAIL("order_ship_email"),
	ORDER_NOTES("order_notes"),
	ORDER_IP("order_ip"),
	ORDER_STATUS("order_status"),
	ORDER_FRAUD_STATUS("order_fraud_status"),
	ORDER_STATUS_DATE_TIME("order_status_date_time"),
	ORDER_CAPT_AMT("order_capt_amt"),
	ORDER_CARD_NAME("order_card_name"),
	ORDER_DELIVERY_DETAILS("order_delivery_details"),
	ORDER_FEE_PERC("order_fee_perc"),
	ORDER_FEE_PERC_VALUE("order_fee_perc_value"),
	ORDER_FEE_FLAT("order_fee_flat"),
	ORDER_GROSS_AMT("order_gross_amt"),
	ORDER_DISCOUNT("order_discount"),
	ORDER_TAX("order_tax"),
	ORDER_BANK_REF_NO("order_bank_ref_no"),
	ORDER_GTW_ID("order_gtw_id"),
	ORDER_BANK_RESPONSE("order_bank_response"),
	ORDER_OPTION_TYPE("order_option_type"),
	ORDER_TDS("order_TDS"),
	ORDER_DEVICE_TYPE("order_device_type"),
	ORDER_RECEIPT_NO("order_receipt_no"),
	ORDER_BANK_MID("order_bank_mid"),
	ORDER_BANK_QSI_NO("order_bank_qsi_no"),
	ECI_VALUE("eci_value"),
	ORDER_CARD_TYPE("order_card_type"),
	CARD_NO("card_no"),
	ORDER_MULTIPLE_CAPTURE("order_multiple_capture"),
	ORDER_CONFIG_CAPT_VALUE("order_config_capt_value"),
	ERROR_DESC("error_desc"),
	STATUS("status"),
	ERROR_CODE("error_code");
	private String key;

	private CcavenueStatusReason(final String key)
	{
		this.key = key;
	}

	public String getKey()
	{
		return key;
	}

}
