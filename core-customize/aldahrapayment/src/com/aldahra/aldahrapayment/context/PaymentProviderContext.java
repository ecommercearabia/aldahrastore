package com.aldahra.aldahrapayment.context;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.aldahra.aldahrapayment.model.PaymentProviderModel;



/**
 * The Interface PaymentProviderContext.
 *
 * @author mnasro
 *
 *         The Interface PaymentProviderContext.
 */
public interface PaymentProviderContext
{

	/**
	 * Gets the provider.
	 *
	 * the provider class
	 *
	 * @return the provider
	 */
	public Optional<PaymentProviderModel> getProvider(Class<?> providerClass, BaseStoreModel baseStoreModel);


	/**
	 * Gets the provider.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @return the provider
	 */
	public Optional<PaymentProviderModel> getProvider(BaseStoreModel baseStoreModel);


	/**
	 * Gets the provider current store.
	 *
	 * @return the provider current store
	 */
	public Optional<PaymentProviderModel> getProviderByCurrentStore();

}
