/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrapayment.context;

import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.store.BaseStoreModel;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.aldahra.aldahraotp.exception.OTPException;
import com.aldahra.aldahrapayment.ccavenue.entry.ApplePayPaymentResponseData;
import com.aldahra.aldahrapayment.ccavenue.entry.ApplePayRequest;
import com.aldahra.aldahrapayment.ccavenue.entry.CustomerPaymentOptionData;
import com.aldahra.aldahrapayment.ccavenue.entry.PaymentLinkResponseData;
import com.aldahra.aldahrapayment.ccavenue.exception.PaymentException;
import com.aldahra.aldahrapayment.entry.PaymentRequestData;
import com.aldahra.aldahrapayment.entry.PaymentResponseData;
import com.aldahra.aldahrapayment.model.PaymentProviderModel;


/**
 * The Interface PaymentContext.
 *
 *
 * @author mnasro
 * @author abu-muhasien
 *
 */
public interface PaymentContext
{

	/**
	 * Gets the payment data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order
	 * @return the payment data
	 */
	public Optional<PaymentRequestData> getPaymentData(PaymentProviderModel paymentProviderModel,
			AbstractOrderModel abstractOrderModel);

	/**
	 * Gets the payment data.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @param abstractOrderModel
	 *           the abstract order
	 * @return the payment data
	 */
	public Optional<PaymentRequestData> getPaymentData(BaseStoreModel baseStoreModel, AbstractOrderModel abstractOrderModel);

	/**
	 * Gets the payment data by current store.
	 *
	 * @param abstractOrderModel
	 *           the abstract order
	 * @return the payment data by current store
	 */
	public Optional<PaymentRequestData> getPaymentDataByCurrentStore(AbstractOrderModel abstractOrderModel);

	/**
	 * Gets the response data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @param data
	 *           the data
	 * @return the response data
	 */
	public Optional<PaymentResponseData> getResponseData(PaymentProviderModel paymentProviderModel,
			AbstractOrderModel abstractOrderModel, Object data);

	/**
	 * Gets the response data.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @param data
	 *           the data
	 * @return the response data
	 */
	public Optional<PaymentResponseData> getResponseData(BaseStoreModel baseStoreModel, AbstractOrderModel abstractOrderModel,
			Object data);

	/**
	 * Gets the response data by current store.
	 *
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @param data
	 *           the data
	 * @return the response data by current store
	 */
	public Optional<PaymentResponseData> getResponseDataByCurrentStore(AbstractOrderModel abstractOrderModel, Object data);

	/**
	 * Interpret response.
	 *
	 * @param responseParams
	 *           the response params
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @return the optional
	 */
	public Optional<CreateSubscriptionResult> interpretResponse(final Map<String, Object> responseParams,
			PaymentProviderModel paymentProviderModel);

	/**
	 * Interpret response.
	 *
	 * @param responseParams
	 *           the response params
	 * @param baseStoreModel
	 *           the base store model
	 * @return the optional
	 */
	public Optional<CreateSubscriptionResult> interpretResponse(final Map<String, Object> responseParams,
			BaseStoreModel baseStoreModel);

	/**
	 * Interpret response by current store.
	 *
	 * @param responseParams
	 *           the response params
	 * @return the optional
	 */
	public Optional<CreateSubscriptionResult> interpretResponseByCurrentStore(final Map<String, Object> responseParams);


	/**
	 * Checks if is successful paid order.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @param data
	 *           the data
	 * @return true, if is successful paid order
	 */
	public boolean isSuccessfulPaidOrder(PaymentProviderModel paymentProviderModel, AbstractOrderModel abstractOrderModel,
			Object data);

	/**
	 * Checks if is successful paid order.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @param data
	 *           the data
	 * @return true, if is successful paid order
	 */
	public boolean isSuccessfulPaidOrder(BaseStoreModel paymentProviderModel, AbstractOrderModel abstractOrderModel, Object data);

	/**
	 * Checks if is successful paid order by current store.
	 *
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @param data
	 *           the data
	 * @return true, if is successful paid order by current store
	 */
	public boolean isSuccessfulPaidOrderByCurrentStore(AbstractOrderModel abstractOrderModel, Object data);

	/**
	 * Gets the payment order status response data by current store.
	 *
	 * @param data
	 *           the data
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the payment order status response data by current store
	 * @throws PaymentException
	 *            the payment exception
	 */
	public Optional<PaymentResponseData> getPaymentOrderStatusResponseDataByCurrentStore(Object data,
			final AbstractOrderModel abstractOrderModel) throws PaymentException;

	/**
	 * Gets the payment order status response data.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @param data
	 *           the data
	 * @return the payment order status response data
	 * @throws PaymentException
	 *            the payment exception
	 */
	public Optional<PaymentResponseData> getPaymentOrderStatusResponseData(BaseStoreModel baseStoreModel,
			AbstractOrderModel abstractOrderModel, Object data) throws PaymentException;

	/**
	 * Gets the payment order status response data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @param data
	 *           the data
	 * @return the payment order status response data
	 * @throws PaymentException
	 *            the payment exception
	 */
	public Optional<PaymentResponseData> getPaymentOrderStatusResponseData(PaymentProviderModel paymentProviderModel,
			AbstractOrderModel abstractOrderModel, Object data) throws PaymentException;



	/**
	 * Gets the payment order confirmed response data.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the payment order confirmed response data
	 * @throws PaymentException
	 *            the payment exception
	 */
	public Optional<PaymentResponseData> getPaymentOrderConfirmedResponseData(BaseStoreModel baseStoreModel,
			AbstractOrderModel abstractOrderModel) throws PaymentException;

	/**
	 * Gets the payment order confirmed response data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the payment order confirmed response data
	 * @throws PaymentException
	 *            the payment exception
	 */
	public Optional<PaymentResponseData> getPaymentOrderConfirmedResponseData(PaymentProviderModel paymentProviderModel,
			AbstractOrderModel abstractOrderModel) throws PaymentException;




	/**
	 * Gets the payment order cancel response data.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the payment order cancel response data
	 * @throws PaymentException
	 *            the payment exception
	 */
	public Optional<PaymentResponseData> getPaymentOrderCancelResponseData(BaseStoreModel baseStoreModel,
			AbstractOrderModel abstractOrderModel) throws PaymentException;

	/**
	 * Gets the payment order cancel response data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the payment order cancel response data
	 * @throws PaymentException
	 *            the payment exception
	 */
	public Optional<PaymentResponseData> getPaymentOrderCancelResponseData(PaymentProviderModel paymentProviderModel,
			AbstractOrderModel abstractOrderModel) throws PaymentException;



	/**
	 * Gets the payment order refund response data.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the payment order refund response data
	 * @throws PaymentException
	 *            the payment exception
	 */
	public Optional<PaymentResponseData> getPaymentOrderRefundResponseData(BaseStoreModel baseStoreModel,
			AbstractOrderModel abstractOrderModel) throws PaymentException;

	/**
	 * Gets the payment order refund response data.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the payment order refund response data
	 * @throws PaymentException
	 *            the payment exception
	 */
	public Optional<PaymentResponseData> getPaymentOrderRefundResponseData(final BaseStoreModel baseStoreModel,
			final AbstractOrderModel abstractOrderModel, final BigDecimal amount) throws PaymentException;



	/**
	 * Gets the payment order refund response data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the payment order refund response data
	 * @throws PaymentException
	 *            the payment exception
	 */
	public Optional<PaymentResponseData> getPaymentOrderRefundResponseData(PaymentProviderModel paymentProviderModel,
			AbstractOrderModel abstractOrderModel) throws PaymentException;

	/**
	 * Gets the payment order refund response data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the payment order refund response data
	 * @throws PaymentException
	 *            the payment exception
	 */
	public Optional<PaymentResponseData> getPaymentOrderRefundResponseData(PaymentProviderModel paymentProviderModel,
			AbstractOrderModel abstractOrderModel, final BigDecimal amount) throws PaymentException;

	public Optional<PaymentLinkResponseData> getGeneratedPaymentLinkByCurrentStore(AbstractOrderModel abstractOrderModel)
			throws PaymentException, OTPException;;

	public Optional<PaymentLinkResponseData> getGeneratedPaymentLink(AbstractOrderModel abstractOrderModel,
			PaymentProviderModel paymentProviderModel) throws PaymentException, OTPException;

	public Optional<ApplePayPaymentResponseData> getApplePayResponseData(PaymentProviderModel paymentProviderModel,
			AbstractOrderModel abstractOrderModel, ApplePayRequest request) throws PaymentException;

	public Optional<ApplePayPaymentResponseData> getApplePayResponseDataByCurrentBaseStore(AbstractOrderModel abstractOrderModel,
			ApplePayRequest request) throws PaymentException;

	public List<CustomerPaymentOptionData> getCustomerPaymentOptions(final CustomerModel customerModel,
			final PaymentProviderModel paymentProviderModel) throws PaymentException;

	public List<CustomerPaymentOptionData> getCustomerPaymentOptionsByCurrentStore(final CustomerModel customerModel)
			throws PaymentException;

	public List<CustomerPaymentOptionData> getCustomerPaymentOptionsByCurrentStoreAndCustomer() throws PaymentException;

	public void deleteCustomerPaymentOption(final CustomerModel customerModel, final String customerCardId,
			final PaymentProviderModel paymentProviderModel) throws PaymentException;

	public void deleteCustomerPaymentOptionByCurrentStore(final CustomerModel customerModel, final String customerCardId)
			throws PaymentException;

	public void deleteCustomerPaymentOptionByCurrentStoreAndCustomer(final String customerCardId) throws PaymentException;

}


