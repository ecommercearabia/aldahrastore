/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrapayment.populator;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.cybersource.enums.CardTypeEnum;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.PaymentInfoData;

import java.util.Map;

import com.aldahra.aldahrapayment.ccavenue.CcavenueStatusReason;


/**
 * @author amjad.shati@erabia.com
 */
public class PaymentInfoResultPopulator extends AbstractResultPopulator<Map<String, Object>, CreateSubscriptionResult>
{
	@Override
	public void populate(final Map<String, Object> source, final CreateSubscriptionResult target)
	{
		validateParameterNotNull(source, "Parameter [Map<String, String>] source cannot be null");
		validateParameterNotNull(target, "Parameter [CreateSubscriptionResult] target cannot be null");
		final String cardAccountNumber = String.valueOf(source.get(CcavenueStatusReason.CARD_NO.getKey()));
		final String cardCardType = getCardType(String.valueOf(source.get(CcavenueStatusReason.ORDER_CARD_NAME.getKey())));
		final String cardCvNumber = null;
		final Integer cardExpirationMonth = 1;
		final Integer cardExpirationYear = 30;
		final String cardIssueNumber = "0000";
		final String cardStartMonth = "01";
		final String cardStartYear = "30";
		final String paymentOption = "CARD";
		final String cardAccountHolderName = String.valueOf(source.get(CcavenueStatusReason.ORDER_SHIP_NAME.getKey()));

		final PaymentInfoData data = new PaymentInfoData();
		data.setCardAccountNumber(cardAccountNumber);
		data.setCardCardType(cardCardType);
		data.setCardCvNumber(cardCvNumber);
		data.setCardExpirationMonth(cardExpirationMonth);
		data.setCardExpirationYear(cardExpirationYear);
		data.setCardIssueNumber(cardIssueNumber);
		data.setCardStartMonth(cardStartMonth);
		data.setCardStartYear(cardStartYear);
		data.setPaymentOption(paymentOption);
		data.setCardAccountHolderName(cardAccountHolderName);

		target.setPaymentInfoData(data);
	}

	private String getCardType(String brandType)
	{
		if (brandType.toLowerCase().contains("mastercard"))
		{
			brandType = String.valueOf(CardTypeEnum.master.getStringValue());
		}
		else if (brandType.toLowerCase().contains("visacard"))
		{
			brandType = String.valueOf(CardTypeEnum.visa.getStringValue());
		}
		else
		{
			brandType = String.valueOf(CardTypeEnum.master.getStringValue());
		}
		return brandType;
	}

}
