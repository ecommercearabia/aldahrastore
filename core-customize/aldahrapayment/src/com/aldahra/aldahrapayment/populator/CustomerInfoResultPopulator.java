/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrapayment.populator;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.CustomerInfoData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Map;

import javax.annotation.Resource;


/**
 * @author amjad.shati@erabia.com
 */
public class CustomerInfoResultPopulator extends AbstractResultPopulator<Map<String, Object>, CreateSubscriptionResult>
{

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "userService")
	private UserService userService;

	@Override
	public void populate(final Map<String, Object> source, final CreateSubscriptionResult target)
	{
		validateParameterNotNull(target, "Parameter [CreateSubscriptionResult] target cannot be null");

		AddressModel billingAddress = cartService.getSessionCart().getPaymentAddress();
		if (billingAddress == null)
		{
			billingAddress = cartService.getSessionCart().getDeliveryAddress();
		}
		final String contactEmail = ((de.hybris.platform.core.model.user.CustomerModel) cartService.getSessionCart().getUser())
				.getContactEmail();

		final CustomerInfoData data = new CustomerInfoData();
		if (billingAddress != null)
		{
			data.setBillToCity(billingAddress.getCity() == null ? billingAddress.getTown() : billingAddress.getCity().getName());
			data.setBillToCompany(billingAddress.getCompany());
			data.setBillToCountry(billingAddress.getCountry() == null ? "" : billingAddress.getCountry().getIsocode());
			data.setBillToCustomerIdRef(cartService.getSessionCart().getUser().getUid());
			data.setBillToEmail(contactEmail);
			data.setBillToTitleCode(billingAddress.getTitle() == null ? "" : billingAddress.getTitle().getCode());
			data.setBillToFirstName(billingAddress.getFirstname());
			data.setBillToLastName(billingAddress.getLastname());
			data.setBillToPhoneNumber(billingAddress.getMobile());
			data.setBillToPostalCode(billingAddress.getPostalcode());
			data.setBillToState(billingAddress.getRegion() == null ? "" : billingAddress.getRegion().getIsocode());
			data.setBillToStreet1(billingAddress.getLine1());
			data.setBillToStreet2(billingAddress.getLine2());
		}
		final AddressModel deliveryAddress = cartService.getSessionCart().getDeliveryAddress();
		if (deliveryAddress != null)
		{
			data.setShipToCity(deliveryAddress.getCity() == null ? deliveryAddress.getTown() : deliveryAddress.getCity().getName());
			data.setShipToCompany(deliveryAddress.getCompany());
			data.setShipToCountry(deliveryAddress.getCountry() == null ? "" : deliveryAddress.getCountry().getIsocode());
			data.setShipToFirstName(deliveryAddress.getFirstname());
			data.setShipToLastName(deliveryAddress.getLastname());
			data.setShipToPhoneNumber(deliveryAddress.getMobile());
			data.setShipToPostalCode(deliveryAddress.getPostalcode());
			data.setShipToShippingMethod(cartService.getSessionCart().getDeliveryMode() == null ? ""
					: cartService.getSessionCart().getDeliveryMode().getName());
			data.setShipToState(deliveryAddress.getRegion() == null ? "" : deliveryAddress.getRegion().getIsocode());
			data.setShipToStreet1(deliveryAddress.getLine1());
			data.setShipToStreet2(deliveryAddress.getLine2());
		}

		target.setCustomerInfoData(data);
	}
}
