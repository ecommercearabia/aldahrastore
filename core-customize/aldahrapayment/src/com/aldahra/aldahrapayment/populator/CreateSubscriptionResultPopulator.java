/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrapayment.populator;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.enums.DecisionsEnum;

import java.util.Map;

import com.aldahra.aldahrapayment.ccavenue.CcavenueStatusReason;


/**
 * @author amjad.shati@erabia.com
 */
public class CreateSubscriptionResultPopulator extends AbstractResultPopulator<Map<String, Object>, CreateSubscriptionResult>
{
	@Override
	public void populate(final Map<String, Object> source, final CreateSubscriptionResult target)
	{
		validateParameterNotNull(source, "Parameter source (Map<String, String>) cannot be null");
		validateParameterNotNull(target, "Parameter target (CreateSubscriptionResult) cannot be null");

		final String orderStatus = String.valueOf(source.get(CcavenueStatusReason.ORDER_STATUS.getKey()));

		switch (orderStatus.toUpperCase())
		{
			case "SHIPPED":
			case "SUCCESS":
			case "SUCCESSFUL":
				target.setDecision(DecisionsEnum.ACCEPT.toString());
				break;
			default:
				target.setDecision(DecisionsEnum.ERROR.toString());
				break;
		}
		target.setDecisionPublicSignature(orderStatus);
		target.setTransactionSignatureVerified(null);
		target.setReasonCode(getIntegerForString(String.valueOf(source.get(CcavenueStatusReason.STATUS.getKey()))));
		target.setRequestId(String.valueOf(source.get(CcavenueStatusReason.REFERENCE_NO.getKey())));

	}
}

