/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrapayment.populator;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.AuthReplyData;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;

import java.math.BigDecimal;
import java.util.Map;

import com.aldahra.aldahrapayment.ccavenue.CcavenueStatusReason;


/**
 * @author amjad.shati@erabia.com
 */
public class AuthReplySubscriptionResultPopulator extends AbstractResultPopulator<Map<String, Object>, CreateSubscriptionResult>
{
	@Override
	public void populate(final Map<String, Object> source, final CreateSubscriptionResult target)
	{

		validateParameterNotNull(target, "Parameter [CreateSubscriptionResult] target cannot be null");
		validateParameterNotNull(source, "source [Map<String, Object>] source cannot be null");

		final AuthReplyData data = new AuthReplyData();

		final Integer ccAuthReplyReasonCode = getIntegerForString(
				String.valueOf(source.get(CcavenueStatusReason.REFERENCE_NO.getKey())));
		final String ccAuthReplyAuthorizationCode = String.valueOf(source.get(CcavenueStatusReason.REFERENCE_NO.getKey()));
		final String ccAuthReplyCvCode=null;
		final Boolean cvnDecision=Boolean.TRUE;
		final String ccAuthReplyAvsCodeRaw="YES";
		final String ccAuthReplyAvsCode="YES";
		final BigDecimal ccAuthReplyAmount = new BigDecimal((Double) source.get(CcavenueStatusReason.ORDER_AMT.getKey()));
		final String ccAuthReplyProcessorResponse=null;
		final String ccAuthReplyAuthorizedDateTime = String.valueOf(source.get(CcavenueStatusReason.ORDER_DATE_TIME.getKey()));


		data.setCcAuthReplyReasonCode(ccAuthReplyReasonCode);
		data.setCcAuthReplyAuthorizationCode(ccAuthReplyAuthorizationCode);
		data.setCcAuthReplyCvCode(ccAuthReplyCvCode);
		data.setCvnDecision(cvnDecision);
		data.setCcAuthReplyAvsCodeRaw(ccAuthReplyAvsCodeRaw);
		data.setCcAuthReplyAvsCode(ccAuthReplyAvsCode);
		data.setCcAuthReplyAmount(ccAuthReplyAmount);
		data.setCcAuthReplyProcessorResponse(ccAuthReplyProcessorResponse);
		data.setCcAuthReplyAuthorizedDateTime(ccAuthReplyAuthorizedDateTime);

		target.setAuthReplyData(data);
	}
}
