package com.aldahra.aldahrapayment.customer.service.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commerceservices.customer.impl.DefaultCustomerAccountService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.NoCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.dto.BillingInfo;
import de.hybris.platform.payment.dto.CardInfo;
import de.hybris.platform.payment.dto.NewSubscription;

import java.util.Currency;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.aldahra.aldahrapayment.customer.service.CustomCustomerAccountService;
import com.aldahra.aldahrapayment.customer.service.dao.CustomCustomerAccountDao;
import com.aldahra.aldahrapayment.enums.NoCardType;


/**
 * @author mnasro
 *
 *         The Class DefaultCustomCustomerAccountService.
 */
public class DefaultCustomCustomerAccountService extends DefaultCustomerAccountService implements CustomCustomerAccountService
{

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(DefaultCustomCustomerAccountService.class);

	private static final String CUSTOMER_VALIDATION_NONULL = "Customer model cannot be null";

	/** The sefam customer account dao. */
	@Resource(name = "customerAccountDao")
	private CustomCustomerAccountDao customerAccountDao;

	@Override
	public CreditCardPaymentInfoModel createPaymentSubscription(final CustomerModel customerModel, final CardInfo cardInfo,
			final BillingInfo billingInfo, final String titleCode, final String paymentProvider, final boolean saveInAccount)
	{
		validateParameterNotNull(customerModel, "Customer cannot be null");
		validateParameterNotNull(cardInfo, "CardInfo cannot be null");
		validateParameterNotNull(billingInfo, "billingInfo cannot be null");
		validateParameterNotNull(paymentProvider, "PaymentProvider cannot be null");
		final CurrencyModel currencyModel = getCurrency(customerModel);
		validateParameterNotNull(currencyModel, "Customer session currency cannot be null");

		final Currency currency = getI18nService().getBestMatchingJavaCurrency(currencyModel.getIsocode());

		final AddressModel billingAddress = getModelService().create(AddressModel.class);
		if (StringUtils.isNotBlank(titleCode))
		{
			final TitleModel title = new TitleModel();
			title.setCode(titleCode);
			billingAddress.setTitle(getFlexibleSearchService().getModelByExample(title));
		}
		billingAddress.setFirstname(billingInfo.getFirstName());
		billingAddress.setLastname(billingInfo.getLastName());
		billingAddress.setLine1(billingInfo.getStreet1());
		billingAddress.setLine2(billingInfo.getStreet2());
		billingAddress.setTown(billingInfo.getCity());
		billingAddress.setPostalcode(billingInfo.getPostalCode());
		billingAddress.setCountry(getCommonI18NService().getCountry(billingInfo.getCountry()));
		if (billingInfo.getRegion() != null)
		{
			billingAddress.setRegion(getCommonI18NService().getRegion(billingAddress.getCountry(), billingInfo.getRegion()));
		}
		billingAddress.setPhone1(billingInfo.getPhoneNumber());
		final String email = getCustomerEmailResolutionService().getEmailForCustomer(customerModel);
		billingAddress.setEmail(email);

		final String merchantTransactionCode = customerModel.getUid() + "-" + UUID.randomUUID();
		//		try
		//		{
		//			final NewSubscription subscription = getPaymentService().createSubscription(merchantTransactionCode, paymentProvider,
		//					currency, billingAddress, cardInfo);
		final NewSubscription subscription = new NewSubscription();

		subscription.setSubscriptionID(UUID.randomUUID().toString());
		if (StringUtils.isNotBlank(subscription.getSubscriptionID()))
		{
			final CreditCardPaymentInfoModel cardPaymentInfoModel = getModelService().create(CreditCardPaymentInfoModel.class);
			cardPaymentInfoModel.setCode(customerModel.getUid() + "_" + UUID.randomUUID());
			cardPaymentInfoModel.setUser(customerModel);
			cardPaymentInfoModel.setSubscriptionId(subscription.getSubscriptionID());
			cardPaymentInfoModel.setNumber(getMaskedCardNumber(cardInfo.getCardNumber()));
			cardPaymentInfoModel.setType(cardInfo.getCardType());
			cardPaymentInfoModel.setCcOwner(cardInfo.getCardHolderFullName());
			cardPaymentInfoModel.setValidToMonth(String.format("%02d", cardInfo.getExpirationMonth()));
			cardPaymentInfoModel.setValidToYear(String.valueOf(cardInfo.getExpirationYear()));
			if (cardInfo.getIssueMonth() != null)
			{
				cardPaymentInfoModel.setValidFromMonth(String.valueOf(cardInfo.getIssueMonth()));
			}
			if (cardInfo.getIssueYear() != null)
			{
				cardPaymentInfoModel.setValidFromYear(String.valueOf(cardInfo.getIssueYear()));
			}

			cardPaymentInfoModel.setSubscriptionId(subscription.getSubscriptionID());
			cardPaymentInfoModel.setSaved(saveInAccount);
			if (!StringUtils.isEmpty(cardInfo.getIssueNumber()))
			{
				cardPaymentInfoModel.setIssueNumber(Integer.valueOf(cardInfo.getIssueNumber()));
			}

			billingAddress.setOwner(cardPaymentInfoModel);
			cardPaymentInfoModel.setBillingAddress(billingAddress);

			getModelService().saveAll(billingAddress, cardPaymentInfoModel);
			getModelService().refresh(customerModel);

			addPaymentInfo(customerModel, cardPaymentInfoModel);

			return cardPaymentInfoModel;
		}
		//		}
		//		catch (final de.hybris.platform.commerceservices.customer.impl.AdapterException ae) //NOSONAR
		//		{
		//			LOG.error("Failed to create subscription for customer. Customer PK: " + String.valueOf(customerModel.getPk())
		//					+ " Exception: " + ae.getClass().getName());
		//
		//			return null;
		//		}

		return null;
	}


	/**
	 * Creates the payment subscription.
	 *
	 * @param customerModel
	 *           the customer model
	 * @param billingInfo
	 *           the billing info
	 * @param titleCode
	 *           the title code
	 * @param paymentProvider
	 *           the payment provider
	 * @param saveInAccount
	 *           the save in account
	 * @param noCardTypeCode
	 *           the no card type code
	 * @return the no card payment info model
	 */
	@Override
	public Optional<NoCardPaymentInfoModel> createPaymentSubscription(final CustomerModel customerModel,
			final BillingInfo billingInfo, final String titleCode, final String paymentProvider, final boolean saveInAccount,
			final String noCardTypeCode)
	{
		validateParameterNotNull(customerModel, "Customer cannot be null");
		validateParameterNotNull(titleCode, "titleCode cannot be null");
		validateParameterNotNull(billingInfo, "billingInfo cannot be null");
		validateParameterNotNull(paymentProvider, "PaymentProvider cannot be null");
		validateParameterNotNull(noCardTypeCode, "noCardTypeCode cannot be null");
		final CurrencyModel currencyModel = getCurrency(customerModel);
		validateParameterNotNull(currencyModel, "Customer session currency cannot be null");

		final AddressModel billingAddress = getModelService().create(AddressModel.class);
		if (StringUtils.isNotBlank(titleCode))
		{
			final TitleModel title = new TitleModel();
			title.setCode(titleCode);
			billingAddress.setTitle(getFlexibleSearchService().getModelByExample(title));
		}
		billingAddress.setFirstname(billingInfo.getFirstName());
		billingAddress.setLastname(billingInfo.getLastName());
		billingAddress.setLine1(billingInfo.getStreet1());
		billingAddress.setLine2(billingInfo.getStreet2());
		billingAddress.setTown(billingInfo.getCity());
		billingAddress.setPostalcode(billingInfo.getPostalCode());
		billingAddress.setCountry(getCommonI18NService().getCountry(billingInfo.getCountry()));
		if (billingInfo.getRegion() != null)
		{
			billingAddress.setRegion(getCommonI18NService().getRegion(billingAddress.getCountry(), billingInfo.getRegion()));
		}
		billingAddress.setPhone1(billingInfo.getPhoneNumber());
		final String email = getCustomerEmailResolutionService().getEmailForCustomer(customerModel);
		billingAddress.setEmail(email);
		billingAddress.setOwner(customerModel);
		getModelService().save(billingAddress);

		try
		{
			final NoCardPaymentInfoModel noCardPaymentInfoModel = getModelService().create(NoCardPaymentInfoModel.class);
			noCardPaymentInfoModel.setCode(customerModel.getUid() + "_" + UUID.randomUUID());
			noCardPaymentInfoModel.setUser(customerModel);
			noCardPaymentInfoModel.setBillingAddress(billingAddress);
			noCardPaymentInfoModel.setType(NoCardType.valueOf(noCardTypeCode));

			noCardPaymentInfoModel.setSaved(saveInAccount);

			billingAddress.setOwner(noCardPaymentInfoModel);
			getModelService().save(noCardPaymentInfoModel);
			getModelService().refresh(customerModel);

			addPaymentInfo(customerModel, noCardPaymentInfoModel);

			return Optional.ofNullable(noCardPaymentInfoModel);
		}
		catch (final AdapterException ae) //NOSONAR
		{
			LOG.error("Failed to create subscription for customer. Customer PK: " + customerModel.getPk() + " Exception: "
					+ ae.getClass().getName());

			return Optional.empty();
		}

	}

	/**
	 * Gets the no card payment infos.
	 *
	 * @param customerModel
	 *           the customer model
	 * @param saved
	 *           the saved
	 * @return the no card payment infos
	 */
	@Override
	public Optional<List<NoCardPaymentInfoModel>> getNoCardPaymentInfos(final CustomerModel customerModel, final boolean saved)
	{
		validateParameterNotNull(customerModel, CUSTOMER_VALIDATION_NONULL);
		return Optional.ofNullable(getCustomerAccountDao().findNoCardPaymentInfosByCustomer(customerModel, saved));
	}

	@Override
	protected CustomCustomerAccountDao getCustomerAccountDao()
	{
		return customerAccountDao;
	}


	/**
	 * Gets the no card payment info for code.
	 *
	 * @param customerModel
	 *           the customer model
	 * @param code
	 *           the code
	 * @return the no card payment info for code
	 */
	@Override
	public Optional<NoCardPaymentInfoModel> getNoCardPaymentInfoForCode(final CustomerModel customerModel, final String code)
	{
		validateParameterNotNull(customerModel, CUSTOMER_VALIDATION_NONULL);
		validateParameterNotNull(code, "code cannot be null");
		return Optional.ofNullable(getCustomerAccountDao().findNoCardPaymentInfoByCustomer(customerModel, code));
	}

	/**
	 * Gets the payment info for code.
	 *
	 * @param customerModel
	 *           the customer model
	 * @param code
	 *           the code
	 * @return the payment info for code
	 */
	@Override
	public Optional<PaymentInfoModel> getPaymentInfoForCode(final CustomerModel customerModel, final String code)
	{
		validateParameterNotNull(customerModel, CUSTOMER_VALIDATION_NONULL);
		validateParameterNotNull(code, "code cannot be null");
		return Optional.ofNullable(getCustomerAccountDao().findPaymentInfoByCustomer(customerModel, code));
	}


	@Override
	public void removePaymentSubscription(final AbstractOrderModel abstractOrderModel)
	{
		if (abstractOrderModel == null || abstractOrderModel.getPaymentInfo() == null)
		{
			return;
		}
		getModelService().remove(abstractOrderModel.getPaymentInfo());
		getModelService().save(abstractOrderModel);
		getModelService().refresh(abstractOrderModel);
	}

}
