package com.aldahra.aldahrapayment.customer.service.dao;

import de.hybris.platform.commerceservices.customer.dao.CustomerAccountDao;
import de.hybris.platform.core.model.order.payment.NoCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.List;


/**
 * The Interface CustomCustomerAccountDao.
 *
 * @author mnasro
 *
 */
public interface CustomCustomerAccountDao extends CustomerAccountDao
{

	/**
	 * Retrieves the customer's list of no card payment infos.
	 *
	 * @param customerModel
	 *           the customer
	 * @param saved
	 *           <code>true</code> to retrieve only saved no card payment infos
	 * @return the list of no card payment infos
	 */
	public List<NoCardPaymentInfoModel> findNoCardPaymentInfosByCustomer(CustomerModel customerModel, boolean saved);

	/**
	 * Retrieves the customer's no card payment info.
	 *
	 * @param customerModel
	 *           the customer
	 * @param code
	 *           the code of the no card payment info
	 * @return the no card payment info
	 */
	public NoCardPaymentInfoModel findNoCardPaymentInfoByCustomer(CustomerModel customerModel, String code);

	/**
	 * Retrieves the customer's no card payment info.
	 *
	 * @param customerModel
	 *           the customer
	 * @param code
	 *           the code of the no card payment info
	 * @return the no card payment info
	 */
	public PaymentInfoModel findPaymentInfoByCustomer(CustomerModel customerModel, String code);
}
