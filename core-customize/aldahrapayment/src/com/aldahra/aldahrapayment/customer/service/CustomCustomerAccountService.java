/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrapayment.customer.service;

import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.NoCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.payment.dto.BillingInfo;

import java.util.List;
import java.util.Optional;


/**
 * @author mnasro
 *
 *         The Interface CustomerAccountService.
 *
 */
public interface CustomCustomerAccountService extends CustomerAccountService
{

	/**
	 * Creates the payment subscription.
	 *
	 * @param customerModel
	 *           the customer model
	 * @param billingInfo
	 *           the billing info
	 * @param titleCode
	 *           the title code
	 * @param paymentProvider
	 *           the payment provider
	 * @param saveInAccount
	 *           the save in account
	 * @param noCardTypeCode
	 *           the no card type code
	 * @return the no card payment info model
	 */
	public Optional<NoCardPaymentInfoModel> createPaymentSubscription(final CustomerModel customerModel,
			final BillingInfo billingInfo, final String titleCode, final String paymentProvider, final boolean saveInAccount,
			final String noCardTypeCode);


	public void removePaymentSubscription(final AbstractOrderModel abstractOrderModel);

	/**
	 * Gets the no card payment infos.
	 *
	 * @param customerModel
	 *           the customer model
	 * @param saved
	 *           the saved
	 * @return the no card payment infos
	 */
	public Optional<List<NoCardPaymentInfoModel>> getNoCardPaymentInfos(final CustomerModel customerModel, final boolean saved);

	/**
	 * Gets the no card payment info for code.
	 *
	 * @param customerModel
	 *           the customer model
	 * @param code
	 *           the code
	 * @return the no card payment info for code
	 */
	public Optional<NoCardPaymentInfoModel> getNoCardPaymentInfoForCode(final CustomerModel customerModel, final String code);

	/**
	 * Gets the payment info for code.
	 *
	 * @param customerModel
	 *           the customer model
	 * @param code
	 *           the code
	 * @return the payment info for code
	 */
	public Optional<PaymentInfoModel> getPaymentInfoForCode(final CustomerModel customerModel, final String code);



}
