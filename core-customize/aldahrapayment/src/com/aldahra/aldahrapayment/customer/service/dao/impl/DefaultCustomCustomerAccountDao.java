package com.aldahra.aldahrapayment.customer.service.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commerceservices.customer.dao.CustomerAccountDao;
import de.hybris.platform.commerceservices.customer.dao.impl.DefaultCustomerAccountDao;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.payment.NoCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.aldahra.aldahrapayment.customer.service.dao.CustomCustomerAccountDao;


/**
 * @author mnasro
 *
 *         The Class DefaultCustomCustomerAccountDao.
 *
 */
public class DefaultCustomCustomerAccountDao extends DefaultCustomerAccountDao implements CustomCustomerAccountDao
{
	private static final String CUSTOMER_MUST_NOT_BE_NULL = "Customer must not be null";

	private static final String SELECT = "SELECT {";
	private static final String FROM = "} FROM {";
	private static final String WHERE = "} WHERE {";
	private static final String CUSTOMER_AND = "} = ?customer AND {";
	private static final String PK_AND = "} = ?pk AND {";
	private static final String DUPLICATE = "duplicate";
	private static final String Q_DUPLICATE = "} = ?" + DUPLICATE;
	/** The Constant FIND_SAVED_PAYMENT_INFOS_BY_CUSTOMER_QUERY. */
	private static final String FIND_SAVED_PAYMENT_INFOS_BY_CUSTOMER_QUERY = SELECT + ItemModel.PK + FROM
			+ NoCardPaymentInfoModel._TYPECODE + WHERE + PaymentInfoModel.USER + CUSTOMER_AND + PaymentInfoModel.SAVED
			+ "} = ?saved AND {" + PaymentInfoModel.DUPLICATE + Q_DUPLICATE;

	/** The Constant FIND_PAYMENT_INFOS_BY_CUSTOMER_QUERY. */
	private static final String FIND_PAYMENT_INFOS_BY_CUSTOMER_QUERY = SELECT + ItemModel.PK + FROM
			+ NoCardPaymentInfoModel._TYPECODE + WHERE + PaymentInfoModel.USER + CUSTOMER_AND + PaymentInfoModel.DUPLICATE
			+ Q_DUPLICATE;

	/** The Constant FIND_PAYMENT_INFO_BY_CUSTOMER_QUERY. */
	private static final String FIND_PAYMENT_INFO_BY_CUSTOMER_QUERY = SELECT + ItemModel.PK + FROM
			+ NoCardPaymentInfoModel._TYPECODE + WHERE + PaymentInfoModel.USER + CUSTOMER_AND + ItemModel.PK + PK_AND
			+ PaymentInfoModel.DUPLICATE + Q_DUPLICATE;

	/** The Constant FIND_GENERAL_PAYMENT_INFO_BY_CUSTOMER_QUERY. */
	private static final String FIND_GENERAL_PAYMENT_INFO_BY_CUSTOMER_QUERY = SELECT + ItemModel.PK + FROM
			+ PaymentInfoModel._TYPECODE + WHERE + PaymentInfoModel.USER + CUSTOMER_AND + ItemModel.PK + PK_AND
			+ PaymentInfoModel.DUPLICATE + Q_DUPLICATE;

	private static final String CUSTOMER = "customer";



	/** The customer account dao. */
	@Resource(name = "customerAccountDao")
	private CustomerAccountDao customerAccountDao;


	/**
	 * Find no card payment infos by customer.
	 *
	 * @param customerModel
	 *           the customer model
	 * @param saved
	 *           the saved
	 * @return the list
	 */
	@Override
	public List<NoCardPaymentInfoModel> findNoCardPaymentInfosByCustomer(final CustomerModel customerModel, final boolean saved)
	{
		validateParameterNotNull(customerModel, CUSTOMER_MUST_NOT_BE_NULL);
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put(CUSTOMER, customerModel);
		if (saved)
		{
			queryParams.put("saved", Boolean.TRUE);
		}
		queryParams.put(DUPLICATE, Boolean.FALSE);
		final SearchResult<NoCardPaymentInfoModel> result = getFlexibleSearchService()
				.search(saved ? FIND_SAVED_PAYMENT_INFOS_BY_CUSTOMER_QUERY : FIND_PAYMENT_INFOS_BY_CUSTOMER_QUERY, queryParams);
		return result.getResult();
	}


	/**
	 * Find no card payment info by customer.
	 *
	 * @param customerModel
	 *           the customer model
	 * @param code
	 *           the code
	 * @return the no card payment info model
	 */
	@Override
	public NoCardPaymentInfoModel findNoCardPaymentInfoByCustomer(final CustomerModel customerModel, final String code)
	{
		validateParameterNotNull(customerModel, CUSTOMER_MUST_NOT_BE_NULL);
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put(CUSTOMER, customerModel);
		queryParams.put(DUPLICATE, Boolean.FALSE);
		queryParams.put("pk", PK.parse(code));
		final SearchResult<NoCardPaymentInfoModel> result = getFlexibleSearchService().search(FIND_PAYMENT_INFO_BY_CUSTOMER_QUERY,
				queryParams);
		return result.getCount() > 0 ? result.getResult().get(0) : null;


	}


	/**
	 * Find payment info by customer.
	 *
	 * @param customerModel
	 *           the customer model
	 * @param code
	 *           the code
	 * @return the payment info model
	 */
	@Override
	public PaymentInfoModel findPaymentInfoByCustomer(final CustomerModel customerModel, final String code)
	{
		validateParameterNotNull(customerModel, CUSTOMER_MUST_NOT_BE_NULL);
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put(CUSTOMER, customerModel);
		queryParams.put(DUPLICATE, Boolean.FALSE);
		queryParams.put("pk", PK.parse(code));
		final SearchResult<PaymentInfoModel> result = getFlexibleSearchService().search(FIND_GENERAL_PAYMENT_INFO_BY_CUSTOMER_QUERY,
				queryParams);
		return result.getCount() > 0 ? result.getResult().get(0) : null;
	}

}

