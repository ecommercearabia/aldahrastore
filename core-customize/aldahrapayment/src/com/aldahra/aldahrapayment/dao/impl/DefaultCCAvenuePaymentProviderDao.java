/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrapayment.dao.impl;

import com.aldahra.aldahrapayment.dao.PaymentProviderDao;
import com.aldahra.aldahrapayment.model.CCAvenuePaymentProviderModel;


/**
 * @author mnasro
 *
 *         The Class DefaultCCAvenuePaymentProviderDao.
 */
public class DefaultCCAvenuePaymentProviderDao extends DefaultPaymentProviderDao implements PaymentProviderDao
{

	/**
	 * Instantiates a new default CC avenue payment provider dao.
	 */
	public DefaultCCAvenuePaymentProviderDao()
	{
		super(CCAvenuePaymentProviderModel._TYPECODE);
	}

	/**
	 * Gets the model name.
	 *
	 * @return the model name
	 */
	@Override
	protected String getModelName()
	{
		return CCAvenuePaymentProviderModel._TYPECODE;
	}

}
