/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrapayment.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.aldahra.aldahrapayment.dao.PaymentProviderDao;
import com.aldahra.aldahrapayment.model.PaymentProviderModel;
import com.google.common.base.Preconditions;


/**
 * @author mnasro
 *
 *         The Class DefaultPaymentProviderDao.
 */
public abstract class DefaultPaymentProviderDao extends DefaultGenericDao<PaymentProviderModel> implements PaymentProviderDao
{

	/** The base store service. */
	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	/** The Constant CODE_MUSTN_T_BE_NULL. */
	private static final String CODE_MUSTN_T_BE_NULL = "code mustn't be null or empty";

	/** The Constant STORES. */
	private static final String STORES = "stores";

	/** The Constant CODE. */
	private static final String CODE = "code";

	/** The Constant ACTIVE. */
	private static final String ACTIVE = "active";

	/**
	 * Instantiates a new default payment provider dao.
	 *
	 * @param typecode
	 *           the typecode
	 */
	public DefaultPaymentProviderDao(final String typecode)
	{
		super(typecode);
	}

	/**
	 * Gets the model name.
	 *
	 * @return the model name
	 */
	protected abstract String getModelName();

	/**
	 * Gets the.
	 *
	 * @param code
	 *           the code
	 * @return the optional
	 */
	@Override
	public Optional<PaymentProviderModel> get(final String code)
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(code), CODE_MUSTN_T_BE_NULL);
		final Map<String, Object> params = new HashMap<>();
		params.put(CODE, code);
		final List<PaymentProviderModel> find = find(params);
		final Optional<PaymentProviderModel> findFirst = find.stream().findFirst();

		return findFirst.isPresent() ? Optional.ofNullable(findFirst.get()) : Optional.ofNullable(null);
	}

	/**
	 * Gets the active.
	 *
	 * @param baseStoreUid
	 *           the base store uid
	 * @return the active
	 */
	@Override
	public Optional<PaymentProviderModel> getActive(final String baseStoreUid)
	{
		return getActive(baseStoreService.getBaseStoreForUid(baseStoreUid));
	}

	/**
	 * Gets the active.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @return the active
	 */
	@Override
	public Optional<PaymentProviderModel> getActive(final BaseStoreModel baseStoreModel)
	{
		Preconditions.checkArgument(baseStoreModel != null, "baseStoreModel must not be null");
		final Optional<Collection<PaymentProviderModel>> find = find(Arrays.asList(baseStoreModel), Boolean.TRUE, 1);

		return find.isPresent() && !find.get().isEmpty() ? Optional.ofNullable(find.get().iterator().next())
				: Optional.ofNullable(null);
	}

	/**
	 * Gets the active by current base store.
	 *
	 * @return the active by current base store
	 */
	@Override
	public Optional<PaymentProviderModel> getActiveByCurrentBaseStore()
	{
		return getActive(baseStoreService.getCurrentBaseStore());
	}



	/**
	 * Find.
	 *
	 * @param stores
	 *           the stores
	 * @param active
	 *           the active
	 * @param countRecords
	 *           the count records
	 * @return the optional
	 */
	protected Optional<Collection<PaymentProviderModel>> find(final List<BaseStoreModel> stores, final Boolean active,
			final Integer countRecords)
	{
		final Map<String, Object> params = new HashMap<>();
		final StringBuilder query = new StringBuilder();
		query.append("SELECT {pp.PK} ");
		query.append("FROM ");
		query.append("{ ");
		query.append(getModelName()).append(" AS pp ");


		if (CollectionUtils.isNotEmpty(stores))
		{
			query.append("JOIN PaymentProviderForBaseStore AS ppfbs ON {ppfbs.target}={pp.PK} ");
			query.append("JOIN BaseStore AS bs ON {ppfbs.source}={bs.PK} AND {bs.PK} IN (?stores) ");
			params.put(STORES, stores);
		}

		query.append("} ");

		int activeValue = 0;
		if (active == null || Boolean.TRUE.equals(active))
		{
			activeValue = 1;
		}
		query.append("WHERE {pp.active}=?active ");
		params.put(ACTIVE, activeValue);

		query.append("ORDER BY {pp.creationtime} DESC");

		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query.toString());
		flexibleSearchQuery.getQueryParameters().putAll(params);
		if (countRecords != null && countRecords > 0)
		{
			flexibleSearchQuery.setCount(countRecords);
		}

		final SearchResult<PaymentProviderModel> result = getFlexibleSearchService().search(flexibleSearchQuery);

		return result != null ? Optional.ofNullable(result.getResult()) : Optional.ofNullable(Collections.emptyList());
	}

}
