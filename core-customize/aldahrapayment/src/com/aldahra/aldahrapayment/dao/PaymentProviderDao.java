/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrapayment.dao;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.aldahra.aldahrapayment.model.PaymentProviderModel;


/**
 * @author mnasro
 *
 *         The Interface PaymentProviderDao.
 */
public interface PaymentProviderDao
{

	/**
	 * Gets the.
	 *
	 * @param code
	 *           the code
	 * @return the optional
	 */
	public Optional<PaymentProviderModel> get(String code);

	/**
	 * Gets the active.
	 *
	 * @param baseStoreUid
	 *           the base store uid
	 * @return the active
	 */
	public Optional<PaymentProviderModel> getActive(String baseStoreUid);

	/**
	 * Gets the active.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @return the active
	 */
	public Optional<PaymentProviderModel> getActive(BaseStoreModel baseStoreModel);

	/**
	 * Gets the active by current base store.
	 *
	 * @return the active by current base store
	 */
	public Optional<PaymentProviderModel> getActiveByCurrentBaseStore();
}
