/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.aldahra.aldahrawarehousingcustomwebservices.aldahrawarehousingcustomwebservices;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.oauth2.constants.OAuth2Constants;
import de.hybris.platform.warehousing.util.models.Warehouses;
import com.aldahra.aldahrawarehousingcustomwebservices.constants.AldahrawarehousingcustomwebservicesConstants;
import com.aldahra.aldahrawarehousingcustomwebservices.dto.store.WarehouseSearchPageWsDto;
import com.aldahra.aldahrawarehousingcustomwebservices.aldahrawarehousingcustomwebservices.util.BaseWarehousingWebservicesIntegrationTest;
import de.hybris.platform.webservicescommons.testsupport.server.NeedsEmbeddedServer;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@NeedsEmbeddedServer(webExtensions = { AldahrawarehousingcustomwebservicesConstants.EXTENSIONNAME, OAuth2Constants.EXTENSIONNAME })
@IntegrationTest
public class BaseStoresControllerIntegrationTest extends BaseWarehousingWebservicesIntegrationTest
{
	@Before
	public void setup()
	{
		super.setup();
	}

	@Test
	public void getAllDefaultWarehouse()
	{
		//When
		final WarehouseSearchPageWsDto result = getAllWarehousesByDefault();
		final List<String> warehouses = Arrays
				.asList(Warehouses.CODE_BOSTON, Warehouses.CODE_MONTREAL, Warehouses.CODE_GRIFFINTOWN);
		//then
		assertEquals(3, result.getWarehouses().size());
		assertTrue(result.getWarehouses().stream().anyMatch(warehouse -> warehouses.contains(warehouse.getCode())));
	}
}
