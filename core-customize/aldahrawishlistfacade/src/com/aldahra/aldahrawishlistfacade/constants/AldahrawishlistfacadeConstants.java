/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrawishlistfacade.constants;

/**
 * Global class for all Aldahrawishlistfacade constants. You can add global constants for your extension into this class.
 */
public final class AldahrawishlistfacadeConstants extends GeneratedAldahrawishlistfacadeConstants
{
	public static final String EXTENSIONNAME = "aldahrawishlistfacade";

	private AldahrawishlistfacadeConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "aldahrawishlistfacadePlatformLogo";
}
