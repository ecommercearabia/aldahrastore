/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrawishlistfacade.mapper;

import java.util.Optional;

import com.aldahra.aldahrawishlistfacade.data.ResponseData;



// TODO: Auto-generated Javadoc
//
/**
 * The Interface WishlistResponseMapper.
 *
 * @author mohammad-abu-muhasien
 */
public interface WishlistResponseMapper
{

	/**
	 * Gets the wish lists.
	 *
	 * @return the wish lists
	 */
	public Optional<ResponseData> getWishLists();




	/**
	 * Gets the default wish list.
	 *
	 * @return the default wish list
	 */
	public Optional<ResponseData> getDefaultWishList();

	/**
	 * Gets the wish list entries.
	 *
	 * @param wishlistPK
	 *           the wishlist PK
	 * @param currentPage
	 *           the current page
	 * @param pageSize
	 *           the page size
	 * @return the wish list entries
	 */
	public Optional<ResponseData> getWishListEntries(final String wishlistPK, final int currentPage, final int pageSize);


	/**
	 * Adds the entry to default wishlist by product code.
	 *
	 * @param productCode
	 *           the product code
	 * @return the optional
	 */
	public Optional<ResponseData> addEntryToDefaultWishlistByProductCode(final String productCode);

	/**
	 * Gets the default wishlist entry for product.
	 *
	 * @param productCode
	 *           the product code
	 * @return the default wishlist entry for product
	 */
	public Optional<ResponseData> getDefaultWishlistEntryForProduct(final String productCode);


	/**
	 * Gets the wishlist by PK.
	 *
	 * @param pk
	 *           the pk
	 * @return the wishlist by PK
	 */
	public Optional<ResponseData> getWishlistByPK(final String pk);

	/**
	 * Removes the wishlist entry for product.
	 *
	 * @param productCode
	 *           the product code
	 * @return the optional
	 */
	public Optional<ResponseData> removeWishlistEntryForProduct(String productCode);



	/**
	 * Removes the wishlist entry for product.
	 *
	 * @param wishlistPk
	 *           the wishlist pk
	 * @param productCode
	 *           the product code
	 * @return the optional
	 */
	public Optional<ResponseData> removeWishlistEntryForProduct(String wishlistPk, String productCode);

	/**
	 * Edits the wishlist name.
	 *
	 * @param newName
	 *           the new name
	 * @param wishlistPK
	 *           the wishlist PK
	 * @return the optional
	 */
	public Optional<ResponseData> editWishlistName(String newName, String wishlistPK);

	/**
	 * Adds the wishlist entry.
	 *
	 * @param productCode
	 *           the product code
	 * @param pk
	 *           the pk
	 * @return the optional
	 */
	public Optional<ResponseData> addWishlistEntry(final String productCode, final String pk);

	/**
	 * Adds the all entries to cart.
	 *
	 * @param wishlistPK
	 *           the wishlist PK
	 * @return the optional
	 */
	public Optional<ResponseData> addAllEntriesToCart(String wishlistPK);

	/**
	 * Removes the all wishlist entries.
	 *
	 * @param wishlistPKl
	 *           the wishlist P kl
	 * @return the optional
	 */
	public Optional<ResponseData> removeAllWishlistEntries(String wishlistPKl);

	/**
	 * Removes the wish list by PK.
	 *
	 * @param wishlistPK
	 *           the wishlist PK
	 * @return the optional
	 */
	public Optional<ResponseData> removeWishListByPK(String wishlistPK);

	/**
	 * Creates the wishlist.
	 *
	 * @param name
	 *           the name
	 * @param description
	 *           the description
	 * @param defaultWL
	 *           the default WL
	 * @return the optional
	 */
	public Optional<ResponseData> createWishlist(String name, String description, Boolean defaultWL);

	/**
	 * Checks if is product in wish list.
	 *
	 * @param productCode
	 *           the product code
	 * @return the optional
	 */
	public Optional<ResponseData> isProductInWishList(String productCode);


	/**
	 * Checks if is product in wish list.
	 *
	 * @param wishlistPK
	 *           the wishlist PK
	 * @param productCode
	 *           the product code
	 * @return the optional
	 */
	public Optional<ResponseData> isProductInWishList(final String wishlistPK, final String productCode);


}
