/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrawishlistfacade.mapper.impl;

import de.hybris.platform.cms2.data.PageableData;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;

import com.aldahra.aldahrawishlist.exception.WishlistException;
import com.aldahra.aldahrawishlistfacade.data.MetaData;
import com.aldahra.aldahrawishlistfacade.data.ResponseData;
import com.aldahra.aldahrawishlistfacade.data.WishlistEntryPriorityData;
import com.aldahra.aldahrawishlistfacade.facade.WishlistFacade;
import com.aldahra.aldahrawishlistfacade.mapper.WishlistResponseMapper;
import com.google.common.base.Preconditions;



/**
 * @author mohammad-abu-muhasien
 */
public class DefaultWishlistResponseMapper implements WishlistResponseMapper
{



	private static final String THE_RESPONSE_RETURNED_SUCCESSFULLY = "The response  returned successfully";

	/** The wishlist facade. */
	@Resource(name = "wishlistFacade")
	public WishlistFacade wishlistFacade;

	private ConfigurationService configurationService;


	protected Optional<ResponseData> getResponseData(final String message, final String method, final Object[] arg,
			final Class... clazz)
	{

		Preconditions.checkArgument(StringUtils.isNotEmpty(message), "message can not be null or empty");
		Preconditions.checkArgument(StringUtils.isNotEmpty(method), "method can not be null or empty");
		final ResponseData responseData = new ResponseData();
		final MetaData meta = new MetaData();
		final Object data = null;
		try
		{
			Method declaredMethod = null;
			if (clazz == null || clazz.length == 0)
			{
				declaredMethod = wishlistFacade.getClass().getDeclaredMethod(method);
			}
			else
			{
				declaredMethod = wishlistFacade.getClass().getDeclaredMethod(method, clazz);
			}
			final Object invoke = declaredMethod.invoke(wishlistFacade, arg);
			if (invoke != null && invoke instanceof Optional<?> && ((Optional<?>) invoke).isPresent())
			{
				responseData.setData(((Optional<?>) invoke).get());
			}
			else
			{
				responseData.setData(invoke);
				meta.setMessage(message);
				meta.setStatusCode(HttpStatus.SC_OK);
			}
		}
		catch (final Exception ex)
		{
			if (ex instanceof InvocationTargetException)
			{
				if (ex.getCause() instanceof WishlistException)
				{
					final WishlistException wishlistException = (WishlistException) ex.getCause();
					switch (wishlistException.getWishlistExceptionType())
					{
						case WISHLIST_NOT_FOUND:
							meta.setStatusCode(HttpStatus.SC_NOT_FOUND);
							break;
						case LOGIN_REQUIRED:
							meta.setStatusCode(HttpStatus.SC_UNAUTHORIZED);
							break;
						case MAX_CAPACITY:

						case PRODUCT_NOT_FOUND:
						case PRODUCT_IS_EXIST:
						default:
							meta.setStatusCode(HttpStatus.SC_BAD_REQUEST);
					}
					meta.setMessage(wishlistException.getWishlistExceptionType().name() + " : " + ex.getMessage());
				}
				else
				{

					meta.setMessage(ex.getCause().getMessage());

					meta.setStatusCode(HttpStatus.SC_BAD_REQUEST);
				}


			}
			meta.setMessage(ex.getCause().getMessage());
		}
		responseData.setMeta(meta);
		return Optional.ofNullable(responseData);
	}


	@Override
	public Optional<ResponseData> getWishLists()
	{

		return getResponseData(THE_RESPONSE_RETURNED_SUCCESSFULLY, "getWishLists", null);
	}


	@Override
	public Optional<ResponseData> getDefaultWishList()
	{

		return getResponseData(THE_RESPONSE_RETURNED_SUCCESSFULLY, "getDefaultWishList", null);
	}


	@Override
	public Optional<ResponseData> getWishListEntries(final String wishlistPK, final int currentPage, final int pageSize)
	{
		final PageableData pageableData = new PageableData();
		pageableData.setCurrentPage(currentPage);
		pageableData.setPageSize(pageSize);
		return getResponseData(THE_RESPONSE_RETURNED_SUCCESSFULLY, "getWishListEntries", new Object[]
		{ wishlistPK, pageableData }, new Class[]
		{ String.class, PageableData.class });
	}


	@Override
	public Optional<ResponseData> getWishlistByPK(final String pk)
	{

		return getResponseData(THE_RESPONSE_RETURNED_SUCCESSFULLY, "getWishlistByPK", new Object[]
		{ pk }, new Class[]
		{ String.class });
	}


	@Override
	public Optional<ResponseData> removeWishlistEntryForProduct(final String productCode)
	{

		return getResponseData(THE_RESPONSE_RETURNED_SUCCESSFULLY, "removeWishlistEntryForProduct", new Object[]
		{ productCode }, new Class[]
		{ String.class });
	}


	@Override
	public Optional<ResponseData> removeWishlistEntryForProduct(final String wishlistPk, final String productCode)
	{

		return getResponseData(THE_RESPONSE_RETURNED_SUCCESSFULLY, "removeWishlistEntryForProduct", new Object[]
		{ wishlistPk, productCode }, new Class[]
		{ String.class, String.class });
	}

	@Override
	public Optional<ResponseData> getDefaultWishlistEntryForProduct(final String productCode)
	{

		return getResponseData(THE_RESPONSE_RETURNED_SUCCESSFULLY, "getDefaultWishlistEntryForProduct", new Object[]
		{ productCode }, new Class[]
		{ String.class });
	}

	@Override
	public Optional<ResponseData> editWishlistName(final String newName, final String wishlistPK)
	{

		return getResponseData(THE_RESPONSE_RETURNED_SUCCESSFULLY, "editWishlistName", new Object[]
		{ newName, wishlistPK }, new Class[]
		{ String.class, String.class });
	}


	@Override
	public Optional<ResponseData> addWishlistEntry(final String productCode, final String pk)
	{

		return getResponseData(THE_RESPONSE_RETURNED_SUCCESSFULLY, "addWishlistEntry", new Object[]
		{ pk, productCode, 0, null, "" }, new Class[]
		{ String.class, String.class, Integer.class, WishlistEntryPriorityData.class, String.class });
	}

	@Override
	public Optional<ResponseData> addEntryToDefaultWishlistByProductCode(final String productCode)
	{

		final ResponseData responseData = new ResponseData();
		final MetaData meta = new MetaData();
		final Object data = null;
		try
		{
			wishlistFacade.addEntryToDefaultWishlistByProductCode(productCode, null, null, null);

			meta.setMessage(THE_RESPONSE_RETURNED_SUCCESSFULLY);
			meta.setStatusCode(HttpStatus.SC_OK);
		}
		catch (final WishlistException wishlistException)
		{


			switch (wishlistException.getWishlistExceptionType())
			{
				case WISHLIST_NOT_FOUND:
					meta.setStatusCode(HttpStatus.SC_NOT_FOUND);
					break;
				case LOGIN_REQUIRED:
					meta.setStatusCode(HttpStatus.SC_UNAUTHORIZED);
					break;
				case MAX_CAPACITY:

				case PRODUCT_NOT_FOUND:
				case PRODUCT_IS_EXIST:
				default:
					meta.setStatusCode(HttpStatus.SC_BAD_REQUEST);
			}
			meta.setMessage(wishlistException.getWishlistExceptionType().name() + " : " + wishlistException.getMessage());
		}
		return Optional.ofNullable(responseData);
	}

	@Override
	public Optional<ResponseData> addAllEntriesToCart(final String wishlistPK)
	{

		return getResponseData(THE_RESPONSE_RETURNED_SUCCESSFULLY, "addAllEntriesToCart", new Object[]
		{ wishlistPK }, new Class[]
		{ String.class });
	}


	@Override
	public Optional<ResponseData> removeAllWishlistEntries(final String wishlistPKl)
	{

		return getResponseData(THE_RESPONSE_RETURNED_SUCCESSFULLY, "removeAllWishlistEntries", new Object[]
		{ wishlistPKl }, new Class[]
		{ String.class });
	}


	@Override
	public Optional<ResponseData> removeWishListByPK(final String wishlistPK)
	{

		return getResponseData(THE_RESPONSE_RETURNED_SUCCESSFULLY, "removeWishListByPK", new Object[]
		{ wishlistPK }, new Class[]
		{ String.class });
	}


	@Override
	public Optional<ResponseData> createWishlist(final String name, final String description, final Boolean defaultWL)
	{

		return getResponseData(THE_RESPONSE_RETURNED_SUCCESSFULLY, "createWishlist", new Object[]
		{ name, description, defaultWL }, new Class[]
		{ String.class, String.class, Boolean.class });
	}


	@Override
	public Optional<ResponseData> isProductInWishList(final String productCode)
	{

		return getResponseData(THE_RESPONSE_RETURNED_SUCCESSFULLY, "isProductInWishList", new Object[]
		{ productCode }, new Class[]
		{ String.class });
	}


	@Override
	public Optional<ResponseData> isProductInWishList(final String wishlistPK, final String productCode)
	{

		return getResponseData(THE_RESPONSE_RETURNED_SUCCESSFULLY, "isProductInWishList", new Object[]
		{ wishlistPK, productCode }, new Class[]
		{ String.class, String.class });
	}
}
