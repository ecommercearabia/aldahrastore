/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrawishlistfacade.facade;

import de.hybris.platform.cms2.data.PageableData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;

import java.util.List;
import java.util.Optional;

import com.aldahra.aldahrawishlist.exception.WishlistException;
import com.aldahra.aldahrawishlistfacade.data.WishlistData;
import com.aldahra.aldahrawishlistfacade.data.WishlistEntryData;
import com.aldahra.aldahrawishlistfacade.data.WishlistEntryPriorityData;




/**
 * The Interface WishlistFacade.
 *
 * @author mohammad-abu muhasien
 */

public interface WishlistFacade
{





	/**
	 * Returns wishlists List for the current user.
	 *
	 * @return wishlists
	 * @throws WishlistException
	 *            the wishlist exception
	 */
	public Optional<List<WishlistData>> getWishLists() throws WishlistException;

	/**
	 * Returns wishlists List for current user.
	 *
	 * @return wishlists
	 * @throws WishlistException
	 *            the wishlist exception
	 */

	public Optional<WishlistData> getDefaultWishList() throws WishlistException;

	/**
	 * Returns wishlist entries List for given user.
	 *
	 * @param wishlistPK
	 *           the wishlist PK
	 * @return wishlists entries
	 * @throws WishlistException
	 *            the wishlist exception
	 */
	public Optional<List<WishlistEntryData>> getWishListEntries(String wishlistPK) throws WishlistException;

	/**
	 * Returns wishlist entries with pagination options for given wishlist.
	 *
	 * @param wishlistPK
	 *           the wishlist PK
	 * @param pageableData
	 *           the pageable data
	 * @return the wish list entries
	 * @throws WishlistException
	 *            the wishlist exception
	 */
	public Optional<List<WishlistEntryData>> getWishListEntries(String wishlistPK, PageableData pageableData)
			throws WishlistException;

	/**
	 * Returns wishlist entry by product for current user.
	 *
	 * @param productCode
	 *           the product code
	 * @param wishlistPK
	 *           the wishlist PK
	 * @return the wishlist entry for product
	 * @throws WishlistException
	 *            the wishlist exception
	 */
	public Optional<WishlistEntryData> getWishlistEntryForProduct(String productCode, String wishlistPK) throws WishlistException;


	/**
	 * Gets the wishlist entry for product.
	 *
	 * @param productCode
	 *           the product code
	 * @return the wishlist entry for product
	 * @throws WishlistException
	 *            the wishlist exception
	 */
	public Optional<WishlistEntryData> getDefaultWishlistEntryForProduct(String productCode) throws WishlistException;

	/**
	 * Removes wishlist entry by product code for given user.
	 *
	 * @param productCode
	 *           the product code
	 * @throws WishlistException
	 *            the wishlist exception
	 */
	public void removeWishlistEntryForProduct(String productCode) throws WishlistException;


	/**
	 * Removes wishlist entry by product code for given user.
	 *
	 * @param wishlistPk
	 *           the wishlist pk
	 * @param productCode
	 *           the product code
	 * @throws WishlistException
	 *            the wishlist exception
	 */
	public void removeWishlistEntryForProduct(String wishlistPk, String productCode) throws WishlistException;

	/**
	 * Removes all wishlist entries for given user.
	 *
	 * @param wishlistPKl
	 *           the wishlist P kl
	 * @throws WishlistException
	 *            the wishlist exception
	 */
	public void removeAllWishlistEntries(String wishlistPKl) throws WishlistException;

	public void removeAllEntriesForDefaultWishlist() throws WishlistException;

	/**
	 * remove WishList by PK.
	 *
	 * @param wishlistPK
	 *           the wishlist PK
	 * @throws WishlistException
	 *            the wishlist exception
	 */
	public void removeWishListByPK(String wishlistPK) throws WishlistException;

	/**
	 * Adds wishlist entry by product code for given user.
	 *
	 * @param wishlistPK
	 *           the wishlist PK
	 * @param productCode
	 *           the product code
	 * @param desired
	 *           the desired
	 * @param priority
	 *           the priority
	 * @param comment
	 *           the comment
	 * @throws WishlistException
	 *            the wishlist exception
	 */
	public void addWishlistEntry(String wishlistPK, String productCode, Integer desired, WishlistEntryPriorityData priority,
			String comment) throws WishlistException;

	/**
	 * Adds the entry to default wishlist by product code.
	 *
	 * @param productCode
	 *           the product code
	 * @param desired
	 *           the desired
	 * @param priority
	 *           the priority
	 * @param comment
	 *           the comment
	 * @throws WishlistException
	 *            the wishlist exception
	 */
	public void addEntryToDefaultWishlistByProductCode(String productCode, Integer desired, WishlistEntryPriorityData priority,
			String comment) throws WishlistException;

	public void addAllDefaultWishlistEntryToCart() throws WishlistException, CommerceCartModificationException;

	/**
	 * Changes wishlist's name.
	 *
	 * @param newName
	 *           the new name
	 * @param wishlistPK
	 *           the wishlist PK
	 * @return the optional
	 * @throws WishlistException
	 *            the wishlist exception
	 * @returnWishlistData
	 */
	public Optional<WishlistData> editWishlistName(String newName, String wishlistPK) throws WishlistException;

	/**
	 * Adds all products in a wishlist to the cart for current user.
	 *
	 * @param wishlistPK
	 *           the wishlist PK
	 * @throws CommerceCartModificationException
	 *            the commerce cart modification exception
	 * @throws WishlistException
	 *            the wishlist exception
	 */
	public void addAllEntriesToCart(String wishlistPK) throws CommerceCartModificationException, WishlistException;

	/**
	 * Returns wishlist entries with pagination and product options.
	 *
	 * @param wishlistPK
	 *           the wishlist PK
	 * @param pageableData
	 *           the pageable data
	 * @param productOptions
	 *           the product options
	 * @return the wish list entries
	 * @throws WishlistException
	 *            the wishlist exception
	 */
	public Optional<List<WishlistEntryData>> getWishListEntries(final String wishlistPK, final PageableData pageableData,
			List<ProductOption> productOptions) throws WishlistException;


	/**
	 * Creates new wishlist for given user.
	 *
	 * @param name
	 *           the name
	 * @param description
	 *           the description
	 * @param defaultWL
	 *           the default WL
	 * @return the optional
	 * @throws WishlistException
	 *            the wishlist exception
	 */
	public Optional<WishlistData> createWishlist(String name, String description, Boolean defaultWL) throws WishlistException;

	/**
	 * Returns WishlistData By PK for given user.
	 *
	 * @param wishlistPK
	 *           the wishlist PK
	 * @return WishlistData
	 * @throws WishlistException
	 *            the wishlist exception
	 */
	public Optional<WishlistData> getWishlistByPK(String wishlistPK) throws WishlistException;


	/**
	 * return true is found.
	 *
	 * @param productCode
	 *           the product code
	 * @return boolean
	 * @throws WishlistException
	 *            the wishlist exception
	 */
	public boolean isProductInWishList(String productCode) throws WishlistException;

	/**
	 * Checks if is product in wish list.
	 *
	 * @param wishlistPK
	 *           the wishlist PK
	 * @param productCode
	 *           the product code
	 * @return true, if is product in wish list
	 * @throws WishlistException
	 *            the wishlist exception
	 */
	public boolean isProductInWishList(final String wishlistPK, final String productCode) throws WishlistException;

	/**
	 * Gets the wish list entries count.
	 *
	 * @param wishlistPK
	 *           the wishlist PK
	 * @param pageableData
	 *           the pageable data
	 * @return the wish list entries count
	 * @throws WishlistException
	 *            the my wishlist execption
	 */
	public int getWishListEntriesCount(final String wishlistPK, final PageableData pageableData) throws WishlistException;

	public List<CartModificationData> addAllEntriesToCartWithCartModification(String wishlistPK)
			throws CommerceCartModificationException, WishlistException;


}
