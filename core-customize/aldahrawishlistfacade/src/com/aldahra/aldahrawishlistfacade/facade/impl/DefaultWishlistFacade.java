/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrawishlistfacade.facade.impl;

import de.hybris.platform.cms2.data.PageableData;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.impl.DefaultCartFacade;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.wishlist2.enums.Wishlist2EntryPriority;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.aldahra.aldahrawishlist.exception.WishlistException;
import com.aldahra.aldahrawishlist.service.WishlistService;
import com.aldahra.aldahrawishlistfacade.data.WishlistData;
import com.aldahra.aldahrawishlistfacade.data.WishlistEntryData;
import com.aldahra.aldahrawishlistfacade.data.WishlistEntryPriorityData;
import com.aldahra.aldahrawishlistfacade.facade.WishlistFacade;




/**
 * The Class DefaultSefamWishlistFacade.
 *
 * @author mohammad-abu-muhasien
 */


public class DefaultWishlistFacade implements WishlistFacade
{

	/** The Aldahra wishlist service. */
	@Resource(name = "wishlistService")
	public WishlistService wishlistService;

	/** The product service. */
	@Resource(name = "productService")
	private ProductService productService;

	/** The user service. */
	@Resource(name = "userService")
	private UserService userService;


	/** The wishlist converter. */
	@Resource(name = "wishlistConverter")
	private Converter<Wishlist2Model, WishlistData> wishlistConverter;

	/** The wishlist entry converter. */
	@Resource(name = "wishlistEntryConverter")
	private Converter<Wishlist2EntryModel, WishlistEntryData> wishlistEntryConverter;

	/** The cms site service. */
	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	/** The model service. */
	@Resource(name = "modelService")
	private ModelService modelService;

	/** The enumeration service. */
	@Resource(name = "enumerationService")
	private EnumerationService enumerationService;

	/** The cart facade. */
	@Resource(name = "cartFacade")
	private CartFacade cartFacade;

	private DefaultCartFacade defaultCartFacade;

	/** The product facade. */
	@Resource(name = "productFacade")
	private ProductFacade productFacade;

	/** The wishlist entry priority converter. */
	@Resource(name = "wishlistEntryPriorityConverter")
	private Converter<Wishlist2EntryPriority, WishlistEntryPriorityData> wishlistEntryPriorityConverter;

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(DefaultWishlistFacade.class);

	/**
	 * Gets the wish lists.
	 *
	 * @return the wish lists
	 * @throws WishlistException
	 */
	@Override
	public Optional<List<WishlistData>> getWishLists() throws WishlistException
	{

		final Optional<List<Wishlist2Model>> optiWishlists = wishlistService.getWishListsByCurrentUser();
		if (!optiWishlists.isPresent())
		{

			LOG.debug(String.format("There's no wishlists for the current user [%s]", userService.getCurrentUser().getUid()));
			return Optional.empty();
		}
		final List<Wishlist2Model> wishlist2s = optiWishlists.get();
		return Optional.ofNullable(wishlistConverter.convertAll(wishlist2s));

	}

	/**
	 * Gets the default wish list.
	 *
	 * @return the default wish list
	 * @throws WishlistException
	 */
	@Override
	public Optional<WishlistData> getDefaultWishList() throws WishlistException
	{
		Wishlist2Model defaultWishlist = wishlistService.getDefaultWishlist();
		if (defaultWishlist == null)
		{
			LOG.info("There's no default wishlist.");
			LOG.info("Create default wishlists.");

			final Optional<WishlistData> createWishlist = createWishlist(getDefaultWishlistName(), getDefaultWishlistName(),
					Boolean.TRUE);
			defaultWishlist = wishlistService.getDefaultWishlist();
			LOG.info(" default wishlist has been created successfully");

		}

		if (defaultWishlist == null)
		{
			return Optional.empty();
		}

		return Optional.ofNullable(wishlistConverter.convert(defaultWishlist));
	}


	private String getDefaultWishlistName()
	{

		return cmsSiteService.getCurrentSite() == null
				|| StringUtils.isEmpty(cmsSiteService.getCurrentSite().getDefaultWishlistTitle()) ? "Wishlist"
						: cmsSiteService.getCurrentSite().getDefaultWishlistTitle();

	}

	/**
	 * Gets the wish list entries.
	 *
	 * @param wishlistPK
	 *           the wishlist PK
	 * @return the wish list entries
	 * @throws WishlistException
	 */
	@Override
	public Optional<List<WishlistEntryData>> getWishListEntries(final String wishlistPK) throws WishlistException
	{
		final Optional<List<Wishlist2EntryModel>> optWishlist = wishlistService.getWishlistEntries(wishlistPK);

		if (!optWishlist.isPresent())
		{
			LOG.debug(String.format("There's no entries for wishlist with PK [%s]", wishlistPK));
			return Optional.empty();

		}
		return Optional.ofNullable(wishlistEntryConverter.convertAll(optWishlist.get()));

	}

	/**
	 * Gets the wish list entries.
	 *
	 * @param wishlistPK
	 *           the wishlist PK
	 * @param pageableData
	 *           the pageable data
	 * @return the wish list entries
	 * @throws WishlistException
	 */
	@Override
	public Optional<List<WishlistEntryData>> getWishListEntries(final String wishlistPK, final PageableData pageableData)
			throws WishlistException
	{
		final Optional<List<Wishlist2EntryModel>> optWishlist = wishlistService.getWishlistEntries(wishlistPK, pageableData);

		if (!optWishlist.isPresent())
		{

			LOG.debug(String.format("There's no entries for wishlist with PK [%s]", wishlistPK));
			return Optional.empty();

		}
		return Optional.ofNullable(wishlistEntryConverter.convertAll(optWishlist.get()));

	}

	@Override
	public Optional<WishlistEntryData> getDefaultWishlistEntryForProduct(final String productCode) throws WishlistException
	{
		final Optional<WishlistData> defaultWishList = getDefaultWishList();
		if (defaultWishList.isPresent())
		{
			return getWishlistEntryForProduct(productCode, defaultWishList.get().getPk());
		}

		return Optional.empty();
	}

	/**
	 * Gets the wishlist entry for product.
	 *
	 * @param productCode
	 *           the product code
	 * @param wishlistPK
	 *           the wishlist PK
	 * @return the wishlist entry for product
	 * @throws WishlistException
	 */
	@Override
	public Optional<WishlistEntryData> getWishlistEntryForProduct(final String productCode, final String wishlistPK)
			throws WishlistException
	{
		final Optional<Wishlist2EntryModel> optWishlistEntry = wishlistService.getWishlistEntryByProductCode(productCode,
				wishlistPK);
		if (!optWishlistEntry.isPresent())
		{
			return Optional.empty();
		}
		return Optional.ofNullable(wishlistEntryConverter.convert(optWishlistEntry.get()));

	}

	/**
	 * Removes the wishlist entry for product.
	 *
	 * @param productCode
	 *           the product code
	 * @throws WishlistException
	 */
	@Override
	public void removeWishlistEntryForProduct(final String productCode) throws WishlistException
	{
		wishlistService.removeWishlistEntryForProduct(productCode);

	}

	/**
	 * Removes the wishlist entry for product.
	 *
	 * @param wishlistPk
	 *           the wishlist pk
	 * @param productCode
	 *           the product code
	 * @throws WishlistException
	 */
	@Override
	public void removeWishlistEntryForProduct(final String wishlistPk, final String productCode) throws WishlistException
	{
		wishlistService.removeWishlistEntryForProduct(wishlistPk, productCode);
	}


	/**
	 * Removes the all wishlist entries.
	 *
	 * @param wishlistPKl
	 *           the wishlist P kl
	 * @throws WishlistException
	 */
	@Override
	public void removeAllWishlistEntries(final String wishlistPKl) throws WishlistException
	{
		wishlistService.removeAllWishlistEntries(wishlistPKl);
	}


	/**
	 * Removes the wish list by PK.
	 *
	 * @param wishlistPK
	 *           the wishlist PK
	 * @throws WishlistException
	 */
	@Override
	public void removeWishListByPK(final String wishlistPK) throws WishlistException
	{
		wishlistService.removeWishList(wishlistPK);
	}


	/**
	 * Adds the wishlist entry.
	 *
	 * @param wishlistPK
	 *           the wishlist PK
	 * @param productCode
	 *           the product code
	 * @param desired
	 *           the desired
	 * @param priority
	 *           the priority
	 * @param comment
	 *           the comment
	 * @throws WishlistMaxCapacityException
	 *            the wishlist max capacity exception
	 * @throws ProductExistInWishlistException
	 *            the product exist in wishlist exception
	 * @throws ProductNotFoundException
	 *            the product not found exception
	 * @throws WishlistException
	 */
	@Override
	public void addWishlistEntry(final String wishlistPK, final String productCode, final Integer desired,
			final WishlistEntryPriorityData priority, final String comment) throws WishlistException
	{

		final Wishlist2EntryPriority entryPriority = (priority == null) ? Wishlist2EntryPriority.MEDIUM
				: enumerationService.getEnumerationValue(Wishlist2EntryPriority._TYPECODE, priority.getCode());
		wishlistService.addWishlistEntryWithCapacitySite(wishlistPK, productCode, desired, entryPriority, comment,
				cmsSiteService.getCurrentSite());
	}


	/**
	 * Edits the wishlist name.
	 *
	 * @param newName
	 *           the new name
	 * @param wishlistPK
	 *           the wishlist PK
	 * @return the wishlist data
	 * @throws WishlistException
	 */
	@Override
	public Optional<WishlistData> editWishlistName(final String newName, final String wishlistPK) throws WishlistException
	{

		final Optional<Wishlist2Model> editWishlistName = wishlistService.editWishlistName(newName, wishlistPK);
		if (!editWishlistName.isPresent())
		{
			return Optional.empty();
		}
		return Optional.ofNullable(wishlistConverter.convert(editWishlistName.get()));

	}


	/**
	 * Adds the all entries to cart.
	 *
	 * @param wishlistPK
	 *           the wishlist PK
	 * @throws CommerceCartModificationException
	 *            the commerce cart modification exception
	 * @throws WishlistException
	 */
	@Override
	public void addAllEntriesToCart(final String wishlistPK) throws CommerceCartModificationException, WishlistException
	{
		final Optional<Wishlist2Model> wishlistModel = wishlistService.getWishlistByPK(wishlistPK);
		if (wishlistModel.isPresent())
		{
			final List<Wishlist2EntryModel> wishlistEntries = wishlistModel.get().getEntries();
			if (wishlistEntries != null && !wishlistEntries.isEmpty())
			{

				wishlistEntries.forEach(entry -> {
					try
					{
						final CartModificationData addToCart = cartFacade.addToCart(entry.getProduct().getCode(), 1);
					}
					catch (final CommerceCartModificationException e)
					{
						LOG.error(e);
					}
				});
			}
		}


	}

	/**
	 * Gets the wish list entries.
	 *
	 * @param wishlistPK
	 *           the wishlist PK
	 * @param pageableData
	 *           the pageable data
	 * @param productOptions
	 *           the product options
	 * @return the wish list entries
	 * @throws WishlistException
	 */
	@Override
	public Optional<List<WishlistEntryData>> getWishListEntries(final String wishlistPK, final PageableData pageableData,
			final List<ProductOption> productOptions) throws WishlistException
	{
		final Optional<Wishlist2Model> wishlistModel = wishlistService.getWishlistByPK(wishlistPK);

		final Optional<List<Wishlist2EntryModel>> optWishlistEntries = wishlistService.getWishlistEntries(wishlistPK, pageableData);
		if (!optWishlistEntries.isPresent())
		{
			return Optional.empty();
		}
		final List<Wishlist2EntryModel> wishlistEntries = optWishlistEntries.get();
		List<WishlistEntryData> wishlistEntryDatas = null;

		if (wishlistEntries.isEmpty())
		{
			return Optional.empty();
		}
		wishlistEntryDatas = new ArrayList<>();
		WishlistEntryData wishlistEntryData = null;
		for (final Wishlist2EntryModel wishlist2EntryModel : wishlistEntries)
		{
			wishlistEntryData = new WishlistEntryData();
			wishlistEntryData.setAddedDate(wishlist2EntryModel.getAddedDate());
			wishlistEntryData.setComment(wishlist2EntryModel.getComment());
			wishlistEntryData.setDesired(wishlist2EntryModel.getDesired());
			if (wishlist2EntryModel.getPriority() != null)
			{
				wishlistEntryData.setPriority(wishlistEntryPriorityConverter.convert(wishlist2EntryModel.getPriority()));
			}
			if (wishlist2EntryModel.getProduct() != null)
			{
				try
				{
					final ProductData productData = productFacade
							.getProductForCodeAndOptions(wishlist2EntryModel.getProduct().getCode(), productOptions);
					wishlistEntryData.setProduct(productData);
				}
				catch (final UnknownIdentifierException e)
				{
					LOG.error(e.getMessage());
					continue;
				}
			}

			wishlistEntryData.setReceived(wishlist2EntryModel.getReceived());
			if (wishlistModel.isPresent())
			{
				final Wishlist2Model model = wishlistModel.get();
				wishlistEntryData.setWishlistPK(model.getPk().toString());
			}

			wishlistEntryDatas.add(wishlistEntryData);
		}

		return Optional.ofNullable(wishlistEntryDatas);


	}

	/**
	 * Creates the wishlist.
	 *
	 * @param name
	 *           the name
	 * @param description
	 *           the description
	 * @param defaultWL
	 *           the default WL
	 * @return the wishlist data
	 */
	@Override
	public Optional<WishlistData> createWishlist(final String name, final String description, final Boolean defaultWL)
	{
		if (Boolean.TRUE.equals(defaultWL))
		{
			final Wishlist2Model wishlist2Model = wishlistService.createDefaultWishlist(name, description);
			//			modelService.refresh(wishlist2Model);
			if (wishlist2Model != null)
			{
				return Optional.ofNullable(wishlistConverter.convert(wishlist2Model));

			}
		}
		else
		{
			final Wishlist2Model wishlist2Model = wishlistService.createWishlist(name, description);
			//			modelService.refresh(wishlist2Model);
			if (wishlist2Model != null)
			{
				return Optional.ofNullable(wishlistConverter.convert(wishlist2Model));

			}
		}
		return Optional.empty();
	}

	/**
	 * Gets the wish list by PK.
	 *
	 * @param wishlistPK
	 *           the wishlist PK
	 * @return the wish list by PK
	 * @throws WishlistException
	 */
	@Override
	public Optional<WishlistData> getWishlistByPK(final String wishlistPK) throws WishlistException
	{
		final Optional<Wishlist2Model> wishlist2Model = wishlistService.getWishlistByPK(wishlistPK);
		if (wishlist2Model.isPresent())
		{
			return Optional.ofNullable(wishlistConverter.convert(wishlist2Model.get()));
		}
		return Optional.empty();
	}

	/**
	 * Checks if is product in wish list.
	 *
	 * @param productCode
	 *           the product code
	 * @return true, if is product in wish list
	 * @throws ProductNotFoundException
	 *            the product not found exception
	 * @throws WishlistException
	 */
	@Override
	public boolean isProductInWishList(final String productCode) throws WishlistException
	{
		try
		{
			return wishlistService.isProductInWishList(productCode);
		}
		catch (final WishlistException e)
		{
			return false;
		}
	}

	/**
	 * Checks if is product in wish list.
	 *
	 * @param wishlistPK
	 *           the wishlist PK
	 * @param productCode
	 *           the product code
	 * @return true, if is product in wish list
	 * @throws WishlistException
	 */
	@Override
	public boolean isProductInWishList(final String wishlistPK, final String productCode) throws WishlistException
	{
		return wishlistService.isProductInWishList(wishlistPK, productCode);
	}

	@Override
	public int getWishListEntriesCount(final String wishlistPK, final PageableData pageableData) throws WishlistException
	{
		final Optional<Wishlist2Model> wishListByPK = wishlistService.getWishlistByPK(wishlistPK);

		if (wishListByPK.isPresent())
		{
			return wishlistService.getWishlistEntriesCount(wishListByPK.get(), pageableData);
		}
		return 0;
	}

	@Override
	public void addEntryToDefaultWishlistByProductCode(final String productCode, final Integer desired,
			final WishlistEntryPriorityData priority, final String comment) throws WishlistException
	{
		//final Wishlist2Model defaultWishlist = wishlistService.getDefaultWishlist();
		final Optional<WishlistData> defaultWishList = getDefaultWishList();
		defaultWishList.ifPresent(e -> {
			try
			{
				addWishlistEntry(e.getPk(), productCode, desired, priority, comment);
			}
			catch (final WishlistException e1)
			{
				LOG.error(e1.getMessage());

			}
		});



	}

	@Override
	public void addAllDefaultWishlistEntryToCart() throws WishlistException, CommerceCartModificationException
	{
		final Optional<WishlistData> defaultWishList = getDefaultWishList();
		if (defaultWishList.isPresent())
		{
			addAllEntriesToCart(defaultWishList.get().getPk());
		}

	}

	@Override
	public void removeAllEntriesForDefaultWishlist() throws WishlistException
	{
		final Optional<WishlistData> defaultWishList = getDefaultWishList();
		if (defaultWishList.isPresent())
		{
			removeAllWishlistEntries(defaultWishList.get().getPk());
		}
	}

	@Override
	public List<CartModificationData> addAllEntriesToCartWithCartModification(final String wishlistPK)
			throws CommerceCartModificationException, WishlistException
	{


		final List<CartModificationData> list = new ArrayList<>();
		final Optional<Wishlist2Model> wishlistModel = wishlistService.getWishlistByPK(wishlistPK);
		if (wishlistModel.isEmpty())
		{
			return null;
		}

		final List<Wishlist2EntryModel> wishlistEntries = wishlistModel.get().getEntries();
		if (wishlistEntries == null || wishlistEntries.isEmpty())
		{
			return null;
		}


		wishlistEntries.forEach(entry -> {
			try
			{
				LOG.info(String.format("%s", entry.getProduct().getCode()));
				final CartModificationData addToCart = cartFacade.addToCart(entry.getProduct().getCode(), 1);

				list.add(addToCart);
			}
			catch (final CommerceCartModificationException e)
			{
				LOG.error(e);
			}
		});


		return list;
	}


}
