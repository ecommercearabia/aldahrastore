/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrawishlistfacade.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.aldahra.aldahrawishlistfacade.data.WishlistData;
import com.aldahra.aldahrawishlistfacade.data.WishlistEntryData;


/**
 * @author mohammad-abu muhasien
 */
public class WishlistPopulator implements Populator<Wishlist2Model, WishlistData>
{

	@Resource(name = "wishlistEntryConverter")
	private Converter<Wishlist2EntryModel, WishlistEntryData> wishlistEntryConverter;

	@Resource(name = "productService")
	private ProductService productService;

	private static final Logger LOG = Logger.getLogger(WishlistPopulator.class);

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void populate(final Wishlist2Model source, final WishlistData target)
	{
		target.setDefaultWishlist(source.getDefault());
		target.setDescription(source.getDescription());
		target.setName(source.getName());
		target.setPk(source.getPk().toString());

		List<WishlistEntryData> entries = null;
		if (source.getEntries() != null && !source.getEntries().isEmpty())
		{

			entries = new ArrayList<>();
			for (final Wishlist2EntryModel entryModel : source.getEntries())
			{
				try
				{
					productService.getProductForCode(entryModel.getProduct().getCode());
				}
				catch (final UnknownIdentifierException ex)
				{
					LOG.warn("product code [" + entryModel.getProduct().getCode() + "] not found!!!");
					continue;
				}
				entries.add(wishlistEntryConverter.convert(entryModel));
			}

			target.setWishlistEntries(entries);
		}
	}

}
