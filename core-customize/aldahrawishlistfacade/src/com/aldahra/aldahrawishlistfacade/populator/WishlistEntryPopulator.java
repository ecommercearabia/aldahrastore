/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrawishlistfacade.populator;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.wishlist2.enums.Wishlist2EntryPriority;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.aldahra.aldahrawishlistfacade.data.WishlistEntryData;
import com.aldahra.aldahrawishlistfacade.data.WishlistEntryPriorityData;


/**
 * @author mohammad-abu muhasien
 */

public class WishlistEntryPopulator implements Populator<Wishlist2EntryModel, WishlistEntryData>
{
	private static final List<ProductOption> OPTIONS = new ArrayList<>(Arrays.asList(ProductOption.VARIANT_MATRIX_BASE,
			ProductOption.VARIANT_MATRIX_URL, ProductOption.VARIANT_MATRIX_MEDIA, ProductOption.VARIANT_FIRST_VARIANT,
			ProductOption.BASIC, ProductOption.URL, ProductOption.PRICE, ProductOption.SUMMARY, ProductOption.DESCRIPTION,
			ProductOption.GALLERY, ProductOption.CATEGORIES, ProductOption.REVIEW, ProductOption.PROMOTIONS,
			ProductOption.CLASSIFICATION, ProductOption.VARIANT_FULL, ProductOption.STOCK, ProductOption.VOLUME_PRICES,
			ProductOption.PRICE_RANGE, ProductOption.DELIVERY_MODE_AVAILABILITY));

	@Resource(name = "wishlistEntryPriorityConverter")
	private Converter<Wishlist2EntryPriority, WishlistEntryPriorityData> wishlistEntryPriorityConverter;

	@Resource(name = "productConverter")
	private Converter<ProductModel, ProductData> productConverter;

	@Resource(name = "productVariantFacade")
	private ProductFacade productFacade;

	private static final Logger LOG = Logger.getLogger(WishlistEntryPopulator.class);

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void populate(final Wishlist2EntryModel source, final WishlistEntryData target)
	{
		if (source.getProduct() == null || !ArticleApprovalStatus.APPROVED.equals(source.getProduct().getApprovalStatus()))
		{
			return;
		}
		try
		{
			target.setProduct(productFacade.getProductForCodeAndOptions(source.getProduct().getCode(), OPTIONS));

		}
		catch (final UnknownIdentifierException ex)
		{
			LOG.warn("product code [" + source.getProduct().getCode() + "] not found");
			return;
		}

		target.setComment(source.getComment());
		target.setAddedDate(source.getAddedDate());
		target.setDesired(source.getDesired());
		target.setReceived(source.getReceived());
		if (source.getPriority() != null)
		{
			target.setPriority(wishlistEntryPriorityConverter.convert(source.getPriority()));
		}

		if (source.getWishlist() != null)
		{
			target.setWishlistPK(source.getWishlist().getPk().toString());
		}
	}

}
