/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrauserfacades.address.populator;

import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.AreaData;
import de.hybris.platform.commercefacades.user.data.CityData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.aldahra.aldahrauser.model.AreaModel;
import com.aldahra.aldahrauser.model.CityModel;


/**
 * @author mnasro
 *
 *         Converter implementation for {@link de.hybris.platform.core.model.user.AddressModel} as source and
 *         {@link de.hybris.platform.commercefacades.user.data.AddressData} as target type.
 */
public class AddressPopulator implements Populator<AddressModel, AddressData>
{

	/** The city converter. */
	@Resource(name = "cityConverter")
	private Converter<CityModel, CityData> cityConverter;

	/** The country converter. */
	@Resource(name = "countryConverter")
	private Converter<CountryModel, CountryData> countryConverter;

	/** The area converter. */
	@Resource(name = "areaConverter")
	private Converter<AreaModel, AreaData> areaConverter;




	/**
	 * Fill the source to target.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	@Override
	public void populate(final AddressModel source, final AddressData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		target.setCity(source.getCity() == null ? null : getCityConverter().convert(source.getCity()));
		target.setMobileCountry(
				source.getMobileCountry() == null ? null : getCountryConverter().convert(source.getMobileCountry()));
		target.setMobileNumber(source.getMobile());
		target.setTown(target.getTown() == null ? null : target.getTown());
		target.setPostalCode(target.getPostalCode());
		target.setArea(source.getArea() == null ? null : getAreaConverter().convert(source.getArea()));
		target.setLatitude(source.getLatitude());
		target.setLongitude(source.getLongitude());
		target.setNearestLandmark(source.getNearestLandmark());
		target.setAddressName(source.getAddressName());
		target.setTitleCode(source.getTitle() == null ? null : source.getTitle().getCode());
		target.setTitle(source.getTitle() == null ? null : source.getTitle().getName());
		target.setBuildingName(source.getBuildingName());
		target.setStreetName(source.getCustomStreetName());
		target.setApartmentNumber(source.getApartmentNumber());
	}



	/**
	 * Gets the city converter.
	 *
	 * @return the city converter
	 */
	protected Converter<CityModel, CityData> getCityConverter()
	{
		return cityConverter;
	}

	/**
	 * Gets the country converter.
	 *
	 * @return the country converter
	 */
	protected Converter<CountryModel, CountryData> getCountryConverter()
	{
		return countryConverter;
	}


	/**
	 * @return the areaConverter
	 */
	public Converter<AreaModel, AreaData> getAreaConverter()
	{
		return areaConverter;
	}




}
