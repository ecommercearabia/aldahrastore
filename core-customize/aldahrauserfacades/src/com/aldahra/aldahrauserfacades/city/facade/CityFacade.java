/**
 *
 */
package com.aldahra.aldahrauserfacades.city.facade;

import de.hybris.platform.commercefacades.user.data.CityData;

import java.util.List;
import java.util.Optional;


/**
 * The Interface CityFacade.
 *
 * @author mnasro
 */
public interface CityFacade
{

	/**
	 * Gets the by country isocode.
	 *
	 * @param isoCode
	 *           the iso code
	 * @return the by country isocode
	 */
	public Optional<List<CityData>> getByCountryIsocode(final String isoCode);


	/**
	 * Gets the all city
	 *
	 * @return the all city
	 */
	public Optional<List<CityData>> getAll();


	/**
	 * Gets the city
	 *
	 * @param code
	 *           the code
	 * @return the city data
	 */
	public Optional<CityData> get(final String code);
}
