/**
 *
 */
package com.aldahra.aldahrauserfacades.city.facade.impl;

import de.hybris.platform.commercefacades.user.data.CityData;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.aldahra.aldahrauser.city.service.CityService;
import com.aldahra.aldahrauser.model.CityModel;
import com.aldahra.aldahrauserfacades.city.facade.CityFacade;


/**
 * The Class DefaultCityFacade.
 *
 * @author @mnasro
 */
public class DefaultCityFacade implements CityFacade
{

	/** The city service. */
	@Resource(name = "cityService")
	private CityService cityService;

	/** The city converter. */
	@Resource(name = "cityConverter")
	private Converter<CityModel, CityData> cityConverter;


	/**
	 * Gets the by country isocode.
	 *
	 * @param isoCode
	 *           the iso code
	 * @return the by country isocode
	 */
	@Override
	public Optional<List<CityData>> getByCountryIsocode(final String isoCode)
	{
		ServicesUtil.validateParameterNotNull(isoCode, "isoCode must not be null");

		final Optional<List<CityModel>> cityList = cityService.getByCountryIsocode(isoCode);
		if (cityList.isPresent())
		{
			final List<CityData> cityListData = cityConverter.convertAll(cityList.get()).stream()
					.sorted((x, y) -> x.getName().compareTo(y.getName())).collect(Collectors.toList());
			return Optional.ofNullable(cityListData);
		}
		return Optional.empty();
	}


	/**
	 * Gets the all CityData
	 *
	 * @return the all
	 */
	@Override
	public Optional<List<CityData>> getAll()
	{
		final Optional<List<CityModel>> cityList = cityService.getAll();

		return cityList.isPresent() ? Optional.ofNullable(cityConverter.convertAll(cityList.get())) : Optional.empty();
	}


	/**
	 * Gets the CityData
	 *
	 * @param code
	 *           the code
	 * @return the city data
	 */
	@Override
	public Optional<CityData> get(final String code)
	{
		ServicesUtil.validateParameterNotNull(code, "code must not be null");

		final Optional<CityModel> city = cityService.get(code);

		return city.isPresent() ? Optional.ofNullable(cityConverter.convert(city.get())) : Optional.empty();
	}

}
