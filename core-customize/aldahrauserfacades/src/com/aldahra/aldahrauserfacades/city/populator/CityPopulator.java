package com.aldahra.aldahrauserfacades.city.populator;

import de.hybris.platform.commercefacades.user.data.AreaData;
import de.hybris.platform.commercefacades.user.data.CityData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import com.aldahra.aldahrauser.model.AreaModel;
import com.aldahra.aldahrauser.model.CityModel;


/**
 * The Class CityPopulator.
 *
 * @author mnasro
 */
public class CityPopulator implements Populator<CityModel, CityData>
{

	/** The area converter. */
	@Resource(name = "areaConverter")
	private Converter<AreaModel, AreaData> areaConverter;


	/**
	 * Fill the source to target.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 * @throws ConversionException
	 *            the conversion exception
	 */
	@Override
	public void populate(final CityModel source, final CityData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		target.setCode(source.getCode());
		target.setName(source.getName());

		if (!CollectionUtils.isEmpty(source.getAreas()))
		{
			target.setAreas(getAreaConverter().convertAll(source.getAreas()));
		}
		target.setExpress(source.isExpress());
	}

	protected Converter<AreaModel, AreaData> getAreaConverter()
	{
		return areaConverter;
	}
}
