/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrauserfacades.user.facade.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commercefacades.order.data.NoCardPaymentInfoData;
import de.hybris.platform.commercefacades.user.impl.DefaultUserFacade;
import de.hybris.platform.core.model.order.payment.NoCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import com.aldahra.aldahrapayment.customer.service.CustomCustomerAccountService;
import com.aldahra.aldahrauserfacades.user.facade.CustomUserFacade;


/**
 * The Class DefaultUserFacade.
 *
 * @author mnasro
 */
public class DefaultCustomUserFacade extends DefaultUserFacade implements CustomUserFacade
{

	/** The no card payment info converter. */
	@Resource(name = "noCardPaymentInfoConverter")
	private Converter<NoCardPaymentInfoModel, NoCardPaymentInfoData> noCardPaymentInfoConverter;

	/** The customer account service. */
	@Resource(name = "customerAccountService")
	private CustomCustomerAccountService customerAccountService;

	/**
	 * Gets the no card payment infos.
	 *
	 * @param saved
	 *           the saved
	 * @return the no card payment infos
	 */
	@Override
	public Optional<List<NoCardPaymentInfoData>> getNoCardPaymentInfos(final boolean saved)
	{
		final CustomerModel currentCustomer = getCurrentUserForCheckout();
		final Optional<List<NoCardPaymentInfoModel>> noCardPaymentInfoModels = getCustomerAccountService()
				.getNoCardPaymentInfos(currentCustomer, saved);

		if (noCardPaymentInfoModels.isPresent())
		{
			final List<NoCardPaymentInfoData> noCardPaymentInfos = new ArrayList<>();
			final PaymentInfoModel defaultPaymentInfoModel = currentCustomer.getDefaultPaymentInfo();
			for (final NoCardPaymentInfoModel noCardPaymentInfoModel : noCardPaymentInfoModels.get())
			{
				final NoCardPaymentInfoData paymentInfoData = getNoCardPaymentInfoConverter().convert(noCardPaymentInfoModel);
				if (noCardPaymentInfoModel.equals(defaultPaymentInfoModel))
				{
					paymentInfoData.setDefaultPaymentInfo(true);
					noCardPaymentInfos.add(0, paymentInfoData);
				}
				else
				{
					noCardPaymentInfos.add(paymentInfoData);
				}
			}
		}
		return Optional.empty();
	}

	/**
	 * Gets the no card payment info converter.
	 *
	 * @return the noCardPaymentInfoConverter
	 */
	public Converter<NoCardPaymentInfoModel, NoCardPaymentInfoData> getNoCardPaymentInfoConverter()
	{
		return noCardPaymentInfoConverter;
	}


	/**
	 * Gets the customer account service.
	 *
	 * @return the customerAccountService
	 */
	@Override
	public CustomCustomerAccountService getCustomerAccountService()
	{
		return customerAccountService;
	}

	/**
	 * Sets the default payment info.
	 *
	 * @param paymentInfo
	 *           the new default payment info
	 */
	@Override
	public void setDefaultPaymentInfo(final NoCardPaymentInfoData paymentInfo)
	{
		validateParameterNotNullStandardMessage("paymentInfoData", paymentInfo);
		final CustomerModel currentCustomer = getCurrentUserForCheckout();
		final Optional<NoCardPaymentInfoModel> noCardPaymentInfoModel = getCustomerAccountService()
				.getNoCardPaymentInfoForCode(currentCustomer, paymentInfo.getId());
		if (noCardPaymentInfoModel.isPresent())
		{
			getCustomerAccountService().setDefaultPaymentInfo(currentCustomer, noCardPaymentInfoModel.get());
		}
	}

}
