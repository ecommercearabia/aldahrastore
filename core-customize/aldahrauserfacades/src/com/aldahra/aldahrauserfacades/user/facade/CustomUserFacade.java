/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrauserfacades.user.facade;

import de.hybris.platform.commercefacades.order.data.NoCardPaymentInfoData;

import java.util.List;
import java.util.Optional;


/**
 * @author mnasro
 *
 *         The Interface UserFacade.
 */
public interface CustomUserFacade extends de.hybris.platform.commercefacades.user.UserFacade
{

	/**
	 * Gets the no card payment infos.
	 *
	 * @param saved
	 *           the saved
	 * @return the no card payment infos
	 */
	public Optional<List<NoCardPaymentInfoData>> getNoCardPaymentInfos(final boolean saved);

	/**
	 * Sets the default payment info.
	 *
	 * @param paymentInfo
	 *           the new default payment info
	 */
	void setDefaultPaymentInfo(NoCardPaymentInfoData paymentInfo);
}
