/**
 *
 */
package com.aldahra.aldahrauserfacades.area.facade.impl;

import de.hybris.platform.commercefacades.user.data.AreaData;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import com.aldahra.aldahrauser.area.service.AreaService;
import com.aldahra.aldahrauser.model.AreaModel;
import com.aldahra.aldahrauserfacades.area.facade.AreaFacade;



/**
 * The Class DefaultAreaFacade.
 *
 * @author mnasro
 */
public class DefaultAreaFacade implements AreaFacade
{

	/** The area service. */
	@Resource(name = "areaService")
	private AreaService areaService;


	/** The area converter. */
	@Resource(name = "areaConverter")
	private Converter<AreaModel, AreaData> areaConverter;



	/**
	 * Gets the by city code.
	 *
	 * @param cityCode
	 *           the city code
	 * @return the by city code
	 */
	@Override
	public Optional<List<AreaData>> getByCityCode(final String cityCode)
	{
		final Optional<List<AreaModel>> areas = getAreaService().getByCityCode(cityCode);
		if (areas.isPresent())
		{
			final List<AreaData> areaList = getAreaConverter().convertAll(areas.get());

			Collections.sort(areaList, new Comparator<AreaData>()
			{
				public int compare(final AreaData a1, final AreaData a2)
				{

					final String areaName1 = a1.getName() != null ? a1.getName() : "";
					final String areaName2 = a2.getName() != null ? a2.getName() : "";

					return areaName1.compareTo(areaName2);

				}
			});


			return Optional.ofNullable(areaList);
		}
		return Optional.empty();
	}

	/**
	 * Gets the all.
	 *
	 * @return the all
	 */
	@Override
	public Optional<List<AreaData>> getAll()
	{
		final Optional<List<AreaModel>> areas = getAreaService().getAll();
		if (areas.isPresent())
		{
			final List<AreaData> areasToSort = getAreaConverter().convertAll(areas.get());
			Collections.sort(areasToSort, new Comparator<AreaData>()
			{

				public int compare(final AreaData a1, final AreaData a2)
				{
					final String areaName1 = a1.getName() != null ? a1.getName() : "";
					final String areaName2 = a2.getName() != null ? a2.getName() : "";
					return areaName1.compareToIgnoreCase(areaName2);
				}
			});
			return Optional.ofNullable(areasToSort);
		}
		return Optional.empty();
	}

	/**
	 * Gets the.
	 *
	 * @param code
	 *           the code
	 * @return the optional
	 */
	@Override
	public Optional<AreaData> get(final String code)
	{

		final Optional<AreaModel> area = getAreaService().get(code);
		if (area.isPresent())
		{
			return Optional.of(getAreaConverter().convert(area.get()));
		}
		return Optional.empty();
	}

	protected AreaService getAreaService()
	{
		return areaService;
	}

	protected Converter<AreaModel, AreaData> getAreaConverter()
	{
		return areaConverter;
	}
}
