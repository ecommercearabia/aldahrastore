package com.aldahra.aldahrauserfacades.area.facade;

import de.hybris.platform.commercefacades.user.data.AreaData;

import java.util.List;
import java.util.Optional;


/**
 * The Interface AreaFacade.
 *
 * @author mnasro
 */
public interface AreaFacade
{

	/**
	 * Gets the by city code.
	 *
	 * @param cityCode
	 *           the city code
	 * @return the by city code
	 */
	public Optional<List<AreaData>> getByCityCode(final String cityCode);

	/**
	 * Gets the all.
	 *
	 * @return the all
	 */
	public Optional<List<AreaData>> getAll();

	/**
	 * Gets the.
	 *
	 * @param code
	 *           the code
	 * @return the optional
	 */
	public Optional<AreaData> get(final String code);

}
