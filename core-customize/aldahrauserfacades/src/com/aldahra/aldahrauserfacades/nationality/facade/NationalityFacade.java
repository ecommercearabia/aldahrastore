/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrauserfacades.nationality.facade;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commercefacades.user.data.NationalityData;

import java.util.List;
import java.util.Optional;


/**
 * @author Tuqa
 */
public interface NationalityFacade
{

	public List<NationalityData> getAll();

	public List<NationalityData> getBySite(CMSSiteModel cmsSiteModel);

	public List<NationalityData> getByCurrentSite();

	public Optional<NationalityData> get(final String code);
}
