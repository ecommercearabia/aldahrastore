/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrauserfacades.nationality.facade.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commercefacades.user.data.NationalityData;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;

import com.aldahra.aldahrauser.model.NationalityModel;
import com.aldahra.aldahrauser.service.NationalityService;
import com.aldahra.aldahrauserfacades.nationality.facade.NationalityFacade;


/**
 * @author Tuqa
 */
public class DefaultNationalityFacade implements NationalityFacade
{
	@Resource(name = "nationalityService")
	private NationalityService nationalityService;

	@Resource(name = "nationalityConverter")
	private Converter<NationalityModel, NationalityData> nationalityConverter;


	/**
	 * @return the nationalityService
	 */
	public NationalityService getNationalityService()
	{
		return nationalityService;
	}

	/**
	 * @return the nationalityConverter
	 */
	public Converter<NationalityModel, NationalityData> getNationalityConverter()
	{
		return nationalityConverter;
	}

	@Override
	public List<NationalityData> getAll()
	{
		final List<NationalityModel> nationalitys = getNationalityService().getAll();
		return CollectionUtils.isEmpty(nationalitys) ? Collections.emptyList() : getNationalityConverter().convertAll(nationalitys);

	}

	@Override
	public List<NationalityData> getBySite(final CMSSiteModel cmsSiteModel)
	{

		final List<NationalityModel> nationalitys = getNationalityService().getBySite(cmsSiteModel);
		return CollectionUtils.isEmpty(nationalitys) ? Collections.emptyList() : getNationalityConverter().convertAll(nationalitys);
	}

	@Override
	public List<NationalityData> getByCurrentSite()
	{
		final List<NationalityModel> nationalitys = getNationalityService().getByCurrentSite();
		if (!CollectionUtils.isEmpty(nationalitys))
		{
			final List<NationalityData> list = getNationalityConverter().convertAll(nationalitys);
			Collections.sort(list, new Comparator<NationalityData>()
			{
				@Override
				public int compare(final NationalityData nationalityData1, final NationalityData nationalityData2)
				{
					return nationalityData1.getName().compareTo(nationalityData2.getName());
				}
			});
			return list;
		}
		return Collections.emptyList();
	}

	@Override
	public Optional<NationalityData> get(final String code)
	{
		final Optional<NationalityModel> nationality = getNationalityService().get(code);
		if (nationality.isPresent())
		{
			return Optional.of(getNationalityConverter().convert(nationality.get()));
		}
		return Optional.empty();
	}

}
