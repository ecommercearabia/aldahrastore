/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrauserfacades.nationality.populator;

import de.hybris.platform.commercefacades.user.data.NationalityData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.util.Assert;

import com.aldahra.aldahrauser.model.NationalityModel;


/**
 * @author Tuqa
 */
public class NationalityPopulator implements Populator<NationalityModel, NationalityData>
{

	@Override
	public void populate(final NationalityModel source, final NationalityData target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		target.setCode(source.getCode());
		target.setName(source.getName());

	}

}
