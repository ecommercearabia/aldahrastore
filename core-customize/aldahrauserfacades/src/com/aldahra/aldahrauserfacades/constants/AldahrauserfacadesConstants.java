/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrauserfacades.constants;

/**
 * Global class for all Aldahrauserfacades constants. You can add global constants for your extension into this class.
 */
public final class AldahrauserfacadesConstants extends GeneratedAldahrauserfacadesConstants
{
	public static final String EXTENSIONNAME = "aldahrauserfacades";

	private AldahrauserfacadesConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "aldahrauserfacadesPlatformLogo";
}
