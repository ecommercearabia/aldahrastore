/**
 *
 */
package com.aldahra.aldahrauserfacades.country.facade;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commercefacades.user.data.CountryData;

import java.util.List;
import java.util.Optional;


/**
 * The Interface CountryFacade.
 *
 * @author mnasro
 */
public interface CountryFacade
{

	/**
	 * Gets the mobile countries by curunt site.
	 *
	 * @return the mobile countries by curunt site
	 */
	Optional<List<CountryData>> getMobileCountriesByCuruntSite();

	/**
	 * Gets the mobile countries.
	 *
	 * @param cmsSiteModel
	 *           the cms site model
	 * @return the mobile countries
	 */
	Optional<List<CountryData>> getMobileCountries(CMSSiteModel cmsSiteModel);

	/**
	 * Gets the all countries.
	 *
	 * @return the all countries
	 */
	Optional<List<CountryData>> getAllCountries();

	/**
	 * Gets the country.
	 *
	 * @param isocode
	 *           the isocode
	 * @return the country
	 */
	Optional<CountryData> getCountry(String isocode);
}
