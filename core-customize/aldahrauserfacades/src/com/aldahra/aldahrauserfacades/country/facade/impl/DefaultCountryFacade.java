/**
 *
 */
package com.aldahra.aldahrauserfacades.country.facade.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.aldahra.aldahrauserfacades.country.facade.CountryFacade;


/**
 * The Class DefaultCountryFacade.
 *
 * @author mnasro
 */
public class DefaultCountryFacade implements CountryFacade
{

	/** The country converter. */
	@Resource(name = "countryConverter")
	private Converter<CountryModel, CountryData> countryConverter;

	/** The common I 18 N service. */
	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	/** The cms site service. */
	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	/**
	 * Gets the mobile countries by curunt site.
	 *
	 * @return the mobile countries by curunt site
	 */
	@Override
	public Optional<List<CountryData>> getMobileCountriesByCuruntSite()
	{
		final Optional<List<CountryData>> mobileCountries = getMobileCountries(getCmsSiteService().getCurrentSite());
		if (mobileCountries.isPresent())
		{
			return Optional.ofNullable(
					mobileCountries.get().stream().sorted((x, y) -> x.getName().compareTo(y.getName())).collect(Collectors.toList()));
		}
		return Optional.empty();
	}

	/**
	 * Gets the mobile countries.
	 *
	 * @param cmsSiteModel
	 *           the cms site model
	 * @return the mobile countries
	 */
	@Override
	public Optional<List<CountryData>> getMobileCountries(final CMSSiteModel cmsSiteModel)
	{
		ServicesUtil.validateParameterNotNull(cmsSiteModel, "cmsSiteModel must not be null");
		final List<CountryModel> mobileCountries = getCmsSiteService().getCurrentSite().getMobileCountries();

		if (mobileCountries == null || mobileCountries.isEmpty())
		{
			return Optional.empty();
		}
		return Optional.ofNullable(getCountryConverter().convertAll(mobileCountries).stream()
				.sorted((x, y) -> x.getName().compareTo(y.getName())).collect(Collectors.toList()));
	}

	/**
	 * Gets the all countries.
	 *
	 * @return the all countries
	 */
	@Override
	public Optional<List<CountryData>> getAllCountries()
	{
		final List<CountryModel> allCountries = getCommonI18NService().getAllCountries();
		if (allCountries == null || allCountries.isEmpty())
		{
			return Optional.empty();
		}
		return Optional.ofNullable(getCountryConverter().convertAll(allCountries).stream()
				.sorted((x, y) -> x.getName().compareTo(y.getName())).collect(Collectors.toList()));
	}

	/**
	 * Gets the country.
	 *
	 * @param isocode
	 *           the isocode
	 * @return the country
	 */
	@Override
	public Optional<CountryData> getCountry(final String isocode)
	{
		ServicesUtil.validateParameterNotNull(isocode, "isocode must not be null");
		final CountryModel country = getCommonI18NService().getCountry(isocode);
		if (country == null)
		{
			return null;
		}
		return country == null ? Optional.empty() : Optional.ofNullable(getCountryConverter().convert(country));
	}


	/**
	 * Gets the country converter.
	 *
	 * @return the country converter
	 */
	protected Converter<CountryModel, CountryData> getCountryConverter()
	{
		return countryConverter;
	}


	/**
	 * Gets the cms site service.
	 *
	 * @return the cms site service
	 */
	protected CMSSiteService getCmsSiteService()
	{
		return cmsSiteService;
	}


	/**
	 * Gets the common I 18 N service.
	 *
	 * @return the common I 18 N service
	 */
	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}


}
