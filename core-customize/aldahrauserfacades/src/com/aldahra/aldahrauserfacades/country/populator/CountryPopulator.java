/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrauserfacades.country.populator;

import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;

import org.springframework.util.Assert;


/**
 * @author ashati
 */
public class CountryPopulator implements Populator<CountryModel, CountryData>
{

	@Override
	public void populate(final CountryModel source, final CountryData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setIsdcode(source.getIsdcode());
	}

}
