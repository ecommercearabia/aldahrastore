/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrauserfacades.customer.facade;

import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.MobileTokenData;

import java.util.List;
import java.util.Optional;


/**
 * @author monzer
 */
public interface CustomCustomerFacade extends CustomerFacade
{
	public void updateSignatureIdCurrentCustomer(String signatureId);

	public boolean isValidMobileNumber(String mobileNumber);

	/**
	 * Gets Match the identifier if it is email or mobile number.
	 *
	 * @param identifier
	 *           the identifier
	 * @return the CustomerData
	 */
	public Optional<CustomerData> getCustomerByIdentifier(String identifier);

	public boolean isMobileTokenValid(String mobileToken);

	public void updateCustomerMobileToken(String mobileToken);

	public List<MobileTokenData> getCustomerMobileTokens();

}
