/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrauserfacades.customer.facade.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.customer.impl.DefaultCustomerFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.MobileTokenData;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.event.UpdatedProfileEvent;
import de.hybris.platform.commerceservices.model.consent.ConsentTemplateModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import com.aldahra.aldahrareferralcode.exception.ReferralCodeException;
import com.aldahra.aldahrareferralcode.service.ReferralCodeService;
import com.aldahra.aldahrauser.model.MobileTokenModel;
import com.aldahra.aldahrauser.model.NationalityModel;
import com.aldahra.aldahrauser.service.CustomerMobileTokenService;
import com.aldahra.aldahrauser.service.MobilePhoneService;
import com.aldahra.aldahrauser.service.NationalityService;
import com.aldahra.aldahrauser.service.SingatureIdEncoderService;
import com.aldahra.aldahrauserfacades.customer.facade.CustomCustomerFacade;
import com.aldahra.core.service.CustomerMatchingService;
import com.aldahra.facades.facade.CustomConsentFacade;


/**
 * @author mnasro
 *
 *         Default implementation for the {@link CustomerFacade}.
 */
public class DefaultCustomCustomerFacade extends DefaultCustomerFacade implements CustomCustomerFacade
{
	private static final Logger LOG = Logger.getLogger(DefaultCustomCustomerFacade.class);
	public static final String REGISTRATION_CONSENT_ID = "registration.consent.id.";

	/** The customer converter. */
	@Resource(name = "customerConverter")
	private Converter<UserModel, CustomerData> customerConverter;

	/** The customer reverse converter. */
	@Resource(name = "customerReverseConverter")
	private Converter<CustomerData, CustomerModel> customerReverseConverter;


	@Resource(name = "mobileTokenConverter")
	private Converter<MobileTokenModel, MobileTokenData> mobileTokenConverter;


	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;

	@Resource(name = "referralCodeService")
	private ReferralCodeService referralCodeService;

	@Resource(name = "singatureIdEncoderService")
	private SingatureIdEncoderService singatureIdEncoderService;

	@Resource(name = "eventService")
	private EventService eventService;

	/** The user service. */
	@Resource(name = "userService")
	private UserService userService;



	@Resource(name = "consentFacade")
	private CustomConsentFacade consentFacade;

	@Resource(name = "siteConfigService")
	private SiteConfigService siteConfigService;


	@Resource(name = "customerMatchingService")
	private CustomerMatchingService customerMatchingService;

	@Resource(name = "customerMobileTokenService")
	private CustomerMobileTokenService customerMobileTokenService;

	@Resource(name = "nationalityService")
	private NationalityService nationalityService;



	/**
	 * @return the siteConfigService
	 */
	public SiteConfigService getSiteConfigService()
	{
		return siteConfigService;
	}

	/**
	 * @return the consentFacade
	 */
	private CustomConsentFacade getCustomConsentFacade()
	{
		return consentFacade;
	}

	/**
	 * Sets the common properties for register.
	 *
	 * @param registerData
	 *           the register data
	 * @param customerModel
	 *           the customer model
	 */
	@Override
	protected void setCommonPropertiesForRegister(final RegisterData registerData, final CustomerModel customerModel)
	{
		customerModel.setName(getCustomerNameStrategy().getName(registerData.getFirstName(), registerData.getLastName()));
		setTitleForRegister(registerData, customerModel);
		setUidForRegister(registerData, customerModel);
		setMobileNumberForRegister(registerData, customerModel);
		customerModel.setSessionLanguage(getCommonI18NService().getCurrentLanguage());
		customerModel.setSessionCurrency(getCommonI18NService().getCurrentCurrency());
		customerModel.setUid(customerModel.getUid());
		customerModel.setNationalityID(registerData.getNationalityId());
		if (StringUtils.isNotBlank(registerData.getNationality()))
		{
			final Optional<NationalityModel> nationalityModel = nationalityService.get(registerData.getNationality());
			if (nationalityModel.isPresent())
			{
				customerModel.setNationality(nationalityModel.get());

			}
		}
	}

	@Override
	public void register(final RegisterData registerData) throws DuplicateUidException
	{
		validateParameterNotNullStandardMessage("registerData", registerData);
		Assert.hasText(registerData.getFirstName(), "The field [FirstName] cannot be empty");
		Assert.hasText(registerData.getLastName(), "The field [LastName] cannot be empty");
		Assert.hasText(registerData.getLogin(), "The field [Login] cannot be empty");

		final CustomerModel newCustomer = getModelService().create(CustomerModel.class);
		setCommonPropertiesForRegister(registerData, newCustomer);
		getCustomerAccountService().register(newCustomer, registerData.getPassword());
		getModelService().refresh(newCustomer);

		if (StringUtils.isNotBlank(registerData.getReferralCode()))
		{
			applyReferralCode(registerData.getReferralCode(), newCustomer);
		}
		autoAppliedConsentTemplates(newCustomer);
	}

	/**
	 *
	 */
	protected void autoAppliedConsentTemplates(final CustomerModel customerModel)
	{
		final Set<ConsentTemplateModel> autoAppliedConsentTemplates = getAutoAppliedConsentTemplates();
		if (customerModel == null || CollectionUtils.isEmpty(autoAppliedConsentTemplates))
		{
			return;
		}
		for (final ConsentTemplateModel consentTemplateModel : autoAppliedConsentTemplates)
		{
			try
			{
				getCustomConsentFacade().giveConsent(consentTemplateModel.getId(), consentTemplateModel.getVersion(), customerModel);
			}
			catch (final Exception e)
			{
				LOG.error("DefaultCustomCustomerFacade:" + autoAppliedConsentTemplates + " faild ->  Id["
						+ consentTemplateModel.getId() + "]:" + e.getMessage());
			}
		}
	}

	protected Set<ConsentTemplateModel> getAutoAppliedConsentTemplates()
	{
		final BaseSiteModel currentBaseSite = getBaseSiteService().getCurrentBaseSite();

		if (currentBaseSite == null || CollectionUtils.isEmpty(currentBaseSite.getAutoAppliedConsentTemplates()))
		{
			return Collections.EMPTY_SET;
		}
		return currentBaseSite.getAutoAppliedConsentTemplates();
	}

	/**
	 * Update profile.
	 *
	 * @param customerData
	 *           the customer data
	 * @throws DuplicateUidException
	 *            the duplicate uid exception
	 */
	@Override
	public void updateProfile(final CustomerData customerData) throws DuplicateUidException
	{
		final String name = getCustomerNameStrategy().getName(customerData.getFirstName(), customerData.getLastName());
		final CustomerModel customer = getCurrentSessionCustomer();
		customer.setOriginalUid(customerData.getDisplayUid());
		customerReverseConverter.convert(customerData, customer);
		getCustomerAccountService().updateProfile(customer, customerData.getTitleCode(), name, customerData.getUid());
	}

	/**
	 * Sets the mobile number for register.
	 *
	 * @param registerData
	 *           the register data
	 * @param customerModel
	 *           the customer model
	 */
	protected void setMobileNumberForRegister(final RegisterData registerData, final CustomerModel customerModel)
	{
		if (StringUtils.isNotBlank(registerData.getMobileCountry()) && StringUtils.isNotBlank(registerData.getMobileNumber()))
		{
			final Optional<String> normalizedPhoneNumber = mobilePhoneService
					.validateAndNormalizePhoneNumberByIsoCode(registerData.getMobileCountry(), registerData.getMobileNumber());

			if (normalizedPhoneNumber.isPresent())
			{
				customerModel.setMobileNumber(normalizedPhoneNumber.get());
			}
			else
			{
				customerModel.setMobileNumber(registerData.getMobileNumber());
			}

			customerModel.setMobileCountry(getCommonI18NService().getCountry(registerData.getMobileCountry()));
		}
	}

	@Override
	public void updateFullProfile(final CustomerData customerData) throws DuplicateUidException
	{
		validateDataBeforeUpdate(customerData);

		final CustomerModel customer = getCurrentSessionCustomer();
		customerReverseConverter.convert(customerData, customer);
		if (customer.getDefaultPaymentAddress() != null)
		{
			getModelService().save(customer.getDefaultPaymentAddress());
		}

		if (customer.getDefaultShipmentAddress() != null)
		{
			getModelService().save(customer.getDefaultShipmentAddress());
		}

		getModelService().save(customer);
		getEventService().publishEvent(initializeCommerceEvent(new UpdatedProfileEvent(), customer));
	}

	/**
	 * Gets the current customer.
	 *
	 * @return the current customer
	 */
	@Override
	public CustomerData getCurrentCustomer()
	{
		return customerConverter.convert(getCurrentUser());
	}

	@Override
	public void updateSignatureIdCurrentCustomer(final String signatureId)
	{
		final UserModel currentUser = getCurrentUser();
		if (currentUser == null || !(currentUser instanceof CustomerModel))
		{
			return;
		}
		final CustomerModel customer = (CustomerModel) currentUser;
		singatureIdEncoderService.setSignatureIdForCustomer(customer, signatureId);
		getModelService().save(customer);
	}

	/**
	 * @return the customerConverter
	 */
	@Override
	protected Converter<UserModel, CustomerData> getCustomerConverter()
	{
		return customerConverter;
	}

	/**
	 * @return the customerReverseConverter
	 */
	protected Converter<CustomerData, CustomerModel> getCustomerReverseConverter()
	{
		return customerReverseConverter;
	}

	/**
	 * @return the mobilePhoneService
	 */
	protected MobilePhoneService getMobilePhoneService()
	{
		return mobilePhoneService;
	}

	/**
	 * @return the referralCodeService
	 */
	protected ReferralCodeService getReferralCodeService()
	{
		return referralCodeService;
	}

	/**
	 * @return the singatureIdEncoderService
	 */
	protected SingatureIdEncoderService getSingatureIdEncoderService()
	{
		return singatureIdEncoderService;
	}

	/**
	 * @return the eventService
	 */
	@Override
	public EventService getEventService()
	{
		return eventService;
	}


	public void applyReferralCode(final String referralCode, final CustomerModel customer)
	{
		try
		{
			referralCodeService.applyCustomerByCurrentBaseStore(referralCode, customer);
		}
		catch (final ReferralCodeException e)
		{
			LOG.error(e.getMessage(), e);
		}
	}

	/**
	 *
	 */
	@Override
	public boolean isValidMobileNumber(final String mobileNumber)
	{
		return mobilePhoneService.isMobileNumberValidByCurrentCustomer(mobileNumber);
	}

	@Override
	public Optional<CustomerData> getCustomerByIdentifier(final String identifier)
	{
		CustomerData customer = null;
		final Optional<String> customerUid = customerMatchingService.getCustomerUidAfterMatching(identifier);
		if (customerUid.isEmpty())
		{
			LOG.error("No users found for mobile number {" + identifier + "}");
			return Optional.empty();
		}
		try
		{
			customer = getUserForUID(customerUid.get());
		}
		catch (final Exception e)
		{
			LOG.error(e.getMessage());
			return Optional.empty();
		}
		return Optional.ofNullable(customer);
	}

	@Override
	public boolean isMobileTokenValid(final String mobileToken)
	{
		return getCustomerMobileTokenService().isValidMobileToken(mobileToken);
	}

	@Override
	public void updateCustomerMobileToken(final String mobileToken)
	{
		getCustomerMobileTokenService().updateCurrentCustomerMobileToken(mobileToken);
	}

	/**
	 * @return the customerMobileTokenService
	 */
	public CustomerMobileTokenService getCustomerMobileTokenService()
	{
		return customerMobileTokenService;
	}

	@Override
	public List<MobileTokenData> getCustomerMobileTokens()
	{
		return getMobileTokenConverter().convertAll(getCustomerMobileTokenService().getCurrentCustomerMobileTokens());
	}

	/**
	 * @return the mobileTokenConverter
	 */
	protected Converter<MobileTokenModel, MobileTokenData> getMobileTokenConverter()
	{
		return mobileTokenConverter;
	}

}
