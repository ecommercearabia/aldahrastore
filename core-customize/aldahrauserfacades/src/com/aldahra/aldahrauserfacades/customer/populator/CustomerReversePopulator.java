package com.aldahra.aldahrauserfacades.customer.populator;

import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

import com.aldahra.aldahrauser.model.NationalityModel;
import com.aldahra.aldahrauser.service.MobilePhoneService;
import com.aldahra.aldahrauser.service.NationalityService;


/**
 * The Class CustomerReversePopulator.
 *
 * @author mnasro
 */
public class CustomerReversePopulator implements Populator<CustomerData, CustomerModel>
{
	/** The common I 18 N service. */
	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;

	@Resource(name = "nationalityService")
	private NationalityService nationalityService;


	/**
	 * Fill the source to target.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 * @throws ConversionException
	 *            the conversion exception
	 */
	@Override
	public void populate(final CustomerData source, final CustomerModel target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		target.setNationalityID(source.getNationalityID());
		populateNationality(source, target);
		populateMobileNumber(source, target);
	}

	/**
	 *
	 */
	private void populateNationality(final CustomerData source, final CustomerModel target)
	{
		if (source.getNationality() != null && StringUtils.isNotBlank(source.getNationality().getCode()))
		{
			final Optional<NationalityModel> nationality = getNationalityService().get(source.getNationality().getCode());
			if (nationality.isPresent())
			{
				target.setNationality(nationality.get());
			}
		}

	}

	protected void populateMobileNumber(final CustomerData customerData, final CustomerModel customerModel)
	{
		customerModel.setMobileNumber(customerModel.getMobileNumber());

		if (customerData.getMobileCountry() != null && StringUtils.isNotBlank(customerData.getMobileCountry().getIsocode())
				&& StringUtils.isNotBlank(customerData.getMobileNumber()))
		{
			final Optional<String> normalizedPhoneNumber = mobilePhoneService.validateAndNormalizePhoneNumberByIsoCode(
					customerData.getMobileCountry().getIsocode(), customerData.getMobileNumber());

			if (normalizedPhoneNumber.isPresent())
			{
				customerModel.setMobileNumber(normalizedPhoneNumber.get());
			}
			else
			{
				customerModel.setMobileNumber(customerData.getMobileNumber());
			}

		}
		if (customerData.getMobileCountry() != null && !StringUtils.isEmpty(customerData.getMobileCountry().getIsocode()))
		{
			customerModel.setMobileCountry(commonI18NService.getCountry(customerData.getMobileCountry().getIsocode()));
		}
	}

	/**
	 * @return the nationalityService
	 */
	protected NationalityService getNationalityService()
	{
		return nationalityService;
	}

}
