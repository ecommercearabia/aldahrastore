/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrauserfacades.customer.populator;

import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.NationalityData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Objects;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.aldahra.aldahrareferralcode.model.ReferralCodeModel;
import com.aldahra.aldahrareferralcodefacades.ReferralCodeData;
import com.aldahra.aldahrauser.model.NationalityModel;


/**
 * The Class CustomerPopulator.
 *
 * @author mnasro
 *
 *         Converter implementation for {@link de.hybris.platform.core.model.user.UserModel} as source and
 *         {@link de.hybris.platform.commercefacades.user.data.CustomerData} as target type.
 */


public class CustomerPopulator implements Populator<CustomerModel, CustomerData>
{

	/** The country converter. */
	@Resource(name = "countryConverter")
	private Converter<CountryModel, CountryData> countryConverter;

	@Resource(name = "referralCodeConverter")
	private Converter<ReferralCodeModel, ReferralCodeData> referralCodeConverter;
	@Resource(name = "nationalityConverter")
	private Converter<NationalityModel, NationalityData> nationalityConverter;

	/**
	 * @return the nationalityConverter
	 */
	protected Converter<NationalityModel, NationalityData> getNationalityConverter()
	{
		return nationalityConverter;
	}



	/**
	 * Fill the source to target.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	@Override
	public void populate(final CustomerModel source, final CustomerData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setMobileCountry(
				source.getMobileCountry() == null ? null : getCountryConverter().convert(source.getMobileCountry()));

		target.setMobileNumber(source.getMobileNumber());

		if (!Objects.isNull(source.getReferralCode()))
		{
			target.setReferralCode(referralCodeConverter.convert(source.getReferralCode()));
		}
		target.setNationality(source.getNationality() == null ? null : getNationalityConverter().convert(source.getNationality()));
		target.setNationalityID(source.getNationalityID());
	}



	/**
	 * Gets the country converter.
	 *
	 * @return the countryConverter
	 */
	public Converter<CountryModel, CountryData> getCountryConverter()
	{
		return countryConverter;
	}

}
