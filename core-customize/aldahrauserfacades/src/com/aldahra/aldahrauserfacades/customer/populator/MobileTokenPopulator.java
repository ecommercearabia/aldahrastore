/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrauserfacades.customer.populator;

import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.MobileTokenData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.aldahra.aldahrauser.model.MobileTokenModel;


/**
 * The Class CustomerPopulator.
 *
 * @author mnasro
 *
 *         Converter implementation for {@link de.hybris.platform.core.model.user.UserModel} as source and
 *         {@link de.hybris.platform.commercefacades.user.data.CustomerData} as target type.
 */


public class MobileTokenPopulator implements Populator<MobileTokenModel, MobileTokenData>
{

	/** The country converter. */
	@Resource(name = "customerConverter")
	private Converter<CustomerModel, CustomerData> customerConverter;


	/**
	 * Fill the source to target.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	@Override
	public void populate(final MobileTokenModel source, final MobileTokenData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setMobileToken(source.getToken());
		target.setEncodedMobileToken(source.getEncodedToken());
		target.setCustomer(getCustomerConverter().convert(source.getCustomer()));
	}


	/**
	 * @return the customerConverter
	 */
	protected Converter<CustomerModel, CustomerData> getCustomerConverter()
	{
		return customerConverter;
	}





}
