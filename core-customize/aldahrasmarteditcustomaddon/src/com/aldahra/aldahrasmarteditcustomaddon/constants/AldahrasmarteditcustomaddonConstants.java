/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahrasmarteditcustomaddon.constants;

@SuppressWarnings({"deprecation","PMD","squid:CallToDeprecatedMethod"})
public class AldahrasmarteditcustomaddonConstants extends GeneratedAldahrasmarteditcustomaddonConstants
{
	public static final String EXTENSIONNAME = "aldahrasmarteditcustomaddon";
	
	private AldahrasmarteditcustomaddonConstants()
	{
		//empty
	}
}
