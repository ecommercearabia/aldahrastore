/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahracmsocccustomaddon.jaxb.adapters.slots;

import static com.aldahra.aldahracmsocccustomaddon.jaxb.adapters.slots.SlotAdapterUtil.SlotAdaptedData;

import com.aldahra.aldahracmsocccustomaddon.data.ContentSlotWsDTO;

import java.util.Objects;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * This adapter is used to convert {@link ContentSlotWsDTO} into {@link SlotAdaptedData}
 */
public class SlotWsDTOAdapter extends XmlAdapter<SlotAdaptedData, ContentSlotWsDTO>
{
	@Override
	public SlotAdaptedData marshal(final ContentSlotWsDTO slot)
	{
		return Objects.nonNull(slot) ? convert(slot) : null;
	}

	protected SlotAdaptedData convert(final ContentSlotWsDTO slot)
	{
		return SlotAdapterUtil.convert(slot);
	}

	@Override
	public ContentSlotWsDTO unmarshal(final SlotAdaptedData slotAdaptedData)
	{
		throw new UnsupportedOperationException();
	}
}
