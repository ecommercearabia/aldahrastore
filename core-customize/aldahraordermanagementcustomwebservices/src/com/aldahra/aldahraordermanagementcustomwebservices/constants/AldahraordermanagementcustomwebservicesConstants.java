/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 */
package com.aldahra.aldahraordermanagementcustomwebservices.constants;

/**
 * Global class for all Aldahraordermanagementcustomwebservices constants. You can add global constants for your extension into this class.
 */
public final class AldahraordermanagementcustomwebservicesConstants
{
	public static final String EXTENSIONNAME = "aldahraordermanagementcustomwebservices";
	public static final String DEFAULT_ENCODING = "UTF-8";

	private AldahraordermanagementcustomwebservicesConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
