package com.aldahra.aldahraproductfacades.price.impl;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.LanguageModel;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.springframework.util.Assert;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class AldahraPriceDataFactory extends de.hybris.platform.commercefacades.product.impl.DefaultPriceDataFactory
{
	private static final String ARABIC = "Arabic";
	private final ConcurrentMap<String, NumberFormat> currencyFormatsEn = new ConcurrentHashMap<>();
	private final ConcurrentMap<String, NumberFormat> currencyFormatsAr = new ConcurrentHashMap<>();


	@Override
	public PriceData create(final PriceDataType priceType, final BigDecimal value, final String currencyIso)
	{
		final CurrencyModel currency = getCommonI18NService().getCurrency(currencyIso);
		return create(priceType, value, currency);
	}

	@Override
	public PriceData create(final PriceDataType priceType, final BigDecimal value, final CurrencyModel currency)
	{
		Assert.notNull(priceType, "Parameter priceType cannot be null.");
		Assert.notNull(value, "Parameter value cannot be null.");
		Assert.notNull(currency, "Parameter currency cannot be null.");

		final PriceData priceData = createPriceData();
		priceData.setPriceType(priceType);
		priceData.setValue(value);
		priceData.setCurrencyIso(currency.getIsocode());
		priceData.setFormattedValue(formatPrice(value, currency));

		final Locale loc = getI18NService().getCurrentLocale();
		String symbol = currency.getSymbolLoc(loc);
		symbol = symbol == null || symbol.trim().isEmpty() ? currency.getSymbol() : symbol;
		priceData.setSymbolLoc(symbol);

		String formattedValue = priceData.getFormattedValue();
		final String[] split = formattedValue.split(symbol);
		if (split.length > 1)
		{
			formattedValue = symbol + " " + split[1];
			priceData.setFormattedValue(formattedValue);
		}

		return priceData;
	}

	@Override
	protected NumberFormat createCurrencyFormat(final Locale locale, final CurrencyModel currency)
	{
		final String key = locale.getISO3Country() + "_" + currency.getIsocode();
		final Locale loc = getI18NService().getCurrentLocale();
		NumberFormat numberFormat = null;
		if (ARABIC.equals(loc.getDisplayLanguage()))
		{
			numberFormat = currencyFormatsAr.get(key);
			if (numberFormat == null)
			{
				final NumberFormat currencyFormat = createNumberFormat(locale, currency);
				if (currencyFormatsAr.putIfAbsent(key, currencyFormat) == null)
				{
					numberFormat = currencyFormat;
				}
			}
		}
		else
		{
			numberFormat = currencyFormatsEn.get(key);
			if (numberFormat == null)
			{
				final NumberFormat currencyFormat = createNumberFormat(locale, currency);
				if (currencyFormatsEn.putIfAbsent(key, currencyFormat) == null)
				{
					numberFormat = currencyFormat;
				}
			}
		}
		// don't allow multiple references
		return numberFormat == null ? null : (NumberFormat) numberFormat.clone();
	}


	@Override
	protected DecimalFormat adjustSymbol(final DecimalFormat format, final CurrencyModel currency)
	{
		final Locale loc = getI18NService().getCurrentLocale();
		String symbol = currency.getSymbolLoc(loc);
		symbol = symbol == null || symbol.trim().isEmpty() ? currency.getSymbol() : symbol;
		if (symbol != null)
		{
			final DecimalFormatSymbols symbols = format.getDecimalFormatSymbols(); // does cloning
			final String iso = currency.getIsocode();
			boolean changed = false;
			if (!iso.equalsIgnoreCase(symbols.getInternationalCurrencySymbol()))
			{
				symbols.setInternationalCurrencySymbol(iso);
				changed = true;
			}
			if (!symbol.equals(symbols.getCurrencySymbol()))
			{
				symbols.setCurrencySymbol(symbol);
				changed = true;
			}
			if (changed)
			{
				format.setDecimalFormatSymbols(symbols);
			}
		}
		return format;
	}



	public PriceData create(final PriceDataType priceType, final BigDecimal value, final CurrencyModel currency,
			final Integer digits)
	{
		Assert.notNull(priceType, "Parameter priceType cannot be null.");
		Assert.notNull(value, "Parameter value cannot be null.");
		Assert.notNull(currency, "Parameter currency cannot be null.");

		final PriceData priceData = createPriceData();

		priceData.setPriceType(priceType);
		priceData.setValue(value);
		priceData.setCurrencyIso(currency.getIsocode());
		priceData.setFormattedValue(formatPrice(value, currency, digits));

		final Locale loc = getI18NService().getCurrentLocale();
		String symbol = currency.getSymbolLoc(loc);
		symbol = symbol == null || symbol.trim().isEmpty() ? currency.getSymbol() : symbol;
		priceData.setSymbolLoc(symbol);

		String formattedValue = priceData.getFormattedValue();
		final String[] split = formattedValue.split(symbol);
		if (split.length > 1)
		{
			formattedValue = split[1] + " " + symbol;
			priceData.setFormattedValue(formattedValue);
		}

		return priceData;
	}

	protected String formatPrice(final BigDecimal value, final CurrencyModel currency, final Integer digits)
	{
		final LanguageModel currentLanguage = getCommonI18NService().getCurrentLanguage();
		Locale locale = getCommerceCommonI18NService().getLocaleForLanguage(currentLanguage);
		if (locale == null)
		{
			// Fallback to session locale
			locale = getI18NService().getCurrentLocale();
		}

		final NumberFormat currencyFormat = createCurrencyFormat(locale, currency, digits);
		return currencyFormat.format(value);
	}

	protected NumberFormat createCurrencyFormat(final Locale locale, final CurrencyModel currency, final Integer digits)
	{
		final String key = locale.getISO3Country() + "_" + currency.getIsocode();
		final Locale loc = getI18NService().getCurrentLocale();
		NumberFormat numberFormat = null;
		if (ARABIC.equals(loc.getDisplayLanguage()))
		{
			final NumberFormat currencyFormat = createNumberFormat(locale, currency, digits);
			currencyFormatsAr.putIfAbsent(key, currencyFormat);
			numberFormat = currencyFormat;
		}
		else
		{
			final NumberFormat currencyFormat = createNumberFormat(locale, currency, digits);
			currencyFormatsEn.putIfAbsent(key, currencyFormat);
			numberFormat = currencyFormat;
		}

		// don't allow multiple references
		return (NumberFormat) numberFormat.clone();
	}



	protected NumberFormat createNumberFormat(final Locale locale, final CurrencyModel currency, final Integer digits)
	{
		final DecimalFormat currencyFormat = (DecimalFormat) NumberFormat.getCurrencyInstance(locale);
		adjustDigits(currencyFormat, currency, digits);
		adjustSymbol(currencyFormat, currency);
		return currencyFormat;
	}

	protected DecimalFormat adjustDigits(final DecimalFormat format, final CurrencyModel currencyModel, final Integer digitsTemp)
	{
		int tempDigits = 0;
		if (digitsTemp == null)
		{
			tempDigits = currencyModel.getDigits() == null ? 0 : currencyModel.getDigits().intValue();
		}
		else
		{
			tempDigits = currencyModel.getDigits() == null ? 0 : digitsTemp.intValue();
		}

		final int digits = Math.max(0, tempDigits);

		format.setMaximumFractionDigits(digits);
		format.setMinimumFractionDigits(digits);
		if (digits == 0)
		{
			format.setDecimalSeparatorAlwaysShown(false);
		}

		return format;
	}


}