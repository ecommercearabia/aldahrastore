/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraproductfacades.populator;

import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.variants.model.VariantProductModel;

import com.aldahra.aldahraproduct.model.GroceryVariantProductModel;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class VariantOptionUnitOfMeasurePopulator implements Populator<VariantProductModel, VariantOptionData>
{

	@Override
	public void populate(final VariantProductModel source, final VariantOptionData target)
	{
		if (source instanceof GroceryVariantProductModel)
		{
			final GroceryVariantProductModel variant = (GroceryVariantProductModel) source;
			target.setUnitOfMeasure(variant.getUnitOfMeasure());
			target.setUnitOfMeasureDescription(variant.getUnitOfMeasureDescription());
		}
	}

}
