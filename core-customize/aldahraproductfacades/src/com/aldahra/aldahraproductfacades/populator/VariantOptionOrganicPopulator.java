/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraproductfacades.populator;

import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.variants.model.VariantProductModel;

import javax.annotation.Resource;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class VariantOptionOrganicPopulator implements Populator<VariantProductModel, VariantOptionData>
{
	@Resource(name = "classificationService")
	private ClassificationService classificationService;

	private static final String ORGANIC = "Organic";

	@Override
	public void populate(final VariantProductModel source, final VariantOptionData target)
	{
		if (source != null)
		{
			populateOrganic(source, target);
		}
	}

	/**
	 * @param source
	 *
	 * @param target
	 */
	private void populateOrganic(final VariantProductModel source, final VariantOptionData target)
	{
		final FeatureList features = classificationService.getFeatures(source);

		if (features != null)
		{
			final Feature collectionFeatureLabel = features.getFeatureByName(ORGANIC);
			if (collectionFeatureLabel != null && collectionFeatureLabel.getValue() != null)
			{
				final Boolean organic = (Boolean) collectionFeatureLabel.getValue().getValue();

				target.setOrganic(organic);

			}
		}
	}
}
