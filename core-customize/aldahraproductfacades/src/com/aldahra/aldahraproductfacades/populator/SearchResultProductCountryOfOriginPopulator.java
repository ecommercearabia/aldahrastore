package com.aldahra.aldahraproductfacades.populator;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.converters.Populator;

import org.springframework.util.Assert;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class SearchResultProductCountryOfOriginPopulator implements Populator<SearchResultValueData, ProductData>
{


	@Override
	public void populate(final SearchResultValueData source, final ProductData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		// Pull the values directly from the SearchResult object
		target.setCountryOfOrigin(this.<String> getValue(source, "CountryOfOrigin"));
		target.setCountryOfOriginIsocode(this.<String> getValue(source, "CountryOfOriginIsocode"));
	}

	protected <T> T getValue(final SearchResultValueData source, final String propertyName)
	{
		if (source.getValues() == null)
		{
			return null;
		}

		// DO NOT REMOVE the cast (T) below, while it should be unnecessary it is required by the javac compiler
		return (T) source.getValues().get(propertyName);
	}
}
