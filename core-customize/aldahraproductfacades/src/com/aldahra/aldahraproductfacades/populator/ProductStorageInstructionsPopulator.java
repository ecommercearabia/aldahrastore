package com.aldahra.aldahraproductfacades.populator;


import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;

import javax.annotation.Resource;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class ProductStorageInstructionsPopulator implements Populator<ProductModel, ProductData>
{
	@Resource(name = "classificationService")
	private ClassificationService classificationService;

	private static final String STORAGE_INSTRUCTIONS = "FoodcrowdClassification/1.0/FoodCrowdClassifications.storageinstructions";

	@Override
	public void populate(final ProductModel source, final ProductData target)
	{
		if (source != null)
		{
			populateClassification(source, target);
		}
	}

	/**
	 * @param source
	 *
	 * @param target
	 */
	private void populateClassification(final ProductModel source, final ProductData target)
	{
		final FeatureList features = classificationService.getFeatures(source);

		if (features != null)
		{
			final Feature storageInstructions = features.getFeatureByCode(STORAGE_INSTRUCTIONS);
			if (storageInstructions != null && storageInstructions.getValue() != null)
			{
				target.setStorageInstructions(storageInstructions.getValue().getValue().toString());
			}
		}
	}
}