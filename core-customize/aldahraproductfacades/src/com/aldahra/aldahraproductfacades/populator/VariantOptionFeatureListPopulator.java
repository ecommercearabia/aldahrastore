/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraproductfacades.populator;

import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.commercefacades.product.data.ClassificationData;
import de.hybris.platform.commercefacades.product.data.FeatureData;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class VariantOptionFeatureListPopulator<SOURCE extends FeatureList, TARGET extends VariantOptionData>
		implements Populator<SOURCE, TARGET>
{
	@Resource(name = "classificationConverter")
	private Converter<ClassificationClassModel, ClassificationData> classificationConverter;
	@Resource(name = "featureConverter")
	private Converter<Feature, FeatureData> featureConverter;

	@Override
	public void populate(final SOURCE source, final TARGET target)
	{
		target.setClassifications(buildClassificationDataList(source));
	}

	protected List<ClassificationData> buildClassificationDataList(final FeatureList source)
	{
		final List<ClassificationData> result = new ArrayList<>();
		final Map<String, ClassificationData> map = new HashMap<>();

		for (final Feature feature : source.getFeatures())
		{
			if (feature.getValues() != null && !feature.getValues().isEmpty())
			{
				final ClassificationData classificationData;
				final ClassificationClassModel classificationClass = feature.getClassAttributeAssignment().getClassificationClass();
				final String classificationClassCode = classificationClass.getCode();
				if (map.containsKey(classificationClassCode))
				{
					classificationData = map.get(classificationClassCode);
				}
				else
				{
					classificationData = classificationConverter.convert(classificationClass);

					map.put(classificationClassCode, classificationData);
					result.add(classificationData);
				}

				// Create the feature
				final FeatureData newFeature = featureConverter.convert(feature);

				// Add the feature to the classification
				if (classificationData.getFeatures() == null)
				{
					classificationData.setFeatures(new ArrayList<>(1));
				}
				classificationData.getFeatures().add(newFeature);
			}
		}

		return result.isEmpty() ? null : result;
	}
}
