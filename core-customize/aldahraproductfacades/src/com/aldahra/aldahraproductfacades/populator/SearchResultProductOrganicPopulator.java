package com.aldahra.aldahraproductfacades.populator;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.converters.Populator;

import org.springframework.util.Assert;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class SearchResultProductOrganicPopulator implements Populator<SearchResultValueData, ProductData>
{


	@Override
	public void populate(final SearchResultValueData source, final ProductData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		// Pull the values directly from the SearchResult object
		final Object organic = getValue(source, "Organic");
		target.setOrganic(false);
		if (Boolean.TRUE.equals(organic))
		{
			target.setOrganic(true);
		}

		final Object vegan = getValue(source, "Vegan");
		target.setVegan(false);
		if (Boolean.TRUE.equals(vegan))
		{
			target.setVegan(true);
		}

		final Object glutenFree = getValue(source, "GlutenFree");
		target.setGlutenFree(false);
		if (Boolean.TRUE.equals(vegan))
		{
			target.setGlutenFree(true);
		}

		final Object frozen = getValue(source, "Frozen");
		target.setFrozen(false);
		if (Boolean.TRUE.equals(frozen))
		{
			target.setFrozen(true);
		}

		final Object dry = getValue(source, "Dry");
		target.setDry(false);
		if (Boolean.TRUE.equals(dry))
		{
			target.setDry(true);
		}

		final Object chilled = getValue(source, "Chilled");
		target.setChilled(false);
		if (Boolean.TRUE.equals(chilled))
		{
			target.setChilled(true);
		}
	}

	protected <T> T getValue(final SearchResultValueData source, final String propertyName)
	{
		if (source.getValues() == null)
		{
			return null;
		}

		// DO NOT REMOVE the cast (T) below, while it should be unnecessary it is required by the javac compiler
		return (T) source.getValues().get(propertyName);
	}
}
