package com.aldahra.aldahraproductfacades.populator;


import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;

import javax.annotation.Resource;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class ProductClassificationFlagPopulator implements Populator<ProductModel, ProductData>
{
	@Resource(name = "classificationService")
	private ClassificationService classificationService;

	private static final String FROZEN = "FoodcrowdClassification/1.0/FoodCrowdClassifications.frozen";
	private static final String DRY = "FoodcrowdClassification/1.0/FoodCrowdClassifications.dry";
	private static final String CHILLED = "FoodcrowdClassification/1.0/FoodCrowdClassifications.chilled";


	@Override
	public void populate(final ProductModel source, final ProductData target)
	{
		if (source != null)
		{
			populateFrozen(source, target);
			populateDry(source, target);
			populateChilled(source, target);
		}
	}

	/**
	 * @param source
	 *
	 * @param target
	 */
	private void populateFrozen(final ProductModel source, final ProductData target)
	{
		final FeatureList features = classificationService.getFeatures(source);

		if (features != null)
		{
			final Feature collectionFeatureLabel = features.getFeatureByCode(FROZEN);
			if (collectionFeatureLabel != null && collectionFeatureLabel.getValue() != null)
			{
				final Boolean frozen = (Boolean) collectionFeatureLabel.getValue().getValue();

				target.setFrozen(frozen);

			}
		}
	}

	/**
	 * @param source
	 *
	 * @param target
	 */
	private void populateDry(final ProductModel source, final ProductData target)
	{
		final FeatureList features = classificationService.getFeatures(source);

		if (features != null)
		{
			final Feature collectionFeatureLabel = features.getFeatureByCode(DRY);
			if (collectionFeatureLabel != null && collectionFeatureLabel.getValue() != null)
			{
				final Boolean dry = (Boolean) collectionFeatureLabel.getValue().getValue();

				target.setDry(dry);

			}
		}
	}

	/**
	 * @param source
	 *
	 * @param target
	 */
	private void populateChilled(final ProductModel source, final ProductData target)
	{
		final FeatureList features = classificationService.getFeatures(source);

		if (features != null)
		{
			final Feature collectionFeatureLabel = features.getFeatureByCode(CHILLED);
			if (collectionFeatureLabel != null && collectionFeatureLabel.getValue() != null)
			{
				final Boolean chilled = (Boolean) collectionFeatureLabel.getValue().getValue();

				target.setChilled(chilled);

			}
		}
	}
}