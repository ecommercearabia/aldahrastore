/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraproductfacades.populator;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.aldahra.aldahrawishlist.exception.WishlistException;
import com.aldahra.aldahrawishlist.service.WishlistService;


/**
 * @author amjad.shati@erabia.com
 */
public class ProductWishlistPopulator implements Populator<ProductModel, ProductData>
{
	@Resource(name = "wishlistService")
	private WishlistService wishlistService;

	@Override
	public void populate(final ProductModel source, final ProductData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		try
		{
			target.setInWishlist(wishlistService.isProductInWishList(source.getCode()));
		}
		catch (final WishlistException e)
		{
			target.setInWishlist(false);
		}


	}

}
