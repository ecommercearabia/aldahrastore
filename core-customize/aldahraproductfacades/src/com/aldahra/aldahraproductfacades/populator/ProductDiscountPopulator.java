package com.aldahra.aldahraproductfacades.populator;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.DiscountPriceData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.promotionengineservices.model.PromotionSourceRuleModel;
import de.hybris.platform.promotionengineservices.model.RuleBasedPromotionModel;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.model.AbstractPromotionModel;
import de.hybris.platform.promotions.model.PromotionGroupModel;
import de.hybris.platform.ruleengine.model.DroolsRuleModel;
import de.hybris.platform.ruleengineservices.rule.data.RuleActionData;
import de.hybris.platform.ruleengineservices.rule.data.RuleConditionData;
import de.hybris.platform.ruleengineservices.rule.data.RuleParameterData;
import de.hybris.platform.ruleengineservices.rule.services.RuleActionsRegistry;
import de.hybris.platform.ruleengineservices.rule.services.RuleConditionsRegistry;
import de.hybris.platform.ruleengineservices.rule.strategies.RuleActionsConverter;
import de.hybris.platform.ruleengineservices.rule.strategies.RuleConditionsConverter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import com.aldahra.aldahraproductfacades.price.enums.RuleActionDefinitionType;
import com.aldahra.aldahraproductfacades.price.enums.RuleConditionChildrenDefinitionType;
import com.google.common.base.Preconditions;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class ProductDiscountPopulator implements Populator<ProductModel, ProductData>
{

	@Resource(name = "commercePriceService")
	private CommercePriceService commercePriceService;

	@Resource(name = "priceDataFactory")
	private PriceDataFactory priceDataFactory;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Resource(name = "promotionsService")
	private PromotionsService promotionsService;

	@Resource(name = "timeService")
	private TimeService timeService;

	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	@Resource(name = "catalogVersionService")
	private CatalogVersionService catalogVersionService;

	@Resource(name = "ruleActionsConverter")
	private RuleActionsConverter ruleActionsConverter;

	@Resource(name = "ruleConditionsConverter")
	private RuleConditionsConverter ruleConditionsConverter;
	@Resource(name = "ruleConditionsRegistry")
	private RuleConditionsRegistry ruleConditionsRegistry;

	@Resource(name = "ruleActionsRegistry")
	private RuleActionsRegistry ruleActionsRegistry;

	public static final String PRICE_REGEX = "\\{0\\}";
	public static final String DISCOUNT_PRICE_REGEX = "\\{1\\}";
	public static final String SAVING_REGEX = "\\{2\\}";
	public static final String PERCENTAGE_REGEX = "\\{3\\}";
	public static final String QUANTITY = "quantity";

	@Override
	public void populate(final ProductModel productModel, final ProductData productData)
	{
		populateDiscountPrice(productModel, productData);
	}

	/**
	 *
	 */
	private void populateDiscountPrice(final ProductModel productModel, final ProductData productData)
	{
		final Optional<BigDecimal> percentageValue = getPromotionPercentageValue(productModel);
		final PriceInformation info = getCommercePriceService().getWebPriceForProduct(productModel);
		if (percentageValue.isPresent())
		{
			final double priceVal = convertPrice(info.getPriceValue().getValue());
			final double savingVal = percentageValue.get().doubleValue() * priceVal / 100;
			productData.setDiscount(createDiscountPriceData(priceVal, savingVal, info, PriceDataType.BUY));
		}
		else
		{
			final Optional<BigDecimal> originalPrice = getOriginalPriceValue(productModel);
			if (originalPrice.isPresent())
			{
				productData.setDiscount(createDiscountPriceData(originalPrice.get().doubleValue(),
						originalPrice.get().doubleValue() - convertPrice(info.getPriceValue().getValue()), info, PriceDataType.BUY));
			}
		}

	}

	private double convertPrice(final double price)
	{
		final CurrencyModel currentCurrency = commonI18NService.getCurrentCurrency();

		final CurrencyModel baseCurrency = commonI18NService.getBaseCurrency();
		final double baseConversion = (baseCurrency.getConversion() == null ? 1.0 : baseCurrency.getConversion().doubleValue());
		return commonI18NService.convertAndRoundCurrency(baseConversion, baseConversion, currentCurrency.getDigits().intValue(),
				price);

	}

	private DiscountPriceData createDiscountPriceData(final double priceVal, final double savingVal, final PriceInformation info,
			final PriceDataType priceType)
	{
		DiscountPriceData discountPriceData = null;
		final double priceAfterDiscount = priceVal - savingVal;
		if (priceAfterDiscount < priceVal)
		{
			discountPriceData = new DiscountPriceData();
			final PriceData price = getPriceData(priceVal, priceType, info);
			final PriceData discount = getPriceData(priceAfterDiscount, priceType, info);
			final PriceData saving = getPriceData(savingVal, priceType, info);
			final double percentageValue = (savingVal / priceVal) * 100;

			discountPriceData.setPrice(price);
			discountPriceData.setDiscountPrice(discount);
			discountPriceData.setSaving(saving);
			discountPriceData.setPercentage(Double.valueOf(percentageValue));
			discountPriceData.setPercentageInt(Double.valueOf(percentageValue).intValue());
		}
		return discountPriceData;

	}

	/**
	 *
	 */
	private Optional<BigDecimal> getOriginalPriceValue(final ProductModel productModel)
	{
		Preconditions.checkArgument(productModel != null, "productModel mustn't be null");
		final CurrencyModel currentCurrency = commonI18NService.getCurrentCurrency();
		final double conversion = (currentCurrency.getConversion() == null ? 1.0 : currentCurrency.getConversion().doubleValue());
		final CurrencyModel baseCurrency = commonI18NService.getBaseCurrency();
		final double baseConversion = (baseCurrency.getConversion() == null ? 1.0 : baseCurrency.getConversion().doubleValue());

		final Optional<PriceRowModel> selectedPriceRow = getSelectedPriceRow(productModel.getEurope1Prices());

		if (selectedPriceRow.isEmpty() || selectedPriceRow.get().getOriginalPrice() == null)
		{
			return Optional.empty();
		}

		double originalPriceValue;
		if (!currentCurrency.getIsocode().equals(selectedPriceRow.get().getCurrency().getIsocode()))
		{
			originalPriceValue = commonI18NService.convertAndRoundCurrency(baseConversion, conversion,
					currentCurrency.getDigits().intValue(), selectedPriceRow.get().getOriginalPrice().doubleValue());
		}
		else
		{
			originalPriceValue = selectedPriceRow.get().getOriginalPrice().doubleValue();
		}

		return Optional.of(BigDecimal.valueOf(originalPriceValue));
	}


	/**
	 *
	 */
	private Optional<PriceRowModel> getSelectedPriceRow(final Collection<PriceRowModel> priceRowModels)
	{
		final CurrencyModel currentCurrency = commonI18NService.getCurrentCurrency();
		final Optional<PriceRowModel> selectedPriceRow = findPriceRowByCurrency(priceRowModels, currentCurrency.getIsocode());
		if (selectedPriceRow.isPresent())
		{
			return selectedPriceRow;
		}

		final CurrencyModel baseCurrency = commonI18NService.getBaseCurrency();
		return findPriceRowByCurrency(priceRowModels, baseCurrency.getIsocode());
	}


	/**
	 *
	 */
	private Optional<PriceRowModel> findPriceRowByCurrency(final Collection<PriceRowModel> priceRowModels,
			final String isocodeCurrency)
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(isocodeCurrency), "isocodeCurrency mustn't be null or empty");
		if (CollectionUtils.isEmpty(priceRowModels))
		{
			return Optional.empty();
		}
		for (final PriceRowModel priceRowModel : priceRowModels)
		{
			if (priceRowModel.getCurrency() != null && isocodeCurrency.equals(priceRowModel.getCurrency().getIsocode()))
			{
				return Optional.of(priceRowModel);
			}
		}
		return Optional.empty();
	}


	/**
	 *
	 */
	private Optional<BigDecimal> getPromotionPercentageValue(final ProductModel productModel)
	{
		Preconditions.checkArgument(productModel != null, "productModel mustn't be null");

		final BaseSiteModel baseSiteModel = getBaseSiteService().getCurrentBaseSite();
		if (baseSiteModel == null || baseSiteModel.getDefaultPromotionGroup() == null)
		{
			return Optional.empty();
		}
		final PromotionGroupModel defaultPromotionGroup = baseSiteModel.getDefaultPromotionGroup();

		final List<AbstractPromotionModel> promotions = (List<AbstractPromotionModel>) getPromotionsService()
				.getAbstractProductPromotions(Collections.singletonList(defaultPromotionGroup), productModel, true,
						DateUtils.round(getTimeService().getCurrentTime(), Calendar.MINUTE));

		if (promotions == null || promotions.size() != 1 || !(promotions.get(0) instanceof RuleBasedPromotionModel))
		{
			return Optional.empty();
		}
		final RuleBasedPromotionModel ruleBasedPromotionModel = (RuleBasedPromotionModel) promotions.get(0);
		if (ruleBasedPromotionModel == null || ruleBasedPromotionModel.getRule() == null
				|| !(ruleBasedPromotionModel.getRule() instanceof DroolsRuleModel))
		{
			return Optional.empty();
		}
		final DroolsRuleModel droolsRuleModel = (DroolsRuleModel) ruleBasedPromotionModel.getRule();
		if (droolsRuleModel == null || droolsRuleModel.getSourceRule() == null
				|| !(droolsRuleModel.getSourceRule() instanceof PromotionSourceRuleModel))
		{
			return Optional.empty();
		}
		final PromotionSourceRuleModel promotionSourceRuleModel = (PromotionSourceRuleModel) droolsRuleModel.getSourceRule();
		if (promotionSourceRuleModel.getActions() == null || promotionSourceRuleModel.getConditions() == null
				|| !isValidConditions(promotionSourceRuleModel))
		{
			return Optional.empty();
		}

		final List<RuleActionData> ruleActionDataList = ruleActionsConverter.fromString(promotionSourceRuleModel.getActions(),
				ruleActionsRegistry.getAllActionDefinitionsAsMap());
		if (ruleActionDataList == null || ruleActionDataList.size() != 1)
		{
			return Optional.empty();
		}
		if (!RuleActionDefinitionType.PERCENTAGE_DISCOUNT.getName().equals(ruleActionDataList.get(0).getDefinitionId()))
		{
			return Optional.empty();
		}

		final Map<String, RuleParameterData> parameters = ruleActionDataList.get(0).getParameters();
		if (parameters == null || parameters.size() != 1)
		{
			return Optional.empty();
		}

		final RuleParameterData ruleParameterData = parameters.get(parameters.keySet().iterator().next());
		if (!"java.math.BigDecimal".equals(ruleParameterData.getType()) || ruleParameterData.getValue() == null)
		{
			return Optional.empty();
		}

		return Optional.of(new BigDecimal(ruleParameterData.getValue().toString()));
	}


	private boolean isValidConditions(final PromotionSourceRuleModel promotionSourceRuleModel)
	{
		if (promotionSourceRuleModel == null)
		{
			return false;
		}
		final List<RuleConditionData> ruleConditionDataList = ruleConditionsConverter
				.fromString(promotionSourceRuleModel.getConditions(), ruleConditionsRegistry.getAllConditionDefinitionsAsMap());


		if (ruleConditionDataList == null || ruleConditionDataList.isEmpty() || ruleConditionDataList.size() != 1)
		{
			return false;
		}
		for (final RuleConditionData ruleConditionData : ruleConditionDataList)
		{
			if (ruleConditionData.getChildren() != null)
			{
				for (final RuleConditionData childrenRuleConditionData : ruleConditionData.getChildren())
				{

					final Map<String, RuleParameterData> parameters = childrenRuleConditionData.getParameters();
					if ((RuleConditionChildrenDefinitionType.CATEGORIES.getName().equals(childrenRuleConditionData.getDefinitionId())
							|| RuleConditionChildrenDefinitionType.PRODUCTS.getName()
									.equals(childrenRuleConditionData.getDefinitionId()))
							&& parameters != null && parameters.containsKey(QUANTITY) && parameters.get(QUANTITY).getValue() != null
							&& "1".equals("" + parameters.get(QUANTITY).getValue()))
					{
						return true;
					}

				}
			}
		}

		return false;
	}

	public String roundCeil(double value, final int places)
	{
		if (places < 0)
		{
			throw new IllegalArgumentException();
		}

		final long factor = (long) Math.pow(10, places);
		value = value * factor;
		final long tmp = Math.round(value);
		final double roundNumber = (double) tmp / factor;
		return String.format("%," + (places + 1) + "f", Double.valueOf(roundNumber));
	}


	/**
	 * @param value
	 * @param priceType
	 * @param info
	 * @return PriceData
	 */
	private PriceData getPriceData(final double value, final PriceDataType priceType, final PriceInformation info)
	{
		return getPriceDataFactory().create(priceType, BigDecimal.valueOf(value), info.getPriceValue().getCurrencyIso());
	}



	/**
	 * @return the commercePriceService
	 */
	public CommercePriceService getCommercePriceService()
	{
		return commercePriceService;
	}



	/**
	 * @return the priceDataFactory
	 */
	public PriceDataFactory getPriceDataFactory()
	{
		return priceDataFactory;
	}



	/**
	 * @return the baseStoreService
	 */
	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}



	/**
	 * @return the commonI18NService
	 */
	public CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}



	/**
	 * @return the promotionsService
	 */
	public PromotionsService getPromotionsService()
	{
		return promotionsService;
	}


	/**
	 * @return the timeService
	 */
	public TimeService getTimeService()
	{
		return timeService;
	}

	/**
	 * @return the baseSiteService
	 */
	public BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	/**
	 * @return the catalogVersionService
	 */
	public CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

}
