/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraproductfacades.populator;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import javax.annotation.Resource;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class VariantOptionDiscountPopulator implements Populator<VariantProductModel, VariantOptionData>
{
	@Resource(name = "productDiscountPopulator")
	private Populator<ProductModel, ProductData> productDiscountPopulator;

	@Override
	public void populate(final VariantProductModel productModel, final VariantOptionData variantOptionData)
	{
		final ProductData productData = new ProductData();
		productDiscountPopulator.populate(productModel, productData);
		variantOptionData.setDiscountPrice(productData.getDiscount());
	}
}
