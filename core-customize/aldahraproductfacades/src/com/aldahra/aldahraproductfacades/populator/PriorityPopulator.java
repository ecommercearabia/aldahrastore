package com.aldahra.aldahraproductfacades.populator;


import de.hybris.platform.commercefacades.product.data.PriorityData;
import de.hybris.platform.converters.Populator;

import com.aldahra.core.model.PriorityModel;


/**
 * @author Tuqa
 *
 */
public class PriorityPopulator implements Populator<PriorityModel, PriorityData>
{
	@Override
	public void populate(final PriorityModel source, final PriorityData target)
	{
		if (source != null)
		{
			populateCode(source, target);
			target.setPriority(target.getPriority());

		}
	}

	/**
	 *
	 */
	private void populateCode(final PriorityModel source, final PriorityData target)
	{
		if (target.getCode() != null)
		{
			target.setCode(target.getCode());

		}
	}





}