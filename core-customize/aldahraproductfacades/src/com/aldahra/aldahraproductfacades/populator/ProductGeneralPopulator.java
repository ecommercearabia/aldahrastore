package com.aldahra.aldahraproductfacades.populator;


import de.hybris.platform.commercefacades.product.data.PriorityData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import com.aldahra.core.model.PriorityModel;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class ProductGeneralPopulator implements Populator<ProductModel, ProductData>
{
	@Resource(name = "priorityConverter")
	private Converter<PriorityModel, PriorityData> priorityConverter;

	@Override
	public void populate(final ProductModel source, final ProductData target)
	{
		if (source != null)
		{
			populateExpress(source, target);
			populateNutritionFacts(source, target);
			populatePickSlipPriority(source, target);
		}
	}


	protected Converter<PriorityModel, PriorityData> getPriorityConverter()
	{
		return priorityConverter;
	}

	/**
	 *
	 */
	private void populatePickSlipPriority(final ProductModel source, final ProductData target)
	{
		target.setPickSlipPriority(getPriorityConverter().convert(source.getPickSlipPriority()));

	}

	/**
	 *
	 */
	private void populateNutritionFacts(final ProductModel source, final ProductData target)
	{
		target.setNutritionFacts(source.getNutritionFacts());
	}

	private void populateExpress(final ProductModel source, final ProductData target)
	{
		target.setExpress(source.isExpress());
	}

}