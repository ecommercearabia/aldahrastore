/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraproductfacades.populator;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.converters.populator.SearchResultVariantProductPopulator;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.ArrayList;
import java.util.List;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class CustomSearchResultVariantProductPopulator extends SearchResultVariantProductPopulator
{

	@Override
	protected void populateStock(final SearchResultValueData source, final ProductData target)
	{
		final String stockLevelStatus = this.<String> getValue(source, "stockLevelStatus");
		try
		{
			// In case of low stock then make a call to the stock service to determine if in or out of stock.
			// In this case (low stock) it is ok to load the product from the DB and do the real stock check
			final ProductModel productModel = getProductService().getProductForCode(target.getCode());
			if (productModel != null)
			{
				target.setStock(getStockConverter().convert(productModel));
			}
		}
		catch (final UnknownIdentifierException ex)
		{
			// If the product is no longer visible to the customergroup then this exception can be thrown

			// We can't remove the product from the results, but we can mark it as out of stock
			target.setStock(getStockLevelStatusConverter().convert(StockLevelStatus.OUTOFSTOCK));
		}
	}
	@Override
	protected List<ImageData> createImageData(final SearchResultValueData source)
	{
		final List<ImageData> result = new ArrayList<>();

		addImageData(source, "thumbnail", result);
		addImageData(source, "product", result);
		addImageData(source, "store", result);
		addImageData(source, "zoom", result);

		return result;
	}
}
