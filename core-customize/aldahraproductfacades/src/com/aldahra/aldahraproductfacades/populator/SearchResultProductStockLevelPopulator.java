/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraproductfacades.populator;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import javax.annotation.Resource;

import org.springframework.util.Assert;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class SearchResultProductStockLevelPopulator implements Populator<SearchResultValueData, ProductData>
{
	@Resource(name = "stockConverter")
	private Converter<ProductModel, StockData> stockConverter;

	@Resource(name = "productService")
	private ProductService productService;

	@Resource(name = "stockLevelStatusConverter")
	private Converter<StockLevelStatus, StockData> stockLevelStatusConverter;

	@Override
	public void populate(final SearchResultValueData source, final ProductData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		// Pull the values directly from the SearchResult object
		populateStock(source, target);
	}

	protected <T> T getValue(final SearchResultValueData source, final String propertyName)
	{
		if (source.getValues() == null)
		{
			return null;
		}

		// DO NOT REMOVE the cast (T) below, while it should be unnecessary it is required by the javac compiler
		return (T) source.getValues().get(propertyName);
	}

	protected void populateStock(final SearchResultValueData source, final ProductData target)
	{
		try
		{
			// In case of low stock then make a call to the stock service to determine if in or out of stock.
			// In this case (low stock) it is ok to load the product from the DB and do the real stock check
			final ProductModel productModel = productService.getProductForCode(target.getCode());
			if (productModel != null)
			{
				populateExpress(productModel, target);
				target.setStock(stockConverter.convert(productModel));
			}
		}
		catch (final UnknownIdentifierException ex)
		{
			// If the product is no longer visible to the customergroup then this exception can be thrown

			// We can't remove the product from the results, but we can mark it as out of stock
			target.setStock(stockLevelStatusConverter.convert(StockLevelStatus.OUTOFSTOCK));
		}
	}

	private void populateExpress(final ProductModel source, final ProductData target)
	{
		target.setExpress(source.isExpress());
	}
}
