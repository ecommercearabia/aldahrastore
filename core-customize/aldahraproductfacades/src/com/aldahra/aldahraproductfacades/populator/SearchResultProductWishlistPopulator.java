/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraproductfacades.populator;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.converters.Populator;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.aldahra.aldahrawishlist.exception.WishlistException;
import com.aldahra.aldahrawishlist.service.WishlistService;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class SearchResultProductWishlistPopulator implements Populator<SearchResultValueData, ProductData>
{
	@Resource(name = "wishlistService")
	private WishlistService wishlistService;

	@Override
	public void populate(final SearchResultValueData source, final ProductData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		// Pull the values directly from the SearchResult object
		try
		{
			target.setInWishlist(wishlistService.isProductInWishList(this.<String> getValue(source, "code")));
		}
		catch (final WishlistException e)
		{
			target.setInWishlist(false);
		}
	}

	protected <T> T getValue(final SearchResultValueData source, final String propertyName)
	{
		if (source.getValues() == null)
		{
			return null;
		}

		// DO NOT REMOVE the cast (T) below, while it should be unnecessary it is required by the javac compiler
		return (T) source.getValues().get(propertyName);
	}

}
