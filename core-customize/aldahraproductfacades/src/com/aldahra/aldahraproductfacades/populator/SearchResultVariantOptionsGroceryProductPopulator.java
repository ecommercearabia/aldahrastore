/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraproductfacades.populator;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commercefacades.search.converters.populator.SearchResultVariantOptionsProductPopulator;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.aldahra.aldahraproduct.model.GroceryVariantProductModel;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class SearchResultVariantOptionsGroceryProductPopulator extends SearchResultVariantOptionsProductPopulator
{
	@Override
	protected List<VariantOptionData> getVariantOptions(final List<SearchResultValueData> variants,
			final Set<String> variantTypeAttributes, final String rollupProperty)
	{
		final List<VariantOptionData> variantOptions = new ArrayList<>();

		for (final SearchResultValueData variant : variants)
		{
			final VariantOptionData variantOption = new VariantOptionData();
			variantOption.setCode((String) getValue(variant, CODE_VARIANT_PROPERTY));
			final Double priceValue = this.<Double> getValue(variant, PRICE_VALUE_VARIANT_PROPERTY);
			if (priceValue != null)
			{
				final PriceData priceData = getPriceDataFactory().create(PriceDataType.BUY,
						BigDecimal.valueOf(priceValue.doubleValue()), getCommonI18NService().getCurrentCurrency());
				variantOption.setPriceData(priceData);
			}

			variantOption.setUrl((String) getValue(variant, URL_VARIANT_PROPERTY));
			variantOption.setVariantOptionQualifiers(getVariantOptionQualifiers(variant, variantTypeAttributes, rollupProperty));

			populateUnitOfMeasure(variant, variantOption);

			variantOptions.add(variantOption);
		}
		return variantOptions;
	}

	protected void populateUnitOfMeasure(final SearchResultValueData variant, final VariantOptionData variantOption)
	{
		try
		{
			final ProductModel productModel = getProductService().getProductForCode(this.<String> getValue(variant, "code"));

			if (productModel instanceof GroceryVariantProductModel)
			{
				variantOption.setUnitOfMeasure(((GroceryVariantProductModel) productModel).getUnitOfMeasure());
				variantOption.setUnitOfMeasureDescription(((GroceryVariantProductModel) productModel).getUnitOfMeasureDescription());
			}
		}
		catch (final UnknownIdentifierException ex)
		{
			// DO NOTHING
		}
	}
}
