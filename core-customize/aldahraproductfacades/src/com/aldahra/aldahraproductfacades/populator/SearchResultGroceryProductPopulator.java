/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraproductfacades.populator;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import javax.annotation.Resource;

import com.aldahra.aldahraproduct.model.GroceryVariantProductModel;


/**
 *
 */
public class SearchResultGroceryProductPopulator implements Populator<SearchResultValueData, ProductData>
{
	@Resource(name = "productService")
	private ProductService productService;

	/**
	 * @return the productService
	 */
	protected ProductService getProductService()
	{
		return productService;
	}



	@Override
	public void populate(final SearchResultValueData source, final ProductData target)
	{
		populateUnitOfMeasure(source, target);

	}

	protected <T> T getValue(final SearchResultValueData source, final String propertyName)
	{
		if (source.getValues() == null)
		{
			return null;
		}

		// DO NOT REMOVE the cast (T) below, while it should be unnecessary it is required by the javac compiler
		return (T) source.getValues().get(propertyName);
	}


	protected void populateUnitOfMeasure(final SearchResultValueData variant, final ProductData target)
	{
		try
		{
			final ProductModel productModel = getProductService().getProductForCode(this.<String> getValue(variant, "code"));

			if (productModel instanceof GroceryVariantProductModel)
			{
				target.setUnitOfMeasure(((GroceryVariantProductModel) productModel).getUnitOfMeasure());
				target.setUnitOfMeasureDescription(((GroceryVariantProductModel) productModel).getUnitOfMeasureDescription());
			}
		}
		catch (final UnknownIdentifierException ex)
		{
			// DO NOTHING
		}
	}



}
