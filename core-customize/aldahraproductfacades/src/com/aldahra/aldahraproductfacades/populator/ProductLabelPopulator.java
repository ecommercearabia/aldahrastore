package com.aldahra.aldahraproductfacades.populator;


import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;

import javax.annotation.Resource;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class ProductLabelPopulator implements Populator<ProductModel, ProductData>
{
	@Resource(name = "classificationService")
	private ClassificationService classificationService;

	private static final String PRODUCT_LABEL = "Label";

	@Override
	public void populate(final ProductModel source, final ProductData target)
	{
		if (source != null)
		{
			populateClassification(source, target);
		}
	}

	/**
	 * @param source
	 *
	 * @param target
	 */
	private void populateClassification(final ProductModel source, final ProductData target)
	{
		final FeatureList features = classificationService.getFeatures(source);

		if (features != null)
		{
			final Feature collectionFeatureLabel = features.getFeatureByName(PRODUCT_LABEL);
			if (collectionFeatureLabel != null && collectionFeatureLabel.getValue() != null)
			{
				target.setProductLabel(collectionFeatureLabel.getValue().getValue().toString());
			}
		}
	}
}