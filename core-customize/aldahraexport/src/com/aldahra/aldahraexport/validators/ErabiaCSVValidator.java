/**
 *
 */
package com.aldahra.aldahraexport.validators;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.aldahra.aldahraexport.model.CSVFieldModel;
import com.aldahra.aldahraexport.model.CSVImportCronJobModel;
import com.aldahra.aldahraexport.model.ImpExScriptModel;


/**
 * @author jaafarNaddaf
 *
 */
public interface ErabiaCSVValidator
{
	/**
	 * Validates the CSVImportCronJobModel
	 *
	 * @param data
	 * @param scripts
	 * @return Map<String, String>: <fieldName, error>
	 */
	public Map<String, String> validate(final StringBuilder data, final Set<ImpExScriptModel> scripts);

	/**
	 * Checks whether the cronjob has at least one source file
	 *
	 * @param job
	 * @return boolean
	 */
	public boolean hasFile(CSVImportCronJobModel job);

	/**
	 * @param mailAddress
	 * @return boolean
	 */
	public boolean validateMail(final String mailAddress);

	/**
	 * @param script
	 * @return boolean
	 */
	public boolean hasScript(final String script);

	/**
	 * @param job
	 * @return boolean
	 */
	public boolean hasFTP(final CSVImportCronJobModel job);

	/**
	 *
	 * @param csv
	 * @param fields
	 * @return Map<String, String>
	 */
	public Map<String, String> isValid(final List<List<String>> csv, final Set<CSVFieldModel> fields);
}
