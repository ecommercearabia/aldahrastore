/**
 *
 */
package com.aldahra.aldahraexport.services;

import de.hybris.platform.core.model.media.MediaModel;

import java.util.ArrayList;
import java.util.List;


/**
 * @author jaafarnaddaf
 *
 */
public interface ErabiaReportService
{
	/**
	 * Finds all reports
	 *
	 * @return List<MediaModel>
	 */
	List<MediaModel> getReports();

	/**
	 * Finds results for the provided query
	 *
	 * @param query
	 * @return List<ArrayList<String>>
	 */
	List<ArrayList<String>> getResultForQuery(int columns, String query);

	/**
	 * Finds results for the provided query
	 *
	 * @param query
	 * @return List<String>
	 */
	List<String> getResultForQuery(String query);
}
