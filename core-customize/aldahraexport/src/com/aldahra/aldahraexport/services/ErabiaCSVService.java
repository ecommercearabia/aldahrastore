/**
 *
 */
package com.aldahra.aldahraexport.services;

import de.hybris.platform.servicelayer.impex.ImportConfig;

import java.util.List;
import java.util.Set;

import com.aldahra.aldahraexport.model.CSVFieldModel;
import com.aldahra.aldahraexport.model.ImpExScriptModel;


/**
 * @author jaafarNaddaf
 *
 */
public interface ErabiaCSVService
{
	/**
	 *
	 * @param hostname
	 * @param path
	 * @param username
	 * @param password
	 * @return StringBuilder
	 */
	public StringBuilder readFTP(String hostname, String path, String username, String password);

	/**
	 *
	 * @param hostname
	 * @param path
	 * @param username
	 * @param password
	 * @param regEx
	 * @return StringBuilder
	 */
	public StringBuilder readFTPDirectory(String hostname, String path, String username, String password, String regEx);

	/**
	 *
	 * @param hostname
	 * @param path
	 * @param username
	 * @param password
	 * @return boolean
	 */
	public boolean isFTPDirectory(String hostname, String path, String username, String password);

	/**
	 *
	 * @param script
	 * @return StringBuilder
	 */
	public StringBuilder readScript(String script);

	/**
	 *
	 * @param data
	 * @param string
	 * @return List<List<String>>
	 */
	public List<List<String>> readerToCSV(StringBuilder data, String string);

	/**
	 *
	 * @param scripts
	 * @return Set<CSVFieldModel>
	 */
	public Set<CSVFieldModel> getFields(Set<ImpExScriptModel> scripts);

	/**
	 * @param csv
	 * @param script
	 * @param fields
	 * @return String
	 */
	public String csvToImpEx(List<List<String>> csv, ImpExScriptModel script, Set<CSVFieldModel> fields);

	/**
	 *
	 * @param impExScript
	 * @return ImportConfig
	 */
	public ImportConfig prepareImportConfig(final StringBuilder impExScript);
}
