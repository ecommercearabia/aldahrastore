/**
 *
 */
package com.aldahra.aldahraexport.services.impl;

import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.media.MediaService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.aldahra.aldahraexport.dao.ErabiaReportDao;
import com.aldahra.aldahraexport.services.ErabiaReportService;


/**
 * @author jaafarnaddaf
 *
 */
public class DefaultErabiaReportService implements ErabiaReportService
{
	@Resource(name = "mediaService")
	private MediaService mediaService;

	@Resource(name = "erabiaReportDao")
	private ErabiaReportDao erabiaReportDao;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.erabia.core.services.ErabiaReportService#getReports()
	 */
	@Override
	public List<MediaModel> getReports()
	{
		final String catalog = cmsSiteService.getCurrentCatalogVersion().getCatalog().getPk().toString();
		final String catalogVersion = cmsSiteService.getCurrentCatalogVersion().getPk().toString();
		final List<String> mediaCodes;
		final List<MediaModel> media = new ArrayList<>();
		mediaCodes = erabiaReportDao.getReports(catalog, catalogVersion);
		for (final String code : mediaCodes)
		{
			media.add(mediaService.getMedia(code));
		}

		return media;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.erabia.core.services.ErabiaReportService#getResultForQuery(java.lang.String)
	 */
	@Override
	public List<ArrayList<String>> getResultForQuery(final int columns, final String query)
	{
		final List<ArrayList<String>> result;
		result = erabiaReportDao.getResultForQuery(columns, query);

		return result;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.erabia.core.services.ErabiaReportService#getResultForQuery(java.lang.String)
	 */
	@Override
	public List<String> getResultForQuery(final String query)
	{
		final List<String> result;
		result = erabiaReportDao.getResultForQuery(query);

		return result;
	}
}
