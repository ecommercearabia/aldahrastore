/**
 *
 */
package com.aldahra.aldahraexport.services.impl;

import de.hybris.platform.servicelayer.impex.ImpExResource;
import de.hybris.platform.servicelayer.impex.ImportConfig;
import de.hybris.platform.servicelayer.impex.impl.StreamBasedImpExResource;
import de.hybris.platform.util.CSVConstants;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.log4j.Logger;

import com.aldahra.aldahraexport.model.CSVFieldModel;
import com.aldahra.aldahraexport.model.ImpExScriptModel;
import com.aldahra.aldahraexport.services.ErabiaCSVService;


/**
 * @author jaafarNaddaf
 *
 */
public class DefaultErabiaCSVService implements ErabiaCSVService
{
	private static final Logger LOG = Logger.getLogger(DefaultErabiaCSVService.class.getName());
	private static final String IN_PATTERN = "\\{\\+*[0-9]*\\}";

	/*
	 * (non-Javadoc)
	 *
	 * @see com.aldahra.aldahraexport.services.ErabiaCSVService#readFTP(java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public StringBuilder readFTP(final String hostname, final String path, final String username, final String password)
	{
		final StringBuilder content = new StringBuilder();
		final FTPClient ftpClient = new FTPClient();
		InputStream inputStream = null;
		try
		{
			ftpClient.connect(hostname);
			ftpClient.login(username, password);
			ftpClient.enterLocalPassiveMode();
			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
			ftpClient.setControlKeepAliveTimeout(0);
			inputStream = ftpClient.retrieveFileStream(path);
		}
		catch (final IOException ex)
		{
			LOG.error("Couldn't connect to host:[" + hostname + "]");
			return content;
		}
		finally
		{
			try
			{
				if (ftpClient.isConnected())
				{
					ftpClient.logout();
					ftpClient.disconnect();
				}
			}
			catch (final IOException ex)
			{
				LOG.error(ex.getMessage());
			}
		}

		String line = "";
		if (inputStream != null)
		{
			try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream)))
			{
				while ((line = reader.readLine()) != null)
				{
					content.append(line + "\n");
				}
			}
			catch (final IOException ex)
			{
				LOG.error(ex.getMessage());
			}
		}
		else
		{
			LOG.error("Ftp file not exist:[" + path + "]");
		}

		return content;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.aldahra.aldahraexport.services.ErabiaCSVService#readFTPDirectory(java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public StringBuilder readFTPDirectory(final String hostname, final String path, final String username, final String password,
			final String regEx)
	{
		final FTPClient ftpClient = new FTPClient();
		final StringBuilder content = new StringBuilder();
		try
		{
			ftpClient.connect(hostname);
			ftpClient.login(username, password);
			ftpClient.enterLocalPassiveMode();
			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
			ftpClient.setControlKeepAliveTimeout(0);

			final List<FTPFile> files = new ArrayList<>();

			final FTPFile[] ftpClientFiles = ftpClient.listFiles(path);
			if (ftpClientFiles != null && ftpClientFiles.length != 0)
			{

				for (final FTPFile file : ftpClientFiles)
				{
					if (regEx != null && file.getName().matches(regEx))
					{
						files.add(file);
					}
				}

				Collections.sort(files, (final FTPFile a, final FTPFile b) -> a.getName().compareToIgnoreCase(b.getName()));

				for (final FTPFile file : files)
				{
					content.append(readFTP(hostname, path + file.getName(), username, password));
				}
			}
			else
			{
				LOG.error("FTP Directory does not have files:[" + path + "]");
			}
		}
		catch (final IOException ex)
		{
			LOG.error("Couldn't connect to host:[" + hostname + "]");
		}
		finally
		{
			try
			{
				if (ftpClient.isConnected())
				{
					ftpClient.logout();
					ftpClient.disconnect();
				}
			}
			catch (final IOException ex)
			{
				LOG.error(ex.getMessage());
			}
		}
		return content;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.aldahra.aldahraexport.services.ErabiaCSVService#isFTPDirectory(java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public boolean isFTPDirectory(final String hostname, final String path, final String username, final String password)
	{
		final FTPClient ftpClient = new FTPClient();
		try
		{
			ftpClient.connect(hostname);
			ftpClient.login(username, password);
			ftpClient.enterLocalPassiveMode();
			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
			ftpClient.setControlKeepAliveTimeout(0);

			return ftpClient.changeWorkingDirectory(path);
		}
		catch (final IOException ex)
		{
			LOG.error(ex.getMessage());
		}
		finally
		{
			try
			{
				if (ftpClient.isConnected())
				{
					ftpClient.logout();
					ftpClient.disconnect();
				}
			}
			catch (final IOException ex)
			{
				LOG.error(ex.getMessage());
			}
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.aldahra.aldahraexport.services.ErabiaCSVService#readScript(java.lang.String)
	 */
	@Override
	public StringBuilder readScript(final String script)
	{
		final InputStream input = new ByteArrayInputStream(script.getBytes());
		final BufferedReader reader = new BufferedReader(new InputStreamReader(input));
		final StringBuilder content = new StringBuilder();
		String line = StringUtils.EMPTY;

		try
		{
			while ((line = reader.readLine()) != null)
			{
				content.append(line + "\n");
			}

			reader.close();
		}
		catch (final IOException e)
		{
			LOG.error(e.getMessage());
		}
		return content;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.aldahra.aldahraexport.services.ErabiaCSVService#readerToCSV(java.io.BufferedReader)
	 */
	@Override
	public List<List<String>> readerToCSV(final StringBuilder data, final String separator)
	{
		final List<List<String>> csv = new ArrayList<>();
		final List<String> lines = Arrays.asList(StringUtils.split(data.toString(), "\n"));

		for (String line : lines)
		{
			line = line.replaceAll("\"", "\"\"");
			csv.add(Arrays.asList(line.split(separator, -1)));
		}

		return csv;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.aldahra.aldahraexport.services.ErabiaCSVService#getFields(java.util.List)
	 */
	@Override
	public Set<CSVFieldModel> getFields(final Set<ImpExScriptModel> scripts)
	{
		final Set<CSVFieldModel> fields = new HashSet<>();
		final Pattern pattern = Pattern.compile(IN_PATTERN);
		for (final ImpExScriptModel impEx : scripts)
		{
			final Matcher matcher = pattern.matcher(impEx.getImpExScript());
			while (matcher.find())
			{
				try
				{
					final CSVFieldModel field = new CSVFieldModel();
					field.setPosition(Integer.valueOf(matcher.group().substring(1, matcher.group().indexOf('}'))));
					field.setRequired(Boolean.valueOf(matcher.group().contains("+")));
					final CSVFieldModel optionalField = new CSVFieldModel();
					optionalField.setPosition(field.getPosition());
					optionalField.setRequired(Boolean.FALSE);
					if (fields.contains(optionalField) && field.getRequired().booleanValue())
					{
						fields.remove(optionalField);
					}

					fields.add(field);
				}
				catch (final NumberFormatException ex)
				{
					LOG.error(ex.getMessage());
				}
			}
		}

		return fields;
	}

	@Override
	public String csvToImpEx(final List<List<String>> csv, final ImpExScriptModel impExScript, final Set<CSVFieldModel> fields)
	{
		final StringBuilder script = new StringBuilder();
		for (final List<String> value : csv)
		{
			String line = impExScript.getImpExScript();
			for (final CSVFieldModel field : fields)
			{
				try
				{
					line = line.replaceAll("\\{\\+*" + field.getPosition().intValue() + "\\}",
							value.get(field.getPosition().intValue()));
				}
				catch (final ArrayIndexOutOfBoundsException aioobe)
				{
					if (!field.getRequired().booleanValue())
					{
						line = line.replaceAll("\\{\\+*" + field.getPosition().intValue() + "\\}", StringUtils.EMPTY);
					}
					else
					{
						throw aioobe;
					}
				}
			}
			script.append(line + "\n");
		}

		return script.toString();
	}

	@Override
	public ImportConfig prepareImportConfig(final StringBuilder impExScript)
	{
		final ImportConfig config = new ImportConfig();
		final ImpExResource script = new StreamBasedImpExResource(new ByteArrayInputStream((impExScript.toString()).getBytes()),
				CSVConstants.HYBRIS_ENCODING);
		config.setScript(script);
		config.setFailOnError(true);
		config.setDumpingEnabled(true);
		return config;
	}
}
