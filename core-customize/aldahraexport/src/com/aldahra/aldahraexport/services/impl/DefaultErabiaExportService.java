/**
 *
 */
package com.aldahra.aldahraexport.services.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.jalo.Catalog;
import de.hybris.platform.catalog.jalo.CatalogManager;
import de.hybris.platform.catalog.jalo.CatalogVersion;
import de.hybris.platform.catalog.jalo.SyncItemCronJob;
import de.hybris.platform.catalog.jalo.SyncItemJob;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaFolderModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.jalo.CronJob;
import de.hybris.platform.impex.model.ImpExMediaModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.aldahra.aldahraexport.bean.AddressBean;
import com.aldahra.aldahraexport.bean.EmailBean;
import com.aldahra.aldahraexport.process.email.actions.SendEmailAction;
import com.aldahra.aldahraexport.services.ErabiaExportService;


/**
 * @author jaafarNaddaf
 *
 */
public class DefaultErabiaExportService implements ErabiaExportService
{
	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "sendCustomEmail")
	private SendEmailAction sendCustomEmail;

	@Resource(name = "mediaService")
	private MediaService mediaService;

	@Resource(name = "catalogVersionService")
	private CatalogVersionService catalogVersionService;

	public static final Logger LOG = Logger.getLogger(DefaultErabiaExportService.class.getName());

	/*
	 * (non-Javadoc)
	 *
	 * @see com.aldahra.aldahraexport.services.ErabiaExportService#sendMail(com.erabia.core.bean.EmailBean)
	 */
	@Override
	public boolean sendMail(final EmailBean mail)
	{
		boolean isSent = false;

		try
		{
			final String fromAddressString = configurationService.getConfiguration().getString("mail.smtp.user");
			final AddressBean fromAddress = new AddressBean();
			fromAddress.setAddress(fromAddressString);
			fromAddress.setDisplayName(fromAddressString);
			mail.setFromAddress(fromAddress);
			mail.setReplyToAddres(fromAddress.getAddress());
			isSent = sendCustomEmail.sendEmail(mail);
		}
		catch (final Exception e)
		{
			LOG.error(e.getMessage());
		}

		return isSent;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.aldahra.aldahraexport.services.ErabiaExportService#synchronize(java.lang.String)
	 */
	@Override
	public void synchronize(final String catalogId)
	{
		final SyncItemJob catalogSyncJob = getCatalogSyncJob(catalogId);
		if (catalogSyncJob == null)
		{
			LOG.error("Couldn't find 'SyncItemJob' for catalog [" + catalogId + "]", null);
		}
		else
		{
			final SyncItemCronJob syncJob = getLastFailedSyncCronJob(catalogSyncJob);
			syncJob.setLogToDatabase(false);
			syncJob.setLogToFile(false);
			syncJob.setForceUpdate(false);

			catalogSyncJob.configureFullVersionSync(syncJob);

			catalogSyncJob.perform(syncJob, true);
		}
	}

	private SyncItemJob getCatalogSyncJob(final String catalogId)
	{
		// Lookup the catalog name
		final Catalog catalog = CatalogManager.getInstance().getCatalog(catalogId);
		if (catalog != null)
		{
			final CatalogVersion source = catalog.getCatalogVersion(CatalogManager.OFFLINE_VERSION);
			final CatalogVersion target = catalog.getCatalogVersion(CatalogManager.ONLINE_VERSION);

			if (source != null && target != null)
			{
				return CatalogManager.getInstance().getSyncJob(source, target);
			}
		}
		return null;
	}

	private SyncItemCronJob getLastFailedSyncCronJob(final SyncItemJob syncItemJob)
	{
		SyncItemCronJob syncCronJob = null;
		if (CollectionUtils.isNotEmpty(syncItemJob.getCronJobs()))
		{
			final List<CronJob> cronjobs = new ArrayList<>(syncItemJob.getCronJobs());
			Collections.sort(cronjobs, (final CronJob cronJob1, final CronJob cronJob2) -> {
				if (cronJob1 == null || cronJob1.getEndTime() == null || cronJob2 == null || cronJob2.getEndTime() == null)
				{
					return 0;
				}
				else
				{
					return cronJob1.getEndTime().compareTo(cronJob2.getEndTime());
				}
			});
			final SyncItemCronJob latestCronJob = (SyncItemCronJob) cronjobs.get(cronjobs.size() - 1);
			final CronJobResult result = modelService.get(latestCronJob.getResult());
			final CronJobStatus status = modelService.get(latestCronJob.getStatus());
			if (CronJobStatus.FINISHED.equals(status) && !CronJobResult.SUCCESS.equals(result))
			{
				syncCronJob = latestCronJob;
			}
		}

		if (syncCronJob == null)
		{
			syncCronJob = syncItemJob.newExecution();
		}

		return syncCronJob;
	}


	private MediaFolderModel getImpExMediaFolder(final String folderName)
	{
		MediaFolderModel folder = new MediaFolderModel();

		try
		{
			folder = mediaService.getFolder(folderName);
		}
		catch (final UnknownIdentifierException uie)
		{
			folder.setQualifier(folderName);
			folder.setPath(folderName);
			modelService.save(folder);
		}
		catch (final AmbiguousIdentifierException aie)
		{
			folder = mediaService.getRootFolder();
		}
		return folder;
	}

	protected CatalogVersionModel getCatalogVersion()
	{
		final String catalog = "Default";

		return catalogVersionService.getCatalogVersion(catalog, "Staged");
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.aldahra.aldahraexport.services.ErabiaExportService#impExToMedia(java.lang.String, java.lang.StringBuilder,
	 * java.lang.String)
	 */
	@Override
	public ImpExMediaModel impExToMedia(final String code, final StringBuilder data, final String folderName)
	{
		try
		{
			final InputStream stream = new ByteArrayInputStream(data.toString().getBytes(StandardCharsets.UTF_8));

			final ImpExMediaModel media = modelService.create(ImpExMediaModel.class);
			final String name = code + "_" + System.currentTimeMillis() + ".impex";
			final MediaFolderModel folder = getImpExMediaFolder(folderName);

			media.setCode(name);
			media.setMime("text/plain");
			media.setRealFileName(name + ".impex");
			media.setCatalogVersion(getCatalogVersion());
			media.setFolder(folder);
			modelService.save(media);

			mediaService.setStreamForMedia(media, stream, name, "text/csv", folder);
			return (ImpExMediaModel) mediaService.getMedia(getCatalogVersion(), name);
		}
		catch (final Exception e)
		{
			LOG.error("Couldn't save media [" + code + "]");
			return null;
		}
	}
}
