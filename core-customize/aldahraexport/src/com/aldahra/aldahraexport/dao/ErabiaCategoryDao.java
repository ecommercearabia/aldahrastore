package com.aldahra.aldahraexport.dao;

import de.hybris.platform.category.daos.CategoryDao;
import de.hybris.platform.category.model.CategoryModel;

import java.util.List;


public interface ErabiaCategoryDao extends CategoryDao
{
	/**
	 * Returns CategoryModel for the specified query
	 *
	 * @param query
	 * @return CategoryModel
	 */
	List<CategoryModel> getResultForQuery(String query);


	/**
	 * @param query
	 * @return
	 */
	List<String> getResultForQueryAsList(String query);
}
