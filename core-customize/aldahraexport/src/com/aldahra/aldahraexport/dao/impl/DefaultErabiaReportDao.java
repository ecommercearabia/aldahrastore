package com.aldahra.aldahraexport.dao.impl;

import de.hybris.platform.servicelayer.media.impl.DefaultMediaDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.aldahra.aldahraexport.dao.ErabiaReportDao;


/**
 * @author jaafarnaddaf
 *
 */
public class DefaultErabiaReportDao extends DefaultMediaDao implements ErabiaReportDao
{

	/*
	 * (non-Javadoc)
	 *
	 * @see com.erabia.core.dao.ErabiaReportDao#getReports()
	 */
	@Override
	public List<String> getReports(final String catalog, final String catalogVersion)
	{
		final StringBuilder query = new StringBuilder();

		query.append("SELECT * FROM {JasperMedia} WHERE {RealFileName}!=''");
		query.append("AND {Catalog}='" + catalog + "'");
		query.append("AND {CatalogVersion}='" + catalogVersion + "'");

		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query);

		flexibleSearchQuery.setResultClassList(Collections.singletonList(String.class));

		final SearchResult<String> result = getFlexibleSearchService().search(flexibleSearchQuery);

		return result.getResult();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.erabia.core.dao.ErabiaReportDao#getResultForQuery(java.lang.String)
	 */
	@Override
	public List<ArrayList<String>> getResultForQuery(final int columns, final String query)
	{
		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query);
		final List<Class<String>> columnList = new ArrayList<>();

		for (int i = 0; i < columns; i++)
		{
			columnList.add(String.class);
		}

		flexibleSearchQuery.setResultClassList(columnList);

		final SearchResult<ArrayList<String>> result = getFlexibleSearchService().search(flexibleSearchQuery);

		return result.getResult();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.erabia.core.dao.ErabiaReportDao#getResultForQuery(java.lang.String)
	 */
	@Override
	public List<String> getResultForQuery(final String query)
	{
		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query);
		flexibleSearchQuery.setResultClassList(Collections.singletonList(String.class));

		final SearchResult<String> result = getFlexibleSearchService().search(flexibleSearchQuery);

		return result.getResult();
	}
}
