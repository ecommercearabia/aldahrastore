/**
 *
 */
package com.aldahra.aldahraexport.dao.impl;


import de.hybris.platform.category.daos.impl.DefaultCategoryDao;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.List;

import com.aldahra.aldahraexport.dao.ErabiaCategoryDao;


/**
 * @author m-nasro
 *
 */
public class DefaultErabiaCategoryDao extends DefaultCategoryDao implements ErabiaCategoryDao
{
	/*
	 * (non-Javadoc)
	 *
	 * @see com.erabia.core.dao.ErabiaCategoryDao#getResultForQuery(java.lang.String)
	 */
	@Override
	public List<CategoryModel> getResultForQuery(final String query)
	{
		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query);

		final SearchResult<CategoryModel> result = getFlexibleSearchService().search(flexibleSearchQuery);

		return result.getResult();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.erabia.core.dao.ErabiaCategoryDao#getResultForQueryAsList(java.lang.String)
	 */
	@Override
	public List<String> getResultForQueryAsList(final String query)
	{
		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query);
		flexibleSearchQuery.setResultClassList(Collections.singletonList(String.class));

		final SearchResult<String> result = getFlexibleSearchService().search(flexibleSearchQuery);

		return result.getResult();
	}
}
