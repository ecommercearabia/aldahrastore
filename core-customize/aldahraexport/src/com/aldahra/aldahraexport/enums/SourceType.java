/**
 *
 */
package com.aldahra.aldahraexport.enums;

import java.util.HashMap;
import java.util.Map;


/**
 * @author jaafarNaddaf
 *
 */
public enum SourceType
{
	FTP("ftp"), MEDIA("media"), SCRIPT("script");
	private final String key;

	private static final Map<String, SourceType> LOOKUP = new HashMap<>();

	static
	{
		for (final SourceType sourceType : SourceType.values())
		{
			LOOKUP.put(sourceType.getKey(), sourceType);
		}
	}

	/**
	 *
	 */
	private SourceType(final String key)
	{
		this.key = key;
	}

	public String getKey()
	{
		return this.key;
	}

	public static SourceType get(final String value)
	{
		return LOOKUP.get(value);
	}
}
