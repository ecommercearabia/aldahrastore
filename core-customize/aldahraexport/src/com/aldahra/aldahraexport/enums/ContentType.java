/**
 *
 */
package com.aldahra.aldahraexport.enums;

import java.util.HashMap;
import java.util.Map;


/**
 * @author jaafarNaddaf
 *
 */
public enum ContentType
{
	CSV("csv"), CONTENT("content");
	private final String key;

	private static final Map<String, ContentType> LOOKUP = new HashMap<>();

	static
	{
		for (final ContentType contentType : ContentType.values())
		{
			LOOKUP.put(contentType.getKey(), contentType);
		}
	}

	/**
	 *
	 */
	private ContentType(final String key)
	{
		this.key = key;
	}

	public String getKey()
	{
		return this.key;
	}

	public static ContentType get(final String value)
	{
		return LOOKUP.get(value);
	}
}
