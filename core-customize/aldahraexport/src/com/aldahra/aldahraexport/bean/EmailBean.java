/**
 *
 */
package com.aldahra.aldahraexport.bean;

import de.hybris.platform.acceleratorservices.model.email.EmailAttachmentModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * @author k-rummaneh
 *
 */
public class EmailBean implements Serializable
{
	private final List<AddressBean> toAddresses;
	private AddressBean fromAddress;
	private String replyToAddres;
	private String body;
	private String subject;
	private final List<EmailAttachmentModel> attachments;

	public EmailBean()
	{
		attachments = new ArrayList<>();
		toAddresses = new ArrayList<>();
	}

	/**
	 * @return the toAddresses
	 */
	public List<AddressBean> getToAddresses()
	{
		return toAddresses;
	}

	/**
	 * @return the fromAddress
	 */
	public AddressBean getFromAddress()
	{
		return fromAddress;
	}

	/**
	 * @param fromAddress
	 *           the fromAddress to set
	 */
	public void setFromAddress(final AddressBean fromAddress)
	{
		this.fromAddress = fromAddress;
	}

	/**
	 * @return the replyToAddres
	 */
	public String getReplyToAddres()
	{
		return replyToAddres;
	}

	/**
	 * @param replyToAddres
	 *           the replyToAddres to set
	 */
	public void setReplyToAddres(final String replyToAddres)
	{
		this.replyToAddres = replyToAddres;
	}

	/**
	 * @return the body
	 */
	public String getBody()
	{
		return body;
	}

	/**
	 * @param body
	 *           the body to set
	 */
	public void setBody(final String body)
	{
		this.body = body;
	}

	/**
	 * @return the subject
	 */
	public String getSubject()
	{
		return subject;
	}

	/**
	 * @param subject
	 *           the subject to set
	 */
	public void setSubject(final String subject)
	{
		this.subject = subject;
	}

	/**
	 * @return the attachments
	 */
	public List<EmailAttachmentModel> getAttachments()
	{
		return attachments;
	}
}
