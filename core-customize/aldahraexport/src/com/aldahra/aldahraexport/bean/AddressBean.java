/**
 *
 */
package com.aldahra.aldahraexport.bean;

import java.io.Serializable;


/**
 * @author jaafarnaddaf
 *
 */
public class AddressBean implements Serializable
{
	String address;
	String displayName;

	/**
	 * @return the address
	 */
	public String getAddress()
	{
		return address;
	}

	/**
	 * @param address
	 *           the address to set
	 */
	public void setAddress(final String address)
	{
		this.address = address;
	}

	/**
	 * @return the displayName
	 */
	public String getDisplayName()
	{
		return displayName;
	}

	/**
	 * @param displayName
	 *           the displayName to set
	 */
	public void setDisplayName(final String displayName)
	{
		this.displayName = displayName;
	}
}
