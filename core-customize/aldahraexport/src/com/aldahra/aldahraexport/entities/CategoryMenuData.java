package com.aldahra.aldahraexport.entities;

import de.hybris.platform.commercefacades.product.data.CategoryData;

import java.util.ArrayList;
import java.util.List;


/**
 * @author m-nasro
 *
 */
public class CategoryMenuData
{
	private CategoryData superCategoryData;
	private final List<CategoryMenuData> subCategoryDataList = new ArrayList<>();
	private boolean containsProducts;

	/**
	 *
	 */
	public CategoryMenuData()
	{
		// YTODO Auto-generated constructor stub
	}

	/**
	 * @return the superCategoryData
	 */
	public CategoryData getSuperCategoryData()
	{
		return superCategoryData;
	}

	/**
	 * @param superCategoryData
	 *           the superCategoryData to set
	 */
	public void setSuperCategoryData(final CategoryData superCategoryData)
	{
		this.superCategoryData = superCategoryData;
	}

	/**
	 * @return the containsProducts
	 */
	public boolean isContainsProducts()
	{
		return containsProducts;
	}

	/**
	 * @param containsProducts
	 *           the containsProducts to set
	 */
	public void setContainsProducts(final boolean containsProducts)
	{
		this.containsProducts = containsProducts;
	}

	/**
	 * @return the subCategoryDataList
	 */
	public List<CategoryMenuData> getSubCategoryDataList()
	{
		return subCategoryDataList;
	}





}
