package com.aldahra.aldahraexport;

import de.hybris.platform.acceleratorservices.model.email.EmailAttachmentModel;
import de.hybris.platform.catalog.CatalogService;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaFolderModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelRemovalException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.media.MediaService;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.log4j.Logger;
import org.assertj.core.util.Files;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.aldahra.aldahraexport.bean.AddressBean;
import com.aldahra.aldahraexport.bean.EmailBean;
import com.aldahra.aldahraexport.model.ReportCronJobModel;
import com.aldahra.aldahraexport.process.email.actions.SendEmailAction;
import com.aldahra.aldahraexport.services.ErabiaReportService;


/**
 * @author Baha Almtoor
 *
 */
public class DataExporter extends AbstractJobPerformable<ReportCronJobModel>
{
	private static final Logger LOG = Logger.getLogger(DataExporter.class.getName());

	@Resource(name = "erabiaReportService")
	private ErabiaReportService erabiaReportService;

	@Resource(name = "mediaService")
	private MediaService mediaService;

	@Resource(name = "sendCustomEmail")
	private SendEmailAction sendCustomEmail;

	@Resource(name = "catalogService")
	private CatalogService catalogService;

	@Resource(name = "catalogVersionService")
	private CatalogVersionService catalogVersionService;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;


	public ErabiaReportService getErabiaReportService()
	{
		return erabiaReportService;
	}

	public MediaService getMediaService()
	{
		return mediaService;
	}

	public SendEmailAction getSendCustomEmail()
	{
		return sendCustomEmail;
	}


	public CatalogService getCatalogService()
	{
		return catalogService;
	}

	public CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	protected String getFileName(final ReportCronJobModel cronjob)
	{
		String fileName = cronjob.getFileName();
		if (StringUtils.isBlank(fileName))
		{
			fileName = cronjob.getCode();
		}

		return fileName;
	}

	protected String getDatePattern(final ReportCronJobModel cronjob)
	{
		String pattern = cronjob.getAdvancedExportDateformat();
		if (StringUtils.isBlank(pattern))
		{
			pattern = "yyyy-dd-mm";
		}

		return pattern;
	}

	protected void delete(final File file)
	{
		if (file != null && file.exists())
		{
			Files.delete(file);
		}
	}

	protected void writeResults(final ReportCronJobModel cronjob, final File file, final List<ArrayList<String>> results)
	{
		if (file != null)
		{
			try (PrintWriter writer = new PrintWriter(file))
			{
				writer.println(cronjob.getHeader());

				if (CollectionUtils.isNotEmpty(results))
				{
					for (final ArrayList<String> resultList : results)
					{
						if (CollectionUtils.isNotEmpty(resultList))
						{
							final String line = StringUtils.join(resultList.toArray(), ",");
							writer.println(line);
						}
					}
				}
			}
			catch (final FileNotFoundException ex)
			{
				LOG.error(ex.getMessage());
			}
		}
	}

	protected boolean sendMail(final ReportCronJobModel cronjob, final File file, final List<ArrayList<String>> results)
	{
		if (Boolean.TRUE.equals(cronjob.getSendMail()))
		{
			if (CollectionUtils.isNotEmpty(results)
					|| (CollectionUtils.isEmpty(results) && Boolean.TRUE.equals(cronjob.getAllowSendEmailOrFTPWithBlankResults())))
			{
				final String fileName = getFileName(cronjob);
				if (!sendMail(file, cronjob, fileName))
				{
					return false;
				}
			}
		}

		return true;
	}

	protected boolean sendBySFTP(final ReportCronJobModel cronjob, final File file, final List<ArrayList<String>> results)
	{
		if (Boolean.TRUE.equals(cronjob.getSaveToFTPServer()))
		{
			if (CollectionUtils.isNotEmpty(results)
					|| (CollectionUtils.isEmpty(results) && Boolean.TRUE.equals(cronjob.getAllowSendEmailOrFTPWithBlankResults())))
			{
				if (Boolean.TRUE.equals(cronjob.getSecureFTP()))
				{
					if (!sendBySFTP(file, cronjob))
					{
						LOG.error("No send by sftp");
						return false;
					}
				}
				else
				{
					if (!sendByFTP(file, cronjob))
					{
						LOG.error("No send by ftp");
						return false;
					}
				}
			}
		}

		return true;
	}

	protected List<ArrayList<String>> getResults(final ReportCronJobModel cronjob)
	{
		final String header = cronjob.getHeader() == null ? "" : cronjob.getHeader();
		final int columns = StringUtils.split(header, ',').length;
		final List<ArrayList<String>> results = getErabiaReportService().getResultForQuery(columns, cronjob.getQuery());

		if (results == null)
		{
			return Collections.emptyList();
		}

		return results;
	}

	protected boolean checkAdvancedExportDir(final ReportCronJobModel cronjob)
	{
		final String advancedExportDir = cronjob.getAdvancedExportDir();

		if (StringUtils.isBlank(advancedExportDir))
		{
			final String error = String.format("Invalid Advanced Export Directory, Where value=[%s]", advancedExportDir);
			LOG.error(error);
			return false;
		}

		final File dir = new File(advancedExportDir);
		if (!dir.exists() || !dir.mkdirs())
		{
			LOG.error("Directory " + dir + " does not exist. Unable to create it.");
			return false;
		}

		return true;
	}

	protected File getFile(final ReportCronJobModel cronjob) throws IOException
	{
		File file = null;
		final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(getDatePattern(cronjob));
		final String fileName = getFileName(cronjob);
		final String exportDir = cronjob.getAdvancedExportDir() == null ? "" : cronjob.getAdvancedExportDir();
		if (Boolean.TRUE.equals(cronjob.getAddDateToName()))
		{
			file = new File(exportDir + fileName + "_" + simpleDateFormat.format(new Date()) + ".csv");
		}
		else
		{
			file = new File(exportDir + fileName + ".csv");
		}

		if (!file.exists())
		{
			file.createNewFile();
		}

		return file;
	}

	protected boolean sendByFTP(final File file, final ReportCronJobModel cronjob)
	{
		final String host = cronjob.getHostname();
		final String username = cronjob.getUsername();
		final String password = cronjob.getPassword();
		final FTPClient ftpClient = new FTPClient();

		try (InputStream fis = new FileInputStream(file))
		{
			int port = 21;
			if (cronjob.getPort() != null)
			{
				port = cronjob.getPort().intValue();
			}

			ftpClient.connect(host, port);
			ftpClient.login(username, password);
			ftpClient.enterLocalPassiveMode();
			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

			final int reply = ftpClient.getReplyCode();
			if (!FTPReply.isPositiveCompletion(reply))
			{
				ftpClient.disconnect();
				return false;
			}

			LOG.info("Start transfer " + file.getName());
			final boolean done = ftpClient.storeFile(cronjob.getPath() + file.getName(), fis);
			if (done)
			{
				LOG.info(file.getName() + " is transfer successfully.");
				return true;
			}
			else
			{
				return false;
			}
		}
		catch (final IOException ex)
		{
			LOG.info("Error Found while tranfer: " + ex.getMessage());
			return false;
		}
		finally
		{
			try
			{
				if (ftpClient.isConnected())
				{
					ftpClient.logout();
					ftpClient.disconnect();
				}
			}
			catch (final IOException ex)
			{
				LOG.info("Error: " + ex.getMessage());
			}
		}
	}

	public boolean sendBySFTP(final File file, final ReportCronJobModel cronjob)
	{
		final String host = cronjob.getHostname();
		final String username = cronjob.getUsername();
		final String password = cronjob.getPassword();

		Session session = null;
		Channel channel = null;
		ChannelSftp channelSftp = null;

		try (InputStream fis = new FileInputStream(file))
		{
			int port = 22;
			if (cronjob.getPort() != null)
			{
				port = cronjob.getPort().intValue();
			}

			final JSch jsch = new JSch();
			session = jsch.getSession(username, host, port);
			session.setPassword(password);
			final java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();

			LOG.info("Host connected." + cronjob.getHostname());

			channel = session.openChannel("sftp");
			channel.connect();

			LOG.info("sftp channel opened and connected.");

			channelSftp = (ChannelSftp) channel;
			channelSftp.cd(cronjob.getPath());
			channelSftp.put(fis, file.getName());

			LOG.info(file.getName() + " transfered successfully to host.");

			return true;
		}
		catch (final Exception ex)
		{
			LOG.info("Error Found while tranfer the response: " + ex.getMessage());
			return false;
		}
		finally
		{
			if (channelSftp != null)
			{
				channelSftp.exit();
				LOG.info("sftp Channel exited.");
			}
			if (channel != null)
			{
				channel.disconnect();
				LOG.info("Channel disconnected.");
			}
			if (session != null)
			{
				session.disconnect();
				LOG.info("Host Session disconnected.");
			}
		}
	}

	protected boolean sendMail(final File file, final ReportCronJobModel cronjob, final String fileName)
	{
		boolean isSent = false;
		final EmailBean email = new EmailBean();
		try (DataInputStream data = new DataInputStream(new FileInputStream(file)))
		{
			final String fromAddressString = getConfigurationService().getConfiguration().getString("mail.smtp.user");
			final String fromDisplayName = getConfigurationService().getConfiguration().getString("report.mail.from.display");
			final AddressBean toAddress = new AddressBean();
			toAddress.setAddress(cronjob.getToAddress());
			toAddress.setDisplayName(cronjob.getToAddress());
			final AddressBean fromAddress = new AddressBean();
			fromAddress.setAddress(fromAddressString);
			fromAddress.setDisplayName(fromDisplayName);
			email.setBody("File Attached");
			email.setSubject(cronjob.getSubject());
			email.getToAddresses().add(toAddress);
			email.setFromAddress(fromAddress);
			email.setReplyToAddres(fromAddress.getAddress());
			final EmailAttachmentModel attachment = createEmailAttachment(data, cronjob, file.getName(), "text/csv");
			email.getAttachments().add(attachment);
			isSent = getSendCustomEmail().sendEmail(email);

			modelService.remove(attachment);
			LOG.info("Successfully removed attachments for [" + email.getSubject() + "]");
		}
		catch (final ModelRemovalException mre)
		{
			LOG.error("Failed to remove attachments for [" + email.getSubject() + "]");
			LOG.error(mre.getMessage());
		}
		catch (final Exception e)
		{
			LOG.error(e.getMessage());
		}

		if (isSent)
		{
			LOG.info("Mail [Subject:" + cronjob.getSubject() + " - To: " + cronjob.getToAddress() + " - File Name:" + fileName
					+ " - Cronjob:" + cronjob.getCode() + "] Sent Successfully");
			return true;
		}
		else
		{
			LOG.error("Could not send mail [Subject:" + cronjob.getSubject() + " - To: " + cronjob.getToAddress() + " - File Name:"
					+ fileName + " - Cronjob:" + cronjob.getCode() + "]");
			return false;
		}
	}

	protected CatalogVersionModel getCatalogVersion(final ReportCronJobModel cronjob)
	{
		final String catalog = cronjob.getMailCatalogName().getId();

		return getCatalogVersionService().getCatalogVersion(catalog, "Staged");
	}

	protected EmailAttachmentModel createEmailAttachment(final DataInputStream masterDataStream, final ReportCronJobModel cronjob,
			final String filename, final String mimeType)
	{
		final String code = cronjob.getCode();
		String attachedFormat = cronjob.getMailAttachmentDateformat();
		attachedFormat = (attachedFormat == null || attachedFormat.trim().isEmpty()) ? "yyyy-dd-mm" : attachedFormat;
		final SimpleDateFormat format = new SimpleDateFormat(attachedFormat);

		final EmailAttachmentModel attachment = modelService.create(EmailAttachmentModel.class);
		attachment.setCode(code + "-" + format.format(new Date()));
		attachment.setMime(mimeType);
		attachment.setRealFileName(filename);
		attachment.setCatalogVersion(getCatalogVersion(cronjob));
		modelService.save(attachment);

		getMediaService().setStreamForMedia(attachment, masterDataStream, filename, mimeType, getEmailAttachmentsMediaFolder());
		return attachment;
	}

	protected MediaFolderModel getEmailAttachmentsMediaFolder()
	{
		MediaFolderModel folder = new MediaFolderModel();
		final String folderName = "email-attachments";
		try
		{
			folder = getMediaService().getFolder(folderName);
		}
		catch (final UnknownIdentifierException uie)
		{
			folder.setQualifier(folderName);
			folder.setPath(folderName);
			modelService.save(folder);
		}
		catch (final AmbiguousIdentifierException aie)
		{
			folder = getMediaService().getRootFolder();
		}
		return folder;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable#perform(de.hybris.platform.cronjob.model.
	 * CronJobModel )
	 */
	@Override
	public PerformResult perform(final ReportCronJobModel cronjob)
	{
		File file = null;
		try
		{
			file = getFile(cronjob);

			final String filePathInfo = String.format("File path=[%s]", file.getAbsolutePath());
			LOG.info(filePathInfo);

			final List<ArrayList<String>> results = getResults(cronjob);
			writeResults(cronjob, file, results);

			final boolean sendBySFTP = sendBySFTP(cronjob, file, results);
			if (!sendBySFTP)
			{
				LOG.error("No send by ftp/sftp");
				return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
			}

			final boolean sendMail = sendMail(cronjob, file, results);
			if (!sendMail)
			{
				LOG.error("not send mail");
				return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
			}

			LOG.info("CSV file \"" + file.getAbsolutePath() + "\" exported successfully");

			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
		catch (final Exception ex)
		{
			LOG.error("CSV file couldn't be exported");
			LOG.error(ex.getMessage());
			return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
		}
		finally
		{
			delete(file);
		}
	}
}
