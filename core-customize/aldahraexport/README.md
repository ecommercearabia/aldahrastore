# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Used language details 
* Java 11
* Eclipse: 2018-09 (4.9.0) 

---

### What is this repository for?
This extension is used to Export/Import data

---

### How do I get set up?
* Put this extension inside the /sefamstore/core-customize folder
* Add this extension to the "localextensions.xml" file in "Extension Info" 
* Ant-clean all build 
* Run the server 
* Update extension from "HAC" 
* Clear the hMC configuration from the database
* Create essential data
* Localize types

---

### Who do I talk to?
* Repo owner or admin
* Development team

---
### Special Notes



