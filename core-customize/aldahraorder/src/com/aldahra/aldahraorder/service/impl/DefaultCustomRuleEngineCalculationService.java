/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraorder.service.impl;

import de.hybris.order.calculation.domain.Order;
import de.hybris.order.calculation.domain.OrderDiscount;
import de.hybris.order.calculation.money.Currency;
import de.hybris.order.calculation.money.Money;
import de.hybris.platform.ruleengineservices.calculation.impl.DefaultRuleEngineCalculationService;

import java.math.BigDecimal;
/**
 * @author amjad.shati@erabia.com
 *
 */
public class DefaultCustomRuleEngineCalculationService extends DefaultRuleEngineCalculationService
{
	@Override
	protected OrderDiscount createOrderDiscount(final Order cart, final boolean absolute, final BigDecimal amount)
	{
		final Currency currency = cart.getCurrency();
		final BigDecimal adjustedDiscountAmount = absolute ? amount
				: this.convertPercentageDiscountToAbsoluteDiscount(amount, cart);
		final Money discountAmount = new Money(adjustedDiscountAmount, currency);
		OrderDiscount discount = new OrderDiscount(discountAmount);

		final Money discountAfterChecking = getDiscountAfterChecking(cart, discount, currency);
		if (discountAfterChecking != null)
		{
			discount = new OrderDiscount(discountAfterChecking);
		}

		if (this.getMinimumAmountValidationStrategy().isOrderLowerLimitValid(cart, discount))
		{
			cart.addDiscount(discount);
			return discount;
		}
		final Money zeroDiscountAmount = new Money(BigDecimal.ZERO, currency);
		final OrderDiscount zeroDiscount = new OrderDiscount(zeroDiscountAmount);
		cart.addDiscount(zeroDiscount);
		return zeroDiscount;
	}

	private Money getDiscountAfterChecking(final Order cart, final OrderDiscount discount, final Currency currency)
	{
		if (cart.getDiscounts().contains(discount))
		{
			throw new IllegalArgumentException("The order already has the discount.");
		}
		try
		{
			cart.addDiscount(discount);
			final Money totalDiscount = cart.getTotalDiscount();

			if (cart.getSubTotal().subtract(totalDiscount).getAmount().doubleValue() < 0)
			{
				return new Money(cart.getSubTotal().getAmount(), currency);
			}
		}
		finally
		{
			cart.removeDiscount(discount);
		}
		return null;
	}
}
