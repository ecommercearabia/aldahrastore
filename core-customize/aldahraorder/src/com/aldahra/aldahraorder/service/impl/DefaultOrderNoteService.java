/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraorder.service.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;

import javax.annotation.Resource;

import com.aldahra.aldahraorder.model.OrderNoteEntryModel;
import com.aldahra.aldahraorder.model.OrderNoteModel;
import com.aldahra.aldahraorder.service.OrderNoteService;
import com.google.common.base.Preconditions;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class DefaultOrderNoteService implements OrderNoteService
{
	private static final String CMSSITE_IS_NULL = "CMSSite must not be null";

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Override
	public List<OrderNoteModel> getOrderNotesByCurrentSite()
	{
		return getOrderNotes(cmsSiteService.getCurrentSite());
	}

	@Override
	public List<OrderNoteModel> getOrderNotes(final CMSSiteModel cmsSiteModel)
	{
		Preconditions.checkArgument(cmsSiteModel != null, CMSSITE_IS_NULL);
		return cmsSiteModel.getOrderNotes();
	}

	@Override
	public void saveOrderNoteEntry(final OrderNoteEntryModel noteModel, final CartModel cart)
	{
		modelService.refresh(cart);
		modelService.save(noteModel);
		modelService.refresh(noteModel);
		cart.setOrderNoteEntry(noteModel);
		modelService.save(cart);
	}

}