package com.aldahra.aldahraorder.cart.service;

import de.hybris.platform.core.model.order.CartModel;

import com.aldahra.aldahraorder.cart.exception.CartValidationException;


/**
 * @author mnasro
 *
 */
public interface CartValidationService {

	/**
	 *
	 * @throws CartValidationException
	 */
	public boolean validateCartMinAmount(CartModel cartModel) throws CartValidationException;


	public boolean validateCartMinAmountByCurrentCart() throws CartValidationException;

	/**
	 *
	 * @throws CartValidationException
	 */
	public boolean validateCartMaxAmount(CartModel cartModel) throws CartValidationException;

	public boolean validateCartMaxAmountByCurrentCart() throws CartValidationException;


	public boolean validateProfileAttributes(CartModel cartModel) throws CartValidationException;

	public boolean validateProfileAttributesByCurrentCart() throws CartValidationException;
}
