/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraorder.cart.exception.enums;

/**
 *
 */
public enum CartExceptionType
{
	MAX_QTY, MIN_QTY, MIN_AMOUNT, MAX_AMOUNT, MAX_WEIGHT, MIN_WEIGHT, STORE, SHIPPINGMETHOD, MISSING_VALUE;
}
