package com.aldahra.aldahraorder.cart.exception;

import com.aldahra.aldahraorder.cart.exception.enums.CartExceptionType;


/**
 *
 */
public class CartValidationException extends Exception {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private CartExceptionType cartExceptionType;
	private String[] errorParm;

	/**
	 *
	 */
	public CartValidationException(final String message, final CartExceptionType cartExceptionType, final String[] errorParm)
	{
		super(message);
		this.cartExceptionType = cartExceptionType;
		this.errorParm = errorParm;
	}

	/**
	 *
	 */
	public CartValidationException(final CartExceptionType cartExceptionType, final String[] errorParm)
	{
		super();
		this.cartExceptionType = cartExceptionType;
		this.errorParm = errorParm;
	}

	/**
	 * @return the errorParm
	 */
	public String[] getErrorParm() {
		return errorParm;
	}

	/**
	 * @param errorParm
	 *            the errorParm to set
	 */
	public void setErrorParm(final String[] errorParm) {
		this.errorParm = errorParm;
	}

	/**
	 * @return the cartExceptionType
	 */
	public CartExceptionType getCartExceptionType() {
		return cartExceptionType;
	}

	/**
	 * @param cartExceptionType
	 *            the cartExceptionType to set
	 */
	public void setCartExceptionType(final CartExceptionType cartExceptionType) {
		this.cartExceptionType = cartExceptionType;
	}
}