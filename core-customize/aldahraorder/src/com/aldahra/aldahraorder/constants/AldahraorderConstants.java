/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraorder.constants;

/**
 * Global class for all Aldahraorder constants. You can add global constants for your extension into this class.
 */
public final class AldahraorderConstants extends GeneratedAldahraorderConstants
{
	public static final String EXTENSIONNAME = "aldahraorder";

	private AldahraorderConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "aldahraorderPlatformLogo";
}
