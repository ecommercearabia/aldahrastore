/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aldahra.aldahraorder.dao.impl;

import de.hybris.platform.commerceservices.search.dao.impl.DefaultPagedGenericDao;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

import java.util.Map;
import java.util.Map.Entry;


public class OrderByNullVersionIdWithCodePagedDao extends DefaultPagedGenericDao
{
	protected final String typeCode;
	protected static final String WILD_CARD = "%";

	public OrderByNullVersionIdWithCodePagedDao(final String typeCode)
	{
		super(typeCode);
		this.typeCode = typeCode;
	}

	@Override
	protected StringBuilder createQueryString()
	{
		final StringBuilder builder = new StringBuilder(25);
		builder.append("SELECT {c:").append(OrderModel.PK).append("} ");
		builder.append("FROM {").append(OrderModel._TYPECODE).append(" AS c} ");
		builder.append("WHERE {c:code} LIKE ?code");
		return builder;
	}

	public SearchPageData<OrderModel> findCustom(final Map<String, String> params, final PageableData pageableData)
	{
		final FlexibleSearchQuery query = createFlexibleSearchQueryCustom(params);
		return getPagedFlexibleSearchService().search(query, pageableData);
	}

	protected FlexibleSearchQuery createFlexibleSearchQueryCustom(final Map<String, String> params)
	{
		final StringBuilder builder = createQueryString();
		final FlexibleSearchQuery query = new FlexibleSearchQuery(builder.toString());

		if (params == null || params.isEmpty())
		{
			return query;
		}

		for (final Entry<String, String> entry : params.entrySet())
		{
			final String likeValue = WILD_CARD + entry.getValue() + WILD_CARD;
			params.put(entry.getKey(), likeValue);
		}
		query.addQueryParameters(params);
		return query;
	}
}
