/**
 *
 */
package com.aldahra.aldahraorderconfirmation.service;

import de.hybris.platform.acceleratorservices.email.EmailGenerationService;


/**
 * @author mohammad-abumuhasien
 *
 */
public interface CustomEmailGenerationService extends EmailGenerationService
{

}
