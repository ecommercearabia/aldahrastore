/**
 *
 */
package com.aldahra.aldahraorderconfirmation.service;

import java.util.List;

import com.aldahra.aldahraorderconfirmation.model.OrderConfirmationEmailModel;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface OrderEmailService
{
	List<OrderConfirmationEmailModel> findByStoreUid(String storeUid);
}
