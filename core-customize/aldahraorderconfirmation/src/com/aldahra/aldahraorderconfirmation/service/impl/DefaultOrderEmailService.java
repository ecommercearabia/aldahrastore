/**
 *
 */
package com.aldahra.aldahraorderconfirmation.service.impl;

import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.List;

import javax.annotation.Resource;

import com.aldahra.aldahraorderconfirmation.dao.OrderEmailDao;
import com.aldahra.aldahraorderconfirmation.model.OrderConfirmationEmailModel;
import com.aldahra.aldahraorderconfirmation.service.OrderEmailService;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class DefaultOrderEmailService implements OrderEmailService
{
	@Resource(name = "orderEmailDao")
	private OrderEmailDao orderEmailDao;

	public OrderEmailDao getOrderEmailDao()
	{
		return orderEmailDao;
	}

	@Override
	public List<OrderConfirmationEmailModel> findByStoreUid(final String storeUid)
	{
		ServicesUtil.validateParameterNotNull(storeUid, "storeUid must not be null");

		return getOrderEmailDao().findByStoreUid(storeUid);
	}
}
