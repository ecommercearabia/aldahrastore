/**
 *
 */
package com.aldahra.aldahraorderconfirmation.service.impl;

import de.hybris.platform.acceleratorservices.email.EmailService;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageTemplateModel;
import de.hybris.platform.acceleratorservices.model.email.EmailAddressModel;
import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.acceleratorservices.process.email.context.EmailContextFactory;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.commons.renderer.RendererService;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.store.services.BaseStoreService;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.aldahra.Ordermanagement.model.ReferralCodeShareEmailProcessModel;
import com.aldahra.aldahraorderconfirmation.enums.EmailTransactionType;
import com.aldahra.aldahraorderconfirmation.enums.OrderConfirmationEmailType;
import com.aldahra.aldahraorderconfirmation.enums.OrderConfirmationSendEmailType;
import com.aldahra.aldahraorderconfirmation.model.EmailModel;
import com.aldahra.aldahraorderconfirmation.model.OrderConfirmationEmailModel;
import com.aldahra.aldahraorderconfirmation.service.CustomEmailGenerationService;
import com.aldahra.aldahraorderconfirmation.service.OrderEmailService;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class DefaultCustomEmailGenerationService extends AbstractBusinessService implements CustomEmailGenerationService
{
	private static final Logger LOG = Logger.getLogger(DefaultCustomEmailGenerationService.class);
	protected static final String CONSIGNMENT_CANCEL_EMAIL_UID = "consignmentCancelEmail";
	protected static final String REFERRAL_EMAIL = "ReferralCodeShareEmail";
	protected static final String ORDER_CONFIRMATION_EMAIL_UID = "OrderConfirmationEmail";
	protected static final String ORDER_CANCELLED_EMAIL_UID = "OrderCancelledEmail";
	protected static final String ORDER_PARTIALLY_CANCELED_EMAIL_UID = "OrderPartiallyCanceledEmail";
	protected static final String INTEGRATION_ERROR_EMAIL_UID = "GenerateErrorEmail";
	protected static final String INTEGRATION_NOTIFICATION_EMAIL_UID = "GenerateNotificationEmail";

	private transient EmailService emailService;
	private transient RendererService rendererService;
	private transient EmailContextFactory<BusinessProcessModel> emailContextFactory;
	private transient ConfigurationService configurationService;

	@Resource(name = "userService")
	private transient UserService userService;

	@Resource(name = "baseStoreService")
	private transient BaseStoreService baseStoreService;

	@Resource(name = "orderEmailService")
	private transient OrderEmailService orderEmailService;

	private OrderModel order;

	public UserService getUserService()
	{
		return userService;
	}

	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	public OrderEmailService getOrderEmailService()
	{
		return orderEmailService;
	}

	public OrderModel getOrder()
	{
		return order;
	}

	public void setOrder(final OrderModel order)
	{
		this.order = order;
	}

	protected void setReferralEmailMessage(final EmailMessageModel emailMessage, final Collection<String> emails,
			final OrderConfirmationSendEmailType type)
	{
		if (CollectionUtils.isNotEmpty(emails))
		{
			final List<EmailAddressModel> emailAddresses = new ArrayList<>();
			for (final String email : emails)
			{
				if (email != null && StringUtils.isNotBlank(email))
				{
					final EmailAddressModel emailAddress = getEmailService().getOrCreateEmailAddressForEmail(email, email);
					emailAddresses.add(emailAddress);
				}
			}

			if (!emailAddresses.isEmpty())
			{
				if (OrderConfirmationSendEmailType.TO.equals(type))
				{
					emailMessage.setToAddresses(emailAddresses);
				}
				else if (OrderConfirmationSendEmailType.CC.equals(type))
				{
					emailMessage.setCcAddresses(emailAddresses);
				}
				else if (OrderConfirmationSendEmailType.BCC.equals(type))
				{
					emailMessage.setBccAddresses(emailAddresses);
				}

				getModelService().save(emailMessage);
			}
		}
	}

	protected void setEmailMessage(final EmailMessageModel emailMessage, final List<EmailModel> emails,
			final OrderConfirmationSendEmailType type)
	{
		if (CollectionUtils.isNotEmpty(emails))
		{
			final List<EmailAddressModel> emailAddresses = new ArrayList<>();
			for (final EmailModel email : emails)
			{
				if (email != null && StringUtils.isNotBlank(email.getEmail()))
				{
					final EmailAddressModel emailAddress = getEmailService().getOrCreateEmailAddressForEmail(email.getEmail(),
							email.getEmail());
					emailAddresses.add(emailAddress);
				}
			}

			if (!emailAddresses.isEmpty())
			{
				if (OrderConfirmationSendEmailType.TO.equals(type))
				{
					emailMessage.setToAddresses(emailAddresses);
				}
				else if (OrderConfirmationSendEmailType.CC.equals(type))
				{
					emailMessage.setCcAddresses(emailAddresses);
				}
				else if (OrderConfirmationSendEmailType.BCC.equals(type))
				{
					emailMessage.setBccAddresses(emailAddresses);
				}

				getModelService().save(emailMessage);
			}
		}
	}

	protected void fillEmails(final Set<EmailModel> emails, final List<EmailModel> emailsTO, final List<EmailModel> emailsCC,
			final List<EmailModel> emailsBCC)
	{
		if (CollectionUtils.isNotEmpty(emails))
		{
			for (final EmailModel email : emails)
			{
				if (email != null)
				{
					if (OrderConfirmationSendEmailType.TO.equals(email.getType()))
					{
						emailsTO.add(email);
					}
					else if (OrderConfirmationSendEmailType.CC.equals(email.getType()))
					{
						emailsCC.add(email);
					}
					else if (OrderConfirmationSendEmailType.BCC.equals(email.getType()))
					{
						emailsBCC.add(email);
					}
				}
			}
		}
	}

	protected void merchant(final EmailMessageModel emailMessage, final Collection<String> toEmails)
	{
		setReferralEmailMessage(emailMessage, toEmails, OrderConfirmationSendEmailType.TO);
	}

	protected void merchant(final EmailMessageModel emailMessage, final OrderConfirmationEmailModel orderConfirmationEmail)
	{
		final List<EmailModel> emailsTO = new ArrayList<>();
		final List<EmailModel> emailsCC = new ArrayList<>();
		final List<EmailModel> emailsBCC = new ArrayList<>();

		fillEmails(orderConfirmationEmail.getEmails(), emailsTO, emailsCC, emailsBCC);
		setEmailMessage(emailMessage, emailsTO, OrderConfirmationSendEmailType.TO);
		setEmailMessage(emailMessage, emailsCC, OrderConfirmationSendEmailType.CC);
		setEmailMessage(emailMessage, emailsBCC, OrderConfirmationSendEmailType.BCC);
	}

	protected void sendEmails(final EmailMessageModel emailMessage, final EmailTransactionType transactionType)
	{
		if (emailMessage != null)
		{
			final String storeUid = getBaseStoreService().getCurrentBaseStore().getUid();
			final List<OrderConfirmationEmailModel> orderConfirmationEmails = getOrderEmailService().findByStoreUid(storeUid);

			if (CollectionUtils.isNotEmpty(orderConfirmationEmails) && orderConfirmationEmails.get(0) != null
					&& orderConfirmationEmails.get(0).getTransactionType() != null)
			{
				final OrderConfirmationEmailModel orderConfirmationEmail = orderConfirmationEmails.get(0);
				final EmailTransactionType emailTransactionType = orderConfirmationEmail.getTransactionType();

				if (EmailTransactionType.ALL.equals(emailTransactionType) || emailTransactionType.equals(transactionType))
				{
					final OrderConfirmationEmailType type = orderConfirmationEmail.getType();
					if (type == null || OrderConfirmationEmailType.MERCHANT.getCode().equals(type.getCode()))
					{
						merchant(emailMessage, orderConfirmationEmail);
					}
				}
			}
		}
	}


	protected void sendReferralCodeEmails(final EmailMessageModel emailMessage, final BusinessProcessModel businessProcessModel)
	{
		if (emailMessage != null && businessProcessModel instanceof ReferralCodeShareEmailProcessModel)
		{
			final ReferralCodeShareEmailProcessModel referralCodeShareEmailProcessModel = (ReferralCodeShareEmailProcessModel) businessProcessModel;
			merchant(emailMessage, referralCodeShareEmailProcessModel.getToAddresses());
		}
	}

	protected boolean validate(final AbstractEmailContext<BusinessProcessModel> emailContext)
	{
		boolean valid = true;
		if (StringUtils.isBlank(emailContext.getToEmail()))
		{
			LOG.error("Missing ToEmail in AbstractEmailContext");
			valid = false;
		}

		if (StringUtils.isBlank(emailContext.getFromEmail()))
		{
			LOG.error("Missing FromEmail in AbstractEmailContext");
			valid = false;
		}
		return valid;
	}

	protected EmailMessageModel createEmailMessage(final String emailSubject, final String emailBody,
			final AbstractEmailContext<BusinessProcessModel> emailContext)
	{
		final List<EmailAddressModel> toEmails = new ArrayList<>();
		final EmailAddressModel toAddress = getEmailService().getOrCreateEmailAddressForEmail(emailContext.getToEmail(),
				emailContext.getToDisplayName());
		toEmails.add(toAddress);

		final EmailAddressModel fromAddress = getEmailService().getOrCreateEmailAddressForEmail(emailContext.getFromEmail(),
				emailContext.getFromDisplayName());
		return getEmailService().createEmailMessage(toEmails, new ArrayList<>(), new ArrayList<>(), fromAddress,
				emailContext.getFromEmail(), emailSubject, emailBody, null);
	}

	@Override
	public EmailMessageModel generate(final BusinessProcessModel businessProcessModel, final EmailPageModel emailPageModel)
	{
		ServicesUtil.validateParameterNotNull(emailPageModel, "EmailPageModel cannot be null");
		Assert.isInstanceOf(EmailPageTemplateModel.class, emailPageModel.getMasterTemplate(),
				"MasterTemplate associated with EmailPageModel should be EmailPageTemplate");

		final EmailPageTemplateModel emailPageTemplateModel = (EmailPageTemplateModel) emailPageModel.getMasterTemplate();
		final RendererTemplateModel bodyRenderTemplate = emailPageTemplateModel.getHtmlTemplate();
		Assert.notNull(bodyRenderTemplate, "HtmlTemplate associated with MasterTemplate of EmailPageModel cannot be null");
		final RendererTemplateModel subjectRenderTemplate = emailPageTemplateModel.getSubject();
		Assert.notNull(subjectRenderTemplate, "Subject associated with MasterTemplate of EmailPageModel cannot be null");

		final EmailMessageModel emailMessageModel;
		//This call creates the context to be used for rendering of subject and body templates.
		final AbstractEmailContext<BusinessProcessModel> emailContext = getEmailContextFactory().create(businessProcessModel,
				emailPageModel, bodyRenderTemplate);

		if (emailContext == null)
		{
			LOG.error("Failed to create email context for businessProcess [" + businessProcessModel + "]");
			throw new IllegalArgumentException("Failed to create email context for businessProcess [" + businessProcessModel + "]");
		}
		else
		{
			if (!validate(emailContext))
			{
				LOG.error("Email context for businessProcess [" + businessProcessModel + "] is not valid: "
						+ ReflectionToStringBuilder.toString(emailContext));
				throw new IllegalArgumentException("Email context for businessProcess [" + businessProcessModel + "] is not valid: "
						+ ReflectionToStringBuilder.toString(emailContext));
			}

			final StringWriter subject = new StringWriter();
			getRendererService().render(subjectRenderTemplate, emailContext, subject);

			final StringWriter body = new StringWriter();
			getRendererService().render(bodyRenderTemplate, emailContext, body);

			emailMessageModel = createEmailMessage(subject.toString(), body.toString(), emailContext);

			if (LOG.isDebugEnabled())
			{
				LOG.debug("Email Subject: " + emailMessageModel.getSubject());
				LOG.debug("Email Body: " + emailMessageModel.getBody());
			}

			if (businessProcessModel instanceof OrderProcessModel)
			{
				setOrder(((OrderProcessModel) businessProcessModel).getOrder());
			}

			final String emailPageUid = emailPageModel.getUid();
			if (emailPageUid.contains(ORDER_CONFIRMATION_EMAIL_UID))
			{
				sendEmails(emailMessageModel, EmailTransactionType.ORDERCONFIRMATIONEMAIL);
			}
			else if (emailPageUid.contains(ORDER_CANCELLED_EMAIL_UID) || emailPageUid.contains(ORDER_PARTIALLY_CANCELED_EMAIL_UID))
			{
				sendEmails(emailMessageModel, EmailTransactionType.ORDERCANCELLEDEMAIL);
			}
			else if (emailPageUid.contains(CONSIGNMENT_CANCEL_EMAIL_UID))
			{
				sendEmails(emailMessageModel, EmailTransactionType.CONSIGNMENTCANCELEMAIL);
			}
			else if (emailPageUid.contains(REFERRAL_EMAIL))
			{
				sendReferralCodeEmails(emailMessageModel, businessProcessModel);
			}
			else if (emailPageUid.contains(INTEGRATION_ERROR_EMAIL_UID))
			{
				sendEmails(emailMessageModel, EmailTransactionType.GENERATEERROREMAIL);
			}
			else if (emailPageUid.contains(INTEGRATION_NOTIFICATION_EMAIL_UID))
			{
				sendEmails(emailMessageModel, EmailTransactionType.GENERATENOTIFICATIONEMAIL);
			}
		}

		return emailMessageModel;
	}

	protected EmailService getEmailService()
	{
		return emailService;
	}

	@Autowired
	public void setEmailService(final EmailService emailService)
	{
		this.emailService = emailService;
	}

	protected RendererService getRendererService()
	{
		return rendererService;
	}

	@Autowired
	public void setRendererService(final RendererService rendererService)
	{
		this.rendererService = rendererService;
	}

	protected EmailContextFactory<BusinessProcessModel> getEmailContextFactory()
	{
		return emailContextFactory;
	}

	@Autowired
	public void setEmailContextFactory(final EmailContextFactory<BusinessProcessModel> emailContextFactory)
	{
		this.emailContextFactory = emailContextFactory;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Autowired
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
