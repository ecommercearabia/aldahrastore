/**
 *
 */
package com.aldahra.aldahraorderconfirmation.dao;

import java.util.List;

import com.aldahra.aldahraorderconfirmation.model.OrderConfirmationEmailModel;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface OrderEmailDao
{
	/**
	 * @param storeUid
	 * @return
	 */
	List<OrderConfirmationEmailModel> findByStoreUid(String storeUid);
}
