/**
 *
 */
package com.aldahra.aldahraorderconfirmation.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.aldahra.aldahraorderconfirmation.dao.OrderEmailDao;
import com.aldahra.aldahraorderconfirmation.model.OrderConfirmationEmailModel;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class DefaultOrderEmailDao extends AbstractItemDao implements OrderEmailDao
{
	private static final String FIND_BY_STORE_UID = "SELECT {oce.pk} FROM { "
			+ "OrderConfirmationEmail AS oce JOIN BaseStore AS bs ON {oce.store} = {bs.pk} } WHERE {bs.uid} = ?storeUid";

	@Override
	public List<OrderConfirmationEmailModel> findByStoreUid(final String storeUid)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put("storeUid", storeUid);

		return doSearch(FIND_BY_STORE_UID, params, OrderConfirmationEmailModel.class);
	}

	protected <T> List<T> doSearch(final String query, final Map<String, Object> params, final Class<T> resultClass)
	{
		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(query);
		if (params != null)
		{
			fQuery.addQueryParameters(params);
		}

		fQuery.setResultClassList(Collections.singletonList(resultClass));

		final SearchResult<T> searchResult = search(fQuery);
		return searchResult.getResult();
	}
}
