/**
 *
 */
package com.aldahra.backoffice.varinat;

import de.hybris.platform.platformbackoffice.variant.VariantAttributesSectionRenderer;
import de.hybris.platform.variants.model.VariantProductModel;

import org.zkoss.zk.ui.Component;

import com.hybris.cockpitng.core.config.impl.jaxb.editorarea.AbstractSection;
import com.hybris.cockpitng.dataaccess.facades.type.DataAttribute;
import com.hybris.cockpitng.dataaccess.facades.type.DataType;
import com.hybris.cockpitng.engine.WidgetInstanceManager;


/**
 * @author monzer
 *
 */
public class CustomVariantAttributesSectionRenderer extends VariantAttributesSectionRenderer
{
	@Override
	public void render(final Component parent, final AbstractSection configuration, final VariantProductModel data,
			final DataType dataType, final WidgetInstanceManager widgetInstanceManager)
	{
		// XXX Auto-generated method stub
		super.render(parent, configuration, data, dataType, widgetInstanceManager);
	}

	@Override
	protected boolean canChangeProperty(final DataAttribute attribute, final Object instance)
	{
		if (attribute != null)
		{
			final boolean attributeWritable = getObjectFacade().isNew(instance)
					? (attribute.isWritableOnCreation() || attribute.isWritable())
					: attribute.isWritable();
			return attributeWritable
					&& getEditAvailabilityProviderFactory().getProvider(attribute, instance).isAllowedToEdit(attribute, instance)
					&& getPermissionFacade().canChangeInstanceProperty(instance, attribute.getQualifier());
		}
		return false;
	}

}
